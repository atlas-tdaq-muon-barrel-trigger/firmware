# Sector Logic prototype guide

This directory contains the instructions to activate and program the Sector Logic v1 prototype board in the Laser Room.  

The [QuickStartGuide](/SL_prototype_guide/QuickStartGuide.md) reports all the steps needed to setup the SL prototype.  
For more detailed information please refer to the [l0muon_sl_example](/SL_prototype_guide/l0muon_sl_example) folder, which is a copy of the Endcap SL repository.