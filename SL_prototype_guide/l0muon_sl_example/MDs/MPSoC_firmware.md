
# L0Muon SL example /MPSoC PL firmware design

- [To get started](#to-get-started)
- [Mercury XU5](#mercury-xu5)
- [Block Design](#block-design)
    - [PS block](#ps-block)
    - [System Management Wizard](#system-management-wizard)
    - [PL Ethernet](#pl-ethernet)
    - [Debug hub](#debug-hub)
    - [AXI Chip2Chip](#axi-chip2chip)
    - [Others](#others)
    - [Address Map](#address-map)
- [GMII-RGMII converter](#gmii-rgmii-converter)
- [HDL wrapper](#hdl-wrapper)
- [Constraints file](#constraints-file)

## To get started

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/amishima/l0muon_sl_example.git
$ cd MercuryXU5_SL
$ vivado MercuryXU5_SL.xpr
```

The project is designed in Vivado 2020.2.


## Mercury XU5
The MPSoC is on a Mercury XU5 mezzanine card mounted on the SL board.

<div align="center">
    <img src="../figures/MercuryXU5.png" width="600px">
</div>

Enclustra provides a reference design for Mercury+ PE1, which is evaluation board on which Mercury XU5 is mounted.
The reference design contains preset specific to Mercury XU5, so we should take advantage of it.

<details><summary>View how to start with the PE1 reference design</summary><div>

If you make the block design from scratch based on the [Mercury XU5 PE1 Reference Design](https://github.com/enclustra/Mercury_XU5_PE1_Reference_Design), 
git clone the repository and checkout the tag depending on your Vivado version. 

```bash
$ git clone https://github.com/enclustra/Mercury_XU5_PE1_Reference_Design.git
$ cd Mercury_XU5_PE1_Reference_Design/
$ git checkout -b 2020.2_v1.2.0 refs/tags/2020.2_v1.2.0
```

Edit the `module_name` variable in `reference_design/scripts/settings.tcl` according to your module's name.


```diff
# ----------------------------------------------------------------------------------
# Modify this variable to select your module
- if {![info exists module_name]} {set module_name ME-XU5-2CG-1E-D10H}
+ if {![info exists module_name]} {set module_name ME-XU5-5EV-2I-D12E}
if {![info exists baseboard]}   {set baseboard PE1}
# ----------------------------------------------------------------------------------
```

Open Vivado and run the command below in Tcl Console displayed on the bottom of the window. 
This allows us to generate a Vivado project of the reference design as reference_design/vivado/<module name>/Mercury_XU5_PE1.xpr.

```tcl
cd <path to>/Mercury_XU5_PE1_Reference_Design/reference_design/
source ./scripts/create_project.tcl
```
As a tip, you can change the path by copying the project by [File] → [Project] → [Save As ...].
In this repository I have copied the project as MercuryXU5_SL/.

</div></details>


## Block Design
`design_1.bd` is the one we have designed and is in the compile order.
`Mercury_XU5.bd` is a copy of the [Mercury XU5 PE1 Reference Design](https://github.com/enclustra/Mercury_XU5_PE1_Reference_Design) and is unreferenced.

Landscape view of `design_1.bd` is shown in [`figures/MPSoC_design_1.pdf`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/figures/MPSoC_design_1.pdf).<br>
Landscape view of `Mercury_XU5.bd` is shown in [`figures/Mercury_XU5.pdf`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/figures/Mercury_XU5.pdf).

The following is a description of the `design_1.bd`

### PS block
The PS block in the `design_1.bd` is originally derived from the one in the `Mercury_XU5.bd`.
If necessary, change the PL clock frequency, the number of AXI port, and so on.

### System Management Wizard
The System Management Wizard block is a copy of the one in the `Mercury_XU5.bd`.
Refer to the block design and complete the connections around the block.

### PL Ethernet
The Clock Wizard block for the PL Ethernet is a copy of the one in the `Mercury_XU5.bd`.
Refer to the block design and complete the connections around the block.
The ports `GMII` and `MDIO` are also needed for the PL Ethernet. These are to be connected to the PS block.

### Debug hub
In the `design_1.bd`, there are two debug hub chains as shown in the figure below.
One of them is for the MPSoC PL, and the other is for the FPGA.

<div align="center">
    <img src="../figures/debug-hubs.png" width="1200px">
</div>

Refer to the block design `design_1.bd` and complete the block settings and the connections.
Please note that the DebugHub and Debug IP do not appear in the block design.

The names of the Debug Bridge blocks connected directly to the PS block via AXI are needed in the devicetree settings.
(Also refer to `Edit devicetree` in [`Build_bootfs.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/Build_bootfs.md))

### AXI Chip2Chip
In the design in this repository, MPSoC is the primary of the AXI Chip2Chip and can access BRAM or registers in the FPGA.

The PHY type AURORA64B66B is used. This is because the PHY types of the primary and secondary are consistent, and only AURORA64B66B is supported in the FPGA.

In the Aurora 64B66B block, GTH channels in X0Y4 is selected, and ref. clock source is MGTREFCLK1 of Quad X0Y1.
The Dataflow mode is Duplex, and the Interface is Streaming.
Shared Logic is included in the core.

PMA initialization input of the AXI Chip2Chip Bridge block `aurora_pma_init_in` is connected to the RTL module `pma_init_generator`.
With this module, we can control reset of the PMA via the AXI GPIO.

### Others
`GPIO_CFGPROG` is used when you start the FPGA configuration with a bitstream stored in the QSPI flash.<br>
`GPIO_FIRSEL` is for selecting which FireFly module is to be monitored via the I2C.<br>
`gpio_led` is connected to one of the orange LEDs on the Mercury XU5 mezzanine.
`heartbeat` module is connected to the other LED, and blink it in a heartbeat-like pattern.<br>
`Si5345_INSEL` is used to select the input clock source of the Si5345.<br>
`ila_hw_checker` is to monitor the board status.

### Address Map
The address map is as shown below.

<div align="center">
    <img src="../figures/address_editor.png" width="1200px">
</div>


## GMII-RGMII converter
This is an IP module to interface the Ethernet MAC in PS and Ethernet PHY on the Mercury XU5 mezzanine via the PL.
We should use one provided by Enclustra in the [Mercury XU5 PE1 Reference Design](https://github.com/enclustra/Mercury_XU5_PE1_Reference_Design) without any change.
In the case of 2020.2_v1.2.0, the file name is `Mercury_XU5_gmii2rgmii.edn`.

## HDL wrapper
The top module is described in `MercuryXU5_SL.vhd`. This is a wrapper of the block design and also of the GMII-RGMII converter.

## constraints file
There are two constraints files used in the our example design.
One of them is `MercuryXU5_SL.xdc` and device pin constraints are described in it.

The other is `MercuryXU5_SL_gmii2rgmii_timing.xdc`. 
This is derived from `Mercury_XU5_gmii2rgmii_timing.xdc`, which is proveided in the [Mercury XU5 PE1 Reference Design](https://github.com/enclustra/Mercury_XU5_PE1_Reference_Design).
In this original file, the name of block design is hard-coded, so we have to edit it.
