
# L0Muon SL example /FPGA firmware design

- [To get started](#to-get-started)
- [Block Design](#block-design)
- [Register Interface](#register-interface)
- [HDL wrapper](#hdl-wrapper)
- [constraints file](#constraints-file)

## To get started

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/amishima/l0muon_sl_example.git
$ cd FPGA/project_1
$ vivado project_1.xpr
```

The project is designed in Vivado 2020.2.

## Block Design
In the FPGA project, there are two block designs `design_1.bd` and `design_2.bd`.

In the `design_1.bd`, the secondary AXI Chip2Chip is simply connected to the BRAM.
So, you can write and read data from the MPSoC.

The `design_2.bd` is a little more practical design.
You can access the registers of which address ranges can be configured in the firmware.
AXI protocol from the secondary AXI Chip2Chip is translated to a native protocol by the RTL module `AXI_Translator`,
and it is connected to the `RegisterInterface` as shown in the figure.
The wires between them can be identified by the prefix `RI_`.

<div align="center">
    <img src="../figures/register-interface.png" width="500px">
</div>

Landscape view of `design_1.bd` is shown in [`figures/FPGA_design_1.pdf`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/figures/FPGA_design_1.pdf).<br>
Landscape view of `design_2.bd` is shown in [`figures/FPGA_design_2.pdf`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/figures/FPGA_design_2.pdf).

## Register Interface
Registers are instantiated in this module and they are controlled by the `AXI_Translator`, as stated above.
The register space is 32 bit width, and has depth of 2^16 (the address is 16 bit width).

Address ranges of the registers are defined in the verilog header `RI_addr_defs.v`.

```verilog
parameter DEPTH_RW = 100;
parameter DEPTH_RO = 100;
parameter BASE_ADDRESS_RW = 16'h0000;
parameter HIGH_ADDRESS_RW = 16'h1fff;
parameter BASE_ADDRESS_RO = 16'h2000;
parameter HIGH_ADDRESS_RO = 16'h3fff;
```

The r/w range starts at `BASE_ADDRESS_RW` and ends at `HIGH_ADDRESS_RW`.
`DEPTH_RW` defines the actual number of r/w registers to be instantiated.
For the design of the repository, r/w registers are instantiated from the address 16'h0000 to 16'h0063.
Please note that the number of actual instantiated registers are independent of the high address.

The same is true for the r/o registers. Parameters for them can be identified  by the suffix `_RO`

<div align="center">
    <img src="../figures/address_reservation.png" width="800px">
</div>

<div align="center">
    <img src="../figures/address_instantiation.png" width="800px">
</div>

By the way, r/o registers is read only **from the view point of MPSoC**.
The value of the r/o registers is rewritable by the FPGA logic.
You can connect your module regsiters to the r/o registers via the `register_ro_in` port of the module.

On the other hand, the value of r/w registers is not rewritable by the FPGA logic, but is by MPSoC.
The value of r/w registers is readable from either FPGA logic or MPSoC.
You can refer the value stored in the r/w register via `register_rw_out` port of the module.

From the viewpoint of the MPSoC PS, these register addressed are offsets to the AXI Chip2Chip address.
For example, when you access the register of address 16'h0063, and assuming the AXI Chip2Chip address is 32'h8003_0000,
you should access the address 32'h8003_0063 from the MPSoC.

The reason why `RegisterInterface` is separated from the `AXI_Translator` is to instantiate multiple `RegisterInterface` modules,
and integrate them to the HDL-based design.
For reference, the register control system of the Endcap SL firmware is designed as shown in the figure.

<div align="center">
    <img src="../figures/endcap-control.png" width="1200px">
</div>

If you multiply the `RegisterInterface`, define the addresses for each of the instances.
RI_mode, RI_address, RI_avalid, RI_wdata, and RI_wvalid can be broadcasted via the bus connection.
RI_rdata and RI_rvalid must be duplicated for each instance.

## HDL wrapper
The top module is described in `top.sv`, and this is a wrapper of the block design.
You can select which block design is to be instantiated by commenting out one of them.

<details><summary>monologue</summary><div>
The instantiation switch by the comment-out is not smart.
I hope I could switch this with conditional generate like:

```verilog
generate 
  if (***) begin
      ***
  else begin
      ***
  end
endgenerate
```

However, when I tried this, I got a critical warning [Designutils 20-1280] while running Design Initialization.
The problem is that XDC file of IP module is not read for any cell.
How can I fix this? Or is it allowed to suppress it?
</div></details>

## constraints file
The constraints file is `constraints.xdc`.
Device pin constraints and clock timing constraints are described in it.
