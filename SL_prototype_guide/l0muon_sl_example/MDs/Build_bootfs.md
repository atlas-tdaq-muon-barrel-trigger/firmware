
# L0Muon SL example /Build bootfs

- [Create a PetaLinux project](#create-a-petalinux-project)
- [Configure the project](#configure-the-project)
- [Specify IP and MAC address](#specify-ip-and-mac-address)
- [Edit devicetree](#edit-devicetree)
- [Build the project](#build-the-project)
- [Prepare a microSD](#prepare-a-microsd)


## Create a PetaLinux project
BSP file of the PetaLinux (2020.2) project is prepared
([`PetaLinux/ZynqUltraScalePlus_L0MuonSL.bsp`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/PetaLinux/ZynqUltraScalePlus_L0MuonSL.bsp)),
so you can create a PetaLinux project with it.

```bash
$ petalinux-create --type project -s <path-to-bsp-file>.bsp
```

If you like to create the project from scratch, just execute the command:

```bash
$ petalinux-create --type project --name <project_name> --template zynqMP
```

## Configure the project
Assuming a XSA file of the MPSoC firmware is already exported.
In the PetaLinux project directory, configure the project specifying the XSA:

```bash
$ petalinux-config --get-hw-description=<path to a direcory in which an XSA file exists>/
```

Configure as below in the GUI wizard.

```
Subsystem AUTO Hardware Settings ---> SD/SDIO Settings  ---> Primary SD/SDIO (psu_sd_1)
Subsystem AUTO Hardware Settings ---> Advanced bootable images storage Settings ---> boot image settings            ---> image storage media (primary sd)
                                                                                ---> u-boot env partition settings  ---> image storage media (primary sd)
                                                                                ---> kernel image settings          ---> image storage media (primary sd)
                                                                                ---> jffs2 rootfs image settings    ---> image storage media (primary flash)
                                                                                ---> dtb image settings             ---> image storage media (from boot image)
DTG Settings ---> Kernel Bootargs ---> [ ] generate boot args automatically
                                       [ ] enable kernel earlyprintk
                                        (console=ttyPS0,115200 clk_ignore_unused root=/dev/mmcblk1p2 rootfstype=ext4 rw rootwait earlyprintk)
Image Packaging Configuration ---> Root filesystem type (EXT4 (SD/eMMC/SATA/USB))
                              ---> (/dev/mmcblk1p2) Device node of SD device
```

Also, we need to configure I2C device drivers.

```bash
$ petalinux-config -c u-boot
```

Configure as below.

```
Networking support ---> [ ] Random ethaddr if unset
```

And as the figure below.

<div align="center">
    <img src="../figures/i2c-support.png" width="600px">
</div>


Further, we need to allow the Linux to use UIO.

```bash
$ petalinux-config -c kernel
```

Configure as below.

```
CPU Power Management ---> CPU Idle ---> [ ] CPU idle PM suppor
```

And as the figure below.

<div align="center">
    <img src="../figures/userspace-io-drivers.png" width="600px">
</div>


## Specify IP and MAC address
We adopt the boot sequence as shown in the figure.

<div align="center">
    <img src="../figures/boot-sequence.png" width="1200px">
</div>

Environmental variables such as IP address and MAC address are specified in the `uEnv.txt`.
(Refer to [`example_files/bootfs/uEnv.txt`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/bootfs/uEnv.txt))

Also, we need to modify `boot.scr` so that U-Boot referes to `uEnv.txt`.
In order to alter the `boot.scr`, edit `<path-to-project>/project-spec/meta-user/recipes-bsp/u-boot/u-boot-zynq-scr/boot.cmd.default.initrd` 
as it is in [`example_files/petalinux/boot.cmd.default.initrd`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/petalinux/boot.cmd.default.initrd).
(Or you can just copy the file into the directory of your PetaLinux project)

## Edit devicetree
Add the lines below to the user-editable devicetree source in order to disable the write protection of the SD1 device.

```
/* SD1 with level shifter */
&sdhci1 {
	status = "okay";
	no-1-8-v;	/* for 1.0 silicon */
	disable-wp;
	xlnx,mio_bank = <1>;
};

```

Also, we need to edit a devicetree file so that Linux can detect and handle I2C devices on the hardware.
In general, you have to know I2C device addresses, referring to datasheets or schematics.
As for prototype 1, the address of the I2C MUX is `0x70`.

So, add the lines below to the devicetree source.

```
&i2c0 {
    i2c-mux@70 { /* U23 */
        compatible = "nxp,pca9548";
        #address-cells = <1>;
        #size-cells = <0>;
        reg = <0x70>;
        i2c@3 {
            #address-cells = <1>;
            #size-cells = <0>;
            reg = <3>;
            si5345b: clock-generator@68 { /* U1 */
                compatible = "silabs,si5345";
                reg = <0x68>;
                #clock-cells = <2>;
                clocks = <40000000>; // 40 MHz
                clock-names = "in0";
                out@0 { reg = <0>; };
                out@1 { reg = <1>; };
                out@2 { reg = <2>; };
                out@3 { reg = <3>; };
                out@4 { reg = <4>; };
                out@5 { reg = <5>; };
                out@6 { reg = <6>; };
                out@7 { reg = <7>; };
                out@8 { reg = <8>; };
            };
        };
        i2c@4 {
            #address-cells = <1>;
            #size-cells = <0>;
            reg = <4>;
            si5344b: clock-generator@68 { /* U2 */
                compatible = "silabs,si5344";
                reg = <0x68>;
                #clock-cells = <2>;
                clocks = <40000000>; // 40 MHz
                clock-names = "in0";
                out@0 { reg = <0>; };
                out@1 { reg = <1>; };
                out@2 { reg = <2>; };
                out@3 { reg = <3>; };
            };
        };
        /* port 7 unconnected */
    };
};

```

Further, we need to add the lines below into the devicetree source
so that Linux can detect debug bridges in Userspace I/O (UIO).
Here the node name &debug_bridge_PL and &JTAG_bridge_XCVU13P should be consistent with the names of the IP blocks in the block design.

```
// Allow the Debug Bridge to communicate in the Linux uio space.
&debug_bridge_PL {
    compatible = "generic-uio";
};

&JTAG_bridge_XCVU13P {
    compatible = "generic-uio";
};
```

In this repository, example devicetree files are prepared in [`example_files/petalinux/devicetree/`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/tree/master/example_files/petalinux/devicetree).
If you like it, just copy these files into `<path-to-project>/project-spec/meta-user/recipes-bsp/device-tree/files/`.

## Build the project
After the procedures stated above, build the project.

```bash
$ petalinux-build
```
This takes time. When finished, all the outputs are generated in `<path-to-project>/images/linux/`.
The kernel image `Image` and the devicetree blob `system.dtb` are automatically packaged into the kernel image `image.ub`.
We need to package the boot image `BOOT.BIN` manually.
`BOOT.BIN` will consists of the FSBL `zynqmp_fsbl.elf`, the PMU firmware `pmufw.elf`, the Arm Trusted Firmware `bl31.elf`, 
the U-Boot `u-boot.elf`, and the FPGA bitstream file `system.bit`. We can package `BOOT.BIN` by this command.

```bash
$ cd images/linux
$ petalinux-package --boot --fsbl zynqmp_fsbl.elf --fpga system.bit --pmufw pmufw.elf --u-boot --force

```

The `--force` option is needed to overwirte a former `BOOT.BIN` and not needed for the first time.
The directory `<path-to-project>/images/linux/` has also `boot.scr` which is a script for how U-Boot will be executed.
Preparing boot files is done now.

Once you successfully make the boot files, you can bypass most of the procedures above after the second time since the PetaLinux settings are saved.
If you changed the XSA file, just execute this:

```bash
$ petalinux-config --get-hw-description=<path to a direcory in which an XSA file exists>/
    # close the GUI wizard without doing anything
$ petalinux-build
$ cd images/linux
$ petalinux-package --boot --fsbl zynqmp_fsbl.elf --fpga system.bit --pmufw pmufw.elf --u-boot --force
```


## Prepare a microSD
Xilinx offers a standard way to prepare a bootable SD card [here](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841655/Prepare+Boot+Medium#PrepareBootMedium-TaskDescription) and we will follow it.
We will format and devide it into 2 partitions.
The first partition formatted FAT32 will be dedicated to be a boot partition,
and the second partition formatted EXT4 will hold Linux root filesystem (rootfs).
We will have the first partiton be 200 MB for now and the second be the left.

First insert your microSD into your Ubuntu machine, and identify its device file named by Ubuntu with this command.

```bash
$ dmesg | tail
[1200944.639088] Lockdown: systemd-logind: hibernation is restricted; see man kernel_lockdown.7
[1200998.472740] mmc0: new ultra high speed SDR104 SDHC card at address aaaa
[1200998.543530] mmcblk0: mmc0:aaaa SB16G 14.8 GiB
[1200998.565693]  mmcblk0: p1
[1201028.341264] [UFW BLOCK] IN=wlo1 OUT= MAC=01:00:5e:00:00:01:e0:cb:bc:7d:4e:68:08:00 SRC=0.0.0.0 DST=224.0.0.1 LEN=32 TOS=0x00 PREC=0xC0 TTL=1 ID=3888 PROTO=2
[1201158.183828] [UFW BLOCK] IN=wlo1 OUT= MAC=01:00:5e:00:00:01:e0:cb:bc:7d:4e:68:08:00 SRC=0.0.0.0 DST=224.0.0.1 LEN=32 TOS=0x00 PREC=0xC0 TTL=1 ID=3890 PROTO=2
[1201159.662848] mmc0: card aaaa removed
[1201174.774656] mmc0: new ultra high speed SDR104 SDHC card at address aaaa
[1201174.775256] mmcblk0: mmc0:aaaa SB16G 14.8 GiB (ro)
[1201174.795557]  mmcblk0: p1

$ ls /dev/mmcblk0 # tab
mmcblk0    mmcblk0p1
```

If you insert your microSD directly, it should be detected as `/dev/mmcblkX` where `X` is a number.
Or if you use a microSD-USB converter, it should be detected as `/dev/sdX` where `X` is an alphabet.
Please carefully identify the device because if you pick up a wrong storage, all the data in it will be deleted.

**I'll use `/dev/mmcblkX` to refer to the microSD device in this tutorial.**
Make sure all the partitions of your SD are unmounted using `umount` command before you go.

First erase the first sector.

```bash
$ sudo dd if=/dev/zero of=/dev/mmcblkX bs=1024 count=1
```

Format the disk.

```bash
$ sudo fdisk /dev/mmcblkX
```

```bash
Command (m for help): x
Expert command (m for help): h
Number of heads (1-256, default 30): 255
Expert command (m for help): s
Number of sectors (1-63, default 29): 63
Expert command (m for help): c
Number of cylinders (1-1048576, default 2286): # Just hit enter
Expert command (m for help): r


Command (m for help): n
Partition type:
 p primary (0 primary, 0 extended, 4 free)
 e extended
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-15759359, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-15759359, default 15759359): +200M

Command (m for help): n
Partition type:
 p primary (1 primary, 0 extended, 3 free)
 e extended
Select (default p): p
Partition number (1-4, default 2): 2
First sector (411648-15759359, default 411648):
Using default value 411648
Last sector, +sectors or +size{K,M,G} (411648-15759359, default 15759359):
Using default value 15759359

Command (m for help): a
Partition number (1-4): 1

Command (m for help): t
Partition number (1-4): 1
Hex code (type L to list codes): c
Changed system type of partition 1 to c (W95 FAT32 (LBA))

Command (m for help): t
Partition number (1-4): 2
Hex code (type L to list codes): 83


Command (m for help): p

Disk /dev/mmcblkX: 8068 MB, 8068792320 bytes
249 heads, 62 sectors/track, 1020 cylinders, total 15759360 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x920c958b

 Device Boot Start End Blocks Id System
/dev/mmcblkXp1 * 2048 411647 204800 c W95 FAT32 (LBA)
/dev/mmcblkXp2 411648 15759359 7673856 83 Linux

Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.

WARNING: If you have created or modified any DOS 6.x
partitions, please see the fdisk manual page for additional
information.
Syncing disks.
```

Create new filesystems on the new partitons.

```bash
$ sudo mkfs.vfat -F 32 -n BOOTFS /dev/mmcblkXp1
$ sudo mkfs.ext4 -L ROOTFS /dev/mmcblkXp2
```

`BOOTFS` and `ROOTFS` are partiton labels.

Copy the bootfiles generated by PetaLinux into our new first partition.

```bash
$ sudo mount /dev/mmcblkXp1 /mnt/
$ sudo cd <path to PetaLinux project>/images/linux/
$ sudo cp BOOT.BIN image.ub boot.scr /mnt/
$ sudo umount /dev/mmcblkXp1
```

Download `CentOS-7-aarch64-rootfs-7.4.1708.tar.xz` from a [mirror site](https://ftp.jaist.ac.jp/pub/Linux/CentOS-vault/altarch/7.4.1708/isos/aarch64/), and extract it into the second partition.

```bash
$ sudo mount /dev/mmcblkXp2 /mnt
$ sudo xz -dv ~/Downloads/CentOS-7-aarch64-rootfs-7.4.1708.tar.xz
$ sudo tar xvf ~/Downloads/CentOS-7-aarch64-rootfs-7.4.1708.tar -C /mnt
$ sudo umount /dev/mmcblkXp2
```

Now your bootable microSD is ready, and safely remove it from the PC.
