# L0Muon SL example /Quick Start Guide

- [Reuqest permission for laser room entrance](#enter-laser-room)
- [Power up the server](#power-up-the-server)
- [Access the server](#access-the-umass-server)
- [Add new user to umass server](#add-new-user-umass)
- [Access the shelf](#access-the-shelf)
- [Power up the board](#power-up-the-board)
- [Login MPSoC](#login-mpsoc)
- [Configure the jitter cleaner](#configure-the-jitter-cleaner)
- [Open Vivado](#open-vivado)
- [Set up XVC server](#set-up-xvc-server)
- [Change bootfs](#change-bootfs)
- [Use devmem application](#use-devmem-application)
- [Control registers in the FPGA via AXI Chip2Chip](#control-registers-in-the-fpga-via-axi-chip2chip)
- [Access the IPMC](#access-the-ipmc)

## Enter the laser room
To enter the laser room you must have requested the entrance permit on [ADaMS](https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:1:4546154023779:::::).
The room code is 2175-R-B03 and is sited at P1. The room door has an electronic lock.
Thus, the room has to be entered using your badge that has to be previously badged in a validation station.
You can find a list of all the validation stations [here](https://cern.service-now.com/service-portal?id=kb_article&n=KB0004041).
The procedure has to be repeated every thirty days, as the validation expires.


## Power up the server
The server to be used in the laser room is inside the rack with blue doors. It is labelled NSW 2u PC SVR.
It is powered via a cable connected to a switch on the well where the entrance door is sited.
Switch on both the switch on the wall and the one on the rack. Wait for a few minutes, the process might take time.

## Access the umass server
The NSW PC server (umass2021s1n1) is running in the shelf, it is connected to the CERN GPN and has a local network.
The SL board is connected to the local network, so you need to login on umass2021s1n1 via the CERN GPN.
To do so, execute the following:

```bash
$ ssh <CERN_account_name>@lxplus.cern.ch  # needed only If you are out of the GPN
$ ssh <user_name>@umass2021s1n1.cern.ch   # access the umass server
```
No password will be asked to access the umass server if you connect from lxplus (otherwise if you are in the CERN GPN you have to put your cern account pwd).

## Add new user to umass server
To add a new user to the umass server, first check with the [laser-room-test-mattermost-channel](https://mattermost.web.cern.ch/signup_user_complete/?id=wnq5y5ojktdxpbstjuyqz88c7a&md=link&sbr=su).
Then, ask to a Elena Pompa Pacchi (epompapa) to add you, at the moment she is the only sudoer of the RM1 group. She will perform the following

```bash
$ ssh <CERN_account_name>@lxplus.cern.ch  # needed only If you are out of the GPN
$ ssh <user_name>@umass2021s1n1.cern.ch   # access the umass server
$ sudo addusercern <username> # add a new cern user, their home on umass will be their AFS
```

## Power up the board
The SL board power supply can be controlled from ATCA shelf manager, whose IP is 192.168.0.140.
After having accessed the umass server as explained previously, you can access the shelf managaer.

```bash
$ ssh root@192.168.0.140 # access the shelf managaer
```

You will be asked for a password, but just press Enter without any.<br>
After successfully logging in, check the slot number where the SL board is plugged in, and the status of the power supply:

```bash
$ clia board
```

<table><tr><td>
    <img src="../figures/clia_board.png" width="1200px">
</td></tr></table>

In this screenshot case, the SL board is located in the slot 1, and is active.<br>
When you activate or deactivate the board, execute the following command:

```bash
$ clia activate board <slot_number> 
$ clia deactivate board <slot_number>
```

## Login MPSoC
After the board is powered up, login the CentOS running on the MPSoC via umass2021s1n1, whose IP is 192.168.0.200.
If it is your first login, login as the root user (the password is "centos") and execute the following:

```bash
$ ssh root@192.168.0.200 # at first login access the MPSoC as root
```

To add you (or any other user) execute the following, after having logged in as root:

```bash
$ adduser <user_name>  # make a user
$ passwd <user_name> <user_passwd>  # set the user password
$ usermod -aG wheel <user_name>  # give the user sudo access
```

Then, logout and login again with your account:

```bash
$ ssh <user_name>@192.168.0.200 # at following logins access the MPSoC using your account
```

## Configure the jitter cleaner
There are two jitter cleaners in the SL board: the U1 jitter cleaner (Si5345) and the U2 jitter cleaner (Si5344). 
The listed instructions refer to the U1 jitter cleaner. 
To configure the U1 jitter cleaner, first of all, make a C header file for the configuration with Clock Builder Pro (this softwore can be installed only on Windows...)<br>
An example header file is prepared in
[`example_files/si5345/Si5345-RevD-OUT23-125M-Registers.h`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/si5345/Si5345-RevD-OUT23-125M-Registers.h).<br>
With this example file, OUT2 and OUT3 of the Si5345 are configured with 125 MHz. 
If you are to access the AXI Chip2Chip with this repository design, you should use this example file so that the reference clocks of the transceivers are properly supplied.

The configuration procedure is described in [`example_files/si5345/si5345.c`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/si5345/si5345.c).
In the code, including statement of the header is hard-coded, so edit it depending on your header file name.

```diff
- #include "Si5345-RevD-OUT23-125M-Registers.h"
+ #include "<header name>.h"
```

Makefile([`example_files/si5345/Makefile`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/si5345/Makefile)) will help in compiling the code.

After you successfully compile the code, execute the app on the MPSoC to configure the Si5345:

```bash
$ sudo ./si5345 --write-all #program the U1 jitter cleaner
```

If you would like to configure the U2 jitter cleaner instead, you would need to create a new C header file with Clock Builder Pro.
Then you would need to edit [example_file/si5345/si5345.c](https://gitlab.cern.ch/utokyo-miyolab/amishima/l0muon_sl_example/-/blob/master/example_files/si5345/si5345.c).
You should change the header file name included in L17 with the new header name (as stated above).
Additionally, you would need to change the device name in L20 from "/dev/i2c-4" to "/dev/i2c-5".
Then, you can program the jitter cleaner as explained above.

## Open Vivado

The umass2021s1n1 has Vivado Lab and we can use it.
To use it, open the hardware manager of Vivado in the local network of umass2021s1n1 (after having followed the procedure in [Access the umass server](#access-the-umass-server)).

```bash
$ source /opt/Xilinx/Vivado_Lab/2018.3/settings64.sh
$ vivado_lab
```


## Set up XVC server
When you access the debug_hub in the FPGA or the MPSoC PL, the Xilinx Virtual Cable - XVC - is useful.

An example code for XVC is prepared in 
[`example_files/xvc/xvcserver.c`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/xvc/xvcserver.c).
In the example code, `UIO_PATH` is defined as `/dev/uio0`. This is for the debug_hub in the FPGA.
If you would like to get an application for the debug_hub in the MPSoC PL, you have to edit it as follows:
```diff
- #define UIO_PATH "/dev/uio0"
+ #define UIO_PATH "/dev/uio1"
```

Compile the code and execute the application to set up the XVC server:

```bash
$ gcc -o xvcserver xvcserver.c
$ sudo ./xvcserver
```

In the current rootfs on the SD card, XVC applications are located in the `/usr/bin/` so that we can set up the server
by just executing the following command:

```bash
$ sudo xvcserver0  # XVC for the FPGA debug_hub
$ sudo xvcserver1  # XVC for the MPSoC PL debug_hub
```

After having setup Vivado as explained above (Refer to [Open Vivado](#open-vivado)), follow the procedure to access the MPSoC using xvc:

```
press "Open target" --->
press "Open New Target" --->
press "Next" --->
choose "Local server" and press "Next" --->
press "Add Xilinx Virtual Cable" --->
set Host name "192.168.0.200", set Port number "2542", and press "OK"
```

Now, the device should be detected.


## Change bootfs
First of all, make the boot files (`BOOT.BIN`, `image.ub`, `boot.scr`, `uEnv.txt`).
Example boot files are prepared in [`example_files/bootfs/`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/tree/master/example_files/bootfs).

Mount the SD card, copy the boot files into it, and unmount it. Then, boot the CentOS on the MPSoC with the SD card.
All of these procedures can be performed on the running MPSoC:

```bash
$ sudo mount /dev/mmcblk1p1 /mnt
$ sudo cp BOOT.BIN image.ub boot.scr uEnv.txt /mnt
$ sudo umount /dev/mmcblk1p1
$ sudo reboot

```

In the current test bench, often (or maybe always) boot fails when execute the reboot command somehow.
If you cannot login the MPSoC after rebooting and waiting for a while, 
performing a power cycle will be help (Refer to [Power up the board](#power-up-the-board)).

## Use devmem application
In the MPSoC design in this repository, PL modules can be accessed by opening the device character `/dev/mem`.
An example application to access the device file
([`example_files/devmem/devmem.c`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/example_files/devmem/devmem.c))
is prepared.
If you like, you can use it.

```bash
$ gcc -o devmem devmem.c
$ sudo ./devmem -w 0x<wirte_address> 0x<write_data>  # write data
$ sudo ./devmem -r 0x<read_address>  # read data
```

## Control registers in the FPGA via AXI Chip2Chip
After booting the MPSoC PS, configuring the Si5345 so that reference clocks for the transceivers are properly supplied,
and configuring MPSoC PL and FPGA, you can access registers in the FPGA from the MPSoC via AXI Chip2Chip.

As for the example design in this repository, you must initialize the PMA of the MPSoC GTH before using the AXI Chip2Chip.
`pma_init_generator` performs the PMA reset.

```bash
$ sudo ./devmem -w 0x80070000 0x1  # Assert PMA reset
$ sudo ./devmem -w 0x80070000 0x0  # Deassert PMA reset
```

(Refer to `AXI Chip2Chip` and `Address Map` in [`MPSoC_firmware.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/MPSoC_firmware.md))

Now you can access the address range of the AXI Chip2Chip. For example:

```bash
$ sudo ./devmem -w 0x80030000 0xdeadbeef
$ sudo ./devmem -r 0x80030000
```

In the example design, two red LEDs on the board are connected to the LSBs of registers of address 0x80030000 and 0x80030001 respectively via the FPGA GPIO,
so you can know if the data reaches the FPGA registers by checking them.
Or, of course you can also validate the chip2chip path by using ILA or VIO.


## Access the IPMC
You can access the IPMC from the local network of the umass2021s1n1.
IPMC doesn't have OS, so you cannnot ssh to it.
You can list the sensors for example with a software `ipmitool`.
(The tool is already installed in the umass2021s1n1)

```bash
$ ipmitool -I lan -H 192.168.0.34 -U "" -P "" sdr
```
