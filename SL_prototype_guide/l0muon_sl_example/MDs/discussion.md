
# L0Muon SL example /discussion

```diff
- 🚧 This md is under construction.
```


- [Ethernet configuration](#ethernet-configuration)
- [FPGA configuration](#fpga-configuration)

## Ethernet configuration
Ethernet configuration of the MPSoC is supposed to be done in the boot process.
The IP address is specified in `uEnv.txt`.
(Refer to `Specify IP and MAC address` in [`Build_bootfs.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/Build_bootfs.md))

In the test bench at Laser Room, the Ethernet configuration did not succeed somehow,
though the same desgin worked in the test bench at KEK in Japan.

As a workaround, I logged in the MPSoC via the UART and manually configured the Ethernet settings 
by NetworkManager Command Line Interface (nmcli).

I'm investigating the cause now...

## FPGA configuration
To access debug_hub in the FPGA and MPSoC PL, we have to open the hardware manager of Vivado in the network in which the SL board is located.
Fortunately, in the current test bench, Vivado Lab is installed in the umass2021s1n1, and we have their permission to take advantage of it.

But it doesn't always work like this, so establishing an alternative method for FPGA configuration would be worthwhile.
Using SVF would be one of the solutions, and I'm now taking care of it.
