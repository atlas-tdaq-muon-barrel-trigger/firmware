
#-----------------------------------------------------------------------------------
# device pin constraints
#-----------------------------------------------------------------------------------

set_property PACKAGE_PIN R33 [get_ports FPOSCCLN]
set_property IOSTANDARD LVDS [get_ports FPOSCCLN]
set_property PACKAGE_PIN R32 [get_ports FPOSCCLP]
set_property IOSTANDARD LVDS [get_ports FPOSCCLP]

set_property PACKAGE_PIN AV26 [get_ports FPGARSTB]
set_property IOSTANDARD LVCMOS18 [get_ports FPGARSTB]

set_property PACKAGE_PIN H40 [get_ports GTYCLN7]
set_property PACKAGE_PIN H39 [get_ports GTYCLP7]

set_property PACKAGE_PIN E32 [get_ports bank135_ch1_gtyrxp_in]
set_property PACKAGE_PIN E33 [get_ports bank135_ch1_gtyrxn_in]
set_property PACKAGE_PIN E37 [get_ports bank135_ch1_gtytxp_out]
set_property PACKAGE_PIN E38 [get_ports bank135_ch1_gtytxn_out]

#set_property PACKAGE_PIN F34 [get_ports bank135_ch2_gtyrxp_in]
#set_property PACKAGE_PIN F35 [get_ports bank135_ch2_gtyrxn_in]
#set_property PACKAGE_PIN F39 [get_ports bank135_ch2_gtytxp_out]
#set_property PACKAGE_PIN F40 [get_ports bank135_ch2_gtytxn_out]

set_property PACKAGE_PIN BA23 [get_ports SW1[0]]
set_property IOSTANDARD LVCMOS18 [get_ports SW1[0]]
set_property PACKAGE_PIN AY23 [get_ports SW1[1]]
set_property IOSTANDARD LVCMOS18 [get_ports SW1[1]]
set_property PACKAGE_PIN AW23 [get_ports SW1[2]]
set_property IOSTANDARD LVCMOS18 [get_ports SW1[2]]
set_property PACKAGE_PIN AV24 [get_ports SW1[3]]
set_property IOSTANDARD LVCMOS18 [get_ports SW1[3]]

set_property PACKAGE_PIN BC22 [get_ports DBGLED0]
set_property IOSTANDARD LVCMOS18 [get_ports DBGLED0]
set_property PACKAGE_PIN BB22 [get_ports DBGLED1]
set_property IOSTANDARD LVCMOS18 [get_ports DBGLED1]

#-----------------------------------------------------------------------------------
# clock constraints
#-----------------------------------------------------------------------------------

create_clock -period 8.000 -name GTYCLN7 [get_ports GTYCLN7]
