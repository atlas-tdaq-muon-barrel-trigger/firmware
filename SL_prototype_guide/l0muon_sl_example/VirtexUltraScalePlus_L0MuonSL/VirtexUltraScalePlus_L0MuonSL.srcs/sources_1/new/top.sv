`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/30/2022 04:00:44 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`ifndef DEF_PARAM
    `include "parameters.v"
    `define DEF_PARAM
`endif

module top(
    input wire FPOSCCLN,
    input wire FPOSCCLP,
    input wire FPGARSTB,
    input wire GTYCLN7,
    input wire GTYCLP7,
    output wire bank135_ch1_gtytxn_out,
    output wire bank135_ch1_gtytxp_out,
    input wire bank135_ch1_gtyrxn_in,
    input wire bank135_ch1_gtyrxp_in,
    input wire [3:0] SW1,
    output wire DBGLED0,
    output wire DBGLED1
    );
    
    wire CLK125;
    wire CLK125_locked;
    
    clk_wiz_200Min_125Mout clk_wiz_200Min_125Mout (
        .clk_out1 (CLK125),
        .reset (~FPGARSTB),
        .locked (CLK125_locked),
        .clk_in1_p (FPOSCCLP),
        .clk_in1_n (FPOSCCLN)
    );
    
    // design_1 -----------------------------------------------------------------------------------
    /*
    design_1_wrapper design_1_wrapper (
        .CLK125 (CLK125),
        .CLK125_locked (CLK125_locked),
        .FPGARSTB (FPGARSTB),
        .GT_DIFF_REFCLK1_clk_n (GTYCLN7),
        .GT_DIFF_REFCLK1_clk_p (GTYCLP7),
        .GT_SERIAL_F2Z_txn (bank135_ch1_gtytxn_out),
        .GT_SERIAL_F2Z_txp (bank135_ch1_gtytxp_out),
        .GT_SERIAL_Z2F_rxn (bank135_ch1_gtyrxn_in),
        .GT_SERIAL_Z2F_rxp (bank135_ch1_gtyrxp_in)
    );
    
    reg [26:0] counter;
    
    always @(posedge CLK125) begin
        counter++;
    end
    
    assign DBGLED0 = counter[26];
    assign DBGLED1 = CLK125_locked;
    */
    // --------------------------------------------------------------------------------------------
    
    // design_2 -----------------------------------------------------------------------------------
    wire RI_mode;
    wire [15:0] RI_addr;
    wire RI_avalid;
    wire [31:0] RI_rdata;
    wire RI_rvalid;
    wire [31:0] RI_wdata;
    wire RI_wvalid;
    wire [31:0] reg_out [DEPTH_RW-1:0];
    wire [31:0] reg_in [DEPTH_RO-1:0];
            
    design_2_wrapper design_2_wrapper (
        .CLK125 (CLK125),
        .CLK125_locked (CLK125_locked),
        .FPGARSTB (FPGARSTB),
        .GT_DIFF_REFCLK1_clk_n (GTYCLN7),
        .GT_DIFF_REFCLK1_clk_p (GTYCLP7),
        .GT_SERIAL_F2Z_txn (bank135_ch1_gtytxn_out),
        .GT_SERIAL_F2Z_txp (bank135_ch1_gtytxp_out),
        .GT_SERIAL_Z2F_rxn (bank135_ch1_gtyrxn_in),
        .GT_SERIAL_Z2F_rxp (bank135_ch1_gtyrxp_in),
        .RI_mode (RI_mode),
        .RI_addr (RI_addr),
        .RI_avalid (RI_avalid),
        .RI_rdata (RI_rdata),
        .RI_rvalid (RI_rvalid),
        .RI_wdata (RI_wdata),
        .RI_wvalid (RI_wvalid)
    );
            
    RegisterInterface #(
        .DEPTH_RW (DEPTH_RW),
        .DEPTH_RO (DEPTH_RO),
        .BASE_ADDRESS_RW (BASE_ADDRESS_RW),
        .HIGH_ADDRESS_RW (HIGH_ADDRESS_RW),
        .BASE_ADDRESS_RO (BASE_ADDRESS_RO),
        .HIGH_ADDRESS_RO (HIGH_ADDRESS_RO)
    ) RegisterInterface (
        .clk (CLK125),
        .resetn (CLK125_locked),
        .RI_mode (RI_mode),
        .RI_addr (RI_addr),
        .RI_avalid (RI_avalid),
        .RI_wdata (RI_wdata),
        .RI_wvalid (RI_wvalid),
        .RI_rdata (RI_rdata),
        .RI_rvalid (RI_rvalid),
        .register_rw_out (reg_out),
        .register_ro_in (reg_in)
    );
    
    assign reg_in[0][3:0] = SW1[3:0];
            
    assign DBGLED0 = reg_out[0][0];
    assign DBGLED1 = reg_out[1][0];
    // --------------------------------------------------------------------------------------------
    
endmodule
