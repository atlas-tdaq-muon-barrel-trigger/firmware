`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/22/2022 04:39:43 PM
// Design Name: 
// Module Name: parameters
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

    parameter DEPTH_RW = 200;
    parameter DEPTH_RO = 100;
    parameter BASE_ADDRESS_RW = 16'h0000;
    parameter HIGH_ADDRESS_RW = 16'h1fff;
    parameter BASE_ADDRESS_RO = 16'h2000;
    parameter HIGH_ADDRESS_RO = 16'h3fff;
