`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/21/2022 12:16:25 AM
// Design Name: 
// Module Name: RegisterInterface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterInterface #(
    parameter DEPTH_RW = 0,
    parameter DEPTH_RO = 0,
    parameter BASE_ADDRESS_RW = 16'b0,
    parameter HIGH_ADDRESS_RW = 16'b0,
    parameter BASE_ADDRESS_RO = 16'b0,
    parameter HIGH_ADDRESS_RO = 16'b0
    )(
    input wire clk,
    input wire resetn,
    input wire RI_mode,
    input wire [15:0] RI_addr,
    input wire RI_avalid,
    input wire [31:0] RI_wdata,
    input wire RI_wvalid,
    output reg [31:0] RI_rdata,
    output reg RI_rvalid,
    output wire [31:0] register_rw_out [DEPTH_RW-1:0],
    input wire [31:0] register_ro_in [DEPTH_RO-1:0]
    );
    
    reg [31:0] reg_addr;
    
    reg [31:0] register_rw [DEPTH_RW-1:0];
    reg [31:0] register_ro [DEPTH_RO-1:0];
    
    for(genvar gi = 0; gi < DEPTH_RW; gi++) begin
        assign register_rw_out[gi][31:0] = register_rw[gi][31:0];
    end
    for(genvar gi = 0; gi < DEPTH_RO; gi++) begin
        always @(posedge clk) begin
            register_ro[gi][31:0] <= register_ro_in[gi][31:0];
        end
    end
    
    localparam STATE_IDLE  = 2'd0;
    localparam STATE_READ  = 2'd1;
    localparam STATE_WRITE = 2'd2;
    
    reg [1:0] state = STATE_IDLE;
    
    always @(posedge clk) begin
        if (~resetn) begin
            RI_rdata <= 32'b0;
            RI_rvalid <= 1'b0;
            reg_addr <= 32'b0;
        end
        else begin
            case (state)
                STATE_IDLE: begin
                    if (RI_avalid && (RI_addr >= BASE_ADDRESS_RW) && (RI_addr <= HIGH_ADDRESS_RW)) begin
                        if (RI_mode == 1'b0) begin
                            RI_rdata <= register_rw[RI_addr-BASE_ADDRESS_RW];
                            RI_rvalid <= 1'b1;
                            reg_addr <= RI_addr;
                            state <= STATE_IDLE;
                        end
                        else if (RI_mode == 1'b1) begin
                            RI_rdata <= 32'b0;
                            RI_rvalid <= 1'b0;
                            reg_addr <= RI_addr;
                            state <= STATE_WRITE;
                        end
                    end
                    else if (RI_avalid && (RI_addr >= BASE_ADDRESS_RO) && (RI_addr <= HIGH_ADDRESS_RO)) begin
                        if (RI_mode == 1'b0) begin
                            RI_rdata <= register_ro[RI_addr-BASE_ADDRESS_RO];
                            RI_rvalid <= 1'b1;
                            reg_addr <= RI_addr;
                            state <= STATE_IDLE;
                        end
                        else begin
                            RI_rdata <= 32'b0;
                            RI_rvalid <= 1'b0;
                            reg_addr <= 32'b0;
                            state <= STATE_IDLE;
                        end
                    end
                    else begin
                        RI_rdata <= 32'b0;
                        RI_rvalid <= 1'b0;
                        reg_addr <= 32'b0;
                        state <= STATE_IDLE;
                    end
                end
                STATE_WRITE: begin
                    RI_rdata <= 32'b0;
                    RI_rvalid <= 1'b0;
                    reg_addr <= reg_addr;
                    if (RI_wvalid) begin
                        register_rw[reg_addr-BASE_ADDRESS_RW] <= RI_wdata;
                        state <= STATE_IDLE;
                    end
                    else begin
                        state <= STATE_WRITE;
                    end
                end
            endcase
        end
    end
    
    
endmodule
