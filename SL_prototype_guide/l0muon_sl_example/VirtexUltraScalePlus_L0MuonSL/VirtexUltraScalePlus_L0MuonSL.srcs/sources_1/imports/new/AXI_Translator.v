`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/01/2022 04:04:51 PM
// Design Name: 
// Module Name: AXI_Translator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`ifndef DEF_PARAM
    `include "parameters.v"
    `define DEF_PARAM
`endif

module AXI_Translator #(
	parameter integer C_S_AXI_ID_WIDTH = 12,
	parameter integer C_S_AXI_ADDR_WIDTH = 32,
	parameter integer C_S_AXI_DATA_WIDTH = 32
    )(
    input wire clk,
    input wire resetn,
    // AXI Interface ------------------------------------------------
    // AR channel
    input  wire [C_S_AXI_ADDR_WIDTH-1:0] s_axi_araddr,
    input  wire [1:0] s_axi_arburst,
    input  wire [C_S_AXI_ID_WIDTH-1:0] s_axi_arid,
    input  wire [7:0] s_axi_arlen,
    output reg  s_axi_arready,
    input  wire [2:0] s_axi_arsize,
    input  wire s_axi_arvalid,
    // AW channel
    input  wire [C_S_AXI_ADDR_WIDTH-1:0] s_axi_awaddr,
    input  wire [1:0] s_axi_awburst,
    input  wire [C_S_AXI_ID_WIDTH-1:0] s_axi_awid,
    input  wire [7:0] s_axi_awlen,
    output reg  s_axi_awready,
    input  wire [2:0] s_axi_awsize,
    input  wire s_axi_awvalid,
    // B channel
    output reg [C_S_AXI_ID_WIDTH-1:0] s_axi_bid,
    input  wire s_axi_bready,
    output reg  [1:0] s_axi_bresp,
    output reg  s_axi_bvalid,
    // R channel
    output reg  [C_S_AXI_DATA_WIDTH-1:0] s_axi_rdata,
    output reg  [C_S_AXI_ID_WIDTH-1:0] s_axi_rid,
    output reg  s_axi_rlast,
    input  wire s_axi_rready,
    output reg  [1:0] s_axi_rresp,
    output reg  s_axi_rvalid,
    // W channel
    input  wire [C_S_AXI_DATA_WIDTH-1:0] s_axi_wdata,
    input  wire s_axi_wlast,
    output reg  s_axi_wready,
    input  wire [C_S_AXI_DATA_WIDTH/8-1 : 0] s_axi_wstrb,
    input  wire [3:0] s_axi_wuser,
    input  wire s_axi_wvalid,
    // Register Interface -------------------------------------------
    output reg  RI_mode,
    output reg  [15:0] RI_addr,
    output reg  RI_avalid,
    output reg  [31:0] RI_wdata,
    output reg  RI_wvalid,
    input  wire [31:0] RI_rdata,
    input  wire RI_rvalid
    );
    
    wire register_not_found;
    assign register_not_found = !(((s_axi_arvalid) && (({s_axi_araddr[15:12] , 2'b0 , s_axi_araddr[11:2]} >= BASE_ADDRESS_RW) && ({s_axi_araddr[15:12] , 2'b0 , s_axi_araddr[11:2]} <= BASE_ADDRESS_RW + DEPTH_RW - 1) )) ||
                                  ((s_axi_awvalid) && (({s_axi_awaddr[15:12] , 2'b0 , s_axi_awaddr[11:2]} >= BASE_ADDRESS_RW) && ({s_axi_awaddr[15:12] , 2'b0 , s_axi_awaddr[11:2]} <= BASE_ADDRESS_RW + DEPTH_RW - 1) )) ||
                                  ((s_axi_arvalid) && (({s_axi_araddr[15:12] , 2'b0 , s_axi_araddr[11:2]} >= BASE_ADDRESS_RO) && ({s_axi_araddr[15:12] , 2'b0 , s_axi_araddr[11:2]} <= BASE_ADDRESS_RO + DEPTH_RO - 1) )) );

    localparam STATE_IDLE        = 3'b0;
    localparam STATE_READ        = 3'd1;
    localparam STATE_WRITE       = 3'd2;
    localparam STATE_RESP        = 3'd3;
    localparam STATE_READ_ERROR  = 3'd4;
    localparam STATE_WRITE_ERROR = 3'd5;
    localparam STATE_RESP_ERROR  = 3'd6;

    reg [2:0] state = STATE_IDLE;
    
    always @(posedge clk) begin
        if (~resetn) begin
            RI_mode <= 1'b0;
            RI_addr <= 16'b0;
            RI_avalid <= 1'b0;
            RI_wdata <= 32'b0;
            RI_wvalid <= 1'b0;
            state <= STATE_IDLE;
        end
        else begin
            case (state)
                STATE_IDLE: begin
                    if (s_axi_arvalid && s_axi_arready) begin
                        if (register_not_found) begin
                            RI_mode <= 1'b0;
                            RI_addr <= 16'b0;
                            RI_avalid <= 1'b0;
                            RI_wdata <= 32'b0;
                            RI_wvalid <= 1'b0;
                            state <= STATE_READ_ERROR;
                        end
                        else begin
                            RI_mode <= 1'b0;
                            RI_addr <= { s_axi_araddr[15:12] , 2'b0 , s_axi_araddr[11:2] };
                            RI_avalid <= 1'b1;
                            RI_wdata <= 32'b0;
                            RI_wvalid <= 1'b0;
                            state <= STATE_READ;
                        end
                    end
                    else if (s_axi_awvalid && s_axi_awready) begin
                        if (register_not_found) begin
                            RI_mode <= 1'b0;
                            RI_addr <= 16'b0;
                            RI_avalid <= 1'b0;
                            RI_wdata <= 32'b0;
                            RI_wvalid <= 1'b0;
                            state <= STATE_WRITE_ERROR;
                        end
                        else begin
                            RI_mode <= 1'b1;
                            RI_addr <= { s_axi_awaddr[15:12] , 2'b0 , s_axi_awaddr[11:2] };
                            RI_avalid <= 1'b1;
                            RI_wdata <= 32'b0;
                            RI_wvalid <= 1'b0;
                            state <= STATE_WRITE;
                        end
                    end
                    else begin
                        RI_mode <= 1'b0;
                        RI_addr <= 16'b0;
                        RI_avalid <= 1'b0;
                        RI_wdata <= 32'b0;
                        RI_wvalid <= 1'b0;
                        state <= STATE_IDLE;
                    end
                end
                STATE_READ: begin
                    RI_mode <= 1'b0;
                    RI_addr <= 16'b0;
                    RI_avalid <= 1'b0;
                    RI_wdata <= 32'b0;
                    RI_wvalid <= 1'b0;
                    if (s_axi_rvalid && s_axi_rready) begin
                        state <= STATE_IDLE;
                    end
                    else begin
                        state <= STATE_READ;
                    end
                end
                STATE_WRITE: begin
                    RI_mode <= 1'b1;
                    RI_addr <= 16'b0;
                    RI_avalid <= 1'b0;
                    if (s_axi_wvalid && s_axi_wready) begin
                        RI_wdata <= s_axi_wdata;
                        RI_wvalid <= 1'b1;
                        state <= STATE_RESP;
                    end
                    else begin
                        RI_wdata <= 32'b0;
                        RI_wvalid <= 1'b0;
                        state <= STATE_WRITE;
                    end
                end
                STATE_RESP: begin
                    RI_mode <= 1'b1;
                    RI_addr <= 16'b0;
                    RI_avalid <= 1'b0;
                    RI_wdata <= 32'b0;
                    RI_wvalid <= 1'b0;
                    if (s_axi_bvalid && s_axi_bready) begin
                        state <= STATE_IDLE;
                    end
                    else begin
                        state <= STATE_RESP;
                    end
                end
                STATE_READ_ERROR: begin
                    RI_mode <= 1'b0;
                    RI_addr <= 16'b0;
                    RI_avalid <= 1'b0;
                    RI_wdata <= 32'b0;
                    RI_wvalid <= 1'b0;
                    if (s_axi_rvalid && s_axi_rready) begin
                        state <= STATE_IDLE;
                    end
                    else begin
                        state <= STATE_READ_ERROR;
                    end
                end
                STATE_WRITE_ERROR: begin
                    RI_mode <= 1'b0;
                    RI_addr <= 16'b0;
                    RI_avalid <= 1'b0;
                    RI_wdata <= 32'b0;
                    RI_wvalid <= 1'b0;
                    if (s_axi_wvalid && s_axi_wready) begin
                        state <= STATE_RESP_ERROR;
                    end
                    else begin
                        state <= STATE_WRITE_ERROR;
                    end
                end
                STATE_RESP_ERROR: begin
                    RI_mode <= 1'b0;
                    RI_addr <= 16'b0;
                    RI_avalid <= 1'b0;
                    RI_wdata <= 32'b0;
                    RI_wvalid <= 1'b0;
                    if (s_axi_bvalid && s_axi_bready) begin
                        state <= STATE_IDLE;
                    end
                    else begin
                        state <= STATE_RESP_ERROR;
                    end
                end
            endcase
        end
    end
    
    // AR channel
    always @(posedge clk) begin
        if (~resetn) begin
            s_axi_arready <= 1'b0;
        end
        else begin
            if ((state == STATE_IDLE) && s_axi_arvalid && (!s_axi_arready)) begin
                s_axi_arready <= 1'b1;
            end
            else begin
                s_axi_arready <= 1'b0;
            end
        end   
    end
    
    // R channel
    always @(posedge clk) begin
        if (~resetn) begin
            s_axi_rdata <= 'b0;
            s_axi_rid <= 'b0;
            s_axi_rlast <= 1'b0;
            s_axi_rresp <= 2'b0;
            s_axi_rvalid <= 1'b0;
        end
        else begin
            if ((state == STATE_READ_ERROR) && (!s_axi_rvalid)) begin
                s_axi_rdata <= 'b0;
                s_axi_rid <= 'b0;
                s_axi_rlast <= 'b1;
                s_axi_rresp <= 2'b10;
                s_axi_rvalid <= 1'b1;
            end
            else if ((state == STATE_READ) && RI_rvalid && (!s_axi_rvalid)) begin
                s_axi_rdata <= RI_rdata;
                s_axi_rid <= 'b0;
                s_axi_rlast <= 'b1;
                s_axi_rresp <= 2'b0;
                s_axi_rvalid <= 1'b1;
            end
            else if (s_axi_rvalid && (!s_axi_rready)) begin
                s_axi_rdata <= s_axi_rdata;
                s_axi_rid <= s_axi_rid;
                s_axi_rlast <= s_axi_rlast;
                s_axi_rresp <= s_axi_rresp;
                s_axi_rvalid <= s_axi_rvalid;
            end
            else begin
                s_axi_rdata <= 'b0;
                s_axi_rid <= 'b0;
                s_axi_rlast <= 'b0;
                s_axi_rresp <= 2'b0;
                s_axi_rvalid <= 1'b0;
            end
        end   
    end
    
    // AW channel
    always @(posedge clk) begin
        if (~resetn) begin
            s_axi_awready <= 1'b0;
        end
        else begin
            if ((state == STATE_IDLE) && s_axi_awvalid && (!s_axi_awready)) begin
                s_axi_awready <= 1'b1;
            end
            else begin
                s_axi_awready <= 1'b0;
            end
        end
    end
    
    // W channel
    always @(posedge clk) begin
        if (~resetn) begin
            s_axi_wready <= 1'b0;
        end
        else begin
            if (((state == STATE_WRITE) || (state == STATE_WRITE_ERROR)) && s_axi_wvalid && (!s_axi_wready)) begin
                s_axi_wready <= 1'b1;
            end
            else begin
                s_axi_wready <= 1'b0;
            end
        end
    end
    
    // B channel
    always @(posedge clk) begin
        if (~resetn) begin
            s_axi_bid <= 'b0;
            s_axi_bresp <= 2'b0;
            s_axi_bvalid <=  1'b0;
        end
        else begin
            if ((state == STATE_RESP_ERROR) && (!s_axi_bvalid)) begin
                s_axi_bid <= 'b0;
                s_axi_bresp <= 2'b0;
                s_axi_bvalid <=  1'b1;
            end
            else if ((state == STATE_RESP) && (!s_axi_bvalid)) begin
                s_axi_bid <= 'b0;
                s_axi_bresp <= 2'b0;
                s_axi_bvalid <=  1'b1;
            end
            else if (s_axi_bvalid && (!s_axi_bready)) begin
                s_axi_bid <= s_axi_bid;
                s_axi_bresp <= s_axi_bresp;
                s_axi_bvalid <=  s_axi_bvalid;
            end
            else begin
                s_axi_bid <= 'b0;
                s_axi_bresp <= 2'b0;
                s_axi_bvalid <=  1'b0;
            end
        end
    end
    
endmodule
