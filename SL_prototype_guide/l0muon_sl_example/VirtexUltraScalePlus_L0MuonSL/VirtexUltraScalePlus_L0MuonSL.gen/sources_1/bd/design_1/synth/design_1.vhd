--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Sun Oct 30 16:02:14 2022
--Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    CLK200_clk_n : in STD_LOGIC;
    CLK200_clk_p : in STD_LOGIC;
    FPGARSTB : in STD_LOGIC;
    GT_DIFF_REFCLK1_clk_n : in STD_LOGIC;
    GT_DIFF_REFCLK1_clk_p : in STD_LOGIC;
    GT_SERIAL_F2Z_txn : out STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_F2Z_txp : out STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_Z2F_rxn : in STD_LOGIC_VECTOR ( 0 to 0 );
    GT_SERIAL_Z2F_rxp : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=7,numReposBlks=7,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=1,numPkgbdBlks=0,bdsource=USER,da_bram_cntlr_cnt=2,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_axi_chip2chip_0_0 is
  port (
    m_aclk : in STD_LOGIC;
    m_aresetn : in STD_LOGIC;
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awready : in STD_LOGIC;
    m_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_wlast : out STD_LOGIC;
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wready : in STD_LOGIC;
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bready : out STD_LOGIC;
    m_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_arvalid : out STD_LOGIC;
    m_axi_arready : in STD_LOGIC;
    m_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rlast : in STD_LOGIC;
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rready : out STD_LOGIC;
    axi_c2c_s2m_intr_in : in STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_c2c_m2s_intr_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    axi_c2c_phy_clk : in STD_LOGIC;
    axi_c2c_aurora_channel_up : in STD_LOGIC;
    axi_c2c_aurora_tx_tready : in STD_LOGIC;
    axi_c2c_aurora_tx_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    axi_c2c_aurora_tx_tvalid : out STD_LOGIC;
    axi_c2c_aurora_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    axi_c2c_aurora_rx_tvalid : in STD_LOGIC;
    aurora_do_cc : out STD_LOGIC;
    aurora_pma_init_in : in STD_LOGIC;
    aurora_init_clk : in STD_LOGIC;
    aurora_pma_init_out : out STD_LOGIC;
    aurora_mmcm_not_locked : in STD_LOGIC;
    aurora_reset_pb : out STD_LOGIC;
    axi_c2c_config_error_out : out STD_LOGIC;
    axi_c2c_link_status_out : out STD_LOGIC;
    axi_c2c_multi_bit_error_out : out STD_LOGIC
  );
  end component design_1_axi_chip2chip_0_0;
  component design_1_aurora_64b66b_0_0 is
  port (
    rxp : in STD_LOGIC_VECTOR ( 0 to 0 );
    rxn : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_pb : in STD_LOGIC;
    power_down : in STD_LOGIC;
    pma_init : in STD_LOGIC;
    loopback : in STD_LOGIC_VECTOR ( 2 downto 0 );
    txp : out STD_LOGIC_VECTOR ( 0 to 0 );
    txn : out STD_LOGIC_VECTOR ( 0 to 0 );
    hard_err : out STD_LOGIC;
    soft_err : out STD_LOGIC;
    channel_up : out STD_LOGIC;
    lane_up : out STD_LOGIC_VECTOR ( 0 to 0 );
    tx_out_clk : out STD_LOGIC;
    gt_pll_lock : out STD_LOGIC;
    s_axi_tx_tdata : in STD_LOGIC_VECTOR ( 0 to 63 );
    s_axi_tx_tvalid : in STD_LOGIC;
    s_axi_tx_tready : out STD_LOGIC;
    m_axi_rx_tdata : out STD_LOGIC_VECTOR ( 0 to 63 );
    m_axi_rx_tvalid : out STD_LOGIC;
    mmcm_not_locked_out : out STD_LOGIC;
    init_clk : in STD_LOGIC;
    link_reset_out : out STD_LOGIC;
    gt_refclk1_p : in STD_LOGIC;
    gt_refclk1_n : in STD_LOGIC;
    user_clk_out : out STD_LOGIC;
    sync_clk_out : out STD_LOGIC;
    gt_qpllclk_quad1_out : out STD_LOGIC;
    gt_qpllrefclk_quad1_out : out STD_LOGIC;
    gt_qpllrefclklost_quad1_out : out STD_LOGIC;
    gt_qplllock_quad1_out : out STD_LOGIC;
    gt_rxcdrovrden_in : in STD_LOGIC;
    sys_reset_out : out STD_LOGIC;
    gt_reset_out : out STD_LOGIC;
    gt_refclk1_out : out STD_LOGIC;
    gt_powergood : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_aurora_64b66b_0_0;
  component design_1_axi_bram_ctrl_0_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awlock : in STD_LOGIC;
    s_axi_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arlock : in STD_LOGIC;
    s_axi_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    bram_rst_a : out STD_LOGIC;
    bram_clk_a : out STD_LOGIC;
    bram_en_a : out STD_LOGIC;
    bram_we_a : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_addr_a : out STD_LOGIC_VECTOR ( 15 downto 0 );
    bram_wrdata_a : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rddata_a : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rst_b : out STD_LOGIC;
    bram_clk_b : out STD_LOGIC;
    bram_en_b : out STD_LOGIC;
    bram_we_b : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_addr_b : out STD_LOGIC_VECTOR ( 15 downto 0 );
    bram_wrdata_b : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_rddata_b : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component design_1_axi_bram_ctrl_0_0;
  component design_1_clk_wiz_0_0 is
  port (
    clk_in1_p : in STD_LOGIC;
    clk_in1_n : in STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_out1 : out STD_LOGIC;
    locked : out STD_LOGIC
  );
  end component design_1_clk_wiz_0_0;
  component design_1_pma_init_generator_0_0 is
  port (
    aur_init_clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    FPGARSTB : in STD_LOGIC;
    pma_init_in : out STD_LOGIC
  );
  end component design_1_pma_init_generator_0_0;
  component design_1_axi_bram_ctrl_0_bram_0 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC
  );
  end component design_1_axi_bram_ctrl_0_bram_0;
  component design_1_proc_sys_reset_0_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_proc_sys_reset_0_0;
  signal CLK200_1_CLK_N : STD_LOGIC;
  signal CLK200_1_CLK_P : STD_LOGIC;
  signal FPGARSTB_1 : STD_LOGIC;
  signal GT_DIFF_REFCLK1_1_CLK_N : STD_LOGIC;
  signal GT_DIFF_REFCLK1_1_CLK_P : STD_LOGIC;
  signal GT_SERIAL_Z2F_1_RXN : STD_LOGIC_VECTOR ( 0 to 0 );
  signal GT_SERIAL_Z2F_1_RXP : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aurora_64b66b_0_GT_SERIAL_TX_TXN : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aurora_64b66b_0_GT_SERIAL_TX_TXP : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA : STD_LOGIC_VECTOR ( 0 to 63 );
  signal aurora_64b66b_0_USER_DATA_M_AXIS_RX_TVALID : STD_LOGIC;
  signal aurora_64b66b_0_channel_up : STD_LOGIC;
  signal aurora_64b66b_0_gt_reset_out : STD_LOGIC;
  signal aurora_64b66b_0_mmcm_not_locked_out : STD_LOGIC;
  signal aurora_64b66b_0_user_clk_out : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTA_ADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTA_CLK : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTA_DIN : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTA_DOUT : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTA_EN : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTA_RST : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTA_WE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTB_ADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTB_CLK : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTB_DIN : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTB_DOUT : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_bram_ctrl_0_BRAM_PORTB_EN : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTB_RST : STD_LOGIC;
  signal axi_bram_ctrl_0_BRAM_PORTB_WE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_chip2chip_0_AXIS_TX_TDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal axi_chip2chip_0_AXIS_TX_TREADY : STD_LOGIC;
  signal axi_chip2chip_0_AXIS_TX_TVALID : STD_LOGIC;
  signal axi_chip2chip_0_aurora_pma_init_out : STD_LOGIC;
  signal axi_chip2chip_0_aurora_reset_pb : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_ARLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal axi_chip2chip_0_m_axi_ARREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_chip2chip_0_m_axi_ARVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_AWLEN : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal axi_chip2chip_0_m_axi_AWREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_chip2chip_0_m_axi_AWVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_BREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_BVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_RLAST : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_RREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_chip2chip_0_m_axi_RVALID : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_chip2chip_0_m_axi_WLAST : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_WREADY : STD_LOGIC;
  signal axi_chip2chip_0_m_axi_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_chip2chip_0_m_axi_WVALID : STD_LOGIC;
  signal clk_wiz_0_clk_out1 : STD_LOGIC;
  signal clk_wiz_0_locked : STD_LOGIC;
  signal pma_init_generator_0_pma_init_in : STD_LOGIC;
  signal proc_sys_reset_0_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_aurora_64b66b_0_gt_pll_lock_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_gt_qpllclk_quad1_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_gt_qplllock_quad1_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_gt_qpllrefclk_quad1_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_gt_qpllrefclklost_quad1_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_gt_refclk1_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_hard_err_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_link_reset_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_soft_err_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_sync_clk_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_sys_reset_out_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_tx_out_clk_UNCONNECTED : STD_LOGIC;
  signal NLW_aurora_64b66b_0_gt_powergood_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_aurora_64b66b_0_lane_up_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_axi_bram_ctrl_0_bram_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_bram_ctrl_0_bram_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_aurora_do_cc_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_axi_c2c_config_error_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_axi_c2c_link_status_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_axi_c2c_multi_bit_error_out_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_chip2chip_0_axi_c2c_m2s_intr_out_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_proc_sys_reset_0_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_proc_sys_reset_0_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_0_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_proc_sys_reset_0_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of CLK200_clk_n : signal is "xilinx.com:interface:diff_clock:1.0 CLK200 CLK_N";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of CLK200_clk_n : signal is "XIL_INTERFACENAME CLK200, CAN_DEBUG false, FREQ_HZ 200000000";
  attribute X_INTERFACE_INFO of CLK200_clk_p : signal is "xilinx.com:interface:diff_clock:1.0 CLK200 CLK_P";
  attribute X_INTERFACE_INFO of FPGARSTB : signal is "xilinx.com:signal:reset:1.0 RST.FPGARSTB RST";
  attribute X_INTERFACE_PARAMETER of FPGARSTB : signal is "XIL_INTERFACENAME RST.FPGARSTB, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of GT_DIFF_REFCLK1_clk_n : signal is "xilinx.com:interface:diff_clock:1.0 GT_DIFF_REFCLK1 CLK_N";
  attribute X_INTERFACE_PARAMETER of GT_DIFF_REFCLK1_clk_n : signal is "XIL_INTERFACENAME GT_DIFF_REFCLK1, CAN_DEBUG false, FREQ_HZ 125000000";
  attribute X_INTERFACE_INFO of GT_DIFF_REFCLK1_clk_p : signal is "xilinx.com:interface:diff_clock:1.0 GT_DIFF_REFCLK1 CLK_P";
  attribute X_INTERFACE_INFO of GT_SERIAL_F2Z_txn : signal is "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_TX:1.0 GT_SERIAL_F2Z TXN";
  attribute X_INTERFACE_INFO of GT_SERIAL_F2Z_txp : signal is "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_TX:1.0 GT_SERIAL_F2Z TXP";
  attribute X_INTERFACE_INFO of GT_SERIAL_Z2F_rxn : signal is "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_RX:1.0 GT_SERIAL_Z2F RXN";
  attribute X_INTERFACE_INFO of GT_SERIAL_Z2F_rxp : signal is "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_RX:1.0 GT_SERIAL_Z2F RXP";
begin
  CLK200_1_CLK_N <= CLK200_clk_n;
  CLK200_1_CLK_P <= CLK200_clk_p;
  FPGARSTB_1 <= FPGARSTB;
  GT_DIFF_REFCLK1_1_CLK_N <= GT_DIFF_REFCLK1_clk_n;
  GT_DIFF_REFCLK1_1_CLK_P <= GT_DIFF_REFCLK1_clk_p;
  GT_SERIAL_F2Z_txn(0) <= aurora_64b66b_0_GT_SERIAL_TX_TXN(0);
  GT_SERIAL_F2Z_txp(0) <= aurora_64b66b_0_GT_SERIAL_TX_TXP(0);
  GT_SERIAL_Z2F_1_RXN(0) <= GT_SERIAL_Z2F_rxn(0);
  GT_SERIAL_Z2F_1_RXP(0) <= GT_SERIAL_Z2F_rxp(0);
aurora_64b66b_0: component design_1_aurora_64b66b_0_0
     port map (
      channel_up => aurora_64b66b_0_channel_up,
      gt_pll_lock => NLW_aurora_64b66b_0_gt_pll_lock_UNCONNECTED,
      gt_powergood(0) => NLW_aurora_64b66b_0_gt_powergood_UNCONNECTED(0),
      gt_qpllclk_quad1_out => NLW_aurora_64b66b_0_gt_qpllclk_quad1_out_UNCONNECTED,
      gt_qplllock_quad1_out => NLW_aurora_64b66b_0_gt_qplllock_quad1_out_UNCONNECTED,
      gt_qpllrefclk_quad1_out => NLW_aurora_64b66b_0_gt_qpllrefclk_quad1_out_UNCONNECTED,
      gt_qpllrefclklost_quad1_out => NLW_aurora_64b66b_0_gt_qpllrefclklost_quad1_out_UNCONNECTED,
      gt_refclk1_n => GT_DIFF_REFCLK1_1_CLK_N,
      gt_refclk1_out => NLW_aurora_64b66b_0_gt_refclk1_out_UNCONNECTED,
      gt_refclk1_p => GT_DIFF_REFCLK1_1_CLK_P,
      gt_reset_out => aurora_64b66b_0_gt_reset_out,
      gt_rxcdrovrden_in => '0',
      hard_err => NLW_aurora_64b66b_0_hard_err_UNCONNECTED,
      init_clk => clk_wiz_0_clk_out1,
      lane_up(0) => NLW_aurora_64b66b_0_lane_up_UNCONNECTED(0),
      link_reset_out => NLW_aurora_64b66b_0_link_reset_out_UNCONNECTED,
      loopback(2 downto 0) => B"000",
      m_axi_rx_tdata(0 to 63) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(0 to 63),
      m_axi_rx_tvalid => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TVALID,
      mmcm_not_locked_out => aurora_64b66b_0_mmcm_not_locked_out,
      pma_init => axi_chip2chip_0_aurora_pma_init_out,
      power_down => '0',
      reset_pb => axi_chip2chip_0_aurora_reset_pb,
      rxn(0) => GT_SERIAL_Z2F_1_RXN(0),
      rxp(0) => GT_SERIAL_Z2F_1_RXP(0),
      s_axi_tx_tdata(0) => axi_chip2chip_0_AXIS_TX_TDATA(63),
      s_axi_tx_tdata(1) => axi_chip2chip_0_AXIS_TX_TDATA(62),
      s_axi_tx_tdata(2) => axi_chip2chip_0_AXIS_TX_TDATA(61),
      s_axi_tx_tdata(3) => axi_chip2chip_0_AXIS_TX_TDATA(60),
      s_axi_tx_tdata(4) => axi_chip2chip_0_AXIS_TX_TDATA(59),
      s_axi_tx_tdata(5) => axi_chip2chip_0_AXIS_TX_TDATA(58),
      s_axi_tx_tdata(6) => axi_chip2chip_0_AXIS_TX_TDATA(57),
      s_axi_tx_tdata(7) => axi_chip2chip_0_AXIS_TX_TDATA(56),
      s_axi_tx_tdata(8) => axi_chip2chip_0_AXIS_TX_TDATA(55),
      s_axi_tx_tdata(9) => axi_chip2chip_0_AXIS_TX_TDATA(54),
      s_axi_tx_tdata(10) => axi_chip2chip_0_AXIS_TX_TDATA(53),
      s_axi_tx_tdata(11) => axi_chip2chip_0_AXIS_TX_TDATA(52),
      s_axi_tx_tdata(12) => axi_chip2chip_0_AXIS_TX_TDATA(51),
      s_axi_tx_tdata(13) => axi_chip2chip_0_AXIS_TX_TDATA(50),
      s_axi_tx_tdata(14) => axi_chip2chip_0_AXIS_TX_TDATA(49),
      s_axi_tx_tdata(15) => axi_chip2chip_0_AXIS_TX_TDATA(48),
      s_axi_tx_tdata(16) => axi_chip2chip_0_AXIS_TX_TDATA(47),
      s_axi_tx_tdata(17) => axi_chip2chip_0_AXIS_TX_TDATA(46),
      s_axi_tx_tdata(18) => axi_chip2chip_0_AXIS_TX_TDATA(45),
      s_axi_tx_tdata(19) => axi_chip2chip_0_AXIS_TX_TDATA(44),
      s_axi_tx_tdata(20) => axi_chip2chip_0_AXIS_TX_TDATA(43),
      s_axi_tx_tdata(21) => axi_chip2chip_0_AXIS_TX_TDATA(42),
      s_axi_tx_tdata(22) => axi_chip2chip_0_AXIS_TX_TDATA(41),
      s_axi_tx_tdata(23) => axi_chip2chip_0_AXIS_TX_TDATA(40),
      s_axi_tx_tdata(24) => axi_chip2chip_0_AXIS_TX_TDATA(39),
      s_axi_tx_tdata(25) => axi_chip2chip_0_AXIS_TX_TDATA(38),
      s_axi_tx_tdata(26) => axi_chip2chip_0_AXIS_TX_TDATA(37),
      s_axi_tx_tdata(27) => axi_chip2chip_0_AXIS_TX_TDATA(36),
      s_axi_tx_tdata(28) => axi_chip2chip_0_AXIS_TX_TDATA(35),
      s_axi_tx_tdata(29) => axi_chip2chip_0_AXIS_TX_TDATA(34),
      s_axi_tx_tdata(30) => axi_chip2chip_0_AXIS_TX_TDATA(33),
      s_axi_tx_tdata(31) => axi_chip2chip_0_AXIS_TX_TDATA(32),
      s_axi_tx_tdata(32) => axi_chip2chip_0_AXIS_TX_TDATA(31),
      s_axi_tx_tdata(33) => axi_chip2chip_0_AXIS_TX_TDATA(30),
      s_axi_tx_tdata(34) => axi_chip2chip_0_AXIS_TX_TDATA(29),
      s_axi_tx_tdata(35) => axi_chip2chip_0_AXIS_TX_TDATA(28),
      s_axi_tx_tdata(36) => axi_chip2chip_0_AXIS_TX_TDATA(27),
      s_axi_tx_tdata(37) => axi_chip2chip_0_AXIS_TX_TDATA(26),
      s_axi_tx_tdata(38) => axi_chip2chip_0_AXIS_TX_TDATA(25),
      s_axi_tx_tdata(39) => axi_chip2chip_0_AXIS_TX_TDATA(24),
      s_axi_tx_tdata(40) => axi_chip2chip_0_AXIS_TX_TDATA(23),
      s_axi_tx_tdata(41) => axi_chip2chip_0_AXIS_TX_TDATA(22),
      s_axi_tx_tdata(42) => axi_chip2chip_0_AXIS_TX_TDATA(21),
      s_axi_tx_tdata(43) => axi_chip2chip_0_AXIS_TX_TDATA(20),
      s_axi_tx_tdata(44) => axi_chip2chip_0_AXIS_TX_TDATA(19),
      s_axi_tx_tdata(45) => axi_chip2chip_0_AXIS_TX_TDATA(18),
      s_axi_tx_tdata(46) => axi_chip2chip_0_AXIS_TX_TDATA(17),
      s_axi_tx_tdata(47) => axi_chip2chip_0_AXIS_TX_TDATA(16),
      s_axi_tx_tdata(48) => axi_chip2chip_0_AXIS_TX_TDATA(15),
      s_axi_tx_tdata(49) => axi_chip2chip_0_AXIS_TX_TDATA(14),
      s_axi_tx_tdata(50) => axi_chip2chip_0_AXIS_TX_TDATA(13),
      s_axi_tx_tdata(51) => axi_chip2chip_0_AXIS_TX_TDATA(12),
      s_axi_tx_tdata(52) => axi_chip2chip_0_AXIS_TX_TDATA(11),
      s_axi_tx_tdata(53) => axi_chip2chip_0_AXIS_TX_TDATA(10),
      s_axi_tx_tdata(54) => axi_chip2chip_0_AXIS_TX_TDATA(9),
      s_axi_tx_tdata(55) => axi_chip2chip_0_AXIS_TX_TDATA(8),
      s_axi_tx_tdata(56) => axi_chip2chip_0_AXIS_TX_TDATA(7),
      s_axi_tx_tdata(57) => axi_chip2chip_0_AXIS_TX_TDATA(6),
      s_axi_tx_tdata(58) => axi_chip2chip_0_AXIS_TX_TDATA(5),
      s_axi_tx_tdata(59) => axi_chip2chip_0_AXIS_TX_TDATA(4),
      s_axi_tx_tdata(60) => axi_chip2chip_0_AXIS_TX_TDATA(3),
      s_axi_tx_tdata(61) => axi_chip2chip_0_AXIS_TX_TDATA(2),
      s_axi_tx_tdata(62) => axi_chip2chip_0_AXIS_TX_TDATA(1),
      s_axi_tx_tdata(63) => axi_chip2chip_0_AXIS_TX_TDATA(0),
      s_axi_tx_tready => axi_chip2chip_0_AXIS_TX_TREADY,
      s_axi_tx_tvalid => axi_chip2chip_0_AXIS_TX_TVALID,
      soft_err => NLW_aurora_64b66b_0_soft_err_UNCONNECTED,
      sync_clk_out => NLW_aurora_64b66b_0_sync_clk_out_UNCONNECTED,
      sys_reset_out => NLW_aurora_64b66b_0_sys_reset_out_UNCONNECTED,
      tx_out_clk => NLW_aurora_64b66b_0_tx_out_clk_UNCONNECTED,
      txn(0) => aurora_64b66b_0_GT_SERIAL_TX_TXN(0),
      txp(0) => aurora_64b66b_0_GT_SERIAL_TX_TXP(0),
      user_clk_out => aurora_64b66b_0_user_clk_out
    );
axi_bram_ctrl_0: component design_1_axi_bram_ctrl_0_0
     port map (
      bram_addr_a(15 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_ADDR(15 downto 0),
      bram_addr_b(15 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_ADDR(15 downto 0),
      bram_clk_a => axi_bram_ctrl_0_BRAM_PORTA_CLK,
      bram_clk_b => axi_bram_ctrl_0_BRAM_PORTB_CLK,
      bram_en_a => axi_bram_ctrl_0_BRAM_PORTA_EN,
      bram_en_b => axi_bram_ctrl_0_BRAM_PORTB_EN,
      bram_rddata_a(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_DOUT(31 downto 0),
      bram_rddata_b(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_DOUT(31 downto 0),
      bram_rst_a => axi_bram_ctrl_0_BRAM_PORTA_RST,
      bram_rst_b => axi_bram_ctrl_0_BRAM_PORTB_RST,
      bram_we_a(3 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_WE(3 downto 0),
      bram_we_b(3 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_WE(3 downto 0),
      bram_wrdata_a(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_DIN(31 downto 0),
      bram_wrdata_b(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_DIN(31 downto 0),
      s_axi_aclk => clk_wiz_0_clk_out1,
      s_axi_araddr(15 downto 0) => axi_chip2chip_0_m_axi_ARADDR(15 downto 0),
      s_axi_arburst(1 downto 0) => axi_chip2chip_0_m_axi_ARBURST(1 downto 0),
      s_axi_arcache(3 downto 0) => B"0011",
      s_axi_aresetn => proc_sys_reset_0_peripheral_aresetn(0),
      s_axi_arlen(7 downto 0) => axi_chip2chip_0_m_axi_ARLEN(7 downto 0),
      s_axi_arlock => '0',
      s_axi_arprot(2 downto 0) => B"000",
      s_axi_arready => axi_chip2chip_0_m_axi_ARREADY,
      s_axi_arsize(2 downto 0) => axi_chip2chip_0_m_axi_ARSIZE(2 downto 0),
      s_axi_arvalid => axi_chip2chip_0_m_axi_ARVALID,
      s_axi_awaddr(15 downto 0) => axi_chip2chip_0_m_axi_AWADDR(15 downto 0),
      s_axi_awburst(1 downto 0) => axi_chip2chip_0_m_axi_AWBURST(1 downto 0),
      s_axi_awcache(3 downto 0) => B"0011",
      s_axi_awlen(7 downto 0) => axi_chip2chip_0_m_axi_AWLEN(7 downto 0),
      s_axi_awlock => '0',
      s_axi_awprot(2 downto 0) => B"000",
      s_axi_awready => axi_chip2chip_0_m_axi_AWREADY,
      s_axi_awsize(2 downto 0) => axi_chip2chip_0_m_axi_AWSIZE(2 downto 0),
      s_axi_awvalid => axi_chip2chip_0_m_axi_AWVALID,
      s_axi_bready => axi_chip2chip_0_m_axi_BREADY,
      s_axi_bresp(1 downto 0) => axi_chip2chip_0_m_axi_BRESP(1 downto 0),
      s_axi_bvalid => axi_chip2chip_0_m_axi_BVALID,
      s_axi_rdata(31 downto 0) => axi_chip2chip_0_m_axi_RDATA(31 downto 0),
      s_axi_rlast => axi_chip2chip_0_m_axi_RLAST,
      s_axi_rready => axi_chip2chip_0_m_axi_RREADY,
      s_axi_rresp(1 downto 0) => axi_chip2chip_0_m_axi_RRESP(1 downto 0),
      s_axi_rvalid => axi_chip2chip_0_m_axi_RVALID,
      s_axi_wdata(31 downto 0) => axi_chip2chip_0_m_axi_WDATA(31 downto 0),
      s_axi_wlast => axi_chip2chip_0_m_axi_WLAST,
      s_axi_wready => axi_chip2chip_0_m_axi_WREADY,
      s_axi_wstrb(3 downto 0) => axi_chip2chip_0_m_axi_WSTRB(3 downto 0),
      s_axi_wvalid => axi_chip2chip_0_m_axi_WVALID
    );
axi_bram_ctrl_0_bram: component design_1_axi_bram_ctrl_0_bram_0
     port map (
      addra(31 downto 16) => B"0000000000000000",
      addra(15 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_ADDR(15 downto 0),
      addrb(31 downto 16) => B"0000000000000000",
      addrb(15 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_ADDR(15 downto 0),
      clka => axi_bram_ctrl_0_BRAM_PORTA_CLK,
      clkb => axi_bram_ctrl_0_BRAM_PORTB_CLK,
      dina(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_DIN(31 downto 0),
      dinb(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_DIN(31 downto 0),
      douta(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_DOUT(31 downto 0),
      doutb(31 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_DOUT(31 downto 0),
      ena => axi_bram_ctrl_0_BRAM_PORTA_EN,
      enb => axi_bram_ctrl_0_BRAM_PORTB_EN,
      rsta => axi_bram_ctrl_0_BRAM_PORTA_RST,
      rsta_busy => NLW_axi_bram_ctrl_0_bram_rsta_busy_UNCONNECTED,
      rstb => axi_bram_ctrl_0_BRAM_PORTB_RST,
      rstb_busy => NLW_axi_bram_ctrl_0_bram_rstb_busy_UNCONNECTED,
      wea(3 downto 0) => axi_bram_ctrl_0_BRAM_PORTA_WE(3 downto 0),
      web(3 downto 0) => axi_bram_ctrl_0_BRAM_PORTB_WE(3 downto 0)
    );
axi_chip2chip_0: component design_1_axi_chip2chip_0_0
     port map (
      aurora_do_cc => NLW_axi_chip2chip_0_aurora_do_cc_UNCONNECTED,
      aurora_init_clk => clk_wiz_0_clk_out1,
      aurora_mmcm_not_locked => aurora_64b66b_0_mmcm_not_locked_out,
      aurora_pma_init_in => pma_init_generator_0_pma_init_in,
      aurora_pma_init_out => axi_chip2chip_0_aurora_pma_init_out,
      aurora_reset_pb => axi_chip2chip_0_aurora_reset_pb,
      axi_c2c_aurora_channel_up => aurora_64b66b_0_channel_up,
      axi_c2c_aurora_rx_tdata(63) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(0),
      axi_c2c_aurora_rx_tdata(62) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(1),
      axi_c2c_aurora_rx_tdata(61) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(2),
      axi_c2c_aurora_rx_tdata(60) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(3),
      axi_c2c_aurora_rx_tdata(59) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(4),
      axi_c2c_aurora_rx_tdata(58) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(5),
      axi_c2c_aurora_rx_tdata(57) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(6),
      axi_c2c_aurora_rx_tdata(56) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(7),
      axi_c2c_aurora_rx_tdata(55) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(8),
      axi_c2c_aurora_rx_tdata(54) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(9),
      axi_c2c_aurora_rx_tdata(53) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(10),
      axi_c2c_aurora_rx_tdata(52) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(11),
      axi_c2c_aurora_rx_tdata(51) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(12),
      axi_c2c_aurora_rx_tdata(50) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(13),
      axi_c2c_aurora_rx_tdata(49) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(14),
      axi_c2c_aurora_rx_tdata(48) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(15),
      axi_c2c_aurora_rx_tdata(47) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(16),
      axi_c2c_aurora_rx_tdata(46) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(17),
      axi_c2c_aurora_rx_tdata(45) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(18),
      axi_c2c_aurora_rx_tdata(44) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(19),
      axi_c2c_aurora_rx_tdata(43) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(20),
      axi_c2c_aurora_rx_tdata(42) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(21),
      axi_c2c_aurora_rx_tdata(41) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(22),
      axi_c2c_aurora_rx_tdata(40) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(23),
      axi_c2c_aurora_rx_tdata(39) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(24),
      axi_c2c_aurora_rx_tdata(38) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(25),
      axi_c2c_aurora_rx_tdata(37) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(26),
      axi_c2c_aurora_rx_tdata(36) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(27),
      axi_c2c_aurora_rx_tdata(35) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(28),
      axi_c2c_aurora_rx_tdata(34) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(29),
      axi_c2c_aurora_rx_tdata(33) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(30),
      axi_c2c_aurora_rx_tdata(32) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(31),
      axi_c2c_aurora_rx_tdata(31) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(32),
      axi_c2c_aurora_rx_tdata(30) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(33),
      axi_c2c_aurora_rx_tdata(29) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(34),
      axi_c2c_aurora_rx_tdata(28) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(35),
      axi_c2c_aurora_rx_tdata(27) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(36),
      axi_c2c_aurora_rx_tdata(26) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(37),
      axi_c2c_aurora_rx_tdata(25) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(38),
      axi_c2c_aurora_rx_tdata(24) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(39),
      axi_c2c_aurora_rx_tdata(23) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(40),
      axi_c2c_aurora_rx_tdata(22) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(41),
      axi_c2c_aurora_rx_tdata(21) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(42),
      axi_c2c_aurora_rx_tdata(20) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(43),
      axi_c2c_aurora_rx_tdata(19) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(44),
      axi_c2c_aurora_rx_tdata(18) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(45),
      axi_c2c_aurora_rx_tdata(17) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(46),
      axi_c2c_aurora_rx_tdata(16) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(47),
      axi_c2c_aurora_rx_tdata(15) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(48),
      axi_c2c_aurora_rx_tdata(14) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(49),
      axi_c2c_aurora_rx_tdata(13) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(50),
      axi_c2c_aurora_rx_tdata(12) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(51),
      axi_c2c_aurora_rx_tdata(11) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(52),
      axi_c2c_aurora_rx_tdata(10) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(53),
      axi_c2c_aurora_rx_tdata(9) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(54),
      axi_c2c_aurora_rx_tdata(8) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(55),
      axi_c2c_aurora_rx_tdata(7) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(56),
      axi_c2c_aurora_rx_tdata(6) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(57),
      axi_c2c_aurora_rx_tdata(5) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(58),
      axi_c2c_aurora_rx_tdata(4) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(59),
      axi_c2c_aurora_rx_tdata(3) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(60),
      axi_c2c_aurora_rx_tdata(2) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(61),
      axi_c2c_aurora_rx_tdata(1) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(62),
      axi_c2c_aurora_rx_tdata(0) => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA(63),
      axi_c2c_aurora_rx_tvalid => aurora_64b66b_0_USER_DATA_M_AXIS_RX_TVALID,
      axi_c2c_aurora_tx_tdata(63 downto 0) => axi_chip2chip_0_AXIS_TX_TDATA(63 downto 0),
      axi_c2c_aurora_tx_tready => axi_chip2chip_0_AXIS_TX_TREADY,
      axi_c2c_aurora_tx_tvalid => axi_chip2chip_0_AXIS_TX_TVALID,
      axi_c2c_config_error_out => NLW_axi_chip2chip_0_axi_c2c_config_error_out_UNCONNECTED,
      axi_c2c_link_status_out => NLW_axi_chip2chip_0_axi_c2c_link_status_out_UNCONNECTED,
      axi_c2c_m2s_intr_out(3 downto 0) => NLW_axi_chip2chip_0_axi_c2c_m2s_intr_out_UNCONNECTED(3 downto 0),
      axi_c2c_multi_bit_error_out => NLW_axi_chip2chip_0_axi_c2c_multi_bit_error_out_UNCONNECTED,
      axi_c2c_phy_clk => aurora_64b66b_0_user_clk_out,
      axi_c2c_s2m_intr_in(3 downto 0) => B"0000",
      m_aclk => clk_wiz_0_clk_out1,
      m_aresetn => proc_sys_reset_0_peripheral_aresetn(0),
      m_axi_araddr(31 downto 0) => axi_chip2chip_0_m_axi_ARADDR(31 downto 0),
      m_axi_arburst(1 downto 0) => axi_chip2chip_0_m_axi_ARBURST(1 downto 0),
      m_axi_arlen(7 downto 0) => axi_chip2chip_0_m_axi_ARLEN(7 downto 0),
      m_axi_arready => axi_chip2chip_0_m_axi_ARREADY,
      m_axi_arsize(2 downto 0) => axi_chip2chip_0_m_axi_ARSIZE(2 downto 0),
      m_axi_arvalid => axi_chip2chip_0_m_axi_ARVALID,
      m_axi_awaddr(31 downto 0) => axi_chip2chip_0_m_axi_AWADDR(31 downto 0),
      m_axi_awburst(1 downto 0) => axi_chip2chip_0_m_axi_AWBURST(1 downto 0),
      m_axi_awlen(7 downto 0) => axi_chip2chip_0_m_axi_AWLEN(7 downto 0),
      m_axi_awready => axi_chip2chip_0_m_axi_AWREADY,
      m_axi_awsize(2 downto 0) => axi_chip2chip_0_m_axi_AWSIZE(2 downto 0),
      m_axi_awvalid => axi_chip2chip_0_m_axi_AWVALID,
      m_axi_bready => axi_chip2chip_0_m_axi_BREADY,
      m_axi_bresp(1 downto 0) => axi_chip2chip_0_m_axi_BRESP(1 downto 0),
      m_axi_bvalid => axi_chip2chip_0_m_axi_BVALID,
      m_axi_rdata(31 downto 0) => axi_chip2chip_0_m_axi_RDATA(31 downto 0),
      m_axi_rlast => axi_chip2chip_0_m_axi_RLAST,
      m_axi_rready => axi_chip2chip_0_m_axi_RREADY,
      m_axi_rresp(1 downto 0) => axi_chip2chip_0_m_axi_RRESP(1 downto 0),
      m_axi_rvalid => axi_chip2chip_0_m_axi_RVALID,
      m_axi_wdata(31 downto 0) => axi_chip2chip_0_m_axi_WDATA(31 downto 0),
      m_axi_wlast => axi_chip2chip_0_m_axi_WLAST,
      m_axi_wready => axi_chip2chip_0_m_axi_WREADY,
      m_axi_wstrb(3 downto 0) => axi_chip2chip_0_m_axi_WSTRB(3 downto 0),
      m_axi_wvalid => axi_chip2chip_0_m_axi_WVALID
    );
clk_wiz_0: component design_1_clk_wiz_0_0
     port map (
      clk_in1_n => CLK200_1_CLK_N,
      clk_in1_p => CLK200_1_CLK_P,
      clk_out1 => clk_wiz_0_clk_out1,
      locked => clk_wiz_0_locked,
      resetn => FPGARSTB_1
    );
pma_init_generator_0: component design_1_pma_init_generator_0_0
     port map (
      FPGARSTB => FPGARSTB_1,
      aur_init_clk => clk_wiz_0_clk_out1,
      pma_init_in => pma_init_generator_0_pma_init_in,
      resetn => clk_wiz_0_locked
    );
proc_sys_reset_0: component design_1_proc_sys_reset_0_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_proc_sys_reset_0_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => aurora_64b66b_0_gt_reset_out,
      interconnect_aresetn(0) => NLW_proc_sys_reset_0_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_proc_sys_reset_0_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => proc_sys_reset_0_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_proc_sys_reset_0_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => clk_wiz_0_clk_out1
    );
end STRUCTURE;
