//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
//Date        : Wed Nov  2 09:14:09 2022
//Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (CLK125,
    CLK125_locked,
    FPGARSTB,
    GT_DIFF_REFCLK1_clk_n,
    GT_DIFF_REFCLK1_clk_p,
    GT_SERIAL_F2Z_txn,
    GT_SERIAL_F2Z_txp,
    GT_SERIAL_Z2F_rxn,
    GT_SERIAL_Z2F_rxp);
  input CLK125;
  input CLK125_locked;
  input FPGARSTB;
  input GT_DIFF_REFCLK1_clk_n;
  input GT_DIFF_REFCLK1_clk_p;
  output [0:0]GT_SERIAL_F2Z_txn;
  output [0:0]GT_SERIAL_F2Z_txp;
  input [0:0]GT_SERIAL_Z2F_rxn;
  input [0:0]GT_SERIAL_Z2F_rxp;

  wire CLK125;
  wire CLK125_locked;
  wire FPGARSTB;
  wire GT_DIFF_REFCLK1_clk_n;
  wire GT_DIFF_REFCLK1_clk_p;
  wire [0:0]GT_SERIAL_F2Z_txn;
  wire [0:0]GT_SERIAL_F2Z_txp;
  wire [0:0]GT_SERIAL_Z2F_rxn;
  wire [0:0]GT_SERIAL_Z2F_rxp;

  design_1 design_1_i
       (.CLK125(CLK125),
        .CLK125_locked(CLK125_locked),
        .FPGARSTB(FPGARSTB),
        .GT_DIFF_REFCLK1_clk_n(GT_DIFF_REFCLK1_clk_n),
        .GT_DIFF_REFCLK1_clk_p(GT_DIFF_REFCLK1_clk_p),
        .GT_SERIAL_F2Z_txn(GT_SERIAL_F2Z_txn),
        .GT_SERIAL_F2Z_txp(GT_SERIAL_F2Z_txp),
        .GT_SERIAL_Z2F_rxn(GT_SERIAL_Z2F_rxn),
        .GT_SERIAL_Z2F_rxp(GT_SERIAL_Z2F_rxp));
endmodule
