-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Sun Oct 30 16:16:06 2022
-- Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/amishima/vivado_work/CERN_prework/FPGA/project_1/project_1.gen/sources_1/bd/design_1/ip/design_1_pma_init_generator_0_0/design_1_pma_init_generator_0_0_sim_netlist.vhdl
-- Design      : design_1_pma_init_generator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu13p-flga2577-1-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_pma_init_generator_0_0_pma_init_generator is
  port (
    pma_init_in : out STD_LOGIC;
    aur_init_clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    FPGARSTB : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_pma_init_generator_0_0_pma_init_generator : entity is "pma_init_generator";
end design_1_pma_init_generator_0_0_pma_init_generator;

architecture STRUCTURE of design_1_pma_init_generator_0_0_pma_init_generator is
  signal \counter0_carry__0_n_2\ : STD_LOGIC;
  signal \counter0_carry__0_n_3\ : STD_LOGIC;
  signal \counter0_carry__0_n_4\ : STD_LOGIC;
  signal \counter0_carry__0_n_5\ : STD_LOGIC;
  signal \counter0_carry__0_n_6\ : STD_LOGIC;
  signal \counter0_carry__0_n_7\ : STD_LOGIC;
  signal counter0_carry_n_0 : STD_LOGIC;
  signal counter0_carry_n_1 : STD_LOGIC;
  signal counter0_carry_n_2 : STD_LOGIC;
  signal counter0_carry_n_3 : STD_LOGIC;
  signal counter0_carry_n_4 : STD_LOGIC;
  signal counter0_carry_n_5 : STD_LOGIC;
  signal counter0_carry_n_6 : STD_LOGIC;
  signal counter0_carry_n_7 : STD_LOGIC;
  signal \counter[15]_i_1_n_0\ : STD_LOGIC;
  signal counter_reg : STD_LOGIC_VECTOR ( 15 downto 4 );
  signal \counter_reg_n_0_[0]\ : STD_LOGIC;
  signal \counter_reg_n_0_[1]\ : STD_LOGIC;
  signal \counter_reg_n_0_[2]\ : STD_LOGIC;
  signal \counter_reg_n_0_[3]\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^pma_init_in\ : STD_LOGIC;
  signal pma_init_in_i_1_n_0 : STD_LOGIC;
  signal pma_init_in_i_2_n_0 : STD_LOGIC;
  signal pma_init_in_i_3_n_0 : STD_LOGIC;
  signal pma_init_in_i_4_n_0 : STD_LOGIC;
  signal \NLW_counter0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_counter0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \counter0_carry__0\ : label is 35;
begin
  pma_init_in <= \^pma_init_in\;
counter0_carry: unisim.vcomponents.CARRY8
     port map (
      CI => \counter_reg_n_0_[0]\,
      CI_TOP => '0',
      CO(7) => counter0_carry_n_0,
      CO(6) => counter0_carry_n_1,
      CO(5) => counter0_carry_n_2,
      CO(4) => counter0_carry_n_3,
      CO(3) => counter0_carry_n_4,
      CO(2) => counter0_carry_n_5,
      CO(1) => counter0_carry_n_6,
      CO(0) => counter0_carry_n_7,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => p_0_in(8 downto 1),
      S(7 downto 3) => counter_reg(8 downto 4),
      S(2) => \counter_reg_n_0_[3]\,
      S(1) => \counter_reg_n_0_[2]\,
      S(0) => \counter_reg_n_0_[1]\
    );
\counter0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => counter0_carry_n_0,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_counter0_carry__0_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \counter0_carry__0_n_2\,
      CO(4) => \counter0_carry__0_n_3\,
      CO(3) => \counter0_carry__0_n_4\,
      CO(2) => \counter0_carry__0_n_5\,
      CO(1) => \counter0_carry__0_n_6\,
      CO(0) => \counter0_carry__0_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \NLW_counter0_carry__0_O_UNCONNECTED\(7),
      O(6 downto 0) => p_0_in(15 downto 9),
      S(7) => '0',
      S(6 downto 0) => counter_reg(15 downto 9)
    );
\counter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \counter_reg_n_0_[0]\,
      O => p_0_in(0)
    );
\counter[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => resetn,
      I1 => FPGARSTB,
      O => \counter[15]_i_1_n_0\
    );
\counter_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(0),
      Q => \counter_reg_n_0_[0]\,
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(10),
      Q => counter_reg(10),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(11),
      Q => counter_reg(11),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(12),
      Q => counter_reg(12),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(13),
      Q => counter_reg(13),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(14),
      Q => counter_reg(14),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(15),
      Q => counter_reg(15),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(1),
      Q => \counter_reg_n_0_[1]\,
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(2),
      Q => \counter_reg_n_0_[2]\,
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(3),
      Q => \counter_reg_n_0_[3]\,
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(4),
      Q => counter_reg(4),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(5),
      Q => counter_reg(5),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(6),
      Q => counter_reg(6),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(7),
      Q => counter_reg(7),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(8),
      Q => counter_reg(8),
      R => \counter[15]_i_1_n_0\
    );
\counter_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => \^pma_init_in\,
      D => p_0_in(9),
      Q => counter_reg(9),
      R => \counter[15]_i_1_n_0\
    );
pma_init_in_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100FFFFFFFFFFFF"
    )
        port map (
      I0 => pma_init_in_i_2_n_0,
      I1 => pma_init_in_i_3_n_0,
      I2 => pma_init_in_i_4_n_0,
      I3 => \^pma_init_in\,
      I4 => FPGARSTB,
      I5 => resetn,
      O => pma_init_in_i_1_n_0
    );
pma_init_in_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counter_reg(6),
      I1 => counter_reg(5),
      I2 => counter_reg(8),
      I3 => counter_reg(7),
      O => pma_init_in_i_2_n_0
    );
pma_init_in_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counter_reg(14),
      I1 => counter_reg(13),
      I2 => counter_reg(4),
      I3 => counter_reg(15),
      O => pma_init_in_i_3_n_0
    );
pma_init_in_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counter_reg(10),
      I1 => counter_reg(9),
      I2 => counter_reg(12),
      I3 => counter_reg(11),
      O => pma_init_in_i_4_n_0
    );
pma_init_in_reg: unisim.vcomponents.FDRE
     port map (
      C => aur_init_clk,
      CE => '1',
      D => pma_init_in_i_1_n_0,
      Q => \^pma_init_in\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_pma_init_generator_0_0 is
  port (
    aur_init_clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    FPGARSTB : in STD_LOGIC;
    pma_init_in : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_pma_init_generator_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_pma_init_generator_0_0 : entity is "design_1_pma_init_generator_0_0,pma_init_generator,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_pma_init_generator_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_1_pma_init_generator_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_pma_init_generator_0_0 : entity is "pma_init_generator,Vivado 2020.2";
end design_1_pma_init_generator_0_0;

architecture STRUCTURE of design_1_pma_init_generator_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of aur_init_clk : signal is "xilinx.com:signal:clock:1.0 aur_init_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of aur_init_clk : signal is "XIL_INTERFACENAME aur_init_clk, ASSOCIATED_RESET resetn, FREQ_HZ 125000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_0_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of resetn : signal is "xilinx.com:signal:reset:1.0 resetn RST";
  attribute X_INTERFACE_PARAMETER of resetn : signal is "XIL_INTERFACENAME resetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
inst: entity work.design_1_pma_init_generator_0_0_pma_init_generator
     port map (
      FPGARSTB => FPGARSTB,
      aur_init_clk => aur_init_clk,
      pma_init_in => pma_init_in,
      resetn => resetn
    );
end STRUCTURE;
