-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Mon Oct 31 19:22:47 2022
-- Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/amishima/vivado_work/CERN_prework/FPGA/project_1/project_1.gen/sources_1/bd/design_2/ip/design_2_pma_init_generator_0_0/design_2_pma_init_generator_0_0_stub.vhdl
-- Design      : design_2_pma_init_generator_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcvu13p-flga2577-1-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_2_pma_init_generator_0_0 is
  Port ( 
    aur_init_clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    FPGARSTB : in STD_LOGIC;
    pma_init_in : out STD_LOGIC
  );

end design_2_pma_init_generator_0_0;

architecture stub of design_2_pma_init_generator_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "aur_init_clk,resetn,FPGARSTB,pma_init_in";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "pma_init_generator,Vivado 2020.2";
begin
end;
