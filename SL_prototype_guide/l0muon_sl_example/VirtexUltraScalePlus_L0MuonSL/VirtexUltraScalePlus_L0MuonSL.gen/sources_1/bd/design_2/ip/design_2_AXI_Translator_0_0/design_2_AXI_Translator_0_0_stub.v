// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 25 19:13:30 2022
// Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/amishima/vivado_work/l0muon_sl_example/VirtexUltraScalePlus_L0MuonSL/VirtexUltraScalePlus_L0MuonSL.gen/sources_1/bd/design_2/ip/design_2_AXI_Translator_0_0/design_2_AXI_Translator_0_0_stub.v
// Design      : design_2_AXI_Translator_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu13p-flga2577-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "AXI_Translator,Vivado 2020.2" *)
module design_2_AXI_Translator_0_0(clk, resetn, s_axi_araddr, s_axi_arburst, 
  s_axi_arid, s_axi_arlen, s_axi_arready, s_axi_arsize, s_axi_arvalid, s_axi_awaddr, 
  s_axi_awburst, s_axi_awid, s_axi_awlen, s_axi_awready, s_axi_awsize, s_axi_awvalid, 
  s_axi_bid, s_axi_bready, s_axi_bresp, s_axi_bvalid, s_axi_rdata, s_axi_rid, s_axi_rlast, 
  s_axi_rready, s_axi_rresp, s_axi_rvalid, s_axi_wdata, s_axi_wlast, s_axi_wready, s_axi_wstrb, 
  s_axi_wuser, s_axi_wvalid, RI_mode, RI_addr, RI_avalid, RI_wdata, RI_wvalid, RI_rdata, RI_rvalid)
/* synthesis syn_black_box black_box_pad_pin="clk,resetn,s_axi_araddr[31:0],s_axi_arburst[1:0],s_axi_arid[11:0],s_axi_arlen[7:0],s_axi_arready,s_axi_arsize[2:0],s_axi_arvalid,s_axi_awaddr[31:0],s_axi_awburst[1:0],s_axi_awid[11:0],s_axi_awlen[7:0],s_axi_awready,s_axi_awsize[2:0],s_axi_awvalid,s_axi_bid[11:0],s_axi_bready,s_axi_bresp[1:0],s_axi_bvalid,s_axi_rdata[31:0],s_axi_rid[11:0],s_axi_rlast,s_axi_rready,s_axi_rresp[1:0],s_axi_rvalid,s_axi_wdata[31:0],s_axi_wlast,s_axi_wready,s_axi_wstrb[3:0],s_axi_wuser[3:0],s_axi_wvalid,RI_mode,RI_addr[15:0],RI_avalid,RI_wdata[31:0],RI_wvalid,RI_rdata[31:0],RI_rvalid" */;
  input clk;
  input resetn;
  input [31:0]s_axi_araddr;
  input [1:0]s_axi_arburst;
  input [11:0]s_axi_arid;
  input [7:0]s_axi_arlen;
  output s_axi_arready;
  input [2:0]s_axi_arsize;
  input s_axi_arvalid;
  input [31:0]s_axi_awaddr;
  input [1:0]s_axi_awburst;
  input [11:0]s_axi_awid;
  input [7:0]s_axi_awlen;
  output s_axi_awready;
  input [2:0]s_axi_awsize;
  input s_axi_awvalid;
  output [11:0]s_axi_bid;
  input s_axi_bready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  output [31:0]s_axi_rdata;
  output [11:0]s_axi_rid;
  output s_axi_rlast;
  input s_axi_rready;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input [31:0]s_axi_wdata;
  input s_axi_wlast;
  output s_axi_wready;
  input [3:0]s_axi_wstrb;
  input [3:0]s_axi_wuser;
  input s_axi_wvalid;
  output RI_mode;
  output [15:0]RI_addr;
  output RI_avalid;
  output [31:0]RI_wdata;
  output RI_wvalid;
  input [31:0]RI_rdata;
  input RI_rvalid;
endmodule
