// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Fri Nov 25 19:13:30 2022
// Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/amishima/vivado_work/l0muon_sl_example/VirtexUltraScalePlus_L0MuonSL/VirtexUltraScalePlus_L0MuonSL.gen/sources_1/bd/design_2/ip/design_2_AXI_Translator_0_0/design_2_AXI_Translator_0_0_sim_netlist.v
// Design      : design_2_AXI_Translator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcvu13p-flga2577-1-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_2_AXI_Translator_0_0,AXI_Translator,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "AXI_Translator,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module design_2_AXI_Translator_0_0
   (clk,
    resetn,
    s_axi_araddr,
    s_axi_arburst,
    s_axi_arid,
    s_axi_arlen,
    s_axi_arready,
    s_axi_arsize,
    s_axi_arvalid,
    s_axi_awaddr,
    s_axi_awburst,
    s_axi_awid,
    s_axi_awlen,
    s_axi_awready,
    s_axi_awsize,
    s_axi_awvalid,
    s_axi_bid,
    s_axi_bready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_rdata,
    s_axi_rid,
    s_axi_rlast,
    s_axi_rready,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_wdata,
    s_axi_wlast,
    s_axi_wready,
    s_axi_wstrb,
    s_axi_wuser,
    s_axi_wvalid,
    RI_mode,
    RI_addr,
    RI_avalid,
    RI_wdata,
    RI_wvalid,
    RI_rdata,
    RI_rvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF s_axi, ASSOCIATED_RESET resetn, FREQ_HZ 125000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_2_CLK125, INSERT_VIP 0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 resetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME resetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input resetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARADDR" *) input [31:0]s_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARBURST" *) input [1:0]s_axi_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARID" *) input [11:0]s_axi_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARLEN" *) input [7:0]s_axi_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARREADY" *) output s_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARSIZE" *) input [2:0]s_axi_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARVALID" *) input s_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWADDR" *) input [31:0]s_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWBURST" *) input [1:0]s_axi_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWID" *) input [11:0]s_axi_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWLEN" *) input [7:0]s_axi_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWREADY" *) output s_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWSIZE" *) input [2:0]s_axi_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWVALID" *) input s_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BID" *) output [11:0]s_axi_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BREADY" *) input s_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BRESP" *) output [1:0]s_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BVALID" *) output s_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RDATA" *) output [31:0]s_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RID" *) output [11:0]s_axi_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RLAST" *) output s_axi_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RREADY" *) input s_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RRESP" *) output [1:0]s_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RVALID" *) output s_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WDATA" *) input [31:0]s_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WLAST" *) input s_axi_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WREADY" *) output s_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WSTRB" *) input [3:0]s_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WUSER" *) input [3:0]s_axi_wuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 125000000, ID_WIDTH 12, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 4, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0.000, CLK_DOMAIN design_2_CLK125, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s_axi_wvalid;
  output RI_mode;
  output [15:0]RI_addr;
  output RI_avalid;
  output [31:0]RI_wdata;
  output RI_wvalid;
  input [31:0]RI_rdata;
  input RI_rvalid;

  wire \<const0> ;
  wire [15:0]\^RI_addr ;
  wire RI_avalid;
  wire RI_mode;
  wire [31:0]RI_rdata;
  wire RI_rvalid;
  wire [31:0]RI_wdata;
  wire RI_wvalid;
  wire clk;
  wire resetn;
  wire [31:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [31:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rlast;
  wire s_axi_rready;
  wire [1:1]\^s_axi_rresp ;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;

  assign RI_addr[15:12] = \^RI_addr [15:12];
  assign RI_addr[11] = \<const0> ;
  assign RI_addr[10] = \<const0> ;
  assign RI_addr[9:0] = \^RI_addr [9:0];
  assign s_axi_bid[11] = \<const0> ;
  assign s_axi_bid[10] = \<const0> ;
  assign s_axi_bid[9] = \<const0> ;
  assign s_axi_bid[8] = \<const0> ;
  assign s_axi_bid[7] = \<const0> ;
  assign s_axi_bid[6] = \<const0> ;
  assign s_axi_bid[5] = \<const0> ;
  assign s_axi_bid[4] = \<const0> ;
  assign s_axi_bid[3] = \<const0> ;
  assign s_axi_bid[2] = \<const0> ;
  assign s_axi_bid[1] = \<const0> ;
  assign s_axi_bid[0] = \<const0> ;
  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rid[11] = \<const0> ;
  assign s_axi_rid[10] = \<const0> ;
  assign s_axi_rid[9] = \<const0> ;
  assign s_axi_rid[8] = \<const0> ;
  assign s_axi_rid[7] = \<const0> ;
  assign s_axi_rid[6] = \<const0> ;
  assign s_axi_rid[5] = \<const0> ;
  assign s_axi_rid[4] = \<const0> ;
  assign s_axi_rid[3] = \<const0> ;
  assign s_axi_rid[2] = \<const0> ;
  assign s_axi_rid[1] = \<const0> ;
  assign s_axi_rid[0] = \<const0> ;
  assign s_axi_rresp[1] = \^s_axi_rresp [1];
  assign s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_2_AXI_Translator_0_0_AXI_Translator inst
       (.Q(s_axi_rvalid),
        .RI_addr({\^RI_addr [15:12],\^RI_addr [9:0]}),
        .RI_avalid(RI_avalid),
        .RI_mode(RI_mode),
        .RI_rdata(RI_rdata),
        .RI_rvalid(RI_rvalid),
        .RI_wdata(RI_wdata),
        .RI_wvalid(RI_wvalid),
        .clk(clk),
        .resetn(resetn),
        .s_axi_araddr(s_axi_araddr[15:2]),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[15:2]),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid_reg_0(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rlast(s_axi_rlast),
        .s_axi_rready(s_axi_rready),
        .s_axi_rresp(\^s_axi_rresp ),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "AXI_Translator" *) 
module design_2_AXI_Translator_0_0_AXI_Translator
   (Q,
    s_axi_awready,
    s_axi_wready,
    s_axi_bvalid_reg_0,
    s_axi_arready,
    s_axi_rdata,
    RI_mode,
    RI_addr,
    RI_avalid,
    RI_wdata,
    RI_wvalid,
    s_axi_rlast,
    s_axi_rresp,
    resetn,
    clk,
    s_axi_rready,
    RI_rvalid,
    s_axi_bready,
    RI_rdata,
    s_axi_arvalid,
    s_axi_awvalid,
    s_axi_araddr,
    s_axi_awaddr,
    s_axi_wvalid,
    s_axi_wdata);
  output Q;
  output s_axi_awready;
  output s_axi_wready;
  output s_axi_bvalid_reg_0;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  output RI_mode;
  output [13:0]RI_addr;
  output RI_avalid;
  output [31:0]RI_wdata;
  output RI_wvalid;
  output s_axi_rlast;
  output [0:0]s_axi_rresp;
  input resetn;
  input clk;
  input s_axi_rready;
  input RI_rvalid;
  input s_axi_bready;
  input [31:0]RI_rdata;
  input s_axi_arvalid;
  input s_axi_awvalid;
  input [13:0]s_axi_araddr;
  input [13:0]s_axi_awaddr;
  input s_axi_wvalid;
  input [31:0]s_axi_wdata;

  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_2_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state[2]_i_2_n_0 ;
  wire \FSM_onehot_state[2]_i_3_n_0 ;
  wire \FSM_onehot_state[3]_i_1_n_0 ;
  wire \FSM_onehot_state[4]_i_1_n_0 ;
  wire \FSM_onehot_state[5]_i_1_n_0 ;
  wire \FSM_onehot_state[5]_i_2_n_0 ;
  wire \FSM_onehot_state[6]_i_1_n_0 ;
  wire \FSM_onehot_state[6]_i_2_n_0 ;
  wire \FSM_onehot_state_reg_n_0_[1] ;
  wire \FSM_onehot_state_reg_n_0_[5] ;
  wire Q;
  wire [13:0]RI_addr;
  wire \RI_addr[0]_i_1_n_0 ;
  wire \RI_addr[0]_i_2_n_0 ;
  wire \RI_addr[12]_i_1_n_0 ;
  wire \RI_addr[12]_i_2_n_0 ;
  wire \RI_addr[13]_i_1_n_0 ;
  wire \RI_addr[13]_i_2_n_0 ;
  wire \RI_addr[14]_i_1_n_0 ;
  wire \RI_addr[14]_i_2_n_0 ;
  wire \RI_addr[15]_i_1_n_0 ;
  wire \RI_addr[15]_i_2_n_0 ;
  wire \RI_addr[15]_i_3_n_0 ;
  wire \RI_addr[15]_i_4_n_0 ;
  wire \RI_addr[15]_i_5_n_0 ;
  wire \RI_addr[15]_i_6_n_0 ;
  wire \RI_addr[1]_i_1_n_0 ;
  wire \RI_addr[1]_i_2_n_0 ;
  wire \RI_addr[2]_i_1_n_0 ;
  wire \RI_addr[2]_i_2_n_0 ;
  wire \RI_addr[3]_i_1_n_0 ;
  wire \RI_addr[3]_i_2_n_0 ;
  wire \RI_addr[4]_i_1_n_0 ;
  wire \RI_addr[4]_i_2_n_0 ;
  wire \RI_addr[5]_i_1_n_0 ;
  wire \RI_addr[5]_i_2_n_0 ;
  wire \RI_addr[6]_i_1_n_0 ;
  wire \RI_addr[6]_i_2_n_0 ;
  wire \RI_addr[7]_i_1_n_0 ;
  wire \RI_addr[7]_i_2_n_0 ;
  wire \RI_addr[8]_i_1_n_0 ;
  wire \RI_addr[8]_i_2_n_0 ;
  wire \RI_addr[9]_i_1_n_0 ;
  wire \RI_addr[9]_i_2_n_0 ;
  wire RI_avalid;
  wire RI_avalid_i_1_n_0;
  wire RI_avalid_i_2_n_0;
  wire RI_avalid_i_3_n_0;
  wire RI_mode;
  wire RI_mode_i_10_n_0;
  wire RI_mode_i_11_n_0;
  wire RI_mode_i_12_n_0;
  wire RI_mode_i_1_n_0;
  wire RI_mode_i_2_n_0;
  wire RI_mode_i_3_n_0;
  wire RI_mode_i_4_n_0;
  wire RI_mode_i_5_n_0;
  wire RI_mode_i_6_n_0;
  wire RI_mode_i_7_n_0;
  wire RI_mode_i_8_n_0;
  wire RI_mode_i_9_n_0;
  wire [31:0]RI_rdata;
  wire RI_rvalid;
  wire [31:0]RI_wdata;
  wire \RI_wdata[0]_i_1_n_0 ;
  wire \RI_wdata[10]_i_1_n_0 ;
  wire \RI_wdata[11]_i_1_n_0 ;
  wire \RI_wdata[12]_i_1_n_0 ;
  wire \RI_wdata[13]_i_1_n_0 ;
  wire \RI_wdata[14]_i_1_n_0 ;
  wire \RI_wdata[15]_i_1_n_0 ;
  wire \RI_wdata[16]_i_1_n_0 ;
  wire \RI_wdata[17]_i_1_n_0 ;
  wire \RI_wdata[18]_i_1_n_0 ;
  wire \RI_wdata[19]_i_1_n_0 ;
  wire \RI_wdata[1]_i_1_n_0 ;
  wire \RI_wdata[20]_i_1_n_0 ;
  wire \RI_wdata[21]_i_1_n_0 ;
  wire \RI_wdata[22]_i_1_n_0 ;
  wire \RI_wdata[23]_i_1_n_0 ;
  wire \RI_wdata[24]_i_1_n_0 ;
  wire \RI_wdata[25]_i_1_n_0 ;
  wire \RI_wdata[26]_i_1_n_0 ;
  wire \RI_wdata[27]_i_1_n_0 ;
  wire \RI_wdata[28]_i_1_n_0 ;
  wire \RI_wdata[29]_i_1_n_0 ;
  wire \RI_wdata[2]_i_1_n_0 ;
  wire \RI_wdata[30]_i_1_n_0 ;
  wire \RI_wdata[31]_i_1_n_0 ;
  wire \RI_wdata[3]_i_1_n_0 ;
  wire \RI_wdata[4]_i_1_n_0 ;
  wire \RI_wdata[5]_i_1_n_0 ;
  wire \RI_wdata[6]_i_1_n_0 ;
  wire \RI_wdata[7]_i_1_n_0 ;
  wire \RI_wdata[8]_i_1_n_0 ;
  wire \RI_wdata[9]_i_1_n_0 ;
  wire RI_wvalid;
  wire RI_wvalid_i_1_n_0;
  wire clk;
  wire p_0_in11_in;
  wire p_0_in7_in;
  wire [31:0]p_1_in;
  wire p_1_in5_in;
  wire p_1_in9_in;
  wire p_1_in_0;
  wire resetn;
  wire [13:0]s_axi_araddr;
  wire s_axi_arready;
  wire s_axi_arready0;
  wire s_axi_arready_i_1_n_0;
  wire s_axi_arvalid;
  wire [13:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awready0;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid_i_1_n_0;
  wire s_axi_bvalid_reg_0;
  wire [31:0]s_axi_rdata;
  wire s_axi_rdata0;
  wire \s_axi_rdata[31]_i_1_n_0 ;
  wire \s_axi_rdata[31]_i_2_n_0 ;
  wire s_axi_rlast;
  wire s_axi_rlast_i_1_n_0;
  wire s_axi_rready;
  wire [0:0]s_axi_rresp;
  wire s_axi_rvalid_i_1_n_0;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wready0;
  wire s_axi_wvalid;

  LUT6 #(
    .INIT(64'h80FFFFFF80808080)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .I2(p_1_in_0),
        .I3(s_axi_bready),
        .I4(s_axi_bvalid_reg_0),
        .I5(p_0_in7_in),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00707070)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(s_axi_awready),
        .I4(s_axi_awvalid),
        .I5(\FSM_onehot_state[1]_i_2_n_0 ),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF40FF40FF404040)) 
    \FSM_onehot_state[1]_i_2 
       (.I0(RI_mode_i_3_n_0),
        .I1(s_axi_bready),
        .I2(s_axi_bvalid_reg_0),
        .I3(\FSM_onehot_state[6]_i_2_n_0 ),
        .I4(p_1_in9_in),
        .I5(p_0_in11_in),
        .O(\FSM_onehot_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4F4444444F444F44)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(\FSM_onehot_state[2]_i_2_n_0 ),
        .I1(p_1_in_0),
        .I2(\FSM_onehot_state[2]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(RI_mode_i_5_n_0),
        .I5(RI_mode_i_4_n_0),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state[2]_i_2 
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .O(\FSM_onehot_state[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF7F7F7F)) 
    \FSM_onehot_state[2]_i_3 
       (.I0(s_axi_awvalid),
        .I1(s_axi_awready),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(s_axi_arready),
        .I4(s_axi_arvalid),
        .O(\FSM_onehot_state[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F4444444F444F44)) 
    \FSM_onehot_state[3]_i_1 
       (.I0(\FSM_onehot_state[6]_i_2_n_0 ),
        .I1(p_1_in9_in),
        .I2(\RI_addr[15]_i_4_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(RI_mode_i_5_n_0),
        .I5(RI_mode_i_4_n_0),
        .O(\FSM_onehot_state[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h80FFFFFF80808080)) 
    \FSM_onehot_state[4]_i_1 
       (.I0(s_axi_wready),
        .I1(s_axi_wvalid),
        .I2(\FSM_onehot_state_reg_n_0_[5] ),
        .I3(s_axi_bready),
        .I4(s_axi_bvalid_reg_0),
        .I5(p_1_in5_in),
        .O(\FSM_onehot_state[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF000008AA)) 
    \FSM_onehot_state[5]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(RI_mode_i_4_n_0),
        .I2(RI_mode_i_5_n_0),
        .I3(RI_mode_i_6_n_0),
        .I4(RI_mode_i_7_n_0),
        .I5(\FSM_onehot_state[5]_i_2_n_0 ),
        .O(\FSM_onehot_state[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h2A)) 
    \FSM_onehot_state[5]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(s_axi_wvalid),
        .I2(s_axi_wready),
        .O(\FSM_onehot_state[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h444F4F4F444F444F)) 
    \FSM_onehot_state[6]_i_1 
       (.I0(\FSM_onehot_state[6]_i_2_n_0 ),
        .I1(p_0_in11_in),
        .I2(\RI_addr[15]_i_4_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(RI_mode_i_5_n_0),
        .I5(RI_mode_i_4_n_0),
        .O(\FSM_onehot_state[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state[6]_i_2 
       (.I0(s_axi_rready),
        .I1(Q),
        .O(\FSM_onehot_state[6]_i_2_n_0 ));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[0] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(p_0_in7_in),
        .R(s_axi_arready_i_1_n_0));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[1] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[1] ),
        .S(s_axi_arready_i_1_n_0));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(p_1_in_0),
        .R(s_axi_arready_i_1_n_0));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[3]_i_1_n_0 ),
        .Q(p_1_in9_in),
        .R(s_axi_arready_i_1_n_0));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[4] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[4]_i_1_n_0 ),
        .Q(p_1_in5_in),
        .R(s_axi_arready_i_1_n_0));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[5] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[5]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[5] ),
        .R(s_axi_arready_i_1_n_0));
  (* FSM_ENCODED_STATES = "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[6] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\FSM_onehot_state[6]_i_1_n_0 ),
        .Q(p_0_in11_in),
        .R(s_axi_arready_i_1_n_0));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[0]_i_1 
       (.I0(\RI_addr[0]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[0]),
        .O(\RI_addr[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[0]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[0]),
        .O(\RI_addr[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[12]_i_1 
       (.I0(\RI_addr[12]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[10]),
        .O(\RI_addr[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[12]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[10]),
        .O(\RI_addr[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[13]_i_1 
       (.I0(\RI_addr[13]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[11]),
        .O(\RI_addr[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[13]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[11]),
        .O(\RI_addr[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[14]_i_1 
       (.I0(\RI_addr[14]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[12]),
        .O(\RI_addr[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[14]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[12]),
        .O(\RI_addr[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[15]_i_1 
       (.I0(\RI_addr[15]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[13]),
        .O(\RI_addr[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[15]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[13]),
        .O(\RI_addr[15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h000E)) 
    \RI_addr[15]_i_3 
       (.I0(\RI_addr[15]_i_5_n_0 ),
        .I1(RI_mode_i_9_n_0),
        .I2(RI_mode_i_10_n_0),
        .I3(\RI_addr[15]_i_6_n_0 ),
        .O(\RI_addr[15]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \RI_addr[15]_i_4 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_arvalid),
        .I2(s_axi_arready),
        .O(\RI_addr[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h557F)) 
    \RI_addr[15]_i_5 
       (.I0(s_axi_araddr[6]),
        .I1(s_axi_araddr[5]),
        .I2(s_axi_araddr[11]),
        .I3(s_axi_araddr[7]),
        .O(\RI_addr[15]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFF8F)) 
    \RI_addr[15]_i_6 
       (.I0(s_axi_araddr[11]),
        .I1(s_axi_araddr[7]),
        .I2(s_axi_arvalid),
        .I3(s_axi_araddr[10]),
        .O(\RI_addr[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[1]_i_1 
       (.I0(\RI_addr[1]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[1]),
        .O(\RI_addr[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[1]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[1]),
        .O(\RI_addr[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[2]_i_1 
       (.I0(\RI_addr[2]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[2]),
        .O(\RI_addr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[2]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[2]),
        .O(\RI_addr[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[3]_i_1 
       (.I0(\RI_addr[3]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[3]),
        .O(\RI_addr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[3]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[3]),
        .O(\RI_addr[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[4]_i_1 
       (.I0(\RI_addr[4]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[4]),
        .O(\RI_addr[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[4]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[4]),
        .O(\RI_addr[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[5]_i_1 
       (.I0(\RI_addr[5]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[5]),
        .O(\RI_addr[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[5]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[5]),
        .O(\RI_addr[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[6]_i_1 
       (.I0(\RI_addr[6]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[6]),
        .O(\RI_addr[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[6]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[6]),
        .O(\RI_addr[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[7]_i_1 
       (.I0(\RI_addr[7]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[7]),
        .O(\RI_addr[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[7]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[7]),
        .O(\RI_addr[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[8]_i_1 
       (.I0(\RI_addr[8]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[8]),
        .O(\RI_addr[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[8]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[8]),
        .O(\RI_addr[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1011F0FF10111011)) 
    \RI_addr[9]_i_1 
       (.I0(\RI_addr[9]_i_2_n_0 ),
        .I1(RI_mode_i_7_n_0),
        .I2(\RI_addr[15]_i_3_n_0 ),
        .I3(RI_mode_i_6_n_0),
        .I4(\RI_addr[15]_i_4_n_0 ),
        .I5(s_axi_araddr[9]),
        .O(\RI_addr[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \RI_addr[9]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awaddr[9]),
        .O(\RI_addr[9]_i_2_n_0 ));
  FDRE \RI_addr_reg[0] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[0]_i_1_n_0 ),
        .Q(RI_addr[0]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[12] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[12]_i_1_n_0 ),
        .Q(RI_addr[10]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[13] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[13]_i_1_n_0 ),
        .Q(RI_addr[11]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[14] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[14]_i_1_n_0 ),
        .Q(RI_addr[12]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[15] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[15]_i_1_n_0 ),
        .Q(RI_addr[13]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[1] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[1]_i_1_n_0 ),
        .Q(RI_addr[1]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[2] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[2]_i_1_n_0 ),
        .Q(RI_addr[2]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[3] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[3]_i_1_n_0 ),
        .Q(RI_addr[3]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[4] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[4]_i_1_n_0 ),
        .Q(RI_addr[4]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[5] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[5]_i_1_n_0 ),
        .Q(RI_addr[5]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[6] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[6]_i_1_n_0 ),
        .Q(RI_addr[6]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[7] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[7]_i_1_n_0 ),
        .Q(RI_addr[7]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[8] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[8]_i_1_n_0 ),
        .Q(RI_addr[8]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_addr_reg[9] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_addr[9]_i_1_n_0 ),
        .Q(RI_addr[9]),
        .R(s_axi_arready_i_1_n_0));
  LUT6 #(
    .INIT(64'h00C4C4C400C400C4)) 
    RI_avalid_i_1
       (.I0(RI_avalid_i_2_n_0),
        .I1(\FSM_onehot_state_reg_n_0_[1] ),
        .I2(RI_avalid_i_3_n_0),
        .I3(RI_mode_i_6_n_0),
        .I4(RI_mode_i_5_n_0),
        .I5(RI_mode_i_4_n_0),
        .O(RI_avalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h7)) 
    RI_avalid_i_2
       (.I0(s_axi_awready),
        .I1(s_axi_awvalid),
        .O(RI_avalid_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    RI_avalid_i_3
       (.I0(s_axi_arready),
        .I1(s_axi_arvalid),
        .O(RI_avalid_i_3_n_0));
  FDRE RI_avalid_reg
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(RI_avalid_i_1_n_0),
        .Q(RI_avalid),
        .R(s_axi_arready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    RI_mode_i_1
       (.I0(p_1_in9_in),
        .I1(p_0_in11_in),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(RI_mode_i_3_n_0),
        .I4(\FSM_onehot_state_reg_n_0_[5] ),
        .I5(p_1_in_0),
        .O(RI_mode_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    RI_mode_i_10
       (.I0(s_axi_araddr[12]),
        .I1(s_axi_araddr[8]),
        .I2(s_axi_araddr[13]),
        .I3(s_axi_araddr[9]),
        .O(RI_mode_i_10_n_0));
  LUT5 #(
    .INIT(32'hFE000000)) 
    RI_mode_i_11
       (.I0(s_axi_awaddr[4]),
        .I1(s_axi_awaddr[3]),
        .I2(s_axi_awaddr[5]),
        .I3(s_axi_awaddr[6]),
        .I4(s_axi_awaddr[7]),
        .O(RI_mode_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    RI_mode_i_12
       (.I0(s_axi_awaddr[10]),
        .I1(s_axi_awaddr[13]),
        .I2(s_axi_awvalid),
        .I3(s_axi_awaddr[11]),
        .O(RI_mode_i_12_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF000008AA)) 
    RI_mode_i_2
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(RI_mode_i_4_n_0),
        .I2(RI_mode_i_5_n_0),
        .I3(RI_mode_i_6_n_0),
        .I4(RI_mode_i_7_n_0),
        .I5(RI_mode_i_8_n_0),
        .O(RI_mode_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h1)) 
    RI_mode_i_3
       (.I0(p_0_in7_in),
        .I1(p_1_in5_in),
        .O(RI_mode_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hABBBFFFF)) 
    RI_mode_i_4
       (.I0(RI_mode_i_9_n_0),
        .I1(s_axi_araddr[7]),
        .I2(s_axi_araddr[11]),
        .I3(s_axi_araddr[5]),
        .I4(s_axi_araddr[6]),
        .O(RI_mode_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFFFBBB)) 
    RI_mode_i_5
       (.I0(s_axi_araddr[10]),
        .I1(s_axi_arvalid),
        .I2(s_axi_araddr[7]),
        .I3(s_axi_araddr[11]),
        .I4(RI_mode_i_10_n_0),
        .O(RI_mode_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    RI_mode_i_6
       (.I0(RI_mode_i_11_n_0),
        .I1(RI_mode_i_12_n_0),
        .I2(s_axi_awaddr[12]),
        .I3(s_axi_awaddr[8]),
        .I4(s_axi_awaddr[9]),
        .O(RI_mode_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8FFF)) 
    RI_mode_i_7
       (.I0(s_axi_arvalid),
        .I1(s_axi_arready),
        .I2(s_axi_awvalid),
        .I3(s_axi_awready),
        .O(RI_mode_i_7_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    RI_mode_i_8
       (.I0(p_1_in5_in),
        .I1(\FSM_onehot_state_reg_n_0_[5] ),
        .O(RI_mode_i_8_n_0));
  LUT5 #(
    .INIT(32'h00011111)) 
    RI_mode_i_9
       (.I0(s_axi_araddr[3]),
        .I1(s_axi_araddr[4]),
        .I2(s_axi_araddr[2]),
        .I3(s_axi_araddr[7]),
        .I4(s_axi_araddr[5]),
        .O(RI_mode_i_9_n_0));
  FDRE RI_mode_reg
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(RI_mode_i_2_n_0),
        .Q(RI_mode),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[0]_i_1 
       (.I0(s_axi_wdata[0]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[10]_i_1 
       (.I0(s_axi_wdata[10]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[11]_i_1 
       (.I0(s_axi_wdata[11]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[12]_i_1 
       (.I0(s_axi_wdata[12]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[13]_i_1 
       (.I0(s_axi_wdata[13]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[14]_i_1 
       (.I0(s_axi_wdata[14]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[15]_i_1 
       (.I0(s_axi_wdata[15]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[16]_i_1 
       (.I0(s_axi_wdata[16]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[17]_i_1 
       (.I0(s_axi_wdata[17]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[18]_i_1 
       (.I0(s_axi_wdata[18]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[19]_i_1 
       (.I0(s_axi_wdata[19]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[1]_i_1 
       (.I0(s_axi_wdata[1]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[20]_i_1 
       (.I0(s_axi_wdata[20]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[21]_i_1 
       (.I0(s_axi_wdata[21]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[22]_i_1 
       (.I0(s_axi_wdata[22]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[23]_i_1 
       (.I0(s_axi_wdata[23]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[24]_i_1 
       (.I0(s_axi_wdata[24]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[25]_i_1 
       (.I0(s_axi_wdata[25]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[26]_i_1 
       (.I0(s_axi_wdata[26]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[27]_i_1 
       (.I0(s_axi_wdata[27]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[28]_i_1 
       (.I0(s_axi_wdata[28]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[29]_i_1 
       (.I0(s_axi_wdata[29]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[2]_i_1 
       (.I0(s_axi_wdata[2]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[30]_i_1 
       (.I0(s_axi_wdata[30]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[30]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[31]_i_1 
       (.I0(s_axi_wdata[31]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[3]_i_1 
       (.I0(s_axi_wdata[3]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[4]_i_1 
       (.I0(s_axi_wdata[4]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[5]_i_1 
       (.I0(s_axi_wdata[5]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[6]_i_1 
       (.I0(s_axi_wdata[6]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[7]_i_1 
       (.I0(s_axi_wdata[7]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[8]_i_1 
       (.I0(s_axi_wdata[8]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \RI_wdata[9]_i_1 
       (.I0(s_axi_wdata[9]),
        .I1(s_axi_wready),
        .I2(s_axi_wvalid),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .O(\RI_wdata[9]_i_1_n_0 ));
  FDRE \RI_wdata_reg[0] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[0]_i_1_n_0 ),
        .Q(RI_wdata[0]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[10] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[10]_i_1_n_0 ),
        .Q(RI_wdata[10]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[11] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[11]_i_1_n_0 ),
        .Q(RI_wdata[11]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[12] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[12]_i_1_n_0 ),
        .Q(RI_wdata[12]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[13] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[13]_i_1_n_0 ),
        .Q(RI_wdata[13]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[14] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[14]_i_1_n_0 ),
        .Q(RI_wdata[14]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[15] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[15]_i_1_n_0 ),
        .Q(RI_wdata[15]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[16] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[16]_i_1_n_0 ),
        .Q(RI_wdata[16]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[17] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[17]_i_1_n_0 ),
        .Q(RI_wdata[17]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[18] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[18]_i_1_n_0 ),
        .Q(RI_wdata[18]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[19] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[19]_i_1_n_0 ),
        .Q(RI_wdata[19]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[1] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[1]_i_1_n_0 ),
        .Q(RI_wdata[1]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[20] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[20]_i_1_n_0 ),
        .Q(RI_wdata[20]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[21] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[21]_i_1_n_0 ),
        .Q(RI_wdata[21]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[22] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[22]_i_1_n_0 ),
        .Q(RI_wdata[22]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[23] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[23]_i_1_n_0 ),
        .Q(RI_wdata[23]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[24] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[24]_i_1_n_0 ),
        .Q(RI_wdata[24]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[25] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[25]_i_1_n_0 ),
        .Q(RI_wdata[25]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[26] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[26]_i_1_n_0 ),
        .Q(RI_wdata[26]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[27] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[27]_i_1_n_0 ),
        .Q(RI_wdata[27]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[28] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[28]_i_1_n_0 ),
        .Q(RI_wdata[28]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[29] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[29]_i_1_n_0 ),
        .Q(RI_wdata[29]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[2] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[2]_i_1_n_0 ),
        .Q(RI_wdata[2]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[30] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[30]_i_1_n_0 ),
        .Q(RI_wdata[30]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[31] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[31]_i_1_n_0 ),
        .Q(RI_wdata[31]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[3] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[3]_i_1_n_0 ),
        .Q(RI_wdata[3]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[4] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[4]_i_1_n_0 ),
        .Q(RI_wdata[4]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[5] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[5]_i_1_n_0 ),
        .Q(RI_wdata[5]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[6] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[6]_i_1_n_0 ),
        .Q(RI_wdata[6]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[7] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[7]_i_1_n_0 ),
        .Q(RI_wdata[7]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[8] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[8]_i_1_n_0 ),
        .Q(RI_wdata[8]),
        .R(s_axi_arready_i_1_n_0));
  FDRE \RI_wdata_reg[9] 
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(\RI_wdata[9]_i_1_n_0 ),
        .Q(RI_wdata[9]),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h80)) 
    RI_wvalid_i_1
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(s_axi_wvalid),
        .I2(s_axi_wready),
        .O(RI_wvalid_i_1_n_0));
  FDRE RI_wvalid_reg
       (.C(clk),
        .CE(RI_mode_i_1_n_0),
        .D(RI_wvalid_i_1_n_0),
        .Q(RI_wvalid),
        .R(s_axi_arready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    s_axi_arready_i_1
       (.I0(resetn),
        .O(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h08)) 
    s_axi_arready_i_2
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_arvalid),
        .I2(s_axi_arready),
        .O(s_axi_arready0));
  FDRE s_axi_arready_reg
       (.C(clk),
        .CE(1'b1),
        .D(s_axi_arready0),
        .Q(s_axi_arready),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h08)) 
    s_axi_awready_i_1
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(s_axi_awvalid),
        .I2(s_axi_awready),
        .O(s_axi_awready0));
  FDRE s_axi_awready_reg
       (.C(clk),
        .CE(1'b1),
        .D(s_axi_awready0),
        .Q(s_axi_awready),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h55FC)) 
    s_axi_bvalid_i_1
       (.I0(s_axi_bready),
        .I1(p_0_in7_in),
        .I2(p_1_in5_in),
        .I3(s_axi_bvalid_reg_0),
        .O(s_axi_bvalid_i_1_n_0));
  FDRE s_axi_bvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(s_axi_bvalid_i_1_n_0),
        .Q(s_axi_bvalid_reg_0),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[0]_i_1 
       (.I0(RI_rdata[0]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[10]_i_1 
       (.I0(RI_rdata[10]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[11]_i_1 
       (.I0(RI_rdata[11]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[12]_i_1 
       (.I0(RI_rdata[12]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[13]_i_1 
       (.I0(RI_rdata[13]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[14]_i_1 
       (.I0(RI_rdata[14]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[15]_i_1 
       (.I0(RI_rdata[15]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[16]_i_1 
       (.I0(RI_rdata[16]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[17]_i_1 
       (.I0(RI_rdata[17]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[18]_i_1 
       (.I0(RI_rdata[18]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[19]_i_1 
       (.I0(RI_rdata[19]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[1]_i_1 
       (.I0(RI_rdata[1]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[20]_i_1 
       (.I0(RI_rdata[20]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[21]_i_1 
       (.I0(RI_rdata[21]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[22]_i_1 
       (.I0(RI_rdata[22]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[23]_i_1 
       (.I0(RI_rdata[23]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[24]_i_1 
       (.I0(RI_rdata[24]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[25]_i_1 
       (.I0(RI_rdata[25]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[26]_i_1 
       (.I0(RI_rdata[26]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[27]_i_1 
       (.I0(RI_rdata[27]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[28]_i_1 
       (.I0(RI_rdata[28]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[29]_i_1 
       (.I0(RI_rdata[29]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[2]_i_1 
       (.I0(RI_rdata[2]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[30]_i_1 
       (.I0(RI_rdata[30]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[30]));
  LUT3 #(
    .INIT(8'h4F)) 
    \s_axi_rdata[31]_i_1 
       (.I0(Q),
        .I1(p_1_in9_in),
        .I2(resetn),
        .O(\s_axi_rdata[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \s_axi_rdata[31]_i_2 
       (.I0(s_axi_rready),
        .I1(Q),
        .O(\s_axi_rdata[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[31]_i_3 
       (.I0(RI_rdata[31]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[3]_i_1 
       (.I0(RI_rdata[3]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[4]_i_1 
       (.I0(RI_rdata[4]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[5]_i_1 
       (.I0(RI_rdata[5]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[6]_i_1 
       (.I0(RI_rdata[6]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[7]_i_1 
       (.I0(RI_rdata[7]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[8]_i_1 
       (.I0(RI_rdata[8]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \s_axi_rdata[9]_i_1 
       (.I0(RI_rdata[9]),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(p_1_in[9]));
  FDRE \s_axi_rdata_reg[0] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[0]),
        .Q(s_axi_rdata[0]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[10] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[10]),
        .Q(s_axi_rdata[10]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[11] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[11]),
        .Q(s_axi_rdata[11]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[12] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[12]),
        .Q(s_axi_rdata[12]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[13] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[13]),
        .Q(s_axi_rdata[13]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[14] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[14]),
        .Q(s_axi_rdata[14]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[15] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[15]),
        .Q(s_axi_rdata[15]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[16] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[16]),
        .Q(s_axi_rdata[16]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[17] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[17]),
        .Q(s_axi_rdata[17]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[18] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[18]),
        .Q(s_axi_rdata[18]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[19] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[19]),
        .Q(s_axi_rdata[19]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[1] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[1]),
        .Q(s_axi_rdata[1]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[20] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[20]),
        .Q(s_axi_rdata[20]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[21] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[21]),
        .Q(s_axi_rdata[21]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[22] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[22]),
        .Q(s_axi_rdata[22]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[23] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[23]),
        .Q(s_axi_rdata[23]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[24] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[24]),
        .Q(s_axi_rdata[24]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[25] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[25]),
        .Q(s_axi_rdata[25]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[26] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[26]),
        .Q(s_axi_rdata[26]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[27] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[27]),
        .Q(s_axi_rdata[27]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[28] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[28]),
        .Q(s_axi_rdata[28]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[29] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[29]),
        .Q(s_axi_rdata[29]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[2] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[2]),
        .Q(s_axi_rdata[2]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[30] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[30]),
        .Q(s_axi_rdata[30]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[31] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[31]),
        .Q(s_axi_rdata[31]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[3] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[3]),
        .Q(s_axi_rdata[3]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[4] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[4]),
        .Q(s_axi_rdata[4]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[5] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[5]),
        .Q(s_axi_rdata[5]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[6] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[6]),
        .Q(s_axi_rdata[6]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[7] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[7]),
        .Q(s_axi_rdata[7]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[8] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[8]),
        .Q(s_axi_rdata[8]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  FDRE \s_axi_rdata_reg[9] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(p_1_in[9]),
        .Q(s_axi_rdata[9]),
        .R(\s_axi_rdata[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0E0A)) 
    s_axi_rlast_i_1
       (.I0(p_1_in9_in),
        .I1(RI_rvalid),
        .I2(Q),
        .I3(p_0_in11_in),
        .O(s_axi_rlast_i_1_n_0));
  FDRE s_axi_rlast_reg
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(s_axi_rlast_i_1_n_0),
        .Q(s_axi_rlast),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \s_axi_rresp[1]_i_1 
       (.I0(p_1_in9_in),
        .I1(Q),
        .O(s_axi_rdata0));
  FDRE \s_axi_rresp_reg[1] 
       (.C(clk),
        .CE(\s_axi_rdata[31]_i_2_n_0 ),
        .D(s_axi_rdata0),
        .Q(s_axi_rresp),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h5F5F5C50)) 
    s_axi_rvalid_i_1
       (.I0(s_axi_rready),
        .I1(p_0_in11_in),
        .I2(Q),
        .I3(RI_rvalid),
        .I4(p_1_in9_in),
        .O(s_axi_rvalid_i_1_n_0));
  FDRE s_axi_rvalid_reg
       (.C(clk),
        .CE(1'b1),
        .D(s_axi_rvalid_i_1_n_0),
        .Q(Q),
        .R(s_axi_arready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h00E0)) 
    s_axi_wready_i_1
       (.I0(p_1_in_0),
        .I1(\FSM_onehot_state_reg_n_0_[5] ),
        .I2(s_axi_wvalid),
        .I3(s_axi_wready),
        .O(s_axi_wready0));
  FDRE s_axi_wready_reg
       (.C(clk),
        .CE(1'b1),
        .D(s_axi_wready0),
        .Q(s_axi_wready),
        .R(s_axi_arready_i_1_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
