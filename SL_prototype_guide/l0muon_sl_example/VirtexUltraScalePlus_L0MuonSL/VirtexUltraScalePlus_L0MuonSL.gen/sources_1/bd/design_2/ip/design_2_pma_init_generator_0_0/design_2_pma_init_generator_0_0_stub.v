// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Mon Oct 31 19:22:47 2022
// Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/amishima/vivado_work/CERN_prework/FPGA/project_1/project_1.gen/sources_1/bd/design_2/ip/design_2_pma_init_generator_0_0/design_2_pma_init_generator_0_0_stub.v
// Design      : design_2_pma_init_generator_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xcvu13p-flga2577-1-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "pma_init_generator,Vivado 2020.2" *)
module design_2_pma_init_generator_0_0(aur_init_clk, resetn, FPGARSTB, pma_init_in)
/* synthesis syn_black_box black_box_pad_pin="aur_init_clk,resetn,FPGARSTB,pma_init_in" */;
  input aur_init_clk;
  input resetn;
  input FPGARSTB;
  output pma_init_in;
endmodule
