-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Fri Nov 25 19:13:31 2022
-- Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/amishima/vivado_work/l0muon_sl_example/VirtexUltraScalePlus_L0MuonSL/VirtexUltraScalePlus_L0MuonSL.gen/sources_1/bd/design_2/ip/design_2_AXI_Translator_0_0/design_2_AXI_Translator_0_0_sim_netlist.vhdl
-- Design      : design_2_AXI_Translator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcvu13p-flga2577-1-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_2_AXI_Translator_0_0_AXI_Translator is
  port (
    Q : out STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid_reg_0 : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RI_mode : out STD_LOGIC;
    RI_addr : out STD_LOGIC_VECTOR ( 13 downto 0 );
    RI_avalid : out STD_LOGIC;
    RI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RI_wvalid : out STD_LOGIC;
    s_axi_rlast : out STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 0 to 0 );
    resetn : in STD_LOGIC;
    clk : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    RI_rvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    RI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_2_AXI_Translator_0_0_AXI_Translator : entity is "AXI_Translator";
end design_2_AXI_Translator_0_0_AXI_Translator;

architecture STRUCTURE of design_2_AXI_Translator_0_0_AXI_Translator is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[5]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC;
  signal \RI_addr[0]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[0]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[12]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[12]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[13]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[13]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[14]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[14]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[15]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[15]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[15]_i_3_n_0\ : STD_LOGIC;
  signal \RI_addr[15]_i_4_n_0\ : STD_LOGIC;
  signal \RI_addr[15]_i_5_n_0\ : STD_LOGIC;
  signal \RI_addr[15]_i_6_n_0\ : STD_LOGIC;
  signal \RI_addr[1]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[1]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[2]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[2]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[3]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[3]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[4]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[4]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[5]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[5]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[6]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[6]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[7]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[7]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[8]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[8]_i_2_n_0\ : STD_LOGIC;
  signal \RI_addr[9]_i_1_n_0\ : STD_LOGIC;
  signal \RI_addr[9]_i_2_n_0\ : STD_LOGIC;
  signal RI_avalid_i_1_n_0 : STD_LOGIC;
  signal RI_avalid_i_2_n_0 : STD_LOGIC;
  signal RI_avalid_i_3_n_0 : STD_LOGIC;
  signal RI_mode_i_10_n_0 : STD_LOGIC;
  signal RI_mode_i_11_n_0 : STD_LOGIC;
  signal RI_mode_i_12_n_0 : STD_LOGIC;
  signal RI_mode_i_1_n_0 : STD_LOGIC;
  signal RI_mode_i_2_n_0 : STD_LOGIC;
  signal RI_mode_i_3_n_0 : STD_LOGIC;
  signal RI_mode_i_4_n_0 : STD_LOGIC;
  signal RI_mode_i_5_n_0 : STD_LOGIC;
  signal RI_mode_i_6_n_0 : STD_LOGIC;
  signal RI_mode_i_7_n_0 : STD_LOGIC;
  signal RI_mode_i_8_n_0 : STD_LOGIC;
  signal RI_mode_i_9_n_0 : STD_LOGIC;
  signal \RI_wdata[0]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[10]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[11]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[12]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[13]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[14]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[15]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[16]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[17]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[18]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[19]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[1]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[20]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[21]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[22]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[23]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[24]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[25]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[26]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[27]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[28]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[29]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[2]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[30]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[3]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[4]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[5]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[6]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[7]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[8]_i_1_n_0\ : STD_LOGIC;
  signal \RI_wdata[9]_i_1_n_0\ : STD_LOGIC;
  signal RI_wvalid_i_1_n_0 : STD_LOGIC;
  signal p_0_in11_in : STD_LOGIC;
  signal p_0_in7_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_1_in5_in : STD_LOGIC;
  signal p_1_in9_in : STD_LOGIC;
  signal p_1_in_0 : STD_LOGIC;
  signal \^s_axi_arready\ : STD_LOGIC;
  signal s_axi_arready0 : STD_LOGIC;
  signal s_axi_arready_i_1_n_0 : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal s_axi_awready0 : STD_LOGIC;
  signal s_axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \^s_axi_bvalid_reg_0\ : STD_LOGIC;
  signal s_axi_rdata0 : STD_LOGIC;
  signal \s_axi_rdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \s_axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal s_axi_rlast_i_1_n_0 : STD_LOGIC;
  signal s_axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal s_axi_wready0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[2]_i_2\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \FSM_onehot_state[2]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_2\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \FSM_onehot_state[6]_i_2\ : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[4]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[5]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[6]\ : label is "STATE_READ:1000000,STATE_WRITE:0100000,STATE_RESP:0010000,STATE_READ_ERROR:0001000,STATE_WRITE_ERROR:0000100,STATE_RESP_ERROR:0000001,STATE_IDLE:0000010";
  attribute SOFT_HLUTNM of \RI_addr[12]_i_2\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \RI_addr[13]_i_2\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \RI_addr[14]_i_2\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \RI_addr[15]_i_2\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \RI_addr[15]_i_4\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \RI_addr[15]_i_5\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \RI_addr[15]_i_6\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \RI_addr[1]_i_2\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \RI_addr[2]_i_2\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \RI_addr[3]_i_2\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \RI_addr[4]_i_2\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \RI_addr[5]_i_2\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \RI_addr[6]_i_2\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \RI_addr[7]_i_2\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \RI_addr[8]_i_2\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \RI_addr[9]_i_2\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of RI_avalid_i_2 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of RI_mode_i_12 : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of RI_mode_i_3 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of RI_mode_i_4 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of RI_mode_i_5 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of RI_mode_i_7 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \RI_wdata[0]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \RI_wdata[10]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \RI_wdata[11]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \RI_wdata[12]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \RI_wdata[13]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \RI_wdata[14]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \RI_wdata[15]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \RI_wdata[16]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \RI_wdata[17]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \RI_wdata[18]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \RI_wdata[19]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \RI_wdata[1]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \RI_wdata[20]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \RI_wdata[21]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \RI_wdata[22]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \RI_wdata[23]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \RI_wdata[24]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \RI_wdata[25]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \RI_wdata[26]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \RI_wdata[27]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \RI_wdata[28]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \RI_wdata[29]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \RI_wdata[2]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \RI_wdata[30]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \RI_wdata[31]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \RI_wdata[3]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \RI_wdata[4]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \RI_wdata[5]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \RI_wdata[6]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \RI_wdata[7]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \RI_wdata[8]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \RI_wdata[9]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of RI_wvalid_i_1 : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of s_axi_arready_i_2 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of s_axi_awready_i_1 : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of s_axi_bvalid_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \s_axi_rdata[0]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \s_axi_rdata[10]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_axi_rdata[11]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_axi_rdata[12]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_axi_rdata[13]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_axi_rdata[14]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_axi_rdata[15]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_axi_rdata[16]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_axi_rdata[17]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_axi_rdata[18]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axi_rdata[19]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axi_rdata[1]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \s_axi_rdata[20]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axi_rdata[21]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axi_rdata[22]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axi_rdata[23]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axi_rdata[24]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axi_rdata[25]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axi_rdata[26]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \s_axi_rdata[27]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \s_axi_rdata[28]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \s_axi_rdata[29]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \s_axi_rdata[2]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \s_axi_rdata[30]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axi_rdata[31]_i_3\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axi_rdata[3]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \s_axi_rdata[4]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \s_axi_rdata[5]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \s_axi_rdata[6]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axi_rdata[7]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axi_rdata[8]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \s_axi_rdata[9]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of s_axi_rlast_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \s_axi_rresp[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of s_axi_rvalid_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of s_axi_wready_i_1 : label is "soft_lutpair40";
begin
  Q <= \^q\;
  s_axi_arready <= \^s_axi_arready\;
  s_axi_awready <= \^s_axi_awready\;
  s_axi_bvalid_reg_0 <= \^s_axi_bvalid_reg_0\;
  s_axi_wready <= \^s_axi_wready\;
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FFFFFF80808080"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => s_axi_wvalid,
      I2 => p_1_in_0,
      I3 => s_axi_bready,
      I4 => \^s_axi_bvalid_reg_0\,
      I5 => p_0_in7_in,
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00707070"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \^s_axi_awready\,
      I4 => s_axi_awvalid,
      I5 => \FSM_onehot_state[1]_i_2_n_0\,
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF40FF40FF404040"
    )
        port map (
      I0 => RI_mode_i_3_n_0,
      I1 => s_axi_bready,
      I2 => \^s_axi_bvalid_reg_0\,
      I3 => \FSM_onehot_state[6]_i_2_n_0\,
      I4 => p_1_in9_in,
      I5 => p_0_in11_in,
      O => \FSM_onehot_state[1]_i_2_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F4444444F444F44"
    )
        port map (
      I0 => \FSM_onehot_state[2]_i_2_n_0\,
      I1 => p_1_in_0,
      I2 => \FSM_onehot_state[2]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => RI_mode_i_5_n_0,
      I5 => RI_mode_i_4_n_0,
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => s_axi_wvalid,
      O => \FSM_onehot_state[2]_i_2_n_0\
    );
\FSM_onehot_state[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F7F7F"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => \^s_axi_awready\,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \^s_axi_arready\,
      I4 => s_axi_arvalid,
      O => \FSM_onehot_state[2]_i_3_n_0\
    );
\FSM_onehot_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F4444444F444F44"
    )
        port map (
      I0 => \FSM_onehot_state[6]_i_2_n_0\,
      I1 => p_1_in9_in,
      I2 => \RI_addr[15]_i_4_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => RI_mode_i_5_n_0,
      I5 => RI_mode_i_4_n_0,
      O => \FSM_onehot_state[3]_i_1_n_0\
    );
\FSM_onehot_state[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FFFFFF80808080"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => s_axi_wvalid,
      I2 => \FSM_onehot_state_reg_n_0_[5]\,
      I3 => s_axi_bready,
      I4 => \^s_axi_bvalid_reg_0\,
      I5 => p_1_in5_in,
      O => \FSM_onehot_state[4]_i_1_n_0\
    );
\FSM_onehot_state[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF000008AA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => RI_mode_i_4_n_0,
      I2 => RI_mode_i_5_n_0,
      I3 => RI_mode_i_6_n_0,
      I4 => RI_mode_i_7_n_0,
      I5 => \FSM_onehot_state[5]_i_2_n_0\,
      O => \FSM_onehot_state[5]_i_1_n_0\
    );
\FSM_onehot_state[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => s_axi_wvalid,
      I2 => \^s_axi_wready\,
      O => \FSM_onehot_state[5]_i_2_n_0\
    );
\FSM_onehot_state[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"444F4F4F444F444F"
    )
        port map (
      I0 => \FSM_onehot_state[6]_i_2_n_0\,
      I1 => p_0_in11_in,
      I2 => \RI_addr[15]_i_4_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => RI_mode_i_5_n_0,
      I5 => RI_mode_i_4_n_0,
      O => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s_axi_rready,
      I1 => \^q\,
      O => \FSM_onehot_state[6]_i_2_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => p_0_in7_in,
      R => s_axi_arready_i_1_n_0
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[1]\,
      S => s_axi_arready_i_1_n_0
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => p_1_in_0,
      R => s_axi_arready_i_1_n_0
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[3]_i_1_n_0\,
      Q => p_1_in9_in,
      R => s_axi_arready_i_1_n_0
    );
\FSM_onehot_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[4]_i_1_n_0\,
      Q => p_1_in5_in,
      R => s_axi_arready_i_1_n_0
    );
\FSM_onehot_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[5]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[5]\,
      R => s_axi_arready_i_1_n_0
    );
\FSM_onehot_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \FSM_onehot_state[6]_i_1_n_0\,
      Q => p_0_in11_in,
      R => s_axi_arready_i_1_n_0
    );
\RI_addr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[0]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(0),
      O => \RI_addr[0]_i_1_n_0\
    );
\RI_addr[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(0),
      O => \RI_addr[0]_i_2_n_0\
    );
\RI_addr[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[12]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(10),
      O => \RI_addr[12]_i_1_n_0\
    );
\RI_addr[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(10),
      O => \RI_addr[12]_i_2_n_0\
    );
\RI_addr[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[13]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(11),
      O => \RI_addr[13]_i_1_n_0\
    );
\RI_addr[13]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(11),
      O => \RI_addr[13]_i_2_n_0\
    );
\RI_addr[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[14]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(12),
      O => \RI_addr[14]_i_1_n_0\
    );
\RI_addr[14]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(12),
      O => \RI_addr[14]_i_2_n_0\
    );
\RI_addr[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[15]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(13),
      O => \RI_addr[15]_i_1_n_0\
    );
\RI_addr[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(13),
      O => \RI_addr[15]_i_2_n_0\
    );
\RI_addr[15]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000E"
    )
        port map (
      I0 => \RI_addr[15]_i_5_n_0\,
      I1 => RI_mode_i_9_n_0,
      I2 => RI_mode_i_10_n_0,
      I3 => \RI_addr[15]_i_6_n_0\,
      O => \RI_addr[15]_i_3_n_0\
    );
\RI_addr[15]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_arready\,
      O => \RI_addr[15]_i_4_n_0\
    );
\RI_addr[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"557F"
    )
        port map (
      I0 => s_axi_araddr(6),
      I1 => s_axi_araddr(5),
      I2 => s_axi_araddr(11),
      I3 => s_axi_araddr(7),
      O => \RI_addr[15]_i_5_n_0\
    );
\RI_addr[15]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF8F"
    )
        port map (
      I0 => s_axi_araddr(11),
      I1 => s_axi_araddr(7),
      I2 => s_axi_arvalid,
      I3 => s_axi_araddr(10),
      O => \RI_addr[15]_i_6_n_0\
    );
\RI_addr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[1]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(1),
      O => \RI_addr[1]_i_1_n_0\
    );
\RI_addr[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(1),
      O => \RI_addr[1]_i_2_n_0\
    );
\RI_addr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[2]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(2),
      O => \RI_addr[2]_i_1_n_0\
    );
\RI_addr[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(2),
      O => \RI_addr[2]_i_2_n_0\
    );
\RI_addr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[3]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(3),
      O => \RI_addr[3]_i_1_n_0\
    );
\RI_addr[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(3),
      O => \RI_addr[3]_i_2_n_0\
    );
\RI_addr[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[4]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(4),
      O => \RI_addr[4]_i_1_n_0\
    );
\RI_addr[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(4),
      O => \RI_addr[4]_i_2_n_0\
    );
\RI_addr[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[5]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(5),
      O => \RI_addr[5]_i_1_n_0\
    );
\RI_addr[5]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(5),
      O => \RI_addr[5]_i_2_n_0\
    );
\RI_addr[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[6]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(6),
      O => \RI_addr[6]_i_1_n_0\
    );
\RI_addr[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(6),
      O => \RI_addr[6]_i_2_n_0\
    );
\RI_addr[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[7]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(7),
      O => \RI_addr[7]_i_1_n_0\
    );
\RI_addr[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(7),
      O => \RI_addr[7]_i_2_n_0\
    );
\RI_addr[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[8]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(8),
      O => \RI_addr[8]_i_1_n_0\
    );
\RI_addr[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(8),
      O => \RI_addr[8]_i_2_n_0\
    );
\RI_addr[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1011F0FF10111011"
    )
        port map (
      I0 => \RI_addr[9]_i_2_n_0\,
      I1 => RI_mode_i_7_n_0,
      I2 => \RI_addr[15]_i_3_n_0\,
      I3 => RI_mode_i_6_n_0,
      I4 => \RI_addr[15]_i_4_n_0\,
      I5 => s_axi_araddr(9),
      O => \RI_addr[9]_i_1_n_0\
    );
\RI_addr[9]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awaddr(9),
      O => \RI_addr[9]_i_2_n_0\
    );
\RI_addr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[0]_i_1_n_0\,
      Q => RI_addr(0),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[12]_i_1_n_0\,
      Q => RI_addr(10),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[13]_i_1_n_0\,
      Q => RI_addr(11),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[14]_i_1_n_0\,
      Q => RI_addr(12),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[15]_i_1_n_0\,
      Q => RI_addr(13),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[1]_i_1_n_0\,
      Q => RI_addr(1),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[2]_i_1_n_0\,
      Q => RI_addr(2),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[3]_i_1_n_0\,
      Q => RI_addr(3),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[4]_i_1_n_0\,
      Q => RI_addr(4),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[5]_i_1_n_0\,
      Q => RI_addr(5),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[6]_i_1_n_0\,
      Q => RI_addr(6),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[7]_i_1_n_0\,
      Q => RI_addr(7),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[8]_i_1_n_0\,
      Q => RI_addr(8),
      R => s_axi_arready_i_1_n_0
    );
\RI_addr_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_addr[9]_i_1_n_0\,
      Q => RI_addr(9),
      R => s_axi_arready_i_1_n_0
    );
RI_avalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00C4C4C400C400C4"
    )
        port map (
      I0 => RI_avalid_i_2_n_0,
      I1 => \FSM_onehot_state_reg_n_0_[1]\,
      I2 => RI_avalid_i_3_n_0,
      I3 => RI_mode_i_6_n_0,
      I4 => RI_mode_i_5_n_0,
      I5 => RI_mode_i_4_n_0,
      O => RI_avalid_i_1_n_0
    );
RI_avalid_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => s_axi_awvalid,
      O => RI_avalid_i_2_n_0
    );
RI_avalid_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s_axi_arvalid,
      O => RI_avalid_i_3_n_0
    );
RI_avalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => RI_avalid_i_1_n_0,
      Q => RI_avalid,
      R => s_axi_arready_i_1_n_0
    );
RI_mode_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => p_1_in9_in,
      I1 => p_0_in11_in,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => RI_mode_i_3_n_0,
      I4 => \FSM_onehot_state_reg_n_0_[5]\,
      I5 => p_1_in_0,
      O => RI_mode_i_1_n_0
    );
RI_mode_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => s_axi_araddr(12),
      I1 => s_axi_araddr(8),
      I2 => s_axi_araddr(13),
      I3 => s_axi_araddr(9),
      O => RI_mode_i_10_n_0
    );
RI_mode_i_11: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE000000"
    )
        port map (
      I0 => s_axi_awaddr(4),
      I1 => s_axi_awaddr(3),
      I2 => s_axi_awaddr(5),
      I3 => s_axi_awaddr(6),
      I4 => s_axi_awaddr(7),
      O => RI_mode_i_11_n_0
    );
RI_mode_i_12: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => s_axi_awaddr(10),
      I1 => s_axi_awaddr(13),
      I2 => s_axi_awvalid,
      I3 => s_axi_awaddr(11),
      O => RI_mode_i_12_n_0
    );
RI_mode_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF000008AA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => RI_mode_i_4_n_0,
      I2 => RI_mode_i_5_n_0,
      I3 => RI_mode_i_6_n_0,
      I4 => RI_mode_i_7_n_0,
      I5 => RI_mode_i_8_n_0,
      O => RI_mode_i_2_n_0
    );
RI_mode_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => p_0_in7_in,
      I1 => p_1_in5_in,
      O => RI_mode_i_3_n_0
    );
RI_mode_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ABBBFFFF"
    )
        port map (
      I0 => RI_mode_i_9_n_0,
      I1 => s_axi_araddr(7),
      I2 => s_axi_araddr(11),
      I3 => s_axi_araddr(5),
      I4 => s_axi_araddr(6),
      O => RI_mode_i_4_n_0
    );
RI_mode_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBBB"
    )
        port map (
      I0 => s_axi_araddr(10),
      I1 => s_axi_arvalid,
      I2 => s_axi_araddr(7),
      I3 => s_axi_araddr(11),
      I4 => RI_mode_i_10_n_0,
      O => RI_mode_i_5_n_0
    );
RI_mode_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => RI_mode_i_11_n_0,
      I1 => RI_mode_i_12_n_0,
      I2 => s_axi_awaddr(12),
      I3 => s_axi_awaddr(8),
      I4 => s_axi_awaddr(9),
      O => RI_mode_i_6_n_0
    );
RI_mode_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8FFF"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => s_axi_awvalid,
      I3 => \^s_axi_awready\,
      O => RI_mode_i_7_n_0
    );
RI_mode_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => p_1_in5_in,
      I1 => \FSM_onehot_state_reg_n_0_[5]\,
      O => RI_mode_i_8_n_0
    );
RI_mode_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00011111"
    )
        port map (
      I0 => s_axi_araddr(3),
      I1 => s_axi_araddr(4),
      I2 => s_axi_araddr(2),
      I3 => s_axi_araddr(7),
      I4 => s_axi_araddr(5),
      O => RI_mode_i_9_n_0
    );
RI_mode_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => RI_mode_i_2_n_0,
      Q => RI_mode,
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[0]_i_1_n_0\
    );
\RI_wdata[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(10),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[10]_i_1_n_0\
    );
\RI_wdata[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(11),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[11]_i_1_n_0\
    );
\RI_wdata[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(12),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[12]_i_1_n_0\
    );
\RI_wdata[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(13),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[13]_i_1_n_0\
    );
\RI_wdata[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(14),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[14]_i_1_n_0\
    );
\RI_wdata[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(15),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[15]_i_1_n_0\
    );
\RI_wdata[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(16),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[16]_i_1_n_0\
    );
\RI_wdata[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(17),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[17]_i_1_n_0\
    );
\RI_wdata[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(18),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[18]_i_1_n_0\
    );
\RI_wdata[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(19),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[19]_i_1_n_0\
    );
\RI_wdata[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(1),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[1]_i_1_n_0\
    );
\RI_wdata[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(20),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[20]_i_1_n_0\
    );
\RI_wdata[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(21),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[21]_i_1_n_0\
    );
\RI_wdata[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(22),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[22]_i_1_n_0\
    );
\RI_wdata[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(23),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[23]_i_1_n_0\
    );
\RI_wdata[24]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(24),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[24]_i_1_n_0\
    );
\RI_wdata[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(25),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[25]_i_1_n_0\
    );
\RI_wdata[26]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(26),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[26]_i_1_n_0\
    );
\RI_wdata[27]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(27),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[27]_i_1_n_0\
    );
\RI_wdata[28]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(28),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[28]_i_1_n_0\
    );
\RI_wdata[29]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(29),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[29]_i_1_n_0\
    );
\RI_wdata[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(2),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[2]_i_1_n_0\
    );
\RI_wdata[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(30),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[30]_i_1_n_0\
    );
\RI_wdata[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(31),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[31]_i_1_n_0\
    );
\RI_wdata[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(3),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[3]_i_1_n_0\
    );
\RI_wdata[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(4),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[4]_i_1_n_0\
    );
\RI_wdata[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(5),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[5]_i_1_n_0\
    );
\RI_wdata[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(6),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[6]_i_1_n_0\
    );
\RI_wdata[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(7),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[7]_i_1_n_0\
    );
\RI_wdata[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(8),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[8]_i_1_n_0\
    );
\RI_wdata[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => s_axi_wdata(9),
      I1 => \^s_axi_wready\,
      I2 => s_axi_wvalid,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      O => \RI_wdata[9]_i_1_n_0\
    );
\RI_wdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[0]_i_1_n_0\,
      Q => RI_wdata(0),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[10]_i_1_n_0\,
      Q => RI_wdata(10),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[11]_i_1_n_0\,
      Q => RI_wdata(11),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[12]_i_1_n_0\,
      Q => RI_wdata(12),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[13]_i_1_n_0\,
      Q => RI_wdata(13),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[14]_i_1_n_0\,
      Q => RI_wdata(14),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[15]_i_1_n_0\,
      Q => RI_wdata(15),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[16]_i_1_n_0\,
      Q => RI_wdata(16),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[17]_i_1_n_0\,
      Q => RI_wdata(17),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[18]_i_1_n_0\,
      Q => RI_wdata(18),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[19]_i_1_n_0\,
      Q => RI_wdata(19),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[1]_i_1_n_0\,
      Q => RI_wdata(1),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[20]_i_1_n_0\,
      Q => RI_wdata(20),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[21]_i_1_n_0\,
      Q => RI_wdata(21),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[22]_i_1_n_0\,
      Q => RI_wdata(22),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[23]_i_1_n_0\,
      Q => RI_wdata(23),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[24]_i_1_n_0\,
      Q => RI_wdata(24),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[25]_i_1_n_0\,
      Q => RI_wdata(25),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[26]_i_1_n_0\,
      Q => RI_wdata(26),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[27]_i_1_n_0\,
      Q => RI_wdata(27),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[28]_i_1_n_0\,
      Q => RI_wdata(28),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[29]_i_1_n_0\,
      Q => RI_wdata(29),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[2]_i_1_n_0\,
      Q => RI_wdata(2),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[30]_i_1_n_0\,
      Q => RI_wdata(30),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[31]_i_1_n_0\,
      Q => RI_wdata(31),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[3]_i_1_n_0\,
      Q => RI_wdata(3),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[4]_i_1_n_0\,
      Q => RI_wdata(4),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[5]_i_1_n_0\,
      Q => RI_wdata(5),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[6]_i_1_n_0\,
      Q => RI_wdata(6),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[7]_i_1_n_0\,
      Q => RI_wdata(7),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[8]_i_1_n_0\,
      Q => RI_wdata(8),
      R => s_axi_arready_i_1_n_0
    );
\RI_wdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => \RI_wdata[9]_i_1_n_0\,
      Q => RI_wdata(9),
      R => s_axi_arready_i_1_n_0
    );
RI_wvalid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => s_axi_wvalid,
      I2 => \^s_axi_wready\,
      O => RI_wvalid_i_1_n_0
    );
RI_wvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => RI_mode_i_1_n_0,
      D => RI_wvalid_i_1_n_0,
      Q => RI_wvalid,
      R => s_axi_arready_i_1_n_0
    );
s_axi_arready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => s_axi_arready_i_1_n_0
    );
s_axi_arready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_arvalid,
      I2 => \^s_axi_arready\,
      O => s_axi_arready0
    );
s_axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => s_axi_arready0,
      Q => \^s_axi_arready\,
      R => s_axi_arready_i_1_n_0
    );
s_axi_awready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => s_axi_awvalid,
      I2 => \^s_axi_awready\,
      O => s_axi_awready0
    );
s_axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => s_axi_awready0,
      Q => \^s_axi_awready\,
      R => s_axi_arready_i_1_n_0
    );
s_axi_bvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"55FC"
    )
        port map (
      I0 => s_axi_bready,
      I1 => p_0_in7_in,
      I2 => p_1_in5_in,
      I3 => \^s_axi_bvalid_reg_0\,
      O => s_axi_bvalid_i_1_n_0
    );
s_axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => s_axi_bvalid_i_1_n_0,
      Q => \^s_axi_bvalid_reg_0\,
      R => s_axi_arready_i_1_n_0
    );
\s_axi_rdata[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(0),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(0)
    );
\s_axi_rdata[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(10),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(10)
    );
\s_axi_rdata[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(11),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(11)
    );
\s_axi_rdata[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(12),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(12)
    );
\s_axi_rdata[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(13),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(13)
    );
\s_axi_rdata[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(14),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(14)
    );
\s_axi_rdata[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(15),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(15)
    );
\s_axi_rdata[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(16),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(16)
    );
\s_axi_rdata[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(17),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(17)
    );
\s_axi_rdata[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(18),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(18)
    );
\s_axi_rdata[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(19),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(19)
    );
\s_axi_rdata[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(1),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(1)
    );
\s_axi_rdata[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(20),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(20)
    );
\s_axi_rdata[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(21),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(21)
    );
\s_axi_rdata[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(22),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(22)
    );
\s_axi_rdata[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(23),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(23)
    );
\s_axi_rdata[24]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(24),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(24)
    );
\s_axi_rdata[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(25),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(25)
    );
\s_axi_rdata[26]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(26),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(26)
    );
\s_axi_rdata[27]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(27),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(27)
    );
\s_axi_rdata[28]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(28),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(28)
    );
\s_axi_rdata[29]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(29),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(29)
    );
\s_axi_rdata[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(2),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(2)
    );
\s_axi_rdata[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(30),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(30)
    );
\s_axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \^q\,
      I1 => p_1_in9_in,
      I2 => resetn,
      O => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => s_axi_rready,
      I1 => \^q\,
      O => \s_axi_rdata[31]_i_2_n_0\
    );
\s_axi_rdata[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(31),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(31)
    );
\s_axi_rdata[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(3),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(3)
    );
\s_axi_rdata[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(4),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(4)
    );
\s_axi_rdata[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(5),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(5)
    );
\s_axi_rdata[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(6),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(6)
    );
\s_axi_rdata[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(7),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(7)
    );
\s_axi_rdata[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(8),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(8)
    );
\s_axi_rdata[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => RI_rdata(9),
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => p_1_in(9)
    );
\s_axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(0),
      Q => s_axi_rdata(0),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(10),
      Q => s_axi_rdata(10),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(11),
      Q => s_axi_rdata(11),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(12),
      Q => s_axi_rdata(12),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(13),
      Q => s_axi_rdata(13),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(14),
      Q => s_axi_rdata(14),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(15),
      Q => s_axi_rdata(15),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(16),
      Q => s_axi_rdata(16),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(17),
      Q => s_axi_rdata(17),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(18),
      Q => s_axi_rdata(18),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(19),
      Q => s_axi_rdata(19),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(1),
      Q => s_axi_rdata(1),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(20),
      Q => s_axi_rdata(20),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(21),
      Q => s_axi_rdata(21),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(22),
      Q => s_axi_rdata(22),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(23),
      Q => s_axi_rdata(23),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(24),
      Q => s_axi_rdata(24),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(25),
      Q => s_axi_rdata(25),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(26),
      Q => s_axi_rdata(26),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(27),
      Q => s_axi_rdata(27),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(28),
      Q => s_axi_rdata(28),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(29),
      Q => s_axi_rdata(29),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(2),
      Q => s_axi_rdata(2),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(30),
      Q => s_axi_rdata(30),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(31),
      Q => s_axi_rdata(31),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(3),
      Q => s_axi_rdata(3),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(4),
      Q => s_axi_rdata(4),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(5),
      Q => s_axi_rdata(5),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(6),
      Q => s_axi_rdata(6),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(7),
      Q => s_axi_rdata(7),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(8),
      Q => s_axi_rdata(8),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
\s_axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => p_1_in(9),
      Q => s_axi_rdata(9),
      R => \s_axi_rdata[31]_i_1_n_0\
    );
s_axi_rlast_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E0A"
    )
        port map (
      I0 => p_1_in9_in,
      I1 => RI_rvalid,
      I2 => \^q\,
      I3 => p_0_in11_in,
      O => s_axi_rlast_i_1_n_0
    );
s_axi_rlast_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => s_axi_rlast_i_1_n_0,
      Q => s_axi_rlast,
      R => s_axi_arready_i_1_n_0
    );
\s_axi_rresp[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_1_in9_in,
      I1 => \^q\,
      O => s_axi_rdata0
    );
\s_axi_rresp_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \s_axi_rdata[31]_i_2_n_0\,
      D => s_axi_rdata0,
      Q => s_axi_rresp(0),
      R => s_axi_arready_i_1_n_0
    );
s_axi_rvalid_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5F5F5C50"
    )
        port map (
      I0 => s_axi_rready,
      I1 => p_0_in11_in,
      I2 => \^q\,
      I3 => RI_rvalid,
      I4 => p_1_in9_in,
      O => s_axi_rvalid_i_1_n_0
    );
s_axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => s_axi_rvalid_i_1_n_0,
      Q => \^q\,
      R => s_axi_arready_i_1_n_0
    );
s_axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E0"
    )
        port map (
      I0 => p_1_in_0,
      I1 => \FSM_onehot_state_reg_n_0_[5]\,
      I2 => s_axi_wvalid,
      I3 => \^s_axi_wready\,
      O => s_axi_wready0
    );
s_axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => s_axi_wready0,
      Q => \^s_axi_wready\,
      R => s_axi_arready_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_2_AXI_Translator_0_0 is
  port (
    clk : in STD_LOGIC;
    resetn : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wuser : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    RI_mode : out STD_LOGIC;
    RI_addr : out STD_LOGIC_VECTOR ( 15 downto 0 );
    RI_avalid : out STD_LOGIC;
    RI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    RI_wvalid : out STD_LOGIC;
    RI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    RI_rvalid : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_2_AXI_Translator_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_2_AXI_Translator_0_0 : entity is "design_2_AXI_Translator_0_0,AXI_Translator,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_2_AXI_Translator_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of design_2_AXI_Translator_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_2_AXI_Translator_0_0 : entity is "AXI_Translator,Vivado 2020.2";
end design_2_AXI_Translator_0_0;

architecture STRUCTURE of design_2_AXI_Translator_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^ri_addr\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^s_axi_rresp\ : STD_LOGIC_VECTOR ( 1 to 1 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_BUSIF s_axi, ASSOCIATED_RESET resetn, FREQ_HZ 125000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, CLK_DOMAIN design_2_CLK125, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of resetn : signal is "xilinx.com:signal:reset:1.0 resetn RST";
  attribute X_INTERFACE_PARAMETER of resetn : signal is "XIL_INTERFACENAME resetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi ARREADY";
  attribute X_INTERFACE_INFO of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi ARVALID";
  attribute X_INTERFACE_INFO of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi AWREADY";
  attribute X_INTERFACE_INFO of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi AWVALID";
  attribute X_INTERFACE_INFO of s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi BREADY";
  attribute X_INTERFACE_INFO of s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi BVALID";
  attribute X_INTERFACE_INFO of s_axi_rlast : signal is "xilinx.com:interface:aximm:1.0 s_axi RLAST";
  attribute X_INTERFACE_INFO of s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi RREADY";
  attribute X_INTERFACE_INFO of s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi RVALID";
  attribute X_INTERFACE_INFO of s_axi_wlast : signal is "xilinx.com:interface:aximm:1.0 s_axi WLAST";
  attribute X_INTERFACE_INFO of s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi WREADY";
  attribute X_INTERFACE_INFO of s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi WVALID";
  attribute X_INTERFACE_PARAMETER of s_axi_wvalid : signal is "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 125000000, ID_WIDTH 12, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 4, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0.000, CLK_DOMAIN design_2_CLK125, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi ARADDR";
  attribute X_INTERFACE_INFO of s_axi_arburst : signal is "xilinx.com:interface:aximm:1.0 s_axi ARBURST";
  attribute X_INTERFACE_INFO of s_axi_arid : signal is "xilinx.com:interface:aximm:1.0 s_axi ARID";
  attribute X_INTERFACE_INFO of s_axi_arlen : signal is "xilinx.com:interface:aximm:1.0 s_axi ARLEN";
  attribute X_INTERFACE_INFO of s_axi_arsize : signal is "xilinx.com:interface:aximm:1.0 s_axi ARSIZE";
  attribute X_INTERFACE_INFO of s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi AWADDR";
  attribute X_INTERFACE_INFO of s_axi_awburst : signal is "xilinx.com:interface:aximm:1.0 s_axi AWBURST";
  attribute X_INTERFACE_INFO of s_axi_awid : signal is "xilinx.com:interface:aximm:1.0 s_axi AWID";
  attribute X_INTERFACE_INFO of s_axi_awlen : signal is "xilinx.com:interface:aximm:1.0 s_axi AWLEN";
  attribute X_INTERFACE_INFO of s_axi_awsize : signal is "xilinx.com:interface:aximm:1.0 s_axi AWSIZE";
  attribute X_INTERFACE_INFO of s_axi_bid : signal is "xilinx.com:interface:aximm:1.0 s_axi BID";
  attribute X_INTERFACE_INFO of s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi BRESP";
  attribute X_INTERFACE_INFO of s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi RDATA";
  attribute X_INTERFACE_INFO of s_axi_rid : signal is "xilinx.com:interface:aximm:1.0 s_axi RID";
  attribute X_INTERFACE_INFO of s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi RRESP";
  attribute X_INTERFACE_INFO of s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi WDATA";
  attribute X_INTERFACE_INFO of s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi WSTRB";
  attribute X_INTERFACE_INFO of s_axi_wuser : signal is "xilinx.com:interface:aximm:1.0 s_axi WUSER";
begin
  RI_addr(15 downto 12) <= \^ri_addr\(15 downto 12);
  RI_addr(11) <= \<const0>\;
  RI_addr(10) <= \<const0>\;
  RI_addr(9 downto 0) <= \^ri_addr\(9 downto 0);
  s_axi_bid(11) <= \<const0>\;
  s_axi_bid(10) <= \<const0>\;
  s_axi_bid(9) <= \<const0>\;
  s_axi_bid(8) <= \<const0>\;
  s_axi_bid(7) <= \<const0>\;
  s_axi_bid(6) <= \<const0>\;
  s_axi_bid(5) <= \<const0>\;
  s_axi_bid(4) <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_rid(11) <= \<const0>\;
  s_axi_rid(10) <= \<const0>\;
  s_axi_rid(9) <= \<const0>\;
  s_axi_rid(8) <= \<const0>\;
  s_axi_rid(7) <= \<const0>\;
  s_axi_rid(6) <= \<const0>\;
  s_axi_rid(5) <= \<const0>\;
  s_axi_rid(4) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rresp(1) <= \^s_axi_rresp\(1);
  s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.design_2_AXI_Translator_0_0_AXI_Translator
     port map (
      Q => s_axi_rvalid,
      RI_addr(13 downto 10) => \^ri_addr\(15 downto 12),
      RI_addr(9 downto 0) => \^ri_addr\(9 downto 0),
      RI_avalid => RI_avalid,
      RI_mode => RI_mode,
      RI_rdata(31 downto 0) => RI_rdata(31 downto 0),
      RI_rvalid => RI_rvalid,
      RI_wdata(31 downto 0) => RI_wdata(31 downto 0),
      RI_wvalid => RI_wvalid,
      clk => clk,
      resetn => resetn,
      s_axi_araddr(13 downto 0) => s_axi_araddr(15 downto 2),
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(13 downto 0) => s_axi_awaddr(15 downto 2),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid_reg_0 => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rlast => s_axi_rlast,
      s_axi_rready => s_axi_rready,
      s_axi_rresp(0) => \^s_axi_rresp\(1),
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid
    );
end STRUCTURE;
