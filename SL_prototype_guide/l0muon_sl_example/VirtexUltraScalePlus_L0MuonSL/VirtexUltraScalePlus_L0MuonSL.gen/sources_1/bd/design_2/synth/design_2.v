//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
//Date        : Fri Nov 25 19:11:57 2022
//Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target design_2.bd
//Design      : design_2
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_2,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_2,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=6,numReposBlks=6,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=2,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_2.hwdef" *) 
module design_2
   (CLK125,
    CLK125_locked,
    FPGARSTB,
    GT_DIFF_REFCLK1_clk_n,
    GT_DIFF_REFCLK1_clk_p,
    GT_SERIAL_F2Z_txn,
    GT_SERIAL_F2Z_txp,
    GT_SERIAL_Z2F_rxn,
    GT_SERIAL_Z2F_rxp,
    RI_addr,
    RI_avalid,
    RI_mode,
    RI_rdata,
    RI_rvalid,
    RI_wdata,
    RI_wvalid);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK125 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK125, ASSOCIATED_RESET CLK125_locked, CLK_DOMAIN design_2_CLK125, FREQ_HZ 125000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.000" *) input CLK125;
  input CLK125_locked;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.FPGARSTB RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.FPGARSTB, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input FPGARSTB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 GT_DIFF_REFCLK1 CLK_N" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME GT_DIFF_REFCLK1, CAN_DEBUG false, FREQ_HZ 125000000" *) input GT_DIFF_REFCLK1_clk_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:diff_clock:1.0 GT_DIFF_REFCLK1 CLK_P" *) input GT_DIFF_REFCLK1_clk_p;
  (* X_INTERFACE_INFO = "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_TX:1.0 GT_SERIAL_F2Z TXN" *) output [0:0]GT_SERIAL_F2Z_txn;
  (* X_INTERFACE_INFO = "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_TX:1.0 GT_SERIAL_F2Z TXP" *) output [0:0]GT_SERIAL_F2Z_txp;
  (* X_INTERFACE_INFO = "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_RX:1.0 GT_SERIAL_Z2F RXN" *) input [0:0]GT_SERIAL_Z2F_rxn;
  (* X_INTERFACE_INFO = "xilinx.com:display_aurora:GT_Serial_Transceiver_Pins_RX:1.0 GT_SERIAL_Z2F RXP" *) input [0:0]GT_SERIAL_Z2F_rxp;
  output [15:0]RI_addr;
  output RI_avalid;
  output RI_mode;
  input [31:0]RI_rdata;
  input RI_rvalid;
  output [31:0]RI_wdata;
  output RI_wvalid;

  (* DEBUG = "true" *) (* MARK_DEBUG *) wire [15:0]AXI_Translator_0_RI_addr;
  (* DEBUG = "true" *) (* MARK_DEBUG *) wire AXI_Translator_0_RI_avalid;
  (* DEBUG = "true" *) (* MARK_DEBUG *) wire AXI_Translator_0_RI_mode;
  (* DEBUG = "true" *) (* MARK_DEBUG *) wire [31:0]AXI_Translator_0_RI_wdata;
  (* DEBUG = "true" *) (* MARK_DEBUG *) wire AXI_Translator_0_RI_wvalid;
  wire CLK125_locked_1;
  wire FPGARSTB_1;
  wire GT_DIFF_REFCLK1_1_CLK_N;
  wire GT_DIFF_REFCLK1_1_CLK_P;
  wire [0:0]GT_SERIAL_Z2F_1_RXN;
  wire [0:0]GT_SERIAL_Z2F_1_RXP;
  (* DEBUG = "true" *) (* MARK_DEBUG *) wire [31:0]RI_rdata_1;
  (* DEBUG = "true" *) (* MARK_DEBUG *) wire RI_rvalid_1;
  wire [0:0]aurora_64b66b_0_GT_SERIAL_TX_TXN;
  wire [0:0]aurora_64b66b_0_GT_SERIAL_TX_TXP;
  wire [0:63]aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA;
  wire aurora_64b66b_0_USER_DATA_M_AXIS_RX_TVALID;
  wire aurora_64b66b_0_channel_up;
  wire aurora_64b66b_0_gt_reset_out;
  wire aurora_64b66b_0_mmcm_not_locked_out;
  wire aurora_64b66b_0_user_clk_out;
  wire [63:0]axi_chip2chip_0_AXIS_TX_TDATA;
  wire axi_chip2chip_0_AXIS_TX_TREADY;
  wire axi_chip2chip_0_AXIS_TX_TVALID;
  wire axi_chip2chip_0_aurora_pma_init_out;
  wire axi_chip2chip_0_aurora_reset_pb;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 ARADDR" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [31:0]axi_chip2chip_0_m_axi_ARADDR;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 ARBURST" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [1:0]axi_chip2chip_0_m_axi_ARBURST;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 ARLEN" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [7:0]axi_chip2chip_0_m_axi_ARLEN;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 ARREADY" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_ARREADY;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 ARSIZE" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [2:0]axi_chip2chip_0_m_axi_ARSIZE;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 ARVALID" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_ARVALID;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 AWADDR" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [31:0]axi_chip2chip_0_m_axi_AWADDR;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 AWBURST" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [1:0]axi_chip2chip_0_m_axi_AWBURST;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 AWLEN" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [7:0]axi_chip2chip_0_m_axi_AWLEN;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 AWREADY" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_AWREADY;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 AWSIZE" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [2:0]axi_chip2chip_0_m_axi_AWSIZE;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 AWVALID" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_AWVALID;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 BREADY" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_BREADY;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 BRESP" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [1:0]axi_chip2chip_0_m_axi_BRESP;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 BVALID" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_BVALID;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 RDATA" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [31:0]axi_chip2chip_0_m_axi_RDATA;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 RLAST" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_RLAST;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 RREADY" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_RREADY;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 RRESP" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [1:0]axi_chip2chip_0_m_axi_RRESP;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 RVALID" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_RVALID;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 WDATA" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [31:0]axi_chip2chip_0_m_axi_WDATA;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 WLAST" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_WLAST;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 WREADY" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_WREADY;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 WSTRB" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire [3:0]axi_chip2chip_0_m_axi_WSTRB;
  (* CONN_BUS_INFO = "axi_chip2chip_0_m_axi xilinx.com:interface:aximm:1.0 AXI4 WVALID" *) (* DEBUG = "true" *) (* MARK_DEBUG *) wire axi_chip2chip_0_m_axi_WVALID;
  wire clk_wiz_0_clk_out1;
  wire pma_init_generator_0_pma_init_in;
  wire [0:0]proc_sys_reset_0_peripheral_aresetn;

  assign CLK125_locked_1 = CLK125_locked;
  assign FPGARSTB_1 = FPGARSTB;
  assign GT_DIFF_REFCLK1_1_CLK_N = GT_DIFF_REFCLK1_clk_n;
  assign GT_DIFF_REFCLK1_1_CLK_P = GT_DIFF_REFCLK1_clk_p;
  assign GT_SERIAL_F2Z_txn[0] = aurora_64b66b_0_GT_SERIAL_TX_TXN;
  assign GT_SERIAL_F2Z_txp[0] = aurora_64b66b_0_GT_SERIAL_TX_TXP;
  assign GT_SERIAL_Z2F_1_RXN = GT_SERIAL_Z2F_rxn[0];
  assign GT_SERIAL_Z2F_1_RXP = GT_SERIAL_Z2F_rxp[0];
  assign RI_addr[15:0] = AXI_Translator_0_RI_addr;
  assign RI_avalid = AXI_Translator_0_RI_avalid;
  assign RI_mode = AXI_Translator_0_RI_mode;
  assign RI_rdata_1 = RI_rdata[31:0];
  assign RI_rvalid_1 = RI_rvalid;
  assign RI_wdata[31:0] = AXI_Translator_0_RI_wdata;
  assign RI_wvalid = AXI_Translator_0_RI_wvalid;
  assign clk_wiz_0_clk_out1 = CLK125;
  design_2_AXI_Translator_0_0 AXI_Translator_0
       (.RI_addr(AXI_Translator_0_RI_addr),
        .RI_avalid(AXI_Translator_0_RI_avalid),
        .RI_mode(AXI_Translator_0_RI_mode),
        .RI_rdata(RI_rdata_1),
        .RI_rvalid(RI_rvalid_1),
        .RI_wdata(AXI_Translator_0_RI_wdata),
        .RI_wvalid(AXI_Translator_0_RI_wvalid),
        .clk(clk_wiz_0_clk_out1),
        .resetn(proc_sys_reset_0_peripheral_aresetn),
        .s_axi_araddr(axi_chip2chip_0_m_axi_ARADDR),
        .s_axi_arburst(axi_chip2chip_0_m_axi_ARBURST),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen(axi_chip2chip_0_m_axi_ARLEN),
        .s_axi_arready(axi_chip2chip_0_m_axi_ARREADY),
        .s_axi_arsize(axi_chip2chip_0_m_axi_ARSIZE),
        .s_axi_arvalid(axi_chip2chip_0_m_axi_ARVALID),
        .s_axi_awaddr(axi_chip2chip_0_m_axi_AWADDR),
        .s_axi_awburst(axi_chip2chip_0_m_axi_AWBURST),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen(axi_chip2chip_0_m_axi_AWLEN),
        .s_axi_awready(axi_chip2chip_0_m_axi_AWREADY),
        .s_axi_awsize(axi_chip2chip_0_m_axi_AWSIZE),
        .s_axi_awvalid(axi_chip2chip_0_m_axi_AWVALID),
        .s_axi_bready(axi_chip2chip_0_m_axi_BREADY),
        .s_axi_bresp(axi_chip2chip_0_m_axi_BRESP),
        .s_axi_bvalid(axi_chip2chip_0_m_axi_BVALID),
        .s_axi_rdata(axi_chip2chip_0_m_axi_RDATA),
        .s_axi_rlast(axi_chip2chip_0_m_axi_RLAST),
        .s_axi_rready(axi_chip2chip_0_m_axi_RREADY),
        .s_axi_rresp(axi_chip2chip_0_m_axi_RRESP),
        .s_axi_rvalid(axi_chip2chip_0_m_axi_RVALID),
        .s_axi_wdata(axi_chip2chip_0_m_axi_WDATA),
        .s_axi_wlast(axi_chip2chip_0_m_axi_WLAST),
        .s_axi_wready(axi_chip2chip_0_m_axi_WREADY),
        .s_axi_wstrb(axi_chip2chip_0_m_axi_WSTRB),
        .s_axi_wuser({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wvalid(axi_chip2chip_0_m_axi_WVALID));
  design_2_aurora_64b66b_0_0 aurora_64b66b_0
       (.channel_up(aurora_64b66b_0_channel_up),
        .gt_refclk1_n(GT_DIFF_REFCLK1_1_CLK_N),
        .gt_refclk1_p(GT_DIFF_REFCLK1_1_CLK_P),
        .gt_reset_out(aurora_64b66b_0_gt_reset_out),
        .gt_rxcdrovrden_in(1'b0),
        .init_clk(clk_wiz_0_clk_out1),
        .loopback({1'b0,1'b0,1'b0}),
        .m_axi_rx_tdata(aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA),
        .m_axi_rx_tvalid(aurora_64b66b_0_USER_DATA_M_AXIS_RX_TVALID),
        .mmcm_not_locked_out(aurora_64b66b_0_mmcm_not_locked_out),
        .pma_init(axi_chip2chip_0_aurora_pma_init_out),
        .power_down(1'b0),
        .reset_pb(axi_chip2chip_0_aurora_reset_pb),
        .rxn(GT_SERIAL_Z2F_1_RXN),
        .rxp(GT_SERIAL_Z2F_1_RXP),
        .s_axi_tx_tdata({axi_chip2chip_0_AXIS_TX_TDATA[63],axi_chip2chip_0_AXIS_TX_TDATA[62],axi_chip2chip_0_AXIS_TX_TDATA[61],axi_chip2chip_0_AXIS_TX_TDATA[60],axi_chip2chip_0_AXIS_TX_TDATA[59],axi_chip2chip_0_AXIS_TX_TDATA[58],axi_chip2chip_0_AXIS_TX_TDATA[57],axi_chip2chip_0_AXIS_TX_TDATA[56],axi_chip2chip_0_AXIS_TX_TDATA[55],axi_chip2chip_0_AXIS_TX_TDATA[54],axi_chip2chip_0_AXIS_TX_TDATA[53],axi_chip2chip_0_AXIS_TX_TDATA[52],axi_chip2chip_0_AXIS_TX_TDATA[51],axi_chip2chip_0_AXIS_TX_TDATA[50],axi_chip2chip_0_AXIS_TX_TDATA[49],axi_chip2chip_0_AXIS_TX_TDATA[48],axi_chip2chip_0_AXIS_TX_TDATA[47],axi_chip2chip_0_AXIS_TX_TDATA[46],axi_chip2chip_0_AXIS_TX_TDATA[45],axi_chip2chip_0_AXIS_TX_TDATA[44],axi_chip2chip_0_AXIS_TX_TDATA[43],axi_chip2chip_0_AXIS_TX_TDATA[42],axi_chip2chip_0_AXIS_TX_TDATA[41],axi_chip2chip_0_AXIS_TX_TDATA[40],axi_chip2chip_0_AXIS_TX_TDATA[39],axi_chip2chip_0_AXIS_TX_TDATA[38],axi_chip2chip_0_AXIS_TX_TDATA[37],axi_chip2chip_0_AXIS_TX_TDATA[36],axi_chip2chip_0_AXIS_TX_TDATA[35],axi_chip2chip_0_AXIS_TX_TDATA[34],axi_chip2chip_0_AXIS_TX_TDATA[33],axi_chip2chip_0_AXIS_TX_TDATA[32],axi_chip2chip_0_AXIS_TX_TDATA[31],axi_chip2chip_0_AXIS_TX_TDATA[30],axi_chip2chip_0_AXIS_TX_TDATA[29],axi_chip2chip_0_AXIS_TX_TDATA[28],axi_chip2chip_0_AXIS_TX_TDATA[27],axi_chip2chip_0_AXIS_TX_TDATA[26],axi_chip2chip_0_AXIS_TX_TDATA[25],axi_chip2chip_0_AXIS_TX_TDATA[24],axi_chip2chip_0_AXIS_TX_TDATA[23],axi_chip2chip_0_AXIS_TX_TDATA[22],axi_chip2chip_0_AXIS_TX_TDATA[21],axi_chip2chip_0_AXIS_TX_TDATA[20],axi_chip2chip_0_AXIS_TX_TDATA[19],axi_chip2chip_0_AXIS_TX_TDATA[18],axi_chip2chip_0_AXIS_TX_TDATA[17],axi_chip2chip_0_AXIS_TX_TDATA[16],axi_chip2chip_0_AXIS_TX_TDATA[15],axi_chip2chip_0_AXIS_TX_TDATA[14],axi_chip2chip_0_AXIS_TX_TDATA[13],axi_chip2chip_0_AXIS_TX_TDATA[12],axi_chip2chip_0_AXIS_TX_TDATA[11],axi_chip2chip_0_AXIS_TX_TDATA[10],axi_chip2chip_0_AXIS_TX_TDATA[9],axi_chip2chip_0_AXIS_TX_TDATA[8],axi_chip2chip_0_AXIS_TX_TDATA[7],axi_chip2chip_0_AXIS_TX_TDATA[6],axi_chip2chip_0_AXIS_TX_TDATA[5],axi_chip2chip_0_AXIS_TX_TDATA[4],axi_chip2chip_0_AXIS_TX_TDATA[3],axi_chip2chip_0_AXIS_TX_TDATA[2],axi_chip2chip_0_AXIS_TX_TDATA[1],axi_chip2chip_0_AXIS_TX_TDATA[0]}),
        .s_axi_tx_tready(axi_chip2chip_0_AXIS_TX_TREADY),
        .s_axi_tx_tvalid(axi_chip2chip_0_AXIS_TX_TVALID),
        .txn(aurora_64b66b_0_GT_SERIAL_TX_TXN),
        .txp(aurora_64b66b_0_GT_SERIAL_TX_TXP),
        .user_clk_out(aurora_64b66b_0_user_clk_out));
  design_2_axi_chip2chip_0_0 axi_chip2chip_0
       (.aurora_init_clk(clk_wiz_0_clk_out1),
        .aurora_mmcm_not_locked(aurora_64b66b_0_mmcm_not_locked_out),
        .aurora_pma_init_in(pma_init_generator_0_pma_init_in),
        .aurora_pma_init_out(axi_chip2chip_0_aurora_pma_init_out),
        .aurora_reset_pb(axi_chip2chip_0_aurora_reset_pb),
        .axi_c2c_aurora_channel_up(aurora_64b66b_0_channel_up),
        .axi_c2c_aurora_rx_tdata({aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[0],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[1],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[2],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[3],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[4],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[5],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[6],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[7],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[8],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[9],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[10],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[11],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[12],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[13],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[14],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[15],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[16],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[17],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[18],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[19],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[20],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[21],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[22],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[23],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[24],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[25],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[26],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[27],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[28],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[29],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[30],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[31],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[32],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[33],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[34],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[35],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[36],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[37],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[38],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[39],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[40],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[41],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[42],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[43],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[44],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[45],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[46],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[47],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[48],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[49],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[50],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[51],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[52],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[53],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[54],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[55],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[56],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[57],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[58],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[59],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[60],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[61],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[62],aurora_64b66b_0_USER_DATA_M_AXIS_RX_TDATA[63]}),
        .axi_c2c_aurora_rx_tvalid(aurora_64b66b_0_USER_DATA_M_AXIS_RX_TVALID),
        .axi_c2c_aurora_tx_tdata(axi_chip2chip_0_AXIS_TX_TDATA),
        .axi_c2c_aurora_tx_tready(axi_chip2chip_0_AXIS_TX_TREADY),
        .axi_c2c_aurora_tx_tvalid(axi_chip2chip_0_AXIS_TX_TVALID),
        .axi_c2c_phy_clk(aurora_64b66b_0_user_clk_out),
        .axi_c2c_s2m_intr_in({1'b0,1'b0,1'b0,1'b0}),
        .m_aclk(clk_wiz_0_clk_out1),
        .m_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .m_axi_araddr(axi_chip2chip_0_m_axi_ARADDR),
        .m_axi_arburst(axi_chip2chip_0_m_axi_ARBURST),
        .m_axi_arlen(axi_chip2chip_0_m_axi_ARLEN),
        .m_axi_arready(axi_chip2chip_0_m_axi_ARREADY),
        .m_axi_arsize(axi_chip2chip_0_m_axi_ARSIZE),
        .m_axi_arvalid(axi_chip2chip_0_m_axi_ARVALID),
        .m_axi_awaddr(axi_chip2chip_0_m_axi_AWADDR),
        .m_axi_awburst(axi_chip2chip_0_m_axi_AWBURST),
        .m_axi_awlen(axi_chip2chip_0_m_axi_AWLEN),
        .m_axi_awready(axi_chip2chip_0_m_axi_AWREADY),
        .m_axi_awsize(axi_chip2chip_0_m_axi_AWSIZE),
        .m_axi_awvalid(axi_chip2chip_0_m_axi_AWVALID),
        .m_axi_bready(axi_chip2chip_0_m_axi_BREADY),
        .m_axi_bresp(axi_chip2chip_0_m_axi_BRESP),
        .m_axi_bvalid(axi_chip2chip_0_m_axi_BVALID),
        .m_axi_rdata(axi_chip2chip_0_m_axi_RDATA),
        .m_axi_rlast(axi_chip2chip_0_m_axi_RLAST),
        .m_axi_rready(axi_chip2chip_0_m_axi_RREADY),
        .m_axi_rresp(axi_chip2chip_0_m_axi_RRESP),
        .m_axi_rvalid(axi_chip2chip_0_m_axi_RVALID),
        .m_axi_wdata(axi_chip2chip_0_m_axi_WDATA),
        .m_axi_wlast(axi_chip2chip_0_m_axi_WLAST),
        .m_axi_wready(axi_chip2chip_0_m_axi_WREADY),
        .m_axi_wstrb(axi_chip2chip_0_m_axi_WSTRB),
        .m_axi_wvalid(axi_chip2chip_0_m_axi_WVALID));
  design_2_pma_init_generator_0_0 pma_init_generator_0
       (.FPGARSTB(FPGARSTB_1),
        .aur_init_clk(clk_wiz_0_clk_out1),
        .pma_init_in(pma_init_generator_0_pma_init_in),
        .resetn(CLK125_locked_1));
  design_2_proc_sys_reset_0_0 proc_sys_reset_0
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(aurora_64b66b_0_gt_reset_out),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(proc_sys_reset_0_peripheral_aresetn),
        .slowest_sync_clk(clk_wiz_0_clk_out1));
  design_2_system_ila_0_0 system_ila_0
       (.SLOT_0_AXI_araddr(axi_chip2chip_0_m_axi_ARADDR),
        .SLOT_0_AXI_arburst(axi_chip2chip_0_m_axi_ARBURST),
        .SLOT_0_AXI_arlen(axi_chip2chip_0_m_axi_ARLEN),
        .SLOT_0_AXI_arready(axi_chip2chip_0_m_axi_ARREADY),
        .SLOT_0_AXI_arsize(axi_chip2chip_0_m_axi_ARSIZE),
        .SLOT_0_AXI_arvalid(axi_chip2chip_0_m_axi_ARVALID),
        .SLOT_0_AXI_awaddr(axi_chip2chip_0_m_axi_AWADDR),
        .SLOT_0_AXI_awburst(axi_chip2chip_0_m_axi_AWBURST),
        .SLOT_0_AXI_awlen(axi_chip2chip_0_m_axi_AWLEN),
        .SLOT_0_AXI_awready(axi_chip2chip_0_m_axi_AWREADY),
        .SLOT_0_AXI_awsize(axi_chip2chip_0_m_axi_AWSIZE),
        .SLOT_0_AXI_awvalid(axi_chip2chip_0_m_axi_AWVALID),
        .SLOT_0_AXI_bready(axi_chip2chip_0_m_axi_BREADY),
        .SLOT_0_AXI_bresp(axi_chip2chip_0_m_axi_BRESP),
        .SLOT_0_AXI_bvalid(axi_chip2chip_0_m_axi_BVALID),
        .SLOT_0_AXI_rdata(axi_chip2chip_0_m_axi_RDATA),
        .SLOT_0_AXI_rlast(axi_chip2chip_0_m_axi_RLAST),
        .SLOT_0_AXI_rready(axi_chip2chip_0_m_axi_RREADY),
        .SLOT_0_AXI_rresp(axi_chip2chip_0_m_axi_RRESP),
        .SLOT_0_AXI_rvalid(axi_chip2chip_0_m_axi_RVALID),
        .SLOT_0_AXI_wdata(axi_chip2chip_0_m_axi_WDATA),
        .SLOT_0_AXI_wlast(axi_chip2chip_0_m_axi_WLAST),
        .SLOT_0_AXI_wready(axi_chip2chip_0_m_axi_WREADY),
        .SLOT_0_AXI_wstrb(axi_chip2chip_0_m_axi_WSTRB),
        .SLOT_0_AXI_wvalid(axi_chip2chip_0_m_axi_WVALID),
        .clk(clk_wiz_0_clk_out1),
        .probe0(AXI_Translator_0_RI_addr),
        .probe1(AXI_Translator_0_RI_avalid),
        .probe2(AXI_Translator_0_RI_mode),
        .probe3(AXI_Translator_0_RI_wdata),
        .probe4(AXI_Translator_0_RI_wvalid),
        .probe5(RI_rdata_1),
        .probe6(RI_rvalid_1),
        .resetn(proc_sys_reset_0_peripheral_aresetn));
endmodule
