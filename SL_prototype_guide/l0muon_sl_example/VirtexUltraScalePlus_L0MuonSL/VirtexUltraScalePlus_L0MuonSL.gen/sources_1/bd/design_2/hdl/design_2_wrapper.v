//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
//Date        : Fri Nov 25 19:11:58 2022
//Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target design_2_wrapper.bd
//Design      : design_2_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_2_wrapper
   (CLK125,
    CLK125_locked,
    FPGARSTB,
    GT_DIFF_REFCLK1_clk_n,
    GT_DIFF_REFCLK1_clk_p,
    GT_SERIAL_F2Z_txn,
    GT_SERIAL_F2Z_txp,
    GT_SERIAL_Z2F_rxn,
    GT_SERIAL_Z2F_rxp,
    RI_addr,
    RI_avalid,
    RI_mode,
    RI_rdata,
    RI_rvalid,
    RI_wdata,
    RI_wvalid);
  input CLK125;
  input CLK125_locked;
  input FPGARSTB;
  input GT_DIFF_REFCLK1_clk_n;
  input GT_DIFF_REFCLK1_clk_p;
  output [0:0]GT_SERIAL_F2Z_txn;
  output [0:0]GT_SERIAL_F2Z_txp;
  input [0:0]GT_SERIAL_Z2F_rxn;
  input [0:0]GT_SERIAL_Z2F_rxp;
  output [15:0]RI_addr;
  output RI_avalid;
  output RI_mode;
  input [31:0]RI_rdata;
  input RI_rvalid;
  output [31:0]RI_wdata;
  output RI_wvalid;

  wire CLK125;
  wire CLK125_locked;
  wire FPGARSTB;
  wire GT_DIFF_REFCLK1_clk_n;
  wire GT_DIFF_REFCLK1_clk_p;
  wire [0:0]GT_SERIAL_F2Z_txn;
  wire [0:0]GT_SERIAL_F2Z_txp;
  wire [0:0]GT_SERIAL_Z2F_rxn;
  wire [0:0]GT_SERIAL_Z2F_rxp;
  wire [15:0]RI_addr;
  wire RI_avalid;
  wire RI_mode;
  wire [31:0]RI_rdata;
  wire RI_rvalid;
  wire [31:0]RI_wdata;
  wire RI_wvalid;

  design_2 design_2_i
       (.CLK125(CLK125),
        .CLK125_locked(CLK125_locked),
        .FPGARSTB(FPGARSTB),
        .GT_DIFF_REFCLK1_clk_n(GT_DIFF_REFCLK1_clk_n),
        .GT_DIFF_REFCLK1_clk_p(GT_DIFF_REFCLK1_clk_p),
        .GT_SERIAL_F2Z_txn(GT_SERIAL_F2Z_txn),
        .GT_SERIAL_F2Z_txp(GT_SERIAL_F2Z_txp),
        .GT_SERIAL_Z2F_rxn(GT_SERIAL_Z2F_rxn),
        .GT_SERIAL_Z2F_rxp(GT_SERIAL_Z2F_rxp),
        .RI_addr(RI_addr),
        .RI_avalid(RI_avalid),
        .RI_mode(RI_mode),
        .RI_rdata(RI_rdata),
        .RI_rvalid(RI_rvalid),
        .RI_wdata(RI_wdata),
        .RI_wvalid(RI_wvalid));
endmodule
