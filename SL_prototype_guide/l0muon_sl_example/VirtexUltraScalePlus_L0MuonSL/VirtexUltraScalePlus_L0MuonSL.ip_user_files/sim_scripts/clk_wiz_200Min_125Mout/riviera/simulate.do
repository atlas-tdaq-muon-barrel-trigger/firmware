onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+clk_wiz_200Min_125Mout -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.clk_wiz_200Min_125Mout xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {clk_wiz_200Min_125Mout.udo}

run -all

endsim

quit -force
