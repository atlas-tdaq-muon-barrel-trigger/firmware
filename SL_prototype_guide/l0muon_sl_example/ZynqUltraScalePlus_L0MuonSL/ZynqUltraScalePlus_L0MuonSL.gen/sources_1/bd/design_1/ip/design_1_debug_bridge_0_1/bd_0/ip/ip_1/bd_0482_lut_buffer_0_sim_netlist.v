// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Oct 30 02:07:47 2022
// Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/ip/ip_1/bd_0482_lut_buffer_0_sim_netlist.v
// Design      : bd_0482_lut_buffer_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu5ev-sfvc784-2-i
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "bd_0482_lut_buffer_0,lut_buffer_v2_0_0_lut_buffer,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "lut_buffer_v2_0_0_lut_buffer,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module bd_0482_lut_buffer_0
   (tdi_i,
    tms_i,
    tck_i,
    drck_i,
    sel_i,
    shift_i,
    update_i,
    capture_i,
    runtest_i,
    reset_i,
    bscanid_en_i,
    tdo_o,
    tdi_o,
    tms_o,
    tck_o,
    drck_o,
    sel_o,
    shift_o,
    update_o,
    capture_o,
    runtest_o,
    reset_o,
    bscanid_en_o,
    tdo_i);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TDI" *) input tdi_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TMS" *) input tms_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TCK" *) input tck_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan DRCK" *) input drck_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan SEL" *) input sel_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan SHIFT" *) input shift_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan UPDATE" *) input update_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan CAPTURE" *) input capture_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan RUNTEST" *) input runtest_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan RESET" *) input reset_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan BSCANID_EN" *) input bscanid_en_i;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 s_bscan TDO" *) output tdo_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TDI" *) output tdi_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TMS" *) output tms_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TCK" *) output tck_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan DRCK" *) output drck_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan SEL" *) output sel_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan SHIFT" *) output shift_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan UPDATE" *) output update_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan CAPTURE" *) output capture_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan RUNTEST" *) output runtest_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan RESET" *) output reset_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan BSCANID_EN" *) output bscanid_en_o;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TDO" *) input tdo_i;

  wire bscanid_en_i;
  wire bscanid_en_o;
  wire capture_i;
  wire capture_o;
  wire drck_i;
  wire drck_o;
  wire reset_i;
  wire reset_o;
  wire runtest_i;
  wire runtest_o;
  wire sel_i;
  wire sel_o;
  wire shift_i;
  wire shift_o;
  wire tck_i;
  wire tck_o;
  wire tdi_i;
  wire tdi_o;
  wire tdo_i;
  wire tdo_o;
  wire tms_i;
  wire tms_o;
  wire update_i;
  wire update_o;
  wire [31:0]NLW_inst_bscanid_o_UNCONNECTED;

  (* C_EN_BSCANID_VEC = "0" *) 
  (* DONT_TOUCH *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* is_du_within_envelope = "true" *) 
  bd_0482_lut_buffer_0_lut_buffer_v2_0_0_lut_buffer inst
       (.bscanid_en_i(bscanid_en_i),
        .bscanid_en_o(bscanid_en_o),
        .bscanid_i({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .bscanid_o(NLW_inst_bscanid_o_UNCONNECTED[31:0]),
        .capture_i(capture_i),
        .capture_o(capture_o),
        .drck_i(drck_i),
        .drck_o(drck_o),
        .reset_i(reset_i),
        .reset_o(reset_o),
        .runtest_i(runtest_i),
        .runtest_o(runtest_o),
        .sel_i(sel_i),
        .sel_o(sel_o),
        .shift_i(shift_i),
        .shift_o(shift_o),
        .tck_i(tck_i),
        .tck_o(tck_o),
        .tdi_i(tdi_i),
        .tdi_o(tdi_o),
        .tdo_i(tdo_i),
        .tdo_o(tdo_o),
        .tms_i(tms_i),
        .tms_o(tms_o),
        .update_i(update_i),
        .update_o(update_o));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
geSCzw9gYjFCv0Dn0YxOXxhH+GZFMePCQPK3AjT+zbjt1urPphGbRmSIP212qcXhU3u6qBayOOuP
zGTUOznyYQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
OnCSRn8bnLy+eSxgkIEXKk5zY3JDppSX+6N3lQVX9PeSypgnQ/2z4GTpmoL+rdMoco6U9R4G1u4m
E0xhKuM4ba9nEk7cLfAxOQqKqsWQrZaIEmzIr1ET+cp4jOMvYA/MsN4jh93kbuKcSDuJ8zN13DFX
RemIkmekhFjPkyUS5qM=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MSBAO7tnsBVh2XpVImbQvPkv1Ik+6Bw1D0e9n6H/Bw1mXnRXzm0RzPaEYAIFuluPbWglTrw4pQSr
JI/DSdCg6087Xmb+Q5zKawFvuZahx4HgmrKxTL15lZwamiIpmu3LGyxaEH/VbYGM9Ky0jp5PyDKU
Jeskyx64XVUPlRklhMjIKCtYITsgROzqjs+d1jIc494zqnDADEz0msJP38WdzHgwLDQ0NamfpodX
BUqMR71hgPx1Rvdt7HagUbkfyaG3/12LxFvpAblT7W0W6RKBFEFgFrxWRFaDw+jzj4jgl9g+sjY0
cveJYAA4UpZJwPSDIWehjmS+mOinzlnl8UP7jw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UtIiSU3lZ1iUKAuaJuT083jLC5QokBuxbJC/zVsWXf8ozOCIDAvtpSufF02lDCCaNNheB40dXQFS
I8VBcTtdWzNr2vj/HmW17e10D6T6mqn/8t0HnWx9c3modRuXup0Too1mNTU5gTH+v3utogTO5ztm
HbJZ/+5ov0tPkaeJufJl5L/RZAfLmRnRYybtx5bbc7XiGyWaVk6KunsaWtX5zJtVnMeUOkg0N8oL
RBeyFp3tFqTN7ecNUp7zom6BjZ3fR6euRy36u1XviJsqBjcxzASI1k+bn2lDs3oEdXuMHoRvcuWL
mmMddzjwWr43L7YoB/WBz/Tw7t6iYI5B6imPUA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
AMZ17uwJyVzW4KyzjD/YjCrX2GlLIDwW9HSuEat97pn8ZQ7QpDPhFLNx09klp1fHQ8yb1KlxCqpm
IjAljp4A5oQHWcBw/s+Xhtpin6GMDGjsmd5KmAD2J5DQmzqPazc0M8vNO+pGpCJogvWarX5XrP6U
56l9vH5mfyPmbT/09Kw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
oAGTk8IFmmwAT5eT+h5xMK7MsqYCJnsMll3rq17njNu6wbVX2TAoOlVC5DzNg9T5ce7gVnuLuFG/
FgNSnTJx8TlbP73KxlDubmAVofR56G/yHzaJfJ0fwNhrfXm5AFgmFaKFPTKNkrG/qjdNuwUeA8p4
iHoj1zvPx50myVHXSpHLQ8n92DLWgMUX/57aPLbMHmYu/gsD0kHOuQ8Fr2Mi2DxufAvq1gzT0kc+
lxSntoseL+X1HLSvmKpEkR/sjaz6P9omIzqKlmOhvLeTZVEZcUtukVN1HTrlol+4/pTFDztcz0tZ
XqYZKVNB/igvn1iP/Fej8fpkaeJOrk1YgJZ6xw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HM0OWTHTT9qkiqicwldwQlaXCXhAavOkOLtzdsXybVEqcdGTuInS8Xvu2i7fjfGdnZjc+o4ZayFd
adCXUGMQZ+7u79Rm71DMtV7WL4PMhXZmItLJgXQmNzajU211AuWse/CyD7Am9ZDJuQcIK0fcqZQI
XVJU6sMESVWiSWdCuqkcQLSuSoBY7TVLmCDoTF9n7MlYfcxCkkK6d+2Xs/gjaWO59GZ3TbWhAQLc
9hHL9YUJUTzZ8yPC8tX+DLS/YrniD3lBpquxXGcl1FxHKFTSpMG/6pTH+7Y5u1s29iqS/KYCCOfR
Pqg3ikxxR4ywBL+umX+Ijv+Hqgol6tBnCkWh/g==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DQuK/YuairmUNmnTZFN/Tbbjjk202ciTMNZWiES7z/c8BYrmlnBses/x7XzAVxFxOTns+S6gcbxv
lho3MoYQI3b1wxR93ymMpbFY2AAKqfTaYrt9nuBi+J66NUkNb4mO5Ysrmk/FyxUuVMw2JeKhCxVf
1Lkw2weXEA2RSHrWxd8764IFSbBqKoKUTMuqLxHovRaQHDy/mOdyefGG7/6ywGbKjVTlE8lXVH8E
8QodSYZ7p8uod81sVFzJL26a9Tqu+u2tOgD/WqMuxrio7zRkYYC5P+/FtxLC4GaIZ6LivaJuTLOF
bkAMneaa5dlfamLnRyzDXUCJu/DFpJtH5s1eLQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DL1JbuaDxIhP8ir0jAdx6nea42/rMQ2VXG8PZEGqMkgF/yLmK8+UcPNvmkEnUbVxq6WNxCUUYBOO
eroUXvKd3hB7aza+lE1PkczDRQpe4dZWQ8yHCUSbqj/KnUKU7sMHOSk5MiYcbBazdC+B/zdSxJsr
sUmnOLgp/SqygmZW7/oDYMIYyOExEOrIPD4CH/xXZGlvuNs4OjdmaSus7kQp/iaUxQz03NGaMv3/
EuIfORb3j+mQPwXwEBQhecy81p8ky3bmOS0LK+CPuz0LF3VVvrDnnXUSBCWa2WW6t7burmoHvgPV
oB2jrvwkS6dNjRJ7CoyGvV0N6d60kiD0LjZg6Q==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2992)
`pragma protect data_block
Y8CNBszfpu89DcelmrwhxLxrpagXIyHP5ygg8WD5NINSUuniwe82e1rWY2RihsWr/+81ZbpUbf6x
SC30lec09wGni/+8w6A+b2ckQsA7GvsWhCRJqW1/qhVDAPs4Ud/e3+VoPuzESPePz8ZwPvxUGIgU
w8i7fb329qMw6SLAEpmDItcYv/meWViHxLQAxeI/6ZpvztvHbbmXhuR3WvbMfotbbQjy2AaHq6xm
m6KaIbRoUbDji+EwlrlMKKAFgRIg0MqBQzwAJyysrHmxpNOYrPV8jV+BpwY5P5Tt5TqSJJjTim2n
LUKHd8T/no0THGaHtmSsK45/bC7p+WIG7U0qBCbTI+MAPfjhS9/TFIrYkA3EAERBaRogQLUNFc5q
V7ayrPZ3unV3LvpGb785/Mk6YQPJcDkS3BzilUYCnUrAVPMcH486kssUOEaVgUgHQXgvgRTWu0VA
0G6sxbNQ2vXX6mlHmJ9vhxBfP24orr5tx8BqJT7deT6VfCNtkTJ6+/QZuXD5DIoWaUuGNrrwMEhD
smFQDiz8zrZQF05w/ZCqPSTbuervle5qnHVuP1OvZr/9s/VxlJdxmBjgZj6sMwGpeWBjOP4a4idT
3JZvChqUJFDC5kjYa0C4LIGDTI8K9uqPn3OZwfmh9+2YO1+rSX8xVWUN9dFJoy2O0likdX58fgYz
nq2tVgY1lufL5YkmweCfX8bFfxCRwQTwFLkjpRbuVvuuAZdMfA9oNfIOHiWzSpeVtsYBxyzV6CYG
rI8nk9nZ6dEBYRs3ivc69ikFXHc0knufNxxDXgzRoJsltH+W9Cp6UyGEco4OvgP8FeCl/CGxV1Pg
rh/i9QP4a3KtEA1Ygefh42xxIY2zSqwH27WkeGb1L6+qvEaSvaapqy5UA8dr3+Wbm3mh6803bq8V
oIK9hHoW35Obf2Vv6ywFvXwCN2B2YOcezEhH65DQSjE89hHQ8Wa6HzmEwryjlY4k8aMRzHycgaOL
gkslVkXoPNA9RHIiB1xlT+n8ivwBXbS8aTeTJ2KKx0N6Hqz25jNmKOfy5DrM320lFIodhel2pn8u
Fl7M3ozCBiYl99jG8gFrPv5aBwjtPO3hnmCMqNaBz64IJ//CI7jvp019tWC8ASZfCoBHCUdFcxdP
QMqmfmAPkbFNysEQKzR3P5aDGne+H3wL5jP0Mu4UK+5gfm0tasMVtmPcF3PmSsXU/dTlihqF2k8c
NQYEgankCr3/0tPsL8TvRspMj/n6I8pKFQyMF7UrubishvqUkTZBM6Fg53j4GBxyBsFYlusJMD6t
yOuUGUPkEIY/knUOV5rPye7mWxb9rlXuNlqpwBdgoJl4Kmfu7PeDqsUXnTRGIYRIIsoWnttsjWcz
q7P8hTlYuEeGF8CzvAmvN0qMZUt5258fz33LnJjOHFH7ocn7lB/Lkc+nvoyaoTIdpBzBB5qShe5G
DUmICof6yt7dh1pbmOeBQCM4aoes5zLH/pEkMwhiQGbIvHqaAmJFqWGm5FZNmOwh1/qXp+mbr1Hd
755DKXRSlzsVroTF2bNmfrI2Km3+HPIdm920ZDfcTmTAcvMfiBk0gDDn2MFiMgRxmM1BSMVw1Sen
4lQqL8Q5qwDx00yf+oHJQBR+BMTD6whePY/p0aAGaCVWFacQJqAfltU9JSUzqaqP7OQHaBTtHCT2
fD0ty224gr6RcGGzIzlr1HN5pamTsOw0lMaEDuypCTH3XPkiortFU/WK2z4K1AWzLAC5COkqEahl
yPI/jLWl+/xPKnNxscY2W2oz8/KZsNWg3xrZBCIuSuBe2UnefbzJef7FS8f3MfVscrlV7mJ7K27q
XL0r9SZdSZrG+kD2rRqVYKmQM/6RHws5zZ+p12WC9Yt8uezhhSYIwIcBuSRWU3qKrYwLHGSfCHk4
g4v/logae6F0JraLU7BckAL19Z576ALYl2firB47GfJhMafq5tBGnATYZ4LwByMouDc+3D95J5Tn
S08Wj2jmEXEJSDSfY0LWCn6GBqAkgJEmV+0ZHG9GjQEP9F/gZLWvTAlE4NiScpueTw57KZM3KgJh
D3woilrV8NIrQlKS5Eze7QeEs2aLDAja1RzwmHreLCggpxajORqBAsF0lWhDwFNlXRsOiQbIliBX
mtebXAyJrtWt44BzSyz/2IqYiBN7/weDp87x7FrwNs3qesugyuOmFfz6Oi5CCDaUtyhkRZJxnvgQ
QYVSiJFAgk18n6RRVEGv055+8coQrSfvgNAc+/iZFt+76U2Y33hLbCUzcYgVG47vVx3hueeJDbga
/t2nN7wN0ytoVGXSiREs43ASYFyqIBfWoDWBf9k8LXu+BPVWSTxpDecpcSJDzZ5+ck9vXqYGBB3d
BIZ3tNxEUmchkqWtw3br+FQWNvpT7qEgD2I7MtcQ8pTgN94YHQ4zIgaDOgOiRgMMItL4zBUMVtyN
1+gKreFzFbN4bWJmjVvwYDOgzuSS26t5lQaE/kdZmgv2WrCIHIyBUrw6H5vNfhr2iL1j4K8MghsJ
ZER5+fVoLEn+q2nmb+K6PjOeg+4InJIMylHfwcIwaQlUBd6fskIc0bJOqxuv2IWPmnOq+zxSOrku
RTGxz4WxfaYb4yBuT7Avyh/5PvnqbhUMo37ULs5qh4n7ApKLYzve9ppt9JhwxsxcqWp60Gm7D/SE
PoIVa9jkF8b6JUJ34FoIw9JBYtO5jaQPS9+CVhjTEvD+uV8+2PbU2eA44IhsjTxolw4tc5S3JAp3
l/K0ZdDMrJdCGhmLpj1PGA4wIgerqjEJ/Wc2ZgtXjnn9N4lI9+pZbL+SZKNROlcGqbolBaGRUvB/
0USK7K/gU3urvRiR5XlDGpkd9R8/DABp/9ezwWvD1AM0phuTVM26ZK2sIcPYr4R/kkCoKkzV+uwt
nqZBv7qc3vLZ7TDDsFE2OvYKHLDDie4srzNZSpo4/WaAqaW6CufMBJTHlSGYcfQ5+4qECmQoIEqO
u+DlWV/6bf1sQ1HJ7aVDld2W979ZeJoiHBzeDkQWh1r9p7WvMJe+cgZyTEB+5MWWXQx7roRiDRsp
5Grcrgj80++soUjd+KqeHO1ULo9xLtvlK9E9FVz4Pwo5EOquHl3HU0FQ+gXSmwPsBt6gvKv0o1p0
+ThHZx3G4SB9ZcWZxBEpkl40p3T6L2FmxOzaIDy1puiRJk0P17pgWOkbZcefQ2LCXu65ickJ/RAR
pPNHOjrqX0h49ltKGHr1itNF81mvFsqTJLnhIp4az9H6DatdP5erMBNFd1t7nJ9F69lE7Ep54D9z
A0IOj+LLQk94nVT/jwTyJ4W/0ICXJhWriVsz5dl5JJUUKj8xEuIN49PfbhGRSgqRolWLwUGOGQLI
rcNVA5w5+AHV6jfQdbZC0DbvWrjFWwC7oJhY31BMzZoofGJso/yEeIC3WRYh/HhrK+qi+A3Bpdsw
1iJDZPxXg23i+9LxcR046tTXTHj9J2reSdQYidtROGd1sUO1738Zp7Mkqjt/GIZ0CaBeHEw0MBZd
cdL6rt047Hw4J6tdz8/6daMzMijqctrZ+/Z9gSxqjXqUZmaF0gdRnFjCOJjgQuEK6Lev0RmAu8/p
HbTIXyQYeAsi/UytbwWuqotPJMnuZ3Pk5jvJZg+X6ravfT3LPVyekEH1gz+r2kHfrcNyAOLB5Co+
JNczCgr9FHPf3ZIl7L1yPSDvWurD0gTLwl6R3/rKBrgqaFJgbTF2e/PhbfUD1icCK2lrqAVxNbBK
3/RxcADHApjofnGvuhoYE29aEHOox9m4/L/BVmv62EQjb6JkldH3RtrqsPJ4pwkbC8eWk2tks9s9
22dC4Mn4aaiQYP7wQUNFd8XgA5PBf1D2YXzZrj+KxSWBqVvuEGJ8aCu9RjkR/hYLYMAANhf4jQ28
bEvATAhO5X7nQgJydMYUTKkgfZO72wQfF8avSBd+TsHg43Nr5TnYrKOfViseylmPgo2oKcDRx/Dz
mQNWEgDl37xNuIMhtBGP1ERaXUuXZ/ul86RhzQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
