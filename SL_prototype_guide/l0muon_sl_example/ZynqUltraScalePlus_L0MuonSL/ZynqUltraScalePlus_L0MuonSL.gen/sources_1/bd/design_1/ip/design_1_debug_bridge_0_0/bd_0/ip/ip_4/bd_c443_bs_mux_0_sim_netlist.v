// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sun Oct 30 02:07:46 2022
// Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_4/bd_c443_bs_mux_0_sim_netlist.v
// Design      : bd_c443_bs_mux_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu5ev-sfvc784-2-i
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "bd_c443_bs_mux_0,bs_mux_v1_0_0_bs_mux,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "bs_mux_v1_0_0_bs_mux,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module bd_c443_bs_mux_0
   (mux_ctrl_update,
    mux_ctrl_capture,
    mux_ctrl_reset,
    mux_ctrl_runtest,
    mux_ctrl_tck,
    mux_ctrl_tms,
    mux_ctrl_tdi,
    mux_ctrl_sel,
    mux_ctrl_shift,
    mux_ctrl_drck,
    mux_ctrl_bscanid_en,
    mux_ctrl_tdo,
    prim_update,
    prim_capture,
    prim_reset,
    prim_runtest,
    prim_tck,
    prim_tms,
    prim_tdi,
    prim_sel,
    prim_shift,
    prim_drck,
    prim_bscanid_en,
    prim_tdo,
    soft_update,
    soft_capture,
    soft_reset,
    soft_runtest,
    soft_tck,
    soft_tms,
    soft_tdi,
    soft_sel,
    soft_shift,
    soft_drck,
    soft_bscanid_en,
    soft_tdo,
    update,
    capture,
    reset,
    runtest,
    tck,
    tms,
    tdi,
    sel,
    shift,
    drck,
    bscanid_en,
    tdo,
    mux);
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs UPDATE" *) input mux_ctrl_update;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs CAPTURE" *) input mux_ctrl_capture;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs RESET" *) input mux_ctrl_reset;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs RUNTEST" *) input mux_ctrl_runtest;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs TCK" *) input mux_ctrl_tck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs TMS" *) input mux_ctrl_tms;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs TDI" *) input mux_ctrl_tdi;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs SEL" *) input mux_ctrl_sel;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs SHIFT" *) input mux_ctrl_shift;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs DRCK" *) input mux_ctrl_drck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs BSCANID_EN" *) input mux_ctrl_bscanid_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 mux_ctrl_bs TDO" *) output mux_ctrl_tdo;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan UPDATE" *) input prim_update;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan CAPTURE" *) input prim_capture;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan RESET" *) input prim_reset;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan RUNTEST" *) input prim_runtest;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan TCK" *) input prim_tck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan TMS" *) input prim_tms;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan TDI" *) input prim_tdi;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan SEL" *) input prim_sel;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan SHIFT" *) input prim_shift;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan DRCK" *) input prim_drck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan BSCANID_EN" *) input prim_bscanid_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 prim_bscan TDO" *) output prim_tdo;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan UPDATE" *) input soft_update;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan CAPTURE" *) input soft_capture;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan RESET" *) input soft_reset;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan RUNTEST" *) input soft_runtest;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan TCK" *) input soft_tck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan TMS" *) input soft_tms;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan TDI" *) input soft_tdi;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan SEL" *) input soft_sel;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan SHIFT" *) input soft_shift;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan DRCK" *) input soft_drck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan BSCANID_EN" *) input soft_bscanid_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 soft_bscan TDO" *) output soft_tdo;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan UPDATE" *) output update;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan CAPTURE" *) output capture;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan RESET" *) output reset;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan RUNTEST" *) output runtest;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TCK" *) output tck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TMS" *) output tms;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TDI" *) output tdi;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan SEL" *) output sel;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan SHIFT" *) output shift;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan DRCK" *) output drck;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan BSCANID_en" *) output bscanid_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:bscan:1.0 m_bscan TDO" *) input tdo;
  output mux;

  wire bscanid_en;
  wire capture;
  wire drck;
  wire mux;
  wire mux_ctrl_bscanid_en;
  wire mux_ctrl_capture;
  wire mux_ctrl_drck;
  wire mux_ctrl_reset;
  wire mux_ctrl_runtest;
  wire mux_ctrl_sel;
  wire mux_ctrl_shift;
  wire mux_ctrl_tck;
  wire mux_ctrl_tdi;
  wire mux_ctrl_tdo;
  wire mux_ctrl_tms;
  wire mux_ctrl_update;
  wire prim_bscanid_en;
  wire prim_capture;
  wire prim_drck;
  wire prim_reset;
  wire prim_runtest;
  wire prim_sel;
  wire prim_shift;
  wire prim_tck;
  wire prim_tdi;
  wire prim_tdo;
  wire prim_tms;
  wire prim_update;
  wire reset;
  wire runtest;
  wire sel;
  wire shift;
  wire soft_bscanid_en;
  wire soft_capture;
  wire soft_drck;
  wire soft_reset;
  wire soft_runtest;
  wire soft_sel;
  wire soft_shift;
  wire soft_tck;
  wire soft_tdi;
  wire soft_tdo;
  wire soft_tms;
  wire soft_update;
  wire tck;
  wire tdi;
  wire tdo;
  wire tms;
  wire update;

  (* C_BSCANID = "76547072" *) 
  (* is_du_within_envelope = "true" *) 
  bd_c443_bs_mux_0_bs_mux_v1_0_0_bs_mux inst
       (.bscanid_en(bscanid_en),
        .capture(capture),
        .drck(drck),
        .mux(mux),
        .mux_ctrl_bscanid_en(mux_ctrl_bscanid_en),
        .mux_ctrl_capture(mux_ctrl_capture),
        .mux_ctrl_drck(mux_ctrl_drck),
        .mux_ctrl_reset(mux_ctrl_reset),
        .mux_ctrl_runtest(mux_ctrl_runtest),
        .mux_ctrl_sel(mux_ctrl_sel),
        .mux_ctrl_shift(mux_ctrl_shift),
        .mux_ctrl_tck(mux_ctrl_tck),
        .mux_ctrl_tdi(mux_ctrl_tdi),
        .mux_ctrl_tdo(mux_ctrl_tdo),
        .mux_ctrl_tms(mux_ctrl_tms),
        .mux_ctrl_update(mux_ctrl_update),
        .prim_bscanid_en(prim_bscanid_en),
        .prim_capture(prim_capture),
        .prim_drck(prim_drck),
        .prim_reset(prim_reset),
        .prim_runtest(prim_runtest),
        .prim_sel(prim_sel),
        .prim_shift(prim_shift),
        .prim_tck(prim_tck),
        .prim_tdi(prim_tdi),
        .prim_tdo(prim_tdo),
        .prim_tms(prim_tms),
        .prim_update(prim_update),
        .reset(reset),
        .runtest(runtest),
        .sel(sel),
        .shift(shift),
        .soft_bscanid_en(soft_bscanid_en),
        .soft_capture(soft_capture),
        .soft_drck(soft_drck),
        .soft_reset(soft_reset),
        .soft_runtest(soft_runtest),
        .soft_sel(soft_sel),
        .soft_shift(soft_shift),
        .soft_tck(soft_tck),
        .soft_tdi(soft_tdi),
        .soft_tdo(soft_tdo),
        .soft_tms(soft_tms),
        .soft_update(soft_update),
        .tck(tck),
        .tdi(tdi),
        .tdo(tdo),
        .tms(tms),
        .update(update));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
TEdJrOJL2m31rtxuYyI23rAD19RCHCru4sSJZlH/LJYPIaa/hF15OGtqi4vLQqdM4duDJn9XlWmR
VNJpWUk8vQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
pZXduVDUZ0b0tW0cBizS3B3F5Phk8+tAER42pBBn60BTA69tfgSqqe+ZG+bHf8+hAUDUtEE/ENQ5
GKiklkbwaXHVVhb8ztSL48BzibzvOXdELFMsF5nsLGFvQTuhj46SpPpBMO+BPlED28zOXiCkzWct
rss6KWrAt42su3FpqjQ=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Yhe9ZN/K/8IU2RfvVmHKE3XUtggXjNrUOrrQyC3BF2buRnDAInTdXKRDGP2majB5QVkmJzBegnpy
J3CbNmfKZx2cHEpR4VZaFmX7HYAlxyiw1g9WxWEqoDHY7xbuQXcLRQsnlnot8Ij56Sio6vx3ji2m
RB6RPYIZuH46JqgP+W7RUugIsEJVMNWDtlWDCxq7dMGg0WLIql6dhrwgkCMXdgLBbl0Hf8ehU3gy
yUpeZKdn3ZoE24p87tM2cGBrZ3Vyuu/nWWbsHT/tKUoz1jv8CNGnEwLtZaUe2P4XNTpxYFXVDx37
u1l0szw010M8SiPARqB8PraA9mM9EB8T6OuomA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
evUI2CuyKmQrlPhiwtbHbobHn6UzYFuiYkoKtnNLSz2ege0dBMf3hBRowZ9s6SX+INmajwxnFGl2
G/So3dmIqIevBhfd7UHYMnZ0z5IgZzrzsH5p2EXWmHGv+Bk3NfpXXzVd0C9XY1HjTxSEKDCUL5Hh
pZgWJnHykSNb8Y+x/GI/rn0kKP8G6o+7YNrPbPo6Bsmo3ezuAiffjFHQixjOakCL5odntd/LkIkZ
qCFZ0h1MxIGu9ZCB1opOfGsKCc3O6GxQEtAwg/2fByAM3p2VopoZAfQpzLO7IybmtQY7PqjKZ83l
Afh3HDm0k91xGig28fHJJRylZY7dIa1bCpQyxg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
TB8OwLWdGjX48pVE5fuOUb1cC5cwmhb/9y/ELmjkAbN3rrfXLqvzMoFVBJHpACJI+RUYokjWr7Pa
BchPmXRuWZ3xWhRSwWmnDCxvsdXoWqBe0VnGsmNU9+SWTnWGhdu8U8huttkc6SQDjfE3fpGhwkTF
54sagfYkP/Y5VfpAvek=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
grsFo8bmeHjVGkNHei/oO8k5RZVmOtETgrCtlZiCB1FQw12IODKC10e61MjaFQY6wFfWm4FWlEBa
ivv3458XpmhMbejD9n27PoAvp7h0ilPKKNPxv+xp2AenobnSEYW56LnwMvfyogBFzsJt3xQQMr16
SfSV2Azwqqoq3hHFtvthCa/PLGubgzyoSY1XViNwW5sJEbcOH4kl8+MpIzFMP5kh2Xo8dIWq+aaR
89qG6UQOovMmhDIh+toRtMISgOTmjWpcKoMSmMbxr+PJe2wVlbVjA3oPQz3f77M0g5/SUReEYPbW
qJ6hIeVJzmAdoktjJFM1gLm6nygpI35vYM8Q5A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Xk9rreuTg4S+y+Tqr+o6IH50JOwNowdyXCtPv07+sSPXuT9safE2IMGVNEt9mhCw8h/ouT8InAiT
iB0x0XAcB8dz63IwUV0DFw7yfk6CrIbDT8YSZmy0Y/k0CfU0LWTdAt+mT60k6U2ba+tuaoxXUHKq
Jv+SVS2CH0W1iUC83HkyRoCCuWuuR+tNkCbsgrb9qkwoCz1DyYApTgaW03DtL6eDXTRMNq7D0tDJ
mV/5H4ssx3IH98gt1IYAc8KqUZ0NLpg0dcXzFsmcbZXRmymaxaqho2HV+fA/o7VFqFuQ56WvfDTU
GLx3SpATcG/e9z4PrQyPFI8Woc0U6ufYL7Xetg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QGztfRF5/n4JCTFEGUmRoK0TgTOX05GqpRDZNp1/3WLQqkGwythhAQBrjRFtUpHuh+FVr1DVqqLu
PVjhCyLzGJS04bvB4Ym4d+fXZzv0cLS44WUXGgLFjL+1e6q6qYf1H2ohMHNIG41SfosxqTHTSa+m
2urkI+nmcxEB3FxCg1+lfPAOG+WpyDuwyYEFgmrszVWbo+RMQ3M/QeWzSUXeb2NOqhY+lx3qZh7I
dJZ/3fIuKmkFi07YTchBpIA9I05ZFDah8Hc++gaaQizq9edMF1GKIv2NFMQLxpcq5ysuHko9OQAa
h60stCgUoSuzxFw5NMYcwIg1QTRfestphgjDgg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gxZlLGVr6cyvbWcwMNH0ixBg/9Sc3VNqOfjl0kOxnX1+HCa363BYIcCtl1Rpv8eEOqqP/XM1eECH
iPsLblwY0MhUkEFkwujzYIYu6UwDLibRKTHswuxZHK72JLNizc8B5GdQCquhFIuN08keicE3XG2A
Hf1TfwvZuaUjWOMysjfg8re8atK39vGqpDYr568s2MRdXQ2a/m2A/HWWExgV1ZT7MIVQF1DE1a8g
1eWhhapN63+vpy/Px3aWnSpoyF7sI0wCh4MwbLWIRZEcBgUmhtzfF53qOiJH2Og5e839L450daU5
d1ImfO6VcSx7Y1AIchGBIKjU6wl4Do8cFUYvDg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 21360)
`pragma protect data_block
pK6dCT4OiOhOwctD9KJovBBR919fBhKbm6AqvWop5l+Y4YiUQIm+MTy1BXfw3Sgr28haREExxUv8
KVXPOyr4A9ZV4YLXfUENse9EfhHC8IDX/NzB22cr6UXUVTbflwJOV+Dbx1EYYLOX/CullmMqpOcf
H1AXn0AFtzx+vLGIxTm+ZIY+b1rey1ua8cXls18iO/C+yWk3raHZ3gkqVEVVEYzG8VmN67gIML0v
9h7lCt3SdEzcTdU84nHlxTQEHp/XzpgIhzfBFGry8+G1Yk8M4CL06VVJVNz7eO6nSvBl2zGKoFnm
orjyJAAE0sqBmXinQ527g4mCVf2X4bQFAlTbo5d0RTpCCa1tTr9eDBeoIuK7t+1bxz1eCy+aYPQv
10LgOyDh/Xy/3+f863jGU70KE6APAWfQPDo18BjPSf6eGsN9RxV697EVau1/KLGIphF7fZ/CtnGX
N4ZKcvPZaYU4VZmwrd86iOqhfD+p0dXnBVKvXjn7pN1axxAIY7G+phfnynv9l0xYz/4nQgrzJ8oq
O8lsU9SuPnxtPs5TZ/3Tq4r7Ns/gF2FFypvjedixloJ+H8Ta4sWXS3QgxG41COR+mLMt8eshxNPf
Dv0MBfVblPxiYbaT8HquQOcZJb2NQ0CwWKRcCeaR0SI02D/jXdok4prWozomIpx2ce46C2GJPozf
Y7UUNMkffIHUuBGYr4fNZRwEZozRRXckmDt7O+BKP8CQYK2Ld9x6PaBWQB5pEeNeJfCIJlmdgSJE
L1x23EJMdoJh9bi/K53u63z/Y1JjCPNqCt4eWq53IO/8TFMHxGxyOlr/UyCyHToyBwyY3pziwwa9
8qrsu62uQlUjARGaEEyC6DxRSbMVX0abivxhGvXBhYsvCESQou4sOt0LpfeBnD0PyX0c7XywSpW1
xZ8zk9snWG8CTRWxwTrZhQDuvKnI4XLPRpKpipKI7f4dr2FdU7YHpo0JpQmfxF6vOJU3rKyXq01l
V78NvA6z5OplBjA/e5R2CUq6nE8TTKoDP1Glea9fOdzke75SwaEh93vRURozXV8OwEgF2V0QQ0li
lEztuifh34OoMMllDv07elzoUEaBZ/auwClGogqREl+n1a0/tjkMEph7i0u7PWtfTqOwbI9KoN7i
V5RPphqMO1AwgEgbTSWiag5d25uymW/WvLDY8iHBHhbd5N7T7DcAvxnFGHqJtf2lmBCvMz1SXFJR
hyMxViNWoy2YEjSITQ8NHVxQ62Th5iRe/vHFjqBh5d7eiyW4M7pccex4XRQudcP2GwHwIgrRUvar
2Z+Ha2CIbGHf6iP5VaMou6hWAlh4+lMJU5f+riUKuQmqhDwzrfRfRtWNZsGaA1TB7j/nD3z0X2vh
GXgaUpc/cnTnN2NNXYyGDIpev/LIb5Gh5D5swrR75mUW698ubTvzNk/IPP7V2Lf3KbTviVq8h9t0
BVpewPLEs+4Bq5NSFtB1Q6OaeK5K2h3Cf+cKYdeueZNxa1J1zTEyqbxKNNQIEowdqzTkVEkYM3c3
Ku3TUkNVHP2utjD87eyKYlIzoU1kIwsz8y7S3w1UBeB7Ail17cq9tw4Wut/sWTvWHa5IbQxQJjv2
IzaeDZoIOOAkIITWlQ+mURk4zuGJBbn6+ClMryKu9rbV+kTMMoIF08lm5xoLPEudKkvkc4Qy6AVZ
KNS4XbVUAXL6T+nX0Q2PDoOuA1o1gdNgz15B30tF7m47xKamV9/zkb4DWfcqXaCwU/67nDDJdefz
VwNVxqKsG7pHmLV+xML1aXCzBvxhmKVVEbF3OXtiIiWS6sxC0s6z/VUNLDG9m5eAQTK93dEusZdc
97GaCoMX7kxp9C2bjLumITPIn//zbnP0nrah4O3rrmt9ZhXC+qLge9wxzNsev1lEwIjUxzYbLHSH
YM/b2JvJfycqoBeSdyqoeeN+IvHgEoTwVtj5cH19gNUlKZb9fg2gEzCBSkLZTiNolHgAo+1IwTgC
Dx43ESHxYXqpK4ozqY5XlMktGL3FK7hMbtjoKtjX0o4hJ1f96HY/xJQt8tzSt+aYVgk+lziH22A9
d4lNddcm1ZwgSqxD8A3Qo426n3H+mqURWu8OZEXjp0r5FONVcZ7a2MZNNTle8O2PNlCW0aCxgXRE
0PuzLVzgQTfr0ax9Mkrr7bxzq4fSejpZRInh6mo0j/QzNVPzFMQQ2X9vBc0lOluCDrZZmKxcv9wh
lYlrq9PvMqqwsfBZN6yMz6ix3Figl23tBp8N6P1NHvPmqo2gTBervNh/dVpM1UbZ0PAsWBSp5KD0
EZKS+xjYvtQu+ZYeeVuYgj0rDctuC+zeng2dN0P+ZfsBYdHZlKT/KXIO1d1j4EYS1l9P0fmQX93O
N/0EHqo0zOK9TA7WdHr9TW8pRhxvF/kWap7NQUyaDEgtBLw1ObiJxypbVbwQfijO4eOG3y+4Af/Q
6V3pGqcLtR9ADrKWSURW+btkPqWS8YfSHvlkORaqiyKrVJiTSNXVgUZ7FKLW8DBenU5CcG7Pq17e
GBXeSrpX193EUG2GOKd93lCTPLl4a5CLKyEeggAAa2yDyO7zwFJEfG8WtugEpvz3rJ0spq+uLe0V
MjCVZ9jHbqGByysQVPJXSxMHY7x+trGpj5PFWffYFQxQpi6ZhqoG0UlpAxSvGaQWkaYUE2nvvwJE
SuPm9RZssNdOFCtKsXb9f/Ki4TVtnXoi29JSkFYPG6748rbAXwb/VoOvfTVAwlIc2PfFxc9lqQ7N
B2BvrlurnpCSuf7hmUinHhJnEXnO5w8KO8yaZxPcEoKgKjBBpdCAhbMm9C/36ahogZ/vCjXQcK6U
JZwPasufG0Q0lH4O77pjubYyOdWwNcgSaEk/gwqwzBGL7027kdLvYtVoJseNTVJVLNHRycOwF0DZ
UQdzw2v6lvf03ZoZXvboFuDTeVAYYqeS5fQRivuVXoPNsYUnaTVWfjsZhp033ijhb2IfJf53h0YM
zysJWDQxF6JuodvS26idLZLS3nUPQhY5K8hwFqBuzGXFjRg1Sif9DuQrjRcnrRMz15QdFO9IAxIo
wuUoYea+CD70tRHJrWWaoUQMPCPTA7SxTdAeFiRDNu3ytgtkADjFJHuyIuC+9vE5rqyfi7PQt6JL
IZswZIwnWOTcKbbOE4p7rbAeF2vK+mGYg5M4sbkWmGkhyPxOuPIzkTHRc9sLA7oqGMfxepJelKiu
CQJ6VMtiiLNfmiTNSSckyFquh6NEClNtliBdDg546FF/UujXqZJTxkasCtvoCEcTO4SQd2bAhgPf
ZQ9uMsLJsxV8xXFiwAQNtBYCwgwMTAUAIkjGJLly24ZyqiSUTDIBEh0SNq3NsLHBDBLwj9q1kRCx
UhKowQ1qB3IO1neXst1ZZYZ6LlLQkyZJCG6s9BCLatxa5AqBfhDZGKjiaV9GHVwwu05r81Hw1BXn
Gj/PBnqEs2Mt46E64oYCm3xM9yKxTjYkjt4I/Slj2C8ixJs4S1bE1ilxajjsa+ruYUQBHmCNF+N6
nTVr4qyKBXacwbagiAJu24Zry6FK2NxrZmCPDBOZdltvh03DlaK7grA7OkiD/eX7qVXBkuaHcZ8o
21myWRLTImIESwUmM79S5K/lt3xfsZ49mtZSFzP6AvqG8nQ67QFqPohkEKkjHaB5Rqa/yXWEzT7t
B7QOQFBs0F2AgDcvUv5mFchGdEXRQB/2NBkoNalj0Fm64D2uxyymXyOoypF+bi/1B5e0nWJW0MEq
kYvlMJSYWvPJZlyPGlHe6iW93vTedqUF+gFakpD+LRnHGWJVbDc0JA0vS/BAi62x62LBt35IpfaC
awGOJl29eU0y3zvNT1wc+QVmcOBk+2tqfMO8uDIXpE5/Qgj/ffrNNDQBjlCWo/4+/CeGDUDgQEbr
qMYGHmCkIs+j1ctnaCpxlogMIqMZwpeaJpLJD0wRqT+cLKJTZ4lSjcIyxaifq0glv0skiaxqdz/s
vkUtTfGhJBEk4EbhOes9Cz0tHw5MvYgBliVEcIffEeIR+56fkv8tEiZLQejgHl5yJAoKPgFMEzjl
A7sUew88XxvKrKT3LMbfoTLIwcpZC+yKxlu7oyy3Pbu4CHRky8FSVUGCXYZXBNJtjuBYemA47c30
S/XY5mgzBtEmVzDrTcH9zrvDSxKRpnrAumc8Op1MZ10U5286Lp1VkRmeZo6ok32EebHjlc/Ma+GN
VDwRjmobvQYt9vRbRRMqjIv1UfFzILImxjsOsGG9GtzYlWxNdXeAQpANoY67sLcIHP1dbv0bGCYr
VrMCRt2rRnJ5yqEF843JhO/wBWb3zm+vAd1l/IJrneIAZuQWBvMIj4wf6MLgUTv/E2L0xcSkIuow
ZOuKYvbHac9piojo8hvZYqvBTehMw2xjCTlFgnTb658gw2c6rHRUuPyO1Ad5wTMY2kINBBhNGiuH
b8bFqARlonbcQmW0BUyRfrjECCRyoTv/hT+iR/IcMr7SX7BqtQtsV7tdCOdp5ySBfXDFfrmEkRzV
6xZi5EQ7oREzSMLui9WmMnmQ2MuOrGukxnwNN1UNvt5q9zaydSYB5BBM0prRsQHroZqxUkU9AxO9
mRzOI8mVnGekrzDjItrPJecAR4szlvMXRS6y9VQwdWYbexQXY87NW6/gG3P1CWkTmtoD6ZhPSeCC
kLK1V5a8Ypy9ZYwsDUZjTHW16cAmHDWvYUKpqQYHPCgLDDd1mEQt0Z5Gj13oiz/xGGX5T9fLpWG7
kwyvQovWaHBmVJVPRFeR6X5orJVesClrLhAMle+ob41sj7wz/r4aVhBQKLDxu1IJ9IiJiydytW/t
kv/4Kc+eUlS7BeG9Jb6ztiCOqA94C8pGTPnWG3x2clNouiWAw7p74kWGC6Fh28ybW8mEir1R6GGF
0HfyW+7KWdAFxbDrPS+KAX9wxXy9oTGK0sJ6XfQW9WvcOEVwTHciYEx5siofl8KdlJ7pPEAsOITY
58uaNnnnlAygJUf7Qbt6e1JcEtVZgfjfqBO2ajonK5HTk+SC39SSkSNylB5HNsLyfaFPD/0VkJTK
lL1cbDXKrZNV+HtmnWruoer2CQhiNrewfUqZVya+W9h3SIJs9S5nNU0xupyifrzW9iKlQrekCYnd
wj3Zuopy7bhssj6ktDcCTdruc90nS/K8XNU05diV6hzw2BtgQrNiFZeM32wnGwmPLm933HX/NeCb
uQe5qcAYW5JozH5P/LFK8cmtmWDRqLhl3lqHokiOsv3kv5CojDL6sKbfowusoYOmWzJ5xhctA5dM
kI+OIkgWJxvuiFmlAhLJ8+kinWYZWAbw2qHCrD0Fcc7dwo8SKyihC574UxUeogoZdt52Hef/jKYX
XMdwdvBM8Ag2nOlK6mXS3Yqijpvn9kA3Q/7Xxpbxbu3wo5RZmH+7jTRJFHr5j+/qMceioDZO9dgO
5rZ9S6hhDj7NkNGmRAS4k39fzMiJD/ahbNrQ/x2MntZOD0v+CYgVr2Vqdr0kdFajf/d5/bygLKEb
p47pwUQQyHvWY+BhtWcfq49OKAoVY22RnKVPjq6MFmIq+wGFJAm8upBb8Eq5Xiqg6FaO/S17sn5o
t147ZpHqAKr0aSQvCkQ4ta6EnMS7gkuCuahGJ6ebmO0VvZ399FZAgB8Hmlbdlw+7TKcNTC9+shub
kNGvls5Yvj6mh5/ZkGhbtvR9milNzHvmNCY71X1JrfO5GUYku8yH5DnJJgOK2sEVWAEyY3l2BITE
glYG8CKC/f/TDZ4OeijvbL7TYU+cn4IJIp55HPsRjFirrdY+syMxCO3K+Z6HsiP8g/raqnDWBRIW
xCxywbmrsNIOYDrWyVPF7l8NGf1M0hakT07rxY+ar17IpYbcujxwbcRPX3/3Xfr2tfXE+xpSqnBn
baHUUqpkOTYLkkTbXXypUq0f8avlWlW3s1pYJJEUtDUvkBOT+lNJ0rlSSECPtt3Dgy/rqnZJbgFX
wOKuZaYIPJQ/RL/fcDCKV5YBwr8a05g7xFPjXPeprpMa7qDbHUGT7pvz957PCOFQW2gQIbCD4czw
qknowOYfVlnwsGn4T4Sks4KCUWfzuS9PGY++abYYhVHTPDXWIT+o9eCJRYLHzV20AHRb6PhJo7FK
KLc0Z5x7CbcTqbb3nuxNDP245DCmUQfbAUwYbjmtAh0jKRT4xc2BqQu+jSGGKFbyXzTc9YftaURn
lamsmXaRbW4jAFu0zlZv/n7mJqcTkbCyFQ6PRlf02ZgFT/+4ME+DkSWqWrnGvqXQLF/Caj7yRVMl
r2oITC0QpWsAv+xgp94B6bpCdvN0IRL8XvDMwQpZLfNjKDBUr4fpx233mf7BPYiPzERq8tFR3gVo
yJyeaKgZ23+FKE/WQYEahO3V2WYBumIw6lGT2Gmt1lFTEnXG9/LQLadpX6c234yFnN0K44IiMH0z
nZMYiwHlr241hEh1bzel0gVcFCm/lVNtyo9vIL9dUjs9+X8v5FDaLi2E6sDbEwfwxnFDLFHuamuh
DGI+Dg9UiFzzPcGiAzltwOZ0jXnqT9ira9Bt8cye/CeyeRoZUyp78j8ko3adwh+wwCUnLOr2zCek
ZvVlY9KzlMjcDs/9nn4SPc/3DDpujD/aHbzGvSCPdaeKCZyA3VZTpypeYeNqQLzGg+Onf7HGvxBJ
i/azI1BbDcrp4+Gm9S6CM7ZiIuL6mkCmh0MoK+ADaISHz17Qb+pVzH/trz20tMBeiuPQ1Rymldwi
VXdcVloycG/TU06tyOKz950L/E+GfEoKnoEymM9HjxbJE8PPjeS3s4jBRuih0IBLiPgobg/W8Vv+
UNj2ZcxsuYug4ghceqEiQTaZoay9lF975pym25fHswFVbExY+Xqc2eT+yrC3oG27/4GhdQkC0QZo
nPDtGmEKeqcuASMGfpj+6vPX2L1+JAlbcq9yYW/SC8X7Z5x1DSI5j2LUhgpx7YCm4inBfKnJ4LSe
WM9LiXDYUm3gIq6T27e2lOyActdLNBInPTZ1eoDr4b8YZJi+/MXcSdxOvT8Ib1i6WZnNhln18eZt
SiXp83OCr9x2dmc04CoxYvgwIqmivlaZXF4d2q9xfTgQLldw5Ls33A7SC8Qtp/ylW+hYFNdqooCc
e01blcDbn6Vc2maxqO4CqDmKIE/aDUdKvRm7h8vcj2TRIPOXRYSvF+2yUa5Y02W0AXEbtKPZunv6
yQxBVT6aqGw69taCmrqLqf6Awy3Rc4Ks28iUPtodYLo+d/7/IYnYX/6AbaU/XYcUwEMlcPT97aP4
lxYrq06GumH/ok3U15I8ytQwDVSN5j2PgY9pbMXJIUDcFlgKyH8khF+eu7gIY2qkx/Xd02HXA8Xf
/OOYAwTNGKjngQxz5PwRB5Gpvwz9DFWDoRkeDqarpI47Ql6NXb2kxqVV3aOVy8QT8NWpFA8QbCh3
73RyQhIlrcEMmsghuHS6bkXurIhoJYgQl7aNj22k5IVU39n+WSYwVLSVw8NcJDoUGcBwV5ZHGHSN
nIED5wOVrk+VDmmu85Fxc26qGdS7lMHLhqHMVP9xkLpPoiBsaUB+BbddsAo5QrkiELuZtcqfG6vM
7xgjMAatgVe548UuJ5FGmaFWrgZ56xMxmxXI4Zm9z87TcTRCYCngr1Jt23cnNGX6uha5SopZ48wf
WqYyk4FhV9SPbhlhwiunDWurkbGEG+uN3xSW72wQSLB8zxQBZMi1P2DrzT0oSUrO5D6Wbzill+TR
ZjkLlkFrQyuSsACY8v7utkhJowq03CpcjMM68FVVlu29DZOQuZrCithzrMOu8LKjYuZHT6xfMivs
66KyS+SNvvwBaWgKvJ1uJt6DG5LCpEv+bAm/URk+z7wm9OGXm8w+yD1fJu70VA5+0o3bi/GAy6Hf
0VtnAlMEMqmWCJ//nAOdDeONQHzfeFNEz9PuOns7Vwgs+I8u21UNGs3i5/8cSPAXCX/dZNN7Sd/O
5p4W5vYyn81ZikVVfRXnlwWm3j2fjdV99WddcMoWOK9V5yDzgjvdvd3Ra4NaeXlVefo3uLSKqUBC
zgomF7awy8aEufhZpFkA79UEwC0ZWi2FfuZnjQMdWKAz9h80xRITJTLmcwJ51M8S2zzdQdClo7SU
pU9mxcTcqRcOt3iMuSnt/lFX0mE5PxxDq/0tfyclrlZhDWB6QuuZ8T7oCuB3e3V+guuTDKTFUJHl
UeWX3sUwkqgtN39bylI1IgYodzj4VVNLXP3EEoLVnyiEGzQ9thY8v1kmMWuxMTS+Zuxr3OIMmkN0
TLMES6B21LDkL+NNDylCL3XEigbRoQaoaQqxJbHAwiDV8i3U0l6xfNS4MAKLKbTbrKwzehVk/Mq+
s1OPCIHKlvaY4LVONglF4rOsDVIVzChCVBlukZuevbMOn5ew9xw+TCXn5AJqgKfbkTEM9UtHmKw2
ZBhCKFCpOtV707VJ21iA9Eogk9c7UDzlMcOr6WCpgnF4yORhDAS7/lAHsFWJtdRr2QlsvJ7R+RSl
hCOAs56NaHJWasD4qNiGVX7sT1Szaqti2WWZcNZJIkqulbTedycX6xcSj1s6/4Dc1Oqp/Ik53yt9
10v7tuzJkYYKPmiNnxffEvkTg2eYCXwqGlnKwu2fzjImV5Zn9T3cDsGXA58AWLNHCMiuC/XRu7Kv
G63fRHfQR2BlzRBp/bcsgYW1tHk06geX9TSbcfcLtbi0ASYUKjk3SiGUUTn/xpHQ28cAnjVNzAv6
LHyKXtyktOA3lvKzR5Dl2jitnTCZFNe8BjJzUwSdjaiJf1w2dvk6jzgNu8jmLNs8orfVvovKLN7j
PXqHVMidqoVzwK1YX3QWAnAJDL+7ixh+FrQxlatxSMOCE/lARCrQGS7ehwknPQl61lGVPabIdQ6b
cm0sCkKiaOVGy/9KvWS5sUUdCLWDmpiS5qUSNsmJvBzgVf4LiUPYm3vvu09wYhLx10saYW9XeiPs
YTEVS2tyJnTyY2XoGT47zCiR5/3JIPEKT9iDzKxX/hpYRd3a1wnHwjS55Bx1htbg++tZTtkyyf6s
rmXdY8532qK0gRcb8qlzmgj9/K+B6fZ/TU41MTbhG5bNJuaLALpsNmG0jemsO4K+QxHHcJ6pX8pB
U4QWGcyMixOF0EhWy+ygX+r5F0B9eJY0TGnoe9FyfIEgxuTEVKdhDhhALa3y/nhWgQqLY/UMxR5n
xls+fZIbf/GzKOnF0ZykHIMHhIAfS2aok8bLCc4xqc7X7g35gRjzj4R8NYTm7TjVWf8NUePKWFR+
R26PLbNu6AzZX7yfgSkZUAo3dsmoNKsnMdHeOEUpNecjpuz2m5i6qMHKU+69FxZyqpC1N1KtlYwD
AJZhObZ1nQyUDnBjdtpoE337OwCyg61dpm4UFlCrLEh17Kqz2FpiFGv03HTP19Oc0fWKa8y7RB8q
9he/gmvHwEmsi9grC7qrLaJLyaQZtxCAWROhd2zby1/KQevDJGvnUhdO03+GJGBhd9PcEy2rzIYe
ShJX6slzshQMcoBPVjaP5kzT12Xssu33BNN3OHId/J1OsW/x8PsbQVrd4Ox7a7p/InGmO1rupt3d
pj4NJRKrRIKPWLzaTq+8Sld+JpS+4VqIyOXm7m1mTo/u8Q5SxHXrZFdpu0gcTaX8zrjAAOnIOQ31
C676M6reGgdVsqMCo8IrBwWcPYcBCXiybNyd4WO4Kra6SGaV1eRCr+66Ro+wALdt1nhtxpoUbCBQ
qLcK1jry6O34AqzGwbHJXrSsO8v3HYEglpabcr7frSqJISVIw+TLPpiTbwYCKu4p6o6UD8QHAVQX
OESNKwB8R6rtawJq4uiCmpOaunpoo47vVfEhBVdHbRQQufD+vLlKnSEsx9afcMz3pESmxOIDpzMp
SakCyM8C6GIVruU4D4pxun22821dOlyp10SVR9R6Ln8UnztLoVHjhLRYFUsUfUjsfyZuMwRVXVyC
5tR1Hf1f1imr3oGeojQidrGfxo4/WS3kFTbLvDRNoKqdp89E+6e5u7h3hVlCUTFTOsptFu0nUmoZ
ryCHroslxpyWP5J/foWQ+nSJ/6it7mlVNA0k38y2J1VhQ5WGJo9FJ8gzrZuQTUhJRGpJvnd2Qcx4
HC3PhGNCIKmbuBEgugoAr2jIyBVt3yRcc83fA5VpItEO0OifzySohur2YjxgOa2pHh3glnxgqrgs
PppkEQQRkmxz4ntpsCjGJaOXi1JvK+Ns0Zx+Sgvab775j/K/q57KRS3Sx02wSJSRbT/X9VWsLXl/
dJjaBzolIk1yrFrWvew8kB434QLQd8zTUGYph0HhQePT/WhA2s4G9DwqBHiRcIHc8ccjRYuYJg7T
EpMBxNZVPPybuwhEkKLWovWepaRpPluE/PdliDtYeOei6bTArVtakTL+XffKE5xQ/4lNnJit4BH8
Rzoo8YUF6+ABDi7JmUunehfE5Y7Rc34Q+My6LeoCN8Ej/kXVtV79rCDorudPg8fhAFNzcSvsIOif
GJv8/JEsxzVhm0WCrt5GTIRmcgDvrIOp9nGHbq6QZLWkKHM8ZOlHwvZPepR8j8CBBn8H7zQbe6Rd
WNi+QzhEjM4pgmSV3IDtHdJHGTs0biHnMp/5n403WlaXGCUBvAHvxwwP1eAwxHclKphcz22EkUZI
DvLmVn39ZC/H/bNaM4e2ajlTpJX5O1pB98oC7VKD0bwUq/dRhNR3E8oY4RzPtpCm+C/5ikLvLIuf
B8LnfBrQlRszBqxfNLnCBO3hzleYzjHEh/sUcDVSrEYv1gUgXiWY7nl6XlcSg875usyqwoig8rSl
4gucT50Q092zIoLELHq4yJzuwVr9nF38Z4A+1NHpDyIKZvje5+oTT3n+5LoYjxclleVNz26V6Z+d
GX0Uxstpd5JnO/Q9b3IYKFjHCvO+4pSXN9Sgkyt0ZmqJxg00VXt9d3I5FKpKzOAi3wFIxg33p1o4
WpmpK6nnLpH0G3Mt3/bEkGd36u5efN0ifQe7Iyd2moFelbJG8GCa6oWWDr41nXR6xAYHJYhu6HXx
7bVjktc8dnKQW/lnhjPm069KJcrzrZ3AJhHqPaOUG9MfGxFXVpArlcZ9Ko2FZNGoWvK/9+XwLtUf
dTcnBhpXqvfmXbSrw2LLUP3+QTKY4WQQTLJcJ9Pbd6KGgyfrFfcyS5fvBboXYm42j0IrjyCqDUZF
Rcb0Xn623wyt0XGKrk8WAFhEyOTHgBEIGQ/x6g8bhZ7UX5hUq4ZJLjlB+pL112imxJeOGD+rBjBJ
qZ5u3TjOvnC9fWiZnnFV+GYvlbWHMAsYuVsg9oEjQvHhb0DbfP/TfZyluj5ihf56R8guAGlM5Y8S
QupZ6g7gTeLOxTyHqZPlX8K1dCsiDQcFz98vBO1GJCyNwnjrC2DwbW+53san3EObwkO3XgmagI1Z
ZEOKesDc2e7NCcgeHdUiyve5N3a+ONbcUO8J6EBowtPCPRxMVeceY7gzHn1I+hiN2Npj6CsO+WCz
mETZVTAESkGn4bEPoe5jSV4Hrsr5egMGD8BudDVVVTM4FeS+7oFKKGwmNj3qUl4r0LK+6/VtaZmm
+adVLBuTQgGPBdtXh+6RDzDGpULMBPN+7LlfsF0hvfQVw5Z/WUJyOXMqohlhWkkXGqKTbN1Pp1X1
zCRG3bBuXXQ327fuUnSP8eeHXwUhW1kAxxUp8IbMu8ootSzqmCTB//lVRcuD7bK4HJjRJaVGCgpD
iuvEI+pINchk4VBdYPCxCvSycbkfstyVKuUwlRFK3UaToGi2WZqc37fpWhGxBL5Jb8SY8j0gX2kP
MFinopdKSxagt0nB07iVscEvtuGFAubc+JPSZbLSUzxA262i+DmBgWfHdzRAXRSQ0YX7vTtvkT1Q
7K3udJmhU4kP0HPLLEq9hZRSRcwqjhCiY2SX94HAHSD9dfBxEwKA+rv7dB0cs6XJu+QAOYgJABmd
4FKFbzYuI1Fag7cI/eA+RrYdA90VUSnWEPZOsCvgPSywDLWxivDHGhP4YCrugzAsjOJjeAq1eVED
ObbVsutPzBE3TG8cYfiyGVyTjEkPbsoa5w5YkUNsJkEnDYHA8tJnPMsZGb85JwxJzizbEgggMgOe
qj+2Y0dSARDvZ30aoUA9jyo6R5skVsyIW3F8NJXi36i6STwGB8NebIVD+76L5pShYqfgMsEp/14m
70G57JpoyxUwvcWpMEdHkKTOBwvyPzlRsJonZQ+lgdfSK5/rTC+7z6k6WVegEYCrpKSWZs9rp+gp
JaElvDdQ0EuXdkrDJ6jpvyu891XJ7NjChtpu1Orn8buisyy0sT195LrJuT40Hr++dZEQiASkdcis
BrG3oiTj3Xung//Ktt2DJDqniHNKTBpj/DDsqB136Z7VnnH6La1Lb+UGQHR0n8Lz9hCMjK5isMOW
GsuVVGkz9wFl4o3GLIgXqJb62QcHHDZlQe5xPXN+drPoN5RXe3ziPze8Vum2wacsSw8ynxRJE8E3
l/ceITLYw2rogIGrtY99RWeDrZf+QFNnZOa1U0/AT6K5S2I/MXtr37ElYNC4dyF/9jEr6cV0LoZ2
IYDGl5FupPMLn5WPpeLqtL+7vD7AEAoBot7WhkxVm6cMfXff2z9zzbEZZKGlXlV05H6rcNCtpN60
iFTmb7L2hxb6CtFGM17pg7NCO6/rORnxKBNP4MBnD568J3f4ZJEA2LyohaF/SGeiPhkOj8LK85ig
8vNI33SJtRak8j8Q3akGZ5gIYaixIFW7i/ah76z6HKrYoN7S1brD3aad9ifySyBqwzEMyJD7MMDa
JmvBNs+s5t9PB/TzH5/xSocPDVQVzHNz1WRbW0gNlXQfkV6N1fBUsaG5vHda+HkrFeVvVSfDy8Cp
vkFrRPBBsuu5bEFGM5OaRWSZT9t+ve7LFsXzzfvmGZkK4Y6ZFXcr1Ea6xHjPk1STJUa0pTv7leOw
zFRMeJH6ftwqrdfnNWX+yHENWzxZfXO44537YWJ6s0sNVaixI3AS2oyflqUjFdGgFJjaw3r+YGHi
kjX5uQH0Se/CwJqUY1OQvRewn7xMkOJWpiVLtSMgJt2Irrexo/42+hlPVi5IFm5iI6Rfp5+isigN
6MbSyvRiQroTfVdRNua5fWKstChXxrg/Yh3taJgWSH86q5eG0Kyw5R0CbA4/YTVijfX5SF7qv2os
KPEArsmj1rNwVmeLWNtvbrxPycnPBO4pkUrHyxPXyvq6UyAojJWb3LIf96PImGH6b5t/NKULH9Or
EAQSJCeosIYdu/Fg08fWmPT9VW+LS2oItjdGwR5Ezivp+ZGdIPzA1unOYe47b8SRpB2bhyKIqGDu
gGEG8gcZPKikalgXkM2AJuAc5vIbYv9MHf0C1MFFzIrXdN/A0MozwIE9dKy7/am4OkKUymeQiZJl
uEb30bmEuPX9VFGvCe1RWWAL2zoWx4BZcbuJvXiWGwWHT88mtIJadt0MsQyQ3GEcxZ4h0g7V50+V
Dosb2djxrPgOSMCNnJwFJ4EkUSzxWff5ckguR4xhAM9bf6mz1SqQ3p4J3QCQ1OG9T9zQz+PtJ+tY
3Peezop6NTHwpuGjwg2DIf4mxUx1f9pej8N/K2KFnmPbl84SmELTa469YWrr/C3uLBCQSDBrq1kD
3xmEGtWlfjf+Olz2dKN5x68OxhaoErfwW/zcCPqec0fVsPZpiMzDMaRvvdEDEemN2p1fulSd8PuV
ikuymlV8nSsIo+F3jwgZESNeTCFivXepRdT8wG/d59uJyLk27mGe45nyPSJHS+8e+ajp1YuToqAK
qqPMibbkA5MBV87ixjzdUQb2wTE3OcYJZS/8oa/2zsh41Iv3AXO1PYqTwDYmpZ61RguN2jLlJwuW
1birMhX9oU15ItFV3Iy4o0uLgi/8GXzEYOCtfDLFgqwNIMKHt6IlDBWQeKon0Yb6lcUQJkbZDGRn
ZRqsJYMRsNs73uqml4zoQ98OwDEodNxwB9RX7H3Hf+p5nOyN9g5yJ82VsRfU6jcpUSM7JwuM27QN
8n0vtwk2Ff0ee7/EZEDQVp/s91Teyfn5wNeCYJEyCf4vd0/WduCgVIXX0C6xMz8VPUn13t+wRHYz
FaRDfPYhM11kR6a2AAPa3ZpufqaXISgeJLDvn3HnflucxlZ8Pp5XYW6ti7y018Xn6+1rDAL0JZGv
/GeunC4lLYrpAw3yZNf5Iir36Q+UTCMh7lmb0NXLlI8ZPyCImdGMX1tbk5vGW4TfWUH/j5d0f965
0veRI9d8QSU0ZUc8oPrO2bdbP/InivvTaRuBBaS7KJxVpZPN9U41L85EulGvGVYhZFwgSc2i1tDz
SAHJ0PJuv52cB5QjwNv7ZLiBxQTgzlqxR0JvTbsubXwPagSIqFtl7nBLhMUHa6jrvr1yASL7SOix
I6gGIA9KCG7sx+Iz3M4TB0qYwVA3Y+iD5kjYZsHQdjdeKK+fjXephNGp7uMd4+I31ZYsQ3YA59xv
HShBBLRXpmUYHGdhoWRO9bxwF4xtnVvRtaLfINa7WWc0yhjexvo62nI1F3JDEQ2658JNuKAHdTOd
3x4x+L4k8FIeHZyRrmMjC54gWzShvK87itANizWNjOhDpmytjSroCQIImOL3zaU2xO1THYCkWU0W
pD+hwdDF1T3Nzv/5PKKhu6+oCjvv60JYPjqNYQyajczeGjRe/raTEmKKJujUOwtAfRw9k1y3APBj
7XyjOKUkOUZZRK2UjpgiyXFCgEpFkUZIiOJ9VSBV10wwTOkgxn/sDtIQjZIGHD2pINPRzTv+c05U
TDuYQHkWL/2IfuRePhaiji1VgrhPqm5563TMOMf3dVNqKLg7FoPLQ2lQvxU1jSA1MCKzMkTtmVKV
HRG5uhPVPLtbHjI6S0dIWzKagS74vpBCD9RSdAGK98AW3r5b3wwGbhZXLaHOeUmvAOAmu4yrIAp1
jjSNhpofwL5Kmhrz/4TQOq8whq7r8JrJ07FBBGs6ybNOBim03HkEmmflX5yBrgcX/CRfYPVYlN5I
7JP7x89kRD/dHV3Tug/gfYuFCQAvXTNJ7rQyBGcxQ/bdblS2BwdaUUfT/R1+lek1UjRSathYLk5d
F+CwMqiWSq11Oqv/pDY0Tm7C7I/ma+3jJhk1zPYSbC8PTrmQQRq0e5+coZcEub4HE/vLxVuQ4ryO
/wPpVhCH7Z0y/ldmUDHKtmuRiYkphBeMvhhtwEIqfp/WIKdS2/ukrNOChqIqtZ++mgVMntWvQmZ0
u3/QKFRGtX2/irrM3qTrM+ONP0+XIHK5WMWTVtRcPgJECDWo+UHEkBkxHVPCGDcu6oE/wEZTRamy
nqyKbhNTDcNcGaFXwNlNDgKzYBr6pDCLZRvE0lSzyVwonxV4o1JXcOj2dOH4hl3rQqgb+JNOYklO
FrxJ0/C4n5+hpU2m2raUzRUXtbaSR8+ZlY0h84SeRFSxt6FXGgHeR8SQoMUHmomLr0jbmOG3Zc8s
VakfCfCZXTxR1WXVikvkvaQQOaSp6lmgDFXVUOc6GIUYnMWCQijky4fQ4MrBEbWW1zPh/F2SwtYE
wsDRuhMOh6uAR2k0Kq3sa7MdL/NY5GZVLHkpTzcHrhfYPsIBo6sjkdIvgucCbex9dwdtnVyvXXnf
E8R/c5zwZCCpb28yE7DkqMBKxuMlBOjfuSF2cca25zxDkccqVXfJUJUxt2UywxIPs7Bwtq6eg4gc
4ZsjlgaXWnL2C5O+tKHEIadKVUs819rrqTrqFUTXftrpOwXiCG3f4CqhMyZNRwUFKKMuUnKIQoaR
+ISRjB0Pe0oQq35tOJ2c3NWorXB5QHUFNuoTr0oHEGGTWPDt3quFQiMQYwAQl70wgGNl0DYVPCrQ
xi7+KO5ObCrHpWD6vGPh2mOhKl2Q/j3xaFJJ2LiJ1pNXRfjzHsaeciEKDckFCNIhqI/i3XM4avQF
+JXGum50HWrlIIeuGLL7EKciQ8Hue7g6sfO8ZBKfYnUCKnYhuoXNvwGilhAZDWYh/pKVnK55Xg7v
PLyz0bS/nLmwpzMuM5WIjrMhr10jzYFUaPhUrtVgAU3ckwbA2l4CO336T00PE4rC6CpSYpgdYFyt
8p7M1bRkvt8yoq7XsfVyWlQGwPD4fyCVt3pqlOGcNFS8vUVR6ft+7fu/B2AewhGYgVmJmB8qa6NI
43qmB63Q/FeZgFmau/X1sMqt1gw1sWCA2l1SEYK48JC5+eOfVg0FkHQmtHo2UkND/YQK+Dj0R9SB
EMeMBE1O4Wp9jiK4E5CF18BEN4oMnVfeLoyKxTzqiF5N9SQKMDyzmk1Ca2NHIMT9ML7l3M+bsv7W
cofyWbkpFFOlBqoAbSlUBs9d6VYIFvtKgjswbOT0p9/QjHfS8+4tn9dqNWHnC5qCF32dZLQJ4pwb
GTVHz632r/ze4pP38lSc8c32DDk0JROWbRH83XDaB4+7M/ijVAXrLtasK+zK2ugXbj3kmDeuSvgo
lR+7n4OV41Pl8mYiZp/PLfXzDq9BGw8NeLSOfAMKwZeieKRW2ua5XJaUtPFyjEdbF5IqTGgilS27
3gQXeuvfIcKSsFiAAUjfwZZ1nbhF4wG1QX7V5T7VidsJojFTy83kdxmlNthYShP2165tmL41dupY
63iuPiSEPLR2fi1ZQGSHUHq2H/g5fk8nuA74WLSoXdcowXzzdZxns9h3VFb8njRdhOuMGipMSVKk
3tTVi03Vdnq+CeriRQEjZb4zg8t9xs754FjuCmzYvHfG6HeKYpI1rK4VMBYrmh3ejKJCopvW7w1m
q5rDabqRGnlok1+Dp2ON/3YVXNrmGjlY+Cbj3QSutvT1i2WK16erlErlQRdhshs/aFBF4ZNa1Xr6
LgmKo/Jp8M5lEtb5pbirQzHrbUfcJ+E/pBgtgs6PEGKMBcF8t8JY5hxTIemjAkvvVlRzwGsTU2PS
MCApA15gSyrVhTrixLOJ5sKZ8FtcH60AfYfQa3ALjM2D3GQ033mcI6QFh33vKnAsGwUVeD4YL6ML
GnRiD13Vr7rDtQH6WODRi61YxZ7agSc93K3hd+kdogGk5I0N6Px999VappUMCqRtBuComiOTbYvt
b10sZR5GP/BnydBIolK3mHUmZdJ0exdelNjLH5d1x0y3bxrLsEWwPWAUwyCS+1jw7QRLjYcwc4+H
+41SQs2g5wHO9fI5kbsLg4hoT69l1O2FAgJlo0Mn8zMopbGEkOOnahWdfkYraj3Y6CMxhMz3cCxo
FbefCqni3T2lTxjmQuWWlqtiHZyRY6sRKxKfk81PBSPa2VBZBdBQCbzPIJpdHrojJNVr9wa6M8LR
jamWfkU4EFjqg5WU6+wuf8NYvBLi5aN2yhih4nLLmT+lvqy8EjMkZM91U/rlUA+VlAfSooAUCDjd
Z3S4fGAE9dBQtR0uAr6xzwIU0Rgz7cqsdUmua8BuCOM3NU2zYmczZrmTE80ZFovTBHGJDMq4Nzx8
YbODhV5+scdYTUfql3EBbZ8q7TbQ1PHN9TjExQTON2Kiw8Atp1PNIZd22KpwKNv9QpgPWMVzgt8I
VPC/atGnG3VIYr+mJUR6/NWuMCbrtp4WToEOITdud8iQV3lyIebWzr2v7Z/f66pvK1jfKfT2VBSW
zytunODnphxk2PvQ9I/mSMvcnnHheBJywSX4Oca4ZUBPDkethclBpmEsTZrTlYI8lRvuzVaP7I40
p1kfHfjLdRNAE0d4nPECD7HK+0qYkyDURvsMFF0uriVH6PGSpuivkiiyIvJrohj9JKIEIogMXPP1
3wdHNjoiQFPUEvk3OfRShsGBGBGDtjx0K/byBZelDuhkT/G3QT8qCybRTx09blpP9t6msl3phTsG
queCNiAyDImz+gvCjpUZOf2DpQCWbH8SPIu9RybPmiFZMJNkOq0Oe+bTHhfqoi2A/ndJ510Qp+e2
PsFTAalqwdwCQGU84vmeBD7asMC57J2TxHxGZu34phMdj0NAyXE81Q2iR3avYd6g9xUzxt2a5zJS
gICFBHDeXvuATsZkuHIYWllDlOhk6nHca6oOfXjuo9RYcpnw7EXNlGZAnE1x0KhU0EIg44AfIqMo
S30xiKSCU9V0DAhQK3hiuQvf3buj68pgyxzgTDegRinfc7LjvmVqEwO1CohNpz6JVpwLTxRWl0yq
fJpvWK+sVEQZIefkk5FYSbgLh1U4W3bFoikPtBpV/d9eBWiSwwGsU5idDzdl/8/AaI7LLSSbmAgl
WBY1AWkBDsNpBXu8/ov4iktfLJmRwCKzPd5twIwNasrUHAdp3eCgBBNbHyZMUmGrL1T1LJHqgMXm
U4/p9/xMq4021ouZkOaCFCaMG10GbXcH8wl0KdP2GxkOtQWX5gueWPoFsCtxqea62fefCv7XU4a+
FAdOo+dyo5hG52Mp45Il0O4GzA2n1uwhWrmV7segPFVW0QvSA1oL6NBkpHc21gPYpyEz5D0Z5pVO
y/FCb4WYGm994ILxNY8E4ztwqJzl1sb15vVLe5F/Lt7jMjr0Nk6k4xgPjmEXVVBFaFH2ca9elbeG
GOPneveRo9E5PiADCcHUg2Dvh3ZasZ3JPOAkHr1HHtTFoOF42N35TnwonVCKK4VeMZkerHENXU70
HXUVh5Gn1VMRP0ycky6r2ujVaxUaNuHndyrUa9ywFo2cntZGIPO+mSkzkyNPPHei1k3ZJ/vNGeo2
s1ofI9K2TsmnhSGCdW6gc/Mf0qtuxhhSyqaHBrooROdPFjUEw8ZMkVSq1yhyYMNOoJAhZ68kLcYw
QUkdLQepEEJLlHZhK+o4HNVyd/Sq9hadwZjhs6KjtNwgqf/bwhCtrwSM5o+weilY9wMeNC0Sk+21
y3VDUGHwZdTakwqOhvby2yYQhQoFUUbZMnGqSiIAwN+GMuYzueSRc/lPUBBayzXD9JMR7tyfoxs5
AvsUsmKTnrstCrFMU870ddlggrIUd0DN5elgfU9e/bx1U81Bu1u0aBM/LYsiSgfsepZwOn0V3qOo
2xyqnnQmpmLgu6yagw9ff030AS3gszSA8d2XlVzEwwSFZqFYESp1SmdH66JQT/OZpfKr7mU3wUNl
fu3FzWLeEjjqIVU0STwvymyFNNlJc7Vqtl0Tduje37DRncj+m2sNwk2O09vK8AfmRVvU8b+tIlPe
itCRN3UJVHJbbt7SQgDmJ2EQCrC4l09ycJIAzbvVdWutu9b55DdxFFAOANExr27tz98nIzecHBG6
m2T7uaVgHwnZAuWkrBwqCH+dSA7EcM3i0ftvZWi+R9UNgB9psxEuWhULjeLWzJ6VhlNafD5Mizlr
WLlF3WzlK8H89zVmKN0Rfi2Tt26I0wNBt6uyMqO/m0umISUiSTlmeQCj0+S47Xh8G/NNwsPiaxxt
RvD8oWij1SvpsmmGsxcRfvXUx02ICZsCWJTWcuExEiU0lZt+gCGpEPE4FV0NwlbzytxXGpoH9Weo
LgnszN4TV0CP/l2B3aHizRAKOMcvYt7l5yCXdXtWerwhmhAnD/W6tRTnvmpvCGGvcmV+5O5RMj1d
mnbk2SkgbnidKiEPKbnc//ww8e4ttX+6iS9+u/GlpqpMs86bRnZdEFU6/4ywM80a2MVUVOQ7RN2r
0qTHufIJIPR1vT/nvnnpvvhU11JoLkvp68aRPzAos9G/kqdhZEyQ4GzZ6IJxzxqaf3BpEF4BZCwD
78qkyKitaQn48ZnKGq+WMFDhAn6JubyH+giWqVcsdNOSw3WVqwGo22qUBtyr1O4U2bXxLp95jgBQ
1xYIudHx3Tsz4xyjRAZxVmgyW7x1JHCkmqt49R6tkMxiT7GIvv60W51JTFTPzeLolmR0NQx8S1lC
UHzFRn1+HAmDEPk+vTv60eAT0lZ50+CXxZLwes68w7DoKZguTanAJphcH+hwl/sEFljquOc5OpRJ
di+VRgFrzux4jL5ngaaiKifaPxO3dqaFg7WBTe7V+PEvfLsMKoOOdU7AOGcdVZOgEm/lJS5fE80H
UFXuTtjB2O143hBRAWKdn2zkAQjrgd8FLQRJ88OQnyd7fiemfCDXhoP/byB7qhPciKh6FG9N9bKh
68fGLJ6QtdzgUR14DWXJ83NRLYC3C6KGrxZReucyZB0vox4v1IQwPzL+YABgBfyzTcmJggIpi7ll
FeHXxncQBl/RxxOXPfEGj+/6m1xGA6reBmECGoUFJNJPA1ee4b0njgPPmYY7GMJiW/zrARrOP3yQ
9k5ZghtygJuS8OFsc0I+LmwU090UQjopKYW4eglBRP5XnabGhMq8bSJYEPKHtQ/k/+mePzVeBgrW
+ZwqdgDX7GIrFq10EqyHugb6aAn6R7sPnN3FBR/L9wWnYTBxKZWmjbLc7A8Ck65QZUbIrh6dLIE6
I7Tr8bTeZbehE1uTftwqkH8HIs0LJpB2kmSvFiJuOwEBfL0Cx0iglva1imbPzBREUaI4ldtXtp2I
j3G1Bqh2whM70X6Yf5nVuWkpYzFBtg4wEGEtFSKlwCyVLYcgGV8vqpC7BA8gO1Kr+M8K4p4vj20d
cAiYJg9HIEYVlLCCQt86FpH+TjY8GyVzO+d0oDpaxuzxoBJ17U8Knrgn+Iu/26SB/zq9IsT0RMPW
goiNUh9JHyEXD8/zYdSKRKOuM7R851hiC8+Y+U3jcM4U2/blOOML6F8mWzw6yFNnFP6FymwqIu+R
Pxa7A6ouH5O09hHznkSPm0zGUblHwbJ5h82rS4OZ/AaC16AYtjt+0cQR3mqIqKrSK/NBmdZQLegR
NoOe344VpOubMe1DtHgJrPeNltqTiIX/xtWL21qbN/7VpodXPCVkfOr2q91Z2ouSdU/DyWELPczH
lqguS03xR8GbckE+pzxJaowk/L+Ak4YzqLpK5pHs6bOtIsvkIJ90MXTM95ftmoxbgGkGSdsPk+i3
YV7+NHTOWlHns1wjHTmU5uOuKiIa9mJchvC0HYM3PTw0LwTyJGeCq+bBaC4h+y8kXuEdxZcpc0Ey
pGgLyz6slMBwKcN9tYbsr9t7GVfcOSGA/1ZeiY8/NP+whFOPzWCAZFo7725FiubfWy6rRuVgB/Ux
0vhh/Kj7N/J4LeIBBMOc1uOHPrEMU6ttgEDxgNGGeErY5XNr2EB1nt6kh9uXUctfjWqDdAw0rtTq
x2MS3tzO94d9PA8fdrjYZbIx2HO5kIDXfYoeWMf7d3VK1SPh90im7+0gUkznPG1bN/jGOxEj1Cfz
cyZXk8s0OMA7ajWmpQHLrNe192GH7Q5aiDT9H0qph155YVgWF/GgSgANVzV0huhj7biqENfC9lTf
PM2XJhAmmJm3KiQVOFcRVcsRi47DYIc9KXkGni6obXojPXu29Qdh5lO0HxBpOt8IW0ooT8ZjT+TW
+FOpJvmBDMN4VBxLifywHSb9mtBJNuWceRaZB2pvXxGVhukLwBmfN9iMboTgjYoYxWr9ZwnWeahW
GiHvYQIyd27Jrx/oZQWkk3vqvYTTbajrKa22pbUOFK+ICIs/7KL7jLSvGXUNB3YS2gzRim8TOoF0
9lOhaFuoSINnlnd65LF9pBQWBvPv1UsZUFn5dEbvKiNtA2iLSGQdRjIiZp+TsY9xdg+QWhbfzJ81
Q11ON96y5xr+/Yy0bIk7lBOzohjDAJNITJ1qfnmCBD5y30pI/3Rj5bv7Stj3ergQdZKiPCvd9UL/
eYRZl1ZUvlUUR1XNN0EGnBX6ntVYtf1mmV07LjEEjuMl77yoCE8JVwATTaOLfyw4wZP96t7HW6zt
w2mZvkA6NlvLxh8ZaA1jtjoIkfl5zW5YsPpAkoNLMgvFt+Q8IPA4oxQ7GBq5Fj/8FdGZMCsMzaiU
PzEwIrFCZTHC+ixlnebpJ///i5iDxjY13OequfWRh5BaZzNuqSLnTzgyxsT2SqKrKPPdsMu5yNNP
2esuh/g0quje2ExZQcF9Xkz6IAvxEkHRnd4R+ne6hnK5nLc9Hn2SQjKtYp64wmX9JNZ5wSEpSDs1
4Lab/KXox+kNogTp8kq8qa6YjGjzdqhN2YfmFuOCDVyAW8cgYRjVnUjXkgeYsG/3HGGrfBSU4s6/
Gda8E3z3xeHT4M3DIBQ91QMzrmH4sBTwQERt8W/xlwc3JB06qCi+SjkhRnDFqTr0L4na/nMYVNvz
cAxJdjjTPoPMya3knigKER0w/KmJTaSLD1usamRY9zdeJ2ZXsKSa0W1RIBlRaqIiRboNjVPUbVCW
ZFz95+G/5CkmaBUYQofCwvN7y+w1b2XafLd0oJZkGX3n5rfui1pjGU30kpxv3UT+MmVhh/pfwceW
GyPyIbZXN9kGjKcDEkCafpWVIfW8GNiCPtASnKcI9jY/583GwNIVMmCd8IpXGFQWnjivzdtI2/fm
T/s60V5DeZP2iiBRjjc3umgrVPWlpWYKAWqBO/XB+eJBuLAUh+dyL/hlKzJABmZfX1EMiY8SCNf1
A7xWjokXC2vgpkJteCapkgdeUSqNZ/UoOatDuiMJkKP3DEXw4cpCwUqpQTYzn74PC0QR5dprsvpt
k6p5PWgQtZnQKRV78cF6ibHUVYIDdCHQ7M6TKckvCvxR1xH2nHRZj9eazRaCNuQ8jK5Itb2y9nKy
8R/fRqi1XRyo6I+RRdzGaOnnasKj53eAoKuDAzyLS9I8Tj4S2t/k4iS5DwOPO4aSQTsOCimLVZIe
N5EvS8IZxZopDPEjr9NgK3PF01WE8lkPn3d5INo1LcMVSAo+YhjFmcI9WOTwJ0UlGsL4Q1zp1mav
qkITj2OdNv2mzv4E1HW2bdkMwUA2+egXUgrvEVQFkFDEAm0q6dtXKns6UOADE6KPI/VqB3S74Ek+
OXNEU07yJ6y5QGJ030J5W286XpN5ixyWVXaSGfozoyI/rx+j/bcRaZXIJvZqffu684GMg1zLP88F
GZppblOzYRU8cJ+gTlysKxBQuHy172IZL2KeKqKdZj0i9JNlKPAjq/OCcYnlwruoHyxtYKkGt7DU
D1pGqMmkm0x9//PU0Xww/y6q+VmIcFCrBeUUL9Hd4tCnFJMwk2yojF3TFGv1jANzvFcGVmFu2pgS
e6vwCdmJE7MVDKtqdzFBz8xmPiX8sAZ8zOJaySsb4+FKDmOUqrgNoxp44K1kV3SNlJQM+AN2QB3I
YHIfYSySsP90sEcuEltKCIsUfPsMNGVnf09d4yVEy/3hJbLThcJbplKgjfisQCtV92/sgtJHx4ob
fkzqZtNOlNMG3HOJOIaH+6KKCP9PMTYoVKl6urJs4wyZiQIHInjJzWtD7RmlmIxcPPAjdwuota4/
ZPyd3NAdJpt9+6Uuc357bWYf39O+/QCcYOjZ4m8K195PwxEf/iqdXRWI9kLPblVDVGs7ZXo4ZB7t
xwjlUe1oghlgVzt5Jxsat+OFIXUaQOAm6T30m6B5toeSfd/iZwMb0S61/oW0LnkogSClx/8DNyyN
Pa35qrEZmJPbUVVNcYJcJj11m72x/SplOEF5gpFBtmVLYetw6EuSpkTKHPkjKf0bjbxIwgHHfyBo
YyYgwLEXN/R7bV/xPFD2os4XgEve4rQrJBi1h1QJlouQKL+XMnu0ISQ68dK096n/4oxhWiPohKnW
psUip4RA2jtqAZKgYGZ595hqq+uoPwqXfI5Fbv4peksx9lwKviiZrEeQ1gYmgssvEdJ9xDRtbG+P
ITt4E32LsMw286+hDGFDsgchiGli2mlLqVcBETusIO0K2QaCNT5GTPue0wYZxr66uqCSGqN8/mHP
L7jVS3VhZ3WrF/tFRHWKLOM3Ee9ryogh/jV8YMwDeNrllY+1hDQN7/En8YaDFC8KPa68CLY6rQci
lSvBF3cfo8zJue5mXmxlMRssf9/VwHdFSgZFqr6vSNqQ8czRBVVhXIqPbkStJ4BqwyHrkECYq9Gb
AAF2x7IjILht0YUfDXo4l/I1XkecZFWKyqXp9gE0/DWlIW9xh962dZg/7g128tD4CILqPQy6q+wE
G1bfQhy4InRHq1GCT2tTcWLCm/zwqec0brB3JEXXqaya9PSxZsiFlkIVUjfN0STcUmlvxALZS7P7
L8IQeCyg8iapgDtcNQNmKWzpJKKF8umGZ9IdqtpLEALcxI0AfXRHZn3laWG81ptKBncGsnRlIk4P
uPpqIr+hhVXWkoWpGY4TyxE59vVqDSb2KPqcojfhB3rQVBTFWlgxKo3chGcgtfoL2GqeB3V3Vven
ZsObRfYBdri7GrR7pN7XVH5BfWquqhevmIeByAtffqpVrxCTfs1YQeCiI4ZpcskiFiQBn3caUQsH
sr98jb4uAtPGvpQxjPk18XvrqHY+/xAjaV/18BhzOELNNOkG2YURyVh5ckQaAY6gGtMKTgf+97Rf
FHuW+R5E6NSJ7bcX06kscGA0BvgyVFrxTQXTVzncDG+im9q92XOg9StwCQ3Tfy6/UQZ+FA/f8vTS
O/z+j/lHUoNs1hNPLYrc7KUHJ/+tMAroyR3NoPzihfllsy0orPMIHi+lOkWpOr9yhN76ErW3htEI
EIlVmCdSXU+M2V9YOSHMNFgi5eco8FMOBsr8N75qZF/zMoAM3g4ZAmpY80HvIfGoBjFmvZmYIEz3
gvvtd8YwZOV+yTRSUNKhNfjYk7ZBJPSf7o+Qfv4yMKxbPv83W078QrIwnVnatFK0H0VDHeDDmIR8
tmtNhvpMRyB3XJmkCiCMu0vhNLwDABjGXp+r4IxlUV7yV7ZJQ+fY8xy4T053HfOWDGQuWyIF6tfq
8qePluqkC6cr/aC6K8X7/Qm6nC9T18qrVIUXtg2PluANYkzVOp5d19NizBFqneC2yOCSaM5p+745
LSHRykyEY0ku6HoCcxcgTRvYQ7y2BCGXWuviNaiHPKg5nDa80gIPvp1DzM4Gg+zqjeKtZTvuoGJV
7axmUsJaWni7F4HXIsp9GeagfRDpQyArXDO214qqBIVqbNZTyl2dUfXizaCOtcz+zpkiG7T6AbgY
m2lNmeQFfaWLGAkNDYukvNKh29CwI3fDDyS7537TWBxatWPiinS/GAPgw+pAtwH/7jYMx8Ora9lx
qdyk0wTrk/fZN9+1dpoNPrSMgmmHtb3aFqJ6vHXC4mze4XKfY3KLVwXux6mHJ18b2f8Svcqa+JwL
ohZ9cHAI4puP0AWfnzMC3KSEJ3QaJl1UOxYEdOin509TAZyIJc9wFtzQmwB8ewcB9hz7rE7VCC4H
J5ZXxRLP6b+mbhb7cHpRJYf4VU2RDevoxk05gT8MDBPj5D1qITsRN+S2GD77Jhuojlj4se+4szmE
+MeMhvvW2NCnhX3VcG1SZLse7kRCFd1H9LObW6ki7GipNk3IKWvAng3/VNZfTl1O/TY2I9EGcaJV
8VqnhxzfQq9orcrg94fvS6Nk3R3kFT6GybZxAZzOCrfz+aFVUBryN+aYzxNOt6z46jSzgP9w/bgO
3HH5Q54VEWeo0zStY6AHOSNjHCN8fGkRBZjsDB5JXSeGt2m3iTGS6Ix8v+TCQSN1fTFJVwLYYqNC
qnEV9A7lk6Gc3VCSftusxxDdAoQ/xXKW94n3XdtBPc+/Xv9JPyHUbxoiQN6XKyxp+QSIaK4wADIK
lucPo/QfPOkqiUtjWLGJcDzTXa/CTtKgIWzD8XWB/NKh2j71EZ13Tt5QYCmsIGMwVsP7v2xrMSkk
iZfNuOJ12u4qxG0DP3aybyjUrFtsegrmNQ3bhb5NI9KKjM24EJXvTXk6hiKVT9l7bQBhmPEI1PTy
wBK3SRWSDX7JfkfMBlYofqHpU6ETJpSVtJPJ24HY6mK8s7qJP3pQCQz0eO3Wv8k36Rw9e7tD70uA
ZomMQVWGc2HwSupj1mz7QBY3PzeRxuzQflz+jZnY2dzCrKJT+R21P8QqupvUDeuGFN4stybay4g2
4ebjfcKRiM5ocZ2gn7xQi5iEkBkhrwWm3LJkp9TRNihstT817uAPXEEP3jHXlMQ6lPXobl14h5aX
k0F0vQS1QoNZ95mpcIagPk4yEa5xGSljKTjNvlm9kjwwokfRrzV8B7j20nPrfLmAr8OgzW/RFBLJ
c6T3kdF6lhkmttHIoKeUhj2dKnAucR30iySGZZMvB+7sROn/AEXTK1VcGGwHu7ap4koB+V3nVczx
Cy2SgGI4S3hLkG0KvfK+S3+seSvF5CfWL15j+SeWIOebOpsyGRfrizArldYneQyaWBiP022DGx6D
SNofBk0H6ooNqsv+Q7QxM0VITUjDmZ+uWMHn5tzUT2MFdnN8OORC4VPItIgCNLLSVjsOFFnkwg9a
pnMzBI5kQyVIQEQ0bdjeQr9QPss5Eqnae/wOOCq06yjlfz3kSVpNU2b5dotWnOP2UOec6MubXo/u
WoWYMeieiHHcIblKqcO8LDIVrm9UjYt7dGSwIrrRY5nkVm9yXycjCKXsQcyEeEr4UtW9nC/Uhhx3
+p+HjAk52tO/Y7rVXnVB8nqUIlKpH7tcFCBLODOAIOycYC9g7KppuJR9KTaS1KdZ7ifw2LVc48gm
w7g+GLSrHEXf566NCBpiF60M46CNisfCUnWd/ZK4mXUmbMeAmpxWNvM40IgsPiNvm7m1TLwaSxQw
KNRJjGYowGeY7e+hF2esF6kq7MqToTuYH/WtqbVOVFscDKVgC/US5Xo3Ps9EpSdEOVwJDbkv+RhB
FUbfCmpeyBFEvY7AJWv5PNwqENC1Zz/BJdkkduIlA/4PFkIPd+4iJlqj9Dw2FrAvXuZHGk0syugg
YO4mkv+aTgd9gg4HiikblSKY0ig/Pv+8eRWVRxABsDpozypRkI0iTJhngnWSurMHfWnl+S9dQdre
gn6gS6PHYoj2wKpA03wBnS7FyZgwjAH2pXSSQlmdmSJs1d+yjbD7On3H+foom9hYVfC77cRWwvmQ
tSmrlmssDMwHk8MGXSYH3YO9YkGVtcyBcGadEL8i9nxESjVmr5iXhFcm9/meMXMd681H0xALxcJO
HWW0mfMdjPtH2BVP24OEWChlcqdKrJIJToUWK0QIyhU6PpzlREiNFlK6/NEBrmXcuR/gaXN/Y9qz
4Dcd5d3QA2eQ8sgF7hOqndP2jeH9UHHudYfe70CWkKKVLFiA7FyYfBp1EkV9RIg7+U2bXl6kaN5k
J+Xmpn6pozGg3Bezttb8IdYszsnNebFd7PA2KxcITLNlZvYkoyKfcb7xaSXawfWijIjiekNULMc6
DofQhXbxHtebAa/9jda7NJUWaJHohWepcHWKYrX2jDq+Pjb07iiyGk4YWrJYbzrJdHG675Q9eFmg
YvW4i2zYAGzqXmpf+fdDpPyw5S0qeyTwGh1hly4LvaUQsh9a1z8be1na1ht1MKxm0Ve7dVJt36EA
a1uxb9G1Nb3GpY9kPkZ6SBr1xmYhqlZjEoubrZIL4ald7hTP/0ZjVQ4qLGWrvlJX8RKcs3ESA5xb
62kSFrrH3E8Acft7REtokGgujvBzIw4KqK7hSP3oOm5Lm173HrskgKqRus6FHivTk6bgGhZ3zl3g
FcxDyJs+zvCKW0MXOyWHyaC/D2H6FUDY57QOG+DX/rmJpvzP+uTS44KFlCvWRiOTh1OZEaZ7AvhK
gEKNtqy9+tx4vVPOGvApUKrXeq3ohFxyKV8rGHHYroM4zRiiL/EcQ8SMCDueVg7I3w5qrGv9MXhh
xMuPPI4L5bB/SId5WxMsRi5CdrJYKG0qYnu9bRO0crHjWITSdElRb5QfVpqiCUNVfZu/TaYOTksX
9JkCbn4vwn4bEvqfmmwsOHlQzNdhP1E6CMfb72DXKFlqg+PnlsH4HcrDHxDjT/7bVtEZG3Lakfzu
fduRYpbMZb5syn+ZLXgxTSSKUayTzbkTT4sHeCBFGHf72AAj1o0Wts2tvcQgfyXah9YKlv3Xelyx
FTOkfce2R0KqJ/DVR/Wwbum7cW8rx9NJhAeQcwjG9Yij/tWwWCJE0A6vwp0kFW3dXpzDQ87js68X
SAK0JG6x82olP/xI6iTA5M+3gLq5MBcvpTa3NBZfxnbdYWHI0+AohJPtRRzz1WuFTJtsvaPJEnNM
sYVVvaiB6dwVsp6g3eQUUuVQSmjt4+Mnd3ormEVnh2Yru1k8kiYZSd6o0DCR06v1e8wT4VqNKmK9
7GZFdcZzMYiT/B16Lo/vX3Ba0KJ/mg0nWV+vzm3iSwRNmrn/By41TWQa+a1bB274KxSXCS9D8Q29
XLnGukFLj+zxSpb8v8UGQfZM1VNFtIMNoc5vCxRhYmcI13wnsgKXMAPcUn+bBUckj+hRnIcvcmZN
ncXjyCKBTReR+YgWj2JA2T4APpShn7hVC82YMu1Fq0bd4yy/v+zklippLBFkGglJYnwkFzzn6fc5
AqgIxS2l6SbsKF8kz3A3KkgRLnvRwrNl3TGSCc13jpdiy3Qrldp8lOPJa97STjLt3QnG/XpxjbT5
8CfDKa9T4TpRUWtxZgJ0SWW84QLafY/AhoBq5RBJsQQC1NTYEqnyPVKWCzeI4KEHlkZ7ldJ+87Rp
oSqlo4aHx2NpWUGHRIY63Loj/y5SICt2Vmdoapg1Hg81Xk8peWHVBiI6YsYQYMJC4hCh3KkATxku
y2rxuMixaHVSg0UiEAbCN2dTaGwkio6hnvBc8ROWWDHM0IQ30gEUD8lriVR0VjxtypESIP4eUBxl
NikuKVqgFPg8vjA2gz4zzAyh8IRbWuwYAz1dbnwesky8c4VJi91yUi5f
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
