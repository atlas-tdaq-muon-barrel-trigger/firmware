-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Sun Oct 30 02:07:45 2022
-- Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_heartbeat_0_0/design_1_heartbeat_0_0_stub.vhdl
-- Design      : design_1_heartbeat_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xczu5ev-sfvc784-2-i
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_heartbeat_0_0 is
  Port ( 
    clk100 : in STD_LOGIC;
    resetn : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 2 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );

end design_1_heartbeat_0_0;

architecture stub of design_1_heartbeat_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk100,resetn,din[2:0],dout[2:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "heartbeat,Vivado 2020.2";
begin
end;
