-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Sun Oct 30 02:07:47 2022
-- Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/ip/ip_1/bd_0482_lut_buffer_0_sim_netlist.vhdl
-- Design      : bd_0482_lut_buffer_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu5ev-sfvc784-2-i
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
geSCzw9gYjFCv0Dn0YxOXxhH+GZFMePCQPK3AjT+zbjt1urPphGbRmSIP212qcXhU3u6qBayOOuP
zGTUOznyYQ==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
OnCSRn8bnLy+eSxgkIEXKk5zY3JDppSX+6N3lQVX9PeSypgnQ/2z4GTpmoL+rdMoco6U9R4G1u4m
E0xhKuM4ba9nEk7cLfAxOQqKqsWQrZaIEmzIr1ET+cp4jOMvYA/MsN4jh93kbuKcSDuJ8zN13DFX
RemIkmekhFjPkyUS5qM=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
MSBAO7tnsBVh2XpVImbQvPkv1Ik+6Bw1D0e9n6H/Bw1mXnRXzm0RzPaEYAIFuluPbWglTrw4pQSr
JI/DSdCg6087Xmb+Q5zKawFvuZahx4HgmrKxTL15lZwamiIpmu3LGyxaEH/VbYGM9Ky0jp5PyDKU
Jeskyx64XVUPlRklhMjIKCtYITsgROzqjs+d1jIc494zqnDADEz0msJP38WdzHgwLDQ0NamfpodX
BUqMR71hgPx1Rvdt7HagUbkfyaG3/12LxFvpAblT7W0W6RKBFEFgFrxWRFaDw+jzj4jgl9g+sjY0
cveJYAA4UpZJwPSDIWehjmS+mOinzlnl8UP7jw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
UtIiSU3lZ1iUKAuaJuT083jLC5QokBuxbJC/zVsWXf8ozOCIDAvtpSufF02lDCCaNNheB40dXQFS
I8VBcTtdWzNr2vj/HmW17e10D6T6mqn/8t0HnWx9c3modRuXup0Too1mNTU5gTH+v3utogTO5ztm
HbJZ/+5ov0tPkaeJufJl5L/RZAfLmRnRYybtx5bbc7XiGyWaVk6KunsaWtX5zJtVnMeUOkg0N8oL
RBeyFp3tFqTN7ecNUp7zom6BjZ3fR6euRy36u1XviJsqBjcxzASI1k+bn2lDs3oEdXuMHoRvcuWL
mmMddzjwWr43L7YoB/WBz/Tw7t6iYI5B6imPUA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
AMZ17uwJyVzW4KyzjD/YjCrX2GlLIDwW9HSuEat97pn8ZQ7QpDPhFLNx09klp1fHQ8yb1KlxCqpm
IjAljp4A5oQHWcBw/s+Xhtpin6GMDGjsmd5KmAD2J5DQmzqPazc0M8vNO+pGpCJogvWarX5XrP6U
56l9vH5mfyPmbT/09Kw=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
oAGTk8IFmmwAT5eT+h5xMK7MsqYCJnsMll3rq17njNu6wbVX2TAoOlVC5DzNg9T5ce7gVnuLuFG/
FgNSnTJx8TlbP73KxlDubmAVofR56G/yHzaJfJ0fwNhrfXm5AFgmFaKFPTKNkrG/qjdNuwUeA8p4
iHoj1zvPx50myVHXSpHLQ8n92DLWgMUX/57aPLbMHmYu/gsD0kHOuQ8Fr2Mi2DxufAvq1gzT0kc+
lxSntoseL+X1HLSvmKpEkR/sjaz6P9omIzqKlmOhvLeTZVEZcUtukVN1HTrlol+4/pTFDztcz0tZ
XqYZKVNB/igvn1iP/Fej8fpkaeJOrk1YgJZ6xw==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
HM0OWTHTT9qkiqicwldwQlaXCXhAavOkOLtzdsXybVEqcdGTuInS8Xvu2i7fjfGdnZjc+o4ZayFd
adCXUGMQZ+7u79Rm71DMtV7WL4PMhXZmItLJgXQmNzajU211AuWse/CyD7Am9ZDJuQcIK0fcqZQI
XVJU6sMESVWiSWdCuqkcQLSuSoBY7TVLmCDoTF9n7MlYfcxCkkK6d+2Xs/gjaWO59GZ3TbWhAQLc
9hHL9YUJUTzZ8yPC8tX+DLS/YrniD3lBpquxXGcl1FxHKFTSpMG/6pTH+7Y5u1s29iqS/KYCCOfR
Pqg3ikxxR4ywBL+umX+Ijv+Hqgol6tBnCkWh/g==

`protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DQuK/YuairmUNmnTZFN/Tbbjjk202ciTMNZWiES7z/c8BYrmlnBses/x7XzAVxFxOTns+S6gcbxv
lho3MoYQI3b1wxR93ymMpbFY2AAKqfTaYrt9nuBi+J66NUkNb4mO5Ysrmk/FyxUuVMw2JeKhCxVf
1Lkw2weXEA2RSHrWxd8764IFSbBqKoKUTMuqLxHovRaQHDy/mOdyefGG7/6ywGbKjVTlE8lXVH8E
8QodSYZ7p8uod81sVFzJL26a9Tqu+u2tOgD/WqMuxrio7zRkYYC5P+/FtxLC4GaIZ6LivaJuTLOF
bkAMneaa5dlfamLnRyzDXUCJu/DFpJtH5s1eLQ==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
DL1JbuaDxIhP8ir0jAdx6nea42/rMQ2VXG8PZEGqMkgF/yLmK8+UcPNvmkEnUbVxq6WNxCUUYBOO
eroUXvKd3hB7aza+lE1PkczDRQpe4dZWQ8yHCUSbqj/KnUKU7sMHOSk5MiYcbBazdC+B/zdSxJsr
sUmnOLgp/SqygmZW7/oDYMIYyOExEOrIPD4CH/xXZGlvuNs4OjdmaSus7kQp/iaUxQz03NGaMv3/
EuIfORb3j+mQPwXwEBQhecy81p8ky3bmOS0LK+CPuz0LF3VVvrDnnXUSBCWa2WW6t7burmoHvgPV
oB2jrvwkS6dNjRJ7CoyGvV0N6d60kiD0LjZg6Q==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 4240)
`protect data_block
//A9V5G0V5i83OErPmL68C3kdxVWDLG9aOhQixUjVI8K9E7hV+lg/a40W+W4VjaBF4bNxhr+BTDb
g/j3YMmmSU20LExmlcp+SrvmwB+028o8ptC3woQla0BRSoxSxrYVOEDbjzCvx+dvtfM3DrdIowKH
L+vzWn4mdLCbG91She8d7uKJ6Ulg2r+9Gsu7+37nzMlQwchgFOl3hAI8LOgV3ubWYb5eoO1Vfze2
4QSeWinraC9nsdWMFe1bBp7BYuFx/fTLLke58NWJ/JhzqWVZGtBwWKpEe7hwezfRuH52bUYXB/V4
t+tBwFwHk0K5Z8qQ6iqPWVYSpCKNSOfuY8ATydXR7Sg6omXlrdSMv6FexfauxiS+kGHiAZiKQEZG
LCn63YxQZVrseyMh1rBzJs+bgAV1mzk2XACvkacWmEGP5KekjNP495uHLPoVsycEnj7sGdTZQ96e
ZFqPmHd+fNaKcU/C4AMSZZcMHA1uvmoBz7BRGg6hCsZCAgQc8/ulTS/lTaNJuTX/n8fIOpqFLECV
TP4b37yvyNbBB1+aiFvklXZ7hnnC8Akf6tSBn8FGwGAUrSFPMbF588PZqPLNrV4IdZDSB9aDkGY9
Tulz92VWM7N00TH62ItE/Wl1bMw+I96boqLQ17EPMc2ywIX7JumeLO/YxzHFMlOf60+ONfu7iPve
5VD7+5zwaYAtjQLqx1WHRmsj/MkmMlXO2PA5mgn8qGnPoWiXcRIuCEN2+WcZqGazE3cWguRXzDkY
HZJkn9/wfTFTUf5ORK7rKkv5YsrqnLWYTLcYQXE0nxKa0nqg0tYUtvN05ig3CKwbRizH0xxRhU3R
aEwiLxdOV2N+rpRGoYL5rbH7aBKaYQUUqAcmTRClSq6NbPcjQGQfwXdQJ+LxLoFKK5J/fJCEts4S
MpekmVnQNomreBF2ubpqKaZjOSRwUy0/eBpZGg8AMJHUww6/zxspWWJmjpjjpmIGNBKwxC5V1gES
qxsTLoPpIOcqr7Qg475BZNElzDYRfRjx+n2zQ7ILd71uLHSM7kBMwCRWgED2QKVwUIydURUdG+1Z
BoHTu7lJNckMVGdg+cqA5f0HujM9+yTmJyiDjpsrnGPD7/QhnnweInfbsGNX88oX87nD87hLMW+7
LZAApMs7uS7ykFUDnXIbs6W6rSs/bnIuqYP62ryULOtlMtB34I6C+HCg8VBoCb5qKUGpiokCP1l/
2ceLD51jYrEdMfhHXdjRjE3BxeIRV/ajacSTx6ADYh3W0jbvoXmaN9E1p2GL6aG5i56sX0Mp4+GM
4u3h5NHyjOyyxqm4fenew25mkBTrJ2OM5U/DZroFAKDuWYebYVlIE8ivYrfCinJXwoqFj24iZqT6
ABYAnqqZR2VjYzDmn25rMv4Hhg9BnVXMTSNqKtf91SUtzgS+DBDiekyYoL6mnl8cNFTXdFVHj1O4
5X/x8aEVr29LcEBWGFZ8qDW7frcbCx/xdal8bSnI0D0bMP8M5hLUhpEjAfw79Q9Oi5W/ISZaWNj1
kPD444BbP2IQpk6YEmeq7WQq8fwGqC/8aB28jILoBp9vWZ///XsqFkuG3gMBV06SRDLVyLCqwI2M
+o8qPKl8AM3MSYj9h1tlTV7I/O0Q8DC2vHEIvHTukQSaen2eqZ39Ic/nye1o9QHO0TSxBtyY5cLg
c6n7pAnckOQvEPMAsswlGEZ3QLvLMzzmrtZXhEQy/MI9UzdCrKfTIhg6Po7pBNJ0jhp517kC1BlO
Ucd4ISOjgyalgww9iR7af8+RmEIKtDOw8ERwZ5rJ5EVqoKI20fnTTZs3Q10icWOJPSRmY2R/EeWa
ZjUKkT8IAiFpIja2lkzCXapu9ila+x6YGE0whyhjNvyw0OemSkWwpWCX2DFUlh02A/6Wo6wjQX6s
iCEilKQzJMKjeGEJWmiNe2JBYgfIYh8jXWRUIpeex7a9Ah2pDq19tN2HKxPFHDO2zprnZ6WUP/2k
PaxZeGCoopPN6hVzAo6WUX0Z+XzF++awQ5OZtQu4dwPAYs7iN+Qxg3weQ7CeCbw/NT4xD3SlVvak
4aMz1H9Jb5Y2gp889p3wEy5GuKObbdVzy54dc0nfOxL8dkuDAXoog762Iq/wbPWdwFZK4dmetr5T
X47Iny9SyrYkoRUJDokAeuvW1zdN0dQ9cRzK8RYNT5kAG1jDfRPT/7dNanzhQSriFh1g42SetoUx
KA/6/biCi1UjsSdW4qtbDF5khKiiIlOWK7MY+YOVXAsQh/u4rjiyLHcc7Xke6Yc57OnVDW4qzmbu
KL+fAWQFB5O4bCtS43tLDO1xYP574ec4WCvT2RCeL8kOS2GqPkSeXNr2tHXLhDnbvJpQ33XoEWKw
W8khSunj5+D4hQJc6Cc4ZAnONrG0oVwJB0tiiYUqkantamNsPvYG472m1p2VQQnbRuift0Ot9sun
ArW0zMN4cOG1S3Vw6/CSb76EMUAHKIr25eN16GugWCeVK0QudW4m1ELeveLlx4T65W60NQeNS+we
X4PqoR2ufOdNhqI7+0XnKNT7iPhAKqwBotVziRhzt/AVLFsAEVF7ooB66aNRQJBa3IGj6WwhR7qD
CiclJoM1slESgjCobI18VtIujH8bGLVVokRYOp7u8IWePCePpCiMz8rBbyEyonKaS/KZgjk91CkK
vh7wcKON0U7OjltJO1zX8sltq7dBj5DueJHkK/nnncpA+jF+rjBWOImT6tdxqL4IWeQB5Kh15UAg
CkAtldENi57GdU0RvpFY0bI5RuCYz72CacDOCDDcDkteaphQFI934NlAuodI8iXBUbHtpfIpiX57
vvTMiLAcb0J6xzywUP54IqU7o2LzZK8ugcjLB/xDhuDL9eM51QEMgxkNhRdqwti8wcAePKH2J7dJ
dfjmHo8YUO2IBUBla+/493XIK7PdPopjLQayAnumblK2vy/YCPUVeJNiaJXw9qHZP7H1HkHdumFw
z9st9xYiNAfpKC79n7QHPH1RGG6jOy409PAaIhq6+bTMTjVUYcnvKtkp3XHO5MJKo89scYkUoN+q
bv5/v4knq9oI60xS54azGNEeUhVznpgM7megcq4qq5XxGnkjOjs5frc/RZwk6kFZveoUMM7D6zbV
0roeqcXLQ69DTagZK3uSpkZWngDhSCj8dPJ0ArdlMTBXDtCWIknMepFN4dt0gcDc6Pu1naUH9GQB
oXv9++1iUqvBGw6+XIRzPUkDeLjLTepxCpT2Ik4YtNJexWs9u3wOUahbC/7Z0kHxbOY/HfR9O2R9
3B2jid08Fip4ykpTq7bXkuCX/mInyEAJyKAEnbXAVQjKrIp871vdk5Acuu93yFoHofch82ZdqoIs
lCFmpemC412D4UMurB1VEZCXpEb6NsS7Zhiw4w8KV7A2xlXIDRDfzUbUCzB/3tlDP4KIBbr8zBQ4
VYYbq668SzrFauWW2fCeeJ+Zs+BUeMYiunRhqWQTfQ0PJSZvV0ktiTdzVgxFyNq2f8jB6YEAyvwA
htswKy3Ki4TlXhd/W4RheZb7PPnZNPmv6raZDQoVBXuJU86/SSQYxvrZGeNGc9JEvvAyBG3o5E8+
3x3xFgrt99vuXkravvn7Y4abqCly50yl/6OAxn44OHavDWDPiPjRNKSg8y3KaU9S7GkfUgyDE0/x
9mqyA5lrmw2ECg+/R2monGji8pbL4OMfyJbdE2rKGJVyNzCr41U9Ker8eSFqWGZFmGvFeY+2ut+G
6V2GAsJO8EDnSMZSY8wXN8HxjpHj/qBu2KYJgvbyb5XGIfEUS9aFI39cIMIQJFlPQPSDhudnAN3H
27nN+1/fuwsKFRrHiRzX/Fg5vs9aS6IR+dPumRuioLS/iRhKVfofrDp+ZkTNlxc3HLqP0CPnXkRX
PVZ3Og8pzH2hfkSPAMV8oozSASWrBLdJMNIV42LC66HBe4Kz4TX5ESYSv9K2YpAYpvCTlq7pnPmx
Cjc0R/7MDo1SEaYAiZxWJBiLZIbffcBpKiYqRX03fk+Qn48wiWZm0BPHW+fL/gEJcgi4wMTn2ON9
Q67W0ofV5WUEIGsuJHOWbpV4hUsal5hBBB3DEBlusPsXIkpdOMTi08qIB3LRAdk+1kgj+0rYPjKA
lqF096Jm/4eKFzJ6nG8CzxDhb6XUxSfohVB9SQxUiYfFh8x+wW7FCqRNtN1uiar2iM954N+iZ1D6
6Hc8vB67xX4xwVrWZL8xzB1vmlukANJa9xl/MMwvCVM45bXw9HxeMOjZCxbrPJBLaSjb49RMM9fM
BY0SFB5wIkphzyJDQMfR5dg/K3+mwsLrG7NFJtm569vUcs9mSpQBM0bG7jjhvNRvr639N0IBbe+B
KeuJz261rcRIUbzW1l/0M9cVJZ9DOMGTmioClGVMN9SI55XgWkASsTe8mQICMRqEIqIljmp4YjhR
sdP0cJKGTSXgRckvCUD6OjO00e2nAuQ76xqNnC6foeOmZBtNAJYVd2OdnFRk0Pic9/ajLVe99NDB
cEvxArPXDq7ZAsKVwJu8QEnope/7CO90Lu3x/BBgdoXKRtF/FJUGPxLxyg1QtTKLf+XWbVEaM88S
5ipGkyy0KBoLExBHyZzrK/uIOcsDmlQVT3u5r1kZQEsp7G+4UYVvj+i4e9dUmFck+Btt950SBZNJ
qZMajRb+RwOyugHhX61ZBaQ/WNAsArJXX7SrSQbuvllnP3S7+gL6WSzQSlGNrJJCB0Lf7hOHmSMK
apkIIqILKvx/NCnqNJxUV2SLP7zocOVCGuPmFBWADXNtb1ocJosWAHRqsMvlfIlXHQ7CTxoX1M2z
2Yw0UJhcinYEoHY7D2emi8Pz4doRriNFTNZ+oRntoeMoFxWGWpSp0nDF6Qnc8CIiykHFR3AnxZE7
s80XTQOrrOHFwEp0BGZU3XHHYOnGRe6UgMLLPlxSdHBBGGGlA6LJmwDaQeDym+y1lSl7jNgNxDKx
CX0FIM/T6w+ZPP3Y8YUEYAftU35aTRFn13dUB6ywacSQXBEnwt2c+wgd9QP3e5ALiAJSg0SAqdih
wOBo+r2JgKHcMYgkZ2x8mIpBXcROjpnVV9MW2sVMLonqFJKh39E5S6eTMiN3qFjXCJn93ckQ5nrB
/Lcc1pgpMRA5p3AX1snPrxFXarqdyrmL6XA3HbXy6aui327WRiDY90tWXSk6kNPqwoZTlpjjIwlz
AnVuCqT512tHVPFgkXZ3mweG12YZrFbq2hySji4OpqeqAuLlcO52TqcNa3NZYNsvaLnuxNLIRRgU
zlNe/H5DrAMFqLR3RnWpjY8Bc94VGYPu+b43PDGXNsCZhPTyk9Z9xOS+3slOnp92PMQr8H5mDjNN
wYPr+Ynk4sQhE/kYzzBmfIlZpp2FXfWrAmii5ngGoZk8AQ0yPwIS1BJIQP3+ZGwT28bECQy1Oktt
WcVafazMvlmJV+kmDRZ2/oZrLCbEXszyUYZvMG1sjBy/apxwXLGWlpNQM9QvIYQCNTai4OHWYQH1
RjaGTb/R3vHXZnVouazRGWzKwykPvg9xFpfLOH59URYTDhI6x7qChp/bgIUopbpAsJG1a8M4Bj2u
BUmSd4dNDWyQGXhq0GM21dB+mQNXigOiQsfa2BfbQqWkUnZZYksfx2xh650LRoBCPFBMmDdlge+w
Rriw5DDpkbJoM//b26kIEThEszlJog==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_0482_lut_buffer_0 is
  port (
    tdi_i : in STD_LOGIC;
    tms_i : in STD_LOGIC;
    tck_i : in STD_LOGIC;
    drck_i : in STD_LOGIC;
    sel_i : in STD_LOGIC;
    shift_i : in STD_LOGIC;
    update_i : in STD_LOGIC;
    capture_i : in STD_LOGIC;
    runtest_i : in STD_LOGIC;
    reset_i : in STD_LOGIC;
    bscanid_en_i : in STD_LOGIC;
    tdo_o : out STD_LOGIC;
    tdi_o : out STD_LOGIC;
    tms_o : out STD_LOGIC;
    tck_o : out STD_LOGIC;
    drck_o : out STD_LOGIC;
    sel_o : out STD_LOGIC;
    shift_o : out STD_LOGIC;
    update_o : out STD_LOGIC;
    capture_o : out STD_LOGIC;
    runtest_o : out STD_LOGIC;
    reset_o : out STD_LOGIC;
    bscanid_en_o : out STD_LOGIC;
    tdo_i : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of bd_0482_lut_buffer_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of bd_0482_lut_buffer_0 : entity is "bd_0482_lut_buffer_0,lut_buffer_v2_0_0_lut_buffer,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of bd_0482_lut_buffer_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of bd_0482_lut_buffer_0 : entity is "lut_buffer_v2_0_0_lut_buffer,Vivado 2020.2";
end bd_0482_lut_buffer_0;

architecture STRUCTURE of bd_0482_lut_buffer_0 is
  signal NLW_inst_bscanid_o_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute C_EN_BSCANID_VEC : integer;
  attribute C_EN_BSCANID_VEC of inst : label is 0;
  attribute DONT_TOUCH : boolean;
  attribute DONT_TOUCH of inst : label is std.standard.true;
  attribute KEEP_HIERARCHY : string;
  attribute KEEP_HIERARCHY of inst : label is "soft";
  attribute is_du_within_envelope : string;
  attribute is_du_within_envelope of inst : label is "true";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of bscanid_en_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan BSCANID_EN";
  attribute X_INTERFACE_INFO of bscanid_en_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan BSCANID_EN";
  attribute X_INTERFACE_INFO of capture_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan CAPTURE";
  attribute X_INTERFACE_INFO of capture_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan CAPTURE";
  attribute X_INTERFACE_INFO of drck_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan DRCK";
  attribute X_INTERFACE_INFO of drck_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan DRCK";
  attribute X_INTERFACE_INFO of reset_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan RESET";
  attribute X_INTERFACE_INFO of reset_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan RESET";
  attribute X_INTERFACE_INFO of runtest_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan RUNTEST";
  attribute X_INTERFACE_INFO of runtest_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan RUNTEST";
  attribute X_INTERFACE_INFO of sel_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan SEL";
  attribute X_INTERFACE_INFO of sel_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan SEL";
  attribute X_INTERFACE_INFO of shift_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan SHIFT";
  attribute X_INTERFACE_INFO of shift_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan SHIFT";
  attribute X_INTERFACE_INFO of tck_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan TCK";
  attribute X_INTERFACE_INFO of tck_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan TCK";
  attribute X_INTERFACE_INFO of tdi_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan TDI";
  attribute X_INTERFACE_INFO of tdi_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan TDI";
  attribute X_INTERFACE_INFO of tdo_i : signal is "xilinx.com:interface:bscan:1.0 m_bscan TDO";
  attribute X_INTERFACE_INFO of tdo_o : signal is "xilinx.com:interface:bscan:1.0 s_bscan TDO";
  attribute X_INTERFACE_INFO of tms_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan TMS";
  attribute X_INTERFACE_INFO of tms_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan TMS";
  attribute X_INTERFACE_INFO of update_i : signal is "xilinx.com:interface:bscan:1.0 s_bscan UPDATE";
  attribute X_INTERFACE_INFO of update_o : signal is "xilinx.com:interface:bscan:1.0 m_bscan UPDATE";
begin
inst: entity work.bd_0482_lut_buffer_0_lut_buffer_v2_0_0_lut_buffer
     port map (
      bscanid_en_i => bscanid_en_i,
      bscanid_en_o => bscanid_en_o,
      bscanid_i(31 downto 0) => B"00000000000000000000000000000000",
      bscanid_o(31 downto 0) => NLW_inst_bscanid_o_UNCONNECTED(31 downto 0),
      capture_i => capture_i,
      capture_o => capture_o,
      drck_i => drck_i,
      drck_o => drck_o,
      reset_i => reset_i,
      reset_o => reset_o,
      runtest_i => runtest_i,
      runtest_o => runtest_o,
      sel_i => sel_i,
      sel_o => sel_o,
      shift_i => shift_i,
      shift_o => shift_o,
      tck_i => tck_i,
      tck_o => tck_o,
      tdi_i => tdi_i,
      tdi_o => tdi_o,
      tdo_i => tdo_i,
      tdo_o => tdo_o,
      tms_i => tms_i,
      tms_o => tms_o,
      update_i => update_i,
      update_o => update_o
    );
end STRUCTURE;
