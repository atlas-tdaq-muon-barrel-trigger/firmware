# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# IP: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0.xci
# IP: The module: 'design_1_aurora_64b66b_0_0' is the root of the design. Do not add the DONT_TOUCH constraint.

# IP: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master} -quiet] -quiet

# IP: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/design_1_aurora_64b66b_0_0_gt.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_gt || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_gt} -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master.xdc
set_property KEEP_HIERARCHY SOFT [get_cells [split [join [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master_clocks.xdc
#dup# set_property KEEP_HIERARCHY SOFT [get_cells [split [join [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/synth/design_1_aurora_64b66b_0_0_gt_ooc.xdc

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/synth/design_1_aurora_64b66b_0_0_gt.xdc
set_property KEEP_HIERARCHY SOFT [get_cells [split [join [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_gt || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_gt} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_aurora_64b66b_0_0'. Do not add the DONT_TOUCH constraint.
set_property KEEP_HIERARCHY SOFT [get_cells inst -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_aurora_64b66b_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property KEEP_HIERARCHY SOFT [get_cells inst -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0_clocks.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_aurora_64b66b_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property KEEP_HIERARCHY SOFT [get_cells inst -quiet] -quiet

# IP: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0.xci
# IP: The module: 'design_1_aurora_64b66b_0_0' is the root of the design. Do not add the DONT_TOUCH constraint.

# IP: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master.xci
#dup# set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master} -quiet] -quiet

# IP: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/design_1_aurora_64b66b_0_0_gt.xci
#dup# set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_gt || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_gt} -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master.xdc
#dup# set_property KEEP_HIERARCHY SOFT [get_cells [split [join [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master_clocks.xdc
#dup# set_property KEEP_HIERARCHY SOFT [get_cells [split [join [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_fifo_gen_master} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/synth/design_1_aurora_64b66b_0_0_gt_ooc.xdc

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/synth/design_1_aurora_64b66b_0_0_gt.xdc
#dup# set_property KEEP_HIERARCHY SOFT [get_cells [split [join [get_cells -hier -filter {REF_NAME==design_1_aurora_64b66b_0_0_gt || ORIG_REF_NAME==design_1_aurora_64b66b_0_0_gt} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0_ooc.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_aurora_64b66b_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property KEEP_HIERARCHY SOFT [get_cells inst -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_aurora_64b66b_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property KEEP_HIERARCHY SOFT [get_cells inst -quiet] -quiet

# XDC: /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0_clocks.xdc
# XDC: The top module name and the constraint reference have the same name: 'design_1_aurora_64b66b_0_0'. Do not add the DONT_TOUCH constraint.
#dup# set_property KEEP_HIERARCHY SOFT [get_cells inst -quiet] -quiet
