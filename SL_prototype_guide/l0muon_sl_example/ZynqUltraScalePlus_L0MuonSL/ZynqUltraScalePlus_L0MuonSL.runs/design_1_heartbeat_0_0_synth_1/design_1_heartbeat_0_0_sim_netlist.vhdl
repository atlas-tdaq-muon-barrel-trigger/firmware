-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
-- Date        : Sun Oct 30 02:07:45 2022
-- Host        : lhcelec01 running 64-bit Ubuntu 18.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_heartbeat_0_0_sim_netlist.vhdl
-- Design      : design_1_heartbeat_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu5ev-sfvc784-2-i
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_heartbeat is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk100 : in STD_LOGIC;
    resetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_heartbeat;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_heartbeat is
  signal \FSM_sequential_phase[1]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_12_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_13_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_14_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_15_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_16_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_17_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_18_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_19_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase[1]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_sequential_phase_reg_n_0_[0]\ : STD_LOGIC;
  signal counter : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \counter0_carry__0_n_0\ : STD_LOGIC;
  signal \counter0_carry__0_n_1\ : STD_LOGIC;
  signal \counter0_carry__0_n_2\ : STD_LOGIC;
  signal \counter0_carry__0_n_3\ : STD_LOGIC;
  signal \counter0_carry__0_n_4\ : STD_LOGIC;
  signal \counter0_carry__0_n_5\ : STD_LOGIC;
  signal \counter0_carry__0_n_6\ : STD_LOGIC;
  signal \counter0_carry__0_n_7\ : STD_LOGIC;
  signal \counter0_carry__1_n_0\ : STD_LOGIC;
  signal \counter0_carry__1_n_1\ : STD_LOGIC;
  signal \counter0_carry__1_n_2\ : STD_LOGIC;
  signal \counter0_carry__1_n_3\ : STD_LOGIC;
  signal \counter0_carry__1_n_4\ : STD_LOGIC;
  signal \counter0_carry__1_n_5\ : STD_LOGIC;
  signal \counter0_carry__1_n_6\ : STD_LOGIC;
  signal \counter0_carry__1_n_7\ : STD_LOGIC;
  signal \counter0_carry__2_n_2\ : STD_LOGIC;
  signal \counter0_carry__2_n_3\ : STD_LOGIC;
  signal \counter0_carry__2_n_4\ : STD_LOGIC;
  signal \counter0_carry__2_n_5\ : STD_LOGIC;
  signal \counter0_carry__2_n_6\ : STD_LOGIC;
  signal \counter0_carry__2_n_7\ : STD_LOGIC;
  signal counter0_carry_n_0 : STD_LOGIC;
  signal counter0_carry_n_1 : STD_LOGIC;
  signal counter0_carry_n_2 : STD_LOGIC;
  signal counter0_carry_n_3 : STD_LOGIC;
  signal counter0_carry_n_4 : STD_LOGIC;
  signal counter0_carry_n_5 : STD_LOGIC;
  signal counter0_carry_n_6 : STD_LOGIC;
  signal counter0_carry_n_7 : STD_LOGIC;
  signal \counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \counter[31]_i_2_n_0\ : STD_LOGIC;
  signal \counter[31]_i_3_n_0\ : STD_LOGIC;
  signal \counter[31]_i_4_n_0\ : STD_LOGIC;
  signal \counter[31]_i_5_n_0\ : STD_LOGIC;
  signal \counter[31]_i_6_n_0\ : STD_LOGIC;
  signal \counter[31]_i_7_n_0\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal heartbeat_i_1_n_0 : STD_LOGIC;
  signal p_0_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_0_out_0 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \phase__0\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \NLW_counter0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 6 );
  signal \NLW_counter0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 to 7 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_phase[0]_i_1\ : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_phase_reg[0]\ : label is "iSTATE:11,iSTATE0:00,iSTATE1:01,iSTATE2:10,";
  attribute FSM_ENCODED_STATES of \FSM_sequential_phase_reg[1]\ : label is "iSTATE:11,iSTATE0:00,iSTATE1:01,iSTATE2:10,";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of counter0_carry : label is 35;
  attribute ADDER_THRESHOLD of \counter0_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \counter0_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \counter0_carry__2\ : label is 35;
  attribute SOFT_HLUTNM of \counter[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \counter[10]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \counter[11]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \counter[12]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \counter[13]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \counter[14]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \counter[15]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \counter[16]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \counter[17]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \counter[18]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \counter[19]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \counter[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \counter[20]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \counter[21]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \counter[22]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \counter[23]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \counter[24]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \counter[25]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \counter[26]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \counter[27]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \counter[28]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \counter[29]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \counter[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \counter[30]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \counter[31]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \counter[31]_i_5\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \counter[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \counter[4]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \counter[5]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \counter[6]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \counter[7]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \counter[8]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \counter[9]_i_1\ : label is "soft_lutpair5";
begin
\FSM_sequential_phase[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \FSM_sequential_phase_reg_n_0_[0]\,
      O => p_0_out_0(0)
    );
\FSM_sequential_phase[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFEAEAEA"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_3_n_0\,
      I1 => \FSM_sequential_phase[1]_i_4_n_0\,
      I2 => \FSM_sequential_phase[1]_i_5_n_0\,
      I3 => \FSM_sequential_phase[1]_i_6_n_0\,
      I4 => \FSM_sequential_phase[1]_i_7_n_0\,
      I5 => \FSM_sequential_phase[1]_i_8_n_0\,
      O => \FSM_sequential_phase[1]_i_1_n_0\
    );
\FSM_sequential_phase[1]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => counter(13),
      I1 => counter(12),
      I2 => counter(10),
      I3 => counter(9),
      I4 => counter(15),
      I5 => counter(14),
      O => \FSM_sequential_phase[1]_i_10_n_0\
    );
\FSM_sequential_phase[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => counter(20),
      I1 => counter(19),
      I2 => counter(16),
      I3 => counter(18),
      I4 => counter(22),
      I5 => counter(21),
      O => \FSM_sequential_phase[1]_i_11_n_0\
    );
\FSM_sequential_phase[1]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => counter(31),
      I1 => counter(26),
      I2 => counter(11),
      I3 => counter(7),
      O => \FSM_sequential_phase[1]_i_12_n_0\
    );
\FSM_sequential_phase[1]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => counter(10),
      I1 => counter(9),
      O => \FSM_sequential_phase[1]_i_13_n_0\
    );
\FSM_sequential_phase[1]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001000000000"
    )
        port map (
      I0 => counter(19),
      I1 => counter(20),
      I2 => counter(18),
      I3 => counter(16),
      I4 => counter(21),
      I5 => counter(22),
      O => \FSM_sequential_phase[1]_i_14_n_0\
    );
\FSM_sequential_phase[1]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => counter(14),
      I1 => counter(13),
      I2 => counter(10),
      I3 => counter(12),
      I4 => counter(16),
      I5 => counter(15),
      O => \FSM_sequential_phase[1]_i_15_n_0\
    );
\FSM_sequential_phase[1]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010000000000000"
    )
        port map (
      I0 => counter(20),
      I1 => counter(21),
      I2 => counter(19),
      I3 => counter(18),
      I4 => counter(23),
      I5 => counter(22),
      O => \FSM_sequential_phase[1]_i_16_n_0\
    );
\FSM_sequential_phase[1]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counter(2),
      I1 => counter(1),
      I2 => counter(4),
      I3 => counter(3),
      O => \FSM_sequential_phase[1]_i_17_n_0\
    );
\FSM_sequential_phase[1]_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counter(6),
      I1 => counter(5),
      I2 => counter(25),
      I3 => counter(17),
      O => \FSM_sequential_phase[1]_i_18_n_0\
    );
\FSM_sequential_phase[1]_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counter(28),
      I1 => counter(27),
      I2 => counter(30),
      I3 => counter(29),
      O => \FSM_sequential_phase[1]_i_19_n_0\
    );
\FSM_sequential_phase[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \FSM_sequential_phase_reg_n_0_[0]\,
      I1 => \phase__0\(1),
      O => p_0_out_0(1)
    );
\FSM_sequential_phase[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_9_n_0\,
      I1 => \FSM_sequential_phase[1]_i_10_n_0\,
      I2 => \FSM_sequential_phase[1]_i_11_n_0\,
      I3 => counter(8),
      I4 => \phase__0\(1),
      O => \FSM_sequential_phase[1]_i_3_n_0\
    );
\FSM_sequential_phase[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0400000000000000"
    )
        port map (
      I0 => counter(8),
      I1 => \phase__0\(1),
      I2 => counter(24),
      I3 => counter(23),
      I4 => \FSM_sequential_phase_reg_n_0_[0]\,
      I5 => \FSM_sequential_phase[1]_i_12_n_0\,
      O => \FSM_sequential_phase[1]_i_4_n_0\
    );
\FSM_sequential_phase[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => counter(15),
      I1 => counter(14),
      I2 => \FSM_sequential_phase[1]_i_13_n_0\,
      I3 => counter(13),
      I4 => counter(12),
      I5 => \FSM_sequential_phase[1]_i_14_n_0\,
      O => \FSM_sequential_phase[1]_i_5_n_0\
    );
\FSM_sequential_phase[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => counter(24),
      I1 => counter(11),
      I2 => counter(7),
      I3 => \FSM_sequential_phase_reg_n_0_[0]\,
      I4 => counter(31),
      I5 => counter(26),
      O => \FSM_sequential_phase[1]_i_6_n_0\
    );
\FSM_sequential_phase[1]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_15_n_0\,
      I1 => counter(9),
      I2 => counter(8),
      I3 => \phase__0\(1),
      I4 => \FSM_sequential_phase[1]_i_16_n_0\,
      O => \FSM_sequential_phase[1]_i_7_n_0\
    );
\FSM_sequential_phase[1]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_17_n_0\,
      I1 => \FSM_sequential_phase[1]_i_18_n_0\,
      I2 => \FSM_sequential_phase[1]_i_19_n_0\,
      I3 => counter(0),
      O => \FSM_sequential_phase[1]_i_8_n_0\
    );
\FSM_sequential_phase[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => counter(24),
      I1 => counter(23),
      I2 => counter(11),
      I3 => counter(7),
      I4 => counter(31),
      I5 => counter(26),
      O => \FSM_sequential_phase[1]_i_9_n_0\
    );
\FSM_sequential_phase_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => \FSM_sequential_phase[1]_i_1_n_0\,
      CLR => heartbeat_i_1_n_0,
      D => p_0_out_0(0),
      Q => \FSM_sequential_phase_reg_n_0_[0]\
    );
\FSM_sequential_phase_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => \FSM_sequential_phase[1]_i_1_n_0\,
      CLR => heartbeat_i_1_n_0,
      D => p_0_out_0(1),
      Q => \phase__0\(1)
    );
counter0_carry: unisim.vcomponents.CARRY8
     port map (
      CI => counter(0),
      CI_TOP => '0',
      CO(7) => counter0_carry_n_0,
      CO(6) => counter0_carry_n_1,
      CO(5) => counter0_carry_n_2,
      CO(4) => counter0_carry_n_3,
      CO(3) => counter0_carry_n_4,
      CO(2) => counter0_carry_n_5,
      CO(1) => counter0_carry_n_6,
      CO(0) => counter0_carry_n_7,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => data0(8 downto 1),
      S(7 downto 0) => counter(8 downto 1)
    );
\counter0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => counter0_carry_n_0,
      CI_TOP => '0',
      CO(7) => \counter0_carry__0_n_0\,
      CO(6) => \counter0_carry__0_n_1\,
      CO(5) => \counter0_carry__0_n_2\,
      CO(4) => \counter0_carry__0_n_3\,
      CO(3) => \counter0_carry__0_n_4\,
      CO(2) => \counter0_carry__0_n_5\,
      CO(1) => \counter0_carry__0_n_6\,
      CO(0) => \counter0_carry__0_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => data0(16 downto 9),
      S(7 downto 0) => counter(16 downto 9)
    );
\counter0_carry__1\: unisim.vcomponents.CARRY8
     port map (
      CI => \counter0_carry__0_n_0\,
      CI_TOP => '0',
      CO(7) => \counter0_carry__1_n_0\,
      CO(6) => \counter0_carry__1_n_1\,
      CO(5) => \counter0_carry__1_n_2\,
      CO(4) => \counter0_carry__1_n_3\,
      CO(3) => \counter0_carry__1_n_4\,
      CO(2) => \counter0_carry__1_n_5\,
      CO(1) => \counter0_carry__1_n_6\,
      CO(0) => \counter0_carry__1_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7 downto 0) => data0(24 downto 17),
      S(7 downto 0) => counter(24 downto 17)
    );
\counter0_carry__2\: unisim.vcomponents.CARRY8
     port map (
      CI => \counter0_carry__1_n_0\,
      CI_TOP => '0',
      CO(7 downto 6) => \NLW_counter0_carry__2_CO_UNCONNECTED\(7 downto 6),
      CO(5) => \counter0_carry__2_n_2\,
      CO(4) => \counter0_carry__2_n_3\,
      CO(3) => \counter0_carry__2_n_4\,
      CO(2) => \counter0_carry__2_n_5\,
      CO(1) => \counter0_carry__2_n_6\,
      CO(0) => \counter0_carry__2_n_7\,
      DI(7 downto 0) => B"00000000",
      O(7) => \NLW_counter0_carry__2_O_UNCONNECTED\(7),
      O(6 downto 0) => data0(31 downto 25),
      S(7) => '0',
      S(6 downto 0) => counter(31 downto 25)
    );
\counter[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"32"
    )
        port map (
      I0 => \counter[0]_i_2_n_0\,
      I1 => counter(0),
      I2 => \counter[31]_i_2_n_0\,
      O => p_0_out(0)
    );
\counter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_19_n_0\,
      I1 => counter(6),
      I2 => counter(5),
      I3 => counter(25),
      I4 => counter(17),
      I5 => \FSM_sequential_phase[1]_i_17_n_0\,
      O => \counter[0]_i_2_n_0\
    );
\counter[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(10),
      O => p_0_out(10)
    );
\counter[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(11),
      O => p_0_out(11)
    );
\counter[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(12),
      O => p_0_out(12)
    );
\counter[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(13),
      O => p_0_out(13)
    );
\counter[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(14),
      O => p_0_out(14)
    );
\counter[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(15),
      O => p_0_out(15)
    );
\counter[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(16),
      O => p_0_out(16)
    );
\counter[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(17),
      O => p_0_out(17)
    );
\counter[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(18),
      O => p_0_out(18)
    );
\counter[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(19),
      O => p_0_out(19)
    );
\counter[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(1),
      O => p_0_out(1)
    );
\counter[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(20),
      O => p_0_out(20)
    );
\counter[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(21),
      O => p_0_out(21)
    );
\counter[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(22),
      O => p_0_out(22)
    );
\counter[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(23),
      O => p_0_out(23)
    );
\counter[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(24),
      O => p_0_out(24)
    );
\counter[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(25),
      O => p_0_out(25)
    );
\counter[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(26),
      O => p_0_out(26)
    );
\counter[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(27),
      O => p_0_out(27)
    );
\counter[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(28),
      O => p_0_out(28)
    );
\counter[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(29),
      O => p_0_out(29)
    );
\counter[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(2),
      O => p_0_out(2)
    );
\counter[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(30),
      O => p_0_out(30)
    );
\counter[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(31),
      O => p_0_out(31)
    );
\counter[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \counter[31]_i_3_n_0\,
      I1 => \counter[31]_i_4_n_0\,
      I2 => \counter[31]_i_5_n_0\,
      I3 => \counter[31]_i_6_n_0\,
      I4 => \counter[31]_i_7_n_0\,
      O => \counter[31]_i_2_n_0\
    );
\counter[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBBBFFFFDFFFF"
    )
        port map (
      I0 => counter(12),
      I1 => counter(8),
      I2 => counter(11),
      I3 => counter(7),
      I4 => counter(18),
      I5 => counter(16),
      O => \counter[31]_i_3_n_0\
    );
\counter[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F6FFFFFFFFFFFF6F"
    )
        port map (
      I0 => counter(7),
      I1 => counter(19),
      I2 => counter(15),
      I3 => counter(11),
      I4 => counter(21),
      I5 => counter(20),
      O => \counter[31]_i_4_n_0\
    );
\counter[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF7EE7F"
    )
        port map (
      I0 => counter(23),
      I1 => counter(22),
      I2 => counter(7),
      I3 => counter(11),
      I4 => \FSM_sequential_phase_reg_n_0_[0]\,
      O => \counter[31]_i_5_n_0\
    );
\counter[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF9FF9FFFFFF"
    )
        port map (
      I0 => \phase__0\(1),
      I1 => counter(11),
      I2 => counter(10),
      I3 => counter(7),
      I4 => counter(14),
      I5 => counter(13),
      O => \counter[31]_i_6_n_0\
    );
\counter[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBBFFD"
    )
        port map (
      I0 => counter(26),
      I1 => counter(24),
      I2 => counter(7),
      I3 => counter(9),
      I4 => counter(11),
      I5 => counter(31),
      O => \counter[31]_i_7_n_0\
    );
\counter[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(3),
      O => p_0_out(3)
    );
\counter[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(4),
      O => p_0_out(4)
    );
\counter[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(5),
      O => p_0_out(5)
    );
\counter[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(6),
      O => p_0_out(6)
    );
\counter[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(7),
      O => p_0_out(7)
    );
\counter[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(8),
      O => p_0_out(8)
    );
\counter[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \FSM_sequential_phase[1]_i_8_n_0\,
      I1 => \counter[31]_i_2_n_0\,
      I2 => data0(9),
      O => p_0_out(9)
    );
\counter_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(0),
      Q => counter(0)
    );
\counter_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(10),
      Q => counter(10)
    );
\counter_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(11),
      Q => counter(11)
    );
\counter_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(12),
      Q => counter(12)
    );
\counter_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(13),
      Q => counter(13)
    );
\counter_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(14),
      Q => counter(14)
    );
\counter_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(15),
      Q => counter(15)
    );
\counter_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(16),
      Q => counter(16)
    );
\counter_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(17),
      Q => counter(17)
    );
\counter_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(18),
      Q => counter(18)
    );
\counter_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(19),
      Q => counter(19)
    );
\counter_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(1),
      Q => counter(1)
    );
\counter_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(20),
      Q => counter(20)
    );
\counter_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(21),
      Q => counter(21)
    );
\counter_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(22),
      Q => counter(22)
    );
\counter_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(23),
      Q => counter(23)
    );
\counter_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(24),
      Q => counter(24)
    );
\counter_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(25),
      Q => counter(25)
    );
\counter_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(26),
      Q => counter(26)
    );
\counter_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(27),
      Q => counter(27)
    );
\counter_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(28),
      Q => counter(28)
    );
\counter_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(29),
      Q => counter(29)
    );
\counter_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(2),
      Q => counter(2)
    );
\counter_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(30),
      Q => counter(30)
    );
\counter_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(31),
      Q => counter(31)
    );
\counter_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(3),
      Q => counter(3)
    );
\counter_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(4),
      Q => counter(4)
    );
\counter_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(5),
      Q => counter(5)
    );
\counter_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(6),
      Q => counter(6)
    );
\counter_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(7),
      Q => counter(7)
    );
\counter_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(8),
      Q => counter(8)
    );
\counter_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => clk100,
      CE => '1',
      CLR => heartbeat_i_1_n_0,
      D => p_0_out(9),
      Q => counter(9)
    );
heartbeat_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => heartbeat_i_1_n_0
    );
heartbeat_reg: unisim.vcomponents.FDPE
     port map (
      C => clk100,
      CE => '1',
      D => \FSM_sequential_phase_reg_n_0_[0]\,
      PRE => heartbeat_i_1_n_0,
      Q => dout(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk100 : in STD_LOGIC;
    resetn : in STD_LOGIC;
    din : in STD_LOGIC_VECTOR ( 2 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_heartbeat_0_0,heartbeat,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "heartbeat,Vivado 2020.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^din\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^dout\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute x_interface_info : string;
  attribute x_interface_info of resetn : signal is "xilinx.com:signal:reset:1.0 resetn RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of resetn : signal is "XIL_INTERFACENAME resetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  \^din\(2 downto 1) <= din(2 downto 1);
  dout(2 downto 1) <= \^din\(2 downto 1);
  dout(0) <= \^dout\(0);
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_heartbeat
     port map (
      clk100 => clk100,
      dout(0) => \^dout\(0),
      resetn => resetn
    );
end STRUCTURE;
