# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
namespace eval ::optrace {
  variable script "/home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.runs/synth_1/MercuryXU5_SL.tcl"
  variable category "vivado_synth"
}

# Try to connect to running dispatch if we haven't done so already.
# This code assumes that the Tcl interpreter is not using threads,
# since the ::dispatch::connected variable isn't mutex protected.
if {![info exists ::dispatch::connected]} {
  namespace eval ::dispatch {
    variable connected false
    if {[llength [array get env XILINX_CD_CONNECT_ID]] > 0} {
      set result "true"
      if {[catch {
        if {[lsearch -exact [package names] DispatchTcl] < 0} {
          set result [load librdi_cd_clienttcl[info sharedlibextension]] 
        }
        if {$result eq "false"} {
          puts "WARNING: Could not load dispatch client library"
        }
        set connect_id [ ::dispatch::init_client -mode EXISTING_SERVER ]
        if { $connect_id eq "" } {
          puts "WARNING: Could not initialize dispatch client"
        } else {
          puts "INFO: Dispatch client connection id - $connect_id"
          set connected true
        }
      } catch_res]} {
        puts "WARNING: failed to connect to dispatch server - $catch_res"
      }
    }
  }
}
if {$::dispatch::connected} {
  # Remove the dummy proc if it exists.
  if { [expr {[llength [info procs ::OPTRACE]] > 0}] } {
    rename ::OPTRACE ""
  }
  proc ::OPTRACE { task action {tags {} } } {
    ::vitis_log::op_trace "$task" $action -tags $tags -script $::optrace::script -category $::optrace::category
  }
  # dispatch is generic. We specifically want to attach logging.
  ::vitis_log::connect_client
} else {
  # Add dummy proc if it doesn't exist.
  if { [expr {[llength [info procs ::OPTRACE]] == 0}] } {
    proc ::OPTRACE {{arg1 \"\" } {arg2 \"\"} {arg3 \"\" } {arg4 \"\"} {arg5 \"\" } {arg6 \"\"}} {
        # Do nothing
    }
  }
}

proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
OPTRACE "synth_1" START { ROLLUP_AUTO }
set_param chipscope.maxJobs 12
set_msg_config -id {HDL-1065} -limit 10000
set_msg_config  -id {[BD 41-1306]}  -suppress 
set_msg_config  -id {[BD 41-1271]}  -suppress 
OPTRACE "Creating in-memory project" START { }
create_project -in_memory -part xczu5ev-sfvc784-2-i

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.cache/wt [current_project]
set_property parent.project_path /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_FIFO XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property ip_output_repo /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
set_property generic BG_WIDTH=1 [current_fileset]
set src_rc [catch { 
  puts "source /home/amishima/vivado_work/CERN_prework/Mercury_XU5_PE1_Reference_Design/reference_design/scripts/settings.tcl"
  source /home/amishima/vivado_work/CERN_prework/Mercury_XU5_PE1_Reference_Design/reference_design/scripts/settings.tcl
} _RESULT] 
if {$src_rc} { 
  send_msg_id runtcl-1 status "$_RESULT"
  send_msg_id runtcl-2 status "sourcing script /home/amishima/vivado_work/CERN_prework/Mercury_XU5_PE1_Reference_Design/reference_design/scripts/settings.tcl failed"
  return -code error
}
OPTRACE "Creating in-memory project" END { }
OPTRACE "Adding files" START { }
read_vhdl -library xil_defaultlib /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/sources_1/new/MercuryXU5_SL.vhd
add_files /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/sources_1/bd/design_1/design_1.bd
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_zynq_ultra_ps_e_0/design_1_zynq_ultra_ps_e_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_zynq_ultra_ps_e_0/design_1_zynq_ultra_ps_e_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_1/bd_48ac_psr_aclk_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_1/bd_48ac_psr_aclk_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_2/bd_48ac_arsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_3/bd_48ac_rsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_4/bd_48ac_awsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_5/bd_48ac_wsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_6/bd_48ac_bsw_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_10/bd_48ac_s00a2s_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_11/bd_48ac_sarn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_12/bd_48ac_srn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_13/bd_48ac_sawn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_14/bd_48ac_swn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_15/bd_48ac_sbn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_16/bd_48ac_m00s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_17/bd_48ac_m00arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_18/bd_48ac_m00rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_19/bd_48ac_m00awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_20/bd_48ac_m00wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_21/bd_48ac_m00bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_23/bd_48ac_m01s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_24/bd_48ac_m01arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_25/bd_48ac_m01rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_26/bd_48ac_m01awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_27/bd_48ac_m01wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_28/bd_48ac_m01bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_30/bd_48ac_m02s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_31/bd_48ac_m02arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_32/bd_48ac_m02rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_33/bd_48ac_m02awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_34/bd_48ac_m02wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_35/bd_48ac_m02bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_37/bd_48ac_m03s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_38/bd_48ac_m03arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_39/bd_48ac_m03rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_40/bd_48ac_m03awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_41/bd_48ac_m03wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_42/bd_48ac_m03bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_44/bd_48ac_m04s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_45/bd_48ac_m04arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_46/bd_48ac_m04rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_47/bd_48ac_m04awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_48/bd_48ac_m04wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_49/bd_48ac_m04bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_51/bd_48ac_m05s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_52/bd_48ac_m05arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_53/bd_48ac_m05rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_54/bd_48ac_m05awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_55/bd_48ac_m05wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_56/bd_48ac_m05bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_58/bd_48ac_m06s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_59/bd_48ac_m06arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_60/bd_48ac_m06rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_61/bd_48ac_m06awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_62/bd_48ac_m06wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_63/bd_48ac_m06bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_65/bd_48ac_m07s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_66/bd_48ac_m07arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_67/bd_48ac_m07rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_68/bd_48ac_m07awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_69/bd_48ac_m07wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_70/bd_48ac_m07bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_72/bd_48ac_m08s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_73/bd_48ac_m08arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_74/bd_48ac_m08rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_75/bd_48ac_m08awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_76/bd_48ac_m08wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_77/bd_48ac_m08bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_79/bd_48ac_m09s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_80/bd_48ac_m09arn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_81/bd_48ac_m09rn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_82/bd_48ac_m09awn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_83/bd_48ac_m09wn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/bd_0/ip/ip_84/bd_48ac_m09bn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_smartconnect_0_0/ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_3/design_1_axi_gpio_0_3_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_3/design_1_axi_gpio_0_3_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_3/design_1_axi_gpio_0_3.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_bram_ctrl_0_0/design_1_axi_bram_ctrl_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_4/design_1_axi_gpio_0_4_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_4/design_1_axi_gpio_0_4_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_4/design_1_axi_gpio_0_4.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_5/design_1_axi_gpio_0_5_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_5/design_1_axi_gpio_0_5_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_5/design_1_axi_gpio_0_5.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_6/design_1_axi_gpio_0_6_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_6/design_1_axi_gpio_0_6_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_0_6/design_1_axi_gpio_0_6.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_1_0/design_1_axi_gpio_1_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_1_0/design_1_axi_gpio_1_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_gpio_1_0/design_1_axi_gpio_1_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_0/constraints/axi_jtag.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_0/bd_c443_axi_jtag_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_1/constraints/bsip.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_1/bd_c443_bsip_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_2/constraints/bs_switch.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_2/bd_c443_bs_switch_1_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_3/constraints/bs_switch.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_3/bd_c443_bs_switch_2_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_4/constraints/bs_mux.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/ip/ip_4/bd_c443_bs_mux_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/bd_0/bd_c443_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_0/design_1_debug_bridge_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/ip/ip_0/constraints/xsdbm_cc_early.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/ip/ip_0/constraints/xsdbm_cc_late.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/ip/ip_0/constraints/xsdbm_gc_late.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/ip/ip_0/bd_0482_xsdbm_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/bd_0/bd_0482_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_1/design_1_debug_bridge_0_1_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_2/bd_0/ip/ip_0/constraints/axi_jtag.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_2/bd_0/ip/ip_0/bd_05c2_axi_jtag_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_2/bd_0/bd_05c2_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_debug_bridge_0_2/design_1_debug_bridge_0_2_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_system_management_wiz_0/design_1_system_management_wiz_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_system_management_wiz_0/design_1_system_management_wiz_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_system_management_wiz_0/design_1_system_management_wiz_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_chip2chip_0_0/design_1_axi_chip2chip_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_chip2chip_0_0/design_1_axi_chip2chip_0_0_clocks.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_chip2chip_0_0/design_1_axi_chip2chip_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_1/design_1_aurora_64b66b_0_0_fifo_gen_master_clocks.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/synth/design_1_aurora_64b66b_0_0_gt_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/ip_0/synth/design_1_aurora_64b66b_0_0_gt.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_aurora_64b66b_0_0/design_1_aurora_64b66b_0_0_clocks.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_axi_bram_ctrl_0_bram_0/design_1_axi_bram_ctrl_0_bram_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_rst_ps8_99M_0/design_1_rst_ps8_99M_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_rst_ps8_99M_0/design_1_rst_ps8_99M_0.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_rst_ps8_99M_0/design_1_rst_ps8_99M_0_ooc.xdc]
set_property used_in_synthesis false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_ila_0_0/ila_v6_2/constraints/ila_impl.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_ila_0_0/ila_v6_2/constraints/ila_impl.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_ila_0_0/ila_v6_2/constraints/ila.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/ip/design_1_ila_0_0/design_1_ila_0_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.gen/sources_1/bd/design_1/design_1_ooc.xdc]

read_edif /home/amishima/vivado_work/CERN_prework/Mercury_XU5_PE1_Reference_Design/reference_design/src/Mercury_XU5_gmii2rgmii.edn
OPTRACE "Adding files" END { }
# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL.xdc
set_property used_in_implementation false [get_files /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL.xdc]

read_xdc /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL_gmii2rgmii_timing.xdc
set_property used_in_implementation false [get_files /home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL_gmii2rgmii_timing.xdc]

read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 1
close [open __synthesis_is_running__ w]

OPTRACE "synth_design" START { }
synth_design -top MercuryXU5_SL -part xczu5ev-sfvc784-2-i
OPTRACE "synth_design" END { }
if { [get_msg_config -count -severity {CRITICAL WARNING}] > 0 } {
 send_msg_id runtcl-6 info "Synthesis results are not added to the cache due to CRITICAL_WARNING"
}


OPTRACE "write_checkpoint" START { CHECKPOINT }
# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef MercuryXU5_SL.dcp
OPTRACE "write_checkpoint" END { }
OPTRACE "synth reports" START { REPORT }
create_report "synth_1_synth_report_utilization_0" "report_utilization -file MercuryXU5_SL_utilization_synth.rpt -pb MercuryXU5_SL_utilization_synth.pb"
OPTRACE "synth reports" END { }
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
OPTRACE "synth_1" END { }
