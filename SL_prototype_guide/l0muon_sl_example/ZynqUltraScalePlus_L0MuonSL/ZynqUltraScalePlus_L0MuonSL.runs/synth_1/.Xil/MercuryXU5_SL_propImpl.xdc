set_property SRC_FILE_INFO {cfile:/home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL.xdc rfile:../../../MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL.xdc id:1} [current_design]
set_property SRC_FILE_INFO {cfile:/home/amishima/vivado_work/CERN_prework/MercuryXU5_SL/MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL_gmii2rgmii_timing.xdc rfile:../../../MercuryXU5_SL.srcs/constrs_1/new/MercuryXU5_SL_gmii2rgmii_timing.xdc id:2} [current_design]
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN H2    IOSTANDARD LVCMOS18  } [get_ports {LED[0]}];
set_property src_info {type:XDC file:1 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN P9    IOSTANDARD LVCMOS18  } [get_ports {LED[1]}];
set_property src_info {type:XDC file:1 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN K5    IOSTANDARD LVCMOS18  } [get_ports {LED[2]}];
set_property src_info {type:XDC file:1 line:28 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A10   IOSTANDARD LVCMOS18  } [get_ports {ETH1_MDC}]
set_property src_info {type:XDC file:1 line:29 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN J11   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RXD[0]}]
set_property src_info {type:XDC file:1 line:30 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN J10   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RXD[1]}]
set_property src_info {type:XDC file:1 line:31 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN K13   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RXD[2]}]
set_property src_info {type:XDC file:1 line:32 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN K12   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RXD[3]}]
set_property src_info {type:XDC file:1 line:33 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN G11   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RXCLK}]
set_property src_info {type:XDC file:1 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN H11   IOSTANDARD LVCMOS18  } [get_ports {ETH1_TXD[0]}]
set_property src_info {type:XDC file:1 line:35 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN G10   IOSTANDARD LVCMOS18  } [get_ports {ETH1_TXD[1]}]
set_property src_info {type:XDC file:1 line:36 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN J12   IOSTANDARD LVCMOS18  } [get_ports {ETH1_TXD[2]}]
set_property src_info {type:XDC file:1 line:37 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN H12   IOSTANDARD LVCMOS18  } [get_ports {ETH1_TXD[3]}]
set_property src_info {type:XDC file:1 line:38 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN F12   IOSTANDARD LVCMOS18  } [get_ports {ETH1_TXCLK}]
set_property src_info {type:XDC file:1 line:39 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B11   IOSTANDARD LVCMOS18  } [get_ports {ETH1_MDIO}]
set_property src_info {type:XDC file:1 line:40 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN C11   IOSTANDARD LVCMOS18  } [get_ports {ETH1_INT_N}]
set_property src_info {type:XDC file:1 line:41 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B10   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RESET_N}]
set_property src_info {type:XDC file:1 line:42 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN F10   IOSTANDARD LVCMOS18  } [get_ports {ETH1_RXCTL}]
set_property src_info {type:XDC file:1 line:43 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN F11   IOSTANDARD LVCMOS18  } [get_ports {ETH1_TXCTL}]
set_property src_info {type:XDC file:1 line:48 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A2  IOSTANDARD LVCMOS18  } [get_ports {SIRST}];
set_property src_info {type:XDC file:1 line:49 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A3  IOSTANDARD LVCMOS18  } [get_ports {SI44RST}];
set_property src_info {type:XDC file:1 line:51 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A5  IOSTANDARD LVCMOS18} [get_ports {Si5345_INSEL_tri_o[0]}]; # Module connector pin B103
set_property src_info {type:XDC file:1 line:52 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN C6  IOSTANDARD LVCMOS18} [get_ports {Si5345_INSEL_tri_o[1]}]; # Module connector pin B107
set_property src_info {type:XDC file:1 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B6  IOSTANDARD LVCMOS18} [get_ports {CFGPROG_tri_o[0]}];      # Module connector pin B109
set_property src_info {type:XDC file:1 line:55 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A7  IOSTANDARD LVCMOS18} [get_ports {CFGINIT}];               # Module connector pin B111
set_property src_info {type:XDC file:1 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A6  IOSTANDARD LVCMOS18} [get_ports {CFGDONE}];               # Module connector pin B113
set_property src_info {type:XDC file:1 line:58 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AA13  IOSTANDARD LVCMOS18  } [get_ports {FIRINT}];            # Module connector pin A37
set_property src_info {type:XDC file:1 line:60 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AH11  IOSTANDARD LVCMOS18  } [get_ports {FPALERT1}];      # Module connector pin A69
set_property src_info {type:XDC file:1 line:61 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AF11  IOSTANDARD LVCMOS18  } [get_ports {FPTHERM1}];      # Module connector pin A73
set_property src_info {type:XDC file:1 line:62 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AD15  IOSTANDARD LVCMOS18  } [get_ports {LOS0}];          # Module connector pin A43
set_property src_info {type:XDC file:1 line:63 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AD14  IOSTANDARD LVCMOS18  } [get_ports {LOS1}];          # Module connector pin A45
set_property src_info {type:XDC file:1 line:64 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B5    IOSTANDARD LVCMOS18  } [get_ports {SI44INTR}];      # Module connector pin B101
set_property src_info {type:XDC file:1 line:65 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B4    IOSTANDARD LVCMOS18  } [get_ports {SI44LOL}];       # Module connector pin B97
set_property src_info {type:XDC file:1 line:66 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A4    IOSTANDARD LVCMOS18  } [get_ports {SI44LOSX}];      # Module connector pin B99
set_property src_info {type:XDC file:1 line:67 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B3    IOSTANDARD LVCMOS18  } [get_ports {SIINTR}];        # Module connector pin B91
set_property src_info {type:XDC file:1 line:68 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A1    IOSTANDARD LVCMOS18  } [get_ports {SILOL}]; # Module connector pin B89
set_property src_info {type:XDC file:1 line:73 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AG11  IOSTANDARD LVCMOS18  } [get_ports {ZYALERT1}];      # Module connector pin A75
set_property src_info {type:XDC file:1 line:76 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AB10  IOSTANDARD LVCMOS18  } [get_ports {ZYTHERM1}];      # Module connector pin A79
set_property src_info {type:XDC file:1 line:80 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AB13  IOSTANDARD LVCMOS18  } [get_ports {FIRRST}];
set_property src_info {type:XDC file:1 line:82 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN W12   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[0]}];
set_property src_info {type:XDC file:1 line:83 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN W11   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[1]}];
set_property src_info {type:XDC file:1 line:84 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AE15  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[2]}];
set_property src_info {type:XDC file:1 line:85 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AE14  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[3]}];
set_property src_info {type:XDC file:1 line:86 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AE13  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[4]}];
set_property src_info {type:XDC file:1 line:87 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AF13  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[5]}];
set_property src_info {type:XDC file:1 line:88 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AG13  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[6]}];
set_property src_info {type:XDC file:1 line:89 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AH13  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[7]}];
set_property src_info {type:XDC file:1 line:90 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AB11  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[8]}];
set_property src_info {type:XDC file:1 line:91 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AC11  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[9]}];
set_property src_info {type:XDC file:1 line:92 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AA11  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[10]}];
set_property src_info {type:XDC file:1 line:93 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AA10  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[11]}];
set_property src_info {type:XDC file:1 line:94 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN Y9    IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[12]}];
set_property src_info {type:XDC file:1 line:95 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AA8   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[13]}];
set_property src_info {type:XDC file:1 line:96 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN W10   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[14]}];
set_property src_info {type:XDC file:1 line:97 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN Y10   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[15]}];
set_property src_info {type:XDC file:1 line:98 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AC12  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[16]}];
set_property src_info {type:XDC file:1 line:99 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN AD12  IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[17]}];
set_property src_info {type:XDC file:1 line:100 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN E10   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[18]}];
set_property src_info {type:XDC file:1 line:101 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN D10   IOSTANDARD LVCMOS18  } [get_ports {FIRSEL_tri_o[19]}];
set_property src_info {type:XDC file:1 line:104 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN C1    IOSTANDARD LVCMOS18  } [get_ports {ZYNQTMS}];
set_property src_info {type:XDC file:1 line:105 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B1    IOSTANDARD LVCMOS18  } [get_ports {ZYNQTCK}];
set_property src_info {type:XDC file:1 line:106 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN G5    IOSTANDARD LVCMOS18  } [get_ports {ZYNQTDI}];
set_property src_info {type:XDC file:1 line:107 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN F5    IOSTANDARD LVCMOS18  } [get_ports {ZYNQTDO}];
set_property src_info {type:XDC file:1 line:110 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V6 [get_ports GT_DIFF_REFCLK1_clk_p]; # Module connector pin B10
set_property src_info {type:XDC file:1 line:111 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V5 [get_ports GT_DIFF_REFCLK1_clk_n]; # Module connector pin B12
set_property src_info {type:XDC file:1 line:114 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W4 [get_ports {GT_SERIAL_Z2F_txp[0]}]; # Module connector pin B45
set_property src_info {type:XDC file:1 line:115 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W3 [get_ports {GT_SERIAL_Z2F_txn[0]}]; # Module connector pin B47
set_property src_info {type:XDC file:1 line:120 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y2 [get_ports {GT_SERIAL_F2Z_rxp[0]}]; # Module connector pin B48
set_property src_info {type:XDC file:1 line:121 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y1 [get_ports {GT_SERIAL_F2Z_rxn[0]}]; # Module connector pin B50
set_property src_info {type:XDC file:2 line:30 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_DELAY_GROUP rgmii_tx_group [get_nets -of_objects [get_pins design_1_i/clk_wiz_0/inst/clkout1_buf/O] ]
set_property src_info {type:XDC file:2 line:31 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_DELAY_GROUP rgmii_tx_group [get_nets -of_objects [get_pins design_1_i/clk_wiz_0/inst/clkout2_buf/O] ]
set_property src_info {type:XDC file:2 line:33 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -to [get_clocks RGMII_RX_CLK]
set_property src_info {type:XDC file:2 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks RGMII_RX_CLK] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -rise_from [get_clocks RGMII_RX_CLK_VIRT] -rise_to [get_clocks RGMII_RX_CLK] -hold
set_property src_info {type:XDC file:2 line:45 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -fall_from [get_clocks RGMII_RX_CLK_VIRT] -fall_to [get_clocks RGMII_RX_CLK] -hold
set_property src_info {type:XDC file:2 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_multicycle_path -fall_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -fall_to [get_clocks RGMII_TX_CLK_90] -setup -start 0
set_property src_info {type:XDC file:2 line:55 export:INPUT save:INPUT read:READ} [current_design]
set_multicycle_path -rise_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -rise_to [get_clocks RGMII_TX_CLK_90] -setup -start 0
set_property src_info {type:XDC file:2 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_multicycle_path -rise_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -fall_to [get_clocks RGMII_TX_CLK_90] -hold -start 0
set_property src_info {type:XDC file:2 line:57 export:INPUT save:INPUT read:READ} [current_design]
set_multicycle_path -fall_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -rise_to [get_clocks RGMII_TX_CLK_90] -hold -start 0
set_property src_info {type:XDC file:2 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -rise_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -fall_to [get_clocks RGMII_TX_CLK_90] -setup
set_property src_info {type:XDC file:2 line:60 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -fall_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -rise_to [get_clocks RGMII_TX_CLK_90] -setup
set_property src_info {type:XDC file:2 line:61 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -rise_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -rise_to [get_clocks RGMII_TX_CLK_90] -hold
set_property src_info {type:XDC file:2 line:62 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -fall_from [get_clocks clk_out1_design_1_clk_wiz_0_0] -fall_to [get_clocks RGMII_TX_CLK_90] -hold
set_property src_info {type:XDC file:2 line:65 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/Gmii_RxDv_sync_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:66 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxDv_sync_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:67 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/Gmii_RxDv_sync_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:68 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxDv_sync_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:69 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxDv_sync_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:70 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxDv_sync_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:72 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells {i_gmii2rgmii/Gmii_RxD_sync_reg[*]}] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:73 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/Gmii_RxD_sync_reg[*]}] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:74 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells {i_gmii2rgmii/Gmii_RxD_sync_reg[*]}] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:75 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/Gmii_RxD_sync_reg[*]}] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:76 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/Gmii_RxD_sync_reg[*]}] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/Gmii_RxD_sync_reg[*]}] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:79 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/Gmii_RxEr_sync_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:80 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxEr_sync_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:81 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/Gmii_RxEr_sync_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:82 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxEr_sync_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:83 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxEr_sync_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:84 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/Gmii_RxEr_sync_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:87 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEn_Reg_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:88 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEn_Reg_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:89 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEn_Reg_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:90 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEn_Reg_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:91 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEn_Reg_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:92 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEn_Reg_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:94 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells {i_gmii2rgmii/i_rgmii_gmii/Gmii_TxD_Reg_reg[*]}] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:95 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/i_rgmii_gmii/Gmii_TxD_Reg_reg[*]}] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:96 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells {i_gmii2rgmii/i_rgmii_gmii/Gmii_TxD_Reg_reg[*]}] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:97 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/i_rgmii_gmii/Gmii_TxD_Reg_reg[*]}] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:98 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/i_rgmii_gmii/Gmii_TxD_Reg_reg[*]}] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:99 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells {i_gmii2rgmii/i_rgmii_gmii/Gmii_TxD_Reg_reg[*]}] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:101 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEr_Reg_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:102 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEr_Reg_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:103 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks Clk2_5] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEr_Reg_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:104 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEr_Reg_reg] -to [get_clocks Clk2_5]
set_property src_info {type:XDC file:2 line:105 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out1_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEr_Reg_reg] -to [get_clocks clk_out3_design_1_clk_wiz_0_0]
set_property src_info {type:XDC file:2 line:106 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_clocks clk_out3_design_1_clk_wiz_0_0] -through [get_cells i_gmii2rgmii/i_rgmii_gmii/Gmii_TxEr_Reg_reg] -to [get_clocks clk_out1_design_1_clk_wiz_0_0]
