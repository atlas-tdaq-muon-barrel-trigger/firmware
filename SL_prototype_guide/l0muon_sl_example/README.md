
# L0Muon SL example

## Brief Introduction

This repository is prepared for the launching of the L0Muon Sector Logic board at the Laser Room (2175-R-B03) in Nov. 2022.<br>
Minimal designs of the FPGA and MPSoC are contained.
This allows to configure the Si5345, set up the XVC server, access the AXI Chip2Chip, and so on.


## About This Repository

- Quick tutorial is described in [`QuickStartGuide.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/QuickStartGuide.md)
- MPSoC PL firmware design is described in [`MPSoC_firmware.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/MPSoC_firmware.md)
- How to build the bootfs with PetaLinux is described in [`Build_bootfs.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/Build_bootfs.md)
- FPGA firmware design is described in [`FPGA_firmware.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/FPGA_firmware.md)
- Issues which are not critical to the commissioning but to be discussed are in [`discussion.md`](https://gitlab.cern.ch/amishima/l0muon_sl_example/-/blob/master/MDs/discussion.md)


<details><summary>View the structure of this repository</summary><div>

```bash
l0muon_sl_example/
├── MDs/                                 # Markdown files
│   ├── Build_bootfs.md
│   ├── FPGA_firmware.md
│   ├── MPSoC_firmware.md
│   ├── QuickStartGuide.md
│   └── discussion.md
├── PetaLinux/                           # PetaLinux projects
│   └── ZynqUltraScalePlus_L0MuonSL.bsp
├── VirtexUltraScalePlus_L0MuonSL/       # Vivado project directory for FPGA
│   ├── VirtexUltraScalePlus_L0MuonSL.gen/
│   ├── VirtexUltraScalePlus_L0MuonSL.ip_user_files/
│   ├── VirtexUltraScalePlus_L0MuonSL.srcs/
│   └── VirtexUltraScalePlus_L0MuonSL.xpr
├── ZynqUltraScalePlus_L0MuonSL/         # Vivado project directory for MPSoC
│   ├── ZynqUltraScalePlus_L0MuonSL.cache/
│   ├── ZynqUltraScalePlus_L0MuonSL.gen/
│   ├── ZynqUltraScalePlus_L0MuonSL.hw/
│   ├── ZynqUltraScalePlus_L0MuonSL.ip_user_files/
│   ├── ZynqUltraScalePlus_L0MuonSL.runs/
│   ├── ZynqUltraScalePlus_L0MuonSL.srcs/
│   └── ZynqUltraScalePlus_L0MuonSL.xpr
├── example_files/                       # Utility files for the tutorial
│   ├── bootfs/
│   │   ├── BOOT.BIN
│   │   ├── boot.scr
│   │   ├── image.ub
│   │   └── uEnv.txt
│   ├── devmem/
│   │   └── devmem.c
│   ├── petalinux/
│   │   ├── devicetree/
│   │   │       ├── endcap_sl.dtsi
│   │   │       ├── pl-custom.dtsi
│   │   │       ├── system-user.dtsi
│   │   │       └── zynqmp_enclustra_mercury_xu5.dtsi
│   │   └── boot.cmd.default.initrd
│   ├── si5345/
│   │   ├── Makefile
│   │   ├── Si5345-RevD-OUT23-125M-Registers.h
│   │   ├── Si5345-RevD-OUT23-125M.slabtimeproj
│   │   └── si5345.c
│   └── xvc/
│       └── xvcserver.c
├── figures/                             # Figures used in Markdown files   
├── .gitignore
└── README.md                            # This README

```

</div></details>



If you find any problems or have any questions about this repository, feel free to contact me.<br>
Akihiro Mishima:<br>
akihiro.mishima@cern.ch (This may expire in Mar. 2023)<br>
akihiromishima0306@gmail.com
