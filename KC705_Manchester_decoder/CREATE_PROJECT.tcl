set ProjectDir ./

if {[file exists $ProjectDir/Vivado]} {[file rename $ProjectDir/Vivado $ProjectDir/Vivado.[clock format [clock seconds] -format %Y-%m-%d.%H-%M-%S]]}

file mkdir $ProjectDir/Vivado

create_project Manchester_decoder_KC705 $ProjectDir/Vivado -part xc7k325tffg900-2

set_property board_part xilinx.com:kc705:part0:1.6 [current_project]

set_property target_language VHDL [current_project]

import_ip $ProjectDir/IP/mmcm_sysclock200/mmcm_sysclock200.xci
import_ip $ProjectDir/IP/mmcm_userclock156/mmcm_userclock156.xci
import_ip $ProjectDir/IP/deserialiser/deserialiser.xci
import_ip $ProjectDir/IP/ila_manchester/ila_manchester.xci
import_ip $ProjectDir/IP/vio_manchester/vio_manchester.xci
import_ip $ProjectDir/IP/vio_hit_pattern/vio_hit_pattern.xci
import_ip $ProjectDir/IP/serialiser_6b_out/serialiser_6b_out.xci

add_files -norecurse $ProjectDir/RTL/manchester_decoding.vhd
add_files -norecurse $ProjectDir/RTL/top.vhd

add_files -fileset constrs_1 -norecurse $ProjectDir/Constraints/top.xdc

set_property SOURCE_SET sources_1 [get_filesets sim_1]
add_files -fileset sim_1 -norecurse $ProjectDir/TB/top_tb.vhd
