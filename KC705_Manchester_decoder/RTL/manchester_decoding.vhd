-------------------------------------------------------
-- File:          manchester_decoding.vhd
-- Project:       BI DCT firmware
-- Author:        Riccardo Vari
-- Last modified: 2023/05/03
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;



entity manchester_decoding is
port (
    clock          : in  std_logic;
    reset          : in  std_logic;
    data6b_in      : in  std_logic_vector(5 downto 0);
    data18b_out    : out std_logic_vector(17 downto 0);
    data18b_we_out : out std_logic;
    debug_m_100b_out                : out std_logic_vector(99 downto 0);
    debug_manchester_stream_out     : out std_logic_vector(99 downto 0);
    debug_start_counter_out         : out std_logic_vector(31 downto 0);
    debug_fsm_error_counter_out     : out std_logic_vector(31 downto 0);
    debug_decoded_words_counter_out : out std_logic_vector(31 downto 0)
);
end manchester_decoding;



architecture RTL of manchester_decoding is
    
    signal m_100b : std_logic_vector(99 downto 0);
    
    signal manchester_stream : std_logic_vector(99 downto 0);
    
    type states is (idle, decode);
    signal state : states;

    signal decoded_data : std_logic_vector(19 downto 0);
    
    signal debug_start_counter : std_logic_vector(31 downto 0);
    signal debug_fsm_error_counter : std_logic_vector(31 downto 0);
    signal debug_decoded_words_counter : std_logic_vector(31 downto 0);

    signal debug_data18b : std_logic_vector(17 downto 0);

begin

    debug_m_100b_out <= m_100b;
    debug_manchester_stream_out <= manchester_stream;
    debug_start_counter_out <= debug_start_counter;
    debug_fsm_error_counter_out <= debug_fsm_error_counter;
    debug_decoded_words_counter_out <= debug_decoded_words_counter;

    manchester_sampling_stream_logic : process(clock) begin
        if rising_edge(clock) then
            m_100b <= m_100b(93 downto 0) & data6b_in(0) & data6b_in(1) & data6b_in(2) & data6b_in(3) & data6b_in(4) & data6b_in(5);
        end if;
    end process;

    decoding_logic : process(clock) begin
        if rising_edge(clock) then
            if reset = '1' then
                state <= idle;
                debug_start_counter <= (others => '0');
                debug_fsm_error_counter <= (others => '0');
                debug_decoded_words_counter <= (others => '0');
                debug_data18b <= (others => '0');
            else
                case state is
                    when idle =>
                        decoded_data <= "00000000000000000001";
                        data18b_we_out <= '0';
                        data18b_out <= (others => '0');
                        --manchester_stream <= m_100b;
                        if m_100b(99 downto 93) = "0000001" then
                            --manchester_stream <= m_100b(93 downto 0) & "000000";
                            manchester_stream <= std_logic_vector(shift_left(unsigned(m_100b), 6));
                            state <= decode;
                            debug_start_counter <= debug_start_counter + 1;
                        elsif m_100b(98 downto 92) = "0000001" then
                            --manchester_stream <= m_100b(92 downto 0) & "0000000";
                            manchester_stream <= std_logic_vector(shift_left(unsigned(m_100b), 7));
                            state <= decode;
                            debug_start_counter <= debug_start_counter + 1;
                        elsif m_100b(97 downto 91) = "0000001" then
                            --manchester_stream <= m_100b(91 downto 0) & "00000000";
                            manchester_stream <= std_logic_vector(shift_left(unsigned(m_100b), 8));
                            state <= decode;
                            debug_start_counter <= debug_start_counter + 1;
                        elsif m_100b(96 downto 90) = "0000001" then
                            --manchester_stream <= m_100b(90 downto 0) & "000000000";
                            manchester_stream <= std_logic_vector(shift_left(unsigned(m_100b), 9));
                            state <= decode;
                            debug_start_counter <= debug_start_counter + 1;
                        elsif m_100b(95 downto 89) = "0000001" then
                            --manchester_stream <= m_100b(89 downto 0) & "0000000000";
                            manchester_stream <= std_logic_vector(shift_left(unsigned(m_100b), 10));
                            state <= decode;
                            debug_start_counter <= debug_start_counter + 1;
                        elsif m_100b(94 downto 88) = "0000001" then
                            --manchester_stream <= m_100b(88 downto 0) & "00000000000";
                            manchester_stream <= std_logic_vector(shift_left(unsigned(m_100b), 11));
                            state <= decode;
                            debug_start_counter <= debug_start_counter + 1;
                        end if;
                    when decode =>
                        if decoded_data(19 downto 18) = "10" then
                            data18b_we_out <= '1';
                            data18b_out <= decoded_data(17 downto 0);
                            state <= idle;
                            debug_decoded_words_counter <= debug_decoded_words_counter + 1;
                            debug_data18b <= decoded_data(17 downto 0);
                            if decoded_data(17 downto 0) /= debug_data18b + 1 then
                                assert false report "DECODING ERROR" severity warning;
                            end if;
                        else
                            case manchester_stream(99 downto 94) is
                                when "000110" => decoded_data <= decoded_data(18 downto 0) & '1'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 5));
                                when "000111" => decoded_data <= decoded_data(18 downto 0) & '1'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 5));
                                when "001100" => decoded_data <= decoded_data(18 downto 0) & '1'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 4));
                                when "001110" => decoded_data <= decoded_data(18 downto 0) & '1'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 5));
                                when "001111" => decoded_data <= decoded_data(18 downto 0) & '1'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 4));
                                when "110000" => decoded_data <= decoded_data(18 downto 0) & '0'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 4));
                                when "110001" => decoded_data <= decoded_data(18 downto 0) & '0'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 5));
                                when "110011" => decoded_data <= decoded_data(18 downto 0) & '0'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 4));
                                when "111000" => decoded_data <= decoded_data(18 downto 0) & '0'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 5));
                                when "111001" => decoded_data <= decoded_data(18 downto 0) & '0'; manchester_stream <= std_logic_vector(shift_left(unsigned(manchester_stream), 5));
                                when others =>
                                    --assert false report "DECODING ERROR" severity warning;
                                    debug_fsm_error_counter <= debug_fsm_error_counter + 1;
                                    state <= idle;
                            end case;
                        end if;
                    when others =>
                        state <= idle;
                end case;
            end if;
        end if;
    end process;

end RTL;