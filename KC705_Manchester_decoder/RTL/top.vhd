library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

library STD;
use STD.textio.all;



entity top is
port (
    -- system_clock from on-board 200 MHz fixed frequency oscillator, external 100 Ohm resistor on board, MRCC
    sys_clock_p  : in std_logic;
    sys_clock_n  : in std_logic;
    
    -- user_clock from on-board Si570 oscillator, default frequency 156.250 MHz, NO external 100 Ohm resistor, MRCC
    user_clock_p : in std_logic;
    user_clock_n : in std_logic;
    
    -- user SMA clock, MRCC
    user_sma_clock_p : out std_logic;
    user_sma_clock_n : out std_logic;
    
    -- user SMA GPIO
    --user_sma_gpio_p : out std_logic;
    user_sma_gpio_n : out std_logic;
    
    -- FMC HPC pins
    FMC_HPC_HA00_CC_P : in std_logic; -- FMC pin F4,  MEZZANINE BOARD J3 PIN 1
  --FMC_HPC_HA00_CC_N : in std_logic; -- FMC pin F5,  MEZZANINE BOARD J3 PIN 3
    FMC_HPC_HA01_CC_P : in std_logic; -- FMC pin E2,  MEZZANINE BOARD J3 PIN 5
  --FMC_HPC_HA01_CC_N : in std_logic; -- FMC pin E3,  MEZZANINE BOARD J3 PIN 7
    FMC_HPC_HA02_P    : in std_logic; -- FMC pin K7,  MEZZANINE BOARD J3 PIN 9
  --FMC_HPC_HA02_N    : in std_logic; -- FMC pin K8,  MEZZANINE BOARD J3 PIN 11
    FMC_HPC_HA03_P    : in std_logic; -- FMC pin J6,  MEZZANINE BOARD J3 PIN 13
  --FMC_HPC_HA03_N    : in std_logic; -- FMC pin J7,  MEZZANINE BOARD J3 PIN 15
  --FMC_HPC_HA04_P    : in std_logic; -- FMC pin F7,  MEZZANINE BOARD J3 PIN 17
  --FMC_HPC_HA04_N    : in std_logic; -- FMC pin F8,  MEZZANINE BOARD J3 PIN 19
  --FMC_HPC_HA05_P    : in std_logic; -- FMC pin E6,  MEZZANINE BOARD J3 PIN 21
  --FMC_HPC_HA05_N    : in std_logic; -- FMC pin E7,  MEZZANINE BOARD J3 PIN 23
  --FMC_HPC_HA06_P    : in std_logic; -- FMC pin K10, MEZZANINE BOARD J3 PIN 25
  --FMC_HPC_HA06_N    : in std_logic; -- FMC pin K11, MEZZANINE BOARD J3 PIN 27
  --FMC_HPC_HA07_P    : in std_logic; -- FMC pin J9,  MEZZANINE BOARD J3 PIN 29
  --FMC_HPC_HA07_N    : in std_logic; -- FMC pin J10, MEZZANINE BOARD J3 PIN 31
  --FMC_HPC_HA08_P    : in std_logic; -- FMC pin F10, MEZZANINE BOARD J3 PIN 33
  --FMC_HPC_HA08_N    : in std_logic; -- FMC pin F11, MEZZANINE BOARD J3 PIN 35
  --FMC_HPC_HA09_P    : in std_logic; -- FMC pin E9,  MEZZANINE BOARD J3 PIN 37
  --FMC_HPC_HA09_N    : in std_logic; -- FMC pin E10, MEZZANINE BOARD J3 PIN 39
    FMC_HPC_HA10_P    : in std_logic; -- FMC pin K13, MEZZANINE BOARD J3 PIN 2
    FMC_HPC_HA10_N    : in std_logic; -- FMC pin K14, MEZZANINE BOARD J3 PIN 4
    FMC_HPC_HA11_P    : in std_logic; -- FMC pin J12, MEZZANINE BOARD J3 PIN 6
    FMC_HPC_HA11_N    : in std_logic; -- FMC pin J13, MEZZANINE BOARD J3 PIN 8
    FMC_HPC_HA12_P    : in std_logic; -- FMC pin F13, MEZZANINE BOARD J3 PIN 10
    FMC_HPC_HA12_N    : in std_logic; -- FMC pin F14, MEZZANINE BOARD J3 PIN 12
    FMC_HPC_HA13_P    : in std_logic; -- FMC pin E12, MEZZANINE BOARD J3 PIN 14
    FMC_HPC_HA13_N    : in std_logic; -- FMC pin E13, MEZZANINE BOARD J3 PIN 16
  --FMC_HPC_HA14_P    : in std_logic; -- FMC pin J15, MEZZANINE BOARD J3 PIN 18
  --FMC_HPC_HA14_N    : in std_logic; -- FMC pin J16, MEZZANINE BOARD J3 PIN 20
  --FMC_HPC_HA15_P    : in std_logic; -- FMC pin F16, MEZZANINE BOARD J3 PIN 22
  --FMC_HPC_HA15_N    : in std_logic; -- FMC pin F17, MEZZANINE BOARD J3 PIN 24
  --FMC_HPC_HA16_P    : in std_logic; -- FMC pin E15, MEZZANINE BOARD J3 PIN 26
  --FMC_HPC_HA16_N    : in std_logic; -- FMC pin E16, MEZZANINE BOARD J3 PIN 28
  --FMC_HPC_HA17_CC_P : in std_logic; -- FMC pin K16, MEZZANINE BOARD J3 PIN 30
  --FMC_HPC_HA17_CC_N : in std_logic; -- FMC pin K17, MEZZANINE BOARD J3 PIN 32
    FMC_HPC_HA18_P    : in std_logic; -- FMC pin J18, MEZZANINE BOARD J3 PIN 34
  --FMC_HPC_HA18_N    : in std_logic; -- FMC pin J19, MEZZANINE BOARD J3 PIN 36
    FMC_HPC_HA19_P    : in std_logic; -- FMC pin F19, MEZZANINE BOARD J3 PIN 38
  --FMC_HPC_HA19_N    : in std_logic; -- FMC pin F20, MEZZANINE BOARD J3 PIN 40
    
    -- board GPIO_LEDs
    GPIO_LED  : out std_logic_vector(7 downto 0);
    
    -- board push button reset, active high, SW7
    cpu_reset : in  std_logic;
    
    -- board push button reset, active high. SW3
    sw_reset  : in  std_logic;
    
    -- only for simulation, connected to FMC LPC connector
    bcid_out  : out std_logic_vector(11 downto 0)
    );
end top;



architecture RTL of top is

    -- vA1 : added manchester_stream to ILAs; removed control on 6-bit when last bit has been decoded in manchester_decoding.vhd
    constant firmware_version : std_logic_vector(7 downto 0) := X"A1";

    type array_8x6b is array (7 downto 0) of std_logic_vector(5 downto 0);
    type array_8x8b is array (7 downto 0) of std_logic_vector(7 downto 0);
    type array_8x18b is array (7 downto 0) of std_logic_vector(17 downto 0);
    type array_8x12b is array(7 downto 0) of std_logic_vector(11 downto 0);
    type array_8x22b is array(7 downto 0) of std_logic_vector(21 downto 0);
    type array_8x32b is array(7 downto 0) of std_logic_vector(31 downto 0);
    type array_8x100b is array(7 downto 0) of std_logic_vector(99 downto 0);
    type array_8x172b is array(7 downto 0) of std_logic_vector(171 downto 0);

    signal clock40_edge : std_logic := '0';
    signal clock40_edge200_r : std_logic;
    signal clock40_rising200 : std_logic;

    signal clock600_mmcm : std_logic;
    signal clock200_mmcm : std_logic;
    signal clock40_mmcm  : std_logic;

    signal clock468750_mmcm : std_logic;
    signal clock156250_mmcm : std_logic;

    --signal userclock156 : std_logic;

    signal mmcm_sysclock200_locked : std_logic;
    signal mmcm_sysclock200_locked_r0 : std_logic;
    signal mmcm_sysclock200_locked_r1 : std_logic;
    signal int_reset200 : std_logic;

    signal mmcm_userclock156_locked : std_logic;
    signal mmcm_userclock156_locked_r0 : std_logic;
    signal mmcm_userclock156_locked_r1 : std_logic;
    signal int_reset156 : std_logic;
    
    signal manchester_in : std_logic_vector(7 downto 0);
    
    signal manchester_diff_in : std_logic_vector(3 downto 0);
    signal manchester_diff_in_p : std_logic_vector(3 downto 0);
    signal manchester_diff_in_n : std_logic_vector(3 downto 0);
    
    signal discriminator_or_in1 : std_logic;
    signal discriminator_or_in2 : std_logic;
        
    signal bcid : std_logic_vector(11 downto 0);

    signal manchester_6b : array_8x6b := (others => (others => '0'));

    signal decoded_data_18b : array_8x18b := (others => (others => '0'));
    signal decoded_data_18b_valid : std_logic_vector(7 downto 0);
    
    signal vio_probe : std_logic_vector(179 downto 0);
    
    signal hit_pattern156 : std_logic_vector(11 downto 0);
    signal serialiser_hit_pattern156 : std_logic_vector(5 downto 0);
    signal hit_pattern156_counter : std_logic_vector(15 downto 0) := (others => '0');
    signal hit_pattern156_counter_max : std_logic_vector(15 downto 0);

    signal hit_pattern200 : std_logic_vector(11 downto 0);
    signal serialiser_hit_pattern200 : std_logic_vector(5 downto 0);
    signal hit_pattern200_counter : std_logic_vector(15 downto 0) := (others => '0');
    signal hit_pattern200_counter_max : std_logic_vector(15 downto 0);

    signal debug_generated_hit200_counter : std_logic_vector(31 downto 0);
    signal debug_generated_hit156_counter : std_logic_vector(31 downto 0);
    signal debug_generated_hit156_counter_r : std_logic_vector(31 downto 0);
    signal debug_generated_hit156_counter_r2 : std_logic_vector(31 downto 0);
    signal debug_manchester_100b : array_8x100b;
    signal debug_manchester_stream_out : array_8x100b;
    signal debug_start_counter : array_8x32b;
    signal debug_decoded_hit_counter : array_8x32b;
    signal debug_fsm_error_counter : array_8x32b;
    
    component deserialiser
    port (
        data_in_from_pins : in  std_logic;
        data_in_to_device : out std_logic_vector(5 downto 0);
        bitslip           : in  std_logic;
        clk_in            : in  std_logic;
        clk_div_in        : in  std_logic;
        io_reset          : in  std_logic
    );
    end component;

    component serialiser_10b_out
    port (
        data_out_from_device : in  std_logic_vector(9 downto 0);
        data_out_to_pins     : out std_logic;
        clk_in               : in  std_logic;
        clk_div_in           : in  std_logic;
        io_reset             : in  std_logic
    );
    end component;

    component serialiser_6b_out
    port (
        data_out_from_device : in  std_logic_vector(5 downto 0);
        data_out_to_pins     : out std_logic;
        clk_in               : in  std_logic;
        clk_div_in           : in  std_logic;
        io_reset             : in  std_logic
    );
    end component;

    component mmcm_sysclock200
    port (
        clk_out600 : out std_logic;
        clk_out200 : out std_logic;
        clk_out40  : out std_logic;
        reset      : in  std_logic;
        locked     : out std_logic;
        clk_in1_p  : in  std_logic;
        clk_in1_n  : in  std_logic
    );
    end component;
 
    component mmcm_userclock156
    port (
        clk_out468750 : out std_logic;
        clk_out156250 : out std_logic;
        reset         : in  std_logic;
        locked        : out std_logic;
        clk_in1_p     : in  std_logic;
        clk_in1_n     : in  std_logic
    );
    end component;
 
    component ila_manchester
    port (
        clk     : in std_logic;
        probe0  : in std_logic_vector(11 downto 0);
        probe1  : in std_logic_vector(5 downto 0);
        probe2  : in std_logic_vector(99 downto 0);
        probe3  : in std_logic_vector(17 downto 0);
        probe4  : in std_logic_vector(0 downto 0);
        probe5  : in std_logic_vector(0 downto 0);
        probe6  : in std_logic_vector(0 downto 0);
        probe7  : in std_logic_vector(31 downto 0);
        probe8  : in std_logic_vector(31 downto 0);
        probe9  : in std_logic_vector(31 downto 0);
        probe10 : in std_logic_vector(31 downto 0);
        probe11 : in std_logic_vector(31 downto 0);
        probe12 : in std_logic_vector(99 downto 0)
    );
    end component;

    component vio_manchester
    port (
        clk       : in std_logic;
        probe_in0 : in std_logic_vector(179 downto 0)
        );
    end component;

    component vio_hit_pattern
    port (
        clk        : in  std_logic;
        probe_out0 : out std_logic_vector(11 downto 0);
        probe_out1 : out std_logic_vector(15 downto 0)
        );
    end component;

begin

    bcid_out <= bcid;

    manchester_diff_in_p <= FMC_HPC_HA13_P & FMC_HPC_HA12_P & FMC_HPC_HA11_P & FMC_HPC_HA10_P;
    manchester_diff_in_n <= FMC_HPC_HA13_N & FMC_HPC_HA12_N & FMC_HPC_HA11_N & FMC_HPC_HA10_N;

    manchester_in <= manchester_diff_in(3) & manchester_diff_in(2) & manchester_diff_in(1) & manchester_diff_in(0) & FMC_HPC_HA03_P & FMC_HPC_HA02_P & FMC_HPC_HA01_CC_P & FMC_HPC_HA00_CC_P;

    discriminator_or_in1 <= FMC_HPC_HA18_P;
    discriminator_or_in2 <= FMC_HPC_HA19_P;

    GPIO_LED <= "111111" & mmcm_sysclock200_locked & mmcm_userclock156_locked;

    ODDR_inst : ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
        INIT         => '0',         -- Initial value for Q port ('1' or '0')
        SRTYPE       => "SYNC")      -- Reset Type ("ASYNC" or "SYNC")
    port map (
        Q  => user_sma_clock_p,      -- 1-bit DDR output
        C  => clock40_mmcm,          -- 1-bit clock input
        CE => '1',                   -- 1-bit clock enable input
        D1 => '1',                   -- 1-bit data input (positive edge)
        D2 => '0',                   -- 1-bit data input (negative edge)
        R  => '0',                   -- 1-bit reset input
        S  => '0'                    -- 1-bit set input
    );
  
--    userclock156_IBUFDS_inst : IBUFDS
--    generic map (
--        DIFF_TERM => TRUE,
--        IBUF_LOW_PWR => TRUE,
--        IOSTANDARD => "LVDS_25")
--    port map (
--        O  => userclock156,
--        I  => user_clock_p,
--        IB => user_clock_n
--    );

    manchester_in_diff_buff_gen : for i in 3 downto 0 generate
        manchester_in_diff_buff : IBUFDS
        generic map (
            DIFF_TERM    => TRUE,  -- Differential Termination 
            IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
            IOSTANDARD   => "LVDS")
        port map (
            O  => manchester_diff_in(i),
            I  => manchester_diff_in_p(i),
            IB => manchester_diff_in_n(i)
        );
    end generate;

    vio_manchester_inst : vio_manchester
    port map (
        clk       => clock200_mmcm,
        probe_in0 => vio_probe
    );

    vio_hit_pattern156_inst : vio_hit_pattern
    port map (
        clk        => clock156250_mmcm,
        probe_out0 => hit_pattern156,
        probe_out1 => hit_pattern156_counter_max
    );

    vio_hit_pattern200_inst : vio_hit_pattern
    port map (
        clk        => clock200_mmcm,
        probe_out0 => hit_pattern200,
        probe_out1 => hit_pattern200_counter_max
    );

    vio_signals_assignment_logic : process(clock200_mmcm)
    begin
        if rising_edge(clock200_mmcm) then
            if decoded_data_18b_valid /= X"00" then
                vio_probe <= firmware_version &
                             bcid &
                             '0' & decoded_data_18b_valid(7) & decoded_data_18b(7) &
                             '0' & decoded_data_18b_valid(6) & decoded_data_18b(6) &
                             '0' & decoded_data_18b_valid(5) & decoded_data_18b(5) &
                             '0' & decoded_data_18b_valid(4) & decoded_data_18b(4) &
                             '0' & decoded_data_18b_valid(3) & decoded_data_18b(3) &
                             '0' & decoded_data_18b_valid(2) & decoded_data_18b(2) &
                             '0' & decoded_data_18b_valid(1) & decoded_data_18b(1) &
                             '0' & decoded_data_18b_valid(0) & decoded_data_18b(0);
            end if;
        end if;
    end process;

    ila_manchester_ch0 : ila_manchester
    port map (
        clk       => clock200_mmcm,
        probe0    => bcid,
        probe1    => manchester_6b(0),
        probe2    => debug_manchester_100b(0),
        probe3    => decoded_data_18b(0),
        probe4(0) => decoded_data_18b_valid(0),
        probe5(0) => discriminator_or_in1,
        probe6(0) => discriminator_or_in2,
        probe7    => debug_start_counter(0),
        probe8    => debug_decoded_hit_counter(0),
        probe9    => debug_fsm_error_counter(0),
        probe10   => debug_generated_hit200_counter,
        probe11   => debug_generated_hit156_counter_r2,
        probe12   => debug_manchester_stream_out(0)
    );

    ila_manchester_ch1 : ila_manchester
    port map (
        clk       => clock200_mmcm,
        probe0    => bcid,
        probe1    => manchester_6b(1),
        probe2    => debug_manchester_100b(1),
        probe3    => decoded_data_18b(1),
        probe4(0) => decoded_data_18b_valid(1),
        probe5(0) => discriminator_or_in1,
        probe6(0) => discriminator_or_in2,
        probe7    => debug_start_counter(1),
        probe8    => debug_decoded_hit_counter(1),
        probe9    => debug_fsm_error_counter(1),
        probe10   => debug_generated_hit200_counter,
        probe11   => debug_generated_hit156_counter_r2,
        probe12   => debug_manchester_stream_out(1)
    );

    ila_manchester_ch4 : ila_manchester
    port map (
        clk       => clock200_mmcm,
        probe0    => bcid,
        probe1    => manchester_6b(4),
        probe2    => debug_manchester_100b(4),
        probe3    => decoded_data_18b(4),
        probe4(0) => decoded_data_18b_valid(4),
        probe5(0) => discriminator_or_in1,
        probe6(0) => discriminator_or_in2,
        probe7    => debug_start_counter(4),
        probe8    => debug_decoded_hit_counter(4),
        probe9    => debug_fsm_error_counter(4),
        probe10   => debug_generated_hit200_counter,
        probe11   => debug_generated_hit156_counter_r2,
        probe12   => debug_manchester_stream_out(4)
    );

    ila_manchester_ch5 : ila_manchester
    port map (
        clk       => clock200_mmcm,
        probe0    => bcid,
        probe1    => manchester_6b(5),
        probe2    => debug_manchester_100b(5),
        probe3    => decoded_data_18b(5),
        probe4(0) => decoded_data_18b_valid(5),
        probe5(0) => discriminator_or_in1,
        probe6(0) => discriminator_or_in2,
        probe7    => debug_start_counter(5),
        probe8    => debug_decoded_hit_counter(5),
        probe9    => debug_fsm_error_counter(5),
        probe10   => debug_generated_hit200_counter,
        probe11   => debug_generated_hit156_counter_r2,
        probe12   => debug_manchester_stream_out(5)
    );

    mmcm_userclock156_inst : mmcm_userclock156
    port map (
        clk_out468750 => clock468750_mmcm,
        clk_out156250 => clock156250_mmcm,
        reset         => cpu_reset,
        locked        => mmcm_userclock156_locked,
        clk_in1_p     => user_clock_p,
        clk_in1_n     => user_clock_n
    );

    mmcm_sysclock200_inst : mmcm_sysclock200
    port map (
        clk_out600 => clock600_mmcm,
        clk_out200 => clock200_mmcm,
        clk_out40  => clock40_mmcm,
        reset      => cpu_reset,
        locked     => mmcm_sysclock200_locked,
        clk_in1_p  => sys_clock_p,
        clk_in1_n  => sys_clock_n
    );

    int_reset200_logic : process(clock40_mmcm) begin
        if rising_edge(clock40_mmcm) then
            mmcm_sysclock200_locked_r0 <= mmcm_sysclock200_locked;
            mmcm_sysclock200_locked_r1 <= mmcm_sysclock200_locked_r0;
            if mmcm_sysclock200_locked_r1 = '0' or sw_reset = '1' then
                int_reset200 <= '1';
            else
                int_reset200 <= '0';
            end if;
        end if;
    end process;

    int_reset156_logic : process(clock156250_mmcm) begin
        if rising_edge(clock156250_mmcm) then
            mmcm_userclock156_locked_r0 <= mmcm_userclock156_locked;
            mmcm_userclock156_locked_r1 <= mmcm_userclock156_locked_r0;
            if mmcm_userclock156_locked_r1 = '0'  or sw_reset = '1' then
                int_reset156 <= '1';
            else
                int_reset156 <= '0';
            end if;
        end if;
    end process;

    clock40_edge_logic : process(clock40_mmcm) begin
        if rising_edge(clock40_mmcm) then
            clock40_edge <= not clock40_edge;
        end if;
    end process;

    clock40_rising200_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            clock40_edge200_r <= clock40_edge;
            if clock40_edge200_r /= clock40_edge then
                clock40_rising200 <= '1';
            else
                clock40_rising200 <= '0';
            end if;
        end if;
    end process;

    bcid_counter_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if int_reset200 = '1' then
                bcid <= (others => '0');
            else
                if clock40_rising200 = '1' then
                    --3563 = X"DEB"
                    bcid <= std_logic_vector(to_unsigned(to_integer(unsigned(bcid + 1)) mod 3563, 12));
                end if;
            end if;
        end if;
    end process;

    hit_out_pattern156_gen : process(clock156250_mmcm) begin
        if rising_edge(clock156250_mmcm) then
            if int_reset156 = '1' then
                debug_generated_hit156_counter <= (others => '0');
            else
                if hit_pattern156_counter = hit_pattern156_counter_max then
                    hit_pattern156_counter <= (others => '0');
                else
                    hit_pattern156_counter <= hit_pattern156_counter + 1;
                end if;
                if hit_pattern156_counter = "0000000000" then
                    debug_generated_hit156_counter <= debug_generated_hit156_counter + 1;
                    serialiser_hit_pattern156 <= hit_pattern156(5 downto 0);
                elsif hit_pattern156_counter = "0000000001" then
                    serialiser_hit_pattern156 <= hit_pattern156(11 downto 6);
                else
                    serialiser_hit_pattern156 <= (others => '1');
                end if;
            end if;
        end if;
    end process;

    hit_out_pattern200_gen : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if int_reset200 = '1' then
                debug_generated_hit200_counter <= (others => '0');
            else
                if hit_pattern200_counter = hit_pattern200_counter_max then
                    hit_pattern200_counter <= (others => '0');
                else
                    hit_pattern200_counter <= hit_pattern200_counter + 1;
                end if;
                if hit_pattern200_counter = "0000000000" then
                    debug_generated_hit200_counter <= debug_generated_hit200_counter + 1;
                    serialiser_hit_pattern200 <= hit_pattern200(5 downto 0);
                elsif hit_pattern200_counter = "0000000001" then
                    serialiser_hit_pattern200 <= hit_pattern200(11 downto 6);
                else
                    serialiser_hit_pattern200 <= (others => '1');
                end if;
            end if;
        end if;
    end process;

    debug_generated_hit_counter_sync_to_200MHz_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            debug_generated_hit156_counter_r2 <= debug_generated_hit156_counter_r;
            debug_generated_hit156_counter_r <= debug_generated_hit156_counter;
        end if;
    end process;

    serialiser156_out_inst : serialiser_6b_out
    port map (
        data_out_from_device => serialiser_hit_pattern156,
        data_out_to_pins     => user_sma_gpio_n,
        clk_in               => clock468750_mmcm,
        clk_div_in           => clock156250_mmcm,
        io_reset             => int_reset156
    );

    serialiser200_out_inst : serialiser_6b_out
    port map (
        data_out_from_device => serialiser_hit_pattern200,
        data_out_to_pins     => user_sma_clock_n,
        clk_in               => clock600_mmcm,
        clk_div_in           => clock200_mmcm,
        io_reset             => int_reset200
    );

    deserialisers : for i in 7 downto 0 generate
        deser_inst : deserialiser
        port map (
            data_in_from_pins => manchester_in(i),
            data_in_to_device => manchester_6b(i),   -- BIT0 is the OLDEST
            bitslip           => '0',
            clk_in            => clock600_mmcm,
            clk_div_in        => clock200_mmcm,
            io_reset          => int_reset200
        );
    end generate;

    manchester_gen : for i in 7 downto 0 generate
        manchester_decoding_inst : entity work.manchester_decoding
        port map (
            clock                           => clock200_mmcm,
            reset                           => int_reset200,
            data6b_in                       => manchester_6b(i),
            data18b_out                     => decoded_data_18b(i),
            data18b_we_out                  => decoded_data_18b_valid(i),
            debug_m_100b_out                => debug_manchester_100b(i),
            debug_start_counter_out         => debug_start_counter(i),
            debug_fsm_error_counter_out     => debug_fsm_error_counter(i),
            debug_decoded_words_counter_out => debug_decoded_hit_counter(i)
        );
    end generate;

end RTL;
