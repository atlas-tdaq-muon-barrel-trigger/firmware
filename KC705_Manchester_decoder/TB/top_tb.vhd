library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;




entity top_tb is
end top_tb;



architecture Behavioral of top_tb is

    signal clock40 : std_logic;
    signal tb_clock200_p, tb_clock200_n : std_logic;
    signal tb_clock156_p, tb_clock156_n : std_logic;

    signal eta_hit : std_logic_vector(287 downto 0) := (others => '0');

    signal manchester_clock : std_logic;
    type manchester_states is (send_idle, send_error, send_data_idle, send_data);
    type manchester_states_array is array (7 downto 0) of manchester_states;
    signal manchester_state : manchester_states_array;

    type array8x19b is array(7 downto 0) of std_logic_vector(18 downto 0);
    signal tdc_data : array8x19b := (others => (others => '0'));

    type array8xinteger is array(7 downto 0) of integer;
    signal manchester_counter : array8xinteger := (others => 0);

    signal manchester_lines_p : std_logic_vector(7 downto 0);
    signal manchester_lines_n : std_logic_vector(7 downto 0);

    signal bcid_in : std_logic_vector(11 downto 0);
    
    signal discriminator_or : std_logic;

begin

--  manchester transmitter clock
    clock290_gen : process
    begin
        for i in 0 to 14 loop
            manchester_clock <= '1';
            wait for 1.724 ns;
            manchester_clock <= '0';
            wait for 1.724 ns;
        end loop;
        wait for 600 ps;
    end process;
--    clock240_gen : process
--    begin
--        for i in 0 to 14 loop
--            manchester_clock <= '1';
--            wait for 2.083 ns;
--            manchester_clock <= '0';
--            wait for 2.083 ns;
--        end loop;
--        wait for 720 ps;
--    end process;

--  LHC clock frequency 40.079 MHz
    clock40_gen : process
    begin
        clock40 <= '0';
        wait for 12.475 ns;
        clock40 <= '1';
        wait for 12.475 ns;
    end process;

    tb_clock200_gen : process
    begin
        tb_clock200_p <= '1';
        tb_clock200_n <= '0';
        wait for 2.5 ns;
        tb_clock200_p <= '0';
        tb_clock200_n <= '1';
        wait for 2.5 ns;
    end process;

    tb_clock156_gen : process
    begin
        tb_clock156_p <= '1';
        tb_clock156_n <= '0';
        wait for 3.2 ns;
        tb_clock156_p <= '0';
        tb_clock156_n <= '1';
        wait for 3.2 ns;
    end process;

    top_inst : entity work.top
    port map (
        sys_clock_p          => tb_clock200_p,
        sys_clock_n          => tb_clock200_n,
        user_clock_p         => tb_clock156_p,
        user_clock_n         => tb_clock156_n,
        user_sma_clock_p     => open,
        user_sma_clock_n     => open,
        FMC_HPC_HA00_CC_P    => manchester_lines_p(0),
        FMC_HPC_HA01_CC_P    => manchester_lines_p(1),
        FMC_HPC_HA02_P       => manchester_lines_p(2),
        FMC_HPC_HA03_P       => manchester_lines_p(3),
        FMC_HPC_HA10_P       => manchester_lines_p(4),
        FMC_HPC_HA10_N       => manchester_lines_n(4),
        FMC_HPC_HA11_P       => manchester_lines_p(5),
        FMC_HPC_HA11_N       => manchester_lines_n(5),
        FMC_HPC_HA12_P       => manchester_lines_p(6),
        FMC_HPC_HA12_N       => manchester_lines_n(6),
        FMC_HPC_HA13_P       => manchester_lines_p(7),
        FMC_HPC_HA13_N       => manchester_lines_n(7),
        FMC_HPC_HA18_P       => discriminator_or,
        FMC_HPC_HA19_P       => discriminator_or,
        GPIO_LED             => open,
        cpu_reset            => '0',
        sw_reset             => '0',
        bcid_out             => bcid_in
    );

    manchester_lines_n <= not manchester_lines_p;

    manchester_lines_gen : for i in 7 downto 0 generate
        manchester_lines_p(i) <= '0'              when manchester_state(i) = send_error else
                                 manchester_clock when manchester_state(i) = send_idle  else
                                 manchester_clock when manchester_state(i) = send_data_idle  else
                                 tdc_data(i)(manchester_counter(i)) XOR manchester_clock;
        manchester_fsm : process(manchester_clock) begin
            if rising_edge(manchester_clock) then
                case manchester_state(i) is
                    when send_idle =>
                        --tdc_data(i) <= "000" & X"0000";
                        manchester_counter(i) <= 17;
                        if eta_hit(i) = '1' then
                            manchester_state(i) <= send_error;
                        end if;
                    when send_error =>
                        --tdc_data(i) <= '0' & "00" & X"32" & X"20";
                        --tdc_data(i) <= tdc_data(i) + 1;
                        manchester_state(i) <= send_data_idle;
                    when send_data_idle =>
                        --tdc_data(i) <= '0' & "00" & X"32" & X"20";
                        tdc_data(i) <= tdc_data(i) + 1;
                        manchester_state(i) <= send_data;
                    when send_data =>
                        if manchester_counter(i) /= 0 then
                            manchester_counter(i) <= manchester_counter(i) - 1;
                        else
                            manchester_state(i) <= send_idle;
                        end if;
                    when others =>
                end case;
            end if;
        end process;
    end generate;

    periodic_hit_generation : process
    begin
        eta_hit <= (others => '0');
        wait for 20 us;
        for i in 0 to 1000 loop
            eta_hit <= (others => '1');
            wait for 20 ns;
            eta_hit <= (others => '0');
            wait for 1 us; -- 1 MHz
        end loop;
        wait;
    end process;

--    file_hit_read_logic: process
--        file simulation_hit_file: text;
--        file generated_hit_file: text open write_mode is "/home/l0mu/KC705_Manchester_decoder/Data/bi_generated_hits_out.txt";
--        variable file_status : FILE_OPEN_STATUS;
--        variable file_status2 : FILE_OPEN_STATUS;
--        variable hits_line : line;
--        variable data_line : line;
--        variable hit_time : real;
--        variable left_right_eta : integer;
--        variable l0_l1_l2 : integer;
--        variable hit_index : integer;
--        variable mu_match : integer;
--        constant space : string := " ";
--        variable prev_hit_time : real := 0.0;
--    begin
--        --
--        wait for 3 us;
--        --
--        file_open(file_status, simulation_hit_file, "/home/l0mu/KC705_Manchester_decoder/Data/outfile_v2_BIL_6BC_withmu_eta3_addsides_sorted.txt", read_mode);
--        assert file_status = open_ok report FILE_OPEN_STATUS'IMAGE(file_status) & " while opening file!**" severity failure;
--        while not endfile(simulation_hit_file) loop
--            readline(simulation_hit_file, data_line);
--            read(data_line, hit_time);
--            read(data_line, left_right_eta);
--            read(data_line, l0_l1_l2);
--            read(data_line, hit_index);
--            read(data_line, mu_match);
--            wait for (hit_time - prev_hit_time) * 1 ns;
--            prev_hit_time := hit_time;
--            if hit_index + 48 * l0_l1_l2 + 144 * left_right_eta < 279 then
--                eta_hit(hit_index + 48 * l0_l1_l2 + 144 * left_right_eta) <= '1', '0' after 20 ns;
--                discriminator_or <= '1', '0' after 20 ns;
--            else
--            --    assert FALSE report "index number out of range!" severity warning;
--            end if;
--            hwrite(hits_line, bcid_in); -- current bcid
--            write(hits_line, space);
--            write(hits_line, hit_index + 48 * l0_l1_l2 + 144 * left_right_eta);
--            write(hits_line, space);
--            write(hits_line, mu_match);
--            writeline(generated_hit_file, hits_line);
--        end loop;
--        file_close(simulation_hit_file);
--        assert false report "END OF SIMULATION" severity failure;
--        wait;
--    end process;

end Behavioral;