% Template class for TDAQDoc document based On ATLAS note templates:
% 2018-08-20 created for ATLAS TDAQ Phase-II TDAQ Specification Document Template (FL: <Francesco.Lanni@cern.ch>)
% depends On TDAQGenDoc.cls 
% ---
\ProvidesClass{TDAQSpecDoc}[2018/08/20 version 0.1 ATLAS TDAQ HL-LHC Upgrade Project Specification Document Template]
\NeedsTeXFormat{LaTeX2e}

\LoadClass{TDAQGenDoc}

\let\if@CoverPage\iffalse
\let\if@Draft\iftrue
\let\if@Note\iftrue

\let\@DocPreTitle\relax
\def\@DocPreTitle{%
\gls{TDAQ} \PhaseTwo Upgrade Project Specification Document
}
\let\@ShortDocTitle\relax
\def\@ShortDocTitle{%
 Specification Document Template
}
\let\@DocTitle\relax
\def\@DocTitle{%
\gls{TDAQ} \PhaseTwo Upgrade Project Specification Document Template
}

\AtBeginDocument{
 \setglossarysection{subparagraph}
 \renewcommand{\glossarysection}[2][]{}

 \DeclareRobustCommand{\tdaqPrintRelevantDocuments}{%
  \begin{refsection}
  \DeclareFieldFormat{labelnumberwidth}{}
  \nocite{*}
  \printbibliography[omitnumbers=true,heading=none,keyword=relevant] 
  \end{refsection}
  }
} 

\endinput

