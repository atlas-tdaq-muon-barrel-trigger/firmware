# lpGBT - FPGA project

This repository contains the projects to emulate the communication between the lpGBT of the DCT boards and the Sector Logic boards, for the Phase-II Upgrade of the barrel Muon Spectrometer of the ATLAS detector.

### Contents:
* `KC705_test` contains two projects:
  + `sector_logic_emulator` emulates the encoding/decoding logic of the Sector Logic FPGA  needed to communicate with 1 DCT
  + `lpgbt_emulator` is an emulator of the lpGBT, an ASIC  installed on the DCT needed to transmit RPC data to the SL board and to receive control data from the SL board.  
 
The two projects have been adapted to work with a KC705 evaluation board of the Xilinx Kintex-7 family.  

### Project creation
To create the projects from scratch with Vivado, write the following lines on the TCL console:  

##### `sector_logic_emulator`
	cd KC705_test/sector_logic_emulator/
	source create_SL_project.tcl

##### `lpgbt_emulator`
	cd KC705_test/lpgbt_emulator/
	source create_lpgbt_project.tcl

---------------------------------------------------------
Authors: Riccardo Vari, Federico Morodei
