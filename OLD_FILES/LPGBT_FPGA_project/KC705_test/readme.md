## lpGBT FPGA projects for KC705 evaluation boards

*  `sector_logic_emulator` The following folders or files in the design sources directory were taken from [https://gitlab.cern.ch/gbt-fpga/lpgbt-fpga](https://gitlab.cern.ch/gbt-fpga/lpgbt-fpga), the other ones were created by the authors of this repository:
   + `downlink` folder
   + `uplink` folder
   + `lpgbtfpga_package.vhd`
   + `lpgbtfpga_uplink.vhd`
   + `lpgbtfpga_downlink.vhd`
   The `lpgbtfpga_downlink.vhd` file was modified to obtained a downlink bandwidth of 2.56 Gbps:
       - line 24: 3 -> 1
       - line 25: 8 -> 2

*  `lpgbt_emulator` All the files in the folder `codes/design_sources/lpgbt-emul-master` were taken from an emulator of the lpGBT given by CERN, the other ones were created by the authors of this repository. The following files taken from CERN were modified:
   + `lpgbtemul_top.vhd`:
     - line 73: (15 downto 0) -> (3 downto 0)
     - line 199: cnter = 4 -> cnter = 1
     - line 208: 8 -> 2
     - line 209: x"F00F" -> "1001"
     - line 236: `dat_downLinkWord_fromMgt_s(24) & dat_downLinkWord_fromMgt_s(25) & dat_downLinkWord_fromMgt_s(26) & dat_downLinkWord_fromMgt_s(27) & dat_downLinkWord_fromMgt_s(16) & dat_downLinkWord_fromMgt_s(17) & dat_downLinkWord_fromMgt_s(18) & dat_downLinkWord_fromMgt_s(19) & dat_downLinkWord_fromMgt_s(8) & dat_downLinkWord_fromMgt_s(9) & dat_downLinkWord_fromMgt_s(10) & dat_downLinkWord_fromMgt_s(11) & dat_downLinkWord_fromMgt_s(3) & dat_downLinkWord_fromMgt_s(2) & dat_downLinkWord_fromMgt_s(1) & dat_downLinkWord_fromMgt_s(0);` -> `dat_downLinkWord_fromMgt_s(0) & dat_downLinkWord_fromMgt_s(2) & dat_downLinkWord_fromMgt_s(4) & dat_downLinkWord_fromMgt_s(6);`
     - line 247: 8 -> 2
     - line 250: 2 -> 0
   + `rxgearbox.vhd`:
     - line 26: 2 -> 0
-------------------------------------------------------
Scheme of the main signals and clocks used by downlink and uplink in the Sector Logic to communicate with the lpGBT of the DCT.

![](SL_MGT.png)


---------------------------------------------------------
Hardware test setup.

![](KC705_test_setup.png)


---------------------------------------------------------
Authors: Riccardo Vari, Federico Morodei
