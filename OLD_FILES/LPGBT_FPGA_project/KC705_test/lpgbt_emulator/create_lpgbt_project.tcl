
create_project lpgbt_emul lpgbt_emul_vivado -part xc7k325tffg900-2
set_property board_part xilinx.com:kc705:part0:1.6 [current_project]

add_files {
    codes/ip/clk_160_generator/clk_160_generator.xci
    codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci
    codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci}

add_files {
    codes/design_sources/lpgbt_emul_gt_gt_usrclk_source.vhd
    codes/design_sources/lpgbt_emul_gt_exdes.vhd
    codes/design_sources/lpgbt_emul_gt_clock_module.vhd
    codes/design_sources/lpgbt-emul-master/scrambler58bitOrder58.v
    codes/design_sources/lpgbt-emul-master/rs_decoder_N7K5.v
    codes/design_sources/lpgbt-emul-master/rs_encoder_N15K13.v
    codes/design_sources/lpgbt-emul-master/upLinkTxDataPath.v
    codes/design_sources/lpgbt-emul-master/downLinkDeinterleaver.v
    codes/design_sources/lpgbt-emul-master/upLinkDataSelect.v
    codes/design_sources/lpgbt-emul-master/upLinkInterleaver.v
    codes/design_sources/lpgbt-emul-master/lpgbtemul_top.vhd
    codes/design_sources/lpgbt-emul-master/scrambler51bitOrder49.v
    codes/design_sources/lpgbt-emul-master/txgearbox.vhd
    codes/design_sources/lpgbt-emul-master/scrambler53bitOrder49.v
    codes/design_sources/lpgbt-emul-master/gf_multBy3_5.v
    codes/design_sources/lpgbt-emul-master/gf_multBy3_4.v
    codes/design_sources/lpgbt-emul-master/rs_encoder_N31K29.v
    codes/design_sources/lpgbt-emul-master/upLinkFECEncoder.v
    codes/design_sources/lpgbt-emul-master/upLinkScrambler.v
    codes/design_sources/lpgbt-emul-master/descrambler36bitOrder36.v
    codes/design_sources/lpgbt-emul-master/gf_mult_3.v
    codes/design_sources/lpgbt-emul-master/downLinkFECDecoder.v
    codes/design_sources/lpgbt-emul-master/gf_inv_3.v
    codes/design_sources/lpgbt-emul-master/rxgearbox.vhd
    codes/design_sources/lpgbt-emul-master/mgt_frameAligner_pv.vhd
    codes/design_sources/lpgbt-emul-master/gf_multBy2_5.v
    codes/design_sources/lpgbt-emul-master/gf_multBy2_4.v
    codes/design_sources/lpgbt-emul-master/gf_multBy2_3.v
    codes/design_sources/lpgbt-emul-master/gf_log_3.v
    codes/design_sources/lpgbt-emul-master/scrambler60bitOrder58.v
    codes/design_sources/lpgbt-emul-master/downLinkRxDataPath.v
    codes/design_sources/lpgbt-emul-master/gf_add_5.v
    codes/design_sources/lpgbt-emul-master/gf_add_4.v
    codes/design_sources/lpgbt-emul-master/gf_add_3.v
    codes/design_sources/lpgbt_emul_gt_support.vhd
    codes/design_sources/lpgbt_emul_gt_common.vhd
    codes/design_sources/lpgbt_emul_gt_common_reset.vhd}

update_compile_order -fileset sources_1

set_property SOURCE_SET sources_1 [get_filesets sim_1]

add_files -fileset sim_1 {
    codes/simulation/sector_logic_emulator/lpgbt_fpga_gt_gt_usrclk_source.vhd
    codes/simulation/sector_logic_emulator/lpgbtfpga_downlink.vhd
    codes/simulation/sim_reset_gt_model.vhd
    codes/simulation/sector_logic_emulator/lpgbt_fpga_gt_exdes.vhd
    codes/simulation/sector_logic_emulator/lpgbtfpga_package.vhd
    codes/simulation/sector_logic_emulator/lpgbt_fpga_gt_support.vhd
    codes/simulation/sector_logic_emulator/uplink/lpgbtfpga_deinterleaver.vhd
    codes/simulation/sector_logic_emulator/uplink/lpgbtfpga_rxgearbox.vhd
    codes/simulation/sector_logic_emulator/uplink/descrambler_58bitOrder58.vhd
    codes/simulation/sector_logic_emulator/uplink/descrambler_51bitOrder49.vhd
    codes/simulation/sector_logic_emulator/uplink/fec_rsDecoderN31K29.vhd
    codes/simulation/sector_logic_emulator/uplink/fec_rsDecoderN15K13.vhd
    codes/simulation/sector_logic_emulator/uplink/lpgbtfpga_descrambler.vhd
    codes/simulation/sector_logic_emulator/uplink/lpgbtfpga_framealigner.vhd
    codes/simulation/sector_logic_emulator/uplink/lpgbtfpga_decoder.vhd
    codes/simulation/sector_logic_emulator/uplink/descrambler_53bitOrder49.vhd
    codes/simulation/sector_logic_emulator/uplink/descrambler_60bitOrder58.vhd
    codes/simulation/sector_logic_emulator/lpgbt_fpga_gt_clock_module.vhd
    codes/simulation/sector_logic_emulator/lpgbtfpga_uplink.vhd
    codes/simulation/sector_logic_emulator/lpgbt_fpga_gt_common_reset.vhd
    codes/simulation/sector_logic_emulator/downlink/lpgbtfpga_scrambler.vhd
    codes/simulation/sector_logic_emulator/downlink/rs_encoder_N7K5.vhd
    codes/simulation/sector_logic_emulator/downlink/lpgbtfpga_encoder.vhd
    codes/simulation/sector_logic_emulator/downlink/lpgbtfpga_txgearbox.vhd
    codes/simulation/sector_logic_emulator/downlink/lpgbtfpga_interleaver.vhd
    codes/simulation/lpgbt_fpga_gt_tb.vhd
    codes/simulation/sector_logic_emulator/lpgbt_fpga_gt_common.vhd}

update_compile_order -fileset sim_1

add_files -fileset constrs_1 -norecurse  codes/constrs/lpgbt_fpga_gt_exdes.xdc

generate_target all [get_files   codes/ip/clk_160_generator/clk_160_generator.xci]
catch { config_ip_cache -export [get_ips -all clk_160_generator] }
export_ip_user_files -of_objects [get_files  codes/ip/clk_160_generator/clk_160_generator.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1]  codes/ip/clk_160_generator/clk_160_generator.xci]
export_simulation -of_objects [get_files  codes/ip/clk_160_generator/clk_160_generator.xci] -directory  lpgbt_emul_vivado/lpgbt_emul.ip_user_files/sim_scripts -ip_user_files_dir  lpgbt_emul_vivado/lpgbt_emul.ip_user_files -ipstatic_source_dir  lpgbt_emul_vivado/lpgbt_emul.ip_user_files/ipstatic -lib_map_path [list {modelsim= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/modelsim} {questa= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/questa} {ies= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/ies} {xcelium= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/xcelium} {vcs= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/vcs} {riviera= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

generate_target all [get_files   codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci]
catch { config_ip_cache -export [get_ips -all lpgbt_emul_gt] }
export_ip_user_files -of_objects [get_files  codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1]  codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci]
export_simulation -of_objects [get_files  codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci] -directory  lpgbt_emul_vivado/lpgbt_emul.ip_user_files/sim_scripts -ip_user_files_dir  lpgbt_emul_vivado/lpgbt_emul.ip_user_files -ipstatic_source_dir  lpgbt_emul_vivado/lpgbt_emul.ip_user_files/ipstatic -lib_map_path [list {modelsim= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/modelsim} {questa= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/questa} {ies= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/ies} {xcelium= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/xcelium} {vcs= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/vcs} {riviera= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

generate_target all [get_files   codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci]
catch { config_ip_cache -export [get_ips -all lpgbt_fpga_gt] }
export_ip_user_files -of_objects [get_files  codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1]  codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci]
export_simulation -of_objects [get_files  codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci] -directory  lpgbt_emul_vivado/lpgbt_emul.ip_user_files/sim_scripts -ip_user_files_dir  lpgbt_emul_vivado/lpgbt_emul.ip_user_files -ipstatic_source_dir  lpgbt_emul_vivado/lpgbt_emul.ip_user_files/ipstatic -lib_map_path [list {modelsim= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/modelsim} {questa= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/questa} {ies= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/ies} {xcelium= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/xcelium} {vcs= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/vcs} {riviera= lpgbt_emul_vivado/lpgbt_emul.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet
