library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;



entity lpgbt_fpga_gt_exdes is
    generic
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE"; -- simulation setting for GT SecureIP model
        STABLE_CLOCK_PERIOD                     : integer   := 6
    );
    port
    (
        Q2_CLK1_GTREFCLK_PAD_N_IN               : in   std_logic;
        Q2_CLK1_GTREFCLK_PAD_P_IN               : in   std_logic;
        DRP_CLK_IN_P                            : in   std_logic;
        DRP_CLK_IN_N                            : in   std_logic;
        RXN_IN                                  : in   std_logic;
        RXP_IN                                  : in   std_logic;
        TXN_OUT                                 : out  std_logic;
        TXP_OUT                                 : out  std_logic;
        USER_SMA_CLK_P                          : out  std_logic;
        USER_SMA_CLK_N                          : out  std_logic;
        USER_SMA_GPIO_P                         : out  std_logic;
        USER_SMA_GPIO_N                         : out  std_logic;
        GPIO_LED_0                              : out  std_logic;  --for test purpose (light if clock works)
        GPIO_LED_1                              : out  std_logic   -- for test purpose (light if counter arrives from lpgbt)
    );


end lpgbt_fpga_gt_exdes;


    
architecture RTL of lpgbt_fpga_gt_exdes is

--    attribute DowngradeIPIdentifiedWarnings: string;
--    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

--    attribute CORE_GENERATION_INFO : string;
--    attribute CORE_GENERATION_INFO of RTL : architecture is "lpgbt_fpga_gt,gtwizard_v3_6_13,{protocol_file=Start_from_scratch}";

    component clk_160_generator is
        port(
            reset           : in std_logic;
            clk_in1         : in std_logic;
            locked          : out std_logic;
            clk_160_out1    : out std_logic;
            clk_160_out2    : out std_logic
            );
    end component clk_160_generator;
    
    constant DLY : time := 1 ns;

    attribute ASYNC_REG                        : string;

    signal gt0_txfsmresetdone_i            : std_logic;
    signal gt0_rxfsmresetdone_i            : std_logic;
    signal gt0_txfsmresetdone_r            : std_logic;
    signal gt0_txfsmresetdone_r2           : std_logic;
    attribute ASYNC_REG of gt0_txfsmresetdone_r     : signal is "TRUE";
    attribute ASYNC_REG of gt0_txfsmresetdone_r2     : signal is "TRUE";
    signal gt0_rxresetdone_r               : std_logic;
    signal gt0_rxresetdone_r2              : std_logic;
    signal gt0_rxresetdone_r3              : std_logic;
    attribute ASYNC_REG of gt0_rxresetdone_r     : signal is "TRUE";
    attribute ASYNC_REG of gt0_rxresetdone_r2     : signal is "TRUE";
    attribute ASYNC_REG of gt0_rxresetdone_r3     : signal is "TRUE";

    signal gt0_rxdata_i                    : std_logic_vector(31 downto 0);
    signal gt0_rxslide_i                   : std_logic;
    signal gt0_rxresetdone_i               : std_logic;
    signal gt0_txdata_i                    : std_logic_vector(31 downto 0);
    signal gt0_txresetdone_i               : std_logic;
    signal gt0_tx_system_reset_c           : std_logic;
    signal gt0_rx_system_reset_c           : std_logic;
    signal drpclk_in_i                     : std_logic;
    signal DRPCLK_IN                       : std_logic;
    signal gt0_txusrclk2_i                 : std_logic; 
    signal gt0_rxusrclk2_i                 : std_logic;
    
    signal gt0_error_count_i               : std_logic_vector(7 downto 0);
    signal gt0_rxdata_i_r                    : std_logic_vector(31 downto 0);
    signal gt0_rx_data_valid               : std_logic;
    signal rxslide_counter : std_logic_vector(5 downto 0);
    
    signal downlink_clock_en          : std_logic;
    signal downlink_user_data         : std_logic_vector(31 downto 0);
    signal downlink_ec_data           : std_logic_vector(1 downto 0);
    signal downlink_ic_data           : std_logic_vector(1 downto 0);
    signal downlink_ready             : std_logic;
    signal downlink_user_data_tmp     : std_logic_vector(31 downto 0);
    
    signal uplink_user_data           : std_logic_vector(229 downto 0);
    signal uplink_ic_data             : std_logic_vector(1 downto 0);
    signal uplink_ec_data             : std_logic_vector(1 downto 0); 
    signal uplink_dataCorrected       : std_logic_vector(229 downto 0);
    signal uplink_IcCorrected         : std_logic_vector(1 downto 0);
    signal uplink_EcCorrected         : std_logic_vector(1 downto 0); 
    signal uplink_rdy_i               : std_logic;
    signal uplink_clk_en_i            : std_logic;
    
    signal user_SMA_clk_i             : std_logic;
    signal user_SMA_GPIO_i            : std_logic;
    
    signal led_0_counter_i       : std_logic_vector(27 downto 0) := (others=>'0'); -- for test purpose
    signal led_0_i               : std_logic := '0';  -- for test purpose
    signal led_1_counter_i       : std_logic_vector(27 downto 0) := (others=>'0'); -- for test purpose
    signal led_1_i               : std_logic := '0';  -- for test purpose
    signal uplink_user_data_r    : std_logic_vector(229 downto 0); --for test purpose 
    
    
    
    
    
begin

    gt0_tx_system_reset_c <= not gt0_txfsmresetdone_r2;
    gt0_rx_system_reset_c <= not gt0_rxresetdone_r3;

    IBUFDS_DRP_CLK : IBUFDS
    port map
    (
        I  => DRP_CLK_IN_P,
        IB => DRP_CLK_IN_N,
        O  => DRPCLK_IN
    );
    
    DRP_CLK_BUFG : BUFG 
    port map 
    (
        I    => DRPCLK_IN,
        O    => drpclk_in_i 
    );
    
    
    
    
    clk_160_generator_inst: clk_160_generator
        port map(
            reset           => '0',
            clk_in1         => drpclk_in_i,
            locked          => open,
            clk_160_out1    => user_SMA_clk_i,
            clk_160_out2    => user_SMA_GPIO_i
        ); 
        
      OBUFDS_USER_SMA_CLK : OBUFDS
    port map
    (
        I  => user_SMA_clk_i,
        O => USER_SMA_CLK_P,
        OB  => USER_SMA_CLK_N
    );
    
      OBUFDS_USER_GPIO : OBUFDS
    port map
    (
        I  => user_SMA_GPIO_i,
        O => USER_SMA_GPIO_P,
        OB  => USER_SMA_GPIO_N
    );
      
      
    
    lpgbt_fpga_gt_support_i : entity work.lpgbt_fpga_gt_support
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     => EXAMPLE_SIM_GTRESET_SPEEDUP,
        STABLE_CLOCK_PERIOD             => STABLE_CLOCK_PERIOD
    )
    port map
    (
        SOFT_RESET_TX_IN                => '0',
        SOFT_RESET_RX_IN                => '0',
        DONT_RESET_ON_DATA_ERROR_IN     => '0',
        Q2_CLK1_GTREFCLK_PAD_N_IN       => Q2_CLK1_GTREFCLK_PAD_N_IN,
        Q2_CLK1_GTREFCLK_PAD_P_IN       => Q2_CLK1_GTREFCLK_PAD_P_IN,
        GT0_RX_MMCM_LOCK_OUT            => open,
        GT0_TX_FSM_RESET_DONE_OUT       => gt0_txfsmresetdone_i,
        GT0_RX_FSM_RESET_DONE_OUT       => gt0_rxfsmresetdone_i,
        GT0_DATA_VALID_IN               => gt0_rx_data_valid,
        GT0_TXUSRCLK_OUT                => open,
        GT0_TXUSRCLK2_OUT               => gt0_txusrclk2_i,
        GT0_RXUSRCLK_OUT                => open,
        GT0_RXUSRCLK2_OUT               => gt0_rxusrclk2_i,
        gt0_drpaddr_in                  => (others => '0'),
        gt0_drpdi_in                    => (others => '0'),
        gt0_drpdo_out                   => open,
        gt0_drpen_in                    => '0',
        gt0_drprdy_out                  => open,
        gt0_drpwe_in                    => '0',
        gt0_dmonitorout_out             => open,
        gt0_eyescanreset_in             => '0',
        gt0_rxuserrdy_in                => '1',
        gt0_eyescandataerror_out        => open,
        gt0_eyescantrigger_in           => '0',
        gt0_rxdata_out                  => gt0_rxdata_i,
        gt0_gtxrxp_in                   => RXP_IN,
        gt0_gtxrxn_in                   => RXN_IN,
        gt0_rxcommadet_out              => open,
        gt0_rxdfelpmreset_in            => '0',
        gt0_rxmonitorout_out            => open,
        gt0_rxmonitorsel_in             => "00",
        gt0_rxoutclkfabric_out          => open,
        gt0_gtrxreset_in                => '0',
        gt0_rxpmareset_in               => '0',
        gt0_rxpolarity_in               => '0',
        gt0_rxslide_in                  => gt0_rxslide_i,
        gt0_rxresetdone_out             => gt0_rxresetdone_i,
        gt0_gttxreset_in                => '0',
        gt0_txuserrdy_in                => '1',
        gt0_txdata_in                   => gt0_txdata_i,
        gt0_gtxtxn_out                  => TXN_OUT,
        gt0_gtxtxp_out                  => TXP_OUT,
        gt0_txoutclkfabric_out          => open,
        gt0_txoutclkpcs_out             => open,
        gt0_txresetdone_out             => gt0_txresetdone_i,
        gt0_txpolarity_in               => '0',
        GT0_QPLLLOCK_OUT                => open,
        GT0_QPLLREFCLKLOST_OUT          => open,
        GT0_QPLLOUTCLK_OUT              => open,
        GT0_QPLLOUTREFCLK_OUT           => open,
        sysclk_in                       => user_SMA_clk_i
    );

    rx_reset : process(gt0_rxusrclk2_i, gt0_rxresetdone_i)
    begin
        if (gt0_rxresetdone_i = '0') then
            gt0_rxresetdone_r  <= '0'   after DLY;
            gt0_rxresetdone_r2 <= '0'   after DLY;
            gt0_rxresetdone_r3 <= '0'   after DLY;
        elsif rising_edge(gt0_rxusrclk2_i) then
            gt0_rxresetdone_r  <= gt0_rxresetdone_i  after DLY;
            gt0_rxresetdone_r2 <= gt0_rxresetdone_r  after DLY;
            gt0_rxresetdone_r3 <= gt0_rxresetdone_r2 after DLY;
        end if;
    end process;
    
    tx_reset : process(gt0_txusrclk2_i, gt0_txfsmresetdone_i)
    begin
        if (gt0_txfsmresetdone_i = '0') then
            gt0_txfsmresetdone_r  <= '0'   after DLY;
            gt0_txfsmresetdone_r2 <= '0'   after DLY;
        elsif rising_edge(gt0_txusrclk2_i) then
            gt0_txfsmresetdone_r  <= gt0_txfsmresetdone_i after DLY;
            gt0_txfsmresetdone_r2 <= gt0_txfsmresetdone_r after DLY;
        end if;
    end process;

    rxdata_valid : process(gt0_rxusrclk2_i)
    begin
        if rising_edge(gt0_rxusrclk2_i) then
            if gt0_rx_system_reset_c = '1' then
                gt0_rx_data_valid <= '0';
            else
                gt0_rx_data_valid <= '1';
                
            end if;
        end if;
    end process;
    
    
    -- for test purpose
    led_test : process(user_SMA_clk_i) -- 160 MHz clock 
    begin
        if rising_edge(user_SMA_clk_i) then
            if led_0_counter_i = x"9896800" then   -- after 1 s
                led_0_counter_i <= (others => '0');
                led_0_i <= not led_0_i;
            else
             led_0_counter_i <= led_0_counter_i + 1;
            end if;
        end if;
    end process;
    
    GPIO_led_0 <= led_0_i;
    GPIO_LED_1 <= led_1_i;
    
    check_downlink : process(gt0_rxusrclk2_i) -- 320 MHz clock
    begin
        if rising_edge(gt0_rxusrclk2_i) then
            uplink_user_data_r <= uplink_user_data;
            if uplink_user_data = uplink_user_data_r + 1 then
                if led_1_counter_i = x"1312d000" then   -- after 1 s 
                    led_1_counter_i <= (others => '0'); 
                    led_1_i <= not led_1_i; 
                else
                    led_1_counter_i <= led_1_counter_i + 1;                
                end if; 
            end if;
        end if;
    end process;
    -- end of processes for test purpose
    
--    rxdata_check : process(gt0_rxusrclk2_i)
--    begin
--        if rising_edge(gt0_rxusrclk2_i) then
--            if gt0_rx_system_reset_c = '1' then
--                gt0_error_count_i <= (others => '0');
--                gt0_rxslide_i <= '0';
--                gt0_rx_data_valid <= '0';
--                rxslide_counter <= (others => '0');
--            else
--                gt0_rx_data_valid <= '1';
--                gt0_rxdata_i_r <= gt0_rxdata_i;
--                if gt0_rxfsmresetdone_i = '1' then
--                    if gt0_rxdata_i = gt0_rxdata_i_r + 1 then
--                        gt0_rxslide_i <= '0';
--                        rxslide_counter <= (others => '0');
--                    else
--                        rxslide_counter <= rxslide_counter + 1;
--                        if rxslide_counter = "000000" then
--                            gt0_rxslide_i <= '1';
--                            gt0_error_count_i <= gt0_error_count_i + 1;
--                        else
--                            gt0_rxslide_i <= '0';
--                        end if;
--                    end if;
--                end if;
--            end if;
--        end if;
--    end process;


downlink_data_gen : process(gt0_txusrclk2_i)
    begin
        if rising_edge(gt0_txusrclk2_i) then
            if gt0_tx_system_reset_c = '1' then
                downlink_user_data <= (others => '0');
                downlink_ec_data <= (others => '0');
                downlink_ic_data <= (others => '0');
                downlink_clock_en <= '0';
                downlink_user_data_tmp <= (others => '0');
            else
                --if downlink_ready = '1' then
                    downlink_clock_en <= not downlink_clock_en;
                    if downlink_clock_en = '0' then
--                        if downlink_user_data(5 downto 0) = "111111" then
--                            downlink_user_data <= X"F00FF00F";
--                        else
                            downlink_user_data_tmp <= downlink_user_data_tmp + 1;
                            downlink_user_data <= downlink_user_data_tmp;
--                       end if;
                        downlink_ec_data <= downlink_ec_data + 1;
                        downlink_ic_data <= downlink_ic_data + 1;
                        
--                        downlink_user_data <= X"F00F0000";
--                        downlink_ec_data <= "10";
--                        downlink_ic_data <= "10";
                    end if;
                --end if;
            end if;
        end if;
    end process;

    downlink_inst : entity work.lpgbtfpga_downlink
    generic map (
        c_multicyleDelay => 1,
        c_clockRatio     => 2,
        c_outputWidth    => 32
    )
    port map (
        clk_i                 => gt0_txusrclk2_i,
        clkEn_i               => downlink_clock_en,
        rst_n_i               => gt0_txfsmresetdone_r2, -- active low reset
        userData_i            => downlink_user_data,
        ECData_i              => downlink_ec_data,
        ICData_i              => downlink_ic_data,
        mgt_word_o            => gt0_txdata_i,
        interleaverBypass_i   => '0',
        encoderBypass_i       => '0',
        scramblerBypass_i     => '0',
        rdy_o                 => downlink_ready
    );
    
    
    uplink_inst : entity work.lpgbtfpga_uplink 
   generic map(
        -- General configuration
        DATARATE                        => 2,                             --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             => 1,                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                => 3,                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    => 8,                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  => 32,                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            => 5,                                             --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       => 64,                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            => 30,                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                => 1,                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               => 40                                       --! Number of clock cycle required before being back in a stable state
   )
   port map (
        -- Clock and reset
        uplinkClk_i                     => gt0_rxusrclk2_i,                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                => uplink_clk_en_i,                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   => gt0_rxresetdone_i,                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      => gt0_rxdata_i,  --! Input frame coming from the MGT

        -- Data
        userData_o                      => uplink_user_data,              --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        => uplink_ec_data,                  --! EC field value received from the LpGBT
        IcData_o                        => uplink_ic_data,                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             => '0',                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              => '0',                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               => '0',                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               => gt0_rxslide_i,                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 => uplink_dataCorrected,                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   => uplink_IcCorrected,                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   => uplink_EcCorrected,                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           => uplink_rdy_i,                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              => open                                       --! Number of bit slip is even (required only for advanced applications)

   );

    
    
   
end RTL;