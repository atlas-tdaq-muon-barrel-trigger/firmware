library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;



entity lpgbt_emul_gt_exdes is
    generic
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE"; -- simulation setting for GT SecureIP model
        STABLE_CLOCK_PERIOD                     : integer   := 6
    );
    port
    (
        Q2_CLK1_GTREFCLK_PAD_N_IN               : in   std_logic;
        Q2_CLK1_GTREFCLK_PAD_P_IN               : in   std_logic;
        DRP_CLK_IN_P                            : in   std_logic;
        DRP_CLK_IN_N                            : in   std_logic;
        RXN_IN                                  : in   std_logic;
        RXP_IN                                  : in   std_logic;
        TXN_OUT                                 : out  std_logic;
        TXP_OUT                                 : out  std_logic
    
    );


end lpgbt_emul_gt_exdes;


    
architecture RTL of lpgbt_emul_gt_exdes is

--    attribute DowngradeIPIdentifiedWarnings: string;
--    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

--    attribute CORE_GENERATION_INFO : string;
--    attribute CORE_GENERATION_INFO of RTL : architecture is "lpgbt_fpga_gt,gtwizard_v3_6_13,{protocol_file=Start_from_scratch}";

    constant DLY : time := 1 ns;

    attribute ASYNC_REG : string;

    signal gt0_txfsmresetdone_i : std_logic;
    signal gt0_txfsmresetdone_r : std_logic;
    signal gt0_txfsmresetdone_r2 : std_logic;
    attribute ASYNC_REG of gt0_txfsmresetdone_r  : signal is "TRUE";
    attribute ASYNC_REG of gt0_txfsmresetdone_r2 : signal is "TRUE";
    signal gt0_rxfsmresetdone_i : std_logic;
    signal gt0_rxresetdone_r  : std_logic;
    signal gt0_rxresetdone_r2 : std_logic;
    signal gt0_rxresetdone_r3 : std_logic;
    attribute ASYNC_REG of gt0_rxresetdone_r  : signal is "TRUE";
    attribute ASYNC_REG of gt0_rxresetdone_r2 : signal is "TRUE";
    attribute ASYNC_REG of gt0_rxresetdone_r3 : signal is "TRUE";

    signal gt0_rxdata_i                    : std_logic_vector(31 downto 0);
    signal gt0_rxslide_i                   : std_logic;
    signal gt0_rxresetdone_i               : std_logic;
    signal gt0_txdata_i                    : std_logic_vector(31 downto 0);
    --signal gt0_txresetdone_i               : std_logic;
    signal gt0_tx_system_reset_c           : std_logic;
    signal gt0_rx_system_reset_c           : std_logic;
    signal drpclk_in_i                     : std_logic;
    signal DRPCLK_IN                       : std_logic;
    signal gt0_txusrclk2_i                 : std_logic; 
    signal gt0_rxusrclk2_i                 : std_logic;
    
    signal gt0_error_count_i               : std_logic_vector(7 downto 0);
    signal gt0_rxdata_i_r                  : std_logic_vector(31 downto 0);
    signal gt0_rx_data_valid               : std_logic;
    signal rxslide_counter : std_logic_vector(5 downto 0);

    signal downlink_clock_en     : std_logic; 
    signal downlink_data_group_0 : std_logic_vector(15 downto 0);
    signal downlink_data_group_1 : std_logic_vector(15 downto 0);
    signal downlink_data_ec      : std_logic_vector(1 downto 0);
    signal downlink_data_ic      : std_logic_vector(1 downto 0);
    signal downlink_ready        : std_logic;
    
    signal uplink_user_data      : std_logic_vector(223 downto 0);
    signal uplink_ic_data        : std_logic_vector(1 downto 0);
    signal uplink_ec_data        : std_logic_vector(1 downto 0);
    signal uplink_ready_i        : std_logic;
    signal uplink_clk_en         : std_logic;
    signal uplink_clk_counter    : std_logic_vector(2 downto 0);

begin

    gt0_tx_system_reset_c <= not gt0_txfsmresetdone_r2;
    gt0_rx_system_reset_c <= not gt0_rxresetdone_r3;

    IBUFDS_DRP_CLK : IBUFDS
    port map
    (
        I  => DRP_CLK_IN_P,
        IB => DRP_CLK_IN_N,
        O  => DRPCLK_IN
    );
    
    DRP_CLK_BUFG : BUFG 
    port map 
    (
        I    => DRPCLK_IN,
        O    => drpclk_in_i 
    );
    
    lpgbt_emul_gt_support_i : entity work.lpgbt_emul_gt_support
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     => EXAMPLE_SIM_GTRESET_SPEEDUP,
        STABLE_CLOCK_PERIOD             => STABLE_CLOCK_PERIOD
    )
    port map
    (
        SOFT_RESET_TX_IN                => '0',
        SOFT_RESET_RX_IN                => '0',
        DONT_RESET_ON_DATA_ERROR_IN     => '0',
        Q2_CLK1_GTREFCLK_PAD_N_IN       => Q2_CLK1_GTREFCLK_PAD_N_IN,
        Q2_CLK1_GTREFCLK_PAD_P_IN       => Q2_CLK1_GTREFCLK_PAD_P_IN,
        GT0_RX_MMCM_LOCK_OUT            => open,
        GT0_TX_FSM_RESET_DONE_OUT       => gt0_txfsmresetdone_i,
        GT0_RX_FSM_RESET_DONE_OUT       => gt0_rxfsmresetdone_i,
        GT0_DATA_VALID_IN               => gt0_rx_data_valid,
        GT0_TXUSRCLK_OUT                => open,
        GT0_TXUSRCLK2_OUT               => gt0_txusrclk2_i,
        GT0_RXUSRCLK_OUT                => open,
        GT0_RXUSRCLK2_OUT               => gt0_rxusrclk2_i,
        gt0_drpaddr_in                  => (others => '0'),
        gt0_drpdi_in                    => (others => '0'),
        gt0_drpdo_out                   => open,
        gt0_drpen_in                    => '0',
        gt0_drprdy_out                  => open,
        gt0_drpwe_in                    => '0',
        gt0_dmonitorout_out             => open,
        gt0_eyescanreset_in             => '0',
        gt0_rxuserrdy_in                => '1',
        gt0_eyescandataerror_out        => open,
        gt0_eyescantrigger_in           => '0',
        gt0_rxdata_out                  => gt0_rxdata_i,
        gt0_gtxrxp_in                   => RXP_IN,
        gt0_gtxrxn_in                   => RXN_IN,
        gt0_rxcommadet_out              => open,
        gt0_rxdfelpmreset_in            => '0',
        gt0_rxmonitorout_out            => open,
        gt0_rxmonitorsel_in             => "00",
        gt0_rxoutclkfabric_out          => open,
        gt0_gtrxreset_in                => '0',
        gt0_rxpmareset_in               => '0',
        gt0_rxpolarity_in               => '0',
        gt0_rxslide_in                  => gt0_rxslide_i,
        gt0_rxresetdone_out             => open,
        gt0_gttxreset_in                => '0',
        gt0_txuserrdy_in                => uplink_ready_i,
        gt0_txdata_in                   => gt0_txdata_i,
        gt0_gtxtxn_out                  => TXN_OUT,
        gt0_gtxtxp_out                  => TXP_OUT,
        gt0_txoutclkfabric_out          => open,
        gt0_txoutclkpcs_out             => open,
        gt0_txresetdone_out             => gt0_rxresetdone_i,
        gt0_txpolarity_in               => '0',
        GT0_QPLLLOCK_OUT                => open,
        GT0_QPLLREFCLKLOST_OUT          => open,
        GT0_QPLLOUTCLK_OUT              => open,
        GT0_QPLLOUTREFCLK_OUT           => open,
        sysclk_in                       => drpclk_in_i
    );

    rx_reset : process(gt0_rxusrclk2_i, gt0_rxresetdone_i)
    begin
        if (gt0_rxresetdone_i = '0') then
            gt0_rxresetdone_r  <= '0'   after DLY;
            gt0_rxresetdone_r2 <= '0'   after DLY;
            gt0_rxresetdone_r3 <= '0'   after DLY;
        elsif rising_edge(gt0_rxusrclk2_i) then
            gt0_rxresetdone_r  <= gt0_rxresetdone_i  after DLY;
            gt0_rxresetdone_r2 <= gt0_rxresetdone_r  after DLY;
            gt0_rxresetdone_r3 <= gt0_rxresetdone_r2 after DLY;
        end if;
    end process;
    
    tx_reset : process(gt0_txusrclk2_i, gt0_txfsmresetdone_i)
    begin
        if (gt0_txfsmresetdone_i = '0') then
            gt0_txfsmresetdone_r  <= '0'   after DLY;
            gt0_txfsmresetdone_r2 <= '0'   after DLY;
        elsif rising_edge(gt0_txusrclk2_i) then
            gt0_txfsmresetdone_r  <= gt0_txfsmresetdone_i after DLY;
            gt0_txfsmresetdone_r2 <= gt0_txfsmresetdone_r after DLY;
        end if;
    end process;

--    txdata_gen : process(gt0_txusrclk2_i)
--    begin
--        if rising_edge(gt0_txusrclk2_i) then
--            if gt0_tx_system_reset_c = '1' then
--                gt0_txdata_i <= (others => '0');
--            else
--                gt0_txdata_i <= gt0_txdata_i + 1;
--            end if;
--        end if;
--    end process;
    
    uplink_data_gen : process(gt0_txusrclk2_i)
    begin
        if rising_edge(gt0_txusrclk2_i) then
            if gt0_tx_system_reset_c = '1' then
                uplink_user_data <= (others => '0');
                uplink_ic_data <= (others => '0');
                uplink_ec_data <= (others => '0');
                uplink_clk_en <= '0';
                uplink_clk_counter <= (others => '0');
            else
                if uplink_clk_counter = "111" then
                    uplink_clk_en <= '1';
                    uplink_user_data <= uplink_user_data + 1;
                    uplink_ic_data <= uplink_ic_data + 1;
                    uplink_ec_data <= uplink_ec_data + 1;
                    uplink_clk_counter <= (others => '0');
                else
                    uplink_clk_counter <= uplink_clk_counter +1;
                    uplink_clk_en <= '0';
                end if;
            end if;
        end if;
    end process;

    rxdata_check : process(gt0_rxusrclk2_i)
    begin
        if rising_edge(gt0_rxusrclk2_i) then
            if gt0_rx_system_reset_c = '1' then
                gt0_error_count_i <= (others => '0');
                --gt0_rxslide_i <= '0';
                gt0_rx_data_valid <= '0';
                rxslide_counter <= (others => '0');
            else
                gt0_rx_data_valid <= '1';
                gt0_rxdata_i_r <= gt0_rxdata_i;
                if gt0_rxfsmresetdone_i = '1' then
                    if gt0_rxdata_i = gt0_rxdata_i_r + 1 then
                        --gt0_rxslide_i <= '0';
                        rxslide_counter <= (others => '0');
                    else
                        rxslide_counter <= rxslide_counter + 1;
                        if rxslide_counter = "000000" then
                            --gt0_rxslide_i <= '1';
                            gt0_error_count_i <= gt0_error_count_i + 1;
                        else
                            --gt0_rxslide_i <= '0';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    lpgbt_emul_inst : entity work.lpgbtemul_top
    generic map (
        rxslide_pulse_duration =>  1,  -- Duration of GT_RXSLIDE_OUT pulse
        rxslide_pulse_delay    =>  32  -- Minimum time between two GT_RXSLIDE_OUT pulses
    )
    port map (
        -- DownLink
        downlinkClkEn_o    => downlink_clock_en,
        downLinkDataGroup0 => downlink_data_group_0,
        downLinkDataGroup1 => downlink_data_group_1,
        downLinkDataEc     => downlink_data_ec,
        downLinkDataIc     => downlink_data_ic,
        downlinkRdy_o      => downlink_ready,
                           
        -- Uplink          
        uplinkClkEn_i      => uplink_clk_en,
        upLinkData0        => uplink_user_data(31 downto 0),
        upLinkData1        => uplink_user_data(63 downto 32),
        upLinkData2        => uplink_user_data(95 downto 64),
        upLinkData3        => uplink_user_data(127 downto 96),
        upLinkData4        => uplink_user_data(159 downto 128),
        upLinkData5        => uplink_user_data(191 downto 160),
        upLinkData6        => uplink_user_data(223 downto 192),
        upLinkDataIC       => uplink_ic_data,
        upLinkDataEC       => uplink_ec_data,
        uplinkRdy_o        => uplink_ready_i,
                           
        -- Uplink mode     
        fecMode            => '0',
        txDataRate         => '1',

		-- Transceiver     
        GT_RXUSRCLK_IN     => gt0_rxusrclk2_i,
        GT_TXUSRCLK_IN     => gt0_txusrclk2_i,
        GT_RXSLIDE_OUT     => gt0_rxslide_i,
        GT_TXREADY_IN      => gt0_txfsmresetdone_r2,
        GT_RXREADY_IN      => gt0_rxfsmresetdone_i,
        GT_TXDATA_OUT      => gt0_txdata_i,
        GT_RXDATA_IN       => gt0_rxdata_i

    ); 

end RTL;