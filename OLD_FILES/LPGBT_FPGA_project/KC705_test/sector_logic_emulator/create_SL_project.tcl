
create_project SL_emul SL_emul_vivado -part xc7k325tffg900-2
set_property board_part xilinx.com:kc705:part0:1.6 [current_project]

add_files {
    codes/design_sources/lpgbt_fpga_gt_exdes.vhd
    codes/design_sources/lpgbtfpga_uplink.vhd
    codes/design_sources/lpgbt_fpga_gt_support.vhd
    codes/design_sources/lpgbt_fpga_gt_common.vhd
    codes/design_sources/lpgbtfpga_package.vhd
    codes/design_sources/uplink/descrambler_51bitOrder49.vhd
    codes/design_sources/uplink/fec_rsDecoderN15K13.vhd
    codes/design_sources/uplink/descrambler_60bitOrder58.vhd
    codes/design_sources/uplink/descrambler_58bitOrder58.vhd
    codes/design_sources/uplink/lpgbtfpga_framealigner.vhd
    codes/design_sources/uplink/lpgbtfpga_deinterleaver.vhd
    codes/design_sources/uplink/lpgbtfpga_rxgearbox.vhd
    codes/design_sources/uplink/descrambler_53bitOrder49.vhd
    codes/design_sources/uplink/lpgbtfpga_decoder.vhd
    codes/design_sources/uplink/lpgbtfpga_descrambler.vhd
    codes/design_sources/uplink/fec_rsDecoderN31K29.vhd
    codes/design_sources/downlink/lpgbtfpga_encoder.vhd
    codes/design_sources/downlink/lpgbtfpga_scrambler.vhd
    codes/design_sources/downlink/lpgbtfpga_interleaver.vhd
    codes/design_sources/downlink/lpgbtfpga_txgearbox.vhd
    codes/design_sources/downlink/rs_encoder_N7K5.vhd
    codes/design_sources/lpgbt_fpga_gt_clock_module.vhd
    codes/design_sources/lpgbtfpga_downlink.vhd
    codes/design_sources/lpgbt_fpga_gt_common_reset.vhd
    codes/design_sources/lpgbt_fpga_gt_gt_usrclk_source.vhd}

add_files -norecurse {
    codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci
    codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci
    codes/ip/clk_160_generator/clk_160_generator.xci}

update_compile_order -fileset sources_1

set_property SOURCE_SET sources_1 [get_filesets sim_1]

add_files -fileset sim_1 {
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/downLinkRxDataPath.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_multBy2_4.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/scrambler53bitOrder49.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_multBy2_3.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/upLinkInterleaver.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/upLinkDataSelect.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_log_3.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_multBy2_5.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/rs_encoder_N31K29.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/rs_encoder_N15K13.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/scrambler51bitOrder49.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_inv_3.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/mgt_frameAligner_pv.vhd
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_multBy3_5.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/rxgearbox.vhd
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_multBy3_4.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/scrambler58bitOrder58.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/downLinkFECDecoder.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/rs_decoder_N7K5.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/upLinkTxDataPath.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/txgearbox.vhd
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/lpgbtemul_top.vhd
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/scrambler60bitOrder58.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_add_5.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_mult_3.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/downLinkDeinterleaver.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/upLinkScrambler.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/descrambler36bitOrder36.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_add_4.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/gf_add_3.v
    codes/simulation/lpgbt_emulator/lpgbt-emul-master/upLinkFECEncoder.v
    codes/simulation/lpgbt_emulator/lpgbt_emul_gt_clock_module.vhd
    codes/simulation/lpgbt_fpga_gt_tb.vhd
    codes/simulation/lpgbt_emulator/lpgbt_emul_gt_exdes.vhd
    codes/simulation/lpgbt_emulator/lpgbt_emul_gt_support.vhd
    codes/simulation/sim_reset_gt_model.vhd
    codes/simulation/lpgbt_emulator/lpgbt_emul_gt_gt_usrclk_source.vhd
    codes/simulation/lpgbt_emulator/lpgbt_emul_gt_common_reset.vhd
    codes/simulation/lpgbt_emulator/lpgbt_emul_gt_common.vhd}

update_compile_order -fileset sim_1

add_files -fileset constrs_1 -norecurse  codes/constrs/lpgbt_fpga_gt_exdes.xdc
add_files -fileset constrs_1 -norecurse  codes/constrs/TIMING_CONSTRAINTS.xdc


generate_target all [get_files codes/ip/clk_160_generator/clk_160_generator.xci]
catch { config_ip_cache -export [get_ips -all clk_160_generator] }
export_ip_user_files -of_objects [get_files  codes/ip/clk_160_generator/clk_160_generator.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1]  codes/ip/clk_160_generator/clk_160_generator.xci]
export_simulation -of_objects [get_files  codes/ip/clk_160_generator/clk_160_generator.xci] -directory  SL_emul_vivado/SL_emul.ip_user_files/sim_scripts -ip_user_files_dir  SL_emul_vivado/SL_emul.ip_user_files -ipstatic_source_dir  SL_emul_vivado/SL_emul.ip_user_files/ipstatic -lib_map_path [list {modelsim= SL_emul_vivado/SL_emul.cache/compile_simlib/modelsim} {questa= SL_emul_vivado/SL_emul.cache/compile_simlib/questa} {ies= SL_emul_vivado/SL_emul.cache/compile_simlib/ies} {xcelium= SL_emul_vivado/SL_emul.cache/compile_simlib/xcelium} {vcs= SL_emul_vivado/SL_emul.cache/compile_simlib/vcs} {riviera= SL_emul_vivado/SL_emul.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

generate_target all [get_files   codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci]
catch { config_ip_cache -export [get_ips -all lpgbt_fpga_gt] }
export_ip_user_files -of_objects [get_files  codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1]  codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci]
export_simulation -of_objects [get_files  codes/ip/lpgbt_fpga_gt/lpgbt_fpga_gt.xci] -directory  SL_emul_vivado/SL_emul.ip_user_files/sim_scripts -ip_user_files_dir  SL_emul_vivado/SL_emul.ip_user_files -ipstatic_source_dir  SL_emul_vivado/SL_emul.ip_user_files/ipstatic -lib_map_path [list {modelsim= SL_emul_vivado/SL_emul.cache/compile_simlib/modelsim} {questa= SL_emul_vivado/SL_emul.cache/compile_simlib/questa} {ies= SL_emul_vivado/SL_emul.cache/compile_simlib/ies} {xcelium= SL_emul_vivado/SL_emul.cache/compile_simlib/xcelium} {vcs= SL_emul_vivado/SL_emul.cache/compile_simlib/vcs} {riviera= SL_emul_vivado/SL_emul.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

generate_target all [get_files   codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci]
catch { config_ip_cache -export [get_ips -all lpgbt_emul_gt] }
export_ip_user_files -of_objects [get_files  codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1]  codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci]
export_simulation -of_objects [get_files  codes/ip/lpgbt_emul_gt/lpgbt_emul_gt.xci] -directory  SL_emul_vivado/SL_emul.ip_user_files/sim_scripts -ip_user_files_dir  SL_emul_vivado/SL_emul.ip_user_files -ipstatic_source_dir  SL_emul_vivado/SL_emul.ip_user_files/ipstatic -lib_map_path [list {modelsim= SL_emul_vivado/SL_emul.cache/compile_simlib/modelsim} {questa= SL_emul_vivado/SL_emul.cache/compile_simlib/questa} {ies= SL_emul_vivado/SL_emul.cache/compile_simlib/ies} {xcelium= SL_emul_vivado/SL_emul.cache/compile_simlib/xcelium} {vcs= SL_emul_vivado/SL_emul.cache/compile_simlib/vcs} {riviera= SL_emul_vivado/SL_emul.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

