-- project: SLReadout
-- wrapper_top_SLR3.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;

entity wrapper_top_SLR3 is
    Port ( clock_40_int : in std_logic;
           clock_80_int : in std_logic;
           clock_240_int : in std_logic;
           clock_320_int : in std_logic;
           write_en : in std_logic;
           debug_out : out std_logic );
           
end wrapper_top_SLR3;



architecture Behavioral of wrapper_top_SLR3 is

 signal mgtrefclk0_x1y12_p_SLR3 : std_logic;
 signal mgtrefclk0_x1y12_n_SLR3 : std_logic;
 
 signal txctrl2_in_SLR3 : std_logic_vector(23 downto 0);
 signal hb_gtwiz_reset_all_in : std_logic;
  
 signal word_28_SLR0_readout : array_20x28b;
 signal word_28_SLR1_readout : array_10x28b;
 signal word_28_SLR2_readout : array_20x28b;
 
 signal ch0_gtyrxn_in_TDAQ_SLR3 : std_logic;
 signal ch0_gtyrxp_in_TDAQ_SLR3 : std_logic;
 signal ch0_gtytxn_out_TDAQ_SLR3 : std_logic;
 signal ch0_gtytxp_out_TDAQ_SLR3 : std_logic;
 
 signal ch1_gtyrxn_in_TDAQ_SLR3 : std_logic;
 signal ch1_gtyrxp_in_TDAQ_SLR3 : std_logic;
 signal ch1_gtytxn_out_TDAQ_SLR3 : std_logic;
 signal ch1_gtytxp_out_TDAQ_SLR3 : std_logic;
 
 signal ch2_gtyrxn_in_TDAQ_SLR3 : std_logic;
 signal ch2_gtyrxp_in_TDAQ_SLR3 : std_logic;
 signal ch2_gtytxn_out_TDAQ_SLR3 : std_logic;
 signal ch2_gtytxp_out_TDAQ_SLR3 : std_logic;
 
 signal L0_acc : std_logic;
 signal reset : std_logic;
           
           
 attribute dont_touch : string;
 attribute dont_touch of mgtrefclk0_x1y12_p_SLR3 : signal is "true";
 attribute dont_touch of mgtrefclk0_x1y12_n_SLR3 : signal is "true";
 
 attribute dont_touch of txctrl2_in_SLR3 : signal is "true";
 attribute dont_touch of hb_gtwiz_reset_all_in : signal is "true";
 
 attribute dont_touch of word_28_SLR0_readout : signal is "true";
 attribute dont_touch of word_28_SLR1_readout : signal is "true";
 attribute dont_touch of word_28_SLR2_readout : signal is "true";
 
 attribute dont_touch of  ch0_gtyrxn_in_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch0_gtyrxp_in_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch0_gtytxn_out_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch0_gtytxp_out_TDAQ_SLR3 : signal is "true";
 
 attribute dont_touch of  ch1_gtyrxn_in_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch1_gtyrxp_in_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch1_gtytxn_out_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch1_gtytxp_out_TDAQ_SLR3 : signal is "true";
 
 attribute dont_touch of  ch2_gtyrxn_in_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch2_gtyrxp_in_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch2_gtytxn_out_TDAQ_SLR3 : signal is "true";
 attribute dont_touch of  ch2_gtytxp_out_TDAQ_SLR3 : signal is "true";
 
 attribute dont_touch of L0_acc : signal is "true";
 attribute dont_touch of reset : signal is "true";
 
       
begin
  
    SLR3_inst : entity work.SLR3_top 
    port map
    (   mgtrefclk0_x1y12_p  => mgtrefclk0_x1y12_p_SLR3,
        mgtrefclk0_x1y12_n  => mgtrefclk0_x1y12_n_SLR3,
        
         -- Serial data ports for transceiver channel 0
        ch0_gtyrxn_in_TDAQ     => ch0_gtyrxn_in_TDAQ_SLR3,
        ch0_gtyrxp_in_TDAQ     => ch0_gtyrxp_in_TDAQ_SLR3,
        ch0_gtytxn_out_TDAQ    => ch0_gtytxn_out_TDAQ_SLR3,
        ch0_gtytxp_out_TDAQ    => ch0_gtytxp_out_TDAQ_SLR3,
        
       -- Serial data ports for transceiver channel 1
        ch1_gtyrxn_in_TDAQ     => ch1_gtyrxn_in_TDAQ_SLR3, 
        ch1_gtyrxp_in_TDAQ     => ch1_gtyrxp_in_TDAQ_SLR3, 
        ch1_gtytxn_out_TDAQ    => ch1_gtytxn_out_TDAQ_SLR3,
        ch1_gtytxp_out_TDAQ    => ch1_gtytxp_out_TDAQ_SLR3,
        
        -- Serial data ports for transceiver channel 2
        ch2_gtyrxn_in_TDAQ     => ch2_gtyrxn_in_TDAQ_SLR3, 
        ch2_gtyrxp_in_TDAQ     => ch2_gtyrxp_in_TDAQ_SLR3, 
        ch2_gtytxn_out_TDAQ    => ch2_gtytxn_out_TDAQ_SLR3,
        ch2_gtytxp_out_TDAQ    => ch2_gtytxp_out_TDAQ_SLR3,
        
        txctrl2_SLR3_in => txctrl2_in_SLR3,
        
        hb_gtwiz_reset_clk_freerun_in_SLR3   => clock_80_int,
        hb_gtwiz_reset_all_in_SLR3           => hb_gtwiz_reset_all_in,
        
        word_28_SLR0_in => word_28_SLR0_readout,
        word_28_SLR1_in => word_28_SLR1_readout,
        word_28_SLR2_in => word_28_SLR2_readout,
        
        clock_320_in    => clock_320_int,
        clock_240_in    => clock_240_int, 
        clock_80_in     => clock_80_int,
        clock_40_in     => clock_40_int,
        L0_acc          => L0_acc,
        reset           => reset,
        
        write_en        =>  write_en,
        debug           => debug_out
        );
        
        

end Behavioral;
