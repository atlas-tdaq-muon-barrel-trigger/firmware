create_clock -period 25 -name clock_40_int -waveform {0.000 12.5} [get_ports clock_40_int]
create_clock -period 12.5 -name clock_80_int -waveform {0.000 6.25} [get_ports clock_80_int]
create_clock -period 4.1667 -name clock_240_int -waveform {0.000 2.0833} [get_ports clock_240_int]
create_clock -period 3.125 -name clock_320_int -waveform {0.000 1.5625} [get_ports clock_320_int]

create_pblock pblock_SLR3_inst
add_cells_to_pblock [get_pblocks pblock_SLR3_inst] [get_cells -quiet [list SLR3_inst]]
resize_pblock [get_pblocks pblock_SLR3_inst] -add {SLR3}
