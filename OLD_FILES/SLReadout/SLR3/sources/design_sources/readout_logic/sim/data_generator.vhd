-- project SLReadout
-- data_generator.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;


entity data_generator is
    Port (clock : in std_logic;
          enable : in std_logic;
          BM_L_data_bus : out array_slv27to0x19DCT;
          BM_R_data_bus : out array_slv27to0x19DCT;
          BI_data_bus : out array_slv27to0x10DCT );
end data_generator;

architecture Behavioral of data_generator is

component BM_data_generator is
    Port (clock : in std_logic;
          enable : in std_logic;
          DCT_data_bus : out array_slv27to0x19DCT);
end component;

component BI_data_generator is
    Port (clock : in std_logic;
          enable : in std_logic;
          DCT_data_bus : out array_slv27to0x10DCT);
end component;


begin

pmap_BM_L_data_generator : BM_data_generator port map (
    clock => clock,
    enable => enable,
    DCT_data_bus => BM_L_data_bus );
    
pmap_BM_R_data_generator : BM_data_generator port map (
    clock => clock,
    enable => enable,
    DCT_data_bus => BM_R_data_bus );

pmap_BI_data_generator : BI_data_generator port map (
    clock => clock,
    enable => enable,
    DCT_data_bus => BI_data_bus );
    
end Behavioral;
