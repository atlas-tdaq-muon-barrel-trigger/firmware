-- project SLReadout
-- hit_generator.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity hit_generator is


  Port (
    CLK : in std_logic;
    ENA : in std_logic;
    CNT : in std_logic_vector(4 downto 0);
    DOUT : out std_logic_vector(27 downto 0)
  );
end hit_generator;

architecture Behavioral of hit_generator is

type state_type is (idle, ST0, ST1);
signal CS : state_type := idle;
signal BCID : std_logic_vector(8 downto 0) := (others => '0');
signal HIT : std_logic_vector(7 downto 0) := "00000001";
signal hit_count  :std_logic_vector(3 downto 0) := (others => '0');
signal empty_count : std_logic_vector(3 downto 0) := (others => '0');

begin

 seq_proc : process(CLK)
  begin
    if rising_edge(CLK) then
        case CS is
            when idle =>
                DOUT <= (others => '0');
                    if ENA = '1' then
                        CS <= ST0;
                        hit_count <= (others => '0');
                        HIT <= "00000001";
                    end if;
            when ST0 => 
                DOUT <= "0000" & CNT & BCID & "00" & HIT; -- 9 bit stripID + 9 bit BCID + 10 bit HIT timing
                HIT <= HIT + 1;
                if to_integer(unsigned(hit_count)) < to_integer(unsigned(CNT(1 downto 0))) then
                    hit_count <= hit_count + 1;
                else
                    empty_count <= (others => '0');
                    CS <= ST1;
                end if;
            
            when ST1 => 
                DOUT <= "000000000" & BCID & "0000000000"; -- same with null stripID and HIT timing
                if to_integer(unsigned(empty_count)) < 6-to_integer(unsigned(CNT(1 downto 0))) then
                    empty_count <= empty_count + 1;
                else
                    BCID <= BCID + 1;
                    hit_count <= (others => '0');
                    HIT <= "00000001";
                    CS <= ST0;
                end if;
            end case;
        end if;
    end process;


end Behavioral;

