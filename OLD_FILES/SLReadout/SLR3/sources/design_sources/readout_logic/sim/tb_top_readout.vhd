-- project: SLReadout
-- tb_top_readout.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector

use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;

use IEEE.std_logic_textio.all;
use std.textio.all;

entity tb_top_readout is
--  Port ( );
end tb_top_readout;


architecture Behavioral of tb_top_readout is

  signal clock_40MHz : std_logic := '0';
  signal clock_320MHz : std_logic := '0';
  signal clock_80MHz : std_logic := '0';
  signal clock_240MHz : std_logic := '0';
  signal base_clock_40MHz : std_logic := '0';
  signal reset : std_logic := '1';
  signal enable : std_logic := '0'; 
  signal locked : std_logic := '0';
  signal bcid_to_BM_L : std_logic_vector(11 downto 0) := (others => '0');
  signal bcid_to_BM_R : std_logic_vector(11 downto 0) := (others => '0');
  signal bcid_to_BI : std_logic_vector(11 downto 0) := (others => '0');
  signal BM_L_data_bus_fromdg : array_slv27to0x19DCT;
  signal BM_R_data_bus_fromdg : array_slv27to0x19DCT;
  signal BI_data_bus_fromdg : array_slv27to0x10DCT;
  signal BM_L_out : std_logic_vector(31 downto 0);
  signal BM_R_out : std_logic_vector(31 downto 0);
  signal BI_out : std_logic_vector(31 downto 0);
  signal BM_L_out_toCheck : std_logic_vector(27 downto 0);
  signal BM_R_out_toCheck : std_logic_vector(27 downto 0);
  signal BI_out_toCheck : std_logic_vector(27 downto 0);
  signal L0A : std_logic;
  
  
  component top_readout
    Port ( clock : in std_logic;
           bcid_BM_L     : in std_logic_vector(11 downto 0); -- new
           bcid_BM_R     : in std_logic_vector(11 downto 0); -- new
           bcid_BI       : in std_logic_vector(11 downto 0); -- new
           reset : in std_logic;
           L0A : in std_logic;
           BM_L_in : in array_slv27to0x19DCT;
           BM_R_in : in array_slv27to0x19DCT;
           BI_in : in array_slv27to0x10DCT;
           BM_L_out : out std_logic_vector(31 downto 0);
           BM_R_out : out std_logic_vector(31 downto 0);
           BI_out : out std_logic_vector(31 downto 0) );
  end component;
  
  component clk_multiplier
    Port ( clk_in_40MHz : in std_logic;
           clk_out_40MHz : out std_logic;
           clk_out_320MHz : out std_logic;
           clk_out_80MHz : out std_logic;
           clk_out_240MHz : out std_logic;
           locked : out std_logic );
  end component; 
  
  component data_generator
    Port ( clock : in std_logic;
           enable : in std_logic;
           BM_L_data_bus : out array_slv27to0x19DCT;
           BM_R_data_bus : out array_slv27to0x19DCT;
           BI_data_bus : out array_slv27to0x10DCT );
  end component;
  
  component L0A_generator
    Port (clock : in std_logic;
          enable : in std_logic;
          L0A : out std_logic );
  end component;
  
  
      
begin

  pmap_top_readout : top_readout port map (
    clock => clock_240MHz,
    bcid_BM_L => bcid_to_BM_L,
    bcid_BM_R => bcid_to_BM_R,
    bcid_BI => bcid_to_BI,
    reset => reset,
    L0A => L0A,
    BM_L_in => BM_L_data_bus_fromdg,
    BM_R_in => BM_R_data_bus_fromdg,
    BI_in => BI_data_bus_fromdg,
    BM_L_out => BM_L_out,
    BM_R_out => BM_R_out,
    BI_out => BI_out );
    
  pmap_clk_multiplier : clk_multiplier port map (
    clk_in_40MHz => base_clock_40MHz,
    clk_out_40MHz => clock_40MHz,
    clk_out_320MHz => clock_320MHz,
    clk_out_80MHz => clock_80MHz,
    clk_out_240MHz => clock_240MHz,
    locked => locked );
    
  pmap_data_generator : data_generator port map (
    clock => clock_240MHz,
    enable => enable,
    BM_L_data_bus => BM_L_data_bus_fromdg,
    BM_R_data_bus => BM_R_data_bus_fromdg,
    BI_data_bus => BI_data_bus_fromdg );
    
  pmap_L0A_generator : L0A_generator port map (
    clock => clock_240MHz,
    enable => enable,
    L0A => L0A );
    
    
  base_clock_40MHz_gen : process
  begin
    base_clock_40MHz <= '1';
    wait for 12.5 ns;
    base_clock_40MHz <= '0';
    wait for 12.5 ns;
  end process;
 
  reset_gen : process(clock_240MHz) 
  begin
    if rising_edge(clock_240MHz) then
        if locked = '1' then
            reset <= '0';
        else
            reset <= '1';
        end if;
     end if;
  end process;
  
  enable_gen : process
  begin
    enable <= '0';
    wait for 3993.75 ns; -- 4000-(3,125*2) ns
    enable <= '1';
    wait;
  end process;
  
  check_gen : process(clock_240MHz) -- this output signal has the same format of the DCT input, and can be used to check that the readout is working.
  begin
    if rising_edge(clock_240MHz) then
        BM_L_out_toCheck <= BM_L_out(30 downto 22) & BM_L_out(18 downto 0);
        BM_R_out_toCheck <= BM_R_out(30 downto 22) & BM_R_out(18 downto 0);
        BI_out_toCheck <= BI_out(30 downto 22) & BI_out(18 downto 0);
    end if;
  end process;

  bcid_to_BM_L <= "000" & BM_L_data_bus_fromdg(0)(18 downto 10); -- PROBLEM: here need to add the three most significant bits of the SL counter (instead of the three null bits)
  bcid_to_BM_R <= "000" & BM_R_data_bus_fromdg(0)(18 downto 10);
  bcid_to_BI <= "000" & BI_data_bus_fromdg(0)(18 downto 10);
  
  
end Behavioral;
