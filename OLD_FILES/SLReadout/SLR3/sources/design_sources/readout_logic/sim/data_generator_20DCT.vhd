-- project SLReadout
-- data_generator_20DCT.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;


entity data_generator_20DCT is
    Port (clock_320MHz : in std_logic;
          enable : in std_logic;
          word_28_SLR0_readout : out array_20x28b;
          word_28_SLR1_readout : out array_10x28b;
          word_28_SLR2_readout : out array_20x28b );
end data_generator_20DCT;

architecture Behavioral of data_generator_20DCT is

signal BM_L_data_bus : array_slv27to0x19DCT;
signal BM_R_data_bus : array_slv27to0x19DCT;
signal BI_data_bus : array_slv27to0x10DCT;

signal null_BM_L : std_logic_vector(27 downto 0);
signal null_BM_R : std_logic_vector(27 downto 0);

component data_generator is
    Port (clock_320MHz : in std_logic;
          enable : in std_logic;
          BM_L_data_bus : out array_slv27to0x19DCT;
          BM_R_data_bus : out array_slv27to0x19DCT;
          BI_data_bus : out array_slv27to0x10DCT );
end component;

begin

pmap_data_generator : data_generator port map (
    clock_320MHz => clock_320MHz,
    enable => enable,
    BM_L_data_bus => BM_L_data_bus,
    BM_R_data_bus => BM_R_data_bus,
    BI_data_bus => BI_data_bus );

-- map 20DCT signals into 19DCT signals + 1 null sugnal
word_28_SLR0_readout(0) <= BM_L_data_bus(0);
word_28_SLR0_readout(1) <= BM_L_data_bus(1);
word_28_SLR0_readout(2) <= BM_L_data_bus(2);
word_28_SLR0_readout(3) <= BM_L_data_bus(3);
word_28_SLR0_readout(4) <= BM_L_data_bus(4);
word_28_SLR0_readout(5) <= BM_L_data_bus(5);
word_28_SLR0_readout(6) <= BM_L_data_bus(6);
word_28_SLR0_readout(7) <= BM_L_data_bus(7);
word_28_SLR0_readout(8) <= BM_L_data_bus(8);
word_28_SLR0_readout(9) <= BM_L_data_bus(9);
word_28_SLR0_readout(10) <= BM_L_data_bus(10);
word_28_SLR0_readout(11) <= BM_L_data_bus(11);
word_28_SLR0_readout(12) <= BM_L_data_bus(12);
word_28_SLR0_readout(13) <= BM_L_data_bus(13);
word_28_SLR0_readout(14) <= BM_L_data_bus(14);
word_28_SLR0_readout(15) <= BM_L_data_bus(15);
word_28_SLR0_readout(16) <= BM_L_data_bus(16);
word_28_SLR0_readout(17) <= BM_L_data_bus(17);
word_28_SLR0_readout(18) <= BM_L_data_bus(18);
word_28_SLR0_readout(19) <= null_BM_L;

word_28_SLR1_readout(0) <= BI_data_bus(0);
word_28_SLR1_readout(1) <= BI_data_bus(1); 
word_28_SLR1_readout(2) <= BI_data_bus(2);
word_28_SLR1_readout(3) <= BI_data_bus(3);
word_28_SLR1_readout(4) <= BI_data_bus(4);
word_28_SLR1_readout(5) <= BI_data_bus(5);
word_28_SLR1_readout(6) <= BI_data_bus(6);
word_28_SLR1_readout(7) <= BI_data_bus(7);
word_28_SLR1_readout(8) <= BI_data_bus(8);
word_28_SLR1_readout(9) <= BI_data_bus(9);

word_28_SLR2_readout(0) <= BM_R_data_bus(0);
word_28_SLR2_readout(1) <= BM_R_data_bus(1);
word_28_SLR2_readout(2) <= BM_R_data_bus(2);
word_28_SLR2_readout(3) <= BM_R_data_bus(3);
word_28_SLR2_readout(4) <= BM_R_data_bus(4);
word_28_SLR2_readout(5) <= BM_R_data_bus(5);
word_28_SLR2_readout(6) <= BM_R_data_bus(6);
word_28_SLR2_readout(7) <= BM_R_data_bus(7);
word_28_SLR2_readout(8) <= BM_R_data_bus(8);
word_28_SLR2_readout(9) <= BM_R_data_bus(9);
word_28_SLR2_readout(10) <= BM_R_data_bus(10);
word_28_SLR2_readout(11) <= BM_R_data_bus(11);
word_28_SLR2_readout(12) <= BM_R_data_bus(12);
word_28_SLR2_readout(13) <= BM_R_data_bus(13);
word_28_SLR2_readout(14) <= BM_R_data_bus(14);
word_28_SLR2_readout(15) <= BM_R_data_bus(15);
word_28_SLR2_readout(16) <= BM_R_data_bus(16);
word_28_SLR2_readout(17) <= BM_R_data_bus(17);
word_28_SLR2_readout(18) <= BM_R_data_bus(18);
word_28_SLR2_readout(19) <= null_BM_R;

null_sig_gen : process(clock_320MHz)
begin
    if rising_edge(clock_320MHz) then
        null_BM_L <= (others => '0');
        null_BM_R <= (others => '0');
    end if;
end process;



    
end Behavioral;

