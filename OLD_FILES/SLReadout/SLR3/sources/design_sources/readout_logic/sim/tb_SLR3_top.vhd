-- project: SLReadout
-- tb_SLR3_top.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operation with std_logic_vector

use ieee.math_real.uniform; -- to extract uniform random numbers in [0,1)
use ieee.math_real.floor; -- to do floor (parte intera) operation
use ieee.numeric_std.all;

library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;

use IEEE.std_logic_textio.all;
use std.textio.all;

entity tb_SLR3_top is
--  Port ( );
end tb_SLR3_top;


architecture Behavioral of tb_SLR3_top is

signal reset : std_logic := '0';
signal enable : std_logic := '0'; 
signal locked : std_logic := '0';

signal L0A : std_logic;

signal mgtrefclk0_x1y12_p_SLR3 : std_logic;
signal mgtrefclk0_x1y12_n_SLR3 : std_logic;
signal ch0_gtyrxn_in_TDAQ_SLR3 : std_logic;
signal ch0_gtyrxp_in_TDAQ_SLR3 : std_logic;
signal ch0_gtytxn_out_TDAQ_SLR3 : std_logic;
signal ch0_gtytxp_out_TDAQ_SLR3 : std_logic;
signal ch1_gtyrxn_in_TDAQ_SLR3 : std_logic;
signal ch1_gtyrxp_in_TDAQ_SLR3 : std_logic;
signal ch1_gtytxn_out_TDAQ_SLR3 : std_logic;
signal ch1_gtytxp_out_TDAQ_SLR3 : std_logic;
signal ch2_gtyrxn_in_TDAQ_SLR3 : std_logic; 
signal ch2_gtyrxp_in_TDAQ_SLR3 : std_logic; 
signal ch2_gtytxn_out_TDAQ_SLR3 : std_logic;
signal ch2_gtytxp_out_TDAQ_SLR3 : std_logic;
signal txctrl2_in_SLR3 : std_logic_vector(23 downto 0);
signal hb_gtwiz_reset_all_in : std_logic;

signal word_28_SLR0_readout : array_20x28b;
signal word_28_SLR1_readout : array_10x28b;
signal word_28_SLR2_readout : array_20x28b;

signal clock_320_int : std_logic;
signal clock_240_int : std_logic;
signal clock_80_int : std_logic;
signal clock_40_int : std_logic;
signal base_clock_40MHz : std_logic;

--signal debug_out : std_logic;
--signal write_en : std_logic;


component clk_multiplier   
  Port ( clk_in_40MHz : in std_logic;
         clk_out_40MHz : out std_logic;
         clk_out_320MHz : out std_logic;
         clk_out_80MHz : out std_logic;
         clk_out_240MHz : out std_logic;
         locked : out std_logic );
end component; 

component data_generator_20DCT
  Port ( clock_320MHz : in std_logic;
         enable : in std_logic;
         word_28_SLR0_readout : out array_20x28b;
         word_28_SLR1_readout : out array_10x28b;
         word_28_SLR2_readout : out array_20x28b );
end component;
  
component L0A_generator
  Port (  clock_320MHz : in std_logic;
          enable : in std_logic;
          L0A : out std_logic );
end component; 


begin


  SLR3_inst : entity work.SLR3_top 
    port map
    (  mgtrefclk0_x1y12_p  => mgtrefclk0_x1y12_p_SLR3,
       mgtrefclk0_x1y12_n  => mgtrefclk0_x1y12_n_SLR3,
       
        -- Serial data ports for transceiver channel 0
       ch0_gtyrxn_in_TDAQ     => ch0_gtyrxn_in_TDAQ_SLR3,
       ch0_gtyrxp_in_TDAQ     => ch0_gtyrxp_in_TDAQ_SLR3,
       ch0_gtytxn_out_TDAQ    => ch0_gtytxn_out_TDAQ_SLR3,
       ch0_gtytxp_out_TDAQ    => ch0_gtytxp_out_TDAQ_SLR3,
       
      -- Serial data ports for transceiver channel 1
       ch1_gtyrxn_in_TDAQ     => ch1_gtyrxn_in_TDAQ_SLR3, 
       ch1_gtyrxp_in_TDAQ     => ch1_gtyrxp_in_TDAQ_SLR3, 
       ch1_gtytxn_out_TDAQ    => ch1_gtytxn_out_TDAQ_SLR3,
       ch1_gtytxp_out_TDAQ    => ch1_gtytxp_out_TDAQ_SLR3,
       
       -- Serial data ports for transceiver channel 2
       ch2_gtyrxn_in_TDAQ     => ch2_gtyrxn_in_TDAQ_SLR3, 
       ch2_gtyrxp_in_TDAQ     => ch2_gtyrxp_in_TDAQ_SLR3, 
       ch2_gtytxn_out_TDAQ    => ch2_gtytxn_out_TDAQ_SLR3,
       ch2_gtytxp_out_TDAQ    => ch2_gtytxp_out_TDAQ_SLR3,
       
       txctrl2_SLR3_in => txctrl2_in_SLR3,
       
       hb_gtwiz_reset_clk_freerun_in_SLR3   => clock_80_int,
       hb_gtwiz_reset_all_in_SLR3           => hb_gtwiz_reset_all_in,
       
       word_28_SLR0_in => word_28_SLR0_readout,
       word_28_SLR1_in => word_28_SLR1_readout,
       word_28_SLR2_in => word_28_SLR2_readout,
       
       clock_320_in    => clock_320_int,
       clock_240_in    => clock_240_int, 
       clock_80_in     => clock_80_int,
       clock_40_in     => clock_40_int, 
       L0_acc          => L0A,
       reset           => reset
       
       --debug => debug_out, 
       --write_en =>  write_en
       );
     

  pmap_clk_multiplier : clk_multiplier port map (
    clk_in_40MHz => base_clock_40MHz,
    clk_out_40MHz => clock_40_int,
    clk_out_320MHz => clock_320_int,
    clk_out_80MHz => clock_80_int,
    clk_out_240MHz => clock_240_int,
    locked => locked );
  
    
  pmap_data_generator_20DCT : data_generator_20DCT port map (
    clock_320MHz => clock_320_int,
    enable => enable,
    word_28_SLR0_readout => word_28_SLR0_readout,
    word_28_SLR1_readout => word_28_SLR1_readout,
    word_28_SLR2_readout => word_28_SLR2_readout );
    
  pmap_L0A_generator : L0A_generator port map (
    clock_320MHz => clock_320_int,
    enable => enable,
    L0A => L0A );
    
  base_clock_40MHz_gen : process
  begin
    base_clock_40MHz <= '1';
    wait for 12.5 ns;
    base_clock_40MHz <= '0';
    wait for 12.5 ns;
  end process;
  
  reset_gen : process(clock_320_int) 
  begin
    if rising_edge(clock_320_int) then
        if locked = '1' then
            reset <= '0';
        else
            reset <= '1';
        end if;
     end if;
  end process;
  
  enable_gen : process
  begin
    enable <= '0';
    wait for 3993.75 ns; -- 4000-(3,125*2) ns
    enable <= '1';
    wait;
  end process;


  
end Behavioral;
