-- project: SLReadout
-- BM.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

library work;
use work.all;


entity BM is

  
  Port ( clock : in std_logic;
         SL_bcid : std_logic_vector(11 downto 0);
         reset : in std_logic;
         L0A : in std_logic;
         data_in : in array_slv27to0x19DCT;
         data_out : out std_logic_vector(31 downto 0) );
end BM;


architecture Behavioral of BM is

signal address_in : array_slv12to0x19DCT;
signal address_rst : std_logic_vector(8 downto 0) := (others => '0');
signal trig_BCID : std_logic_vector(8 downto 0) := (others => '0');
signal FIFO_L0A_write_en : std_logic := '0';
signal FIFO_L0A_read_en : std_logic := '0';
signal read_BCID : std_logic_vector(8 downto 0);
signal FIFO_L0A_full : std_logic := '0';
signal FIFO_L0A_empty : std_logic := '0';
signal wr_rst_busy : std_logic := '0';
signal rd_rst_busy : std_logic := '0';
signal nHit : std_logic_vector(3 downto 0);
signal nHitMax : std_logic_vector(3 downto 0) := (others => '0');

signal null_input : std_logic_vector(31 downto 0) := (others=> '1');



component FIFO_L0A
  Port ( clk : IN STD_LOGIC;
         srst : IN STD_LOGIC;
         din : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
         wr_en : IN STD_LOGIC;
         rd_en : IN STD_LOGIC;
         dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
         full : OUT STD_LOGIC;
         empty : OUT STD_LOGIC;
         wr_rst_busy : OUT STD_LOGIC;
         rd_rst_busy : OUT STD_LOGIC );
end component;

component top_memory_19DCT
  Port ( clock : in std_logic;
         data_in : in array_slv27to0x19DCT;
         read_BCID : in std_logic_vector(8 downto 0);
         SL_BCID : in std_logic_vector(11 downto 0);
         FIFO_L0A_empty : in std_logic;
         data_out : out std_logic_vector(31 downto 0);
         tm_FIFO_L0A_read_en : out std_logic );
end component;


component multiplexer_512to1
  Port ( clock : in std_logic;
         key : in std_logic_vector(8 downto 0);
         sgn_in : in array_slv3to0x512;
         sgn_out : out std_logic_vector(3 downto 0) );
end component;


begin

  pmap_FIFO_L0A : FIFO_L0A port map (
    clk => clock,
    srst => reset,
    din => trig_BCID,
    wr_en => FIFO_L0A_write_en,
    rd_en => FIFO_L0A_read_en,
    dout => read_BCID,
    full => FIFO_L0A_full,
    empty => FIFO_L0A_empty,
    wr_rst_busy => wr_rst_busy,
    rd_rst_busy => rd_rst_busy );
    
  pmap_top_memory_19DCT : top_memory_19DCT port map (
    clock => clock,
    data_in => data_in,
    read_BCID => read_BCID,
    SL_BCID => SL_BCID,
    FIFO_L0A_empty => FIFO_L0A_empty,
    data_out => data_out,
    tm_FIFO_L0A_read_en => FIFO_L0A_read_en );



trig_BCID_gen : process(clock)
begin
    if rising_edge(clock) then
        trig_BCID <= SL_BCID(8 downto 0) - std_logic_vector(to_unsigned(400, trig_BCID'length));
    end if;
end process;


FIFO_L0A_write_en_gen : process(clock)
begin
    if rising_edge(clock) then
        FIFO_L0A_write_en <= L0A;
    end if;
end process;



end Behavioral;
