-- project: SLReadout
-- top_readout.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
--use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

--library work;
--use work.all;




entity top_readout is
  Port ( clock : in std_logic;
         bcid_BM_L     : in std_logic_vector(11 downto 0);
         bcid_BM_R     : in std_logic_vector(11 downto 0);
         bcid_BI       : in std_logic_vector(11 downto 0);
         reset : in std_logic;
         L0A : in std_logic;
         BM_L_in : in array_slv27to0x19DCT;
         BM_R_in : in array_slv27to0x19DCT;
         BI_in : in array_slv27to0x10DCT;
         BM_L_out : out std_logic_vector(31 downto 0);
         BM_R_out : out std_logic_vector(31 downto 0);
         BI_out : out std_logic_vector(31 downto 0) );
end top_readout;


architecture Behavioral of top_readout is

component BM
  Port ( clock : in std_logic;
         SL_bcid : in std_logic_vector(11 downto 0);
         reset : in std_logic;
         L0A : in std_logic;
         data_in : in array_slv27to0x19DCT;
         data_out : out std_logic_vector(31 downto 0) );
end component;

component BI
  Port ( clock : in std_logic;
         SL_bcid : in std_logic_vector(11 downto 0);
         reset : in std_logic;
         L0A : in std_logic;
         data_in : in array_slv27to0x10DCT;
         data_out : out std_logic_vector(31 downto 0) );
end component;


begin
  pmap_BM_L : BM port map (
    clock => clock,
    SL_bcid => bcid_BM_L,
    reset => reset,
    L0A => L0A,
    data_in => BM_L_in,
    data_out => BM_L_out );
    
  pmap_BM_R : BM port map (
    clock => clock,
    SL_bcid => bcid_BM_R,
    reset => reset,
    L0A => L0A,
    data_in => BM_R_in,
    data_out => BM_R_out );
    
  pmap_BI : BI port map (
    clock => clock,
    SL_bcid => bcid_BI,
    reset => reset,
    L0A => L0A,
    data_in => BI_in,
    data_out => BI_out );
    
end Behavioral;
