-- project: SLReadout
-- my_library.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package my_library is
    
    type array_slv3to0x512 is array (511 downto 0) of std_logic_vector(3 downto 0);
    type array_slv3to0x512x19 is array (18 downto 0) of array_slv3to0x512;
    type array_slv3to0x512x10 is array (9 downto 0) of array_slv3to0x512;

    type array2_slv3to0x512x19DCT is array (18 downto 0) of array_slv3to0x512;
    type array_slv31to0x19DCT is array (18 downto 0) of std_logic_vector(31 downto 0);
    type array_slv27to0x19DCT is array (18 downto 0) of std_logic_vector(27 downto 0); 
    type array_slv12to0x19DCT is array (18 downto 0) of std_logic_vector(12 downto 0);
    type array_slv3to0x19DCT is array (18 downto 0) of std_logic_vector(3 downto 0);
    type array_slx19DCT is array (18 downto 0) of std_logic;

    type array2_slv3to0x512x10DCT is array (9 downto 0) of array_slv3to0x512;
    type array_slv31to0x10DCT is array (9 downto 0) of std_logic_vector(31 downto 0);
    type array_slv27to0x10DCT is array (9 downto 0) of std_logic_vector(27 downto 0);
    type array_slv12to0x10DCT is array (9 downto 0) of std_logic_vector(12 downto 0);
    type array_slv3to0x10DCT is array (9 downto 0) of std_logic_vector(3 downto 0);
    type array_slx10DCT is array (9 downto 0) of std_logic;

    type array_slv27to0x48 is array (47 downto 0) of std_logic_vector(27 downto 0);
        
end package;

package body my_library is

end my_library;
