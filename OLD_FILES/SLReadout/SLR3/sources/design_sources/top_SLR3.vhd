library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;


entity SLR3_top is
    Port (
    
        mgtrefclk0_x1y12_p  : std_logic;
        mgtrefclk0_x1y12_n  : std_logic;
        
         -- Serial data ports for transceiver channel 0
        ch0_gtyrxn_in_TDAQ                   : in std_logic;
        ch0_gtyrxp_in_TDAQ                   : in std_logic;
        ch0_gtytxn_out_TDAQ                  : out std_logic;
        ch0_gtytxp_out_TDAQ                  : out std_logic;
        
       -- Serial data ports for transceiver channel 1
        ch1_gtyrxn_in_TDAQ                   : in std_logic;
        ch1_gtyrxp_in_TDAQ                   : in std_logic;
        ch1_gtytxn_out_TDAQ                  : out std_logic;
        ch1_gtytxp_out_TDAQ                  : out std_logic;
        
        -- Serial data ports for transceiver channel 2
        ch2_gtyrxn_in_TDAQ                   : in std_logic;
        ch2_gtyrxp_in_TDAQ                   : in std_logic;
        ch2_gtytxn_out_TDAQ                  : out std_logic;
        ch2_gtytxp_out_TDAQ                  : out std_logic;
        
        txctrl2_SLR3_in : in std_logic_vector(23 downto 0);
        
        -- User-provided ports for reset helper block(s)
        hb_gtwiz_reset_clk_freerun_in_SLR3 : in std_logic;
        --hb_gtwiz_reset_clk_freerun_in_n_SLR3 : in std_logic;
        hb_gtwiz_reset_all_in_SLR3           : in std_logic;
       
        word_28_SLR0_in : in array_20x28b;
        word_28_SLR1_in : in array_10x28b;
        word_28_SLR2_in : in array_20x28b;
        
        clock_320_in    : in std_logic;
        clock_240_in    : in std_logic;
        clock_80_in     : in std_logic;
        clock_40_in     : in std_logic;
        L0_acc          : in std_logic;
        reset           : in std_logic;
        
        write_en : in std_logic; --debug
        debug : out std_logic  --debug
    );
end SLR3_top;

architecture RTL of SLR3_top is
            

    component fifo_32b_cdc_320to240MHz is
      port (
        wr_clk : in std_logic;
        rd_clk : in std_logic;
        din    : in std_logic_vector(31 downto 0);
        wr_en  : in std_logic;
        rd_en  : in std_logic;
        dout   : out std_logic_vector(31 downto 0);
        full   : out std_logic;
        empty  : out std_logic
      );
    end component;
    
   component top_readout is
   Port ( 
         clock : in std_logic;
         bcid_BM_L     : in std_logic_vector(11 downto 0);
         bcid_BM_R     : in std_logic_vector(11 downto 0);
         bcid_BI       : in std_logic_vector(11 downto 0);
         reset         : in std_logic;
         L0A           : in std_logic;
         BM_L_in       : in array_slv27to0x19DCT;
         BM_R_in       : in array_slv27to0x19DCT;
         BI_in         : in array_slv27to0x10DCT;
         BM_L_out      : out std_logic_vector(31 downto 0);
         BM_R_out      : out std_logic_vector(31 downto 0);
         BI_out        : out std_logic_vector(31 downto 0) 
   );
   end component;
   
   COMPONENT fifo_16x28b is
        port (
            wr_clk   : IN STD_LOGIC;
            rd_clk   : IN STD_LOGIC;
            din   : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout  : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
            full  : OUT STD_LOGIC;
            empty : OUT STD_LOGIC
        );
    end COMPONENT;

    signal gtyrxn_int_TDAQ        : std_logic_vector(2 downto 0);
    signal gtyrxp_int_TDAQ        : std_logic_vector(2 downto 0);
    signal gtytxn_int_TDAQ        : std_logic_vector(2 downto 0);
    signal gtytxp_int_TDAQ        : std_logic_vector(2 downto 0);
    
    signal gtwiz_userclk_tx_reset_int_TDAQ             : std_logic;
    signal hb0_gtwiz_userclk_tx_reset_int_TDAQ         : std_logic; 
    signal gtwiz_userclk_tx_usrclk2_int_TDAQ           : std_logic;
    signal gtwiz_userclk_tx_active_int_TDAQ            : std_logic;
    signal gtwiz_userclk_rx_reset_int_TDAQ             : std_logic;
    signal hb0_gtwiz_userclk_rx_reset_int_TDAQ         : std_logic;
    signal gtwiz_userclk_rx_usrclk2_int_TDAQ           : std_logic;
    signal gtwiz_reset_rx_done_int_TDAQ                : std_logic;
    
    signal gtwiz_userdata_tx_int_TDAQ                  : std_logic_vector (95 downto 0);
    signal gtwiz_userdata_rx_int_TDAQ                  : std_logic_vector (95 downto 0);
    
    signal gtrefclk00_int_TDAQ            : std_logic;
    signal cm0_gtrefclk00_int_TDAQ        : std_logic;
    
    signal qpll0outclk_int_TDAQ           : std_logic;
    signal cm0_qpll0outclk_int_TDAQ       : std_logic;
    
    signal qpll0outrefclk_int_TDAQ        : std_logic;
    signal cm0_qpll0outrefclk_int_TDAQ    : std_logic;
    
    signal gtpowergood_int_TDAQ           : std_logic_vector(2 downto 0);
    signal rxpmaresetdone_int_TDAQ        : std_logic_vector(2 downto 0);  
    signal txpmaresetdone_int_TDAQ        : std_logic_vector(2 downto 0);
    
    signal hb_gtwiz_reset_clk_freerun_buf_int_SLR3     : std_logic;
    signal hb_gtwiz_reset_all_buf_int_SLR3             : std_logic;
    signal hb_gtwiz_reset_all_int_SLR3                 : std_logic;
    
    signal mgtrefclk0_x1y12_int : std_logic;
    
    signal en_8b10b                       : std_logic_vector(2 downto 0);
    signal txctrl                         : std_logic_vector(47 downto 0);  
    signal reset_gt_int_SLR3              : std_logic;

    signal word_28_SLR0    : array_slv27to0x19DCT;
    signal word_28_SLR1    : array_slv27to0x10DCT;
    signal word_28_SLR2    : array_slv27to0x19DCT;
    
    signal word_28_SLR0_tmp :  array_20x28b;
    signal word_28_SLR1_tmp :  array_10x28b;
    signal word_28_SLR2_tmp :  array_20x28b;
    
    signal vettore_debug : std_logic_vector(95 downto 0);

    signal readout_data_SLR0             : std_logic_vector(31 downto 0);
    signal readout_data_SLR0_r           : std_logic_vector(31 downto 0);
    signal readout_data_SLR1             : std_logic_vector(31 downto 0);
    signal readout_data_SLR1_r           : std_logic_vector(31 downto 0);
    signal readout_data_SLR2             : std_logic_vector(31 downto 0);
    signal readout_data_SLR2_r           : std_logic_vector(31 downto 0);
    
    
    signal cdc_readout_write_enable_SLR0 : std_logic;
    signal cdc_readout_read_enable_SLR0  : std_logic;  
    signal cdc_readout_empty_SLR0        : std_logic;
    signal cdc_readout_write_enable_SLR1 : std_logic;
    signal cdc_readout_read_enable_SLR1  : std_logic;  
    signal cdc_readout_empty_SLR1        : std_logic;
    signal cdc_readout_write_enable_SLR2 : std_logic;
    signal cdc_readout_read_enable_SLR2  : std_logic;  
    signal cdc_readout_empty_SLR2        : std_logic;
    
    signal bcid : std_logic_vector(11 downto 0);
    signal bcid_to_BM_L : std_logic_vector(11 downto 0);
    signal bcid_to_BM_R : std_logic_vector(11 downto 0);
    signal bcid_to_BI   : std_logic_vector(11 downto 0);
    
    
begin

    gtyrxn_int_TDAQ(0) <= ch0_gtyrxn_in_TDAQ;
    gtyrxn_int_TDAQ(1) <= ch1_gtyrxn_in_TDAQ;
    gtyrxn_int_TDAQ(2) <= ch2_gtyrxn_in_TDAQ;
    
    gtyrxp_int_TDAQ(0) <= ch0_gtyrxp_in_TDAQ;
    gtyrxp_int_TDAQ(1) <= ch1_gtyrxp_in_TDAQ;
    gtyrxp_int_TDAQ(2) <= ch2_gtyrxp_in_TDAQ;
    
    ch0_gtytxn_out_TDAQ <= gtytxn_int_TDAQ(0);
    ch1_gtytxn_out_TDAQ <= gtytxn_int_TDAQ(1);
    ch2_gtytxn_out_TDAQ <= gtytxn_int_TDAQ(2);
    
    ch0_gtytxp_out_TDAQ <= gtytxp_int_TDAQ(0);
    ch1_gtytxp_out_TDAQ <= gtytxp_int_TDAQ(1);
    ch2_gtytxp_out_TDAQ <= gtytxp_int_TDAQ(2);
    
    gtrefclk00_int_TDAQ <= cm0_gtrefclk00_int_TDAQ;
    cm0_qpll0outclk_int_TDAQ <= qpll0outclk_int_TDAQ;
    cm0_qpll0outrefclk_int_TDAQ <= qpll0outrefclk_int_TDAQ;
    
    en_8b10b <= "111";
    txctrl   <= (others=>'0'); 
    
    
    -- ===================================================================================================================
    -- BUFFERS
    -- ===================================================================================================================
 
    buf_hb_gtwiz_reset_all_inst_SLR3 : IBUF
    port map (
        I => hb_gtwiz_reset_all_in_SLR3,
        O => hb_gtwiz_reset_all_buf_int_SLR3
    );

    hb_gtwiz_reset_all_int_SLR3 <= hb_gtwiz_reset_all_buf_int_SLR3;
    
    bufg_clk_freerun_inst_SLR3 : BUFG
    port map ( 
        I   => hb_gtwiz_reset_clk_freerun_in_SLR3,
        O   => hb_gtwiz_reset_clk_freerun_buf_int_SLR3
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X1Y12_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x1y12_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x1y12_p,         
      IB => mgtrefclk0_x1y12_n        
    );
    
    cm0_gtrefclk00_int_TDAQ <= mgtrefclk0_x1y12_int;
    hb0_gtwiz_userclk_tx_reset_int_TDAQ <= not (and_reduce(txpmaresetdone_int_TDAQ));

    reset_gt_int_SLR3 <= '0';
        
    GT_TDAQ_wrapper_inst: entity work.GT_TDAQ_SLR3_example_wrapper   
 port map 
 ( 
   gtyrxn_in                               => gtyrxn_int_TDAQ,
   gtyrxp_in                               => gtyrxp_int_TDAQ,
   gtytxn_out                              => gtytxn_int_TDAQ,
   gtytxp_out                              => gtytxp_int_TDAQ,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_TDAQ,
   gtwiz_userclk_tx_srcclk_out             => open,
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => gtwiz_userclk_tx_usrclk2_int_TDAQ,
   gtwiz_userclk_tx_active_out             => gtwiz_userclk_tx_active_int_TDAQ,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_TDAQ,
   gtwiz_userclk_rx_srcclk_out             => open,
   gtwiz_userclk_rx_usrclk_out             => open,
   gtwiz_userclk_rx_usrclk2_out            => gtwiz_userclk_rx_usrclk2_int_TDAQ,
   gtwiz_userclk_rx_active_out             => open,
   gtwiz_reset_clk_freerun_in              => hb_gtwiz_reset_clk_freerun_buf_int_SLR3,
   gtwiz_reset_all_in                      => hb_gtwiz_reset_all_int_SLR3,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR3,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR3,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR3,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR3,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open,
   gtwiz_reset_tx_done_out                 => open,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_TDAQ,
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_TDAQ,
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_TDAQ,
   gtrefclk01_in                           => gtrefclk00_int_TDAQ,
   qpll1outclk_out                         => qpll0outclk_int_TDAQ,
   qpll1outrefclk_out                      => qpll0outrefclk_int_TDAQ,
   rx8b10ben_in                            => en_8b10b,
   tx8b10ben_in                            => en_8b10b,
   txctrl0_in                              => txctrl,
   txctrl1_in                              => txctrl,
   txctrl2_in                              => txctrl2_SLR3_in,
   gtpowergood_out                         => gtpowergood_int_TDAQ,
   rxctrl0_out                             => open,
   rxctrl1_out                             => open,
   rxctrl2_out                             => open,
   rxctrl3_out                             => open,
   rxpmaresetdone_out                      => rxpmaresetdone_int_TDAQ,
   txpmaresetdone_out                      => txpmaresetdone_int_TDAQ,
   clock_240_in                            => clock_240_in
);

    GEN_fifo_readout_SLR0 : for k in 0 to 19 generate
        fifo_readout_SLR0 : fifo_16x28b
        port map(
            wr_clk   => clock_320_in,
            rd_clk => clock_240_in,
            din   => word_28_SLR0_in(k), 
            wr_en => '1', 
            rd_en => '1', 
            dout  => word_28_SLR0_tmp(k),
            full  => open,
            empty => open
        );
    end generate;
    
    GEN_fifo_readout_SLR1 : for k in 0 to 9 generate
        fifo_readout_SLR1 : fifo_16x28b
        port map(
            wr_clk   => clock_320_in,
            rd_clk   => clock_240_in,
            din   => word_28_SLR1_in(k), 
            wr_en => '1', 
            rd_en => '1', 
            dout  => word_28_SLR1_tmp(k),
            full  => open,
            empty => open
        );
    end generate;
    
    GEN_fifo_readout_SLR2 : for k in 0 to 19 generate
        fifo_readout_SLR2 : fifo_16x28b
        port map(
            wr_clk   => clock_320_in,
            rd_clk   => clock_240_in,
            din   => word_28_SLR2_in(k), 
            wr_en => '1', 
            rd_en => '1', 
            dout  => word_28_SLR2_tmp(k),
            full  => open,
            empty => open
        );
    end generate;
    
    
--    process(clock_320_in) 
--    begin
--        if rising_edge(clock_320_in) then 
            word_28_SLR0 <= array_slv27to0x19DCT(word_28_SLR0_tmp(18 downto 0));
            word_28_SLR1 <= array_slv27to0x10DCT(word_28_SLR1_tmp);
            word_28_SLR2 <= array_slv27to0x19DCT(word_28_SLR2_tmp(18 downto 0));
--        end if;
--    end process;

 bcid_to_BM_L <= "000" & word_28_SLR0(0)(18 downto 10); -- PROBLEM: here need to add the three most significant bits of the SL counter (instead of the three null bits)
 bcid_to_BM_R <= "000" & word_28_SLR2(0)(18 downto 10);
 bcid_to_BI <= "000" & word_28_SLR1(0)(18 downto 10);
        
    ----- debug ----
--    process(clock_320_in)
--    begin 
--        if rising_edge(clock_320_in) then
--            if write_en = '1' then
--                for i in 0 to 18 loop
--                    vettore_debug(27+28*i downto 28*i) <= word_28_SLR0(i);
--                    vettore_debug(27+28*i+560 downto 28*i+560) <= word_28_SLR2(i);    
--                end loop;
--                for i in 0 to 9 loop
--                    vettore_debug(27+28*i+1120 downto 28*i+1120) <= word_28_SLR1(i);
--                end loop;
--            else 
--                vettore_debug <= vettore_debug(1398 downto 0) & '0';
--            end if;
--            readout_data_SLR0 <= vettore_debug(31 downto 0);
--            readout_data_SLR1 <= vettore_debug(63 downto 32);
--            readout_data_SLR2 <= vettore_debug(95 downto 64);
            
--        end if;
--    end process;
--    debug <= vettore_debug(1399);
------------ fine debug -----------------------

 
    top_readout_inst : top_readout 
    Port map( 
         clock => clock_240_in,
         bcid_BM_L    => bcid_to_BM_L,
         bcid_BM_R    => bcid_to_BM_R,
         bcid_BI      => bcid_to_BI,
         reset        => reset,
         L0A          => L0_acc,
         BM_L_in      => word_28_SLR0,
         BM_R_in      => word_28_SLR2,
         BI_in        => word_28_SLR1,
         BM_L_out     => readout_data_SLR0,
         BM_R_out     => readout_data_SLR2,
         BI_out       => readout_data_SLR1 
    );
    
    not_remove_logic : process(clock_240_in) -- to avoid dont_touch (which is not working for ununderstood reasons)
    begin
        if rising_edge(clock_240_in) then
            if write_en = '1' then
                vettore_debug <= readout_data_SLR0 & readout_data_SLR1 & readout_data_SLR2;
            else
                vettore_debug <= vettore_debug(94 downto 0) & '0';
            end if;
        end if;
    end process;
    
    debug <= vettore_debug(95);

    
    
    gtwiz_userdata_tx_int_TDAQ(31 downto 0) <= readout_data_SLR0;
    gtwiz_userdata_tx_int_TDAQ(63 downto 32) <= readout_data_SLR1;
    gtwiz_userdata_tx_int_TDAQ(95 downto 64) <= readout_data_SLR2;
    
    

    
end RTL;

