library IEEE;
use IEEE.STD_LOGIC_1164.all;

package my_lib is
    
    type array_3x480b    is array (2 downto 0)   of std_logic_vector(479 downto 0);
    type array_3x320b    is array (2 downto 0)   of std_logic_vector(319 downto 0);
    type array_10x128b   is array (9 downto 0)   of std_logic_vector(127 downto 0);
    type array_3x10x128b is array (2 downto 0)   of array_10x128b;
    type array_2x384b    is array (1 downto 0)   of std_logic_vector(383 downto 0);
    type array_2x448b    is array (1 downto 0)   of std_logic_vector(447 downto 0);
    type array_6x121b    is array (5 downto 0)   of std_logic_vector(120 downto 0);
    type array_7x121b    is array (6 downto 0)   of std_logic_vector(120 downto 0);
    type array_2x6x121b  is array (1 downto 0)   of array_6x121b;
    type array_2x7x121b  is array (1 downto 0)   of array_7x121b;
    type array_4x128b    is array (3 downto 0)   of std_logic_vector(127 downto 0);
    type array_4x32b     is array (3 downto 0)   of std_logic_vector(31 downto 0);
    type array_288x7b    is array (287 downto 0) of std_logic_vector(6 downto 0);
    type array_3x48b     is array (2 downto 0)   of std_logic_vector(47 downto 0);
    type array_3x128b    is array (2 downto 0)   of std_logic_vector(127 downto 0);
    type array_20x32b    is array (19 downto 0)  of std_logic_vector(31 downto 0);
    type array_20x230b   is array (19 downto 0)  of std_logic_vector(229 downto 0);
    type array_20x2b     is array (19 downto 0)  of std_logic_vector(1 downto 0);
    type array_20x3b     is array (19 downto 0)  of std_logic_vector(2 downto 0);
    type array_20x28b    is array (19 downto 0)  of std_logic_vector(27 downto 0);
    type array_20x144b   is array (19 downto 0)  of std_logic_vector(143 downto 0);
    type array_20x12b    is array (19 downto 0)  of std_logic_vector(11 downto 0);  
    type array_10x32b    is array (9 downto 0)   of std_logic_vector(31 downto 0);  
    type array_10x2b     is array (9 downto 0)   of std_logic_vector(1 downto 0); 
    type array_10x230b   is array (9 downto 0)   of std_logic_vector(231 downto 0); 
    type array_10x28b    is array (9 downto 0)   of std_logic_vector(27 downto 0); 
    type array_10x48b    is array (9 downto 0)   of std_logic_vector(47 downto 0);
    type array_10x12b    is array (9 downto 0)   of std_logic_vector(11 downto 0);  
    
    type phi_chamber_extended_BI  is array (0 to 9) of std_logic_vector(127 downto 0);
    type phi_chamber_extended_BM1 is array (0 to 5) of std_logic_vector(80+40 downto 0);
    type phi_chamber_extended_BM2 is array (0 to 6) of std_logic_vector(80+40 downto 0);
    type phi_chamber_extended_BO  is array (0 to 5) of std_logic_vector(80+40 downto 0); 
    
    subtype integer_0to16   is integer range 0 to 16; 
    subtype integer_0to511  is integer range 0 to 511;
    subtype integer_0to3563 is integer range 0 to 3563;  
    subtype integer_0to127  is integer range 0 to 127; 

end package;



package body my_lib is

end my_lib;