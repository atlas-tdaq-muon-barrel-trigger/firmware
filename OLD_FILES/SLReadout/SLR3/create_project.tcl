#set projDir /data/gpadovan/vivado_projects/working_dir/firmware/SLReadout/SLR3
#set projDir /home/mbauce/firmware/SLReadout

set projDir [pwd]
create_project SL $projDir/project -part xcvu13p-flga2577-1-e
set_property target_language VHDL [current_project]


add_files -norecurse $projDir/sources/design_sources/wrapper_top_SLR3.vhd
add_files -norecurse $projDir/sources/design_sources/top_SLR3.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/BI.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/BM.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/memory_block.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/multiplexer_512to1.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/multiplexer_dOut_10DCTto1.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/multiplexer_dOut_19DCTto1.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/top_memory_10DCT.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/top_memory_19DCT.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/top_readout.vhd
#top_readout

add_files -norecurse $projDir/sources/design_sources/my_lib.vhd
add_files -norecurse $projDir/sources/design_sources/readout_logic/RTL/my_library.vhd

add_files -norecurse $projDir/sources/design_sources/readout_logic/IP/FIFO_L0A/FIFO_L0A.xci
add_files -norecurse $projDir/sources/design_sources/readout_logic/IP/RAM_32bx8192/RAM_32bx8192.xci
add_files -norecurse $projDir/sources/ip/clk_multiplier/clk_multiplier.xci
add_files -norecurse $projDir/sources/ip/fifo_16x28b/fifo_16x28b.xci
add_files -norecurse $projDir/sources/ip/fifo_32b_cdc_320to240MHz/fifo_32b_cdc_320to240MHz.xci
add_files -norecurse $projDir/sources/ip/GT_TDAQ_SLR3/GT_TDAQ_SLR3.xci
add_files -norecurse $projDir/sources/ip/GT_TDAQ_SLR3/GT_TDAQ_SLR3_example_wrapper.v
add_files -norecurse $projDir/sources/ip/GT_TDAQ_SLR3/GT_TDAQ_SLR3_example_gtwiz_userclk_rx.v
add_files -norecurse $projDir/sources/ip/GT_TDAQ_SLR3/GT_TDAQ_SLR3_example_gtwiz_userclk_tx.v
add_files -norecurse $projDir/sources/ip/GT_TDAQ_SLR3/GT_TDAQ_SLR3_example_wrapper_functions.v

set_property file_type {Verilog Header} [get_files /data/gpadovan/vivado_projects/working_dir/firmware/SLReadout/SLR3/sources/ip/GT_TDAQ_SLR3/GT_TDAQ_SLR3_example_wrapper_functions.v]

add_files -fileset constrs_1 -norecurse $projDir/sources/design_sources/readout_logic/constr/constraints.xdc

set_property SOURCE_SET sources_1 [get_filesets sim_1]

add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/data_generator_20DCT.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/data_generator.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/BM_data_generator.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/BI_data_generator.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/hit_generator.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/L0A_generator.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/tb_top_readout.vhd
add_files -fileset sim_1 -norecurse $projDir/sources/design_sources/readout_logic/sim/tb_SLR3_top.vhd


# optimized synthesis and implementation strategies to avoid timing failures [decomment to activate]
set_property strategy Flow_PerfThresholdCarry [get_runs synth_1]
set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

