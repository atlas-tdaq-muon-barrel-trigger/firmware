to run:

- type `vivado`
- in the vivado command line type `source create_project.tcl`
- run the simulation. You will get an error beacuse the input file with the MC simulated data is not present. But this run will create the folder where to store it, so it is a necessary step with current version of the pipeline.
- after project creation you have to copy locally the MC input file, which is currently stored in my eos space: `scp gpadovan@lxplus.cern.ch:/afs/cern.ch/user/g/gpadovan/eos/www/work_TriggerPhaseII/dataMC_MassimoCorradi/output_2p39ms.v2.txt ./SL/SL.sim/sim_1/behav/xsim/`

Now you can safely run simulation, synthesis, implementation.

