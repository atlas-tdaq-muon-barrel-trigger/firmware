# SLReadout

This folder contains a VHDL simulation for the readout functionality of the Sector Logic.

Three implementations are available for $10$, $20$, $n$ DCTs, which are stored in the respective directories. You can change to the directory of your interest and follow the instructions in the readme.

-------------------

Authors: G. Padovano, M. Bauce, R. Vari