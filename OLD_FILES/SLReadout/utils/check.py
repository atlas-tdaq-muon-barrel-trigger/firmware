# check.py
# Script to check if the putput triggers from the readout system are correponding to data that entered the SL readout in a preceeding time


fname_in_signals = "./data/output_2p39ms.v2.txt" #"my_new_file.txt" #
fname_out_signals = "./data/output_signals.v2.txt"


def get_muon_data(ind, triggered_indexes, lines_out):
    pos = triggered_indexes.index(ind)
    
    BM_L = []
    BM_R = []
    BI = []


    ind_next_L0A = triggered_indexes[pos+1]    
    
    for (i,line) in enumerate(lines_out):
        if ind <= i and i <= ind_next_L0A:
            if line[3] != "0000000":
                BM_L.append(line[3].lower())
            if line[4] != "0000000":
                BM_R.append(line[4].lower())
            if line[5] != "0000000":
                BI.append(line[5].lower())
    
    return BM_L, BM_R, BI


def build_trigger_list(lines_out):
    
    triggered_indexes = []
    for (i,line) in enumerate(lines_out):
        L0A = line[0]
        if L0A == '1':
            triggered_indexes.append(i)
        
    trigger_list = []
    for ind in triggered_indexes:
        if ind != triggered_indexes[-1]:
            trigger = dict()
            trigger["index"] = ind
            trigger["L0A"] = lines_out[ind][0]
            trigger["SL_BCID"] = lines_out[ind][1]
            trigger["trig_BCID"] = lines_out[ind+1][2]
            trigger["BM_L"], trigger["BM_R"], trigger["BI"] = get_muon_data(ind, triggered_indexes, lines_out)
            trigger_list.append(trigger)
    
    return trigger_list

def get_BCID(word):
    
    bin_word = bin(int(word,16))
    bin_word = list(bin_word)
    bin_word.pop(0)
    bin_word.pop(0)
    
    while len(bin_word)!=28:
        bin_word.insert(0,'0')

    BCID_l = bin_word[9:18]
    BCID = ""
    for b in BCID_l:
        BCID = BCID + b
    BCID_hex = list(hex(int(BCID,2)))
        
    BCID_hex.pop(0)
    BCID_hex.pop(0)
    
    BCID = ""
    for b in BCID_hex:
        BCID = BCID + b
        
    while len(BCID)!=3:
        BCID = '0'+BCID
    
    return BCID
    
def build_chamber_keys():
    chamb_groups = ["BM_L", "BM_R", "BI"]
    n_chamb = dict()
    n_chamb["BM_L"] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
                       '10', '11', '12', '13', '14', '15', '16', '17', '18']
    n_chamb["BM_R"] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
                       '10', '11', '12', '13', '14', '15', '16', '17', '18']
    n_chamb["BI"] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    chamb_keys = []
    for chamb_group in chamb_groups:
        for n in n_chamb[chamb_group]:
            chamb_keys.append(chamb_group + "_" + n)

    return chamb_groups, n_chamb, chamb_keys

def get_chamber_key(j):
    
    mapping = dict()
    mapping['0'] = "BM_L_0"
    mapping['1'] = "BM_L_1"
    mapping['2'] = "BM_L_2"
    mapping['3'] = "BM_L_3"
    mapping['4'] = "BM_L_4"
    mapping['5'] = "BM_L_5"
    mapping['6'] = "BM_L_6"
    mapping['7'] = "BM_L_7"
    mapping['8'] = "BM_L_8"
    mapping['9'] = "BM_L_9"
    mapping['10'] = "BM_L_10"
    mapping['11'] = "BM_L_11"
    mapping['12'] = "BM_L_12"
    mapping['13'] = "BM_L_13"
    mapping['14'] = "BM_L_14"
    mapping['15'] = "BM_L_15"
    mapping['16'] = "BM_L_16"
    mapping['17'] = "BM_L_17"
    mapping['18'] = "BM_L_18"
    mapping['19'] = "BM_R_0"
    mapping['20'] = "BM_R_1"
    mapping['21'] = "BM_R_2"
    mapping['22'] = "BM_R_3"
    mapping['23'] = "BM_R_4"
    mapping['24'] = "BM_R_5"
    mapping['25'] = "BM_R_6"
    mapping['26'] = "BM_R_7"
    mapping['27'] = "BM_R_8"
    mapping['28'] = "BM_R_9"
    mapping['29'] = "BM_R_10"
    mapping['30'] = "BM_R_11"
    mapping['31'] = "BM_R_12"
    mapping['32'] = "BM_R_13"
    mapping['33'] = "BM_R_14"
    mapping['34'] = "BM_R_15"
    mapping['35'] = "BM_R_16"
    mapping['36'] = "BM_R_17"
    mapping['37'] = "BM_R_18"
    mapping['38'] = "BI_0"
    mapping['39'] = "BI_1"
    mapping['40'] = "BI_2"
    mapping['41'] = "BI_3"
    mapping['42'] = "BI_4"
    mapping['43'] = "BI_5"
    mapping['44'] = "BI_6"
    mapping['45'] = "BI_7"
    mapping['46'] = "BI_8"
    mapping['47'] = "BI_9"
    
    return mapping[str(j)]
    
def look_for_data(trigger, lines_in):

    index = trigger["index"]
    L0A = trigger["L0A"]
    SL_BCID = trigger["SL_BCID"]
    trig_BCID = trigger["trig_BCID"].lower()
    BM_L = [ s.lower() for s in trigger["BM_L"] ]
    BM_R = [ s.lower() for s in trigger["BM_R"] ]
    BI = [ s.lower() for s in trigger["BI"] ]

    in_data = dict()
    in_data["BCID"] = trig_BCID
    
    chamb_groups, n_chamb, chamb_keys = build_chamber_keys()
    
    for k in chamb_keys:
        in_data[k] = []
        
    """
    x = 600
    if (index-x) > 0:
        i=index-x
    else:
        i=0
    """
    i = 0
    while i<index:
        line = lines_in[i]
        i+=1
    #for (i,line) in enumerate(lines_in):
        for (j,word) in enumerate(line):
            BCID = get_BCID(word)
            if BCID == trig_BCID:                
                key = get_chamber_key(j)

                in_data[key].append(word)
                
    return in_data

def check_matching(trig, in_data):
    
    chamb_groups, n_chamb, chamb_keys = build_chamber_keys()

    out = dict()
    for chamb_group in chamb_groups:
        out[chamb_group] = []
    
    for chamb_group in chamb_groups:
        for n in n_chamb[chamb_group]:
            out[chamb_group] = out[chamb_group] + in_data[chamb_group+"_"+n]
    
    n_success = 0
    for chamb_group in chamb_groups:
        fail = 0
        for w in trig[chamb_group]:
            if w not in out[chamb_group]:
                fail = 1
        if fail == 0:
            n_success += 1
        #if out[chamb_group] == trig[chamb_group]:
        #    n_success += 1

    if n_success == 3:
        return 1
    else:
        return 0

            
def main():

    # read file with iput data to the SL (the triggerable data)
    data_file_in = open(fname_in_signals, 'r')
    lines_in = data_file_in.readlines()
    lines_in = [line.strip().split(" ") for line in lines_in]
    data_file_in.close()
    
    # read file with output data from the SL (the triggered data)
    data_file_out = open(fname_out_signals)
    lines_out = data_file_out.readlines()
    lines_out = [line.strip().split(" ") for line in lines_out]
    data_file_out.close()

    
    trigger_list = build_trigger_list(lines_out)
    
    # control all triggers in the list
    n_good = 0
    for trig in trigger_list:
        print(">>>>>>>>>>>>>>>>>>>>>")
        print("trig: {0}".format(trig))
        in_data = look_for_data(trig, lines_in)
        print("in_data: {0}".format(in_data))
        result = check_matching(trig, in_data)
        n_good += result
        print("result: {0}".format(result))
    
    
    print("--------------------------------------------")
    print("Summary:")
    print("n tot triggers: {0}".format(len(trigger_list)))
    print("n good checks:  {0}".format(n_good))
    print("--------------------------------------------")
    
    return 0

if __name__ == "__main__": main()

