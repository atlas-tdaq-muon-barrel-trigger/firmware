# plot_occupancy.py

import numpy as np
import matplotlib.pyplot as plt



def do_plot(j, lines, chamber):
    y = []
    for i,line in enumerate(lines):
        words = line.split(" ")
        word = words[j]
    
        if i == 0:
            pass
        elif i == 1:
            pass
        else:
            word = int(word,2)
            y.append(word)

    x = list(range(len(y)))

    print("chamber: {0} memory block {1}".format(chamber, j))

    plt.plot(x,y, 'ro') 
    
    plt.xlabel('clock counts (at 320 MHz)')
    plt.ylabel('RAM slice occupancy [max. 15]')
    plt.title("{0} memory block {1}".format(chamber, j))
    
    plt.savefig("/afs/cern.ch/user/g/gpadovan/eos/www/work_TriggerPhaseII/{0}_m_block_{1}.png".format(chamber, j))
    plt.savefig("/afs/cern.ch/user/g/gpadovan/eos/www/work_TriggerPhaseII/{0}_m_block_{1}.pdf".format(chamber, j))
    plt.clf()



chambers = ["BM_L_8", "BM_R_7", "BI_3"]

for chamber in chambers:
    f = open("./data/"+chamber+"_counters_data.txt")
    lines = f.readlines()

    j = 0
    #do_plot(j, lines, chamber)
    for j in range(512):
        do_plot(j, lines, chamber)


