# SL interfaces

This repository contains the VHDL projects with the logic needed by the SL board FPGA (XCVU13P-FLGA2577-1-E) to interface with the RPC DCTs. The logic in the `lpGBT-FPGA_core` subdirectory is provided by CERN.

### Contents:
* `SL_51_GT` contains the logic to instantiate 51 transceivers to interface with 51 RPC DCTs. 51 lpGBT-FPGA core blocks are implemented to allow the data transmission to and from the DCTs. The 51 GTs are distributed on the 4 Super Logic Regions (SLR) that compose the FPGA.
* `SL_20_GT` contains the logic to instantiate 20 transceivers to interface with 20 RPC DCTs. 20 lpGBT-FPGA core blocks are implemented as well as the logic to pass the RPC decoded data to the readout and trigger logic blocks. All the 20 GTs are placed on the same SuperLogic Region (SLR0) of the FPGA. 
