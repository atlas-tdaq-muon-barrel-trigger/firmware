library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


entity SLR0_top is
    Port (
        -- Differential reference clock inputs SLR0
        mgtrefclk0_x0y2_p               : in std_logic;
        mgtrefclk0_x0y2_n               : in std_logic;
        mgtrefclk0_x1y0_p               : in std_logic;
        mgtrefclk0_x1y0_n               : in std_logic;
        
        
        -- Serial data ports for transceiver channel 0
        ch0_gtyrxn_in_SLR0                   : in std_logic;
        ch0_gtyrxp_in_SLR0                   : in std_logic;
        ch0_gtytxn_out_SLR0                  : out std_logic;
        ch0_gtytxp_out_SLR0                  : out std_logic;
        
       -- Serial data ports for transceiver channel 1
        ch1_gtyrxn_in_SLR0                   : in std_logic;
        ch1_gtyrxp_in_SLR0                   : in std_logic;
        ch1_gtytxn_out_SLR0                  : out std_logic;
        ch1_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 2
        ch2_gtyrxn_in_SLR0                   : in std_logic;
        ch2_gtyrxp_in_SLR0                   : in std_logic;
        ch2_gtytxn_out_SLR0                  : out std_logic;
        ch2_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 3
        ch3_gtyrxn_in_SLR0                   : in std_logic;
        ch3_gtyrxp_in_SLR0                   : in std_logic;
        ch3_gtytxn_out_SLR0                  : out std_logic;
        ch3_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 4
        ch4_gtyrxn_in_SLR0                   : in std_logic;
        ch4_gtyrxp_in_SLR0                   : in std_logic;
        ch4_gtytxn_out_SLR0                  : out std_logic;
        ch4_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 5
        ch5_gtyrxn_in_SLR0                   : in std_logic;
        ch5_gtyrxp_in_SLR0                   : in std_logic;
        ch5_gtytxn_out_SLR0                  : out std_logic;
        ch5_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 6
        ch6_gtyrxn_in_SLR0                   : in std_logic;
        ch6_gtyrxp_in_SLR0                   : in std_logic;
        ch6_gtytxn_out_SLR0                  : out std_logic;
        ch6_gtytxp_out_SLR0                  : out std_logic;
  
        -- Serial data ports for transceiver channel 7
        ch7_gtyrxn_in_SLR0                   : in std_logic;
        ch7_gtyrxp_in_SLR0                   : in std_logic;
        ch7_gtytxn_out_SLR0                  : out std_logic;
        ch7_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 8
        ch8_gtyrxn_in_SLR0                   : in std_logic;
        ch8_gtyrxp_in_SLR0                   : in std_logic;
        ch8_gtytxn_out_SLR0                  : out std_logic;
        ch8_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 9
        ch9_gtyrxn_in_SLR0                   : in std_logic;
        ch9_gtyrxp_in_SLR0                   : in std_logic;
        ch9_gtytxn_out_SLR0                  : out std_logic;
        ch9_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 10
        ch10_gtyrxn_in_SLR0                   : in std_logic;
        ch10_gtyrxp_in_SLR0                   : in std_logic;
        ch10_gtytxn_out_SLR0                  : out std_logic;
        ch10_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 11
        ch11_gtyrxn_in_SLR0                   : in std_logic;
        ch11_gtyrxp_in_SLR0                   : in std_logic;
        ch11_gtytxn_out_SLR0                  : out std_logic;
        ch11_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 12
        ch12_gtyrxn_in_SLR0                   : in std_logic;
        ch12_gtyrxp_in_SLR0                   : in std_logic;
        ch12_gtytxn_out_SLR0                  : out std_logic;
        ch12_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 13
        ch13_gtyrxn_in_SLR0                   : in std_logic;
        ch13_gtyrxp_in_SLR0                   : in std_logic;
        ch13_gtytxn_out_SLR0                  : out std_logic;
        ch13_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 14
        ch14_gtyrxn_in_SLR0                   : in std_logic;
        ch14_gtyrxp_in_SLR0                   : in std_logic;
        ch14_gtytxn_out_SLR0                  : out std_logic;
        ch14_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 15
        ch15_gtyrxn_in_SLR0                   : in std_logic;
        ch15_gtyrxp_in_SLR0                   : in std_logic;
        ch15_gtytxn_out_SLR0                  : out std_logic;
        ch15_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 16
        ch16_gtyrxn_in_SLR0                   : in std_logic;
        ch16_gtyrxp_in_SLR0                   : in std_logic;
        ch16_gtytxn_out_SLR0                  : out std_logic;
        ch16_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 17
        ch17_gtyrxn_in_SLR0                   : in std_logic;
        ch17_gtyrxp_in_SLR0                   : in std_logic;
        ch17_gtytxn_out_SLR0                  : out std_logic;
        ch17_gtytxp_out_SLR0                  : out std_logic;
        
        -- Serial data ports for transceiver channel 18
        ch18_gtyrxn_in_SLR0                   : in std_logic;
        ch18_gtyrxp_in_SLR0                   : in std_logic;
        ch18_gtytxn_out_SLR0                  : out std_logic;
        ch18_gtytxp_out_SLR0                  : out std_logic;
       
        -- Serial data ports for transceiver channel 19
        ch19_gtyrxn_in_SLR0                   : in std_logic;
        ch19_gtyrxp_in_SLR0                   : in std_logic;
        ch19_gtytxn_out_SLR0                  : out std_logic;
        ch19_gtytxp_out_SLR0                  : out std_logic;
        
        
        
        
        -- User-provided ports for reset helper block(s)
        hb_gtwiz_reset_clk_freerun_in_SLR0 : in std_logic;
        --hb_gtwiz_reset_clk_freerun_in_n_SLR0 : in std_logic;
        hb_gtwiz_reset_all_in_SLR0           : in std_logic;
        
        bcid_in  : in std_logic_vector(11 downto 0)
        
    );
end SLR0_top;

architecture RTL of SLR0_top is

    COMPONENT data_handler is 
        port (
            clock_in    : in  std_logic;
            BCID_in     : in  std_logic_vector(11 downto 0);
            uplink_user_data_in : in  std_logic_vector(223 downto 0); 
            uplink_clk_en_in : std_logic;
            data_out    : out std_logic_vector(287 downto 0)
        );
    end COMPONENT;

    

  COMPONENT lpgbtfpga_downlink IS
   GENERIC(
        -- Expert parameters
        c_multicyleDelay              : integer RANGE 0 to 7 := 1; --3                      --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                  : integer := 2;              --8                      --! Clock ratio is clock_out / 40 (shall be an integer - E.g.: 320/40 = 8)
        c_outputWidth                 : integer                                             --! Transceiver's word size (Typically 32 bits)
   );
   PORT (
        -- Clocks
        clk_i                         : in  std_logic;                                      --! Downlink datapath clock (Transceiver Tx User clock, typically 320MHz)
        clkEn_i                       : in  std_logic;                                      --! Clock enable (1 pulse over 8 clock cycles when encoding runs @ 320Mhz)
        rst_n_i                       : in  std_logic;                                      --! Downlink reset SIGNAL (Tx ready from the transceiver)

        -- Down link
        userData_i                    : in  std_logic_vector(31 downto 0);                  --! Downlink data (User)
        ECData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink EC field
        ICData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink IC field

        -- Output
        mgt_word_o                    : out std_logic_vector((c_outputWidth-1) downto 0);   --! Downlink encoded frame (IC + EC + User Data + FEC)

        -- Configuration
        interleaverBypass_i           : in  std_logic;                                      --! Bypass downlink interleaver (test purpose only)
        encoderBypass_i               : in  std_logic;                                      --! Bypass downlink FEC (test purpose only)
        scramblerBypass_i             : in  std_logic;                                      --! Bypass downlink scrambler (test purpose only)

        -- Status
        rdy_o                         : out std_logic                                       --! Downlink ready status
   );
END COMPONENT;  

  COMPONENT lpgbtfpga_uplink IS
   GENERIC(
        -- General configuration
        DATARATE                        : integer RANGE 0 to 2;                               --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             : integer RANGE 0 to 2;                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                : integer RANGE 0 to 7 := 3;                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    : integer;                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  : integer;                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            : integer;                                            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       : integer;                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            : integer;                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                : integer := 1;                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               : integer := 40                                       --! Number of clock cycle required before being back in a stable state
   );
   PORT (
        -- Clock and reset
        uplinkClk_i                     : in  std_logic;                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                : out std_logic;                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   : in  std_logic;                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      : in  std_logic_vector((c_mgtWordWidth-1) downto 0);  --! Input frame coming from the MGT

        -- Data
        userData_o                      : out std_logic_vector(229 downto 0);                 --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        : out std_logic_vector(1 downto 0);                   --! EC field value received from the LpGBT
        IcData_o                        : out std_logic_vector(1 downto 0);                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             : in  std_logic;                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              : in  std_logic;                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               : in  std_logic;                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               : out std_logic;                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 : out std_logic_vector(229 downto 0);                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           : out std_logic;                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              : out std_logic                                       --! Number of bit slip is even (required only for advanced applications)

   );
END COMPONENT;


  signal hb_gtwiz_reset_clk_freerun_SLR0  : std_logic;  


  signal gtyrxn_int_SLR0         : std_logic_vector(19 downto 0);
  signal gtyrxp_int_SLR0         : std_logic_vector(19 downto 0);
  signal gtytxn_int_SLR0         : std_logic_vector(19 downto 0);
  signal gtytxp_int_SLR0         : std_logic_vector(19 downto 0);

  signal gtwiz_userclk_tx_reset_int_SLR0              : std_logic;
  signal hb0_gtwiz_userclk_tx_reset_int_SLR0          : std_logic; 
  signal gtwiz_userclk_tx_srcclk_int_SLR0             : std_logic;
  signal gtwiz_userclk_tx_usrclk_int_SLR0             : std_logic;
  signal gtwiz_userclk_tx_usrclk2_int_SLR0            : std_logic;
  signal gtwiz_userclk_tx_active_int_SLR0             : std_logic;
  signal gtwiz_userclk_rx_reset_int_SLR0              : std_logic;
  signal hb0_gtwiz_userclk_rx_reset_int_SLR0          : std_logic;
  signal gtwiz_userclk_rx_srcclk_int_SLR0             : std_logic;
  signal gtwiz_userclk_rx_usrclk_int_SLR0             : std_logic;
  signal gtwiz_userclk_rx_usrclk2_int_SLR0            : std_logic;
  signal gtwiz_userclk_rx_active_int_SLR0             : std_logic;
  signal gtwiz_reset_rx_cdr_stable_int_SLR0           : std_logic;
  signal gtwiz_reset_tx_done_int_SLR0                 : std_logic;
  signal gtwiz_reset_rx_done_int_SLR0                 : std_logic;
  signal reset_gt_int_SLR0                            : std_logic;
  
  
  signal hb_gtwiz_reset_clk_freerun_buf_int_SLR0      : std_logic;
  signal mgtrefclk0_x0y0_int_SLR0                     : std_logic;
  signal hb_gtwiz_reset_all_buf_int_SLR0              : std_logic;
  signal hb_gtwiz_reset_all_int_SLR0                 : std_logic;
  
  signal gtwiz_userdata_tx_int_SLR0                   : std_logic_vector (639 downto 0);
  signal gtwiz_userdata_rx_int_SLR0                   : std_logic_vector (639 downto 0);
  

  signal gtrefclk00_int_SLR0             : std_logic_vector (4 downto 0);
  signal cm0_gtrefclk00_int_SLR0         : std_logic;
  signal cm1_gtrefclk00_int_SLR0         : std_logic;
  signal cm2_gtrefclk00_int_SLR0         : std_logic;
  signal cm3_gtrefclk00_int_SLR0         : std_logic;
  signal cm4_gtrefclk00_int_SLR0         : std_logic;
  
  
  signal qpll0outclk_int_SLR0            : std_logic_vector (4 downto 0);
  signal cm0_qpll0outclk_int_SLR0        : std_logic;
  signal cm1_qpll0outclk_int_SLR0        : std_logic;
  signal cm2_qpll0outclk_int_SLR0        : std_logic;
  signal cm3_qpll0outclk_int_SLR0        : std_logic;
  signal cm4_qpll0outclk_int_SLR0        : std_logic;
  
  
  signal qpll0outrefclk_int_SLR0         : std_logic_vector(4 downto 0);
  signal cm0_qpll0outrefclk_int_SLR0     : std_logic;
  signal cm1_qpll0outrefclk_int_SLR0     : std_logic;
  signal cm2_qpll0outrefclk_int_SLR0     : std_logic;
  signal cm3_qpll0outrefclk_int_SLR0     : std_logic;
  signal cm4_qpll0outrefclk_int_SLR0     : std_logic;

  
  signal rxslide_int_SLR0                : std_logic_vector(19 downto 0);
  signal gtpowergood_int_SLR0            : std_logic_vector(19 downto 0);
  signal rxpmaresetdone_int_SLR0         : std_logic_vector(19 downto 0);  
  signal txpmaresetdone_int_SLR0         : std_logic_vector(19 downto 0);
  
 
  signal mgtrefclk0_x0y2_int : std_logic;
  signal mgtrefclk0_x1y0_int : std_logic;
  
  signal downlink_clock_en_gen_SLR0          : std_logic;
  signal downlink_user_data_gen_SLR0         : std_logic_vector(31 downto 0);
  signal downlink_ec_data_gen_SLR0           : std_logic_vector(1 downto 0);
  signal downlink_ic_data_gen_SLR0          : std_logic_vector(1 downto 0);
  
  
  type downlink_user_data_array_SLR0 is array(0 to 19) of std_logic_vector(31 downto 0);
  type uplink_user_data_array_SLR0 is array(0 to 19) of std_logic_vector(229 downto 0);
  type ic_ec_data_array_SLR0 is array(0 to 19) of std_logic_vector(1 downto 0);
  
  signal downlink_clock_en_SLR0          : std_logic_vector(19 downto 0);
  signal downlink_user_data_SLR0         : downlink_user_data_array_SLR0;
  signal downlink_ec_data_SLR0           : ic_ec_data_array_SLR0;
  signal downlink_ic_data_SLR0           : ic_ec_data_array_SLR0;
  signal downlink_ready_SLR0             : std_logic_vector(19 downto 0);
  
  signal uplink_user_data_SLR0           : uplink_user_data_array_SLR0;
  signal uplink_ic_data_SLR0             : ic_ec_data_array_SLR0;
  signal uplink_ec_data_SLR0             : ic_ec_data_array_SLR0; 
  signal uplink_dataCorrected_SLR0       : uplink_user_data_array_SLR0;
  signal uplink_IcCorrected_SLR0         : ic_ec_data_array_SLR0;
  signal uplink_EcCorrected_SLR0         : ic_ec_data_array_SLR0; 
  signal uplink_rdy_i_SLR0               : std_logic_vector(19 downto 0);
  signal uplink_clk_en_i_SLR0            : std_logic_vector(19 downto 0);
  
  
  type divide_counter_array_SLR0 is array(0 to 19) of std_logic_vector(2 downto 0);
  type output_word_28_array_SLR0 is array(0 to 19) of std_logic_vector(27 downto 0);
  type decoded_frame_224_array_SLR0 is array(0 to 19) of std_logic_vector(223 downto 0);  
  signal divide_counter_int_SLR0         : divide_counter_array_SLR0;
  signal output_word_28_int_SLR0         : output_word_28_array_SLR0;      
  signal decoded_frame_224_int_SLR0      : decoded_frame_224_array_SLR0;
  
  type bcid_ordered_data_array_SLR0 is array(0 to 19) of std_logic_vector(287 downto 0);
  signal bcid_ordered_data_SLR0 : bcid_ordered_data_array_SLR0;
  
  
  
  attribute DONT_TOUCH : string;
  --attribute DONT_TOUCH of output_word_28_int_SLR0 : signal is "TRUE";
  --attribute DONT_TOUCH of uplink_user_data_SLR0 : signal is "TRUE";
  attribute DONT_TOUCH of bcid_ordered_data_SLR0 : signal is "TRUE";
  
  type array_eta_hits is array(8 downto 0) of std_logic_vector(511 downto 0);
  signal eta_hits_layer : array_eta_hits := (others=> (others=>'0'));
  type array_7x121b is array(6 downto 0) of std_logic_vector(120 downto 0); --BM2
  type array_6x121b is array(5 downto 0) of std_logic_vector(120 downto 0); --BM1 and BO
  type array_10x121b is array(9 downto 0) of std_logic_vector(120 downto 0); --BI

  signal  phi_hits_l0 : array_10x121b;
  signal  phi_hits_l1 : array_10x121b;
  signal  phi_hits_l2 : array_10x121b;
  signal  phi_hits_l3 : array_6x121b;
  signal  phi_hits_l4 : array_6x121b;
  signal  phi_hits_l5 : array_7x121b;
  signal  phi_hits_l6 : array_7x121b;
  signal  phi_hits_l7 : array_6x121b;
  signal  phi_hits_l8 : array_6x121b;

  begin
    gtyrxn_int_SLR0(0) <= ch0_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(1) <= ch1_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(2) <= ch2_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(3) <= ch3_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(4) <= ch4_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(5) <= ch5_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(6) <= ch6_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(7) <= ch7_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(8) <= ch8_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(9) <= ch9_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(10) <= ch10_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(11) <= ch11_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(12) <= ch12_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(13) <= ch13_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(14) <= ch14_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(15) <= ch15_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(16) <= ch16_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(17) <= ch17_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(18) <= ch18_gtyrxn_in_SLR0;
    gtyrxn_int_SLR0(19) <= ch19_gtyrxn_in_SLR0;
        
    gtyrxp_int_SLR0(0) <= ch0_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(1) <= ch1_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(2) <= ch2_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(3) <= ch3_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(4) <= ch4_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(5) <= ch5_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(6) <= ch6_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(7) <= ch7_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(8) <= ch8_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(9) <= ch9_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(10) <= ch10_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(11) <= ch11_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(12) <= ch12_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(13) <= ch13_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(14) <= ch14_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(15) <= ch15_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(16) <= ch16_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(17) <= ch17_gtyrxp_in_SLR0;
    gtyrxp_int_SLR0(19) <= ch19_gtyrxp_in_SLR0;
    
   
    ch0_gtytxn_out_SLR0 <= gtytxn_int_SLR0(0);
    ch1_gtytxn_out_SLR0 <= gtytxn_int_SLR0(1);
    ch2_gtytxn_out_SLR0 <= gtytxn_int_SLR0(2);
    ch3_gtytxn_out_SLR0 <= gtytxn_int_SLR0(3);
    ch4_gtytxn_out_SLR0 <= gtytxn_int_SLR0(4);
    ch5_gtytxn_out_SLR0 <= gtytxn_int_SLR0(5);
    ch6_gtytxn_out_SLR0 <= gtytxn_int_SLR0(6);
    ch7_gtytxn_out_SLR0 <= gtytxn_int_SLR0(7);
    ch8_gtytxn_out_SLR0 <= gtytxn_int_SLR0(8);
    ch9_gtytxn_out_SLR0 <= gtytxn_int_SLR0(9);
    ch10_gtytxn_out_SLR0 <= gtytxn_int_SLR0(10);
    ch11_gtytxn_out_SLR0 <= gtytxn_int_SLR0(11);
    ch12_gtytxn_out_SLR0 <= gtytxn_int_SLR0(12);
    ch13_gtytxn_out_SLR0 <= gtytxn_int_SLR0(13);
    ch14_gtytxn_out_SLR0 <= gtytxn_int_SLR0(14);
    ch15_gtytxn_out_SLR0 <= gtytxn_int_SLR0(15);
    ch16_gtytxn_out_SLR0 <= gtytxn_int_SLR0(16);
    ch17_gtytxn_out_SLR0 <= gtytxn_int_SLR0(17);
    ch18_gtytxn_out_SLR0 <= gtytxn_int_SLR0(18);
    ch19_gtytxn_out_SLR0 <= gtytxn_int_SLR0(19);
    

    ch0_gtytxp_out_SLR0 <= gtytxp_int_SLR0(0);
    ch1_gtytxp_out_SLR0 <= gtytxp_int_SLR0(1);
    ch2_gtytxp_out_SLR0 <= gtytxp_int_SLR0(2);
    ch3_gtytxp_out_SLR0 <= gtytxp_int_SLR0(3);
    ch4_gtytxp_out_SLR0 <= gtytxp_int_SLR0(4);
    ch5_gtytxp_out_SLR0 <= gtytxp_int_SLR0(5);
    ch6_gtytxp_out_SLR0 <= gtytxp_int_SLR0(6);
    ch7_gtytxp_out_SLR0 <= gtytxp_int_SLR0(7);
    ch8_gtytxp_out_SLR0 <= gtytxp_int_SLR0(8);
    ch9_gtytxp_out_SLR0 <= gtytxp_int_SLR0(9);
    ch10_gtytxp_out_SLR0 <= gtytxp_int_SLR0(10);
    ch11_gtytxp_out_SLR0 <= gtytxp_int_SLR0(11);
    ch12_gtytxp_out_SLR0 <= gtytxp_int_SLR0(12);
    ch13_gtytxp_out_SLR0 <= gtytxp_int_SLR0(13);
    ch14_gtytxp_out_SLR0 <= gtytxp_int_SLR0(14);
    ch15_gtytxp_out_SLR0 <= gtytxp_int_SLR0(15);
    ch16_gtytxp_out_SLR0 <= gtytxp_int_SLR0(16);
    ch17_gtytxp_out_SLR0 <= gtytxp_int_SLR0(17);
    ch18_gtytxp_out_SLR0 <= gtytxp_int_SLR0(18);
    ch19_gtytxp_out_SLR0 <= gtytxp_int_SLR0(19);
    
    
    gtrefclk00_int_SLR0(0) <= cm0_gtrefclk00_int_SLR0;
    gtrefclk00_int_SLR0(1) <= cm1_gtrefclk00_int_SLR0;
    gtrefclk00_int_SLR0(2) <= cm2_gtrefclk00_int_SLR0;
    gtrefclk00_int_SLR0(3) <= cm3_gtrefclk00_int_SLR0;
    gtrefclk00_int_SLR0(4) <= cm4_gtrefclk00_int_SLR0;
    cm0_qpll0outclk_int_SLR0 <= qpll0outclk_int_SLR0(0);
    cm1_qpll0outclk_int_SLR0 <= qpll0outclk_int_SLR0(1);
    cm2_qpll0outclk_int_SLR0 <= qpll0outclk_int_SLR0(2);
    cm3_qpll0outclk_int_SLR0 <= qpll0outclk_int_SLR0(3);
    cm4_qpll0outclk_int_SLR0 <= qpll0outclk_int_SLR0(4);
    
   
    cm0_qpll0outrefclk_int_SLR0 <= qpll0outrefclk_int_SLR0(0);
    cm1_qpll0outrefclk_int_SLR0 <= qpll0outrefclk_int_SLR0(1);
    cm2_qpll0outrefclk_int_SLR0 <= qpll0outrefclk_int_SLR0(2);
    cm3_qpll0outrefclk_int_SLR0 <= qpll0outrefclk_int_SLR0(3);
    cm4_qpll0outrefclk_int_SLR0 <= qpll0outrefclk_int_SLR0(4);
    
    -- ===================================================================================================================
    -- BUFFERS
    -- ===================================================================================================================
 
    
    buf_hb_gtwiz_reset_all_inst_SLR0 : IBUF
    port map (
        I => hb_gtwiz_reset_all_in_SLR0,
        O => hb_gtwiz_reset_all_buf_int_SLR0
    );

    hb_gtwiz_reset_all_int_SLR0 <= hb_gtwiz_reset_all_buf_int_SLR0;
    
    

    bufg_clk_freerun_inst_SLR0 : BUFG
    port map ( 
        --I   => hb_gtwiz_reset_clk_freerun_SLR0,
        I   => hb_gtwiz_reset_clk_freerun_in_SLR0,
        O   => hb_gtwiz_reset_clk_freerun_buf_int_SLR0
    );
    
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y2_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y2_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y2_p,         
      IB => mgtrefclk0_x0y2_n        
    );
    
    
    IBUFDS_GTE4_MGTREFCLK0_X1Y0_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x1y0_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x1y0_p,         
      IB => mgtrefclk0_x1y0_n        
    );
    

  
    cm0_gtrefclk00_int_SLR0 <= mgtrefclk0_x0y2_int;
    cm1_gtrefclk00_int_SLR0 <= mgtrefclk0_x0y2_int;
    cm2_gtrefclk00_int_SLR0 <= mgtrefclk0_x0y2_int;
    cm3_gtrefclk00_int_SLR0 <= mgtrefclk0_x0y2_int;
    cm4_gtrefclk00_int_SLR0 <= mgtrefclk0_x1y0_int;
    
   
    hb0_gtwiz_userclk_tx_reset_int_SLR0 <= not (and_reduce(txpmaresetdone_int_SLR0));
    hb0_gtwiz_userclk_rx_reset_int_SLR0 <= not (and_reduce(rxpmaresetdone_int_SLR0));
    
    reset_gt_int_SLR0 <= '0';
   
 GT_SLR0_wrapper_inst: entity work.GT_SLR0_example_wrapper
 port map
 (
   gtyrxn_in                               => gtyrxn_int_SLR0,
   gtyrxp_in                               => gtyrxp_int_SLR0,
   gtytxn_out                              => gtytxn_int_SLR0,
   gtytxp_out                              => gtytxp_int_SLR0,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_SLR0,
   gtwiz_userclk_tx_srcclk_out             => gtwiz_userclk_tx_srcclk_int_SLR0,
   gtwiz_userclk_tx_usrclk_out             => gtwiz_userclk_tx_usrclk_int_SLR0,
   gtwiz_userclk_tx_usrclk2_out            => gtwiz_userclk_tx_usrclk2_int_SLR0,
   gtwiz_userclk_tx_active_out             => gtwiz_userclk_tx_active_int_SLR0,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_SLR0,
   gtwiz_userclk_rx_srcclk_out             => gtwiz_userclk_rx_srcclk_int_SLR0,
   gtwiz_userclk_rx_usrclk_out             => gtwiz_userclk_rx_usrclk_int_SLR0,
   gtwiz_userclk_rx_usrclk2_out            => gtwiz_userclk_rx_usrclk2_int_SLR0,
   gtwiz_userclk_rx_active_out             => gtwiz_userclk_rx_active_int_SLR0,
   gtwiz_reset_clk_freerun_in              => hb_gtwiz_reset_clk_freerun_buf_int_SLR0,
   gtwiz_reset_all_in                      => hb_gtwiz_reset_all_int_SLR0,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR0,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => gtwiz_reset_rx_cdr_stable_int_SLR0,
   gtwiz_reset_tx_done_out                 => gtwiz_reset_tx_done_int_SLR0,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_SLR0,
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_SLR0,
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_SLR0,
   gtrefclk00_in                           => gtrefclk00_int_SLR0,
   qpll0outclk_out                         => qpll0outclk_int_SLR0,
   qpll0outrefclk_out                      => qpll0outrefclk_int_SLR0,
   rxslide_in                              => rxslide_int_SLR0,
   gtpowergood_out                         => gtpowergood_int_SLR0,
   rxpmaresetdone_out                      => rxpmaresetdone_int_SLR0,
   txpmaresetdone_out                      => txpmaresetdone_int_SLR0
);
 

GEN_DOWNLINK_SLR0: for i in 0 to 19 generate
downlink_inst_SLR0 : lpgbtfpga_downlink
    generic map (
        c_multicyleDelay => 1,
        c_clockRatio     => 2,
        c_outputWidth    => 32
    )
    port map (
        clk_i                 => gtwiz_userclk_tx_usrclk2_int_SLR0,
        clkEn_i               => downlink_clock_en_SLR0(i),
        rst_n_i               => txpmaresetdone_int_SLR0(i), -- active low reset
        userData_i            => downlink_user_data_SLR0(i),
        ECData_i              => downlink_ec_data_SLR0(i),
        ICData_i              => downlink_ic_data_SLR0(i),
        mgt_word_o            => gtwiz_userdata_tx_int_SLR0(31+32*i downto 32*i),
        interleaverBypass_i   => '0',
        encoderBypass_i       => '0',
        scramblerBypass_i     => '0',
        rdy_o                 => downlink_ready_SLR0(i)
    );
 end generate;
 
 
 GEN_UPLINK_SLR0: for i in 0 to 19 generate
   uplink_inst_SLR0 : lpgbtfpga_uplink 
   generic map(
        -- General configuration
        DATARATE                        => 2,                             --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             => 1,                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                => 3,                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    => 8,                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  => 32,                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            => 5,                                             --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       => 64,                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            => 30,                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                => 1,                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               => 40                                       --! Number of clock cycle required before being back in a stable state
   )
   port map (
        -- Clock and reset
        uplinkClk_i                     => gtwiz_userclk_rx_usrclk2_int_SLR0,                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                => uplink_clk_en_i_SLR0(i),                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   =>  gtwiz_reset_rx_done_int_SLR0,                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      => gtwiz_userdata_rx_int_SLR0(31+32*i downto 32*i),  --! Input frame coming from the MGT

        -- Data
        userData_o                      => uplink_user_data_SLR0(i),              --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        => uplink_ec_data_SLR0(i),                  --! EC field value received from the LpGBT
        IcData_o                        => uplink_ic_data_SLR0(i),                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             => '0',                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              => '0',                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               => '0',                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               => rxslide_int_SLR0(i),                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 => uplink_dataCorrected_SLR0(i),                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   => uplink_IcCorrected_SLR0(i),                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   => uplink_EcCorrected_SLR0(i),                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           => uplink_rdy_i_SLR0(i),                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              => open                                       --! Number of bit slip is even (required only for advanced applications)

   );
   end generate;
   
   
   
   downlink_data_gen_SLR0 : process(gtwiz_userclk_tx_usrclk2_int_SLR0) -- 80 MHz
    begin
        if rising_edge(gtwiz_userclk_tx_usrclk2_int_SLR0) then
            if gtwiz_userclk_tx_active_int_SLR0 = '0' then
                downlink_user_data_gen_SLR0 <= (others => '0');
                downlink_ec_data_gen_SLR0 <= (others => '0');
                downlink_ic_data_gen_SLR0 <= (others => '0');
                downlink_clock_en_gen_SLR0 <= '0';
            else
                    downlink_clock_en_gen_SLR0 <= not downlink_clock_en_gen_SLR0;
                    if downlink_clock_en_gen_SLR0 = '0' then
                        downlink_user_data_gen_SLR0 <= downlink_user_data_gen_SLR0 + 1;
                        downlink_ec_data_gen_SLR0 <= downlink_ec_data_gen_SLR0 + 1;
                        downlink_ic_data_gen_SLR0 <= downlink_ic_data_gen_SLR0 + 1;
                    end if;
            end if;
        end if;
    end process;
    
 
 DOWNLINK_ASSIGNMENT_SLR0: for i in 0 to 19 generate
    downlink_user_data_SLR0(i) <= downlink_user_data_gen_SLR0;
    downlink_ec_data_SLR0(i) <= downlink_ec_data_gen_SLR0;
    downlink_ic_data_SLR0(i) <= downlink_ic_data_gen_SLR0;
    downlink_clock_en_SLR0(i) <= downlink_clock_en_gen_SLR0;
 end generate;
 


  
    GEN_data_handler: for i in 0 to 19 generate
        data_handler_inst_SLR0 : data_handler
            port map(
                clock_in    => gtwiz_userclk_rx_usrclk2_int_SLR0,
                BCID_in     => bcid_in,
                uplink_user_data_in => uplink_user_data_SLR0(i)(223 downto 0), 
                uplink_clk_en_in => uplink_clk_en_i_SLR0(i),
                data_out    => bcid_ordered_data_SLR0(i)
            );
    end generate;
    
    
    data_assignment : process(bcid_ordered_data_SLR0)
    begin
        eta_hits_layer(3)(383 downto 0) <= bcid_ordered_data_SLR0(5)(63 downto 0) & bcid_ordered_data_SLR0(4)(63 downto 0) & bcid_ordered_data_SLR0(3)(63 downto 0) & bcid_ordered_data_SLR0(2)(63 downto 0) & bcid_ordered_data_SLR0(1)(63 downto 0) & bcid_ordered_data_SLR0(0)(63 downto 0);
        eta_hits_layer(4)(383 downto 0) <= bcid_ordered_data_SLR0(5)(127 downto 64) & bcid_ordered_data_SLR0(4)(127 downto 64) & bcid_ordered_data_SLR0(3)(127 downto 64) & bcid_ordered_data_SLR0(2)(127 downto 64) & bcid_ordered_data_SLR0(1)(127 downto 64) & bcid_ordered_data_SLR0(0)(127 downto 64);
        
        eta_hits_layer(5)(447 downto 0) <= bcid_ordered_data_SLR0(12)(63 downto 0) & bcid_ordered_data_SLR0(11)(63 downto 0) & bcid_ordered_data_SLR0(10)(63 downto 0) & bcid_ordered_data_SLR0(9)(63 downto 0) & bcid_ordered_data_SLR0(8)(63 downto 0) & bcid_ordered_data_SLR0(7)(63 downto 0) & bcid_ordered_data_SLR0(6)(63 downto 0);
        eta_hits_layer(6)(447 downto 0) <= bcid_ordered_data_SLR0(12)(127 downto 64) & bcid_ordered_data_SLR0(11)(127 downto 64) & bcid_ordered_data_SLR0(10)(127 downto 64) & bcid_ordered_data_SLR0(9)(127 downto 64) & bcid_ordered_data_SLR0(8)(127 downto 64) & bcid_ordered_data_SLR0(7)(127 downto 64) & bcid_ordered_data_SLR0(6)(127 downto 64);
        
        eta_hits_layer(7)(383 downto 0) <= bcid_ordered_data_SLR0(18)(63 downto 0) & bcid_ordered_data_SLR0(17)(63 downto 0) & bcid_ordered_data_SLR0(16)(63 downto 0) & bcid_ordered_data_SLR0(15)(63 downto 0) & bcid_ordered_data_SLR0(14)(63 downto 0) & bcid_ordered_data_SLR0(13)(63 downto 0);
        eta_hits_layer(8)(383 downto 0) <= bcid_ordered_data_SLR0(18)(127 downto 64) & bcid_ordered_data_SLR0(17)(127 downto 64) & bcid_ordered_data_SLR0(16)(127 downto 64) & bcid_ordered_data_SLR0(15)(127 downto 64) & bcid_ordered_data_SLR0(14)(127 downto 64) & bcid_ordered_data_SLR0(13)(127 downto 64);
        
        phi_hits_l3(0)(100 downto 21) <= bcid_ordered_data_SLR0(0)(207 downto 128); 
        phi_hits_l3(1)(100 downto 21) <= bcid_ordered_data_SLR0(1)(207 downto 128); 
        phi_hits_l3(2)(100 downto 21) <= bcid_ordered_data_SLR0(2)(207 downto 128); 
        phi_hits_l3(3)(100 downto 21) <= bcid_ordered_data_SLR0(3)(207 downto 128); 
        phi_hits_l3(4)(100 downto 21) <= bcid_ordered_data_SLR0(4)(207 downto 128); 
        phi_hits_l3(5)(100 downto 21) <= bcid_ordered_data_SLR0(5)(207 downto 128); 

        phi_hits_l4(0)(100 downto 21) <= bcid_ordered_data_SLR0(0)(287 downto 208); 
        phi_hits_l4(1)(100 downto 21) <= bcid_ordered_data_SLR0(1)(287 downto 208); 
        phi_hits_l4(2)(100 downto 21) <= bcid_ordered_data_SLR0(2)(287 downto 208); 
        phi_hits_l4(3)(100 downto 21) <= bcid_ordered_data_SLR0(3)(287 downto 208); 
        phi_hits_l4(4)(100 downto 21) <= bcid_ordered_data_SLR0(4)(287 downto 208);
        phi_hits_l4(5)(100 downto 21) <= bcid_ordered_data_SLR0(5)(287 downto 208);
        
        phi_hits_l5(0)(100 downto 21) <= bcid_ordered_data_SLR0(6)(207 downto 128); 
        phi_hits_l5(1)(100 downto 21) <= bcid_ordered_data_SLR0(7)(207 downto 128); 
        phi_hits_l5(2)(100 downto 21) <= bcid_ordered_data_SLR0(8)(207 downto 128); 
        phi_hits_l5(3)(100 downto 21) <= bcid_ordered_data_SLR0(9)(207 downto 128); 
        phi_hits_l5(4)(100 downto 21) <= bcid_ordered_data_SLR0(10)(207 downto 128); 
        phi_hits_l5(5)(100 downto 21) <= bcid_ordered_data_SLR0(11)(207 downto 128); 
        phi_hits_l5(6)(100 downto 21) <= bcid_ordered_data_SLR0(12)(207 downto 128); 

        phi_hits_l6(0)(100 downto 21) <= bcid_ordered_data_SLR0(6)(287 downto 208); 
        phi_hits_l6(1)(100 downto 21) <= bcid_ordered_data_SLR0(7)(287 downto 208); 
        phi_hits_l6(2)(100 downto 21) <= bcid_ordered_data_SLR0(8)(287 downto 208); 
        phi_hits_l6(3)(100 downto 21) <= bcid_ordered_data_SLR0(9)(287 downto 208); 
        phi_hits_l6(4)(100 downto 21) <= bcid_ordered_data_SLR0(10)(287 downto 208);
        phi_hits_l6(5)(100 downto 21) <= bcid_ordered_data_SLR0(11)(287 downto 208);
        phi_hits_l6(6)(100 downto 21) <= bcid_ordered_data_SLR0(12)(287 downto 208);
        
        phi_hits_l7(0)(100 downto 21) <= bcid_ordered_data_SLR0(13)(207 downto 128); 
        phi_hits_l7(1)(100 downto 21) <= bcid_ordered_data_SLR0(14)(207 downto 128); 
        phi_hits_l7(2)(100 downto 21) <= bcid_ordered_data_SLR0(15)(207 downto 128); 
        phi_hits_l7(3)(100 downto 21) <= bcid_ordered_data_SLR0(16)(207 downto 128); 
        phi_hits_l7(4)(100 downto 21) <= bcid_ordered_data_SLR0(17)(207 downto 128); 
        phi_hits_l7(5)(100 downto 21) <= bcid_ordered_data_SLR0(18)(207 downto 128); 

        phi_hits_l8(0)(100 downto 21) <= bcid_ordered_data_SLR0(13)(287 downto 208); 
        phi_hits_l8(1)(100 downto 21) <= bcid_ordered_data_SLR0(14)(287 downto 208); 
        phi_hits_l8(2)(100 downto 21) <= bcid_ordered_data_SLR0(15)(287 downto 208); 
        phi_hits_l8(3)(100 downto 21) <= bcid_ordered_data_SLR0(16)(287 downto 208); 
        phi_hits_l8(4)(100 downto 21) <= bcid_ordered_data_SLR0(17)(287 downto 208);
        phi_hits_l8(5)(100 downto 21) <= bcid_ordered_data_SLR0(18)(287 downto 208);
       
        
        
    end process;     
end RTL;

