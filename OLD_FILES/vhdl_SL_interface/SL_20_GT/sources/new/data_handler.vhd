library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;





entity data_handler is
    port (
        clock_in    : in  std_logic;
        BCID_in     : in  std_logic_vector(11 downto 0);
        uplink_user_data_in : in  std_logic_vector(223 downto 0); 
        uplink_clk_en_in : std_logic;
        data_out    : out std_logic_vector(287 downto 0)
    );
end data_handler;




architecture RTL of data_handler is

    signal BCID_diff : std_logic_vector(8 downto 0);
    signal BCID_lsb : std_logic;
    signal strip_index : std_logic_vector(8 downto 0);
    
    signal division_counter : std_logic_vector(3 downto 0);
    
    type array_16x288b is array(15 downto 0) of std_logic_vector(287 downto 0);
    signal data_old : array_16x288b;
    
    signal counter : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r1 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r2 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r3 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r4 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r5 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r6 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r7 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r8 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r9 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r10 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r11 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r12 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r13 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r14 : std_logic_vector(3 downto 0) := (others => '0');
    signal counter_r15 : std_logic_vector(3 downto 0) := (others => '0');
    
    signal word_28_int : std_logic_vector(27 downto 0); -- 9-bit stripID + 9-bit BCID + 10-bit hit time

begin
        
    divide_decoded_uplink_frame_SLR0:  for i in 0 to 19 generate
        process(clock_in)
        begin
            if rising_edge(clock_in) then
                if uplink_clk_en_in = '1' then
                    division_counter <= (others => '0');    
                else
                    division_counter <= division_counter + 1;
                end if;                
                word_28_int <= uplink_user_data_in(27+28*(to_integer(unsigned(division_counter))) downto 28*(to_integer(unsigned(division_counter))) );
            end if;
        end process;
    end generate;
    
      
    DCT_sampling_logic : process(clock_in)
    begin
        if rising_edge(clock_in) then
            BCID_lsb <= BCID_in(0);
            BCID_diff <= BCID_in(8 downto 0) - word_28_int(18 downto 10);
            if BCID_in(0) /= BCID_lsb then
                counter <= counter + 1;
                counter_r1 <= counter;
                counter_r2 <= counter_r1;
                counter_r3 <= counter_r2;
                counter_r4 <= counter_r3;
                counter_r5 <= counter_r4;
                counter_r6 <= counter_r5;
                counter_r7 <= counter_r6;
                counter_r8 <= counter_r7;
                counter_r9 <= counter_r8;
                counter_r10 <= counter_r9;
                counter_r11 <= counter_r10;
                counter_r12 <= counter_r11;
                counter_r13 <= counter_r12;
                counter_r14 <= counter_r13;
                counter_r15 <= counter_r14;
                
                data_out <= data_old(to_integer(unsigned(counter_r15)));
                data_old(to_integer(unsigned(counter_r15))) <= (others => '0');
            elsif word_28_int(27 downto 19) < 288 then 
                strip_index <= word_28_int(27 downto 19);
                if BCID_diff = 20 then 
                    data_old(to_integer(unsigned(counter_r15)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 19 then 
                    data_old(to_integer(unsigned(counter_r14)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 18 then 
                    data_old(to_integer(unsigned(counter_r13)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 17 then 
                    data_old(to_integer(unsigned(counter_r12)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 16 then 
                    data_old(to_integer(unsigned(counter_r11)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 15 then 
                    data_old(to_integer(unsigned(counter_r10)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 14 then 
                    data_old(to_integer(unsigned(counter_r9)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 13 then 
                    data_old(to_integer(unsigned(counter_r8)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 12 then 
                    data_old(to_integer(unsigned(counter_r7)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 11 then 
                    data_old(to_integer(unsigned(counter_r6)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 10 then 
                    data_old(to_integer(unsigned(counter_r5)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 9 then 
                    data_old(to_integer(unsigned(counter_r4)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 8 then 
                    data_old(to_integer(unsigned(counter_r3)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 7 then 
                    data_old(to_integer(unsigned(counter_r2)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 6 then 
                    data_old(to_integer(unsigned(counter_r1)))(to_integer(unsigned(strip_index))) <= '1';
                end if;
                if BCID_diff = 5 then 
                    data_old(to_integer(unsigned(counter)))(to_integer(unsigned(strip_index))) <= '1';
                end if; 
            end if;
        end if;
    end process;



    
    
end RTL;
