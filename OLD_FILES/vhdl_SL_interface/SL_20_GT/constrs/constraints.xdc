#set_property package_pin BE28 [get_ports hb_gtwiz_reset_clk_freerun_in_n_SLR0]
#set_property package_pin BD28 [get_ports hb_gtwiz_reset_clk_freerun_in_p_SLR0]
#set_property package_pin BA17 [get_ports hb_gtwiz_reset_clk_freerun_in_n_SLR1]
#set_property package_pin AY17 [get_ports hb_gtwiz_reset_clk_freerun_in_p_SLR1]

set_property package_pin AY40 [get_ports mgtrefclk0_x0y2_n]
set_property package_pin AY39 [get_ports mgtrefclk0_x0y2_p]
set_property package_pin BD12 [get_ports mgtrefclk0_x1y0_n]
set_property package_pin BD13 [get_ports mgtrefclk0_x1y0_p]

create_clock -name clk_freerun_SLR0 -period 12.5 [get_ports hb_gtwiz_reset_clk_freerun_in_p_SLR0]
create_clock -name clk_mgtrefclk0_x0y2_p -period 6.25 [get_ports mgtrefclk0_x0y2_p]
create_clock -name clk_mgtrefclk0_x1y0_p -period 6.25 [get_ports mgtrefclk0_x1y0_p]



