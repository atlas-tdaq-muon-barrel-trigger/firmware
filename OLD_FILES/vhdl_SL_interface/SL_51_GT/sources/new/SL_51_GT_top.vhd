----------------------------------------------------------------------------------
-- Company:  
-- Engineer: 
-- 
-- Create Date: 11/14/2021 10:09:23 PM
-- Design Name: 
-- Module Name: SL_51_GT_top - RTL
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


entity SL_51_GT_top is
    Port (
        -- Differential reference clock inputs
        mgtrefclk0_x0y2_p               : in std_logic;
        mgtrefclk0_x0y2_n               : in std_logic;
        mgtrefclk0_x0y5_p               : in std_logic;
        mgtrefclk0_x0y5_n               : in std_logic;
        mgtrefclk0_x0y7_p               : in std_logic;
        mgtrefclk0_x0y7_n               : in std_logic;
        mgtrefclk0_x0y9_p               : in std_logic;
        mgtrefclk0_x0y9_n               : in std_logic;
        mgtrefclk0_x0y11_p              : in std_logic;
        mgtrefclk0_x0y11_n              : in std_logic;
        mgtrefclk0_x0y12_p              : in std_logic;
        mgtrefclk0_x0y12_n              : in std_logic;
        
        -- Serial data ports for transceiver channel 0
        ch0_gtyrxn_in                   : in std_logic;
        ch0_gtyrxp_in                   : in std_logic;
        ch0_gtytxn_out                  : out std_logic;
        ch0_gtytxp_out                  : out std_logic;
        
       -- Serial data ports for transceiver channel 1
        ch1_gtyrxn_in                   : in std_logic;
        ch1_gtyrxp_in                   : in std_logic;
        ch1_gtytxn_out                  : out std_logic;
        ch1_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 2
        ch2_gtyrxn_in                   : in std_logic;
        ch2_gtyrxp_in                   : in std_logic;
        ch2_gtytxn_out                  : out std_logic;
        ch2_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 3
        ch3_gtyrxn_in                   : in std_logic;
        ch3_gtyrxp_in                   : in std_logic;
        ch3_gtytxn_out                  : out std_logic;
        ch3_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 4
        ch4_gtyrxn_in                   : in std_logic;
        ch4_gtyrxp_in                   : in std_logic;
        ch4_gtytxn_out                  : out std_logic;
        ch4_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 5
        ch5_gtyrxn_in                   : in std_logic;
        ch5_gtyrxp_in                   : in std_logic;
        ch5_gtytxn_out                  : out std_logic;
        ch5_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 6
        ch6_gtyrxn_in                   : in std_logic;
        ch6_gtyrxp_in                   : in std_logic;
        ch6_gtytxn_out                  : out std_logic;
        ch6_gtytxp_out                  : out std_logic;
  
        -- Serial data ports for transceiver channel 7
        ch7_gtyrxn_in                   : in std_logic;
        ch7_gtyrxp_in                   : in std_logic;
        ch7_gtytxn_out                  : out std_logic;
        ch7_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 8
        ch8_gtyrxn_in                   : in std_logic;
        ch8_gtyrxp_in                   : in std_logic;
        ch8_gtytxn_out                  : out std_logic;
        ch8_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 9
        ch9_gtyrxn_in                   : in std_logic;
        ch9_gtyrxp_in                   : in std_logic;
        ch9_gtytxn_out                  : out std_logic;
        ch9_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 10
        ch10_gtyrxn_in                   : in std_logic;
        ch10_gtyrxp_in                   : in std_logic;
        ch10_gtytxn_out                  : out std_logic;
        ch10_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 11
        ch11_gtyrxn_in                   : in std_logic;
        ch11_gtyrxp_in                   : in std_logic;
        ch11_gtytxn_out                  : out std_logic;
        ch11_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 12
        ch12_gtyrxn_in                   : in std_logic;
        ch12_gtyrxp_in                   : in std_logic;
        ch12_gtytxn_out                  : out std_logic;
        ch12_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 13
        ch13_gtyrxn_in                   : in std_logic;
        ch13_gtyrxp_in                   : in std_logic;
        ch13_gtytxn_out                  : out std_logic;
        ch13_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 14
        ch14_gtyrxn_in                   : in std_logic;
        ch14_gtyrxp_in                   : in std_logic;
        ch14_gtytxn_out                  : out std_logic;
        ch14_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 15
        ch15_gtyrxn_in                   : in std_logic;
        ch15_gtyrxp_in                   : in std_logic;
        ch15_gtytxn_out                  : out std_logic;
        ch15_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 16
        ch16_gtyrxn_in                   : in std_logic;
        ch16_gtyrxp_in                   : in std_logic;
        ch16_gtytxn_out                  : out std_logic;
        ch16_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 17
        ch17_gtyrxn_in                   : in std_logic;
        ch17_gtyrxp_in                   : in std_logic;
        ch17_gtytxn_out                  : out std_logic;
        ch17_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 18
        ch18_gtyrxn_in                   : in std_logic;
        ch18_gtyrxp_in                   : in std_logic;
        ch18_gtytxn_out                  : out std_logic;
        ch18_gtytxp_out                  : out std_logic;
       
        -- Serial data ports for transceiver channel 19
        ch19_gtyrxn_in                   : in std_logic;
        ch19_gtyrxp_in                   : in std_logic;
        ch19_gtytxn_out                  : out std_logic;
        ch19_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 20
        ch20_gtyrxn_in                   : in std_logic;
        ch20_gtyrxp_in                   : in std_logic;
        ch20_gtytxn_out                  : out std_logic;
        ch20_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 21
        ch21_gtyrxn_in                   : in std_logic;
        ch21_gtyrxp_in                   : in std_logic;
        ch21_gtytxn_out                  : out std_logic;
        ch21_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 22
        ch22_gtyrxn_in                   : in std_logic;
        ch22_gtyrxp_in                   : in std_logic;
        ch22_gtytxn_out                  : out std_logic;
        ch22_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 23
        ch23_gtyrxn_in                   : in std_logic;
        ch23_gtyrxp_in                   : in std_logic;
        ch23_gtytxn_out                  : out std_logic;
        ch23_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 24
        ch24_gtyrxn_in                   : in std_logic;
        ch24_gtyrxp_in                   : in std_logic;
        ch24_gtytxn_out                  : out std_logic;
        ch24_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 25
        ch25_gtyrxn_in                   : in std_logic;
        ch25_gtyrxp_in                   : in std_logic;
        ch25_gtytxn_out                  : out std_logic;
        ch25_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 26
        ch26_gtyrxn_in                   : in std_logic;
        ch26_gtyrxp_in                   : in std_logic;
        ch26_gtytxn_out                  : out std_logic;
        ch26_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 27
        ch27_gtyrxn_in                   : in std_logic;
        ch27_gtyrxp_in                   : in std_logic;
        ch27_gtytxn_out                  : out std_logic;
        ch27_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 28
        ch28_gtyrxn_in                   : in std_logic;
        ch28_gtyrxp_in                   : in std_logic;
        ch28_gtytxn_out                  : out std_logic;
        ch28_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 29
        ch29_gtyrxn_in                   : in std_logic;
        ch29_gtyrxp_in                   : in std_logic;
        ch29_gtytxn_out                  : out std_logic;
        ch29_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 30
        ch30_gtyrxn_in                   : in std_logic;
        ch30_gtyrxp_in                   : in std_logic;
        ch30_gtytxn_out                  : out std_logic;
        ch30_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 31
        ch31_gtyrxn_in                   : in std_logic;
        ch31_gtyrxp_in                   : in std_logic;
        ch31_gtytxn_out                  : out std_logic;
        ch31_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 32
        ch32_gtyrxn_in                   : in std_logic;
        ch32_gtyrxp_in                   : in std_logic;
        ch32_gtytxn_out                  : out std_logic;
        ch32_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 33
        ch33_gtyrxn_in                   : in std_logic;
        ch33_gtyrxp_in                   : in std_logic;
        ch33_gtytxn_out                  : out std_logic;
        ch33_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 34
        ch34_gtyrxn_in                   : in std_logic;
        ch34_gtyrxp_in                   : in std_logic;
        ch34_gtytxn_out                  : out std_logic;
        ch34_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 35
        ch35_gtyrxn_in                   : in std_logic;
        ch35_gtyrxp_in                   : in std_logic;
        ch35_gtytxn_out                  : out std_logic;
        ch35_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 36
        ch36_gtyrxn_in                   : in std_logic;
        ch36_gtyrxp_in                   : in std_logic;
        ch36_gtytxn_out                  : out std_logic;
        ch36_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 37
        ch37_gtyrxn_in                   : in std_logic;
        ch37_gtyrxp_in                   : in std_logic;
        ch37_gtytxn_out                  : out std_logic;
        ch37_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 38
        ch38_gtyrxn_in                   : in std_logic;
        ch38_gtyrxp_in                   : in std_logic;
        ch38_gtytxn_out                  : out std_logic;
        ch38_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 39
        ch39_gtyrxn_in                   : in std_logic;
        ch39_gtyrxp_in                   : in std_logic;
        ch39_gtytxn_out                  : out std_logic;
        ch39_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 40
        ch40_gtyrxn_in                   : in std_logic;
        ch40_gtyrxp_in                   : in std_logic;
        ch40_gtytxn_out                  : out std_logic;
        ch40_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 41
        ch41_gtyrxn_in                   : in std_logic;
        ch41_gtyrxp_in                   : in std_logic;
        ch41_gtytxn_out                  : out std_logic;
        ch41_gtytxp_out                  : out std_logic;
        
         -- Serial data ports for transceiver channel 42
        ch42_gtyrxn_in                   : in std_logic;
        ch42_gtyrxp_in                   : in std_logic;
        ch42_gtytxn_out                  : out std_logic;
        ch42_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 43
        ch43_gtyrxn_in                   : in std_logic;
        ch43_gtyrxp_in                   : in std_logic;
        ch43_gtytxn_out                  : out std_logic;
        ch43_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 44
        ch44_gtyrxn_in                   : in std_logic;
        ch44_gtyrxp_in                   : in std_logic;
        ch44_gtytxn_out                  : out std_logic;
        ch44_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 45
        ch45_gtyrxn_in                   : in std_logic;
        ch45_gtyrxp_in                   : in std_logic;
        ch45_gtytxn_out                  : out std_logic;
        ch45_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 46
        ch46_gtyrxn_in                   : in std_logic;
        ch46_gtyrxp_in                   : in std_logic;
        ch46_gtytxn_out                  : out std_logic;
        ch46_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 47
        ch47_gtyrxn_in                   : in std_logic;
        ch47_gtyrxp_in                   : in std_logic;
        ch47_gtytxn_out                  : out std_logic;
        ch47_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 48
        ch48_gtyrxn_in                   : in std_logic;
        ch48_gtyrxp_in                   : in std_logic;
        ch48_gtytxn_out                  : out std_logic;
        ch48_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 49
        ch49_gtyrxn_in                   : in std_logic;
        ch49_gtyrxp_in                   : in std_logic;
        ch49_gtytxn_out                  : out std_logic;
        ch49_gtytxp_out                  : out std_logic;
        
        -- Serial data ports for transceiver channel 50
        ch50_gtyrxn_in                   : in std_logic;
        ch50_gtyrxp_in                   : in std_logic;
        ch50_gtytxn_out                  : out std_logic;
        ch50_gtytxp_out                  : out std_logic;
        
        -- User-provided ports for reset helper block(s)
        hb_gtwiz_reset_clk_freerun_in   : in std_logic;
        hb_gtwiz_reset_all_in           : in std_logic;
        
        -- PRBS-based link status ports
        link_down_latched_reset_in      : in std_logic;
        link_status_out                 : out std_logic;
        link_down_latched_out           : out std_logic
    );
end SL_51_GT_top;

architecture RTL of SL_51_GT_top is

  COMPONENT lpgbtfpga_downlink IS
   GENERIC(
        -- Expert parameters
        c_multicyleDelay              : integer RANGE 0 to 7 := 1; --3                      --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                  : integer := 2;              --8                      --! Clock ratio is clock_out / 40 (shall be an integer - E.g.: 320/40 = 8)
        c_outputWidth                 : integer                                             --! Transceiver's word size (Typically 32 bits)
   );
   PORT (
        -- Clocks
        clk_i                         : in  std_logic;                                      --! Downlink datapath clock (Transceiver Tx User clock, typically 320MHz)
        clkEn_i                       : in  std_logic;                                      --! Clock enable (1 pulse over 8 clock cycles when encoding runs @ 320Mhz)
        rst_n_i                       : in  std_logic;                                      --! Downlink reset SIGNAL (Tx ready from the transceiver)

        -- Down link
        userData_i                    : in  std_logic_vector(31 downto 0);                  --! Downlink data (User)
        ECData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink EC field
        ICData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink IC field

        -- Output
        mgt_word_o                    : out std_logic_vector((c_outputWidth-1) downto 0);   --! Downlink encoded frame (IC + EC + User Data + FEC)

        -- Configuration
        interleaverBypass_i           : in  std_logic;                                      --! Bypass downlink interleaver (test purpose only)
        encoderBypass_i               : in  std_logic;                                      --! Bypass downlink FEC (test purpose only)
        scramblerBypass_i             : in  std_logic;                                      --! Bypass downlink scrambler (test purpose only)

        -- Status
        rdy_o                         : out std_logic                                       --! Downlink ready status
   );
END COMPONENT;  

  COMPONENT lpgbtfpga_uplink IS
   GENERIC(
        -- General configuration
        DATARATE                        : integer RANGE 0 to 2;                               --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             : integer RANGE 0 to 2;                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                : integer RANGE 0 to 7 := 3;                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    : integer;                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  : integer;                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            : integer;                                            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       : integer;                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            : integer;                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                : integer := 1;                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               : integer := 40                                       --! Number of clock cycle required before being back in a stable state
   );
   PORT (
        -- Clock and reset
        uplinkClk_i                     : in  std_logic;                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                : out std_logic;                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   : in  std_logic;                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      : in  std_logic_vector((c_mgtWordWidth-1) downto 0);  --! Input frame coming from the MGT

        -- Data
        userData_o                      : out std_logic_vector(229 downto 0);                 --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        : out std_logic_vector(1 downto 0);                   --! EC field value received from the LpGBT
        IcData_o                        : out std_logic_vector(1 downto 0);                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             : in  std_logic;                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              : in  std_logic;                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               : in  std_logic;                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               : out std_logic;                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 : out std_logic_vector(229 downto 0);                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           : out std_logic;                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              : out std_logic                                       --! Number of bit slip is even (required only for advanced applications)

   );
END COMPONENT;


  

  signal gtyrxn_int         : std_logic_vector(50 downto 0);
  signal gtyrxp_int         : std_logic_vector(50 downto 0);
  signal gtytxn_int         : std_logic_vector(50 downto 0);
  signal gtytxp_int         : std_logic_vector(50 downto 0);

  signal gtwiz_userclk_tx_reset_int              : std_logic;
  signal hb0_gtwiz_userclk_tx_reset_int          : std_logic; 
  signal gtwiz_userclk_tx_srcclk_int             : std_logic;
  signal gtwiz_userclk_tx_usrclk_int             : std_logic;
  signal gtwiz_userclk_tx_usrclk2_int            : std_logic;
  signal gtwiz_userclk_tx_active_int             : std_logic;
  signal gtwiz_userclk_rx_reset_int              : std_logic;
  signal hb0_gtwiz_userclk_rx_reset_int          : std_logic;
  signal gtwiz_userclk_rx_srcclk_int             : std_logic;
  signal gtwiz_userclk_rx_usrclk_int             : std_logic;
  signal gtwiz_userclk_rx_usrclk2_int            : std_logic;
  signal gtwiz_userclk_rx_active_int             : std_logic;
  signal gtwiz_reset_rx_cdr_stable_int           : std_logic;
  signal gtwiz_reset_tx_done_int                 : std_logic;
  signal gtwiz_reset_rx_done_int                 : std_logic;
  signal reset_gt_int                            : std_logic;
  
  signal hb_gtwiz_reset_clk_freerun_buf_int      : std_logic;
  signal mgtrefclk0_x0y0_int                     : std_logic;
  signal hb_gtwiz_reset_all_buf_int              : std_logic;
  signal hb_gtwiz_reset_all_int                  : std_logic;
  
  signal gtwiz_userdata_tx_int                   : std_logic_vector (1631 downto 0);
  signal gtwiz_userdata_rx_int                   : std_logic_vector (1631 downto 0);
  

  signal gtrefclk00_int             : std_logic_vector (12 downto 0);
  signal cm0_gtrefclk00_int         : std_logic;
  signal cm1_gtrefclk00_int         : std_logic;
  signal cm2_gtrefclk00_int         : std_logic;
  signal cm3_gtrefclk00_int         : std_logic;
  signal cm4_gtrefclk00_int         : std_logic;
  signal cm5_gtrefclk00_int         : std_logic;
  signal cm6_gtrefclk00_int         : std_logic;
  signal cm7_gtrefclk00_int         : std_logic;
  signal cm8_gtrefclk00_int         : std_logic;
  signal cm9_gtrefclk00_int         : std_logic;
  signal cm10_gtrefclk00_int        : std_logic;
  signal cm11_gtrefclk00_int        : std_logic;
  signal cm12_gtrefclk00_int        : std_logic;
  
  signal qpll0outclk_int            : std_logic_vector (12 downto 0);
  signal cm0_qpll0outclk_int        : std_logic;
  signal cm1_qpll0outclk_int        : std_logic;
  signal cm2_qpll0outclk_int        : std_logic;
  signal cm3_qpll0outclk_int        : std_logic;
  signal cm4_qpll0outclk_int        : std_logic;
  signal cm5_qpll0outclk_int        : std_logic;
  signal cm6_qpll0outclk_int        : std_logic;
  signal cm7_qpll0outclk_int        : std_logic;
  signal cm8_qpll0outclk_int        : std_logic;
  signal cm9_qpll0outclk_int        : std_logic;
  signal cm10_qpll0outclk_int       : std_logic;
  signal cm11_qpll0outclk_int       : std_logic;
  signal cm12_qpll0outclk_int       : std_logic;
  
  signal qpll0outrefclk_int         : std_logic_vector(12 downto 0);
  signal cm0_qpll0outrefclk_int     : std_logic;
  signal cm1_qpll0outrefclk_int     : std_logic;
  signal cm2_qpll0outrefclk_int     : std_logic;
  signal cm3_qpll0outrefclk_int     : std_logic;
  signal cm4_qpll0outrefclk_int     : std_logic;
  signal cm5_qpll0outrefclk_int     : std_logic;
  signal cm6_qpll0outrefclk_int     : std_logic;
  signal cm7_qpll0outrefclk_int     : std_logic;
  signal cm8_qpll0outrefclk_int     : std_logic;
  signal cm9_qpll0outrefclk_int     : std_logic;
  signal cm10_qpll0outrefclk_int    : std_logic;
  signal cm11_qpll0outrefclk_int    : std_logic;
  signal cm12_qpll0outrefclk_int    : std_logic;
  
  signal rxslide_int                : std_logic_vector(50 downto 0);
  signal gtpowergood_int            : std_logic_vector(50 downto 0);
  signal rxpmaresetdone_int         : std_logic_vector(50 downto 0);  
  signal txpmaresetdone_int         : std_logic_vector(50 downto 0);

  
  signal mgtrefclk0_x0y11_int : std_logic;
  signal mgtrefclk0_x0y12_int : std_logic;
  signal mgtrefclk0_x0y2_int  : std_logic;
  signal mgtrefclk0_x0y5_int  : std_logic;
  signal mgtrefclk0_x0y7_int  : std_logic;
  signal mgtrefclk0_x0y9_int  : std_logic;
  
  signal downlink_clock_en_gen          : std_logic;
  signal downlink_user_data_gen         : std_logic_vector(31 downto 0);
  signal downlink_ec_data_gen           : std_logic_vector(1 downto 0);
  signal downlink_ic_data_gen           : std_logic_vector(1 downto 0);
  
  type downlink_user_data_array is array(0 to 50) of std_logic_vector(31 downto 0);
  type uplink_user_data_array is array(0 to 50) of std_logic_vector(229 downto 0);
  type ic_ec_data_array is array(0 to 50) of std_logic_vector(1 downto 0);
  
  signal downlink_clock_en          : std_logic_vector(50 downto 0);
  signal downlink_user_data         : downlink_user_data_array;
  signal downlink_ec_data           : ic_ec_data_array;
  signal downlink_ic_data           : ic_ec_data_array;
  signal downlink_ready             : std_logic_vector(50 downto 0);
  
  signal uplink_user_data           : uplink_user_data_array;
  signal uplink_ic_data             : ic_ec_data_array;
  signal uplink_ec_data             : ic_ec_data_array; 
  signal uplink_dataCorrected       : uplink_user_data_array;
  signal uplink_IcCorrected         : ic_ec_data_array;
  signal uplink_EcCorrected         : ic_ec_data_array; 
  signal uplink_rdy_i               : std_logic_vector(50 downto 0);
  signal uplink_clk_en_i            : std_logic_vector(50 downto 0);
  
  type divide_counter_array is array(0 to 50) of std_logic_vector(2 downto 0);
  type output_word_28_array is array(0 to 50) of std_logic_vector(27 downto 0);
  type decoded_frame_224_array is array(0 to 50) of std_logic_vector(223 downto 0);  
  signal divide_counter_int         : divide_counter_array;
  signal output_word_28_int         : output_word_28_array;      
  signal decoded_frame_224_int      : decoded_frame_224_array;
  
  
  attribute DONT_TOUCH : string;
  --attribute DONT_TOUCH of uplink_user_data : signal is "TRUE";
  attribute DONT_TOUCH of output_word_28_int : signal is "TRUE";

  begin
    gtyrxn_int(0) <= ch0_gtyrxn_in;
    gtyrxn_int(1) <= ch1_gtyrxn_in;
    gtyrxn_int(2) <= ch2_gtyrxn_in;
    gtyrxn_int(3) <= ch3_gtyrxn_in;
    gtyrxn_int(4) <= ch4_gtyrxn_in;
    gtyrxn_int(5) <= ch5_gtyrxn_in;
    gtyrxn_int(6) <= ch6_gtyrxn_in;
    gtyrxn_int(7) <= ch7_gtyrxn_in;
    gtyrxn_int(8) <= ch8_gtyrxn_in;
    gtyrxn_int(9) <= ch9_gtyrxn_in;
    gtyrxn_int(10) <= ch10_gtyrxn_in;
    gtyrxn_int(11) <= ch11_gtyrxn_in;
    gtyrxn_int(12) <= ch12_gtyrxn_in;
    gtyrxn_int(13) <= ch13_gtyrxn_in;
    gtyrxn_int(14) <= ch14_gtyrxn_in;
    gtyrxn_int(15) <= ch15_gtyrxn_in;
    gtyrxn_int(16) <= ch16_gtyrxn_in;
    gtyrxn_int(17) <= ch17_gtyrxn_in;
    gtyrxn_int(18) <= ch18_gtyrxn_in;
    gtyrxn_int(19) <= ch19_gtyrxn_in;
    gtyrxn_int(20) <= ch20_gtyrxn_in;
    gtyrxn_int(21) <= ch21_gtyrxn_in;
    gtyrxn_int(22) <= ch22_gtyrxn_in;
    gtyrxn_int(23) <= ch23_gtyrxn_in;
    gtyrxn_int(24) <= ch24_gtyrxn_in;
    gtyrxn_int(25) <= ch25_gtyrxn_in;
    gtyrxn_int(26) <= ch26_gtyrxn_in;
    gtyrxn_int(27) <= ch27_gtyrxn_in;
    gtyrxn_int(28) <= ch28_gtyrxn_in;
    gtyrxn_int(29) <= ch29_gtyrxn_in;
    gtyrxn_int(30) <= ch30_gtyrxn_in;
    gtyrxn_int(31) <= ch31_gtyrxn_in;
    gtyrxn_int(32) <= ch32_gtyrxn_in;
    gtyrxn_int(33) <= ch33_gtyrxn_in;
    gtyrxn_int(34) <= ch34_gtyrxn_in;
    gtyrxn_int(35) <= ch35_gtyrxn_in;
    gtyrxn_int(36) <= ch36_gtyrxn_in;
    gtyrxn_int(37) <= ch37_gtyrxn_in;
    gtyrxn_int(38) <= ch38_gtyrxn_in;
    gtyrxn_int(39) <= ch39_gtyrxn_in;
    gtyrxn_int(40) <= ch40_gtyrxn_in;
    gtyrxn_int(41) <= ch41_gtyrxn_in;
    gtyrxn_int(42) <= ch42_gtyrxn_in;
    gtyrxn_int(43) <= ch43_gtyrxn_in;
    gtyrxn_int(44) <= ch44_gtyrxn_in;
    gtyrxn_int(45) <= ch45_gtyrxn_in;
    gtyrxn_int(46) <= ch46_gtyrxn_in;
    gtyrxn_int(47) <= ch47_gtyrxn_in;
    gtyrxn_int(48) <= ch48_gtyrxn_in;
    gtyrxn_int(49) <= ch49_gtyrxn_in;
    gtyrxn_int(50) <= ch50_gtyrxn_in;
    
    gtyrxp_int(0) <= ch0_gtyrxp_in;
    gtyrxp_int(1) <= ch1_gtyrxp_in;
    gtyrxp_int(2) <= ch2_gtyrxp_in;
    gtyrxp_int(3) <= ch3_gtyrxp_in;
    gtyrxp_int(4) <= ch4_gtyrxp_in;
    gtyrxp_int(5) <= ch5_gtyrxp_in;
    gtyrxp_int(6) <= ch6_gtyrxp_in;
    gtyrxp_int(7) <= ch7_gtyrxp_in;
    gtyrxp_int(8) <= ch8_gtyrxp_in;
    gtyrxp_int(9) <= ch9_gtyrxp_in;
    gtyrxp_int(10) <= ch10_gtyrxp_in;
    gtyrxp_int(11) <= ch11_gtyrxp_in;
    gtyrxp_int(12) <= ch12_gtyrxp_in;
    gtyrxp_int(13) <= ch13_gtyrxp_in;
    gtyrxp_int(14) <= ch14_gtyrxp_in;
    gtyrxp_int(15) <= ch15_gtyrxp_in;
    gtyrxp_int(16) <= ch16_gtyrxp_in;
    gtyrxp_int(17) <= ch17_gtyrxp_in;
    gtyrxp_int(19) <= ch19_gtyrxp_in;
    gtyrxp_int(20) <= ch20_gtyrxp_in;
    gtyrxp_int(21) <= ch21_gtyrxp_in;
    gtyrxp_int(22) <= ch22_gtyrxp_in;
    gtyrxp_int(23) <= ch23_gtyrxp_in;
    gtyrxp_int(24) <= ch24_gtyrxp_in;
    gtyrxp_int(25) <= ch25_gtyrxp_in;
    gtyrxp_int(26) <= ch26_gtyrxp_in;
    gtyrxp_int(27) <= ch27_gtyrxp_in;
    gtyrxp_int(28) <= ch28_gtyrxp_in;
    gtyrxp_int(29) <= ch29_gtyrxp_in;
    gtyrxp_int(30) <= ch30_gtyrxp_in;
    gtyrxp_int(31) <= ch31_gtyrxp_in;
    gtyrxp_int(32) <= ch32_gtyrxp_in;
    gtyrxp_int(33) <= ch33_gtyrxp_in;
    gtyrxp_int(34) <= ch34_gtyrxp_in;
    gtyrxp_int(35) <= ch35_gtyrxp_in;
    gtyrxp_int(36) <= ch36_gtyrxp_in;
    gtyrxp_int(37) <= ch37_gtyrxp_in;
    gtyrxp_int(38) <= ch38_gtyrxp_in;
    gtyrxp_int(39) <= ch39_gtyrxp_in;
    gtyrxp_int(40) <= ch40_gtyrxp_in;
    gtyrxp_int(41) <= ch41_gtyrxp_in;
    gtyrxp_int(42) <= ch42_gtyrxp_in;
    gtyrxp_int(43) <= ch43_gtyrxp_in;
    gtyrxp_int(44) <= ch44_gtyrxp_in;
    gtyrxp_int(45) <= ch45_gtyrxp_in;
    gtyrxp_int(46) <= ch46_gtyrxp_in;
    gtyrxp_int(47) <= ch47_gtyrxp_in;
    gtyrxp_int(48) <= ch48_gtyrxp_in;
    gtyrxp_int(49) <= ch49_gtyrxp_in;
    gtyrxp_int(50) <= ch50_gtyrxp_in;
    
    ch0_gtytxn_out <= gtytxn_int(0);
    ch1_gtytxn_out <= gtytxn_int(1);
    ch2_gtytxn_out <= gtytxn_int(2);
    ch3_gtytxn_out <= gtytxn_int(3);
    ch4_gtytxn_out <= gtytxn_int(4);
    ch5_gtytxn_out <= gtytxn_int(5);
    ch6_gtytxn_out <= gtytxn_int(6);
    ch7_gtytxn_out <= gtytxn_int(7);
    ch8_gtytxn_out <= gtytxn_int(8);
    ch9_gtytxn_out <= gtytxn_int(9);
    ch10_gtytxn_out <= gtytxn_int(10);
    ch11_gtytxn_out <= gtytxn_int(11);
    ch12_gtytxn_out <= gtytxn_int(12);
    ch13_gtytxn_out <= gtytxn_int(13);
    ch14_gtytxn_out <= gtytxn_int(14);
    ch15_gtytxn_out <= gtytxn_int(15);
    ch16_gtytxn_out <= gtytxn_int(16);
    ch17_gtytxn_out <= gtytxn_int(17);
    ch18_gtytxn_out <= gtytxn_int(18);
    ch19_gtytxn_out <= gtytxn_int(19);
    ch20_gtytxn_out <= gtytxn_int(20);
    ch21_gtytxn_out <= gtytxn_int(21);
    ch22_gtytxn_out <= gtytxn_int(22);
    ch23_gtytxn_out <= gtytxn_int(23);
    ch24_gtytxn_out <= gtytxn_int(24);
    ch25_gtytxn_out <= gtytxn_int(25);
    ch26_gtytxn_out <= gtytxn_int(26);
    ch27_gtytxn_out <= gtytxn_int(27);
    ch28_gtytxn_out <= gtytxn_int(28);
    ch29_gtytxn_out <= gtytxn_int(29);
    ch30_gtytxn_out <= gtytxn_int(30);
    ch31_gtytxn_out <= gtytxn_int(31);
    ch32_gtytxn_out <= gtytxn_int(32);
    ch33_gtytxn_out <= gtytxn_int(33);
    ch34_gtytxn_out <= gtytxn_int(34);
    ch35_gtytxn_out <= gtytxn_int(35);
    ch36_gtytxn_out <= gtytxn_int(36);
    ch37_gtytxn_out <= gtytxn_int(37);
    ch38_gtytxn_out <= gtytxn_int(38);
    ch39_gtytxn_out <= gtytxn_int(39);
    ch40_gtytxn_out <= gtytxn_int(40);
    ch41_gtytxn_out <= gtytxn_int(41);
    ch42_gtytxn_out <= gtytxn_int(42);
    ch43_gtytxn_out <= gtytxn_int(43);
    ch44_gtytxn_out <= gtytxn_int(44);
    ch45_gtytxn_out <= gtytxn_int(45);
    ch46_gtytxn_out <= gtytxn_int(46);
    ch47_gtytxn_out <= gtytxn_int(47);
    ch48_gtytxn_out <= gtytxn_int(48);
    ch49_gtytxn_out <= gtytxn_int(49);
    ch50_gtytxn_out <= gtytxn_int(50);

    ch0_gtytxp_out <= gtytxp_int(0);
    ch1_gtytxp_out <= gtytxp_int(1);
    ch2_gtytxp_out <= gtytxp_int(2);
    ch3_gtytxp_out <= gtytxp_int(3);
    ch4_gtytxp_out <= gtytxp_int(4);
    ch5_gtytxp_out <= gtytxp_int(5);
    ch6_gtytxp_out <= gtytxp_int(6);
    ch7_gtytxp_out <= gtytxp_int(7);
    ch8_gtytxp_out <= gtytxp_int(8);
    ch9_gtytxp_out <= gtytxp_int(9);
    ch10_gtytxp_out <= gtytxp_int(10);
    ch11_gtytxp_out <= gtytxp_int(11);
    ch12_gtytxp_out <= gtytxp_int(12);
    ch13_gtytxp_out <= gtytxp_int(13);
    ch14_gtytxp_out <= gtytxp_int(14);
    ch15_gtytxp_out <= gtytxp_int(15);
    ch16_gtytxp_out <= gtytxp_int(16);
    ch17_gtytxp_out <= gtytxp_int(17);
    ch18_gtytxp_out <= gtytxp_int(18);
    ch19_gtytxp_out <= gtytxp_int(19);
    ch20_gtytxp_out <= gtytxp_int(20);
    ch21_gtytxp_out <= gtytxp_int(21);
    ch22_gtytxp_out <= gtytxp_int(22);
    ch23_gtytxp_out <= gtytxp_int(23);
    ch24_gtytxp_out <= gtytxp_int(24);
    ch25_gtytxp_out <= gtytxp_int(25);
    ch26_gtytxp_out <= gtytxp_int(26);
    ch27_gtytxp_out <= gtytxp_int(27);
    ch28_gtytxp_out <= gtytxp_int(28);
    ch29_gtytxp_out <= gtytxp_int(29);
    ch30_gtytxp_out <= gtytxp_int(30);
    ch31_gtytxp_out <= gtytxp_int(31);
    ch32_gtytxp_out <= gtytxp_int(32);
    ch33_gtytxp_out <= gtytxp_int(33);
    ch34_gtytxp_out <= gtytxp_int(34);
    ch35_gtytxp_out <= gtytxp_int(35);
    ch36_gtytxp_out <= gtytxp_int(36);
    ch37_gtytxp_out <= gtytxp_int(37);
    ch38_gtytxp_out <= gtytxp_int(38);
    ch39_gtytxp_out <= gtytxp_int(39);
    ch40_gtytxp_out <= gtytxp_int(40);
    ch41_gtytxp_out <= gtytxp_int(41);
    ch42_gtytxp_out <= gtytxp_int(42);
    ch43_gtytxp_out <= gtytxp_int(43);
    ch44_gtytxp_out <= gtytxp_int(44);
    ch45_gtytxp_out <= gtytxp_int(45);
    ch46_gtytxp_out <= gtytxp_int(46);
    ch47_gtytxp_out <= gtytxp_int(47);
    ch48_gtytxp_out <= gtytxp_int(48);
    ch49_gtytxp_out <= gtytxp_int(49);
    ch50_gtytxp_out <= gtytxp_int(50);


    gtrefclk00_int(0) <= cm0_gtrefclk00_int;
    gtrefclk00_int(1) <= cm1_gtrefclk00_int;
    gtrefclk00_int(2) <= cm2_gtrefclk00_int;
    gtrefclk00_int(3) <= cm3_gtrefclk00_int;
    gtrefclk00_int(4) <= cm4_gtrefclk00_int;
    gtrefclk00_int(5) <= cm5_gtrefclk00_int;
    gtrefclk00_int(6) <= cm6_gtrefclk00_int;
    gtrefclk00_int(7) <= cm7_gtrefclk00_int;
    gtrefclk00_int(8) <= cm8_gtrefclk00_int;
    gtrefclk00_int(9) <= cm9_gtrefclk00_int;
    gtrefclk00_int(10) <= cm10_gtrefclk00_int;
    gtrefclk00_int(11) <= cm11_gtrefclk00_int;

    cm0_qpll0outclk_int <= qpll0outclk_int(0);
    cm1_qpll0outclk_int <= qpll0outclk_int(1);
    cm2_qpll0outclk_int <= qpll0outclk_int(2);
    cm3_qpll0outclk_int <= qpll0outclk_int(3);
    cm4_qpll0outclk_int <= qpll0outclk_int(4);
    cm5_qpll0outclk_int <= qpll0outclk_int(5);
    cm6_qpll0outclk_int <= qpll0outclk_int(6);
    cm7_qpll0outclk_int <= qpll0outclk_int(7);
    cm8_qpll0outclk_int <= qpll0outclk_int(8);
    cm9_qpll0outclk_int <= qpll0outclk_int(9);
    cm10_qpll0outclk_int <= qpll0outclk_int(10);
    cm11_qpll0outclk_int <= qpll0outclk_int(11);
    cm12_qpll0outclk_int <= qpll0outclk_int(12);

    cm0_qpll0outrefclk_int <= qpll0outrefclk_int(0);
    cm1_qpll0outrefclk_int <= qpll0outrefclk_int(1);
    cm2_qpll0outrefclk_int <= qpll0outrefclk_int(2);
    cm3_qpll0outrefclk_int <= qpll0outrefclk_int(3);
    cm4_qpll0outrefclk_int <= qpll0outrefclk_int(4);
    cm5_qpll0outrefclk_int <= qpll0outrefclk_int(5);
    cm6_qpll0outrefclk_int <= qpll0outrefclk_int(6);
    cm7_qpll0outrefclk_int <= qpll0outrefclk_int(7);
    cm8_qpll0outrefclk_int <= qpll0outrefclk_int(8);
    cm9_qpll0outrefclk_int <= qpll0outrefclk_int(9);
    cm10_qpll0outrefclk_int <= qpll0outrefclk_int(10);
    cm11_qpll0outrefclk_int <= qpll0outrefclk_int(11);
    cm12_qpll0outrefclk_int <= qpll0outrefclk_int(12);
    

    -- ===================================================================================================================
    -- BUFFERS
    -- ===================================================================================================================
    
    buf_hb_gtwiz_reset_all_inst : IBUF
    port map (
        I => hb_gtwiz_reset_all_in,
        O => hb_gtwiz_reset_all_buf_int
    );

    hb_gtwiz_reset_all_int <= hb_gtwiz_reset_all_buf_int;

    bufg_clk_freerun_inst : BUFG
    port map ( 
        I   => hb_gtwiz_reset_clk_freerun_in,
        O   => hb_gtwiz_reset_clk_freerun_buf_int
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y11_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y11_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y11_p,         
      IB => mgtrefclk0_x0y11_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y12_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y12_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y12_p,         
      IB => mgtrefclk0_x0y12_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y2_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y2_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y2_p,         
      IB => mgtrefclk0_x0y2_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y5_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y5_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y5_p,         
      IB => mgtrefclk0_x0y5_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y7_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y7_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y7_p,         
      IB => mgtrefclk0_x0y7_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y9_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y9_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y9_p,         
      IB => mgtrefclk0_x0y9_n        
    );
    
    cm0_gtrefclk00_int <= mgtrefclk0_x0y2_int;
    cm10_gtrefclk00_int <= mgtrefclk0_x0y11_int;
    cm11_gtrefclk00_int <= mgtrefclk0_x0y11_int;
    cm12_gtrefclk00_int <= mgtrefclk0_x0y12_int;
    cm1_gtrefclk00_int <= mgtrefclk0_x0y2_int;
    cm2_gtrefclk00_int <= mgtrefclk0_x0y2_int;
    cm3_gtrefclk00_int <= mgtrefclk0_x0y2_int;
    cm4_gtrefclk00_int <= mgtrefclk0_x0y5_int;
    cm5_gtrefclk00_int <= mgtrefclk0_x0y5_int;
    cm6_gtrefclk00_int <= mgtrefclk0_x0y7_int;
    cm7_gtrefclk00_int <= mgtrefclk0_x0y7_int;
    cm8_gtrefclk00_int <= mgtrefclk0_x0y9_int;
    cm9_gtrefclk00_int <= mgtrefclk0_x0y9_int;
    
    hb0_gtwiz_userclk_tx_reset_int <= not (and_reduce(txpmaresetdone_int));
    hb0_gtwiz_userclk_rx_reset_int <= not (and_reduce(rxpmaresetdone_int));
    
    reset_gt_int <= '0';
    
 SL_51_GT_example_wrapper_inst: entity work.SL_51_GT_example_wrapper
 port map
 (
   gtyrxn_in                               => gtyrxn_int,
   gtyrxp_in                               => gtyrxp_int,
   gtytxn_out                              => gtytxn_int,
   gtytxp_out                              => gtytxp_int,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int,
   gtwiz_userclk_tx_srcclk_out             => gtwiz_userclk_tx_srcclk_int,
   gtwiz_userclk_tx_usrclk_out             => gtwiz_userclk_tx_usrclk_int,
   gtwiz_userclk_tx_usrclk2_out            => gtwiz_userclk_tx_usrclk2_int,
   gtwiz_userclk_tx_active_out             => gtwiz_userclk_tx_active_int,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int,
   gtwiz_userclk_rx_srcclk_out             => gtwiz_userclk_rx_srcclk_int,
   gtwiz_userclk_rx_usrclk_out             => gtwiz_userclk_rx_usrclk_int,
   gtwiz_userclk_rx_usrclk2_out            => gtwiz_userclk_rx_usrclk2_int,
   gtwiz_userclk_rx_active_out             => gtwiz_userclk_rx_active_int,
   gtwiz_reset_clk_freerun_in              => hb_gtwiz_reset_clk_freerun_buf_int,
   gtwiz_reset_all_in                      => hb_gtwiz_reset_all_int,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => gtwiz_reset_rx_cdr_stable_int,
   gtwiz_reset_tx_done_out                 => gtwiz_reset_tx_done_int,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int,
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int,
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int,
   gtrefclk00_in                           => gtrefclk00_int,
   qpll0outclk_out                         => qpll0outclk_int,
   qpll0outrefclk_out                      => qpll0outrefclk_int,
   rxslide_in                              => rxslide_int,
   gtpowergood_out                         => gtpowergood_int,
   rxpmaresetdone_out                      => rxpmaresetdone_int,
   txpmaresetdone_out                      => txpmaresetdone_int
);

GEN_DOWNLINK: for i in 0 to 50 generate
downlink_inst : lpgbtfpga_downlink
    generic map (
        c_multicyleDelay => 1,
        c_clockRatio     => 2,
        c_outputWidth    => 32
    )
    port map (
        clk_i                 => gtwiz_userclk_tx_usrclk2_int,
        clkEn_i               => downlink_clock_en(i),
        rst_n_i               => txpmaresetdone_int(i), -- active low reset
        userData_i            => downlink_user_data(i),
        ECData_i              => downlink_ec_data(i),
        ICData_i              => downlink_ic_data(i),
        mgt_word_o            => gtwiz_userdata_tx_int(31+32*i downto 32*i),
        interleaverBypass_i   => '0',
        encoderBypass_i       => '0',
        scramblerBypass_i     => '0',
        rdy_o                 => downlink_ready(i)
    );
 end generate;
 
 GEN_UPLINK: for i in 0 to 50 generate
   uplink_inst : lpgbtfpga_uplink 
   generic map(
        -- General configuration
        DATARATE                        => 2,                             --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             => 1,                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                => 3,                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    => 8,                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  => 32,                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            => 5,                                             --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       => 64,                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            => 30,                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                => 1,                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               => 40                                       --! Number of clock cycle required before being back in a stable state
   )
   port map (
        -- Clock and reset
        uplinkClk_i                     => gtwiz_userclk_rx_usrclk2_int,                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                => uplink_clk_en_i(i),                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   =>  gtwiz_reset_rx_done_int,                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      => gtwiz_userdata_rx_int(31+32*i downto 32*i),  --! Input frame coming from the MGT

        -- Data
        userData_o                      => uplink_user_data(i),              --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        => uplink_ec_data(i),                  --! EC field value received from the LpGBT
        IcData_o                        => uplink_ic_data(i),                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             => '0',                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              => '0',                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               => '0',                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               => rxslide_int(i),                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 => uplink_dataCorrected(i),                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   => uplink_IcCorrected(i),                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   => uplink_EcCorrected(i),                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           => uplink_rdy_i(i),                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              => open                                       --! Number of bit slip is even (required only for advanced applications)

   );
   end generate;
   
   
   
   downlink_data_gen : process(gtwiz_userclk_tx_usrclk2_int) -- 80 MHz
    begin
        if rising_edge(gtwiz_userclk_tx_usrclk2_int) then
            if gtwiz_userclk_tx_active_int = '0' then
                downlink_user_data_gen <= (others => '0');
                downlink_ec_data_gen <= (others => '0');
                downlink_ic_data_gen <= (others => '0');
                downlink_clock_en_gen <= '0';
            else
                --if downlink_ready = '1' then
                    downlink_clock_en_gen <= not downlink_clock_en_gen;
                    if downlink_clock_en_gen = '0' then
                        downlink_user_data_gen <= downlink_user_data_gen + 1;
                        downlink_ec_data_gen <= downlink_ec_data_gen + 1;
                        downlink_ic_data_gen <= downlink_ic_data_gen + 1;
                    end if;
                --end if;
            end if;
        end if;
    end process;
 
 DOWNLINK_ASSIGNMENT: for i in 0 to 50 generate
    downlink_user_data(i) <= downlink_user_data_gen;
    downlink_ec_data(i) <= downlink_ec_data_gen;
    downlink_ic_data(i) <= downlink_ic_data_gen;
    downlink_clock_en(i) <= downlink_clock_en_gen;
 end generate;
    
    
    divide_decoded_uplink_frame:  for i in 0 to 50 generate
        process(gtwiz_userclk_rx_usrclk2_int)
        begin
            if rising_edge(gtwiz_userclk_rx_usrclk2_int) then
                if uplink_rdy_i(i) = '0' then
                    divide_counter_int(i) <= (others => '0');
                else
                    if uplink_clk_en_i(i) = '1' then    
                        decoded_frame_224_int(i) <= uplink_user_data(i)(223 downto 0);
                    end if;
                    divide_counter_int(i) <= divide_counter_int(i) + 1;
                     if divide_counter_int(i) = 0 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(27 downto 0);
                    elsif divide_counter_int(i) = 1 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(55 downto 28);
                    elsif divide_counter_int(i) = 2 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(83 downto 56);
                    elsif divide_counter_int(i) = 3 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(111 downto 84);
                    elsif divide_counter_int(i) = 4 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(139 downto 112);
                    elsif divide_counter_int(i) = 5 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(167 downto 140);
                    elsif divide_counter_int(i) = 6 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(195 downto 168);
                    elsif divide_counter_int(i) = 7 then
                        output_word_28_int(i) <= decoded_frame_224_int(i)(223 downto 196);
                    end if;
                end if;
            end if;
        end process;
    end generate;

  
end RTL;
