//------------------------------------------------------------------------------
//  (c) Copyright 2013-2018 Xilinx, Inc. All rights reserved.
//
//  This file contains confidential and proprietary information
//  of Xilinx, Inc. and is protected under U.S. and
//  international copyright and other intellectual property
//  laws.
//
//  DISCLAIMER
//  This disclaimer is not a license and does not grant any
//  rights to the materials distributed herewith. Except as
//  otherwise provided in a valid license issued to you by
//  Xilinx, and to the maximum extent permitted by applicable
//  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
//  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
//  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
//  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
//  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
//  (2) Xilinx shall not be liable (whether in contract or tort,
//  including negligence, or under any other theory of
//  liability) for any loss or damage of any kind or nature
//  related to, arising under or in connection with these
//  materials, including for any direct, or any indirect,
//  special, incidental, or consequential loss or damage
//  (including loss of data, profits, goodwill, or any type of
//  loss or damage suffered as a result of any action brought
//  by a third party) even if such damage or loss was
//  reasonably foreseeable or Xilinx had been advised of the
//  possibility of the same.
//
//  CRITICAL APPLICATIONS
//  Xilinx products are not designed or intended to be fail-
//  safe, or for use in any application requiring fail-safe
//  performance, such as life-support or safety devices or
//  systems, Class III medical devices, nuclear facilities,
//  applications related to the deployment of airbags, or any
//  other applications that could lead to death, personal
//  injury, or severe property or environmental damage
//  (individually and collectively, "Critical
//  Applications"). Customer assumes the sole risk and
//  liability of any use of Xilinx products in Critical
//  Applications, subject only to applicable laws and
//  regulations governing limitations on product liability.
//
//  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
//  PART OF THIS FILE AT ALL TIMES.
//------------------------------------------------------------------------------


`timescale 1ps/1ps

// =====================================================================================================================
// This example design top simulation module instantiates the example design top module, provides basic stimulus to it
// while looping back transceiver data from transmit to receive, and utilizes the PRBS checker-based link status
// indicator to demonstrate simple data integrity checking of the design. This module is for use in simulation only.
// =====================================================================================================================

module SL_51_GT_example_top_sim ();


  // -------------------------------------------------------------------------------------------------------------------
  // Signal declarations and basic example design stimulus
  // -------------------------------------------------------------------------------------------------------------------

  // Declare wires to loop back serial data ports for transceiver channel 0
  wire ch0_gtyxn;
  wire ch0_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 1
  wire ch1_gtyxn;
  wire ch1_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 2
  wire ch2_gtyxn;
  wire ch2_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 3
  wire ch3_gtyxn;
  wire ch3_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 4
  wire ch4_gtyxn;
  wire ch4_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 5
  wire ch5_gtyxn;
  wire ch5_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 6
  wire ch6_gtyxn;
  wire ch6_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 7
  wire ch7_gtyxn;
  wire ch7_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 8
  wire ch8_gtyxn;
  wire ch8_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 9
  wire ch9_gtyxn;
  wire ch9_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 10
  wire ch10_gtyxn;
  wire ch10_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 11
  wire ch11_gtyxn;
  wire ch11_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 12
  wire ch12_gtyxn;
  wire ch12_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 13
  wire ch13_gtyxn;
  wire ch13_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 14
  wire ch14_gtyxn;
  wire ch14_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 15
  wire ch15_gtyxn;
  wire ch15_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 16
  wire ch16_gtyxn;
  wire ch16_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 17
  wire ch17_gtyxn;
  wire ch17_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 18
  wire ch18_gtyxn;
  wire ch18_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 19
  wire ch19_gtyxn;
  wire ch19_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 20
  wire ch20_gtyxn;
  wire ch20_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 21
  wire ch21_gtyxn;
  wire ch21_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 22
  wire ch22_gtyxn;
  wire ch22_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 23
  wire ch23_gtyxn;
  wire ch23_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 24
  wire ch24_gtyxn;
  wire ch24_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 25
  wire ch25_gtyxn;
  wire ch25_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 26
  wire ch26_gtyxn;
  wire ch26_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 27
  wire ch27_gtyxn;
  wire ch27_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 28
  wire ch28_gtyxn;
  wire ch28_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 29
  wire ch29_gtyxn;
  wire ch29_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 30
  wire ch30_gtyxn;
  wire ch30_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 31
  wire ch31_gtyxn;
  wire ch31_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 32
  wire ch32_gtyxn;
  wire ch32_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 33
  wire ch33_gtyxn;
  wire ch33_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 34
  wire ch34_gtyxn;
  wire ch34_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 35
  wire ch35_gtyxn;
  wire ch35_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 36
  wire ch36_gtyxn;
  wire ch36_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 37
  wire ch37_gtyxn;
  wire ch37_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 38
  wire ch38_gtyxn;
  wire ch38_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 39
  wire ch39_gtyxn;
  wire ch39_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 40
  wire ch40_gtyxn;
  wire ch40_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 41
  wire ch41_gtyxn;
  wire ch41_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 42
  wire ch42_gtyxn;
  wire ch42_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 43
  wire ch43_gtyxn;
  wire ch43_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 44
  wire ch44_gtyxn;
  wire ch44_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 45
  wire ch45_gtyxn;
  wire ch45_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 46
  wire ch46_gtyxn;
  wire ch46_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 47
  wire ch47_gtyxn;
  wire ch47_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 48
  wire ch48_gtyxn;
  wire ch48_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 49
  wire ch49_gtyxn;
  wire ch49_gtyxp;

  // Declare wires to loop back serial data ports for transceiver channel 50
  wire ch50_gtyxn;
  wire ch50_gtyxp;

  // Declare register to drive reference clock at location MGTREFCLK0_X0Y11
  reg mgtrefclk0_x0y11 = 1'b0;

  // Drive that reference clock at the appropriate frequency
  // NOTE: the following simulation reference clock period may be up to +/- 2ps from its nominal value, due to rounding
  // within Verilog timescale granularity, especially when transmitter and receiver reference clock frequencies differ
  initial begin
    mgtrefclk0_x0y11 = 1'b0;
    forever
      mgtrefclk0_x0y11 = #3125 ~mgtrefclk0_x0y11;
  end

  // Declare register to drive reference clock at location MGTREFCLK0_X0Y9
  reg mgtrefclk0_x0y9 = 1'b0;

  // Drive that reference clock at the appropriate frequency
  // NOTE: the following simulation reference clock period may be up to +/- 2ps from its nominal value, due to rounding
  // within Verilog timescale granularity, especially when transmitter and receiver reference clock frequencies differ
  initial begin
    mgtrefclk0_x0y9 = 1'b0;
    forever
      mgtrefclk0_x0y9 = #3125 ~mgtrefclk0_x0y9;
  end

  // Declare register to drive reference clock at location MGTREFCLK0_X0Y7
  reg mgtrefclk0_x0y7 = 1'b0;

  // Drive that reference clock at the appropriate frequency
  // NOTE: the following simulation reference clock period may be up to +/- 2ps from its nominal value, due to rounding
  // within Verilog timescale granularity, especially when transmitter and receiver reference clock frequencies differ
  initial begin
    mgtrefclk0_x0y7 = 1'b0;
    forever
      mgtrefclk0_x0y7 = #3125 ~mgtrefclk0_x0y7;
  end

  // Declare register to drive reference clock at location MGTREFCLK0_X0Y5
  reg mgtrefclk0_x0y5 = 1'b0;

  // Drive that reference clock at the appropriate frequency
  // NOTE: the following simulation reference clock period may be up to +/- 2ps from its nominal value, due to rounding
  // within Verilog timescale granularity, especially when transmitter and receiver reference clock frequencies differ
  initial begin
    mgtrefclk0_x0y5 = 1'b0;
    forever
      mgtrefclk0_x0y5 = #3125 ~mgtrefclk0_x0y5;
  end

  // Declare register to drive reference clock at location MGTREFCLK0_X0Y2
  reg mgtrefclk0_x0y2 = 1'b0;

  // Drive that reference clock at the appropriate frequency
  // NOTE: the following simulation reference clock period may be up to +/- 2ps from its nominal value, due to rounding
  // within Verilog timescale granularity, especially when transmitter and receiver reference clock frequencies differ
  initial begin
    mgtrefclk0_x0y2 = 1'b0;
    forever
      mgtrefclk0_x0y2 = #3125 ~mgtrefclk0_x0y2;
  end

  // Declare register to drive reference clock at location MGTREFCLK0_X0Y12
  reg mgtrefclk0_x0y12 = 1'b0;

  // Drive that reference clock at the appropriate frequency
  // NOTE: the following simulation reference clock period may be up to +/- 2ps from its nominal value, due to rounding
  // within Verilog timescale granularity, especially when transmitter and receiver reference clock frequencies differ
  initial begin
    mgtrefclk0_x0y12 = 1'b0;
    forever
      mgtrefclk0_x0y12 = #3125 ~mgtrefclk0_x0y12;
  end

  // Declare registers to drive reset helper block(s)
  reg hb_gtwiz_reset_clk_freerun = 1'b0;
  reg hb_gtwiz_reset_all         = 1'b1;

  // Drive the helper block free running clock
  initial begin
    hb_gtwiz_reset_clk_freerun = 1'b0;
    forever
      hb_gtwiz_reset_clk_freerun = #6250 ~hb_gtwiz_reset_clk_freerun;
  end

  // Drive the helper block "reset all" input high, then low after some time
  initial begin
    hb_gtwiz_reset_all = 1'b1;
    #5E6;
    repeat (100)
      @(hb_gtwiz_reset_clk_freerun);
    hb_gtwiz_reset_all = 1'b0;
  end

  // Declare registers and wires to interface to the PRBS-based link status ports
  reg  link_down_latched_reset = 1'b0;
  wire link_status;
  wire link_down_latched;

  // -------------------------------------------------------------------------------------------------------------------
  // Asymmetric case handling
  // -------------------------------------------------------------------------------------------------------------------

  // The selected configuration either uses asymmetric transmitter and receiver line rates, different
  // data coding on transmit and receive, or enables only a single data direction. Therefore, the loopback
  // configuration this simple simulation environment relies upon is not suitable to demonstrate data
  // integrity. To simulate your configuration, use a suitable link partner. This simulation will simply
  // run for a short period of time and then end with a test completed successfully message, but it should
  // not be construed to mean that data integrity was observed in this asymmetric configuration. You may
  // wish to extend this simulation period to observe PLL lock, reset behavior, etc.

  initial begin
    $display("=====================================================================================================");
    $display("The selected configuration either uses asymmetric transmitter and receiver line rates, different");
    $display("data coding on transmit and receive, or enables only a single data direction. Therefore, the loopback");
    $display("configuration this simple simulation environment relies upon is not suitable to demonstrate data");
    $display("integrity. To simulate your configuration, use a suitable link partner. This simulation will simply");
    $display("run for a short period of time and then end with a test completed successfully message, but it should");
    $display("not be construed to mean that data integrity was observed in this asymmetric configuration. You may");
    $display("wish to extend this simulation period to observe PLL lock, reset behavior, etc.");
    $display("=====================================================================================================");

    #5E7;

    $display("Time : %12d ps   STOP: data integrity could not be *demonstrated* in this asymmetric configuration", $time);
    $display("** Test completed successfully");

    $finish;
  end

  // -------------------------------------------------------------------------------------------------------------------
  // Instantiate example design top module as the simulation DUT
  // -------------------------------------------------------------------------------------------------------------------

  SL_51_GT_example_top example_top_inst (
    .mgtrefclk0_x0y11_p (mgtrefclk0_x0y11),
    .mgtrefclk0_x0y11_n (~mgtrefclk0_x0y11),
    .mgtrefclk0_x0y12_p (mgtrefclk0_x0y12),
    .mgtrefclk0_x0y12_n (~mgtrefclk0_x0y12),
    .mgtrefclk0_x0y2_p (mgtrefclk0_x0y2),
    .mgtrefclk0_x0y2_n (~mgtrefclk0_x0y2),
    .mgtrefclk0_x0y5_p (mgtrefclk0_x0y5),
    .mgtrefclk0_x0y5_n (~mgtrefclk0_x0y5),
    .mgtrefclk0_x0y7_p (mgtrefclk0_x0y7),
    .mgtrefclk0_x0y7_n (~mgtrefclk0_x0y7),
    .mgtrefclk0_x0y9_p (mgtrefclk0_x0y9),
    .mgtrefclk0_x0y9_n (~mgtrefclk0_x0y9),
    .ch0_gtyrxn_in (ch0_gtyxn),
    .ch0_gtyrxp_in (ch0_gtyxp),
    .ch0_gtytxn_out (ch0_gtyxn),
    .ch0_gtytxp_out (ch0_gtyxp),
    .ch1_gtyrxn_in (ch1_gtyxn),
    .ch1_gtyrxp_in (ch1_gtyxp),
    .ch1_gtytxn_out (ch1_gtyxn),
    .ch1_gtytxp_out (ch1_gtyxp),
    .ch2_gtyrxn_in (ch2_gtyxn),
    .ch2_gtyrxp_in (ch2_gtyxp),
    .ch2_gtytxn_out (ch2_gtyxn),
    .ch2_gtytxp_out (ch2_gtyxp),
    .ch3_gtyrxn_in (ch3_gtyxn),
    .ch3_gtyrxp_in (ch3_gtyxp),
    .ch3_gtytxn_out (ch3_gtyxn),
    .ch3_gtytxp_out (ch3_gtyxp),
    .ch4_gtyrxn_in (ch4_gtyxn),
    .ch4_gtyrxp_in (ch4_gtyxp),
    .ch4_gtytxn_out (ch4_gtyxn),
    .ch4_gtytxp_out (ch4_gtyxp),
    .ch5_gtyrxn_in (ch5_gtyxn),
    .ch5_gtyrxp_in (ch5_gtyxp),
    .ch5_gtytxn_out (ch5_gtyxn),
    .ch5_gtytxp_out (ch5_gtyxp),
    .ch6_gtyrxn_in (ch6_gtyxn),
    .ch6_gtyrxp_in (ch6_gtyxp),
    .ch6_gtytxn_out (ch6_gtyxn),
    .ch6_gtytxp_out (ch6_gtyxp),
    .ch7_gtyrxn_in (ch7_gtyxn),
    .ch7_gtyrxp_in (ch7_gtyxp),
    .ch7_gtytxn_out (ch7_gtyxn),
    .ch7_gtytxp_out (ch7_gtyxp),
    .ch8_gtyrxn_in (ch8_gtyxn),
    .ch8_gtyrxp_in (ch8_gtyxp),
    .ch8_gtytxn_out (ch8_gtyxn),
    .ch8_gtytxp_out (ch8_gtyxp),
    .ch9_gtyrxn_in (ch9_gtyxn),
    .ch9_gtyrxp_in (ch9_gtyxp),
    .ch9_gtytxn_out (ch9_gtyxn),
    .ch9_gtytxp_out (ch9_gtyxp),
    .ch10_gtyrxn_in (ch10_gtyxn),
    .ch10_gtyrxp_in (ch10_gtyxp),
    .ch10_gtytxn_out (ch10_gtyxn),
    .ch10_gtytxp_out (ch10_gtyxp),
    .ch11_gtyrxn_in (ch11_gtyxn),
    .ch11_gtyrxp_in (ch11_gtyxp),
    .ch11_gtytxn_out (ch11_gtyxn),
    .ch11_gtytxp_out (ch11_gtyxp),
    .ch12_gtyrxn_in (ch12_gtyxn),
    .ch12_gtyrxp_in (ch12_gtyxp),
    .ch12_gtytxn_out (ch12_gtyxn),
    .ch12_gtytxp_out (ch12_gtyxp),
    .ch13_gtyrxn_in (ch13_gtyxn),
    .ch13_gtyrxp_in (ch13_gtyxp),
    .ch13_gtytxn_out (ch13_gtyxn),
    .ch13_gtytxp_out (ch13_gtyxp),
    .ch14_gtyrxn_in (ch14_gtyxn),
    .ch14_gtyrxp_in (ch14_gtyxp),
    .ch14_gtytxn_out (ch14_gtyxn),
    .ch14_gtytxp_out (ch14_gtyxp),
    .ch15_gtyrxn_in (ch15_gtyxn),
    .ch15_gtyrxp_in (ch15_gtyxp),
    .ch15_gtytxn_out (ch15_gtyxn),
    .ch15_gtytxp_out (ch15_gtyxp),
    .ch16_gtyrxn_in (ch16_gtyxn),
    .ch16_gtyrxp_in (ch16_gtyxp),
    .ch16_gtytxn_out (ch16_gtyxn),
    .ch16_gtytxp_out (ch16_gtyxp),
    .ch17_gtyrxn_in (ch17_gtyxn),
    .ch17_gtyrxp_in (ch17_gtyxp),
    .ch17_gtytxn_out (ch17_gtyxn),
    .ch17_gtytxp_out (ch17_gtyxp),
    .ch18_gtyrxn_in (ch18_gtyxn),
    .ch18_gtyrxp_in (ch18_gtyxp),
    .ch18_gtytxn_out (ch18_gtyxn),
    .ch18_gtytxp_out (ch18_gtyxp),
    .ch19_gtyrxn_in (ch19_gtyxn),
    .ch19_gtyrxp_in (ch19_gtyxp),
    .ch19_gtytxn_out (ch19_gtyxn),
    .ch19_gtytxp_out (ch19_gtyxp),
    .ch20_gtyrxn_in (ch20_gtyxn),
    .ch20_gtyrxp_in (ch20_gtyxp),
    .ch20_gtytxn_out (ch20_gtyxn),
    .ch20_gtytxp_out (ch20_gtyxp),
    .ch21_gtyrxn_in (ch21_gtyxn),
    .ch21_gtyrxp_in (ch21_gtyxp),
    .ch21_gtytxn_out (ch21_gtyxn),
    .ch21_gtytxp_out (ch21_gtyxp),
    .ch22_gtyrxn_in (ch22_gtyxn),
    .ch22_gtyrxp_in (ch22_gtyxp),
    .ch22_gtytxn_out (ch22_gtyxn),
    .ch22_gtytxp_out (ch22_gtyxp),
    .ch23_gtyrxn_in (ch23_gtyxn),
    .ch23_gtyrxp_in (ch23_gtyxp),
    .ch23_gtytxn_out (ch23_gtyxn),
    .ch23_gtytxp_out (ch23_gtyxp),
    .ch24_gtyrxn_in (ch24_gtyxn),
    .ch24_gtyrxp_in (ch24_gtyxp),
    .ch24_gtytxn_out (ch24_gtyxn),
    .ch24_gtytxp_out (ch24_gtyxp),
    .ch25_gtyrxn_in (ch25_gtyxn),
    .ch25_gtyrxp_in (ch25_gtyxp),
    .ch25_gtytxn_out (ch25_gtyxn),
    .ch25_gtytxp_out (ch25_gtyxp),
    .ch26_gtyrxn_in (ch26_gtyxn),
    .ch26_gtyrxp_in (ch26_gtyxp),
    .ch26_gtytxn_out (ch26_gtyxn),
    .ch26_gtytxp_out (ch26_gtyxp),
    .ch27_gtyrxn_in (ch27_gtyxn),
    .ch27_gtyrxp_in (ch27_gtyxp),
    .ch27_gtytxn_out (ch27_gtyxn),
    .ch27_gtytxp_out (ch27_gtyxp),
    .ch28_gtyrxn_in (ch28_gtyxn),
    .ch28_gtyrxp_in (ch28_gtyxp),
    .ch28_gtytxn_out (ch28_gtyxn),
    .ch28_gtytxp_out (ch28_gtyxp),
    .ch29_gtyrxn_in (ch29_gtyxn),
    .ch29_gtyrxp_in (ch29_gtyxp),
    .ch29_gtytxn_out (ch29_gtyxn),
    .ch29_gtytxp_out (ch29_gtyxp),
    .ch30_gtyrxn_in (ch30_gtyxn),
    .ch30_gtyrxp_in (ch30_gtyxp),
    .ch30_gtytxn_out (ch30_gtyxn),
    .ch30_gtytxp_out (ch30_gtyxp),
    .ch31_gtyrxn_in (ch31_gtyxn),
    .ch31_gtyrxp_in (ch31_gtyxp),
    .ch31_gtytxn_out (ch31_gtyxn),
    .ch31_gtytxp_out (ch31_gtyxp),
    .ch32_gtyrxn_in (ch32_gtyxn),
    .ch32_gtyrxp_in (ch32_gtyxp),
    .ch32_gtytxn_out (ch32_gtyxn),
    .ch32_gtytxp_out (ch32_gtyxp),
    .ch33_gtyrxn_in (ch33_gtyxn),
    .ch33_gtyrxp_in (ch33_gtyxp),
    .ch33_gtytxn_out (ch33_gtyxn),
    .ch33_gtytxp_out (ch33_gtyxp),
    .ch34_gtyrxn_in (ch34_gtyxn),
    .ch34_gtyrxp_in (ch34_gtyxp),
    .ch34_gtytxn_out (ch34_gtyxn),
    .ch34_gtytxp_out (ch34_gtyxp),
    .ch35_gtyrxn_in (ch35_gtyxn),
    .ch35_gtyrxp_in (ch35_gtyxp),
    .ch35_gtytxn_out (ch35_gtyxn),
    .ch35_gtytxp_out (ch35_gtyxp),
    .ch36_gtyrxn_in (ch36_gtyxn),
    .ch36_gtyrxp_in (ch36_gtyxp),
    .ch36_gtytxn_out (ch36_gtyxn),
    .ch36_gtytxp_out (ch36_gtyxp),
    .ch37_gtyrxn_in (ch37_gtyxn),
    .ch37_gtyrxp_in (ch37_gtyxp),
    .ch37_gtytxn_out (ch37_gtyxn),
    .ch37_gtytxp_out (ch37_gtyxp),
    .ch38_gtyrxn_in (ch38_gtyxn),
    .ch38_gtyrxp_in (ch38_gtyxp),
    .ch38_gtytxn_out (ch38_gtyxn),
    .ch38_gtytxp_out (ch38_gtyxp),
    .ch39_gtyrxn_in (ch39_gtyxn),
    .ch39_gtyrxp_in (ch39_gtyxp),
    .ch39_gtytxn_out (ch39_gtyxn),
    .ch39_gtytxp_out (ch39_gtyxp),
    .ch40_gtyrxn_in (ch40_gtyxn),
    .ch40_gtyrxp_in (ch40_gtyxp),
    .ch40_gtytxn_out (ch40_gtyxn),
    .ch40_gtytxp_out (ch40_gtyxp),
    .ch41_gtyrxn_in (ch41_gtyxn),
    .ch41_gtyrxp_in (ch41_gtyxp),
    .ch41_gtytxn_out (ch41_gtyxn),
    .ch41_gtytxp_out (ch41_gtyxp),
    .ch42_gtyrxn_in (ch42_gtyxn),
    .ch42_gtyrxp_in (ch42_gtyxp),
    .ch42_gtytxn_out (ch42_gtyxn),
    .ch42_gtytxp_out (ch42_gtyxp),
    .ch43_gtyrxn_in (ch43_gtyxn),
    .ch43_gtyrxp_in (ch43_gtyxp),
    .ch43_gtytxn_out (ch43_gtyxn),
    .ch43_gtytxp_out (ch43_gtyxp),
    .ch44_gtyrxn_in (ch44_gtyxn),
    .ch44_gtyrxp_in (ch44_gtyxp),
    .ch44_gtytxn_out (ch44_gtyxn),
    .ch44_gtytxp_out (ch44_gtyxp),
    .ch45_gtyrxn_in (ch45_gtyxn),
    .ch45_gtyrxp_in (ch45_gtyxp),
    .ch45_gtytxn_out (ch45_gtyxn),
    .ch45_gtytxp_out (ch45_gtyxp),
    .ch46_gtyrxn_in (ch46_gtyxn),
    .ch46_gtyrxp_in (ch46_gtyxp),
    .ch46_gtytxn_out (ch46_gtyxn),
    .ch46_gtytxp_out (ch46_gtyxp),
    .ch47_gtyrxn_in (ch47_gtyxn),
    .ch47_gtyrxp_in (ch47_gtyxp),
    .ch47_gtytxn_out (ch47_gtyxn),
    .ch47_gtytxp_out (ch47_gtyxp),
    .ch48_gtyrxn_in (ch48_gtyxn),
    .ch48_gtyrxp_in (ch48_gtyxp),
    .ch48_gtytxn_out (ch48_gtyxn),
    .ch48_gtytxp_out (ch48_gtyxp),
    .ch49_gtyrxn_in (ch49_gtyxn),
    .ch49_gtyrxp_in (ch49_gtyxp),
    .ch49_gtytxn_out (ch49_gtyxn),
    .ch49_gtytxp_out (ch49_gtyxp),
    .ch50_gtyrxn_in (ch50_gtyxn),
    .ch50_gtyrxp_in (ch50_gtyxp),
    .ch50_gtytxn_out (ch50_gtyxn),
    .ch50_gtytxp_out (ch50_gtyxp),
    .hb_gtwiz_reset_clk_freerun_in (hb_gtwiz_reset_clk_freerun),
    .hb_gtwiz_reset_all_in (hb_gtwiz_reset_all),
    .link_down_latched_reset_in (link_down_latched_reset),
    .link_status_out (link_status),
    .link_down_latched_out (link_down_latched)
  );


endmodule
