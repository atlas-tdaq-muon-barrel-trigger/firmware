//------------------------------------------------------------------------------
//  (c) Copyright 2013-2018 Xilinx, Inc. All rights reserved.
//
//  This file contains confidential and proprietary information
//  of Xilinx, Inc. and is protected under U.S. and
//  international copyright and other intellectual property
//  laws.
//
//  DISCLAIMER
//  This disclaimer is not a license and does not grant any
//  rights to the materials distributed herewith. Except as
//  otherwise provided in a valid license issued to you by
//  Xilinx, and to the maximum extent permitted by applicable
//  law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
//  WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
//  AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
//  BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
//  INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
//  (2) Xilinx shall not be liable (whether in contract or tort,
//  including negligence, or under any other theory of
//  liability) for any loss or damage of any kind or nature
//  related to, arising under or in connection with these
//  materials, including for any direct, or any indirect,
//  special, incidental, or consequential loss or damage
//  (including loss of data, profits, goodwill, or any type of
//  loss or damage suffered as a result of any action brought
//  by a third party) even if such damage or loss was
//  reasonably foreseeable or Xilinx had been advised of the
//  possibility of the same.
//
//  CRITICAL APPLICATIONS
//  Xilinx products are not designed or intended to be fail-
//  safe, or for use in any application requiring fail-safe
//  performance, such as life-support or safety devices or
//  systems, Class III medical devices, nuclear facilities,
//  applications related to the deployment of airbags, or any
//  other applications that could lead to death, personal
//  injury, or severe property or environmental damage
//  (individually and collectively, "Critical
//  Applications"). Customer assumes the sole risk and
//  liability of any use of Xilinx products in Critical
//  Applications, subject only to applicable laws and
//  regulations governing limitations on product liability.
//
//  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
//  PART OF THIS FILE AT ALL TIMES.
//------------------------------------------------------------------------------


`timescale 1ps/1ps

// =====================================================================================================================
// This example design top module instantiates the example design wrapper; slices vectored ports for per-channel
// assignment; and instantiates example resources such as buffers, pattern generators, and pattern checkers for core
// demonstration purposes
// =====================================================================================================================

module SL_51_GT_example_top (

  // Differential reference clock inputs
  input  wire mgtrefclk0_x0y11_p,
  input  wire mgtrefclk0_x0y11_n,
  input  wire mgtrefclk0_x0y12_p,
  input  wire mgtrefclk0_x0y12_n,
  input  wire mgtrefclk0_x0y2_p,
  input  wire mgtrefclk0_x0y2_n,
  input  wire mgtrefclk0_x0y5_p,
  input  wire mgtrefclk0_x0y5_n,
  input  wire mgtrefclk0_x0y7_p,
  input  wire mgtrefclk0_x0y7_n,
  input  wire mgtrefclk0_x0y9_p,
  input  wire mgtrefclk0_x0y9_n,

  // Serial data ports for transceiver channel 0
  input  wire ch0_gtyrxn_in,
  input  wire ch0_gtyrxp_in,
  output wire ch0_gtytxn_out,
  output wire ch0_gtytxp_out,

  // Serial data ports for transceiver channel 1
  input  wire ch1_gtyrxn_in,
  input  wire ch1_gtyrxp_in,
  output wire ch1_gtytxn_out,
  output wire ch1_gtytxp_out,

  // Serial data ports for transceiver channel 2
  input  wire ch2_gtyrxn_in,
  input  wire ch2_gtyrxp_in,
  output wire ch2_gtytxn_out,
  output wire ch2_gtytxp_out,

  // Serial data ports for transceiver channel 3
  input  wire ch3_gtyrxn_in,
  input  wire ch3_gtyrxp_in,
  output wire ch3_gtytxn_out,
  output wire ch3_gtytxp_out,

  // Serial data ports for transceiver channel 4
  input  wire ch4_gtyrxn_in,
  input  wire ch4_gtyrxp_in,
  output wire ch4_gtytxn_out,
  output wire ch4_gtytxp_out,

  // Serial data ports for transceiver channel 5
  input  wire ch5_gtyrxn_in,
  input  wire ch5_gtyrxp_in,
  output wire ch5_gtytxn_out,
  output wire ch5_gtytxp_out,

  // Serial data ports for transceiver channel 6
  input  wire ch6_gtyrxn_in,
  input  wire ch6_gtyrxp_in,
  output wire ch6_gtytxn_out,
  output wire ch6_gtytxp_out,

  // Serial data ports for transceiver channel 7
  input  wire ch7_gtyrxn_in,
  input  wire ch7_gtyrxp_in,
  output wire ch7_gtytxn_out,
  output wire ch7_gtytxp_out,

  // Serial data ports for transceiver channel 8
  input  wire ch8_gtyrxn_in,
  input  wire ch8_gtyrxp_in,
  output wire ch8_gtytxn_out,
  output wire ch8_gtytxp_out,

  // Serial data ports for transceiver channel 9
  input  wire ch9_gtyrxn_in,
  input  wire ch9_gtyrxp_in,
  output wire ch9_gtytxn_out,
  output wire ch9_gtytxp_out,

  // Serial data ports for transceiver channel 10
  input  wire ch10_gtyrxn_in,
  input  wire ch10_gtyrxp_in,
  output wire ch10_gtytxn_out,
  output wire ch10_gtytxp_out,

  // Serial data ports for transceiver channel 11
  input  wire ch11_gtyrxn_in,
  input  wire ch11_gtyrxp_in,
  output wire ch11_gtytxn_out,
  output wire ch11_gtytxp_out,

  // Serial data ports for transceiver channel 12
  input  wire ch12_gtyrxn_in,
  input  wire ch12_gtyrxp_in,
  output wire ch12_gtytxn_out,
  output wire ch12_gtytxp_out,

  // Serial data ports for transceiver channel 13
  input  wire ch13_gtyrxn_in,
  input  wire ch13_gtyrxp_in,
  output wire ch13_gtytxn_out,
  output wire ch13_gtytxp_out,

  // Serial data ports for transceiver channel 14
  input  wire ch14_gtyrxn_in,
  input  wire ch14_gtyrxp_in,
  output wire ch14_gtytxn_out,
  output wire ch14_gtytxp_out,

  // Serial data ports for transceiver channel 15
  input  wire ch15_gtyrxn_in,
  input  wire ch15_gtyrxp_in,
  output wire ch15_gtytxn_out,
  output wire ch15_gtytxp_out,

  // Serial data ports for transceiver channel 16
  input  wire ch16_gtyrxn_in,
  input  wire ch16_gtyrxp_in,
  output wire ch16_gtytxn_out,
  output wire ch16_gtytxp_out,

  // Serial data ports for transceiver channel 17
  input  wire ch17_gtyrxn_in,
  input  wire ch17_gtyrxp_in,
  output wire ch17_gtytxn_out,
  output wire ch17_gtytxp_out,

  // Serial data ports for transceiver channel 18
  input  wire ch18_gtyrxn_in,
  input  wire ch18_gtyrxp_in,
  output wire ch18_gtytxn_out,
  output wire ch18_gtytxp_out,

  // Serial data ports for transceiver channel 19
  input  wire ch19_gtyrxn_in,
  input  wire ch19_gtyrxp_in,
  output wire ch19_gtytxn_out,
  output wire ch19_gtytxp_out,

  // Serial data ports for transceiver channel 20
  input  wire ch20_gtyrxn_in,
  input  wire ch20_gtyrxp_in,
  output wire ch20_gtytxn_out,
  output wire ch20_gtytxp_out,

  // Serial data ports for transceiver channel 21
  input  wire ch21_gtyrxn_in,
  input  wire ch21_gtyrxp_in,
  output wire ch21_gtytxn_out,
  output wire ch21_gtytxp_out,

  // Serial data ports for transceiver channel 22
  input  wire ch22_gtyrxn_in,
  input  wire ch22_gtyrxp_in,
  output wire ch22_gtytxn_out,
  output wire ch22_gtytxp_out,

  // Serial data ports for transceiver channel 23
  input  wire ch23_gtyrxn_in,
  input  wire ch23_gtyrxp_in,
  output wire ch23_gtytxn_out,
  output wire ch23_gtytxp_out,

  // Serial data ports for transceiver channel 24
  input  wire ch24_gtyrxn_in,
  input  wire ch24_gtyrxp_in,
  output wire ch24_gtytxn_out,
  output wire ch24_gtytxp_out,

  // Serial data ports for transceiver channel 25
  input  wire ch25_gtyrxn_in,
  input  wire ch25_gtyrxp_in,
  output wire ch25_gtytxn_out,
  output wire ch25_gtytxp_out,

  // Serial data ports for transceiver channel 26
  input  wire ch26_gtyrxn_in,
  input  wire ch26_gtyrxp_in,
  output wire ch26_gtytxn_out,
  output wire ch26_gtytxp_out,

  // Serial data ports for transceiver channel 27
  input  wire ch27_gtyrxn_in,
  input  wire ch27_gtyrxp_in,
  output wire ch27_gtytxn_out,
  output wire ch27_gtytxp_out,

  // Serial data ports for transceiver channel 28
  input  wire ch28_gtyrxn_in,
  input  wire ch28_gtyrxp_in,
  output wire ch28_gtytxn_out,
  output wire ch28_gtytxp_out,

  // Serial data ports for transceiver channel 29
  input  wire ch29_gtyrxn_in,
  input  wire ch29_gtyrxp_in,
  output wire ch29_gtytxn_out,
  output wire ch29_gtytxp_out,

  // Serial data ports for transceiver channel 30
  input  wire ch30_gtyrxn_in,
  input  wire ch30_gtyrxp_in,
  output wire ch30_gtytxn_out,
  output wire ch30_gtytxp_out,

  // Serial data ports for transceiver channel 31
  input  wire ch31_gtyrxn_in,
  input  wire ch31_gtyrxp_in,
  output wire ch31_gtytxn_out,
  output wire ch31_gtytxp_out,

  // Serial data ports for transceiver channel 32
  input  wire ch32_gtyrxn_in,
  input  wire ch32_gtyrxp_in,
  output wire ch32_gtytxn_out,
  output wire ch32_gtytxp_out,

  // Serial data ports for transceiver channel 33
  input  wire ch33_gtyrxn_in,
  input  wire ch33_gtyrxp_in,
  output wire ch33_gtytxn_out,
  output wire ch33_gtytxp_out,

  // Serial data ports for transceiver channel 34
  input  wire ch34_gtyrxn_in,
  input  wire ch34_gtyrxp_in,
  output wire ch34_gtytxn_out,
  output wire ch34_gtytxp_out,

  // Serial data ports for transceiver channel 35
  input  wire ch35_gtyrxn_in,
  input  wire ch35_gtyrxp_in,
  output wire ch35_gtytxn_out,
  output wire ch35_gtytxp_out,

  // Serial data ports for transceiver channel 36
  input  wire ch36_gtyrxn_in,
  input  wire ch36_gtyrxp_in,
  output wire ch36_gtytxn_out,
  output wire ch36_gtytxp_out,

  // Serial data ports for transceiver channel 37
  input  wire ch37_gtyrxn_in,
  input  wire ch37_gtyrxp_in,
  output wire ch37_gtytxn_out,
  output wire ch37_gtytxp_out,

  // Serial data ports for transceiver channel 38
  input  wire ch38_gtyrxn_in,
  input  wire ch38_gtyrxp_in,
  output wire ch38_gtytxn_out,
  output wire ch38_gtytxp_out,

  // Serial data ports for transceiver channel 39
  input  wire ch39_gtyrxn_in,
  input  wire ch39_gtyrxp_in,
  output wire ch39_gtytxn_out,
  output wire ch39_gtytxp_out,

  // Serial data ports for transceiver channel 40
  input  wire ch40_gtyrxn_in,
  input  wire ch40_gtyrxp_in,
  output wire ch40_gtytxn_out,
  output wire ch40_gtytxp_out,

  // Serial data ports for transceiver channel 41
  input  wire ch41_gtyrxn_in,
  input  wire ch41_gtyrxp_in,
  output wire ch41_gtytxn_out,
  output wire ch41_gtytxp_out,

  // Serial data ports for transceiver channel 42
  input  wire ch42_gtyrxn_in,
  input  wire ch42_gtyrxp_in,
  output wire ch42_gtytxn_out,
  output wire ch42_gtytxp_out,

  // Serial data ports for transceiver channel 43
  input  wire ch43_gtyrxn_in,
  input  wire ch43_gtyrxp_in,
  output wire ch43_gtytxn_out,
  output wire ch43_gtytxp_out,

  // Serial data ports for transceiver channel 44
  input  wire ch44_gtyrxn_in,
  input  wire ch44_gtyrxp_in,
  output wire ch44_gtytxn_out,
  output wire ch44_gtytxp_out,

  // Serial data ports for transceiver channel 45
  input  wire ch45_gtyrxn_in,
  input  wire ch45_gtyrxp_in,
  output wire ch45_gtytxn_out,
  output wire ch45_gtytxp_out,

  // Serial data ports for transceiver channel 46
  input  wire ch46_gtyrxn_in,
  input  wire ch46_gtyrxp_in,
  output wire ch46_gtytxn_out,
  output wire ch46_gtytxp_out,

  // Serial data ports for transceiver channel 47
  input  wire ch47_gtyrxn_in,
  input  wire ch47_gtyrxp_in,
  output wire ch47_gtytxn_out,
  output wire ch47_gtytxp_out,

  // Serial data ports for transceiver channel 48
  input  wire ch48_gtyrxn_in,
  input  wire ch48_gtyrxp_in,
  output wire ch48_gtytxn_out,
  output wire ch48_gtytxp_out,

  // Serial data ports for transceiver channel 49
  input  wire ch49_gtyrxn_in,
  input  wire ch49_gtyrxp_in,
  output wire ch49_gtytxn_out,
  output wire ch49_gtytxp_out,

  // Serial data ports for transceiver channel 50
  input  wire ch50_gtyrxn_in,
  input  wire ch50_gtyrxp_in,
  output wire ch50_gtytxn_out,
  output wire ch50_gtytxp_out,

  // User-provided ports for reset helper block(s)
  input  wire hb_gtwiz_reset_clk_freerun_in,
  input  wire hb_gtwiz_reset_all_in,

  // PRBS-based link status ports
  input  wire link_down_latched_reset_in,
  output wire link_status_out,
  output reg  link_down_latched_out = 1'b1

);


  // ===================================================================================================================
  // PER-CHANNEL SIGNAL ASSIGNMENTS
  // ===================================================================================================================

  // The core and example design wrapper vectorize ports across all enabled transceiver channel and common instances for
  // simplicity and compactness. This example design top module assigns slices of each vector to individual, per-channel
  // signal vectors for use if desired. Signals which connect to helper blocks are prefixed "hb#", signals which connect
  // to transceiver common primitives are prefixed "cm#", and signals which connect to transceiver channel primitives
  // are prefixed "ch#", where "#" is the sequential resource number.

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] gtyrxn_int;
  assign gtyrxn_int[0:0] = ch0_gtyrxn_in;
  assign gtyrxn_int[1:1] = ch1_gtyrxn_in;
  assign gtyrxn_int[2:2] = ch2_gtyrxn_in;
  assign gtyrxn_int[3:3] = ch3_gtyrxn_in;
  assign gtyrxn_int[4:4] = ch4_gtyrxn_in;
  assign gtyrxn_int[5:5] = ch5_gtyrxn_in;
  assign gtyrxn_int[6:6] = ch6_gtyrxn_in;
  assign gtyrxn_int[7:7] = ch7_gtyrxn_in;
  assign gtyrxn_int[8:8] = ch8_gtyrxn_in;
  assign gtyrxn_int[9:9] = ch9_gtyrxn_in;
  assign gtyrxn_int[10:10] = ch10_gtyrxn_in;
  assign gtyrxn_int[11:11] = ch11_gtyrxn_in;
  assign gtyrxn_int[12:12] = ch12_gtyrxn_in;
  assign gtyrxn_int[13:13] = ch13_gtyrxn_in;
  assign gtyrxn_int[14:14] = ch14_gtyrxn_in;
  assign gtyrxn_int[15:15] = ch15_gtyrxn_in;
  assign gtyrxn_int[16:16] = ch16_gtyrxn_in;
  assign gtyrxn_int[17:17] = ch17_gtyrxn_in;
  assign gtyrxn_int[18:18] = ch18_gtyrxn_in;
  assign gtyrxn_int[19:19] = ch19_gtyrxn_in;
  assign gtyrxn_int[20:20] = ch20_gtyrxn_in;
  assign gtyrxn_int[21:21] = ch21_gtyrxn_in;
  assign gtyrxn_int[22:22] = ch22_gtyrxn_in;
  assign gtyrxn_int[23:23] = ch23_gtyrxn_in;
  assign gtyrxn_int[24:24] = ch24_gtyrxn_in;
  assign gtyrxn_int[25:25] = ch25_gtyrxn_in;
  assign gtyrxn_int[26:26] = ch26_gtyrxn_in;
  assign gtyrxn_int[27:27] = ch27_gtyrxn_in;
  assign gtyrxn_int[28:28] = ch28_gtyrxn_in;
  assign gtyrxn_int[29:29] = ch29_gtyrxn_in;
  assign gtyrxn_int[30:30] = ch30_gtyrxn_in;
  assign gtyrxn_int[31:31] = ch31_gtyrxn_in;
  assign gtyrxn_int[32:32] = ch32_gtyrxn_in;
  assign gtyrxn_int[33:33] = ch33_gtyrxn_in;
  assign gtyrxn_int[34:34] = ch34_gtyrxn_in;
  assign gtyrxn_int[35:35] = ch35_gtyrxn_in;
  assign gtyrxn_int[36:36] = ch36_gtyrxn_in;
  assign gtyrxn_int[37:37] = ch37_gtyrxn_in;
  assign gtyrxn_int[38:38] = ch38_gtyrxn_in;
  assign gtyrxn_int[39:39] = ch39_gtyrxn_in;
  assign gtyrxn_int[40:40] = ch40_gtyrxn_in;
  assign gtyrxn_int[41:41] = ch41_gtyrxn_in;
  assign gtyrxn_int[42:42] = ch42_gtyrxn_in;
  assign gtyrxn_int[43:43] = ch43_gtyrxn_in;
  assign gtyrxn_int[44:44] = ch44_gtyrxn_in;
  assign gtyrxn_int[45:45] = ch45_gtyrxn_in;
  assign gtyrxn_int[46:46] = ch46_gtyrxn_in;
  assign gtyrxn_int[47:47] = ch47_gtyrxn_in;
  assign gtyrxn_int[48:48] = ch48_gtyrxn_in;
  assign gtyrxn_int[49:49] = ch49_gtyrxn_in;
  assign gtyrxn_int[50:50] = ch50_gtyrxn_in;

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] gtyrxp_int;
  assign gtyrxp_int[0:0] = ch0_gtyrxp_in;
  assign gtyrxp_int[1:1] = ch1_gtyrxp_in;
  assign gtyrxp_int[2:2] = ch2_gtyrxp_in;
  assign gtyrxp_int[3:3] = ch3_gtyrxp_in;
  assign gtyrxp_int[4:4] = ch4_gtyrxp_in;
  assign gtyrxp_int[5:5] = ch5_gtyrxp_in;
  assign gtyrxp_int[6:6] = ch6_gtyrxp_in;
  assign gtyrxp_int[7:7] = ch7_gtyrxp_in;
  assign gtyrxp_int[8:8] = ch8_gtyrxp_in;
  assign gtyrxp_int[9:9] = ch9_gtyrxp_in;
  assign gtyrxp_int[10:10] = ch10_gtyrxp_in;
  assign gtyrxp_int[11:11] = ch11_gtyrxp_in;
  assign gtyrxp_int[12:12] = ch12_gtyrxp_in;
  assign gtyrxp_int[13:13] = ch13_gtyrxp_in;
  assign gtyrxp_int[14:14] = ch14_gtyrxp_in;
  assign gtyrxp_int[15:15] = ch15_gtyrxp_in;
  assign gtyrxp_int[16:16] = ch16_gtyrxp_in;
  assign gtyrxp_int[17:17] = ch17_gtyrxp_in;
  assign gtyrxp_int[18:18] = ch18_gtyrxp_in;
  assign gtyrxp_int[19:19] = ch19_gtyrxp_in;
  assign gtyrxp_int[20:20] = ch20_gtyrxp_in;
  assign gtyrxp_int[21:21] = ch21_gtyrxp_in;
  assign gtyrxp_int[22:22] = ch22_gtyrxp_in;
  assign gtyrxp_int[23:23] = ch23_gtyrxp_in;
  assign gtyrxp_int[24:24] = ch24_gtyrxp_in;
  assign gtyrxp_int[25:25] = ch25_gtyrxp_in;
  assign gtyrxp_int[26:26] = ch26_gtyrxp_in;
  assign gtyrxp_int[27:27] = ch27_gtyrxp_in;
  assign gtyrxp_int[28:28] = ch28_gtyrxp_in;
  assign gtyrxp_int[29:29] = ch29_gtyrxp_in;
  assign gtyrxp_int[30:30] = ch30_gtyrxp_in;
  assign gtyrxp_int[31:31] = ch31_gtyrxp_in;
  assign gtyrxp_int[32:32] = ch32_gtyrxp_in;
  assign gtyrxp_int[33:33] = ch33_gtyrxp_in;
  assign gtyrxp_int[34:34] = ch34_gtyrxp_in;
  assign gtyrxp_int[35:35] = ch35_gtyrxp_in;
  assign gtyrxp_int[36:36] = ch36_gtyrxp_in;
  assign gtyrxp_int[37:37] = ch37_gtyrxp_in;
  assign gtyrxp_int[38:38] = ch38_gtyrxp_in;
  assign gtyrxp_int[39:39] = ch39_gtyrxp_in;
  assign gtyrxp_int[40:40] = ch40_gtyrxp_in;
  assign gtyrxp_int[41:41] = ch41_gtyrxp_in;
  assign gtyrxp_int[42:42] = ch42_gtyrxp_in;
  assign gtyrxp_int[43:43] = ch43_gtyrxp_in;
  assign gtyrxp_int[44:44] = ch44_gtyrxp_in;
  assign gtyrxp_int[45:45] = ch45_gtyrxp_in;
  assign gtyrxp_int[46:46] = ch46_gtyrxp_in;
  assign gtyrxp_int[47:47] = ch47_gtyrxp_in;
  assign gtyrxp_int[48:48] = ch48_gtyrxp_in;
  assign gtyrxp_int[49:49] = ch49_gtyrxp_in;
  assign gtyrxp_int[50:50] = ch50_gtyrxp_in;

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] gtytxn_int;
  assign ch0_gtytxn_out = gtytxn_int[0:0];
  assign ch1_gtytxn_out = gtytxn_int[1:1];
  assign ch2_gtytxn_out = gtytxn_int[2:2];
  assign ch3_gtytxn_out = gtytxn_int[3:3];
  assign ch4_gtytxn_out = gtytxn_int[4:4];
  assign ch5_gtytxn_out = gtytxn_int[5:5];
  assign ch6_gtytxn_out = gtytxn_int[6:6];
  assign ch7_gtytxn_out = gtytxn_int[7:7];
  assign ch8_gtytxn_out = gtytxn_int[8:8];
  assign ch9_gtytxn_out = gtytxn_int[9:9];
  assign ch10_gtytxn_out = gtytxn_int[10:10];
  assign ch11_gtytxn_out = gtytxn_int[11:11];
  assign ch12_gtytxn_out = gtytxn_int[12:12];
  assign ch13_gtytxn_out = gtytxn_int[13:13];
  assign ch14_gtytxn_out = gtytxn_int[14:14];
  assign ch15_gtytxn_out = gtytxn_int[15:15];
  assign ch16_gtytxn_out = gtytxn_int[16:16];
  assign ch17_gtytxn_out = gtytxn_int[17:17];
  assign ch18_gtytxn_out = gtytxn_int[18:18];
  assign ch19_gtytxn_out = gtytxn_int[19:19];
  assign ch20_gtytxn_out = gtytxn_int[20:20];
  assign ch21_gtytxn_out = gtytxn_int[21:21];
  assign ch22_gtytxn_out = gtytxn_int[22:22];
  assign ch23_gtytxn_out = gtytxn_int[23:23];
  assign ch24_gtytxn_out = gtytxn_int[24:24];
  assign ch25_gtytxn_out = gtytxn_int[25:25];
  assign ch26_gtytxn_out = gtytxn_int[26:26];
  assign ch27_gtytxn_out = gtytxn_int[27:27];
  assign ch28_gtytxn_out = gtytxn_int[28:28];
  assign ch29_gtytxn_out = gtytxn_int[29:29];
  assign ch30_gtytxn_out = gtytxn_int[30:30];
  assign ch31_gtytxn_out = gtytxn_int[31:31];
  assign ch32_gtytxn_out = gtytxn_int[32:32];
  assign ch33_gtytxn_out = gtytxn_int[33:33];
  assign ch34_gtytxn_out = gtytxn_int[34:34];
  assign ch35_gtytxn_out = gtytxn_int[35:35];
  assign ch36_gtytxn_out = gtytxn_int[36:36];
  assign ch37_gtytxn_out = gtytxn_int[37:37];
  assign ch38_gtytxn_out = gtytxn_int[38:38];
  assign ch39_gtytxn_out = gtytxn_int[39:39];
  assign ch40_gtytxn_out = gtytxn_int[40:40];
  assign ch41_gtytxn_out = gtytxn_int[41:41];
  assign ch42_gtytxn_out = gtytxn_int[42:42];
  assign ch43_gtytxn_out = gtytxn_int[43:43];
  assign ch44_gtytxn_out = gtytxn_int[44:44];
  assign ch45_gtytxn_out = gtytxn_int[45:45];
  assign ch46_gtytxn_out = gtytxn_int[46:46];
  assign ch47_gtytxn_out = gtytxn_int[47:47];
  assign ch48_gtytxn_out = gtytxn_int[48:48];
  assign ch49_gtytxn_out = gtytxn_int[49:49];
  assign ch50_gtytxn_out = gtytxn_int[50:50];

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] gtytxp_int;
  assign ch0_gtytxp_out = gtytxp_int[0:0];
  assign ch1_gtytxp_out = gtytxp_int[1:1];
  assign ch2_gtytxp_out = gtytxp_int[2:2];
  assign ch3_gtytxp_out = gtytxp_int[3:3];
  assign ch4_gtytxp_out = gtytxp_int[4:4];
  assign ch5_gtytxp_out = gtytxp_int[5:5];
  assign ch6_gtytxp_out = gtytxp_int[6:6];
  assign ch7_gtytxp_out = gtytxp_int[7:7];
  assign ch8_gtytxp_out = gtytxp_int[8:8];
  assign ch9_gtytxp_out = gtytxp_int[9:9];
  assign ch10_gtytxp_out = gtytxp_int[10:10];
  assign ch11_gtytxp_out = gtytxp_int[11:11];
  assign ch12_gtytxp_out = gtytxp_int[12:12];
  assign ch13_gtytxp_out = gtytxp_int[13:13];
  assign ch14_gtytxp_out = gtytxp_int[14:14];
  assign ch15_gtytxp_out = gtytxp_int[15:15];
  assign ch16_gtytxp_out = gtytxp_int[16:16];
  assign ch17_gtytxp_out = gtytxp_int[17:17];
  assign ch18_gtytxp_out = gtytxp_int[18:18];
  assign ch19_gtytxp_out = gtytxp_int[19:19];
  assign ch20_gtytxp_out = gtytxp_int[20:20];
  assign ch21_gtytxp_out = gtytxp_int[21:21];
  assign ch22_gtytxp_out = gtytxp_int[22:22];
  assign ch23_gtytxp_out = gtytxp_int[23:23];
  assign ch24_gtytxp_out = gtytxp_int[24:24];
  assign ch25_gtytxp_out = gtytxp_int[25:25];
  assign ch26_gtytxp_out = gtytxp_int[26:26];
  assign ch27_gtytxp_out = gtytxp_int[27:27];
  assign ch28_gtytxp_out = gtytxp_int[28:28];
  assign ch29_gtytxp_out = gtytxp_int[29:29];
  assign ch30_gtytxp_out = gtytxp_int[30:30];
  assign ch31_gtytxp_out = gtytxp_int[31:31];
  assign ch32_gtytxp_out = gtytxp_int[32:32];
  assign ch33_gtytxp_out = gtytxp_int[33:33];
  assign ch34_gtytxp_out = gtytxp_int[34:34];
  assign ch35_gtytxp_out = gtytxp_int[35:35];
  assign ch36_gtytxp_out = gtytxp_int[36:36];
  assign ch37_gtytxp_out = gtytxp_int[37:37];
  assign ch38_gtytxp_out = gtytxp_int[38:38];
  assign ch39_gtytxp_out = gtytxp_int[39:39];
  assign ch40_gtytxp_out = gtytxp_int[40:40];
  assign ch41_gtytxp_out = gtytxp_int[41:41];
  assign ch42_gtytxp_out = gtytxp_int[42:42];
  assign ch43_gtytxp_out = gtytxp_int[43:43];
  assign ch44_gtytxp_out = gtytxp_int[44:44];
  assign ch45_gtytxp_out = gtytxp_int[45:45];
  assign ch46_gtytxp_out = gtytxp_int[46:46];
  assign ch47_gtytxp_out = gtytxp_int[47:47];
  assign ch48_gtytxp_out = gtytxp_int[48:48];
  assign ch49_gtytxp_out = gtytxp_int[49:49];
  assign ch50_gtytxp_out = gtytxp_int[50:50];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_reset_int;
  wire [0:0] hb0_gtwiz_userclk_tx_reset_int;
  assign gtwiz_userclk_tx_reset_int[0:0] = hb0_gtwiz_userclk_tx_reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_srcclk_int;
  wire [0:0] hb0_gtwiz_userclk_tx_srcclk_int;
  assign hb0_gtwiz_userclk_tx_srcclk_int = gtwiz_userclk_tx_srcclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_usrclk_int;
  wire [0:0] hb0_gtwiz_userclk_tx_usrclk_int;
  assign hb0_gtwiz_userclk_tx_usrclk_int = gtwiz_userclk_tx_usrclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_usrclk2_int;
  wire [0:0] hb0_gtwiz_userclk_tx_usrclk2_int;
  assign hb0_gtwiz_userclk_tx_usrclk2_int = gtwiz_userclk_tx_usrclk2_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_tx_active_int;
  wire [0:0] hb0_gtwiz_userclk_tx_active_int;
  assign hb0_gtwiz_userclk_tx_active_int = gtwiz_userclk_tx_active_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_reset_int;
  wire [0:0] hb0_gtwiz_userclk_rx_reset_int;
  assign gtwiz_userclk_rx_reset_int[0:0] = hb0_gtwiz_userclk_rx_reset_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_srcclk_int;
  wire [0:0] hb0_gtwiz_userclk_rx_srcclk_int;
  assign hb0_gtwiz_userclk_rx_srcclk_int = gtwiz_userclk_rx_srcclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_usrclk_int;
  wire [0:0] hb0_gtwiz_userclk_rx_usrclk_int;
  assign hb0_gtwiz_userclk_rx_usrclk_int = gtwiz_userclk_rx_usrclk_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_usrclk2_int;
  wire [0:0] hb0_gtwiz_userclk_rx_usrclk2_int;
  assign hb0_gtwiz_userclk_rx_usrclk2_int = gtwiz_userclk_rx_usrclk2_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_userclk_rx_active_int;
  wire [0:0] hb0_gtwiz_userclk_rx_active_int;
  assign hb0_gtwiz_userclk_rx_active_int = gtwiz_userclk_rx_active_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_clk_freerun_int;
  wire [0:0] hb0_gtwiz_reset_clk_freerun_int = 1'b0;
  assign gtwiz_reset_clk_freerun_int[0:0] = hb0_gtwiz_reset_clk_freerun_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_all_int;
  wire [0:0] hb0_gtwiz_reset_all_int = 1'b0;
  assign gtwiz_reset_all_int[0:0] = hb0_gtwiz_reset_all_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_pll_and_datapath_int;
  wire [0:0] hb0_gtwiz_reset_tx_pll_and_datapath_int;
  assign gtwiz_reset_tx_pll_and_datapath_int[0:0] = hb0_gtwiz_reset_tx_pll_and_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_datapath_int;
  wire [0:0] hb0_gtwiz_reset_tx_datapath_int;
  assign gtwiz_reset_tx_datapath_int[0:0] = hb0_gtwiz_reset_tx_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_pll_and_datapath_int;
  wire [0:0] hb0_gtwiz_reset_rx_pll_and_datapath_int = 1'b0;
  assign gtwiz_reset_rx_pll_and_datapath_int[0:0] = hb0_gtwiz_reset_rx_pll_and_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_datapath_int;
  wire [0:0] hb0_gtwiz_reset_rx_datapath_int = 1'b0;
  assign gtwiz_reset_rx_datapath_int[0:0] = hb0_gtwiz_reset_rx_datapath_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_cdr_stable_int;
  wire [0:0] hb0_gtwiz_reset_rx_cdr_stable_int;
  assign hb0_gtwiz_reset_rx_cdr_stable_int = gtwiz_reset_rx_cdr_stable_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_tx_done_int;
  wire [0:0] hb0_gtwiz_reset_tx_done_int;
  assign hb0_gtwiz_reset_tx_done_int = gtwiz_reset_tx_done_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [0:0] gtwiz_reset_rx_done_int;
  wire [0:0] hb0_gtwiz_reset_rx_done_int;
  assign hb0_gtwiz_reset_rx_done_int = gtwiz_reset_rx_done_int[0:0];

  //--------------------------------------------------------------------------------------------------------------------
  wire [1631:0] gtwiz_userdata_tx_int;
  wire [31:0] hb0_gtwiz_userdata_tx_int;
  wire [31:0] hb1_gtwiz_userdata_tx_int;
  wire [31:0] hb2_gtwiz_userdata_tx_int;
  wire [31:0] hb3_gtwiz_userdata_tx_int;
  wire [31:0] hb4_gtwiz_userdata_tx_int;
  wire [31:0] hb5_gtwiz_userdata_tx_int;
  wire [31:0] hb6_gtwiz_userdata_tx_int;
  wire [31:0] hb7_gtwiz_userdata_tx_int;
  wire [31:0] hb8_gtwiz_userdata_tx_int;
  wire [31:0] hb9_gtwiz_userdata_tx_int;
  wire [31:0] hb10_gtwiz_userdata_tx_int;
  wire [31:0] hb11_gtwiz_userdata_tx_int;
  wire [31:0] hb12_gtwiz_userdata_tx_int;
  wire [31:0] hb13_gtwiz_userdata_tx_int;
  wire [31:0] hb14_gtwiz_userdata_tx_int;
  wire [31:0] hb15_gtwiz_userdata_tx_int;
  wire [31:0] hb16_gtwiz_userdata_tx_int;
  wire [31:0] hb17_gtwiz_userdata_tx_int;
  wire [31:0] hb18_gtwiz_userdata_tx_int;
  wire [31:0] hb19_gtwiz_userdata_tx_int;
  wire [31:0] hb20_gtwiz_userdata_tx_int;
  wire [31:0] hb21_gtwiz_userdata_tx_int;
  wire [31:0] hb22_gtwiz_userdata_tx_int;
  wire [31:0] hb23_gtwiz_userdata_tx_int;
  wire [31:0] hb24_gtwiz_userdata_tx_int;
  wire [31:0] hb25_gtwiz_userdata_tx_int;
  wire [31:0] hb26_gtwiz_userdata_tx_int;
  wire [31:0] hb27_gtwiz_userdata_tx_int;
  wire [31:0] hb28_gtwiz_userdata_tx_int;
  wire [31:0] hb29_gtwiz_userdata_tx_int;
  wire [31:0] hb30_gtwiz_userdata_tx_int;
  wire [31:0] hb31_gtwiz_userdata_tx_int;
  wire [31:0] hb32_gtwiz_userdata_tx_int;
  wire [31:0] hb33_gtwiz_userdata_tx_int;
  wire [31:0] hb34_gtwiz_userdata_tx_int;
  wire [31:0] hb35_gtwiz_userdata_tx_int;
  wire [31:0] hb36_gtwiz_userdata_tx_int;
  wire [31:0] hb37_gtwiz_userdata_tx_int;
  wire [31:0] hb38_gtwiz_userdata_tx_int;
  wire [31:0] hb39_gtwiz_userdata_tx_int;
  wire [31:0] hb40_gtwiz_userdata_tx_int;
  wire [31:0] hb41_gtwiz_userdata_tx_int;
  wire [31:0] hb42_gtwiz_userdata_tx_int;
  wire [31:0] hb43_gtwiz_userdata_tx_int;
  wire [31:0] hb44_gtwiz_userdata_tx_int;
  wire [31:0] hb45_gtwiz_userdata_tx_int;
  wire [31:0] hb46_gtwiz_userdata_tx_int;
  wire [31:0] hb47_gtwiz_userdata_tx_int;
  wire [31:0] hb48_gtwiz_userdata_tx_int;
  wire [31:0] hb49_gtwiz_userdata_tx_int;
  wire [31:0] hb50_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[31:0] = hb0_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[63:32] = hb1_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[95:64] = hb2_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[127:96] = hb3_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[159:128] = hb4_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[191:160] = hb5_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[223:192] = hb6_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[255:224] = hb7_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[287:256] = hb8_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[319:288] = hb9_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[351:320] = hb10_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[383:352] = hb11_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[415:384] = hb12_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[447:416] = hb13_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[479:448] = hb14_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[511:480] = hb15_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[543:512] = hb16_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[575:544] = hb17_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[607:576] = hb18_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[639:608] = hb19_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[671:640] = hb20_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[703:672] = hb21_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[735:704] = hb22_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[767:736] = hb23_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[799:768] = hb24_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[831:800] = hb25_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[863:832] = hb26_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[895:864] = hb27_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[927:896] = hb28_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[959:928] = hb29_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[991:960] = hb30_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1023:992] = hb31_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1055:1024] = hb32_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1087:1056] = hb33_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1119:1088] = hb34_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1151:1120] = hb35_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1183:1152] = hb36_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1215:1184] = hb37_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1247:1216] = hb38_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1279:1248] = hb39_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1311:1280] = hb40_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1343:1312] = hb41_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1375:1344] = hb42_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1407:1376] = hb43_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1439:1408] = hb44_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1471:1440] = hb45_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1503:1472] = hb46_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1535:1504] = hb47_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1567:1536] = hb48_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1599:1568] = hb49_gtwiz_userdata_tx_int;
  assign gtwiz_userdata_tx_int[1631:1600] = hb50_gtwiz_userdata_tx_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [1631:0] gtwiz_userdata_rx_int;
  wire [31:0] hb0_gtwiz_userdata_rx_int;
  wire [31:0] hb1_gtwiz_userdata_rx_int;
  wire [31:0] hb2_gtwiz_userdata_rx_int;
  wire [31:0] hb3_gtwiz_userdata_rx_int;
  wire [31:0] hb4_gtwiz_userdata_rx_int;
  wire [31:0] hb5_gtwiz_userdata_rx_int;
  wire [31:0] hb6_gtwiz_userdata_rx_int;
  wire [31:0] hb7_gtwiz_userdata_rx_int;
  wire [31:0] hb8_gtwiz_userdata_rx_int;
  wire [31:0] hb9_gtwiz_userdata_rx_int;
  wire [31:0] hb10_gtwiz_userdata_rx_int;
  wire [31:0] hb11_gtwiz_userdata_rx_int;
  wire [31:0] hb12_gtwiz_userdata_rx_int;
  wire [31:0] hb13_gtwiz_userdata_rx_int;
  wire [31:0] hb14_gtwiz_userdata_rx_int;
  wire [31:0] hb15_gtwiz_userdata_rx_int;
  wire [31:0] hb16_gtwiz_userdata_rx_int;
  wire [31:0] hb17_gtwiz_userdata_rx_int;
  wire [31:0] hb18_gtwiz_userdata_rx_int;
  wire [31:0] hb19_gtwiz_userdata_rx_int;
  wire [31:0] hb20_gtwiz_userdata_rx_int;
  wire [31:0] hb21_gtwiz_userdata_rx_int;
  wire [31:0] hb22_gtwiz_userdata_rx_int;
  wire [31:0] hb23_gtwiz_userdata_rx_int;
  wire [31:0] hb24_gtwiz_userdata_rx_int;
  wire [31:0] hb25_gtwiz_userdata_rx_int;
  wire [31:0] hb26_gtwiz_userdata_rx_int;
  wire [31:0] hb27_gtwiz_userdata_rx_int;
  wire [31:0] hb28_gtwiz_userdata_rx_int;
  wire [31:0] hb29_gtwiz_userdata_rx_int;
  wire [31:0] hb30_gtwiz_userdata_rx_int;
  wire [31:0] hb31_gtwiz_userdata_rx_int;
  wire [31:0] hb32_gtwiz_userdata_rx_int;
  wire [31:0] hb33_gtwiz_userdata_rx_int;
  wire [31:0] hb34_gtwiz_userdata_rx_int;
  wire [31:0] hb35_gtwiz_userdata_rx_int;
  wire [31:0] hb36_gtwiz_userdata_rx_int;
  wire [31:0] hb37_gtwiz_userdata_rx_int;
  wire [31:0] hb38_gtwiz_userdata_rx_int;
  wire [31:0] hb39_gtwiz_userdata_rx_int;
  wire [31:0] hb40_gtwiz_userdata_rx_int;
  wire [31:0] hb41_gtwiz_userdata_rx_int;
  wire [31:0] hb42_gtwiz_userdata_rx_int;
  wire [31:0] hb43_gtwiz_userdata_rx_int;
  wire [31:0] hb44_gtwiz_userdata_rx_int;
  wire [31:0] hb45_gtwiz_userdata_rx_int;
  wire [31:0] hb46_gtwiz_userdata_rx_int;
  wire [31:0] hb47_gtwiz_userdata_rx_int;
  wire [31:0] hb48_gtwiz_userdata_rx_int;
  wire [31:0] hb49_gtwiz_userdata_rx_int;
  wire [31:0] hb50_gtwiz_userdata_rx_int;
  assign hb0_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[31:0];
  assign hb1_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[63:32];
  assign hb2_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[95:64];
  assign hb3_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[127:96];
  assign hb4_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[159:128];
  assign hb5_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[191:160];
  assign hb6_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[223:192];
  assign hb7_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[255:224];
  assign hb8_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[287:256];
  assign hb9_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[319:288];
  assign hb10_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[351:320];
  assign hb11_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[383:352];
  assign hb12_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[415:384];
  assign hb13_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[447:416];
  assign hb14_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[479:448];
  assign hb15_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[511:480];
  assign hb16_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[543:512];
  assign hb17_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[575:544];
  assign hb18_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[607:576];
  assign hb19_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[639:608];
  assign hb20_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[671:640];
  assign hb21_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[703:672];
  assign hb22_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[735:704];
  assign hb23_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[767:736];
  assign hb24_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[799:768];
  assign hb25_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[831:800];
  assign hb26_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[863:832];
  assign hb27_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[895:864];
  assign hb28_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[927:896];
  assign hb29_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[959:928];
  assign hb30_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[991:960];
  assign hb31_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1023:992];
  assign hb32_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1055:1024];
  assign hb33_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1087:1056];
  assign hb34_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1119:1088];
  assign hb35_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1151:1120];
  assign hb36_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1183:1152];
  assign hb37_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1215:1184];
  assign hb38_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1247:1216];
  assign hb39_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1279:1248];
  assign hb40_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1311:1280];
  assign hb41_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1343:1312];
  assign hb42_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1375:1344];
  assign hb43_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1407:1376];
  assign hb44_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1439:1408];
  assign hb45_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1471:1440];
  assign hb46_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1503:1472];
  assign hb47_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1535:1504];
  assign hb48_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1567:1536];
  assign hb49_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1599:1568];
  assign hb50_gtwiz_userdata_rx_int = gtwiz_userdata_rx_int[1631:1600];

  //--------------------------------------------------------------------------------------------------------------------
  wire [12:0] gtrefclk00_int;
  wire [0:0] cm0_gtrefclk00_int;
  wire [0:0] cm1_gtrefclk00_int;
  wire [0:0] cm2_gtrefclk00_int;
  wire [0:0] cm3_gtrefclk00_int;
  wire [0:0] cm4_gtrefclk00_int;
  wire [0:0] cm5_gtrefclk00_int;
  wire [0:0] cm6_gtrefclk00_int;
  wire [0:0] cm7_gtrefclk00_int;
  wire [0:0] cm8_gtrefclk00_int;
  wire [0:0] cm9_gtrefclk00_int;
  wire [0:0] cm10_gtrefclk00_int;
  wire [0:0] cm11_gtrefclk00_int;
  wire [0:0] cm12_gtrefclk00_int;
  assign gtrefclk00_int[0:0] = cm0_gtrefclk00_int;
  assign gtrefclk00_int[1:1] = cm1_gtrefclk00_int;
  assign gtrefclk00_int[2:2] = cm2_gtrefclk00_int;
  assign gtrefclk00_int[3:3] = cm3_gtrefclk00_int;
  assign gtrefclk00_int[4:4] = cm4_gtrefclk00_int;
  assign gtrefclk00_int[5:5] = cm5_gtrefclk00_int;
  assign gtrefclk00_int[6:6] = cm6_gtrefclk00_int;
  assign gtrefclk00_int[7:7] = cm7_gtrefclk00_int;
  assign gtrefclk00_int[8:8] = cm8_gtrefclk00_int;
  assign gtrefclk00_int[9:9] = cm9_gtrefclk00_int;
  assign gtrefclk00_int[10:10] = cm10_gtrefclk00_int;
  assign gtrefclk00_int[11:11] = cm11_gtrefclk00_int;
  assign gtrefclk00_int[12:12] = cm12_gtrefclk00_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [12:0] qpll0outclk_int;
  wire [0:0] cm0_qpll0outclk_int;
  wire [0:0] cm1_qpll0outclk_int;
  wire [0:0] cm2_qpll0outclk_int;
  wire [0:0] cm3_qpll0outclk_int;
  wire [0:0] cm4_qpll0outclk_int;
  wire [0:0] cm5_qpll0outclk_int;
  wire [0:0] cm6_qpll0outclk_int;
  wire [0:0] cm7_qpll0outclk_int;
  wire [0:0] cm8_qpll0outclk_int;
  wire [0:0] cm9_qpll0outclk_int;
  wire [0:0] cm10_qpll0outclk_int;
  wire [0:0] cm11_qpll0outclk_int;
  wire [0:0] cm12_qpll0outclk_int;
  assign cm0_qpll0outclk_int = qpll0outclk_int[0:0];
  assign cm1_qpll0outclk_int = qpll0outclk_int[1:1];
  assign cm2_qpll0outclk_int = qpll0outclk_int[2:2];
  assign cm3_qpll0outclk_int = qpll0outclk_int[3:3];
  assign cm4_qpll0outclk_int = qpll0outclk_int[4:4];
  assign cm5_qpll0outclk_int = qpll0outclk_int[5:5];
  assign cm6_qpll0outclk_int = qpll0outclk_int[6:6];
  assign cm7_qpll0outclk_int = qpll0outclk_int[7:7];
  assign cm8_qpll0outclk_int = qpll0outclk_int[8:8];
  assign cm9_qpll0outclk_int = qpll0outclk_int[9:9];
  assign cm10_qpll0outclk_int = qpll0outclk_int[10:10];
  assign cm11_qpll0outclk_int = qpll0outclk_int[11:11];
  assign cm12_qpll0outclk_int = qpll0outclk_int[12:12];

  //--------------------------------------------------------------------------------------------------------------------
  wire [12:0] qpll0outrefclk_int;
  wire [0:0] cm0_qpll0outrefclk_int;
  wire [0:0] cm1_qpll0outrefclk_int;
  wire [0:0] cm2_qpll0outrefclk_int;
  wire [0:0] cm3_qpll0outrefclk_int;
  wire [0:0] cm4_qpll0outrefclk_int;
  wire [0:0] cm5_qpll0outrefclk_int;
  wire [0:0] cm6_qpll0outrefclk_int;
  wire [0:0] cm7_qpll0outrefclk_int;
  wire [0:0] cm8_qpll0outrefclk_int;
  wire [0:0] cm9_qpll0outrefclk_int;
  wire [0:0] cm10_qpll0outrefclk_int;
  wire [0:0] cm11_qpll0outrefclk_int;
  wire [0:0] cm12_qpll0outrefclk_int;
  assign cm0_qpll0outrefclk_int = qpll0outrefclk_int[0:0];
  assign cm1_qpll0outrefclk_int = qpll0outrefclk_int[1:1];
  assign cm2_qpll0outrefclk_int = qpll0outrefclk_int[2:2];
  assign cm3_qpll0outrefclk_int = qpll0outrefclk_int[3:3];
  assign cm4_qpll0outrefclk_int = qpll0outrefclk_int[4:4];
  assign cm5_qpll0outrefclk_int = qpll0outrefclk_int[5:5];
  assign cm6_qpll0outrefclk_int = qpll0outrefclk_int[6:6];
  assign cm7_qpll0outrefclk_int = qpll0outrefclk_int[7:7];
  assign cm8_qpll0outrefclk_int = qpll0outrefclk_int[8:8];
  assign cm9_qpll0outrefclk_int = qpll0outrefclk_int[9:9];
  assign cm10_qpll0outrefclk_int = qpll0outrefclk_int[10:10];
  assign cm11_qpll0outrefclk_int = qpll0outrefclk_int[11:11];
  assign cm12_qpll0outrefclk_int = qpll0outrefclk_int[12:12];

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] rxslide_int;
  wire [0:0] ch0_rxslide_int = 1'b0;
  wire [0:0] ch1_rxslide_int = 1'b0;
  wire [0:0] ch2_rxslide_int = 1'b0;
  wire [0:0] ch3_rxslide_int = 1'b0;
  wire [0:0] ch4_rxslide_int = 1'b0;
  wire [0:0] ch5_rxslide_int = 1'b0;
  wire [0:0] ch6_rxslide_int = 1'b0;
  wire [0:0] ch7_rxslide_int = 1'b0;
  wire [0:0] ch8_rxslide_int = 1'b0;
  wire [0:0] ch9_rxslide_int = 1'b0;
  wire [0:0] ch10_rxslide_int = 1'b0;
  wire [0:0] ch11_rxslide_int = 1'b0;
  wire [0:0] ch12_rxslide_int = 1'b0;
  wire [0:0] ch13_rxslide_int = 1'b0;
  wire [0:0] ch14_rxslide_int = 1'b0;
  wire [0:0] ch15_rxslide_int = 1'b0;
  wire [0:0] ch16_rxslide_int = 1'b0;
  wire [0:0] ch17_rxslide_int = 1'b0;
  wire [0:0] ch18_rxslide_int = 1'b0;
  wire [0:0] ch19_rxslide_int = 1'b0;
  wire [0:0] ch20_rxslide_int = 1'b0;
  wire [0:0] ch21_rxslide_int = 1'b0;
  wire [0:0] ch22_rxslide_int = 1'b0;
  wire [0:0] ch23_rxslide_int = 1'b0;
  wire [0:0] ch24_rxslide_int = 1'b0;
  wire [0:0] ch25_rxslide_int = 1'b0;
  wire [0:0] ch26_rxslide_int = 1'b0;
  wire [0:0] ch27_rxslide_int = 1'b0;
  wire [0:0] ch28_rxslide_int = 1'b0;
  wire [0:0] ch29_rxslide_int = 1'b0;
  wire [0:0] ch30_rxslide_int = 1'b0;
  wire [0:0] ch31_rxslide_int = 1'b0;
  wire [0:0] ch32_rxslide_int = 1'b0;
  wire [0:0] ch33_rxslide_int = 1'b0;
  wire [0:0] ch34_rxslide_int = 1'b0;
  wire [0:0] ch35_rxslide_int = 1'b0;
  wire [0:0] ch36_rxslide_int = 1'b0;
  wire [0:0] ch37_rxslide_int = 1'b0;
  wire [0:0] ch38_rxslide_int = 1'b0;
  wire [0:0] ch39_rxslide_int = 1'b0;
  wire [0:0] ch40_rxslide_int = 1'b0;
  wire [0:0] ch41_rxslide_int = 1'b0;
  wire [0:0] ch42_rxslide_int = 1'b0;
  wire [0:0] ch43_rxslide_int = 1'b0;
  wire [0:0] ch44_rxslide_int = 1'b0;
  wire [0:0] ch45_rxslide_int = 1'b0;
  wire [0:0] ch46_rxslide_int = 1'b0;
  wire [0:0] ch47_rxslide_int = 1'b0;
  wire [0:0] ch48_rxslide_int = 1'b0;
  wire [0:0] ch49_rxslide_int = 1'b0;
  wire [0:0] ch50_rxslide_int = 1'b0;
  assign rxslide_int[0:0] = ch0_rxslide_int;
  assign rxslide_int[1:1] = ch1_rxslide_int;
  assign rxslide_int[2:2] = ch2_rxslide_int;
  assign rxslide_int[3:3] = ch3_rxslide_int;
  assign rxslide_int[4:4] = ch4_rxslide_int;
  assign rxslide_int[5:5] = ch5_rxslide_int;
  assign rxslide_int[6:6] = ch6_rxslide_int;
  assign rxslide_int[7:7] = ch7_rxslide_int;
  assign rxslide_int[8:8] = ch8_rxslide_int;
  assign rxslide_int[9:9] = ch9_rxslide_int;
  assign rxslide_int[10:10] = ch10_rxslide_int;
  assign rxslide_int[11:11] = ch11_rxslide_int;
  assign rxslide_int[12:12] = ch12_rxslide_int;
  assign rxslide_int[13:13] = ch13_rxslide_int;
  assign rxslide_int[14:14] = ch14_rxslide_int;
  assign rxslide_int[15:15] = ch15_rxslide_int;
  assign rxslide_int[16:16] = ch16_rxslide_int;
  assign rxslide_int[17:17] = ch17_rxslide_int;
  assign rxslide_int[18:18] = ch18_rxslide_int;
  assign rxslide_int[19:19] = ch19_rxslide_int;
  assign rxslide_int[20:20] = ch20_rxslide_int;
  assign rxslide_int[21:21] = ch21_rxslide_int;
  assign rxslide_int[22:22] = ch22_rxslide_int;
  assign rxslide_int[23:23] = ch23_rxslide_int;
  assign rxslide_int[24:24] = ch24_rxslide_int;
  assign rxslide_int[25:25] = ch25_rxslide_int;
  assign rxslide_int[26:26] = ch26_rxslide_int;
  assign rxslide_int[27:27] = ch27_rxslide_int;
  assign rxslide_int[28:28] = ch28_rxslide_int;
  assign rxslide_int[29:29] = ch29_rxslide_int;
  assign rxslide_int[30:30] = ch30_rxslide_int;
  assign rxslide_int[31:31] = ch31_rxslide_int;
  assign rxslide_int[32:32] = ch32_rxslide_int;
  assign rxslide_int[33:33] = ch33_rxslide_int;
  assign rxslide_int[34:34] = ch34_rxslide_int;
  assign rxslide_int[35:35] = ch35_rxslide_int;
  assign rxslide_int[36:36] = ch36_rxslide_int;
  assign rxslide_int[37:37] = ch37_rxslide_int;
  assign rxslide_int[38:38] = ch38_rxslide_int;
  assign rxslide_int[39:39] = ch39_rxslide_int;
  assign rxslide_int[40:40] = ch40_rxslide_int;
  assign rxslide_int[41:41] = ch41_rxslide_int;
  assign rxslide_int[42:42] = ch42_rxslide_int;
  assign rxslide_int[43:43] = ch43_rxslide_int;
  assign rxslide_int[44:44] = ch44_rxslide_int;
  assign rxslide_int[45:45] = ch45_rxslide_int;
  assign rxslide_int[46:46] = ch46_rxslide_int;
  assign rxslide_int[47:47] = ch47_rxslide_int;
  assign rxslide_int[48:48] = ch48_rxslide_int;
  assign rxslide_int[49:49] = ch49_rxslide_int;
  assign rxslide_int[50:50] = ch50_rxslide_int;

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] gtpowergood_int;
  wire [0:0] ch0_gtpowergood_int;
  wire [0:0] ch1_gtpowergood_int;
  wire [0:0] ch2_gtpowergood_int;
  wire [0:0] ch3_gtpowergood_int;
  wire [0:0] ch4_gtpowergood_int;
  wire [0:0] ch5_gtpowergood_int;
  wire [0:0] ch6_gtpowergood_int;
  wire [0:0] ch7_gtpowergood_int;
  wire [0:0] ch8_gtpowergood_int;
  wire [0:0] ch9_gtpowergood_int;
  wire [0:0] ch10_gtpowergood_int;
  wire [0:0] ch11_gtpowergood_int;
  wire [0:0] ch12_gtpowergood_int;
  wire [0:0] ch13_gtpowergood_int;
  wire [0:0] ch14_gtpowergood_int;
  wire [0:0] ch15_gtpowergood_int;
  wire [0:0] ch16_gtpowergood_int;
  wire [0:0] ch17_gtpowergood_int;
  wire [0:0] ch18_gtpowergood_int;
  wire [0:0] ch19_gtpowergood_int;
  wire [0:0] ch20_gtpowergood_int;
  wire [0:0] ch21_gtpowergood_int;
  wire [0:0] ch22_gtpowergood_int;
  wire [0:0] ch23_gtpowergood_int;
  wire [0:0] ch24_gtpowergood_int;
  wire [0:0] ch25_gtpowergood_int;
  wire [0:0] ch26_gtpowergood_int;
  wire [0:0] ch27_gtpowergood_int;
  wire [0:0] ch28_gtpowergood_int;
  wire [0:0] ch29_gtpowergood_int;
  wire [0:0] ch30_gtpowergood_int;
  wire [0:0] ch31_gtpowergood_int;
  wire [0:0] ch32_gtpowergood_int;
  wire [0:0] ch33_gtpowergood_int;
  wire [0:0] ch34_gtpowergood_int;
  wire [0:0] ch35_gtpowergood_int;
  wire [0:0] ch36_gtpowergood_int;
  wire [0:0] ch37_gtpowergood_int;
  wire [0:0] ch38_gtpowergood_int;
  wire [0:0] ch39_gtpowergood_int;
  wire [0:0] ch40_gtpowergood_int;
  wire [0:0] ch41_gtpowergood_int;
  wire [0:0] ch42_gtpowergood_int;
  wire [0:0] ch43_gtpowergood_int;
  wire [0:0] ch44_gtpowergood_int;
  wire [0:0] ch45_gtpowergood_int;
  wire [0:0] ch46_gtpowergood_int;
  wire [0:0] ch47_gtpowergood_int;
  wire [0:0] ch48_gtpowergood_int;
  wire [0:0] ch49_gtpowergood_int;
  wire [0:0] ch50_gtpowergood_int;
  assign ch0_gtpowergood_int = gtpowergood_int[0:0];
  assign ch1_gtpowergood_int = gtpowergood_int[1:1];
  assign ch2_gtpowergood_int = gtpowergood_int[2:2];
  assign ch3_gtpowergood_int = gtpowergood_int[3:3];
  assign ch4_gtpowergood_int = gtpowergood_int[4:4];
  assign ch5_gtpowergood_int = gtpowergood_int[5:5];
  assign ch6_gtpowergood_int = gtpowergood_int[6:6];
  assign ch7_gtpowergood_int = gtpowergood_int[7:7];
  assign ch8_gtpowergood_int = gtpowergood_int[8:8];
  assign ch9_gtpowergood_int = gtpowergood_int[9:9];
  assign ch10_gtpowergood_int = gtpowergood_int[10:10];
  assign ch11_gtpowergood_int = gtpowergood_int[11:11];
  assign ch12_gtpowergood_int = gtpowergood_int[12:12];
  assign ch13_gtpowergood_int = gtpowergood_int[13:13];
  assign ch14_gtpowergood_int = gtpowergood_int[14:14];
  assign ch15_gtpowergood_int = gtpowergood_int[15:15];
  assign ch16_gtpowergood_int = gtpowergood_int[16:16];
  assign ch17_gtpowergood_int = gtpowergood_int[17:17];
  assign ch18_gtpowergood_int = gtpowergood_int[18:18];
  assign ch19_gtpowergood_int = gtpowergood_int[19:19];
  assign ch20_gtpowergood_int = gtpowergood_int[20:20];
  assign ch21_gtpowergood_int = gtpowergood_int[21:21];
  assign ch22_gtpowergood_int = gtpowergood_int[22:22];
  assign ch23_gtpowergood_int = gtpowergood_int[23:23];
  assign ch24_gtpowergood_int = gtpowergood_int[24:24];
  assign ch25_gtpowergood_int = gtpowergood_int[25:25];
  assign ch26_gtpowergood_int = gtpowergood_int[26:26];
  assign ch27_gtpowergood_int = gtpowergood_int[27:27];
  assign ch28_gtpowergood_int = gtpowergood_int[28:28];
  assign ch29_gtpowergood_int = gtpowergood_int[29:29];
  assign ch30_gtpowergood_int = gtpowergood_int[30:30];
  assign ch31_gtpowergood_int = gtpowergood_int[31:31];
  assign ch32_gtpowergood_int = gtpowergood_int[32:32];
  assign ch33_gtpowergood_int = gtpowergood_int[33:33];
  assign ch34_gtpowergood_int = gtpowergood_int[34:34];
  assign ch35_gtpowergood_int = gtpowergood_int[35:35];
  assign ch36_gtpowergood_int = gtpowergood_int[36:36];
  assign ch37_gtpowergood_int = gtpowergood_int[37:37];
  assign ch38_gtpowergood_int = gtpowergood_int[38:38];
  assign ch39_gtpowergood_int = gtpowergood_int[39:39];
  assign ch40_gtpowergood_int = gtpowergood_int[40:40];
  assign ch41_gtpowergood_int = gtpowergood_int[41:41];
  assign ch42_gtpowergood_int = gtpowergood_int[42:42];
  assign ch43_gtpowergood_int = gtpowergood_int[43:43];
  assign ch44_gtpowergood_int = gtpowergood_int[44:44];
  assign ch45_gtpowergood_int = gtpowergood_int[45:45];
  assign ch46_gtpowergood_int = gtpowergood_int[46:46];
  assign ch47_gtpowergood_int = gtpowergood_int[47:47];
  assign ch48_gtpowergood_int = gtpowergood_int[48:48];
  assign ch49_gtpowergood_int = gtpowergood_int[49:49];
  assign ch50_gtpowergood_int = gtpowergood_int[50:50];

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] rxpmaresetdone_int;
  wire [0:0] ch0_rxpmaresetdone_int;
  wire [0:0] ch1_rxpmaresetdone_int;
  wire [0:0] ch2_rxpmaresetdone_int;
  wire [0:0] ch3_rxpmaresetdone_int;
  wire [0:0] ch4_rxpmaresetdone_int;
  wire [0:0] ch5_rxpmaresetdone_int;
  wire [0:0] ch6_rxpmaresetdone_int;
  wire [0:0] ch7_rxpmaresetdone_int;
  wire [0:0] ch8_rxpmaresetdone_int;
  wire [0:0] ch9_rxpmaresetdone_int;
  wire [0:0] ch10_rxpmaresetdone_int;
  wire [0:0] ch11_rxpmaresetdone_int;
  wire [0:0] ch12_rxpmaresetdone_int;
  wire [0:0] ch13_rxpmaresetdone_int;
  wire [0:0] ch14_rxpmaresetdone_int;
  wire [0:0] ch15_rxpmaresetdone_int;
  wire [0:0] ch16_rxpmaresetdone_int;
  wire [0:0] ch17_rxpmaresetdone_int;
  wire [0:0] ch18_rxpmaresetdone_int;
  wire [0:0] ch19_rxpmaresetdone_int;
  wire [0:0] ch20_rxpmaresetdone_int;
  wire [0:0] ch21_rxpmaresetdone_int;
  wire [0:0] ch22_rxpmaresetdone_int;
  wire [0:0] ch23_rxpmaresetdone_int;
  wire [0:0] ch24_rxpmaresetdone_int;
  wire [0:0] ch25_rxpmaresetdone_int;
  wire [0:0] ch26_rxpmaresetdone_int;
  wire [0:0] ch27_rxpmaresetdone_int;
  wire [0:0] ch28_rxpmaresetdone_int;
  wire [0:0] ch29_rxpmaresetdone_int;
  wire [0:0] ch30_rxpmaresetdone_int;
  wire [0:0] ch31_rxpmaresetdone_int;
  wire [0:0] ch32_rxpmaresetdone_int;
  wire [0:0] ch33_rxpmaresetdone_int;
  wire [0:0] ch34_rxpmaresetdone_int;
  wire [0:0] ch35_rxpmaresetdone_int;
  wire [0:0] ch36_rxpmaresetdone_int;
  wire [0:0] ch37_rxpmaresetdone_int;
  wire [0:0] ch38_rxpmaresetdone_int;
  wire [0:0] ch39_rxpmaresetdone_int;
  wire [0:0] ch40_rxpmaresetdone_int;
  wire [0:0] ch41_rxpmaresetdone_int;
  wire [0:0] ch42_rxpmaresetdone_int;
  wire [0:0] ch43_rxpmaresetdone_int;
  wire [0:0] ch44_rxpmaresetdone_int;
  wire [0:0] ch45_rxpmaresetdone_int;
  wire [0:0] ch46_rxpmaresetdone_int;
  wire [0:0] ch47_rxpmaresetdone_int;
  wire [0:0] ch48_rxpmaresetdone_int;
  wire [0:0] ch49_rxpmaresetdone_int;
  wire [0:0] ch50_rxpmaresetdone_int;
  assign ch0_rxpmaresetdone_int = rxpmaresetdone_int[0:0];
  assign ch1_rxpmaresetdone_int = rxpmaresetdone_int[1:1];
  assign ch2_rxpmaresetdone_int = rxpmaresetdone_int[2:2];
  assign ch3_rxpmaresetdone_int = rxpmaresetdone_int[3:3];
  assign ch4_rxpmaresetdone_int = rxpmaresetdone_int[4:4];
  assign ch5_rxpmaresetdone_int = rxpmaresetdone_int[5:5];
  assign ch6_rxpmaresetdone_int = rxpmaresetdone_int[6:6];
  assign ch7_rxpmaresetdone_int = rxpmaresetdone_int[7:7];
  assign ch8_rxpmaresetdone_int = rxpmaresetdone_int[8:8];
  assign ch9_rxpmaresetdone_int = rxpmaresetdone_int[9:9];
  assign ch10_rxpmaresetdone_int = rxpmaresetdone_int[10:10];
  assign ch11_rxpmaresetdone_int = rxpmaresetdone_int[11:11];
  assign ch12_rxpmaresetdone_int = rxpmaresetdone_int[12:12];
  assign ch13_rxpmaresetdone_int = rxpmaresetdone_int[13:13];
  assign ch14_rxpmaresetdone_int = rxpmaresetdone_int[14:14];
  assign ch15_rxpmaresetdone_int = rxpmaresetdone_int[15:15];
  assign ch16_rxpmaresetdone_int = rxpmaresetdone_int[16:16];
  assign ch17_rxpmaresetdone_int = rxpmaresetdone_int[17:17];
  assign ch18_rxpmaresetdone_int = rxpmaresetdone_int[18:18];
  assign ch19_rxpmaresetdone_int = rxpmaresetdone_int[19:19];
  assign ch20_rxpmaresetdone_int = rxpmaresetdone_int[20:20];
  assign ch21_rxpmaresetdone_int = rxpmaresetdone_int[21:21];
  assign ch22_rxpmaresetdone_int = rxpmaresetdone_int[22:22];
  assign ch23_rxpmaresetdone_int = rxpmaresetdone_int[23:23];
  assign ch24_rxpmaresetdone_int = rxpmaresetdone_int[24:24];
  assign ch25_rxpmaresetdone_int = rxpmaresetdone_int[25:25];
  assign ch26_rxpmaresetdone_int = rxpmaresetdone_int[26:26];
  assign ch27_rxpmaresetdone_int = rxpmaresetdone_int[27:27];
  assign ch28_rxpmaresetdone_int = rxpmaresetdone_int[28:28];
  assign ch29_rxpmaresetdone_int = rxpmaresetdone_int[29:29];
  assign ch30_rxpmaresetdone_int = rxpmaresetdone_int[30:30];
  assign ch31_rxpmaresetdone_int = rxpmaresetdone_int[31:31];
  assign ch32_rxpmaresetdone_int = rxpmaresetdone_int[32:32];
  assign ch33_rxpmaresetdone_int = rxpmaresetdone_int[33:33];
  assign ch34_rxpmaresetdone_int = rxpmaresetdone_int[34:34];
  assign ch35_rxpmaresetdone_int = rxpmaresetdone_int[35:35];
  assign ch36_rxpmaresetdone_int = rxpmaresetdone_int[36:36];
  assign ch37_rxpmaresetdone_int = rxpmaresetdone_int[37:37];
  assign ch38_rxpmaresetdone_int = rxpmaresetdone_int[38:38];
  assign ch39_rxpmaresetdone_int = rxpmaresetdone_int[39:39];
  assign ch40_rxpmaresetdone_int = rxpmaresetdone_int[40:40];
  assign ch41_rxpmaresetdone_int = rxpmaresetdone_int[41:41];
  assign ch42_rxpmaresetdone_int = rxpmaresetdone_int[42:42];
  assign ch43_rxpmaresetdone_int = rxpmaresetdone_int[43:43];
  assign ch44_rxpmaresetdone_int = rxpmaresetdone_int[44:44];
  assign ch45_rxpmaresetdone_int = rxpmaresetdone_int[45:45];
  assign ch46_rxpmaresetdone_int = rxpmaresetdone_int[46:46];
  assign ch47_rxpmaresetdone_int = rxpmaresetdone_int[47:47];
  assign ch48_rxpmaresetdone_int = rxpmaresetdone_int[48:48];
  assign ch49_rxpmaresetdone_int = rxpmaresetdone_int[49:49];
  assign ch50_rxpmaresetdone_int = rxpmaresetdone_int[50:50];

  //--------------------------------------------------------------------------------------------------------------------
  wire [50:0] txpmaresetdone_int;
  wire [0:0] ch0_txpmaresetdone_int;
  wire [0:0] ch1_txpmaresetdone_int;
  wire [0:0] ch2_txpmaresetdone_int;
  wire [0:0] ch3_txpmaresetdone_int;
  wire [0:0] ch4_txpmaresetdone_int;
  wire [0:0] ch5_txpmaresetdone_int;
  wire [0:0] ch6_txpmaresetdone_int;
  wire [0:0] ch7_txpmaresetdone_int;
  wire [0:0] ch8_txpmaresetdone_int;
  wire [0:0] ch9_txpmaresetdone_int;
  wire [0:0] ch10_txpmaresetdone_int;
  wire [0:0] ch11_txpmaresetdone_int;
  wire [0:0] ch12_txpmaresetdone_int;
  wire [0:0] ch13_txpmaresetdone_int;
  wire [0:0] ch14_txpmaresetdone_int;
  wire [0:0] ch15_txpmaresetdone_int;
  wire [0:0] ch16_txpmaresetdone_int;
  wire [0:0] ch17_txpmaresetdone_int;
  wire [0:0] ch18_txpmaresetdone_int;
  wire [0:0] ch19_txpmaresetdone_int;
  wire [0:0] ch20_txpmaresetdone_int;
  wire [0:0] ch21_txpmaresetdone_int;
  wire [0:0] ch22_txpmaresetdone_int;
  wire [0:0] ch23_txpmaresetdone_int;
  wire [0:0] ch24_txpmaresetdone_int;
  wire [0:0] ch25_txpmaresetdone_int;
  wire [0:0] ch26_txpmaresetdone_int;
  wire [0:0] ch27_txpmaresetdone_int;
  wire [0:0] ch28_txpmaresetdone_int;
  wire [0:0] ch29_txpmaresetdone_int;
  wire [0:0] ch30_txpmaresetdone_int;
  wire [0:0] ch31_txpmaresetdone_int;
  wire [0:0] ch32_txpmaresetdone_int;
  wire [0:0] ch33_txpmaresetdone_int;
  wire [0:0] ch34_txpmaresetdone_int;
  wire [0:0] ch35_txpmaresetdone_int;
  wire [0:0] ch36_txpmaresetdone_int;
  wire [0:0] ch37_txpmaresetdone_int;
  wire [0:0] ch38_txpmaresetdone_int;
  wire [0:0] ch39_txpmaresetdone_int;
  wire [0:0] ch40_txpmaresetdone_int;
  wire [0:0] ch41_txpmaresetdone_int;
  wire [0:0] ch42_txpmaresetdone_int;
  wire [0:0] ch43_txpmaresetdone_int;
  wire [0:0] ch44_txpmaresetdone_int;
  wire [0:0] ch45_txpmaresetdone_int;
  wire [0:0] ch46_txpmaresetdone_int;
  wire [0:0] ch47_txpmaresetdone_int;
  wire [0:0] ch48_txpmaresetdone_int;
  wire [0:0] ch49_txpmaresetdone_int;
  wire [0:0] ch50_txpmaresetdone_int;
  assign ch0_txpmaresetdone_int = txpmaresetdone_int[0:0];
  assign ch1_txpmaresetdone_int = txpmaresetdone_int[1:1];
  assign ch2_txpmaresetdone_int = txpmaresetdone_int[2:2];
  assign ch3_txpmaresetdone_int = txpmaresetdone_int[3:3];
  assign ch4_txpmaresetdone_int = txpmaresetdone_int[4:4];
  assign ch5_txpmaresetdone_int = txpmaresetdone_int[5:5];
  assign ch6_txpmaresetdone_int = txpmaresetdone_int[6:6];
  assign ch7_txpmaresetdone_int = txpmaresetdone_int[7:7];
  assign ch8_txpmaresetdone_int = txpmaresetdone_int[8:8];
  assign ch9_txpmaresetdone_int = txpmaresetdone_int[9:9];
  assign ch10_txpmaresetdone_int = txpmaresetdone_int[10:10];
  assign ch11_txpmaresetdone_int = txpmaresetdone_int[11:11];
  assign ch12_txpmaresetdone_int = txpmaresetdone_int[12:12];
  assign ch13_txpmaresetdone_int = txpmaresetdone_int[13:13];
  assign ch14_txpmaresetdone_int = txpmaresetdone_int[14:14];
  assign ch15_txpmaresetdone_int = txpmaresetdone_int[15:15];
  assign ch16_txpmaresetdone_int = txpmaresetdone_int[16:16];
  assign ch17_txpmaresetdone_int = txpmaresetdone_int[17:17];
  assign ch18_txpmaresetdone_int = txpmaresetdone_int[18:18];
  assign ch19_txpmaresetdone_int = txpmaresetdone_int[19:19];
  assign ch20_txpmaresetdone_int = txpmaresetdone_int[20:20];
  assign ch21_txpmaresetdone_int = txpmaresetdone_int[21:21];
  assign ch22_txpmaresetdone_int = txpmaresetdone_int[22:22];
  assign ch23_txpmaresetdone_int = txpmaresetdone_int[23:23];
  assign ch24_txpmaresetdone_int = txpmaresetdone_int[24:24];
  assign ch25_txpmaresetdone_int = txpmaresetdone_int[25:25];
  assign ch26_txpmaresetdone_int = txpmaresetdone_int[26:26];
  assign ch27_txpmaresetdone_int = txpmaresetdone_int[27:27];
  assign ch28_txpmaresetdone_int = txpmaresetdone_int[28:28];
  assign ch29_txpmaresetdone_int = txpmaresetdone_int[29:29];
  assign ch30_txpmaresetdone_int = txpmaresetdone_int[30:30];
  assign ch31_txpmaresetdone_int = txpmaresetdone_int[31:31];
  assign ch32_txpmaresetdone_int = txpmaresetdone_int[32:32];
  assign ch33_txpmaresetdone_int = txpmaresetdone_int[33:33];
  assign ch34_txpmaresetdone_int = txpmaresetdone_int[34:34];
  assign ch35_txpmaresetdone_int = txpmaresetdone_int[35:35];
  assign ch36_txpmaresetdone_int = txpmaresetdone_int[36:36];
  assign ch37_txpmaresetdone_int = txpmaresetdone_int[37:37];
  assign ch38_txpmaresetdone_int = txpmaresetdone_int[38:38];
  assign ch39_txpmaresetdone_int = txpmaresetdone_int[39:39];
  assign ch40_txpmaresetdone_int = txpmaresetdone_int[40:40];
  assign ch41_txpmaresetdone_int = txpmaresetdone_int[41:41];
  assign ch42_txpmaresetdone_int = txpmaresetdone_int[42:42];
  assign ch43_txpmaresetdone_int = txpmaresetdone_int[43:43];
  assign ch44_txpmaresetdone_int = txpmaresetdone_int[44:44];
  assign ch45_txpmaresetdone_int = txpmaresetdone_int[45:45];
  assign ch46_txpmaresetdone_int = txpmaresetdone_int[46:46];
  assign ch47_txpmaresetdone_int = txpmaresetdone_int[47:47];
  assign ch48_txpmaresetdone_int = txpmaresetdone_int[48:48];
  assign ch49_txpmaresetdone_int = txpmaresetdone_int[49:49];
  assign ch50_txpmaresetdone_int = txpmaresetdone_int[50:50];


  // ===================================================================================================================
  // BUFFERS
  // ===================================================================================================================

  // Buffer the hb_gtwiz_reset_all_in input and logically combine it with the internal signal from the example
  // initialization block as well as the VIO-sourced reset
  wire hb_gtwiz_reset_all_vio_int;
  wire hb_gtwiz_reset_all_buf_int;
  wire hb_gtwiz_reset_all_init_int;
  wire hb_gtwiz_reset_all_int;

  IBUF ibuf_hb_gtwiz_reset_all_inst (
    .I (hb_gtwiz_reset_all_in),
    .O (hb_gtwiz_reset_all_buf_int)
  );

  assign hb_gtwiz_reset_all_int = hb_gtwiz_reset_all_buf_int || hb_gtwiz_reset_all_init_int || hb_gtwiz_reset_all_vio_int;

  // Globally buffer the free-running input clock
  wire hb_gtwiz_reset_clk_freerun_buf_int;

  BUFG bufg_clk_freerun_inst (
    .I (hb_gtwiz_reset_clk_freerun_in),
    .O (hb_gtwiz_reset_clk_freerun_buf_int)
  );

  // Instantiate a differential reference clock buffer for each reference clock differential pair in this configuration,
  // and assign the single-ended output of each differential reference clock buffer to the appropriate PLL input signal

  // Differential reference clock buffer for MGTREFCLK0_X0Y11
  wire mgtrefclk0_x0y11_int;

  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH  (1'b0),
    .REFCLK_HROW_CK_SEL (2'b00),
    .REFCLK_ICNTL_RX    (2'b00)
  ) IBUFDS_GTE4_MGTREFCLK0_X0Y11_INST (
    .I     (mgtrefclk0_x0y11_p),
    .IB    (mgtrefclk0_x0y11_n),
    .CEB   (1'b0),
    .O     (mgtrefclk0_x0y11_int),
    .ODIV2 ()
  );

  // Differential reference clock buffer for MGTREFCLK0_X0Y12
  wire mgtrefclk0_x0y12_int;

  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH  (1'b0),
    .REFCLK_HROW_CK_SEL (2'b00),
    .REFCLK_ICNTL_RX    (2'b00)
  ) IBUFDS_GTE4_MGTREFCLK0_X0Y12_INST (
    .I     (mgtrefclk0_x0y12_p),
    .IB    (mgtrefclk0_x0y12_n),
    .CEB   (1'b0),
    .O     (mgtrefclk0_x0y12_int),
    .ODIV2 ()
  );

  // Differential reference clock buffer for MGTREFCLK0_X0Y2
  wire mgtrefclk0_x0y2_int;

  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH  (1'b0),
    .REFCLK_HROW_CK_SEL (2'b00),
    .REFCLK_ICNTL_RX    (2'b00)
  ) IBUFDS_GTE4_MGTREFCLK0_X0Y2_INST (
    .I     (mgtrefclk0_x0y2_p),
    .IB    (mgtrefclk0_x0y2_n),
    .CEB   (1'b0),
    .O     (mgtrefclk0_x0y2_int),
    .ODIV2 ()
  );

  // Differential reference clock buffer for MGTREFCLK0_X0Y5
  wire mgtrefclk0_x0y5_int;

  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH  (1'b0),
    .REFCLK_HROW_CK_SEL (2'b00),
    .REFCLK_ICNTL_RX    (2'b00)
  ) IBUFDS_GTE4_MGTREFCLK0_X0Y5_INST (
    .I     (mgtrefclk0_x0y5_p),
    .IB    (mgtrefclk0_x0y5_n),
    .CEB   (1'b0),
    .O     (mgtrefclk0_x0y5_int),
    .ODIV2 ()
  );

  // Differential reference clock buffer for MGTREFCLK0_X0Y7
  wire mgtrefclk0_x0y7_int;

  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH  (1'b0),
    .REFCLK_HROW_CK_SEL (2'b00),
    .REFCLK_ICNTL_RX    (2'b00)
  ) IBUFDS_GTE4_MGTREFCLK0_X0Y7_INST (
    .I     (mgtrefclk0_x0y7_p),
    .IB    (mgtrefclk0_x0y7_n),
    .CEB   (1'b0),
    .O     (mgtrefclk0_x0y7_int),
    .ODIV2 ()
  );

  // Differential reference clock buffer for MGTREFCLK0_X0Y9
  wire mgtrefclk0_x0y9_int;

  IBUFDS_GTE4 #(
    .REFCLK_EN_TX_PATH  (1'b0),
    .REFCLK_HROW_CK_SEL (2'b00),
    .REFCLK_ICNTL_RX    (2'b00)
  ) IBUFDS_GTE4_MGTREFCLK0_X0Y9_INST (
    .I     (mgtrefclk0_x0y9_p),
    .IB    (mgtrefclk0_x0y9_n),
    .CEB   (1'b0),
    .O     (mgtrefclk0_x0y9_int),
    .ODIV2 ()
  );

  assign cm0_gtrefclk00_int = mgtrefclk0_x0y2_int;
  assign cm10_gtrefclk00_int = mgtrefclk0_x0y11_int;
  assign cm11_gtrefclk00_int = mgtrefclk0_x0y11_int;
  assign cm12_gtrefclk00_int = mgtrefclk0_x0y12_int;
  assign cm1_gtrefclk00_int = mgtrefclk0_x0y2_int;
  assign cm2_gtrefclk00_int = mgtrefclk0_x0y2_int;
  assign cm3_gtrefclk00_int = mgtrefclk0_x0y2_int;
  assign cm4_gtrefclk00_int = mgtrefclk0_x0y5_int;
  assign cm5_gtrefclk00_int = mgtrefclk0_x0y5_int;
  assign cm6_gtrefclk00_int = mgtrefclk0_x0y7_int;
  assign cm7_gtrefclk00_int = mgtrefclk0_x0y7_int;
  assign cm8_gtrefclk00_int = mgtrefclk0_x0y9_int;
  assign cm9_gtrefclk00_int = mgtrefclk0_x0y9_int;


  // ===================================================================================================================
  // USER CLOCKING RESETS
  // ===================================================================================================================

  // The TX user clocking helper block should be held in reset until the clock source of that block is known to be
  // stable. The following assignment is an example of how that stability can be determined, based on the selected TX
  // user clock source. Replace the assignment with the appropriate signal or logic to achieve that behavior as needed.
  assign hb0_gtwiz_userclk_tx_reset_int = ~(&txpmaresetdone_int);

  // The RX user clocking helper block should be held in reset until the clock source of that block is known to be
  // stable. The following assignment is an example of how that stability can be determined, based on the selected RX
  // user clock source. Replace the assignment with the appropriate signal or logic to achieve that behavior as needed.
  assign hb0_gtwiz_userclk_rx_reset_int = ~(&rxpmaresetdone_int);


  // ===================================================================================================================
  // PRBS STIMULUS, CHECKING, AND LINK MANAGEMENT
  // ===================================================================================================================

  // PRBS stimulus
  // -------------------------------------------------------------------------------------------------------------------

  // PRBS-based data stimulus module for transceiver channel 0
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst0 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb0_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 1
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst1 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb1_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 2
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst2 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb2_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 3
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst3 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb3_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 4
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst4 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb4_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 5
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst5 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb5_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 6
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst6 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb6_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 7
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst7 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb7_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 8
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst8 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb8_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 9
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst9 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb9_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 10
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst10 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb10_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 11
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst11 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb11_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 12
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst12 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb12_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 13
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst13 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb13_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 14
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst14 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb14_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 15
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst15 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb15_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 16
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst16 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb16_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 17
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst17 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb17_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 18
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst18 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb18_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 19
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst19 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb19_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 20
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst20 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb20_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 21
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst21 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb21_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 22
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst22 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb22_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 23
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst23 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb23_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 24
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst24 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb24_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 25
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst25 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb25_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 26
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst26 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb26_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 27
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst27 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb27_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 28
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst28 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb28_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 29
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst29 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb29_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 30
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst30 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb30_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 31
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst31 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb31_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 32
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst32 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb32_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 33
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst33 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb33_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 34
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst34 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb34_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 35
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst35 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb35_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 36
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst36 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb36_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 37
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst37 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb37_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 38
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst38 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb38_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 39
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst39 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb39_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 40
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst40 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb40_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 41
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst41 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb41_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 42
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst42 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb42_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 43
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst43 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb43_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 44
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst44 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb44_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 45
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst45 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb45_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 46
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst46 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb46_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 47
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst47 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb47_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 48
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst48 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb48_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 49
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst49 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb49_gtwiz_userdata_tx_int)
  );

  // PRBS-based data stimulus module for transceiver channel 50
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_stimulus_raw example_stimulus_inst50 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int),
    .gtwiz_userclk_tx_usrclk2_in (hb0_gtwiz_userclk_tx_usrclk2_int),
    .gtwiz_userclk_tx_active_in  (hb0_gtwiz_userclk_tx_active_int),
    .txdata_out                  (hb50_gtwiz_userdata_tx_int)
  );

  // PRBS checking
  // -------------------------------------------------------------------------------------------------------------------

  // Declare a signal vector of PRBS match indicators, with one indicator bit per transceiver channel
  wire [50:0] prbs_match_int;

  // PRBS-based data checking module for transceiver channel 0
  SL_51_GT_example_checking_raw example_checking_inst0 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb0_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[0])
  );

  // PRBS-based data checking module for transceiver channel 1
  SL_51_GT_example_checking_raw example_checking_inst1 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb1_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[1])
  );

  // PRBS-based data checking module for transceiver channel 2
  SL_51_GT_example_checking_raw example_checking_inst2 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb2_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[2])
  );

  // PRBS-based data checking module for transceiver channel 3
  SL_51_GT_example_checking_raw example_checking_inst3 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb3_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[3])
  );

  // PRBS-based data checking module for transceiver channel 4
  SL_51_GT_example_checking_raw example_checking_inst4 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb4_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[4])
  );

  // PRBS-based data checking module for transceiver channel 5
  SL_51_GT_example_checking_raw example_checking_inst5 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb5_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[5])
  );

  // PRBS-based data checking module for transceiver channel 6
  SL_51_GT_example_checking_raw example_checking_inst6 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb6_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[6])
  );

  // PRBS-based data checking module for transceiver channel 7
  SL_51_GT_example_checking_raw example_checking_inst7 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb7_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[7])
  );

  // PRBS-based data checking module for transceiver channel 8
  SL_51_GT_example_checking_raw example_checking_inst8 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb8_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[8])
  );

  // PRBS-based data checking module for transceiver channel 9
  SL_51_GT_example_checking_raw example_checking_inst9 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb9_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[9])
  );

  // PRBS-based data checking module for transceiver channel 10
  SL_51_GT_example_checking_raw example_checking_inst10 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb10_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[10])
  );

  // PRBS-based data checking module for transceiver channel 11
  SL_51_GT_example_checking_raw example_checking_inst11 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb11_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[11])
  );

  // PRBS-based data checking module for transceiver channel 12
  SL_51_GT_example_checking_raw example_checking_inst12 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb12_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[12])
  );

  // PRBS-based data checking module for transceiver channel 13
  SL_51_GT_example_checking_raw example_checking_inst13 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb13_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[13])
  );

  // PRBS-based data checking module for transceiver channel 14
  SL_51_GT_example_checking_raw example_checking_inst14 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb14_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[14])
  );

  // PRBS-based data checking module for transceiver channel 15
  SL_51_GT_example_checking_raw example_checking_inst15 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb15_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[15])
  );

  // PRBS-based data checking module for transceiver channel 16
  SL_51_GT_example_checking_raw example_checking_inst16 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb16_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[16])
  );

  // PRBS-based data checking module for transceiver channel 17
  SL_51_GT_example_checking_raw example_checking_inst17 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb17_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[17])
  );

  // PRBS-based data checking module for transceiver channel 18
  SL_51_GT_example_checking_raw example_checking_inst18 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb18_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[18])
  );

  // PRBS-based data checking module for transceiver channel 19
  SL_51_GT_example_checking_raw example_checking_inst19 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb19_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[19])
  );

  // PRBS-based data checking module for transceiver channel 20
  SL_51_GT_example_checking_raw example_checking_inst20 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb20_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[20])
  );

  // PRBS-based data checking module for transceiver channel 21
  SL_51_GT_example_checking_raw example_checking_inst21 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb21_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[21])
  );

  // PRBS-based data checking module for transceiver channel 22
  SL_51_GT_example_checking_raw example_checking_inst22 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb22_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[22])
  );

  // PRBS-based data checking module for transceiver channel 23
  SL_51_GT_example_checking_raw example_checking_inst23 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb23_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[23])
  );

  // PRBS-based data checking module for transceiver channel 24
  SL_51_GT_example_checking_raw example_checking_inst24 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb24_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[24])
  );

  // PRBS-based data checking module for transceiver channel 25
  SL_51_GT_example_checking_raw example_checking_inst25 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb25_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[25])
  );

  // PRBS-based data checking module for transceiver channel 26
  SL_51_GT_example_checking_raw example_checking_inst26 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb26_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[26])
  );

  // PRBS-based data checking module for transceiver channel 27
  SL_51_GT_example_checking_raw example_checking_inst27 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb27_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[27])
  );

  // PRBS-based data checking module for transceiver channel 28
  SL_51_GT_example_checking_raw example_checking_inst28 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb28_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[28])
  );

  // PRBS-based data checking module for transceiver channel 29
  SL_51_GT_example_checking_raw example_checking_inst29 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb29_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[29])
  );

  // PRBS-based data checking module for transceiver channel 30
  SL_51_GT_example_checking_raw example_checking_inst30 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb30_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[30])
  );

  // PRBS-based data checking module for transceiver channel 31
  SL_51_GT_example_checking_raw example_checking_inst31 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb31_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[31])
  );

  // PRBS-based data checking module for transceiver channel 32
  SL_51_GT_example_checking_raw example_checking_inst32 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb32_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[32])
  );

  // PRBS-based data checking module for transceiver channel 33
  SL_51_GT_example_checking_raw example_checking_inst33 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb33_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[33])
  );

  // PRBS-based data checking module for transceiver channel 34
  SL_51_GT_example_checking_raw example_checking_inst34 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb34_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[34])
  );

  // PRBS-based data checking module for transceiver channel 35
  SL_51_GT_example_checking_raw example_checking_inst35 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb35_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[35])
  );

  // PRBS-based data checking module for transceiver channel 36
  SL_51_GT_example_checking_raw example_checking_inst36 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb36_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[36])
  );

  // PRBS-based data checking module for transceiver channel 37
  SL_51_GT_example_checking_raw example_checking_inst37 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb37_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[37])
  );

  // PRBS-based data checking module for transceiver channel 38
  SL_51_GT_example_checking_raw example_checking_inst38 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb38_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[38])
  );

  // PRBS-based data checking module for transceiver channel 39
  SL_51_GT_example_checking_raw example_checking_inst39 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb39_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[39])
  );

  // PRBS-based data checking module for transceiver channel 40
  SL_51_GT_example_checking_raw example_checking_inst40 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb40_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[40])
  );

  // PRBS-based data checking module for transceiver channel 41
  SL_51_GT_example_checking_raw example_checking_inst41 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb41_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[41])
  );

  // PRBS-based data checking module for transceiver channel 42
  SL_51_GT_example_checking_raw example_checking_inst42 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb42_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[42])
  );

  // PRBS-based data checking module for transceiver channel 43
  SL_51_GT_example_checking_raw example_checking_inst43 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb43_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[43])
  );

  // PRBS-based data checking module for transceiver channel 44
  SL_51_GT_example_checking_raw example_checking_inst44 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb44_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[44])
  );

  // PRBS-based data checking module for transceiver channel 45
  SL_51_GT_example_checking_raw example_checking_inst45 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb45_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[45])
  );

  // PRBS-based data checking module for transceiver channel 46
  SL_51_GT_example_checking_raw example_checking_inst46 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb46_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[46])
  );

  // PRBS-based data checking module for transceiver channel 47
  SL_51_GT_example_checking_raw example_checking_inst47 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb47_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[47])
  );

  // PRBS-based data checking module for transceiver channel 48
  SL_51_GT_example_checking_raw example_checking_inst48 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb48_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[48])
  );

  // PRBS-based data checking module for transceiver channel 49
  SL_51_GT_example_checking_raw example_checking_inst49 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb49_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[49])
  );

  // PRBS-based data checking module for transceiver channel 50
  SL_51_GT_example_checking_raw example_checking_inst50 (
    .gtwiz_reset_all_in          (hb_gtwiz_reset_all_int || ~hb0_gtwiz_reset_rx_done_int ),
    .gtwiz_userclk_rx_usrclk2_in (hb0_gtwiz_userclk_rx_usrclk2_int),
    .gtwiz_userclk_rx_active_in  (hb0_gtwiz_userclk_rx_active_int),
    .rxdata_in                   (hb50_gtwiz_userdata_rx_int),
    .prbs_match_out              (prbs_match_int[50])
  );

  // PRBS match and related link management
  // -------------------------------------------------------------------------------------------------------------------

  // Perform a bitwise NAND of all PRBS match indicators, creating a combinatorial indication of any PRBS mismatch
  // across all transceiver channels
  wire prbs_error_any_async = ~(&prbs_match_int);
  wire prbs_error_any_sync;

  // Synchronize the PRBS mismatch indicator the free-running clock domain, using a reset synchronizer with asynchronous
  // reset and synchronous removal
  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_reset_synchronizer reset_synchronizer_prbs_match_all_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .rst_in (prbs_error_any_async),
    .rst_out(prbs_error_any_sync)
  );

  // Implement an example link status state machine using a simple leaky bucket mechanism. The link status indicates
  // the continual PRBS match status to both the top-level observer and the initialization state machine, while being
  // tolerant of occasional bit errors. This is an example and can be modified as necessary.
  localparam ST_LINK_DOWN = 1'b0;
  localparam ST_LINK_UP   = 1'b1;
  reg        sm_link      = ST_LINK_DOWN;
  reg [6:0]  link_ctr     = 7'd0;

  always @(posedge hb_gtwiz_reset_clk_freerun_buf_int) begin
    case (sm_link)
      // The link is considered to be down when the link counter initially has a value less than 67. When the link is
      // down, the counter is incremented on each cycle where all PRBS bits match, but reset whenever any PRBS mismatch
      // occurs. When the link counter reaches 67, transition to the link up state.
      ST_LINK_DOWN: begin
        if (prbs_error_any_sync !== 1'b0) begin
          link_ctr <= 7'd0;
        end
        else begin
          if (link_ctr < 7'd67)
            link_ctr <= link_ctr + 7'd1;
          else
            sm_link <= ST_LINK_UP;
        end
      end

      // When the link is up, the link counter is decreased by 34 whenever any PRBS mismatch occurs, but is increased by
      // only 1 on each cycle where all PRBS bits match, up to its saturation point of 67. If the link counter reaches
      // 0 (including rollover protection), transition to the link down state.
      ST_LINK_UP: begin
        if (prbs_error_any_sync !== 1'b0) begin
          if (link_ctr > 7'd33) begin
            link_ctr <= link_ctr - 7'd34;
            if (link_ctr == 7'd34)
              sm_link  <= ST_LINK_DOWN;
          end
          else begin
            link_ctr <= 7'd0;
            sm_link  <= ST_LINK_DOWN;
          end
        end
        else begin
          if (link_ctr < 7'd67)
            link_ctr <= link_ctr + 7'd1;
        end
      end
    endcase
  end

  // Synchronize the latched link down reset input and the VIO-driven signal into the free-running clock domain
  wire link_down_latched_reset_vio_int;
  wire link_down_latched_reset_sync;

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_link_down_latched_reset_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (link_down_latched_reset_in || link_down_latched_reset_vio_int),
    .o_out  (link_down_latched_reset_sync)
  );

  // Reset the latched link down indicator when the synchronized latched link down reset signal is high. Otherwise, set
  // the latched link down indicator upon losing link. This indicator is available for user reference.
  always @(posedge hb_gtwiz_reset_clk_freerun_buf_int) begin
    if (link_down_latched_reset_sync)
      link_down_latched_out <= 1'b0;
    else if (!sm_link)
      link_down_latched_out <= 1'b1;
  end

  // Assign the link status indicator to the top-level two-state output for user reference
  assign link_status_out = sm_link;


  // ===================================================================================================================
  // INITIALIZATION
  // ===================================================================================================================

  // Declare the receiver reset signals that interface to the reset controller helper block. For this configuration,
  // which uses the same PLL type for transmitter and receiver, the "reset RX PLL and datapath" feature is not used.
  wire hb_gtwiz_reset_rx_pll_and_datapath_int = 1'b0;
  wire hb_gtwiz_reset_rx_datapath_int;

  // Declare signals which connect the VIO instance to the initialization module for debug purposes
  wire       init_done_int;
  wire [3:0] init_retry_ctr_int;

  // Combine the receiver reset signals form the initialization module and the VIO to drive the appropriate reset
  // controller helper block reset input
  wire hb_gtwiz_reset_rx_pll_and_datapath_vio_int;
  wire hb_gtwiz_reset_rx_datapath_vio_int;
  wire hb_gtwiz_reset_rx_datapath_init_int;

  assign hb_gtwiz_reset_rx_datapath_int = hb_gtwiz_reset_rx_datapath_init_int || hb_gtwiz_reset_rx_datapath_vio_int;

  // The example initialization module interacts with the reset controller helper block and other example design logic
  // to retry failed reset attempts in order to mitigate bring-up issues such as initially-unavilable reference clocks
  // or data connections. It also resets the receiver in the event of link loss in an attempt to regain link, so please
  // note the possibility that this behavior can have the effect of overriding or disturbing user-provided inputs that
  // destabilize the data stream. It is a demonstration only and can be modified to suit your system needs.
  SL_51_GT_example_init example_init_inst (
    .clk_freerun_in  (hb_gtwiz_reset_clk_freerun_buf_int),
    .reset_all_in    (hb_gtwiz_reset_all_int),
    .tx_init_done_in (gtwiz_reset_tx_done_int),
    .rx_init_done_in (gtwiz_reset_rx_done_int),
    .rx_data_good_in (sm_link),
    .reset_all_out   (hb_gtwiz_reset_all_init_int),
    .reset_rx_out    (hb_gtwiz_reset_rx_datapath_init_int),
    .init_done_out   (init_done_int),
    .retry_ctr_out   (init_retry_ctr_int)
  );


  // ===================================================================================================================
  // VIO FOR HARDWARE BRING-UP AND DEBUG
  // ===================================================================================================================

  // Synchronize gtpowergood into the free-running clock domain for VIO usage
  wire [50:0] gtpowergood_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[0]),
    .o_out  (gtpowergood_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[1]),
    .o_out  (gtpowergood_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[2]),
    .o_out  (gtpowergood_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[3]),
    .o_out  (gtpowergood_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[4]),
    .o_out  (gtpowergood_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[5]),
    .o_out  (gtpowergood_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[6]),
    .o_out  (gtpowergood_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[7]),
    .o_out  (gtpowergood_vio_sync[7])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_8_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[8]),
    .o_out  (gtpowergood_vio_sync[8])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_9_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[9]),
    .o_out  (gtpowergood_vio_sync[9])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_10_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[10]),
    .o_out  (gtpowergood_vio_sync[10])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_11_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[11]),
    .o_out  (gtpowergood_vio_sync[11])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_12_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[12]),
    .o_out  (gtpowergood_vio_sync[12])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_13_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[13]),
    .o_out  (gtpowergood_vio_sync[13])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_14_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[14]),
    .o_out  (gtpowergood_vio_sync[14])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_15_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[15]),
    .o_out  (gtpowergood_vio_sync[15])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_16_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[16]),
    .o_out  (gtpowergood_vio_sync[16])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_17_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[17]),
    .o_out  (gtpowergood_vio_sync[17])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_18_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[18]),
    .o_out  (gtpowergood_vio_sync[18])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_19_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[19]),
    .o_out  (gtpowergood_vio_sync[19])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_20_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[20]),
    .o_out  (gtpowergood_vio_sync[20])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_21_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[21]),
    .o_out  (gtpowergood_vio_sync[21])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_22_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[22]),
    .o_out  (gtpowergood_vio_sync[22])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_23_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[23]),
    .o_out  (gtpowergood_vio_sync[23])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_24_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[24]),
    .o_out  (gtpowergood_vio_sync[24])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_25_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[25]),
    .o_out  (gtpowergood_vio_sync[25])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_26_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[26]),
    .o_out  (gtpowergood_vio_sync[26])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_27_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[27]),
    .o_out  (gtpowergood_vio_sync[27])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_28_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[28]),
    .o_out  (gtpowergood_vio_sync[28])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_29_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[29]),
    .o_out  (gtpowergood_vio_sync[29])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_30_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[30]),
    .o_out  (gtpowergood_vio_sync[30])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_31_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[31]),
    .o_out  (gtpowergood_vio_sync[31])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_32_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[32]),
    .o_out  (gtpowergood_vio_sync[32])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_33_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[33]),
    .o_out  (gtpowergood_vio_sync[33])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_34_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[34]),
    .o_out  (gtpowergood_vio_sync[34])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_35_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[35]),
    .o_out  (gtpowergood_vio_sync[35])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_36_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[36]),
    .o_out  (gtpowergood_vio_sync[36])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_37_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[37]),
    .o_out  (gtpowergood_vio_sync[37])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_38_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[38]),
    .o_out  (gtpowergood_vio_sync[38])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_39_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[39]),
    .o_out  (gtpowergood_vio_sync[39])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_40_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[40]),
    .o_out  (gtpowergood_vio_sync[40])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_41_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[41]),
    .o_out  (gtpowergood_vio_sync[41])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_42_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[42]),
    .o_out  (gtpowergood_vio_sync[42])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_43_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[43]),
    .o_out  (gtpowergood_vio_sync[43])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_44_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[44]),
    .o_out  (gtpowergood_vio_sync[44])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_45_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[45]),
    .o_out  (gtpowergood_vio_sync[45])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_46_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[46]),
    .o_out  (gtpowergood_vio_sync[46])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_47_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[47]),
    .o_out  (gtpowergood_vio_sync[47])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_48_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[48]),
    .o_out  (gtpowergood_vio_sync[48])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_49_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[49]),
    .o_out  (gtpowergood_vio_sync[49])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtpowergood_50_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtpowergood_int[50]),
    .o_out  (gtpowergood_vio_sync[50])
  );

  // Synchronize txpmaresetdone into the free-running clock domain for VIO usage
  wire [50:0] txpmaresetdone_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[0]),
    .o_out  (txpmaresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[1]),
    .o_out  (txpmaresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[2]),
    .o_out  (txpmaresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[3]),
    .o_out  (txpmaresetdone_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[4]),
    .o_out  (txpmaresetdone_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[5]),
    .o_out  (txpmaresetdone_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[6]),
    .o_out  (txpmaresetdone_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[7]),
    .o_out  (txpmaresetdone_vio_sync[7])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_8_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[8]),
    .o_out  (txpmaresetdone_vio_sync[8])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_9_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[9]),
    .o_out  (txpmaresetdone_vio_sync[9])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_10_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[10]),
    .o_out  (txpmaresetdone_vio_sync[10])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_11_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[11]),
    .o_out  (txpmaresetdone_vio_sync[11])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_12_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[12]),
    .o_out  (txpmaresetdone_vio_sync[12])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_13_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[13]),
    .o_out  (txpmaresetdone_vio_sync[13])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_14_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[14]),
    .o_out  (txpmaresetdone_vio_sync[14])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_15_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[15]),
    .o_out  (txpmaresetdone_vio_sync[15])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_16_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[16]),
    .o_out  (txpmaresetdone_vio_sync[16])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_17_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[17]),
    .o_out  (txpmaresetdone_vio_sync[17])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_18_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[18]),
    .o_out  (txpmaresetdone_vio_sync[18])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_19_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[19]),
    .o_out  (txpmaresetdone_vio_sync[19])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_20_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[20]),
    .o_out  (txpmaresetdone_vio_sync[20])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_21_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[21]),
    .o_out  (txpmaresetdone_vio_sync[21])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_22_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[22]),
    .o_out  (txpmaresetdone_vio_sync[22])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_23_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[23]),
    .o_out  (txpmaresetdone_vio_sync[23])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_24_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[24]),
    .o_out  (txpmaresetdone_vio_sync[24])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_25_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[25]),
    .o_out  (txpmaresetdone_vio_sync[25])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_26_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[26]),
    .o_out  (txpmaresetdone_vio_sync[26])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_27_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[27]),
    .o_out  (txpmaresetdone_vio_sync[27])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_28_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[28]),
    .o_out  (txpmaresetdone_vio_sync[28])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_29_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[29]),
    .o_out  (txpmaresetdone_vio_sync[29])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_30_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[30]),
    .o_out  (txpmaresetdone_vio_sync[30])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_31_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[31]),
    .o_out  (txpmaresetdone_vio_sync[31])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_32_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[32]),
    .o_out  (txpmaresetdone_vio_sync[32])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_33_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[33]),
    .o_out  (txpmaresetdone_vio_sync[33])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_34_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[34]),
    .o_out  (txpmaresetdone_vio_sync[34])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_35_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[35]),
    .o_out  (txpmaresetdone_vio_sync[35])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_36_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[36]),
    .o_out  (txpmaresetdone_vio_sync[36])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_37_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[37]),
    .o_out  (txpmaresetdone_vio_sync[37])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_38_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[38]),
    .o_out  (txpmaresetdone_vio_sync[38])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_39_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[39]),
    .o_out  (txpmaresetdone_vio_sync[39])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_40_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[40]),
    .o_out  (txpmaresetdone_vio_sync[40])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_41_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[41]),
    .o_out  (txpmaresetdone_vio_sync[41])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_42_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[42]),
    .o_out  (txpmaresetdone_vio_sync[42])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_43_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[43]),
    .o_out  (txpmaresetdone_vio_sync[43])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_44_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[44]),
    .o_out  (txpmaresetdone_vio_sync[44])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_45_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[45]),
    .o_out  (txpmaresetdone_vio_sync[45])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_46_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[46]),
    .o_out  (txpmaresetdone_vio_sync[46])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_47_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[47]),
    .o_out  (txpmaresetdone_vio_sync[47])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_48_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[48]),
    .o_out  (txpmaresetdone_vio_sync[48])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_49_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[49]),
    .o_out  (txpmaresetdone_vio_sync[49])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_txpmaresetdone_50_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (txpmaresetdone_int[50]),
    .o_out  (txpmaresetdone_vio_sync[50])
  );

  // Synchronize rxpmaresetdone into the free-running clock domain for VIO usage
  wire [50:0] rxpmaresetdone_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[0]),
    .o_out  (rxpmaresetdone_vio_sync[0])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_1_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[1]),
    .o_out  (rxpmaresetdone_vio_sync[1])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_2_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[2]),
    .o_out  (rxpmaresetdone_vio_sync[2])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_3_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[3]),
    .o_out  (rxpmaresetdone_vio_sync[3])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_4_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[4]),
    .o_out  (rxpmaresetdone_vio_sync[4])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_5_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[5]),
    .o_out  (rxpmaresetdone_vio_sync[5])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_6_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[6]),
    .o_out  (rxpmaresetdone_vio_sync[6])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_7_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[7]),
    .o_out  (rxpmaresetdone_vio_sync[7])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_8_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[8]),
    .o_out  (rxpmaresetdone_vio_sync[8])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_9_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[9]),
    .o_out  (rxpmaresetdone_vio_sync[9])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_10_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[10]),
    .o_out  (rxpmaresetdone_vio_sync[10])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_11_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[11]),
    .o_out  (rxpmaresetdone_vio_sync[11])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_12_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[12]),
    .o_out  (rxpmaresetdone_vio_sync[12])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_13_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[13]),
    .o_out  (rxpmaresetdone_vio_sync[13])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_14_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[14]),
    .o_out  (rxpmaresetdone_vio_sync[14])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_15_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[15]),
    .o_out  (rxpmaresetdone_vio_sync[15])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_16_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[16]),
    .o_out  (rxpmaresetdone_vio_sync[16])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_17_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[17]),
    .o_out  (rxpmaresetdone_vio_sync[17])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_18_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[18]),
    .o_out  (rxpmaresetdone_vio_sync[18])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_19_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[19]),
    .o_out  (rxpmaresetdone_vio_sync[19])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_20_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[20]),
    .o_out  (rxpmaresetdone_vio_sync[20])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_21_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[21]),
    .o_out  (rxpmaresetdone_vio_sync[21])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_22_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[22]),
    .o_out  (rxpmaresetdone_vio_sync[22])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_23_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[23]),
    .o_out  (rxpmaresetdone_vio_sync[23])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_24_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[24]),
    .o_out  (rxpmaresetdone_vio_sync[24])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_25_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[25]),
    .o_out  (rxpmaresetdone_vio_sync[25])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_26_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[26]),
    .o_out  (rxpmaresetdone_vio_sync[26])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_27_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[27]),
    .o_out  (rxpmaresetdone_vio_sync[27])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_28_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[28]),
    .o_out  (rxpmaresetdone_vio_sync[28])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_29_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[29]),
    .o_out  (rxpmaresetdone_vio_sync[29])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_30_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[30]),
    .o_out  (rxpmaresetdone_vio_sync[30])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_31_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[31]),
    .o_out  (rxpmaresetdone_vio_sync[31])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_32_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[32]),
    .o_out  (rxpmaresetdone_vio_sync[32])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_33_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[33]),
    .o_out  (rxpmaresetdone_vio_sync[33])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_34_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[34]),
    .o_out  (rxpmaresetdone_vio_sync[34])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_35_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[35]),
    .o_out  (rxpmaresetdone_vio_sync[35])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_36_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[36]),
    .o_out  (rxpmaresetdone_vio_sync[36])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_37_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[37]),
    .o_out  (rxpmaresetdone_vio_sync[37])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_38_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[38]),
    .o_out  (rxpmaresetdone_vio_sync[38])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_39_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[39]),
    .o_out  (rxpmaresetdone_vio_sync[39])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_40_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[40]),
    .o_out  (rxpmaresetdone_vio_sync[40])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_41_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[41]),
    .o_out  (rxpmaresetdone_vio_sync[41])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_42_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[42]),
    .o_out  (rxpmaresetdone_vio_sync[42])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_43_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[43]),
    .o_out  (rxpmaresetdone_vio_sync[43])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_44_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[44]),
    .o_out  (rxpmaresetdone_vio_sync[44])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_45_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[45]),
    .o_out  (rxpmaresetdone_vio_sync[45])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_46_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[46]),
    .o_out  (rxpmaresetdone_vio_sync[46])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_47_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[47]),
    .o_out  (rxpmaresetdone_vio_sync[47])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_48_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[48]),
    .o_out  (rxpmaresetdone_vio_sync[48])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_49_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[49]),
    .o_out  (rxpmaresetdone_vio_sync[49])
  );

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_rxpmaresetdone_50_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (rxpmaresetdone_int[50]),
    .o_out  (rxpmaresetdone_vio_sync[50])
  );

  // Synchronize gtwiz_reset_tx_done into the free-running clock domain for VIO usage
  wire [0:0] gtwiz_reset_tx_done_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtwiz_reset_tx_done_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtwiz_reset_tx_done_int[0]),
    .o_out  (gtwiz_reset_tx_done_vio_sync[0])
  );

  // Synchronize gtwiz_reset_rx_done into the free-running clock domain for VIO usage
  wire [0:0] gtwiz_reset_rx_done_vio_sync;

  (* DONT_TOUCH = "TRUE" *)
  SL_51_GT_example_bit_synchronizer bit_synchronizer_vio_gtwiz_reset_rx_done_0_inst (
    .clk_in (hb_gtwiz_reset_clk_freerun_buf_int),
    .i_in   (gtwiz_reset_rx_done_int[0]),
    .o_out  (gtwiz_reset_rx_done_vio_sync[0])
  );


  // Instantiate the VIO IP core for hardware bring-up and debug purposes, connecting relevant debug and analysis
  // signals which have been enabled during Wizard IP customization. This initial set of connected signals is
  // provided as a convenience and example, but more or fewer ports can be used as needed; simply re-customize and
  // re-generate the VIO instance, then connect any exposed signals that are needed. Signals which are synchronous to
  // clocks other than the free-running clock will require synchronization. For usage, refer to Vivado Design Suite
  // User Guide: Programming and Debugging (UG908)
  SL_51_GT_vio_0 SL_51_GT_vio_0_inst (
    .clk (hb_gtwiz_reset_clk_freerun_buf_int)
    ,.probe_in0 (link_status_out)
    ,.probe_in1 (link_down_latched_out)
    ,.probe_in2 (init_done_int)
    ,.probe_in3 (init_retry_ctr_int)
    ,.probe_in4 (gtpowergood_vio_sync)
    ,.probe_in5 (txpmaresetdone_vio_sync)
    ,.probe_in6 (rxpmaresetdone_vio_sync)
    ,.probe_in7 (gtwiz_reset_tx_done_vio_sync)
    ,.probe_in8 (gtwiz_reset_rx_done_vio_sync)
    ,.probe_out0 (hb_gtwiz_reset_all_vio_int)
    ,.probe_out1 (hb0_gtwiz_reset_tx_pll_and_datapath_int)
    ,.probe_out2 (hb0_gtwiz_reset_tx_datapath_int)
    ,.probe_out3 (hb_gtwiz_reset_rx_pll_and_datapath_vio_int)
    ,.probe_out4 (hb_gtwiz_reset_rx_datapath_vio_int)
    ,.probe_out5 (link_down_latched_reset_vio_int)
  );


  // ===================================================================================================================
  // EXAMPLE WRAPPER INSTANCE
  // ===================================================================================================================

  // Instantiate the example design wrapper, mapping its enabled ports to per-channel internal signals and example
  // resources as appropriate
  SL_51_GT_example_wrapper example_wrapper_inst (
    .gtyrxn_in                               (gtyrxn_int)
   ,.gtyrxp_in                               (gtyrxp_int)
   ,.gtytxn_out                              (gtytxn_int)
   ,.gtytxp_out                              (gtytxp_int)
   ,.gtwiz_userclk_tx_reset_in               (gtwiz_userclk_tx_reset_int)
   ,.gtwiz_userclk_tx_srcclk_out             (gtwiz_userclk_tx_srcclk_int)
   ,.gtwiz_userclk_tx_usrclk_out             (gtwiz_userclk_tx_usrclk_int)
   ,.gtwiz_userclk_tx_usrclk2_out            (gtwiz_userclk_tx_usrclk2_int)
   ,.gtwiz_userclk_tx_active_out             (gtwiz_userclk_tx_active_int)
   ,.gtwiz_userclk_rx_reset_in               (gtwiz_userclk_rx_reset_int)
   ,.gtwiz_userclk_rx_srcclk_out             (gtwiz_userclk_rx_srcclk_int)
   ,.gtwiz_userclk_rx_usrclk_out             (gtwiz_userclk_rx_usrclk_int)
   ,.gtwiz_userclk_rx_usrclk2_out            (gtwiz_userclk_rx_usrclk2_int)
   ,.gtwiz_userclk_rx_active_out             (gtwiz_userclk_rx_active_int)
   ,.gtwiz_reset_clk_freerun_in              ({1{hb_gtwiz_reset_clk_freerun_buf_int}})
   ,.gtwiz_reset_all_in                      ({1{hb_gtwiz_reset_all_int}})
   ,.gtwiz_reset_tx_pll_and_datapath_in      (gtwiz_reset_tx_pll_and_datapath_int)
   ,.gtwiz_reset_tx_datapath_in              (gtwiz_reset_tx_datapath_int)
   ,.gtwiz_reset_rx_pll_and_datapath_in      ({1{hb_gtwiz_reset_rx_pll_and_datapath_int}})
   ,.gtwiz_reset_rx_datapath_in              ({1{hb_gtwiz_reset_rx_datapath_int}})
   ,.gtwiz_reset_rx_cdr_stable_out           (gtwiz_reset_rx_cdr_stable_int)
   ,.gtwiz_reset_tx_done_out                 (gtwiz_reset_tx_done_int)
   ,.gtwiz_reset_rx_done_out                 (gtwiz_reset_rx_done_int)
   ,.gtwiz_userdata_tx_in                    (gtwiz_userdata_tx_int)
   ,.gtwiz_userdata_rx_out                   (gtwiz_userdata_rx_int)
   ,.gtrefclk00_in                           (gtrefclk00_int)
   ,.qpll0outclk_out                         (qpll0outclk_int)
   ,.qpll0outrefclk_out                      (qpll0outrefclk_int)
   ,.rxslide_in                              (rxslide_int)
   ,.gtpowergood_out                         (gtpowergood_int)
   ,.rxpmaresetdone_out                      (rxpmaresetdone_int)
   ,.txpmaresetdone_out                      (txpmaresetdone_int)
);


endmodule
