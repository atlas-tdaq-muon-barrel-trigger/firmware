import ROOT
import numpy as np
import math
import windows_DB as DB


#return the order for RPC candidates
def ordering(eta_coin):
  trig_topo = 0
  #RPC0-RPC1-RPC2-RPC3
  if((eta_coin[0] == 1 or eta_coin[1] == 1 or eta_coin[3] == 1 )  and (eta_coin[3] == 1 or eta_coin[4] == 1 ) and (eta_coin[5] == 1 or eta_coin[6] == 1 ) and (eta_coin[7] == 1 or eta_coin[8] == 1 )): trig_topo = 5   
  #RPC1-RPC2-RPC3
  elif((eta_coin[0] == 0 and eta_coin[1] == 0 and eta_coin[3] == 0 )  and (eta_coin[3] == 1 or eta_coin[4] == 1 ) and (eta_coin[5] == 1 or eta_coin[6] == 1 ) and (eta_coin[7] == 1 or eta_coin[8] == 1 )): trig_topo = 4   
  #RPC0-RPC2-RPC3
  elif((eta_coin[0] == 1 or eta_coin[1] == 1 or eta_coin[3] == 1 )  and (eta_coin[3] == 0 and eta_coin[4] == 0 ) and (eta_coin[5] == 1 or eta_coin[6] == 1 ) and (eta_coin[7] == 1 or eta_coin[8] == 1 )): trig_topo = 3   
  #RPC0-RPC1-RPC3
  elif((eta_coin[0] == 1 or eta_coin[1] == 1 or eta_coin[3] == 1 )  and (eta_coin[3] == 1 or eta_coin[4] == 1  ) and (eta_coin[5] == 0 and eta_coin[6] == 0 ) and (eta_coin[7] == 1 or eta_coin[8] == 1 )): trig_topo = 2   
  #RPC0-RPC1-RPC2
  elif((eta_coin[0] == 1 or eta_coin[1] == 1 or eta_coin[3] == 1 )  and (eta_coin[3] == 1 or eta_coin[4] == 1  ) and (eta_coin[5] == 1 or eta_coin[6] == 1 ) and (eta_coin[7] == 0 and eta_coin[8] == 0 )): trig_topo = 1   
  #RPC0-RPC3
  elif((eta_coin[0] == 1 or eta_coin[1] == 1 or eta_coin[3] == 1 )  and (eta_coin[3] == 0 and eta_coin[4] == 0  ) and (eta_coin[5] == 0 and eta_coin[6] == 0 ) and (eta_coin[7] == 1 or eta_coin[8] == 1 )): trig_topo = 0   
  return trig_topo



def trigger_cand(current_pivot_eta,max_prio_eta):

 final_cand_eta = []
 memory_pivot_eta = []
 for cand in current_pivot_eta:
  #remove double counting from the 2D grid
  if(cand[0] in memory_pivot_eta):continue
  memory_pivot_eta.append(cand[0])
  eta_coin = cand[1]
  #order based on the priority
  trig_topo = ordering(eta_coin)
  if(trig_topo<max_prio_eta):continue  
#  print(cand[0],trig_topo,eta_coin,(cand[0])*bin_x+x_min)
  final_cand_eta.append(cand[0])
 return final_cand_eta
 



#Settori Large, aggiungere repo
#0,   1    2   3   4    5    6      7
#IM1s,IM2s,IOs,MMs,M10s,M20s, ggBM,ggBI
#TODO aggiungere finestre tra gasgap
#Windows = [
#[0.014,0.0165,0.036,0.0085,0.026,0.0205        ,0.0089,0.00425],
#[0.011,0.015,0.033,0.0075,0.024,0.019          ,0.007,0.0038],
#[0.0105,0.014,0.0305,0.0075,0.0225,0.018       ,0.007,0.0036],
#[0.0095,0.013,0.027,0.007,0.0195,0.0155        ,0.0069,0.0041],
#[0.009,0.012,0.0255,0.0065,0.0185,0.0155       ,0.0061,0.0037],
#[0.0085,0.0115,0.0235,0.0065,0.017,0.0135      ,0.00605,0.00345],
#[0.007,0.0105,0.0215,0.006,0.0155,0.012        ,0.00645,0.00305],
#[0.007,0.01,0.0205,0.0055,0.0145,0.012         ,0.00665,0.00325],
#[0.0065, 0.0095, 0.019, 0.005, 0.0135, 0.011   ,0.00635,0.00375],
#[0.0075, 0.0095, 0.019, 0.0055, 0.0135, 0.011  ,0.00495,0.0042],
#[0.0075, 0.0095, 0.019, 0.0055, 0.0135, 0.011  ,0.00495,0.0042],
#[0.0075, 0.0095, 0.019, 0.005, 0.013, 0.011    ,0.0065,0.0042],
#[0.007, 0.0095, 0.019, 0.005, 0.014, 0.011     ,0.00475,0.00375],
#[0.007, 0.0105, 0.021, 0.0055, 0.0145, 0.012   ,0.0053,0.00325],
#[0.007, 0.0105, 0.021, 0.006, 0.0155, 0.012    ,0.006,0.003],
#[0.0085, 0.0115, 0.024, 0.0065, 0.0175, 0.0135 ,0.006,0.00345],
#[0.009, 0.012, 0.0255, 0.0065, 0.019, 0.0155   ,0.006,0.0037],
#[0.0095, 0.013, 0.027, 0.007, 0.019, 0.0155    ,0.0069,0.00405],
#[0.0105, 0.014, 0.03, 0.0075, 0.022, 0.0175    ,0.00655,0.0035],
#[0.011,0.0155, 0.033, 0.008, 0.024, 0.0185     ,0.007,0.0038],
#[0.014, 0.0165, 0.036, 0.0085, 0.026, 0.021    ,0.00835,0.00425]
#]
#
#Windows_small = [
#[0.019,0.092,0.045,0.0485,0.029,0.036              ,0.0089  ,0.00425],
#[0.0185,0.0225,0.041,0.008,0.025,0.0195              ,0.007  ,0.0038],
#[0.0175,0.021,0.0365,0.0075,0.0215,0.0175            ,0.007  ,0.0036],
#[0.017,0.0205,0.034,0.0075,0.0195,0.016            ,0.0069  ,0.0041],
#[0.0155,0.019,0.0315,0.0065,0.0185,0.0145           ,0.0061  ,0.0037],
#[0.015,0.018,0.029,0.006,0.016,0.013               ,0.00605  ,0.00345],
#[0.014,0.017,0.0275,0.0055,0.0145,0.0115             ,0.00645  ,0.00305],
#[0.0135,0.0165,0.0265,0.0055,0.0145,0.011           ,0.00665  ,0.00325],
#[0.013,0.016 , 0.019, 0.0255,0.0055, 0.011          ,0.00635  ,0.00375],
#[0.013, 0.0155, 0.025, 0.0055, 0.0145,0.0105,         0.00495  ,0.0042],
#[0.013, 0.0155, 0.025, 0.0055, 0.0145, 0.0105        ,0.00495  ,0.0042],
#[0.013, 0.0155, 0.0245, 0.0055, 0.0145, 0.0105       ,0.0065  ,0.0042],
#[0.013, 0.0155, 0.025, 0.0055, 0.0145, 0.011        ,0.00475  ,0.00375],
#[0.0135, 0.015, 0.027, 0.0055, 0.0145, 0.011       ,0.0053  ,0.00325],
#[0.0135, 0.0155,0.027, 0.006, 0.0155, 0.012         ,0.006  ,0.003],
#[0.015, 0.0165, 0.029, 0.0065, 0.0145, 0.0125       ,0.006  ,0.00345],
#[0.0155, 0.017, 0.032, 0.0065, 0.0145, 0.0145       ,0.006  ,0.0037],
#[0.017, 0.018, 0.0345, 0.007, 0.018, 0.016         ,0.0069  ,0.00405],
#[0.0175, 0.019, 0.0375, 0.008, 0.022, 0.0175        ,0.00655  ,0.0035],
#[0.0185,0.0215, 0.0415, 0.008, 0.024, 0.02         ,0.007  ,0.0038],
#[0.0195, 0.023, 0.045, 0.049, 0.03, 0.062          ,0.00835  ,0.00425]
#]
#
#
##Settori Large, aggiungere repo
##0,   1    2   3   4    5    6      7
##IM1s,IM2s,IOs,MMs,M10s,M20s, ggBM,ggBI
#Windows_phi = [
#[0.0075, 0.008, 0.013, 0.004, 0.009, 0.0065    ,0.0071 ,0.0036],
#[0.007, 0.008, 0.012, 0.0035, 0.008, 0.006     ,0.0045 ,0.00335],
#[0.007, 0.008, 0.0115, 0.004, 0.008, 0.006     ,0.00465,0.00335],
#[0.0065, 0.0075, 0.0105, 0.0035, 0.0075, 0.006 ,0.0044 ,0.00335], 
#[0.006, 0.007, 0.0095, 0.0035, 0.0065, 0.005   ,0.00425,0.00305], 
#[0.0055, 0.0065, 0.009, 0.0035, 0.006, 0.005   ,0.0042 ,0.00305], 
#[0.0055, 0.006, 0.0085, 0.003, 0.0055, 0.0045  ,0.00415,0.00315], 
#[0.005, 0.006, 0.008, 0.003, 0.0055, 0.0045    ,0.0042 ,0.00305],
#[0.005, 0.0055, 0.0075, 0.003, 0.005, 0.0045   ,0.00415,0.00305], 
#[0.005, 0.0055, 0.0075, 0.003, 0.0055, 0.0045  ,0.00415,0.003], 
#[0.005, 0.0055, 0.0075, 0.003, 0.0055, 0.0045  ,0.00415,0.003], 
#[0.005, 0.0055, 0.007, 0.003, 0.005, 0.0045    ,0.00405,0.00295], 
#[0.005, 0.0055, 0.0075, 0.003, 0.005, 0.0045   ,0.00415,0.003],  
#[0.005, 0.006, 0.008, 0.0035, 0.0055, 0.0045   ,0.004  ,0.003],  
#[0.005, 0.006, 0.0085, 0.003, 0.0055, 0.0045   ,0.00405,0.00305],  
#[0.0055, 0.0065, 0.009, 0.0035, 0.006, 0.0045  ,0.0042 ,0.003], 
#[0.006, 0.007, 0.01, 0.0035, 0.0065, 0.0055    ,0.00425,0.00305],
#[0.0065, 0.0075, 0.011, 0.0035, 0.0075, 0.0055 ,0.00435,0.00335], 
#[0.0065, 0.008, 0.0115, 0.004, 0.0075, 0.006   ,0.00465,0.00345],  
#[0.007,0.0085, 0.0125,0.0035, 0.008, 0.006     ,0.00445,0.00335],  
#[0.008, 0.0085, 0.0135, 0.004, 0.01, 0.0065    ,0.0071 ,0.00365] 
#]
#
#
#
#Windows_small_phi = [
#[0.0185, 0, 0.0235, 0, 0.0085, 0          ,0.0071 ,0.0036],
#[0.0155, 0.0165, 0.021, 0.0045, 0.0085, 0.005  ,0.0045 ,0.00335],
#[0.0145, 0.0155, 0.019, 0.0045, 0.008, 0.005   ,0.00465,0.00335],
#[0.0135, 0.015, 0.018, 0.004, 0.0075, 0.005    ,0.0044 ,0.00335], 
#[0.013, 0.0135, 0.0165, 0.0035, 0.006, 0.0045  ,0.00425,0.00305], 
#[0.011, 0.0115, 0.0145, 0.0035, 0.006, 0.0045  ,0.0042 ,0.00305], 
#[0.01, 0.0105, 0.013, 0.0035, 0.0055, 0.004    ,0.00415,0.00315], 
#[0.009, 0.0095, 0.012, 0.0035, 0.005, 0.004    ,0.0042 ,0.00305],
#[0.008, 0.0085, 0.0105, 0.003, 0.005, 0.0035   ,0.00415,0.00305], 
#[0.0075, 0.0085, 0.0105, 0.003, 0.005, 0.0035  ,0.00415,0.003], 
#[0.0075, 0.0085, 0.0105, 0.003, 0.005, 0.0035  ,0.00415,0.003], 
#[0.0075, 0.0085, 0.01, 0.003, 0.0045, 0.004    ,0.00405,0.00295], 
#[0.0075, 0.0085, 0.0105, 0.0035, 0.005, 0.004  ,0.00415,0.003],  
#[0.0095, 0.0095, 0.0115, 0.003, 0.005, 0.004   ,0.004  ,0.003],  
#[0.01, 0.0105, 0.013, 0.0035, 0.0055, 0.004    ,0.00405,0.00305], 
#[0.011, 0.0115, 0.0145, 0.0035, 0.006, 0.0045  ,0.0042 ,0.003], 
#[0.012, 0.0135, 0.0185, 0.0035, 0.0065, 0.005  ,0.00425,0.00305],
#[0.0135, 0.015, 0.018, 0.0045, 0.009, 0.0055   ,0.00435,0.00335], 
#[0.014, 0.015, 0.0195, 0.0045, 0.008, 0.005    ,0.00465,0.00345], 
#[0.0165, 0.018, 0.023, 0.0045, 0.0095, 0.006   ,0.00445,0.00335], 
#[0.0245, 0, 0.023, 0, 0.012, 0                 ,0.0071 ,0.00365] 
#]
#check_0 = ROOT.TH2F("check_0","check_0",50,-1.4,1.4,50,-3.14,3.14)
#check_1 = ROOT.TH2F("check_1","check_1",50,-1.4,1.4,50,-3.14,3.14)
#check_2 = ROOT.TH2F("check_2","check_2",50,-1.4,1.4,50,-3.14,3.14)
#check_3 = ROOT.TH2F("check_3","check_3",50,-1.4,1.4,50,-3.14,3.14)
#check_4 = ROOT.TH2F("check_4","check_4",50,-1.4,1.4,50,-3.14,3.14)
#check_5 = ROOT.TH2F("check_5","check_5",50,-1.4,1.4,50,-3.14,3.14)
#check_6 = ROOT.TH2F("check_6","check_6",50,-1.4,1.4,50,-3.14,3.14)
#check_7 = ROOT.TH2F("check_7","check_7",50,-1.4,1.4,50,-3.14,3.14)
#check_8 = ROOT.TH2F("check_8","check_8",50,-1.4,1.4,50,-3.14,3.14)
#
#check.append(check_0)
#check.append(check_1)
#check.append(check_2)
#check.append(check_3)
#check.append(check_4)
#check.append(check_5)
#check.append(check_6)
#check.append(check_7)
#check.append(check_8)



def PivotTrigger(c_x,c_layer,c_station,c_match,c_x_phi,c_layer_phi,c_eta_phi):
 isTrig_1_2_1 = 0
 tower = 0 
 tower_final = 0 
 #Define the window to search a trigger
 
 window = [10,10,1,10,10,10,10,20,20]
 
 x_min = -1.4
 x_max = 1.4
 #bin_x = (x_max-x_min) / 1013.
 bin_x = (x_max-x_min) / (512.*2.)

 bin_geta = (1.24+1.24) / (512.*2.)
 #now bin in phi
 bin_phi     =  2*3.14 / (128.)
 x_min_phi = -0.27

# print("Start") 
 list_perLayer = [[],[],[],[],[],[],[],[],[]]
 list_MatchedperLayer = [[],[],[],[],[],[],[],[],[]]
 for indx,x in enumerate(c_x):
  x_int = int( (x-x_min) / bin_x)
  sta = c_station[indx]
  ma = c_match[indx]
#  check[c_layer[indx]].Fill(c_x,c_x)
  lly = c_layer[indx]

  #rimuovi piedi for now
  if lly > 8: continue
  list_perLayer[lly].append(x_int)
  list_MatchedperLayer[lly].append(ma)

 binnned_x = (c_x-x_min) / bin_x
 #Need to zip the c_x and layer
 #convert eta coordinate for phi trigger
 c_eta_phi_int =  (c_eta_phi+1.24) / bin_geta 
 c_eta_phi_int_conv = c_eta_phi_int.astype(int)
 c_eta_phi_int_conv1 = c_eta_phi_int_conv.astype(float)

 #this is no longer needed as now we open a window around the pivot candidate in eta to make the selection
 tower_phi_f = c_eta_phi_int_conv1/48.
 tower_phi_rf = np.rint(tower_phi_f)
 tower_phi   = tower_phi_rf.astype(int)
 
 #loop over the BI to define pivot on layer 2 
 isTrig_pT = []
 pTs = ["5","10","15","20"]
 for idx_pt,pT in enumerate(pTs):
  isTrig_pT.append(0)
  isTrig_3_3_3 = 0
  isTrig_2_2_1 = 0
  isTrig_2_0_1 = 0
  isTrig_2_0_1_halfm = 0
  isTrig_2_0_1_halfp = 0
  isTrig_2_0_1_half = 0
  DeltaX = [[],[],[],[],[],[],[],[],[]]
  Windows_store = [[],[],[],[],[],[],[],[],[]]
  current_pivot = []
  current_pivot_eta = []
  max_prio_eta = 0
  max_prio_phi = 0
  current_pivot_phi = []
  isTrig_2_0_1_perPivot_final = 0
  isTrig_2_0_1_perPivot_phi_event = 0
  #Running now the different trigger regimes
  Windows =           getattr(DB.Coinc, "Windows_"+pT) 
  Windows_small =     getattr(DB.Coinc, "Windows_small_"+pT)
  Windows_phi  =      getattr(DB.Coinc, "Windows_phi_"+pT)
  Windows_small_phi = getattr(DB.Coinc, "Windows_small_phi_"+pT)
  for cell in range(1024): 
    isTrig_2_0_1_perPivot = 0
    con = True
  #  if (cell in list_perLayer[5]): con  = False
    if ((cell in list_perLayer[0]) or (cell in list_perLayer[1]) or (cell in list_perLayer[2]) or ( cell in list_perLayer[3]) or (cell in list_perLayer[4]) or (cell in list_perLayer[5]) or (cell in list_perLayer[5]) or ( cell in list_perLayer[6]) or (cell in list_perLayer[7]) or (cell in list_perLayer[8])): con  = False
  #  if con == True: continue

  #for indx,x in enumerate(c_x):
    #here I need to define the tower 
    #if math.fabs(x)>1.1:continue
    NTOWERS = 21
    ETARANGE = 2.2
    TOWERSIZE    = 2 * ETARANGE / NTOWERS 
    ETAHALFRANGE =  ETARANGE / 2.

    #now connect the cell value to the eta' coordinates and then to the global eta coordinate, gets corresponding bin index and check which tower has been fired
    etaprime = (cell)*bin_x+x_min
    globaleta = 0
    if(etaprime!=0):globaleta = -1*math.log(math.tan(0.5*math.atan(math.fabs(1./etaprime))))
    if(etaprime<0):globaleta = -1*globaleta
    globaleta_idx =(int) ((globaleta+1.24)/bin_geta)
    tower = int(float(globaleta_idx/48.))
    #tower = int((float(cell)/48.)) 
    #tower = int((2.0 * ((-1.1+float(cell)/512.) + ETAHALFRANGE) / TOWERSIZE) - 0.5)
       
# 0,   1    2   3   4    5    6
# IM1s,IM2s,IOs,MMs,M10s,M20s,OOs



    #x_int = int( (x-x_min) / bin_x)
    x_int = cell 
    #require pivot  
    #if c_layer[indx] == 2 or c_layer[indx] == 1 or c_layer[indx] == 0  : 
    decision = [0,0,0,0,0,0,0,0,0]

    decision_halfp = [0,0,0,0,0,0,0,0,0]
    decision_halfm = [0,0,0,0,0,0,0,0,0]
   
   #,   1    2   3   4    5    6      7
   #M1s,IM2s,IOs,MMs,M10s,M20s, ggBM,ggBI
   
    for ly in range(9):
       idx_t = -1
       if ly ==0: idx_t = 1 # here need to change
       if ly ==1: idx_t = 1 # here also need to change 
       if ly ==2: idx_t = 1 # here also need to change
       if ly ==3: idx_t = 3
       if ly ==4: idx_t = 3
       if ly ==5: idx_t = 6
       if ly ==6: idx_t = 6
       if ly ==7: idx_t = 5
       if ly ==8: idx_t = 5
       if tower == 21: tower = 20
       if tower < 0: print(tower,x,idx_t)
   
       Wind = 0
       if sta == "L" or sta == "M" or sta == "R":
        Wind = (  ( Windows[tower][idx_t] / bin_x ) )  
       else :
        Wind = (  ( Windows_small[tower][idx_t] / bin_x )  )
       Wind = int(round(Wind))
   #   Wind =  int(round( Windows[tower][idx_t] / (bin_x)))
   
       #if ly == 2: continue
       mean = x_int
       #mean = x_int + Mean
       for idx_bi in range(mean-Wind,mean+Wind+1):
        if idx_bi in list_perLayer[ly]:
         decision[ly] = 1
       #check half windows
   
    hit_bi = 0
    hit_bm = 0
    hit_bo = 0
   
    if 1 ==  decision[0]: hit_bi = 1 + hit_bi
    if 1 ==  decision[1]: hit_bi = 1 + hit_bi
    if 1 ==  decision[2]: hit_bi = 1 + hit_bi
     
    if 1 ==  decision[3]: hit_bm = 1 + hit_bm
    if 1 ==  decision[4]: hit_bm = 1 + hit_bm
    if 1 ==  decision[5]: hit_bm = 1 + hit_bm
    if 1 ==  decision[6]: hit_bm = 1 + hit_bm
   
    if 1 ==  decision[7]: hit_bo = 1 + hit_bo
    if 1 ==  decision[8]: hit_bo = 1 + hit_bo
   
   
    #if hit_bi > 0 or hit_bm > 0 or hit_bo >0: print(hit_bi,hit_bm,hit_bo,"****************",cell-512,Wind,decision[7],decision[8])
    #3/3 hit_bm > 2 and hit_bo > 0
    #if hit_bi > 1: isTrig_2_0_1 = 1
    if (hit_bi > 1 and hit_bo > 0) or ( (hit_bi > 1 and (hit_bm+hit_bo)>2  ) or (hit_bm > 2 and hit_bo> 0)) : isTrig_2_0_1 = 1
    #if ((hit_bm > 2 and hit_bo> 0)): isTrig_2_0_1_perPivot = 1 
    if ( (hit_bi > 1 and hit_bo > 0) or (hit_bi > 1 and (hit_bm+hit_bo)>2  ) or (hit_bm > 2 and hit_bo> 0)): isTrig_2_0_1_perPivot = 1 
    #if ( (hit_bi > 1 and (hit_bm+hit_bo)>2  ) or (hit_bm > 2 and hit_bo> 0)): isTrig_2_0_1_perPivot = 1 
    if ( (hit_bi > 1 and (hit_bm+hit_bo)>2  ) or (hit_bm > 2 and hit_bo> 0)) : isTrig_2_2_1 = 1 
    if (  (hit_bm > 2 and hit_bo> 0)) : isTrig_3_3_3 = 1 
   
    
    #Now given the tower of the current pivot, we can look if there is a coincidence in phi as well
    if(isTrig_2_0_1_perPivot == 1):
     #print("pivot,",Wind,tower, mean,(mean)*bin_x+x_min, decision[0],decision[1],decision[2],decision[3],decision[4],decision[5],decision[6],decision[7],decision[8])
     #print("********* phi ********")
     
     pivot_etaprime = (mean)*bin_x+x_min
     globaleta_pivot = 0
     if(pivot_etaprime!=0):globaleta_pivot = -1*math.log(math.tan(0.5*math.atan(math.fabs(1./pivot_etaprime))))
     if(pivot_etaprime<0): globaleta_pivot = -1*globaleta_pivot
     globaleta_idx_pivot =(int) ((globaleta_pivot+1.24)/bin_geta)
     #around this, look for a spots around the indeces  
     #ind_eta = np.asarray(tower_phi) == tower
     ind_etam = np.asarray(c_eta_phi_int_conv1) > globaleta_idx_pivot - 24  
     ind_etap = np.asarray(c_eta_phi_int_conv1) < globaleta_idx_pivot + 24
     #mask_3 = np.logical_or(ind_eta, ind_etam)
     mask = np.logical_and(ind_etam, ind_etap)
     bin_phi = 128.
     bin_phi = (0.27-x_min_phi) / (128.)
     phi_int =  (c_x_phi-x_min_phi) / bin_phi
     phi_int = phi_int.astype(int)
     phi_int = phi_int[mask]
     layer_phi = c_layer_phi[mask]
     #print("-------------",phi_int,layer_phi,c_eta_phi[mask])
     #now run the trigger coincidence
     cand_layer_phi = [[],[],[],[],[],[],[],[],[]]
     for idx,bin_p in enumerate(phi_int):
        if layer_phi[idx] > 8: continue
        cand_layer_phi[layer_phi[idx]].append(bin_p)
     for bin_p in range(128):
      isTrig_2_0_1_perPivot_phi = 0
      decision_phi = [0,0,0,0,0,0,0,0,0]
      for ly in range(9):
       idx_t = -1
       if ly ==0: idx_t = 1 # here need to change
       if ly ==1: idx_t = 1 # here also need to change 
       if ly ==2: idx_t = 1 # here also need to change
       if ly ==3: idx_t = 3
       if ly ==4: idx_t = 3
       if ly ==5: idx_t = 6
       if ly ==6: idx_t = 6
       if ly ==7: idx_t = 5
       if ly ==8: idx_t = 5
       if tower == 21: tower = 20
       if tower < 0: print(tower,x,idx_t)
   
       Wind = 0
       if sta == "L" or sta == "M" or sta == "R":
        Wind = (  ( Windows_phi[tower][idx_t] / bin_phi ) )  
       else :
        Wind = (  ( Windows_small_phi[tower][idx_t] / bin_phi )  )
       Wind = int(round(Wind))
   
       mean_phi = bin_p
       for idx_bi in range(mean_phi-Wind,mean_phi+Wind+1):
        if idx_bi in cand_layer_phi[ly]:
         decision_phi[ly] = 1
      
      hit_bi_phi = 0
      hit_bm_phi = 0
      hit_bo_phi = 0
      if 1 ==  decision_phi[0]: hit_bi_phi = 1 + hit_bi_phi
      if 1 ==  decision_phi[1]: hit_bi_phi = 1 + hit_bi_phi
      if 1 ==  decision_phi[2]: hit_bi_phi = 1 + hit_bi_phi
      if 1 ==  decision_phi[3]: hit_bm_phi = 1 + hit_bm_phi
      if 1 ==  decision_phi[4]: hit_bm_phi = 1 + hit_bm_phi
      if 1 ==  decision_phi[5]: hit_bm_phi = 1 + hit_bm_phi
      if 1 ==  decision_phi[6]: hit_bm_phi = 1 + hit_bm_phi
   
      if 1 ==  decision_phi[7]: hit_bo_phi = 1 + hit_bo_phi
      if 1 ==  decision_phi[8]: hit_bo_phi = 1 + hit_bo_phi
      if ( (hit_bi_phi > 1 and hit_bo_phi > 0) or (hit_bi_phi > 1 and (hit_bm_phi+hit_bo_phi)>2  ) or (hit_bm_phi > 2 and hit_bo_phi> 0)): isTrig_2_0_1_perPivot_phi = 1
      if ( (hit_bi_phi > 1 and hit_bo_phi > 0) or (hit_bi_phi > 1 and (hit_bm_phi+hit_bo_phi)>2  ) or (hit_bm_phi > 2 and hit_bo_phi> 0)): isTrig_2_0_1_perPivot_event = 1
     
      
      if( isTrig_2_0_1_perPivot_phi == 1 and isTrig_2_0_1_perPivot == 1) : 
       tower_final = tower
       isTrig_2_0_1_perPivot_final = 1
       isTrig_pT[idx_pt] = 1
   #    print("pivot,", mean, decision[0],decision[1],decision[2],decision[3],decision[4],decision[5],decision[6],decision[7],decision[8])
   #    print("pi_ph,", mean_phi, decision_phi[0],decision_phi[1],decision_phi[2],decision_phi[3],decision_phi[4],decision_phi[5],decision_phi[6],decision_phi[7],decision_phi[8])
       current_pivot_eta.append([mean,decision])
       trig_topo_eta = ordering(decision)
       trig_topo_phi = ordering(decision_phi)
       #WARNING: this is assuming 1 candidate per event, it shall be for each tower
       if(trig_topo_eta > max_prio_eta): max_prio_eta = trig_topo_eta
       if(trig_topo_phi > max_prio_phi): max_prio_phi = trig_topo_phi
       current_pivot_phi.append([mean_phi,decision_phi])
  #This is to select which pivot has to be considered 
  best_candidates = []
  memory_pivot_eta = []
  memory_pivot_phi = []
  #now select best candidate in eta - shall do this in a function
  #Need to generalize if we have more than one candidate, possibly by running over each tower
  final_cand_eta = trigger_cand(current_pivot_eta,max_prio_eta)
  final_cand_phi = trigger_cand(current_pivot_phi,max_prio_phi)
  # for cand in current_pivot_eta:
  #  #remove double counting from the 2D grid
  #  if(cand[0] in memory_pivot_eta):continue
  #  memory_pivot_eta.append(cand[0])
  #  eta_coin = cand[1]
  #  #order based on the priority
  #  trig_topo = ordering(eta_coin)
  #  if(trig_topo<max_prio_eta):continue  
  ##  print(cand[0],trig_topo,eta_coin,(cand[0])*bin_x+x_min)
  #  final_cand_eta.append(cand[0])
  
  closest_perLayer = [[],[],[],[],[],[],[],[],[]]
  choice_pivot = -1
  choice_pivot_phi = -1
  num_piv = 0
#  print(final_cand_eta) 
  if(len(final_cand_eta)>0):
    n_tot = len(final_cand_eta)
    mid =  n_tot/2
    choice_pivot = final_cand_eta[mid]
#  print(final_cand_phi) 
  if(len(final_cand_phi)>0):
    n_tot = len(final_cand_phi)
    mid =  n_tot/2
    choice_pivot_phi = final_cand_phi[mid]
  
    
    mean = choice_pivot
    #tower = int((float(mean)/48.)) 
    #print("choose: ",choice_pivot,(mean)*bin_x+x_min,current_pivot,isTrig_3_3_3,isTrig_2_0_1,tower) 
    #here we get the closest hit per layer
    #####################################
    for ly in range(9):
      idx_t = -1
      if ly ==0: idx_t = 1 # here need to change
      if ly ==1: idx_t = 1 # here also need to change 
      if ly ==2: idx_t = 1 # here also need to change
      if ly ==3: idx_t = 3
      if ly ==4: idx_t = 3
      if ly ==5: idx_t = 6
      if ly ==6: idx_t = 6
      if ly ==7: idx_t = 5
      if ly ==8: idx_t = 5
      if tower_final == 21: tower_final = 20
      if tower_final < 0: print(tower_final,x,idx_t)
      Wind = 0
      if sta == "L" or sta == "M" or sta == "R":
       Wind = (  ( Windows[tower_final][idx_t] / bin_x ) )  
      else :
       Wind = (  ( Windows_small[tower_final][idx_t] / bin_x )  )
      Wind = int(round(Wind))
      #this is now not the closest but rather the largest 
      closest = -100
      isMatch = -1
      for idx_bi in range(mean-Wind,mean+Wind+1):
       if idx_bi in list_perLayer[ly]:
          DeltaX[ly].append(idx_bi-mean)
          Windows_store[ly].append(Wind)
#          print("Windows:",Wind)
          if(math.fabs(idx_bi-mean) > closest): 
            closest = idx_bi-mean
            isMatch = list_MatchedperLayer[ly][list_perLayer[ly].index(idx_bi)]
          #  print("closest hit is:",closest, "for lay",ly)
      closest_perLayer[ly].append(closest) 
      closest_perLayer[ly].append(isMatch)
    #####################################
      
#  print("closest",closest_perLayer,Wind,tower,tower_final)
  eta_track = (choice_pivot)*bin_x+x_min
  phi_track = (choice_pivot_phi)*bin_phi+x_min_phi
  #print(isTrig_2_0_1_perPivot_final, "pTs ------ ", pT ,"   coordinates,",eta_track,phi_track,choice_pivot_phi)
 print("  FOR ALL LAYERS:   ",isTrig_pT)
 return isTrig_2_0_1_perPivot_final,isTrig_2_0_1,isTrig_3_3_3,DeltaX,Windows_store,isTrig_2_0_1_half,tower,closest_perLayer,eta_track,isTrig_pT
