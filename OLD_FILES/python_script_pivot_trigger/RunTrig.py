import argparse
import os
import ROOT
import trigger
import numpy as np
import math


#f = ROOT.TFile.Open("/eos/atlas/user/n/nbruscin/MuonPhase2Ntuples/hacked_ntuples/out_SingleLepton_data18.00363947.Main.root")
f = ROOT.TFile.Open("out.root")

tree = f.Get('tree')


trig_pass_201  = ROOT.TH2F("trig_pass_201","trig_pass_201",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_201 = ROOT.TH2F("trig_total_201","trig_total_201",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_201_pt   = ROOT.TH1F("trig_pass_201_pt","trig_pass_201_pt",80,0,40)
trig_total_201_pt  = ROOT.TH1F("trig_total_201_pt","trig_total_201_pt",80,0,40)

trig_pass_201_pt_half   = ROOT.TH1F("trig_pass_201_pt_half","trig_pass_201_pt_half",80,0,40)
trig_total_201_pt_half  = ROOT.TH1F("trig_total_201_pt_half","trig_total_201_pt_half",80,0,40)

trig_pass_221  = ROOT.TH2F("trig_pass_221","trig_pass_221",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_221 = ROOT.TH2F("trig_total_221","trig_total_221",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_333  = ROOT.TH2F("trig_pass_333","trig_pass_333",  50,-1.4,1.4,50,-3.14,3.14)
trig_total_333 = ROOT.TH2F("trig_total_333","trig_total_333",50,-1.4,1.4,50,-3.14,3.14)

trig_pass_221_pt   = ROOT.TH1F("trig_pass_221_pt","trig_pass_221_pt",80,0,40)
trig_total_221_pt  = ROOT.TH1F("trig_total_221_pt","trig_total_221_pt",80,0,40)

trig_pass_333_pt   = ROOT.TH1F("trig_pass_333_pt","trig_pass_333_pt",80,0,40)
trig_total_333_pt  = ROOT.TH1F("trig_total_333_pt","trig_total_333_pt",80,0,40)

trig_pass_q  = ROOT.TH1F("trig_pass_q","trig_pass_q",  25,0,100)
trig_total_q = ROOT.TH1F("trig_total_q","trig_total_q",25,0,100)


trig_pass_q_eta  = ROOT.TH1F("trig_pass_q_eta","trig_pass_q_eta",  30,-1.5,1.5)
trig_total_q_eta = ROOT.TH1F("trig_total_q_eta","trig_total_q_eta",30,-1.5,1.5)

prob_pass_pt  = ROOT.TH1F("prob_pass_pt ","prob_pass_pt ",  30,0,30)
prob_total_pt = ROOT.TH1F("prob_total_pt","prob_total_pt",  30,0,30)

plot_deltaW = []
plot_numb = []
plot_num_match = []
plot_den_match = []

for ly in range(22):
  deltaW  = ROOT.TH2F("delta_"+str(ly),"delta"+str(ly),40,-20,20,9,0,9)
  plot_deltaW.append(deltaW) 
  numb  = ROOT.TH2F("numb_"+str(ly),"numb"+str(ly),15,0,15,30,0,40)
  plot_numb.append(numb) 

  if ly < 10:
   match_num  = ROOT.TH1F("match_num"+str(ly),"match_num"+str(ly),  25,0,100)
   match_den  = ROOT.TH1F("match_den"+str(ly),"match_den"+str(ly),  25,0,100)
   plot_num_match.append(match_num)
   plot_den_match.append(match_den)


pTs = ["5","10","15","20"]
pT_Trig_num = []
pT_Trig_den = []
for pT in pTs:
   isTrig_num  = ROOT.TH1F("isTrig_num"+str(pT),"isTrig_num"+str(pT),  25,0,30)
   isTrig_den  = ROOT.TH1F("isTrig_den"+str(pT),"isTrig_den"+str(pT),  25,0,30)
   pT_Trig_num.append(isTrig_num)
   pT_Trig_den.append(isTrig_den)


trig_check  = ROOT.TH2F("trig_check","trig_check",  50,-1.4,1.4,50,-3.14,3.14)
trig_pT_largest  = ROOT.TH2F("trig_pT_largest","trig_pT_largest",  25,0,100,40,-20,20)
residuals_eta  = ROOT.TH2F("residuals_eta","residuals_eta",  25,0,100,1000,-1.2,1.2)
#trig_pT_largest  = ROOT.TH3F("trig_pT_largest","trig_pT_largest",  25,0,100,30,-1.5,1.5,20,-1,19)


i = -1
evn = 0
sel_evn = 0
number_of_tracks = 0
for event in f.tree:
        evn =  evn + 1
        #if evn < 349: continue
        if evn >55000000: break
        #if evn !=34: continue
        ncl = len(event.x)
        x = event.x 
        mPhi = event.mPhi 
        time = event.time
        layer = event.layer 
        g_match = event.g_match
        station_eta = event.station_eta
        station_phi = event.station_phi
        station_name = event.station_name
        trk_pt = event.trk_pt
        trk_q = event.trk_q
        trk_eta = event.trk_eta
        trk_phi = event.trk_phi
        g_eta = event.g_eta
        g_phi = event.g_phi
        #For now use single muon event
        if len(trk_pt) ==0 or len(trk_pt)>1: 
         number_of_tracks = number_of_tracks + 1
         continue
#        if math.fabs(trk_eta[0]) < 1.0: continue       
        #if(trk_q[0] >0): continue
        #if(trk_eta[0] >0): continue
        if(trk_pt[0] >30 ): continue
        if len(x) < 2: continue
 
        ind1 = np.asarray(mPhi) == 0 
        ind2_R = np.asarray(station_name) != "R"
        ind2_M = np.asarray(station_name) != "M"
        ind2_F = np.asarray(station_name) != "F"
        ind2_G = np.asarray(station_name) != "G"
        ind2_L = np.asarray(station_name) == "L"
        ind3 = np.asarray(station_phi) == 3
        ind = ind1
        #if True in ind2: continue
        #ind = ind1*ind2_R*ind2_M*ind2_F*ind2_G 
        #do not consider small sector
        ncell = 0
        #rude way to remove the Small sector
#        for i in ind:
#         if i == True: ncell = ncell + 1
#        if ncell < 3: continue

        #ind = np.multiply(ind1,ind2) 
        #ind = np.asarray(mPhi) > 0 and np.asarray(station_name) == "L" 
        xt = np.asarray(x)
        lay = np.asarray(layer)
        sta = np.asarray(station_name)
        match = np.asarray(g_match)
        eta = np.asarray(g_eta)

        x4Trig       = xt[ind]
        ly4Trig      = lay[ind]
        sta4Trig     = sta[ind]
        g_match4Trig = match[ind]
        g_eta       = eta[ind]
        #and now pass the phi coordinates
        ind_phi = np.asarray(mPhi) == 1 
        x4Trig_phi       = xt[ind_phi]
        ly4Trig_phi      = lay[ind_phi]
        sta4Trig_phi     = sta[ind_phi]
        g_match4Trig_phi = match[ind_phi]
        g_eta_phi        = eta[ind_phi]


        #if len(x4Trig) < 1: continue

        isTrig_2_0_1,isTrig_2_2_1,isTrig_3_3_3, list_strips, list_Windows,isTrig_2_0_1_half,tower,closest,Etrk_eta, isTrig_pT = trigger.PivotTrigger(x4Trig ,ly4Trig,sta4Trig,g_match4Trig,x4Trig_phi,ly4Trig_phi,g_eta_phi)



        gen_trig = 0
        if 1 in isTrig_pT: gen_trig = 1
        print("done",trk_pt[0],trk_eta[0],trk_phi[0],evn,isTrig_2_0_1,isTrig_2_2_1,trk_eta[0]-Etrk_eta)
        #check if the pT is assigned correctly, for now we have 5,10,15,20
        if(gen_trig == 1):
         pT = 0
         for idx,is_Trig_pT in enumerate(isTrig_pT):
           if is_Trig_pT == 1:
             pT = 5*(idx+1)
         pt_t = trk_pt[0]
         if pt_t > 20: pt_t = 20
         if(math.fabs(pT-pt_t) < 5): prob_pass_pt.Fill(trk_pt[0])
         prob_total_pt.Fill(trk_pt[0])
         print(math.fabs(pT-pt_t) )

        for idx,isT in enumerate(isTrig_pT):
          if isT == 1:
            pT_Trig_num[idx].Fill(trk_pt[0])
          pT_Trig_den[idx].Fill(trk_pt[0]) 

        if(isTrig_2_0_1):
         residuals_eta.Fill(trk_q[0]*trk_pt[0],trk_eta[0]-Etrk_eta)
         sign_BI  = 0
         sign_BM1 = 0
         sign_BM2 = 0
         sign_BO  = 0
         Issign = [0,0,0,0,0,0,0,0,0]
         #delta Windows vs trk pT, for which eta?
         #trig_pT_largest.Fill()
         minL = 100
         maxL = 0
         if(len( closest[0] )>0):
          for lly in range(9):
           if closest[lly][0] != -100 and lly < minL: minL = lly   
           if closest[8-lly][0] != -100 and (8-lly) > maxL: maxL = 8-lly  
#          print("max,",maxL," min ",minL,math.fabs(closest[maxL][0] - closest[minL][0] )) 
          trig_pT_largest.Fill(trk_pt[0],closest[maxL][0] - closest[minL][0])
         for lly in range(9):
         #fill probably to get a match#
          if(len(closest[lly]) < 1): continue
          if(closest[lly][0] != 99):
           if(closest[lly][1] > 0): plot_num_match[lly].Fill(trk_pt[0])
           plot_den_match[lly].Fill(trk_pt[0])
           
        
         #-------------
          en = -1
          if(len(list_Windows[lly]) > 0): Issign[lly] = 1
          for lW in list_Windows[lly]: 
           en = en + 1
           dW = float(list_strips[lly][en])
            #dW = float(list_strips[lly][en])/float(lW)
           plot_deltaW[tower].Fill(dW,lly)

           if(dW > 0 and lly <=2): sign_BI = sign_BI + 1 
           if(dW > 0 and (lly >=3 and lly <=4)): sign_BM1 = sign_BM1 + 1 
           if(dW > 0 and (lly >=5 and lly <=6)): sign_BM2 = sign_BM2 + 1 
           if(dW > 0 and (lly >=7 and lly <=8)): sign_BO = sign_BO + 1 

           if(dW < 0 and lly <=2): sign_BI = sign_BI - 1 
           if(dW < 0 and (lly >=3 and lly <=4)): sign_BM1 = sign_BM1 - 1 
           if(dW < 0 and (lly >=5 and lly <=6)): sign_BM2 = sign_BM2 - 1 
           if(dW < 0 and (lly >=7 and lly <=8)): sign_BO = sign_BO - 1 
 

          plot_numb[lly].Fill(en,trk_pt[0])
         Trig_qDecision = 0

         if(Issign[7] == 1 or Issign[8] == 1 ):

            if((Issign[3]==1 or Issign[4]==1) and (Issign[5]==1 or Issign[6]==1)):
             if( (sign_BM1 <= 0 or sign_BI < 0 ) and (sign_BO >= 0 or sign_BM2 >= 0)): Trig_qDecision = 1
             elif((sign_BM1 >= 0 or sign_BI > 0 ) and (sign_BO <= 0 or sign_BM2 <= 0)): Trig_qDecision = -1
            if((Issign[3]==0 and Issign[4]==0) and (Issign[5]==1 or Issign[6]==1)):
             if( (sign_BI < 0 ) and (sign_BO >= 0 or sign_BM2 >= 0)): Trig_qDecision = 1
             elif((sign_BI > 0 ) and (sign_BO <= 0 or sign_BM2 <= 0)): Trig_qDecision = -1
            if((Issign[3]==1 or Issign[4]==1) and (Issign[5]==0 and Issign[6]==0)):
             if( (sign_BM1 <= 0 or sign_BI < 0 ) and (sign_BO  >= 0)): Trig_qDecision = 1
             elif((sign_BM1 >= 0 or sign_BI > 0 ) and (sign_BO <= 0)): Trig_qDecision = -1

         elif((Issign[0] == 1 or Issign[1] == 1  or Issign[2] == 1) and (Issign[7] == 0 or Issign[8] == 0) ):

            if((Issign[3]==1 or Issign[4]==1) and (Issign[5]==1 or Issign[6]==1)):
             if( (sign_BM1 <= 0 or sign_BI <= 0 ) and (sign_BO > 0 or sign_BM2 >= 0)): Trig_qDecision = 1
             elif((sign_BM1 >= 0 or sign_BI >= 0 ) and (sign_BO < 0 or sign_BM2 <= 0)): Trig_qDecision = -1
            if((Issign[3]==0 and Issign[4]==0) and (Issign[5]==1 or Issign[6]==1)):
             if( (sign_BI <= 0 ) and (sign_BO > 0 or sign_BM2 >= 0)): Trig_qDecision = 1
             elif((sign_BI >= 0 ) and (sign_BO < 0 or sign_BM2 <= 0)): Trig_qDecision = -1
            if((Issign[3]==1 or Issign[4]==1) and (Issign[5]==0 and Issign[6]==0)):
             if( (sign_BM1 <= 0 or sign_BI <= 0 ) and (sign_BO  > 0)): Trig_qDecision = 1
             elif((sign_BM1 >= 0 or sign_BI >= 0 ) and (sign_BO < 0)): Trig_qDecision = -1


         elif((Issign[0] == 1 or Issign[1] == 1  or Issign[2] == 1) and (Issign[7] == 1 or Issign[8] == 1) ):

             if( (sign_BI < 0 ) and (sign_BO >= 0 )): Trig_qDecision = 1
             elif((sign_BI > 0 ) and (sign_BO <= 0 )): Trig_qDecision = -1



         elif((Issign[0] == 0 or Issign[1] == 0 or Issign[2] == 0) and (Issign[7] == 0 or Issign[8] == 0)):
            if((Issign[3]==1 or Issign[4]==1) and (Issign[5]==1 or Issign[6]==1)):
             if( (sign_BM1 <= 0 or sign_BI < 0 ) and (sign_BO > 0 or sign_BM2 >= 0)): Trig_qDecision = 1
             elif((sign_BM1 >= 0 or sign_BI > 0 ) and (sign_BO < 0 or sign_BM2 <= 0)): Trig_qDecision = -1
            if((Issign[3]==0 and Issign[4]==0) and (Issign[5]==1 or Issign[6]==1)):
             if( (sign_BI < 0 ) and (sign_BO > 0 or sign_BM2 >= 0)): Trig_qDecision = 1
             elif((sign_BI > 0 ) and (sign_BO < 0 or sign_BM2 <= 0)): Trig_qDecision = -1
            if((Issign[3]==1 or Issign[4]==1) and (Issign[5]==0 and Issign[6]==0)):
             if( (sign_BM1 <= 0 or sign_BI < 0 ) and (sign_BO  > 0)): Trig_qDecision = 1
             elif((sign_BM1 >= 0 or sign_BI > 0 ) and (sign_BO < 0)): Trig_qDecision = -1




         if Trig_qDecision == trk_q[0]: trig_pass_q.Fill(trk_pt[0])
         if Trig_qDecision == trk_q[0]: trig_pass_q_eta.Fill(trk_eta[0])
         trig_total_q.Fill(trk_pt[0])
         trig_total_q_eta.Fill(trk_eta[0])
         #if(Trig_qDecision != 0):trig_total_q.Fill(trk_pt[0])
         if Trig_qDecision != trk_q[0]:
          print(sign_BI,sign_BM1,sign_BM2,sign_BO)
          

        if math.fabs(trk_eta[0]) < 1.1: 

             
          if 0 not in ly4Trig or 1 not in ly4Trig or 2 not in ly4Trig:
           trig_check.Fill(trk_eta[0],trk_phi[0])
 
          if isTrig_2_0_1: 
           if trk_pt[0] > 20: trig_pass_201.Fill(trk_eta[0],trk_phi[0])
           trig_pass_201_pt.Fill(trk_pt[0])
          if trk_pt[0] > 20: trig_total_201.Fill(trk_eta[0],trk_phi[0])
          trig_total_201_pt.Fill(trk_pt[0])


          if isTrig_2_0_1_half: 
           trig_pass_201_pt_half.Fill(trk_pt[0])
          trig_total_201_pt_half.Fill(trk_pt[0])


          if isTrig_2_2_1: 
            if trk_pt[0] > 20: trig_pass_221.Fill(trk_eta[0],trk_phi[0])
            trig_pass_221_pt.Fill(trk_pt[0])
          if trk_pt[0] > 20: trig_total_221.Fill(trk_eta[0],trk_phi[0])
          trig_total_221_pt.Fill(trk_pt[0])
          if isTrig_3_3_3: 
            if trk_pt[0] > 20: trig_pass_333.Fill(trk_eta[0],trk_phi[0])
            trig_pass_333_pt.Fill(trk_pt[0])
          trig_total_333_pt.Fill(trk_pt[0])
          if trk_pt[0] > 20: trig_total_333.Fill(trk_eta[0],trk_phi[0])



File_trig = ROOT.TFile("Trig_results.root","recreate")
File_trig.cd()
trig_pass_221.Divide(trig_total_221)
trig_pass_221.Write()
trig_total_221.Write()

trig_pass_221_pt.Divide(trig_total_221_pt)
trig_pass_221_pt.Write()


trig_pass_333.Divide(trig_total_333)
trig_pass_333.Write()



trig_pass_201.Divide(trig_total_201)
trig_pass_201.Write()

trig_pass_201_pt.Divide(trig_total_201_pt)
trig_pass_201_pt.Write()

trig_pass_201_pt_half.Divide(trig_total_201_pt)
trig_pass_201_pt_half.Write()


trig_pass_333_pt.Divide(trig_total_333_pt)
trig_pass_333_pt.Write()

enu = - 1
for pl in plot_deltaW:
 enu = enu + 1
 pl.Write()
# plot_numb[enu].Write()

trig_pass_q.Divide(trig_total_q)
trig_pass_q.Write()


trig_pass_q_eta.Divide(trig_total_q_eta)
trig_pass_q_eta.Write()

nn = -1
for lY in plot_num_match:
 nn = nn + 1
 lY.Divide(plot_den_match[nn])
 lY.Write()

for idx,pt_plot in enumerate(pT_Trig_num):
  pt_plot.Divide(pT_Trig_den[idx])
  pt_plot.Write()

prob_pass_pt.Divide(prob_total_pt)
prob_pass_pt.Write() 

residuals_eta.Write()
trig_pT_largest.Write()
trig_check.Write()

