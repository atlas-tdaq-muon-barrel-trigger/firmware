import argparse
import os
import ROOT
import numpy as np
import math
import uproot



def computeOffset_eta(strip,station_eta,doubleZ):
   max_station = 10
   max_offset = {} 
   max_offset_1 = {} 
   integral_offset = {} 
   integral_offset_1 = {} 


   for sta in range(-max_station,max_station):
       count = strip[station_eta==sta].shape[0]
       if count == 0: continue
       strip_1 = strip[station_eta == sta]
       doubleZ_1 = doubleZ[station_eta == sta]
       #divide into doubleZ
       ind_1 = doubleZ_1 == 1
       ind_2 = doubleZ_1 == 2

       strip_max_1 = strip_1[ind_1]
       strip_max_2 = strip_1[ind_2]
       count1 = strip_max_1.shape[0]
       count2 = strip_max_2.shape[0]
       max1 = 0
       max2 = 0
       if count1 != 0:
         max1 = np.max(strip_max_1)
       if count2 != 0:
         max2 = np.max(strip_max_2)
       #sum them up
       max_offset.update({sta:max1+max2})
       max_offset_1.update({sta:max1})

   count = 0
   for key, value in max_offset.items(): 
       count = value + count
       integral_offset.update({key:count})  

   return integral_offset, max_offset_1


#f = uproot.open("out.root")
#f = uproot.open("out_Zmumu_mu0_cb0_data18.00363947.Main.root")
f = uproot.open("out_all.root")
tree = f['tree']


h_layers = []
h_layers_512 = []
for ly in range(10):
    if ly < 5:
        emin = -1.36
        emax = 1.36
    elif ly >= 5 and ly < 7:
        emin = -1.29
        emax = 1.29
    elif ly >= 7 and ly < 9:
        emin = -1.25
        emax = 1.25

    eta_strip      = ROOT.TH2F("eta_strip_"+str(ly),"eta_strip_"+str(ly),  900,0,900,10000,-2,2)
    eta_strip_512  = ROOT.TH2F("eta_strip_512_"+str(ly),"eta_strip_512_"+str(ly),  900,0,900,512*2,emin,emax)
    h_layers.append(eta_strip)
    h_layers_512.append(eta_strip_512)


inputs = [ 
        "mPhi",
        "layer",
        "station_eta",
        "station_phi",
        "station_name",
        "strip",
        "doubletPhi",
        "doubletZ",
        "g_eta",
        "x",
        ]


full_data_array_nosel = {}

for var in inputs: 
    full_data_array_nosel[var] = tree[var].array(library='np', entry_stop=490)
    #flatten the array
    full_data_array_nosel[var] = np.concatenate( full_data_array_nosel[var] )



for ly in range(9):
    print("processing layer: ",ly)
    #selections
    full_data_array = {}
    ind1 = full_data_array_nosel["mPhi"] == 0
    ind_l = full_data_array_nosel["layer"] == ly
    ind_phi = full_data_array_nosel["station_phi"] == 3
    ind2_M = full_data_array_nosel["station_name"] == "L"
    #ind_f = ind1 * ind_l * ind_phi
    ind_f = ind1 * ind2_M * ind_l * ind_phi
    
    for var in inputs: 
        full_data_array[var] = full_data_array_nosel[var][ind_f]  
    
    
    
    off_set = {}
    off_set,off_set_1 = computeOffset_eta(full_data_array["strip"],full_data_array["station_eta"],full_data_array["doubletZ"]) 
    off_set.update({0:0})
    off_set_1.update({0:0})

#Offset found by hand. This might be related to each sector, otherwise it will cause discontinouty. Shall follow up
    if( ly < 5):
        off_set_1.update({-7:0})
        off_set.update({-7:0})
        off_set_1.update({-9:0})
        off_set.update({-9:0})
    elif(ly >= 5 and ly < 7):
        off_set_1.update({-8:0})
        off_set.update({-8:0})
        off_set_1.update({-7:0})
        off_set.update({-7:0})
    elif(ly == 7 or ly == 8):
        off_set_1.update({-7:0})
        off_set.update({-7:0})

    #full_data_array["strip_prime"] = full_data_array["strip"]*full_data_array["doubletZ"]+off_set[full_data_array["station_eta"]]
    print(off_set)
    print(off_set_1)
    
    full_data_array["strip_prime"] = [full_data_array["strip"][i] +  off_set_1[full_data_array["station_eta"][i]]*(full_data_array["doubletZ"][i]-1)+off_set[full_data_array["station_eta"][i]-1] for i in range(len(full_data_array["station_eta"])) ]
    
    
    print(type(full_data_array["strip_prime"]))
    
    for idx in range(len(full_data_array["strip_prime"])):
        offset = 0
    #    print(full_data_array["strip"][idx],full_data_array["doubletZ"][idx],full_data_array["strip"][idx] +  (off_set[full_data_array["station_eta"][idx]]-off_set[full_data_array["station_eta"][idx]-1])/2.*(full_data_array["doubletZ"][idx]-1))
        if full_data_array["station_eta"][idx] == 1: offset = off_set[-1] 
        h_layers[ly].Fill(full_data_array["strip_prime"][idx]+offset,full_data_array["x"][idx])
        h_layers_512[ly].Fill(full_data_array["strip_prime"][idx]+offset,full_data_array["x"][idx])

File_trig = ROOT.TFile("map_strip.root","recreate")

for ly in range(10):
 h_layers[ly].Write()
 h_layers_512[ly].Write()
 binx = h_layers_512[ly].GetXaxis().GetNbins()
 biny = h_layers_512[ly].GetYaxis().GetNbins()
 print("=============================================",ly,binx,biny)
 num_tot = 0
 num_list = []
 for nx in range(binx): 
    for ny in range(biny): 
        content = h_layers_512[ly].GetBinContent(nx,ny)
        geta = h_layers_512[ly].GetYaxis().GetBinCenter(ny)
        if content > 0 and geta > 0:
            if ly == 3 or ly == 5 or ly == 7:
                print("file_eta_hits("+str(ny-512)+") := file_eta_hits_input("+str(num_tot)+");")
#                print(" file_eta_hits("+str(ny-512)+") := '1';")
#                print(ny-512,",",end = '')
                #print(geta,",",end = '')
                num_list.append(ny-512)
                num_tot = num_tot + 1
 if ly == 3 or ly == 5 or ly == 7:
   for i in range(512):
     if i not in num_list: 
                print("file_eta_hits("+str(i)+") := '0';")

 print("tot",num_tot )
 print("tot",num_list)
                #print("strip ", nx," binE  ",ny, " eta ", h_layers_512[ly].GetYaxis().GetBinCenter(ny))

File_trig.Close()





