import ROOT
import numpy as np
import math

trigger = []
for i in range(4,5):
 trig_lay = []
 for j in range(9):
  trig  = ROOT.TH2F("trig"+str(i)+str(j),"trig"+str(i)+str(j),  21,0,21,50,0.0,0.1)
  trig_lay.append(trig)
 trigger.append(trig_lay)

NTOWERS = 21
ETARANGE = 2.2
TOWERSIZE    =  ETARANGE / NTOWERS 
ETAHALFRANGE =  ETARANGE 

f = ROOT.TFile.Open("out.root")
tree = f.Get('tree')
evn = - 1
for event in f.tree:
        if evn > 18000: break       
        evn =  evn + 1
        ncl = len(event.x)
        x = event.x 
        mPhi = event.mPhi 
        time = event.time
        layer = event.layer 
        g_match = event.g_match
        station_eta = event.station_eta
        station_phi = event.station_phi
        station_name = event.station_name
        trk_pt = event.trk_pt
        trk_eta = event.trk_eta
        trk_phi = event.trk_phi
        g_eta = event.g_eta
        g_phi = event.g_phi
        #For now use single muon event
        if len(trk_pt) != 1: continue
        if trk_pt[0] < 18 or  trk_pt[0] > 22: continue
        in_lay = []
        for indx,ge in enumerate(g_eta):

         xi =x[indx] 
         if mPhi[indx] == 1: continue
         if station_name[indx] != "L": continue
         if layer[indx] == 3:
          x_min = -1.1
          x_max = 1.1
          #bin_x = (x_max-x_min) / 1013.
          bin_x = (x_max-x_min) / (512.*2.)
          x_int = int( (xi-x_min) / bin_x)

          in_lay.append(xi)        
        for indx,ge in enumerate(g_eta):
         xl =x[indx] 
         if mPhi[indx] == 1: continue
         if station_name[indx] != "L": continue
         if layer[indx] != 3:
  
          laye = layer[indx]    
          for iL in in_lay:
           
           tower = int(( ((-1.1+iL) + ETAHALFRANGE) / TOWERSIZE) )
           print(iL,tower,laye,iL-xl)
           trigger[0][laye].Fill(tower,math.fabs(iL-xl)) 





File_trig = ROOT.TFile("Trig_2D.root","recreate")  


for trin in trigger:
 for r in trin:
  r.Write()

File_trig.Close()
