import argparse
import os
import ROOT
import trigger
import numpy as np

f = ROOT.TFile.Open("out.root")
tree = f.Get('tree')



#trig_pass_201  = ROOT.TH2F("trig_pass_201","trig_pass_201",  50,-1.4,1.4,50,-3.14,3.14)
#trig_total_201 = ROOT.TH2F("trig_total_201","trig_total_201",50,-1.4,1.4,50,-3.14,3.14)
#
#trig_pass_201_pt   = ROOT.TH2F("trig_pass_201_pt","trig_pass_201_pt",20,0,80)
#trig_total_201_pt  = ROOT.TH2F("trig_total_201_pt","trig_total_201_pt",20,0,80)
#
#
#trig_pass_221  = ROOT.TH2F("trig_pass_221","trig_pass_221",  50,-1.4,1.4,50,-3.14,3.14)
#trig_total_221 = ROOT.TH2F("trig_total_221","trig_total_221",50,-1.4,1.4,50,-3.14,3.14)
#
#trig_pass_221_pt   = ROOT.TH2F("trig_pass_221_pt","trig_pass_221_pt",20,0,80)
#trig_total_221_pt  = ROOT.TH2F("trig_total_221_pt","trig_total_221_pt",20,0,80)
#
#
#trig_pass_bm  = ROOT.TH2F("trig_pass_bm","trig_pass_bm",  100,-1.4,1.4,100,-3.14,3.14)
#trig_total_bm = ROOT.TH2F("trig_total_bm","trig_total_bm",100,-1.4,1.4,100,-3.14,3.14)
#
#
#trig_pass_bo  = ROOT.TH2F("trig_pass_bo","trig_pass_bo",  100,-1.4,1.4,100,-3.14,3.14)
#trig_total_bo = ROOT.TH2F("trig_total_bo","trig_total_bo",100,-1.4,1.4,100,-3.14,3.14)
#

test_file = open("testfile.txt","w")

i = -1
evn = 0
sel_evn = 0
number_of_tracks = 0
for event in f.tree:
        ncl = len(event.x)
        x = event.x 
        mPhi = event.mPhi 
        time = event.time
        layer = event.layer 
        g_match = event.g_match
        station_eta = event.station_eta
        station_phi = event.station_phi
        station_name = event.station_name
        trk_pt = event.trk_pt
        trk_eta = event.trk_eta
        trk_phi = event.trk_phi
        g_eta = event.g_eta
        g_phi = event.g_phi
        if len(trk_pt) !=1: 
         number_of_tracks = number_of_tracks + 1
         continue

#        if trk_pt[0] > 10.19 or trk_pt[0] < 10.18: continue 


        ind1 = np.asarray(mPhi) == 0
        ind = ind1
        #if True in ind2: continue
        #ind = ind1*ind2_R*ind2_M*ind2_F*ind2_G 
        #do not consider small sector
        ncell = 0
        #rude way to remove the Small sector
#        for i in ind:
#         if i == True: ncell = ncell + 1
#        if ncell < 3: continue

        #ind = np.multiply(ind1,ind2) 
        ind = np.asarray(station_name) == "L" 
        ind2 = np.asarray(x) > 0 
        xt = np.asarray(x)
        lay = np.asarray(layer)
        sta = np.asarray(station_name)
        ind = ind * ind1 *ind2
        x4Trig = xt[ind]
        ly4Trig = lay[ind]
        sta4Trig = sta[ind]
        if len(x4Trig) < 1: continue

        isTrig_2_0_1,isTrig_2_2_1,isTrig_3_3_3 = trigger.PivotTrigger(x4Trig ,ly4Trig,sta4Trig)



        evn = evn + 1
        is4Save = 0
        for clu in range(ncl):

         

         if mPhi[clu] == 0:
          #if station_name[clu] == "L":
          #if x[clu] >0 and station_phi[clu] == 3 and station_name[clu] == "L":
          if x[clu] >0  and station_name[clu] == "L":
          #if station_eta[clu] == 3:

           NTOWERS = 21
           ETARANGE = 2.2
           TOWERSIZE    = 2 * ETARANGE / NTOWERS
           ETAHALFRANGE =  ETARANGE / 2.
        


           x_min = -1.4
           x_max = 1.4
           bin_x = (x_max-x_min) / (512.*2.)



           #bin_x = (x_max-x_min) / 1013.
           #bin_x = (x_max-x_min) / (512.)
           x_int = int( (x[clu]-x_min) / bin_x)
           x_int = x_int - 512



           tower = int((2.0 * ((-1.1+float(x_int+512)/512.) + ETAHALFRANGE) / TOWERSIZE) - 0.5)
           #if( g_match[clu] > 0  ):
           #if( g_match[clu] > 0 and station_name[clu] == "L" ):
           print(x_int,x[clu],station_name[clu],mPhi[clu],time[clu],layer[clu],g_match[clu],int(time[clu]/25.))
           test_file.write("\n"+str(x_int)+" "+str(mPhi[clu])+" "+str(x[clu])+" "+str(layer[clu])+" "+str(g_match[clu])+" "+str( 0 )+" "+str(tower)+" "+str(isTrig_2_0_1) ) 
           is4Save = 1
           if x_int< 21 or x_int>491: isTrig_2_0_1 = 0

        if is4Save == 1: test_file.write("\n"+str(0)+" "+str(mPhi[clu])+" "+str(time[clu])+" "+str(layer[clu])+" "+str(g_match[clu])+" "+str( trk_pt[0] )+" "+str(tower)+ " "+ str(isTrig_2_0_1)) 
        #if is4Save == 1: test_file.write("\n"+str(x_int)+" "+str(mPhi[clu])+" "+str(time[clu])+" "+str(layer[clu])+" "+str(g_match[clu])+" "+str( trk_pt[0] )+" "+str(tower)+ " "+ str(isTrig_3_3_3)) 
        if is4Save == 1 : sel_evn = sel_evn + 1
        #if is4Save == 1 and trk_pt[0] != -1: sel_evn = sel_evn + 1
test_file.close()
print("Selcted event = ",sel_evn, float(sel_evn)/float(evn)) 

File_trig = ROOT.TFile("Trig_results.root","recreate")
File_trig.cd()
#trig_pass_221.Divide(trig_total_221)
#trig_pass_221.Write()
#trig_total_221.Write()
#
#trig_pass_201.Divide(trig_total_201)
#trig_pass_201.Write()
#
#
File_trig.Close()
