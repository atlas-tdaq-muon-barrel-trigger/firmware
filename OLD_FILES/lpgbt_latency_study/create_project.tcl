
create_project lpgbt_latency_study lpgbt_latency_study -part xcvu13p-flga2577-1-e
set_property target_language VHDL [current_project]

add_files -scan_for_includes {
    sources/RTL/top.vhd
    sources/RTL/LPGBT-FPGA_logic/lpgbtfpga_package.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/fec_rsDecoderN15K13.vhd
    sources/RTL/LPGBT-FPGA_logic/downlink/lpgbtfpga_scrambler.vhd
    sources/RTL/LPGBT-FPGA_logic/lpgbtfpga_uplink.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/lpgbtfpga_deinterleaver.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/fec_rsDecoderN31K29.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/descrambler_51bitOrder49.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/descrambler_58bitOrder58.vhd
    sources/RTL/LPGBT-FPGA_logic/downlink/rs_encoder_N7K5.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/lpgbtfpga_rxgearbox.vhd
    sources/RTL/LPGBT-FPGA_logic/lpgbtfpga_downlink.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/lpgbtfpga_decoder.vhd
    sources/RTL/LPGBT-FPGA_logic/downlink/lpgbtfpga_interleaver.vhd
    sources/RTL/LPGBT-FPGA_logic/downlink/lpgbtfpga_encoder.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/lpgbtfpga_descrambler.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/descrambler_60bitOrder58.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/descrambler_53bitOrder49.vhd
    sources/RTL/LPGBT-FPGA_logic/downlink/lpgbtfpga_txgearbox.vhd
    sources/RTL/LPGBT-FPGA_logic/uplink/lpgbtfpga_framealigner.vhd
    sources/IP/GTY_X1Y0/GTY_X1Y0.xci
    sources/RTL/GTY_X1Y0_example_gtwiz_userclk_rx.v
    sources/RTL/GTY_X1Y0_example_gtwiz_userclk_tx.v
    sources/RTL/GTY_X1Y0_example_wrapper_functions.v
    sources/RTL/GTY_X1Y0_example_wrapper.v
    sources/IP/clk_multiplier/clk_multiplier.xci
    }


add_files -fileset constrs_1 -norecurse {
    sources/constraints/constraints.xdc
    sources/constraints/lpgbt_constraints.xdc}

add_files -fileset sim_1 -norecurse {
    sources/SIM/testbench.vhd
    sources/SIM/lpgbt_emulator/lpgbt_emul_gt_exdes.vhd
    sources/SIM/lpgbt_emulator/lpgbt-emul-master/
    sources/IP/GTY_X0Y0/GTY_X0Y0.xci
    sources/RTL/GTY_X0Y0_example_gtwiz_userclk_rx.v
    sources/RTL/GTY_X0Y0_example_gtwiz_userclk_tx.v
    sources/RTL/GTY_X0Y0_example_wrapper_functions.v
    sources/RTL/GTY_X0Y0_example_wrapper.v
    }



generate_target all [get_files sources/IP/clk_multiplier/clk_multiplier.xci]
generate_target all [get_files sources/IP/GTY_X1Y0/GTY_X1Y0.xci]
generate_target all [get_files sources/IP/GTY_X0Y0/GTY_X0Y0.xci]


update_compile_order -fileset sources_1

set_property strategy Flow_PerfThresholdCarry [get_runs synth_1]
set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

update_compile_order -fileset sim_1
