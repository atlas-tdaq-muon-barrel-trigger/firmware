-------------------------------------------------------
-- File:          top.vhd
-- Project:       lpgbt latency study
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2024/06/16
-------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity top is
    Port (
        base_clock_40_p     : in std_logic;
        base_clock_40_n     : in std_logic;
        mgtrefclk0_x1y1_p   : in std_logic;
        mgtrefclk0_x1y1_n   : in std_logic;
        
        gtyrxn_in           : in std_logic;
        gtyrxp_in           : in std_logic;
        gtytxn_out          : out std_logic;
        gtytxp_out          : out std_logic;
  
        user_data_out       : out std_logic_vector(229 downto 0);
        
        reset               : in std_logic
    );
end top;

architecture Rtl of top is

    component clk_multiplier
    Port (clk_out_40MHz  : out std_logic;
          clk_out_320MHz : out std_logic;
          clk_out_80MHz  : out std_logic;
          clk_out_240MHz : out std_logic;
          locked         : out std_logic;
          clk_in1_p      : in std_logic;
          clk_in1_n      : in std_logic );
    end component; 
    
    COMPONENT lpgbtfpga_downlink IS
   GENERIC(
        -- Expert parameters
        c_multicyleDelay              : integer RANGE 0 to 7 := 1; --3                      --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                  : integer := 2;              --8                      --! Clock ratio is clock_out / 40 (shall be an integer - E.g.: 320/40 = 8)
        c_outputWidth                 : integer                                             --! Transceiver's word size (Typically 32 bits)
   );
   PORT (
        -- Clocks
        clk_i                         : in  std_logic;                                      --! Downlink datapath clock (Transceiver Tx User clock, typically 320MHz)
        clkEn_i                       : in  std_logic;                                      --! Clock enable (1 pulse over 8 clock cycles when encoding runs @ 320Mhz)
        rst_n_i                       : in  std_logic;                                      --! Downlink reset SIGNAL (Tx ready from the transceiver)

        -- Down link
        userData_i                    : in  std_logic_vector(31 downto 0);                  --! Downlink data (User)
        ECData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink EC field
        ICData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink IC field

        -- Output
        mgt_word_o                    : out std_logic_vector((c_outputWidth-1) downto 0);   --! Downlink encoded frame (IC + EC + User Data + FEC)

        -- Configuration
        interleaverBypass_i           : in  std_logic;                                      --! Bypass downlink interleaver (test purpose only)
        encoderBypass_i               : in  std_logic;                                      --! Bypass downlink FEC (test purpose only)
        scramblerBypass_i             : in  std_logic;                                      --! Bypass downlink scrambler (test purpose only)

        -- Status
        rdy_o                         : out std_logic                                       --! Downlink ready status
   );
  END COMPONENT;  

  COMPONENT lpgbtfpga_uplink IS
   GENERIC(
        -- General configuration
        DATARATE                        : integer RANGE 0 to 2;                               --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             : integer RANGE 0 to 2;                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                : integer RANGE 0 to 7 := 3;                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    : integer;                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  : integer;                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            : integer;                                            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       : integer;                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            : integer;                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                : integer := 1;                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               : integer := 40                                       --! Number of clock cycle required before being back in a stable state
   );
   PORT (
        -- Clock and reset
        uplinkClk_i                     : in  std_logic;                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                : out std_logic;                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   : in  std_logic;                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      : in  std_logic_vector((c_mgtWordWidth-1) downto 0);  --! Input frame coming from the MGT

        -- Data
        userData_o                      : out std_logic_vector(229 downto 0);                 --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        : out std_logic_vector(1 downto 0);                   --! EC field value received from the LpGBT
        IcData_o                        : out std_logic_vector(1 downto 0);                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             : in  std_logic;                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              : in  std_logic;                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               : in  std_logic;                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               : out std_logic;                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 : out std_logic_vector(229 downto 0);                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           : out std_logic;                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              : out std_logic                                       --! Number of bit slip is even (required only for advanced applications)

   );
END COMPONENT;
    
    
    signal clock_320_int : std_logic;
    signal clock_40_int  : std_logic;
    signal clock_80_int  : std_logic;
    signal clock_240_int : std_logic;
    signal reset_int     : std_logic;
    
    -- GTY signals
    signal gtwiz_userclk_tx_reset_int     : std_logic;
    signal gtwiz_userclk_tx_active_int    : std_logic;
    signal gtwiz_userclk_rx_reset_int     : std_logic;
    signal gtwiz_reset_rx_done_int        : std_logic;
    
    signal reset_gt_int                   : std_logic;
    signal mgtrefclk0_x1y1_int            : std_logic;
      
    signal gtwiz_userdata_tx_int          : std_logic_vector (31 downto 0);
    signal gtwiz_userdata_rx_int          : std_logic_vector (31 downto 0);
    
    --signal gtrefclk00_int                 : std_logic;
    signal qpll0outclk_int                : std_logic;
    signal qpll0outrefclk_int             : std_logic;
 
    signal rxslide_int                    : std_logic;
    signal gtpowergood_int                : std_logic;
    signal rxpmaresetdone_int             : std_logic;  
    signal txpmaresetdone_int             : std_logic;
 
    
    --signals for downlink logic
    signal downlink_clock_en_gen     : std_logic;
    signal downlink_clock_en         : std_logic;
    signal downlink_user_data        : std_logic_vector(31 downto 0);
    signal downlink_ec_data          : std_logic_vector(1 downto 0);
    signal downlink_ic_data          : std_logic_vector(1 downto 0);
    signal downlink_ready            : std_logic;
    
    --signals for uplink logic
    signal uplink_user_data          : std_logic_vector(229 downto 0);
    signal uplink_ic_data            : std_logic_vector(1 downto 0);
    signal uplink_ec_data            : std_logic_vector(1 downto 0); 
    signal uplink_dataCorrected      : std_logic_vector(229 downto 0);
    signal uplink_IcCorrected        : std_logic_vector(1 downto 0);
    signal uplink_EcCorrected        : std_logic_vector(1 downto 0); 
    signal uplink_rdy_i              : std_logic;
    signal uplink_clk_en_i           : std_logic;
  
    
    
begin
 
   buf_reset : IBUF
   port map (
       I => reset,
       O => reset_int
   );
   
   
   clk_multiplier_inst : clk_multiplier
   port map (
      clk_out_40MHz  => clock_40_int,
      clk_out_320MHz => clock_320_int,
      clk_out_80MHz  => clock_80_int,
      clk_out_240MHz => clock_240_int,
      locked         => open,
      clk_in1_p      => base_clock_40_p,
      clk_in1_n      => base_clock_40_n
   );
   
   IBUFDS_GTE4_MGTREFCLK0_X1Y1_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x1y1_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x1y1_p,         
      IB => mgtrefclk0_x1y1_n        
    );
    
    gtwiz_userclk_tx_reset_int <= not txpmaresetdone_int;
    gtwiz_userclk_rx_reset_int <= not rxpmaresetdone_int;
    
    
    reset_gt_int <= '0';
   
 GTY_X1Y0_wrapper_inst: entity work.GTY_X1Y0_example_wrapper
 port map
 (
   gtyrxn_in                               => gtyrxn_in,
   gtyrxp_in                               => gtyrxp_in,
   gtytxn_out                              => gtytxn_out,
   gtytxp_out                              => gtytxp_out,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int,
   gtwiz_userclk_tx_srcclk_out             => open, 
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => gtwiz_userclk_tx_active_int,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int,
   gtwiz_userclk_rx_srcclk_out             => open, 
   gtwiz_userclk_rx_usrclk_out             => open, 
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open, 
   gtwiz_reset_clk_freerun_in              => clock_80_int,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open, 
   gtwiz_reset_tx_done_out                 => open, 
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int,
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int,
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int,
   gtrefclk00_in                           => mgtrefclk0_x1y1_int,
   qpll0outclk_out                         => qpll0outclk_int,
   qpll0outrefclk_out                      => qpll0outrefclk_int,
   rxslide_in                              => rxslide_int,
   gtpowergood_out                         => gtpowergood_int,
   rxpmaresetdone_out                      => rxpmaresetdone_int,
   txpmaresetdone_out                      => txpmaresetdone_int,
   clock_320_in                            => clock_320_int,
   clock_80_in                             => clock_80_int
);
 
 
 uplink_inst : lpgbtfpga_uplink 
   generic map(
        -- General configuration
        DATARATE                        => 2,            --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             => 1,            --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                => 3,            --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    => 8,            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  => 32,           --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            => 5,            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       => 64,           --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            => 30,           --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                => 1,            --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               => 40            --! Number of clock cycle required before being back in a stable state
   )
   port map (
        -- Clock and reset
        uplinkClk_i                     => clock_320_int,                                    --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                => uplink_clk_en_i,                         --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   => gtwiz_reset_rx_done_int,                     --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      => gtwiz_userdata_rx_int,  --! Input frame coming from the MGT

        -- Data
        userData_o                      => uplink_user_data,      --! User output (decoded data). The payload size varies depending on the
                                                                          --! datarate/FEC configuration:
                                                                          --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                          --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                          --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                          --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        => uplink_ec_data,        --! EC field value received from the LpGBT
        IcData_o                        => uplink_ic_data,        --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             => '0',                           --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              => '0',                           --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               => '0',                           --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               => rxslide_int,            --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 => uplink_dataCorrected,  --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   => uplink_IcCorrected,    --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   => uplink_EcCorrected,    --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           => uplink_rdy_i,          --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              => open                           --! Number of bit slip is even (required only for advanced applications)
   );
   
   
   user_data_out <= uplink_user_data;



downlink_inst : lpgbtfpga_downlink
    generic map (
        c_multicyleDelay => 1,
        c_clockRatio     => 2,
        c_outputWidth    => 32
    )
    port map (
        clk_i                 => clock_80_int,    
        clkEn_i               => downlink_clock_en,
        rst_n_i               => txpmaresetdone_int, -- active low reset
        userData_i            => downlink_user_data,
        ECData_i              => downlink_ec_data,
        ICData_i              => downlink_ic_data,
        mgt_word_o            => gtwiz_userdata_tx_int,
        interleaverBypass_i   => '0',
        encoderBypass_i       => '0',
        scramblerBypass_i     => '0',
        rdy_o                 => downlink_ready
    );
    
    
    downlink_data_gen : process(clock_80_int)    
    begin
        if rising_edge(clock_80_int) then          
            if gtwiz_userclk_tx_active_int = '0' then
                downlink_user_data <= (others => '0');
                downlink_ec_data   <= (others => '0');
                downlink_ic_data   <= (others => '0');
                downlink_clock_en  <= '0';
                downlink_clock_en_gen <= '0';
            else
                    downlink_clock_en_gen <= not downlink_clock_en_gen;
                    if downlink_clock_en_gen = '0' then
                        downlink_user_data <= downlink_user_data + 1;
                        downlink_ec_data   <= downlink_ec_data + 1;
                        downlink_ic_data   <= downlink_ic_data + 1;
                    end if;
            end if;
        end if;
    end process;
    
    end Rtl;