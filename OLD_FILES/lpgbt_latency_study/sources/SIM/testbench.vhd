-------------------------------------------------------        
-- File:          testbench.vhd                                   
-- Project:       lpgbt_latency_study                           
-- Author:        Federico Morodei <federico.morodei@cern.ch>  
-- Last modified: 2024/06/17     
-- Description:   testbench                               
-------------------------------------------------------        

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb is

end tb;

architecture Behavioral of tb is


component clk_multiplier
    Port (clk_out_40MHz  : out std_logic;
          clk_out_320MHz : out std_logic;
          clk_out_80MHz  : out std_logic;
          clk_out_240MHz : out std_logic;
          locked         : out std_logic;
          clk_in1_p      : in std_logic;
          clk_in1_n      : in std_logic );
    end component; 

constant clk_40_period  : time := 25.0 ns;

signal clk_40_n  : std_logic;
signal clk_40_p  : std_logic;
signal clk_320 : std_logic;
signal clk_80  : std_logic;
signal clk_320_p : std_logic;
signal clk_320_n : std_logic;

signal lpgbt_rxn   : std_logic;
signal lpgbt_rxp   : std_logic;
signal lpgbt_txn   : std_logic;
signal lpgbt_txp   : std_logic;
signal SL_rxn      : std_logic;
signal SL_rxp      : std_logic;
signal SL_txn      : std_logic;
signal SL_txp      : std_logic;
signal reset       : std_logic;

signal SL_user_data   : std_logic_vector(229 downto 0);


begin

process
begin
    clk_40_n <= '1';
    wait for clk_40_period/2;
    clk_40_n <= '0';
    wait for clk_40_period/2;
end process;

clk_40_p <= not clk_40_n;

clk_multiplier_inst : clk_multiplier
   port map (
      clk_out_40MHz  => open,
      clk_out_320MHz => clk_320,
      clk_out_80MHz  => clk_80,
      clk_out_240MHz => open,
      locked         => open,
      clk_in1_p      => clk_40_p,
      clk_in1_n      => clk_40_n
   );
   
 clk_320_p <= clk_320;
 clk_320_n <= not clk_320;
   
process
begin
    reset <= '1';
    wait for 500 ns;
    reset <= '0';
    wait;
end process;



-- connect lpGBT TX to SL RX and SL TX to lpGBT RX
SL_rxn    <= lpgbt_txn;
SL_rxp    <= lpgbt_txp;
lpgbt_rxn <= SL_txn;
lpgbt_rxp <= SL_txp;


lpgbt_emul_exdes_inst : entity work.lpgbt_emul_gt_exdes
port map (
    RXN_IN                     => lpgbt_rxn,
    RXP_IN                     => lpgbt_rxp,
    TXN_OUT                    => lpgbt_txn,
    TXP_OUT                    => lpgbt_txp,
    clk_80                     => clk_80,
    clk_320                    => clk_320,
    reset                      => reset
);


top_inst : entity work.top
port map (
     base_clock_40_p     => clk_40_p,
     base_clock_40_n     => clk_40_n,
     mgtrefclk0_x1y1_p   => clk_320_p,
     mgtrefclk0_x1y1_n   => clk_320_n,     
     gtyrxn_in           => SL_rxn,
     gtyrxp_in           => SL_rxp,
     gtytxn_out          => SL_txn,
     gtytxp_out          => SL_txp,

     user_data_out       => SL_user_data,
     
     reset               => reset

);
 

end Behavioral;
