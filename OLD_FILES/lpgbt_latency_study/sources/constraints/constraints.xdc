create_clock -period 3.125 -name clk_mgtrefclk0_x1y1_p [get_ports mgtrefclk0_x1y1_p]
create_clock -period 25.000 -name base_clock_40_p [get_ports base_clock_40_p]

set_property package_pin BB12 [get_ports mgtrefclk0_x1y1_n_SLR0]
set_property package_pin BB13 [get_ports mgtrefclk0_x1y1_p_SLR0]

set_property package_pin M21 [get_ports base_clock_40_n]
set_property package_pin M22 [get_ports base_clock_40_p]ß