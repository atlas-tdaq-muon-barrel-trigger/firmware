library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;




package pack is

subtype integer_fixRange is integer range 0 to 511;
subtype integer_fixWindow is integer range 0 to 63;
subtype integer_fixRangePrio is integer range 0 to 6;
-- 2 values in case of asymmetric windows
type integer_array is array (1 downto 0) of integer_fixRange;
-- 21 towers
type integer_array2d is array (20 downto 0) of integer_array;
--5 pT values
type integer_array3d is array (3 downto 0) of integer_array2d;

type tower_candidate is array (0 to 7) of integer_fixRange;

type t_p_clos_vector is array (0 to 3) of integer_fixRange;
type t_vector_prio is array (0 to 4) of integer_fixRangePrio;


type mapphi is array (7 downto 0) of integer_fixRange;
type maptower is array (511 downto 0) of integer_fixRange;
type maptower_extended is array (531 downto 0) of integer_fixRange;
function trigger_decluster_7_global (in5,in4,in3,in2,in1,in0 : std_logic_vector(6 downto 0) ) return std_logic;
function select_candidate_w50 (signal list_candidates : in std_logic_vector(63 downto 0)) return integer_fixRange;
 
function trigger_decluster_7 (in0, in1, in2, in3, in4, in5, in6 : std_logic) return std_logic;
procedure priority_encoder(signal tower_cand_20: in tower_candidate; signal tower_cand_15: in tower_candidate;
                           signal tower_cand_10: in tower_candidate; signal tower_cand_5: in tower_candidate; 
                           signal o_tower_cand_20: out tower_candidate; signal o_tower_cand_15: out tower_candidate;
                           signal o_tower_cand_10: out tower_candidate; signal o_tower_cand_5: out tower_candidate; 
                           signal candidate: out integer_fixRange; signal pT_idx: out integer_fixRangePrio);
                                                      
procedure coin_type (constant RPC0: in std_logic;constant RPC1: in std_logic;constant RPC2: in std_logic;constant RPC3: in std_logic; signal prior : out integer_fixRange);


procedure closest_hit (constant window : in integer;
                       constant pivot_index : in integer;
                       signal list_hit : in std_logic_vector;
                       signal clos_index : out integer_fixRange);
                                     
function closest_hit_fix_range ( list_hit : in std_logic_vector(64 downto 0)) return integer_fixRange;
function closest_hit_fix_range_half ( list_hit : in std_logic_vector(64 downto 0)) return integer_fixRange;

                       

procedure fill_trig_candidate (constant potential_candidates: in std_logic_vector; signal candidates : inout std_logic_vector; signal candidate_num : inout integer);


constant map_phi_BM : mapphi := (0,0,1,2,3,4,5,5);
constant map_phi_BO : mapphi := (0,0,1,2,3,4,5,6);

--procedure PARITY
--   (signal X : in std_logic_vector;
--    signal Y : out std_logic);

constant trigger_map : maptower := (
20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

constant trigger_map_phi : maptower_extended := (
0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7);                      


--type integer_array is array (natural range<>) of integer;
--type integer_array2d is array (natural range<>) of integer_array;

constant trigger_patterns_phi_9l : integer_array2d := (
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);
constant trigger_patterns_phi_8l : integer_array2d := (
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);

constant trigger_patterns_phi_7l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);
constant trigger_patterns_phi_6l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);
constant trigger_patterns_phi_5l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);
constant trigger_patterns_phi_4l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);


constant trigger_patterns_phi_3l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5)
);

constant trigger_patterns_phi_2l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5)
);
constant trigger_patterns_phi_1l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5)
);


constant trigger_patterns_1l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
);
constant trigger_patterns_2l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
);
constant trigger_patterns_3l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
);
constant trigger_patterns_4l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);
constant trigger_patterns_5l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);
constant trigger_patterns_6l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
);
constant trigger_patterns_7l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
);
constant trigger_patterns_8l : integer_array2d := (
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
);


constant trigger_patterns_1l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),
--(
--(23,23),
--(22,22),
--(19,19),
--(18,18),
--(16,16),
--(15,15),
--(14,14),
--(13,13),
--(12,12),
--(13,13),
--(13,13),
--(12,12),
--(12,12),
--(13,13),
--(14,14),
--(15,15),
--(16,16),
--(18,18),
--(19,19),
--(22,22),
--(22,22)
-- ),
-- pT 10
(
(12,12),
(11,11),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(7,7),
(7,7),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12)
 ),
-- pT 15
(
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(5,5),
(5,5),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8)
 ),
-- pT 20
(
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
 )
);
constant trigger_patterns_2l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),

--(
--(23,23),
--(22,22),
--(19,19),
--(18,18),
--(16,16),
--(15,15),
--(14,14),
--(13,13),
--(12,12),
--(13,13),
--(13,13),
--(12,12),
--(12,12),
--(13,13),
--(14,14),
--(15,15),
--(16,16),
--(18,18),
--(19,19),
--(22,22),
--(22,22)
-- ),
-- pT 10
(
(12,12),
(11,11),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(7,7),
(7,7),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12)
 ),
-- pT 15
(
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(5,5),
(5,5),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8)
 ),
-- pT 20
(
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
 )
);
constant trigger_patterns_3l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),

--(
--(23,23),
--(22,22),
--(19,19),
--(18,18),
--(16,16),
--(15,15),
--(14,14),
--(13,13),
--(12,12),
--(13,13),
--(13,13),
--(12,12),
--(12,12),
--(13,13),
--(14,14),
--(15,15),
--(16,16),
--(18,18),
--(19,19),
--(22,22),
--(22,22)
-- ),
-- pT 10
(
(12,12),
(11,11),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(7,7),
(7,7),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12)
 ),
-- pT 15
(
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(5,5),
(5,5),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8)
 ),
-- pT 20
(
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
 )
);
constant trigger_patterns_4l_3d : integer_array3d := ( 
-- pT 5
(
(10,10),
(10,10),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(7,7),
(7,7),
(8,8),
(10,10),
(9,9)
 ),
-- pT 10
(
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5)
 ),
-- pT 15
(
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
 )
);
constant trigger_patterns_5l_3d : integer_array3d := (
-- pT 5
(
(10,10),
(10,10),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(7,7),
(7,7),
(8,8),
(10,10),
(9,9)
 ),
-- pT 10
(
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5)
 ),
-- pT 15
(
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
 )
);
constant trigger_patterns_6l_3d : integer_array3d := ( 
-- pT 5
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 10
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 15
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
 )
);
constant trigger_patterns_7l_3d : integer_array3d := (
-- pT 5
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 10
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 15
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
 )
);




constant trigger_patterns_8l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),
--(
--(33,33),
--(33,33),
--(30,30),
--(25,25),
--(23,23),
--(21,21),
--(19,19),
--(17,17),
--(17,17),
--(16,16),
--(16,16),
--(16,16),
--(17,17),
--(17,17),
--(19,19),
--(21,21),
--(23,23),
--(25,25),
--(29,29),
--(33,33),
--(32,32)
-- ),
-- pT 10
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
 ),
-- pT 15
(
(10,10),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(10,10)
 ),
-- pT 20
(
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
 )
);




signal trigger_patterns_9l : integer_array2d := (
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
);



constant trigger_patterns_9l_3d : integer_array3d :=  (
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
--(33,33),
--(33,33),
--(30,30),
--(25,25),
--(23,23),
--(21,21),
--(19,19),
--(17,17),
--(17,17),
--(16,16),
--(16,16),
--(16,16),
--(17,17),
--(17,17),
--(19,19),
--(21,21),
--(23,23),
--(25,25),
--(29,29),
--(33,33),
--(32,32)
 ),
-- pT 10
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
 ),
-- pT 15
(
(10,10),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(10,10)
 ),
-- pT 20
(
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
 ) -- close for pT loop
);





end pack;


package body pack is






-- first stage to remove redudancy -- assumere there is only one muon within +- 3 strips
function trigger_decluster_7_global (in5,in4,in3,in2,in1,in0 : std_logic_vector(6 downto 0) ) 
   return std_logic is
   variable pivot_decl : std_logic;
    begin
      -- 3 is the pivot of interest
      if(in5(3) = '1') then
        pivot_decl := '1';
      -- it is not priority 0, is there any other one available?
      elsif (in5(0) = '1' or in5(1) = '1' or in5(2) = '1' or in5(4) = '1' or in5(5) = '1' or in5(6) = '1') then
        pivot_decl := '0';
--      -- there is no priority 5 - so are we looking at a priority 4?
      elsif (in4(3) = '1') then
--        pivot_decl := '1';
      elsif (in4(0) = '1' or in4(1) = '1' or in4(2) = '1' or in4(4) = '1' or in4(5) = '1' or in4(6) = '1') then
--        pivot_decl := '0';
      elsif (in3(3) = '1') then
--        pivot_decl := '1';
      elsif (in3(0) = '1' or in3(1) = '1' or in3(2) = '1' or in3(4) = '1' or in3(5) = '1' or in3(6) = '1') then
--        pivot_decl := '0';
      elsif (in2(3) = '1') then
--        pivot_decl := '1';
      elsif (in2(0) = '1' or in2(1) = '1' or in2(2) = '1' or in2(4) = '1' or in2(5) = '1' or in2(6) = '1') then
--        pivot_decl := '0';
      elsif (in1(3) = '1') then
--        pivot_decl := '1';
      elsif (in1(0) = '1' or in1(1) = '1' or in1(2) = '1' or in1(4) = '1' or in1(5) = '1' or in1(6) = '1') then
--        pivot_decl := '0';
      elsif (in0(3) = '1') then
--        pivot_decl := '1';
      else
       pivot_decl := '0';
      end if; 

    return pivot_decl;
  end function trigger_decluster_7_global;

--function used to select the first candidate in a tower of 50 indeces

function  select_candidate_w50 (signal list_candidates :  std_logic_vector(63 downto 0))
    return integer_fixRange is
    variable i_cand : integer_fixRange;

    begin
            if (list_candidates(0)  = '1') then
       i_cand := 0;  
            elsif (list_candidates(1) = '1') then
       i_cand := 1;
            elsif (list_candidates(2) = '1') then
       i_cand := 2;
            elsif (list_candidates(3) = '1') then
       i_cand := 3;
            elsif (list_candidates(4) = '1') then
       i_cand := 4;
            elsif (list_candidates(5) = '1') then
       i_cand := 5; 
            elsif (list_candidates(6) = '1') then
       i_cand := 6; 
            elsif (list_candidates(7) = '1') then
       i_cand := 7;
            elsif (list_candidates(8) = '1') then
       i_cand := 8; 
            elsif (list_candidates(9) = '1') then
       i_cand := 9;  
            elsif (list_candidates(10) = '1') then
       i_cand := 10; 
            elsif (list_candidates(11) = '1') then
       i_cand := 11; 
            elsif (list_candidates(12) = '1') then
       i_cand := 12;  
            elsif (list_candidates(13) = '1') then
       i_cand := 13;  
            elsif (list_candidates(14) = '1') then
       i_cand := 14;  
            elsif (list_candidates(15) = '1') then
       i_cand := 15;  
            elsif (list_candidates(16) = '1') then
       i_cand := 16;  
            elsif (list_candidates(17) = '1') then
       i_cand := 17;  
            elsif (list_candidates(18) = '1') then
       i_cand := 18;  
            elsif (list_candidates(19) = '1') then
       i_cand := 19;  
            elsif (list_candidates(20) = '1') then
       i_cand := 20;  
            elsif (list_candidates(21) = '1') then
       i_cand := 21;  
            elsif (list_candidates(22) = '1') then
       i_cand := 22; 
            elsif (list_candidates(23) = '1') then
       i_cand := 23; 
            elsif (list_candidates(24) = '1') then
       i_cand := 24;  
            elsif (list_candidates(25) = '1') then
       i_cand := 25;  
            elsif (list_candidates(26) = '1') then
       i_cand := 26; 
            elsif (list_candidates(27) = '1') then
       i_cand := 27; 
            elsif (list_candidates(28) = '1') then
       i_cand := 28; 
            elsif (list_candidates(29) = '1') then
       i_cand := 29; 
            elsif (list_candidates(30) = '1') then
       i_cand := 30; 
            elsif (list_candidates(31) = '1') then
       i_cand := 31; 
            elsif (list_candidates(32) = '1') then
       i_cand := 32; 
            elsif (list_candidates(33) = '1') then
       i_cand := 33; 
            elsif (list_candidates(34) = '1') then
       i_cand := 34; 
            elsif (list_candidates(35) = '1') then
       i_cand := 35; 
            elsif (list_candidates(36) = '1') then
       i_cand := 36; 
            elsif (list_candidates(37) = '1') then
       i_cand := 37; 
            elsif (list_candidates(38) = '1') then
       i_cand := 38; 
            elsif (list_candidates(39) = '1') then
       i_cand := 39; 
            elsif (list_candidates(40) = '1') then
       i_cand := 40; 
            elsif (list_candidates(41) = '1') then
       i_cand := 41;
                   elsif (list_candidates(41) = '1') then
       i_cand := 41;
                   elsif (list_candidates(42) = '1') then
       i_cand := 42;
                   elsif (list_candidates(43) = '1') then
       i_cand := 43;
                   elsif (list_candidates(44) = '1') then
       i_cand := 44;
                   elsif (list_candidates(45) = '1') then
       i_cand := 45;
                   elsif (list_candidates(46) = '1') then
       i_cand := 46;
                  elsif (list_candidates(47) = '1') then
       i_cand := 47;
                   elsif (list_candidates(48) = '1') then
       i_cand := 48;
                   elsif (list_candidates(49) = '1') then
       i_cand := 49;
                   elsif (list_candidates(50) = '1') then
       i_cand := 50;
                 elsif (list_candidates(51) = '1') then
       i_cand := 51;  
                   elsif (list_candidates(52) = '1') then
       i_cand := 52;
                   elsif (list_candidates(53) = '1') then
       i_cand := 53;
                   elsif (list_candidates(54) = '1') then
       i_cand := 54;
                   elsif (list_candidates(55) = '1') then
       i_cand := 55;  
                   elsif (list_candidates(56) = '1') then
       i_cand := 56;
                   elsif (list_candidates(57) = '1') then
       i_cand := 57;
                   elsif (list_candidates(58) = '1') then
       i_cand := 58;
                   elsif (list_candidates(59) = '1') then
       i_cand := 59;
                   elsif (list_candidates(60) = '1') then
       i_cand := 60;
                   elsif (list_candidates(61) = '1') then
       i_cand := 61;  
                   elsif (list_candidates(62) = '1') then
       i_cand := 62;
                   elsif (list_candidates(63) = '1') then
       i_cand := 63;
       else
        i_cand := 64;
     end if;
     return i_cand;
  end function select_candidate_w50;






-- this is the lookup table for declustering
function trigger_decluster_7 (in0, in1, in2, in3, in4, in5, in6 : std_logic) 
   return std_logic is
   variable pivot_decl : std_logic;
    begin
      if ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' )  then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      else 
        pivot_decl := '0';
      end if;
      return pivot_decl;
  end function trigger_decluster_7;


--define the priority
procedure coin_type (constant RPC0: in std_logic;constant RPC1: in std_logic;constant RPC2: in std_logic;constant RPC3: in std_logic; signal prior : out integer_fixRangePrio) is
	begin 

        --RPC0-RPC1-RPC2-RPC3 
        if(RPC0='1' and RPC1='1' and RPC2='1' and RPC3='1') then
         prior <= 6;
	--RPC1-RPC2-RPC3
        elsif(RPC1='1' and RPC2='1' and RPC3='1') then
         prior <= 5;
	--RPC0-RPC2-RPC3
        elsif(RPC0='1' and RPC2='1' and RPC3='1') then
         prior <= 4;
	--RPC0-RPC1-RPC3
        elsif(RPC0='0' and RPC1='1' and RPC3='1') then
         prior <= 3;
	--RPC0-RPC1-RPC2
        elsif(RPC0='1' and RPC1='1' and RPC2='1') then
         prior <= 2;
	--RPC0-RPC3
        elsif(RPC0='1' and RPC3='1') then
         prior <= 1;
         else 
         prior <= 0;
        end if;

	--transform an integer into a std_logic_vector?
	
    end procedure coin_type;


-- algorithm used to get the closest hit. This assume that at least one hit in the layer is present
procedure closest_hit (constant window : in integer_fixRange; -- this can be reduced even
                       constant pivot_index : in integer_fixRange;
                       signal list_hit : in std_logic_vector;
                       signal clos_index : out integer_fixRange) is
          variable min :  integer_fixRange;
          begin 

          --min and max are the same
          min := pivot_index - window;
          for i in min to pivot_index loop
           --check right and left part, arbitrarily, give preference to left
           if ( list_hit(pivot_index+window-i) = '1' ) then
             clos_index <= pivot_index-i ;
           --now look from left to right
           elsif ( list_hit(i) = '1') then
             clos_index <= i;
           end if;   
          end loop;

   end procedure closest_hit;
   
   
-- algorithm used to get the closest hit. This assume that at least one hit in the layer is present
function closest_hit_fix_range_half (list_hit : std_logic_vector(64 downto 0) )
           return integer_fixRange is
           variable clos_index : integer_fixRange;
          begin    
   -- elsif (list_hit(41) = '1') then
  if (list_hit(41) = '1') then
    clos_index := 41;
 elsif (list_hit(20) = '1') then
    clos_index := 20;
 elsif (list_hit(42) = '1') then   
    clos_index := 42;
 elsif (list_hit(19) = '1') then
    clos_index := 19;
 elsif (list_hit(43) = '1') then   
    clos_index := 43;
 elsif (list_hit(18) = '1') then
    clos_index := 18;
 elsif (list_hit(44) = '1') then   
    clos_index := 44;
 elsif (list_hit(17) = '1') then
    clos_index := 17;
 elsif (list_hit(45) = '1') then   
    clos_index := 45;
 elsif (list_hit(16) = '1') then
    clos_index := 16;
 elsif (list_hit(46) = '1') then   
    clos_index := 46;
 elsif (list_hit(15) = '1') then
    clos_index := 15;
 elsif (list_hit(47) = '1') then   
    clos_index := 47;
 elsif (list_hit(14) = '1') then
    clos_index := 14;
 elsif (list_hit(48) = '1') then   
    clos_index := 48;
    
 elsif (list_hit(13) = '1') then
    clos_index := 13;
 elsif (list_hit(49) = '1') then   
    clos_index := 49;
    
 elsif (list_hit(12) = '1') then
    clos_index := 12;
 elsif (list_hit(50) = '1') then   
    clos_index := 50;
    
 elsif (list_hit(11) = '1') then
    clos_index := 11;
 elsif (list_hit(51) = '1') then   
    clos_index := 51;
 elsif (list_hit(10) = '1') then
    clos_index := 10;
    
 elsif (list_hit(52) = '1') then   
    clos_index := 52;
 elsif (list_hit(9) = '1') then
    clos_index := 9;
 elsif (list_hit(53) = '1') then   
    clos_index := 53;
  elsif (list_hit(8) = '1') then         
     clos_index := 8;                    --     
       elsif (list_hit(54) = '1') then   
    clos_index := 54;
elsif (list_hit(7) = '1') then          
   clos_index := 7;       
          
 elsif (list_hit(55) = '1') then   
    clos_index := 55;
    
 elsif (list_hit(6) = '1') then                 
   clos_index := 6;                            

       elsif (list_hit(56) = '1') then   
    clos_index := 56;

 elsif (list_hit(5) = '1') then         
    clos_index := 5;     
            
 elsif (list_hit(57) = '1') then   
    clos_index := 57;
    
    
       elsif (list_hit(58) = '1') then   
    clos_index := 58;
  elsif (list_hit(4) = '1') then         
     clos_index := 4;                    
 elsif (list_hit(59) = '1') then   
    clos_index := 59;
  elsif (list_hit(3) = '1') then                 
     clos_index := 3;                        




 elsif (list_hit(60) = '1') then   
    clos_index := 60;
 elsif (list_hit(2) = '1') then     
    clos_index := 2;   
          
   elsif (list_hit(61) = '1') then   
    clos_index := 61;

   elsif (list_hit(1) = '1') then     
   clos_index := 1;                


    
    
    elsif (list_hit(62) = '1') then   
    clos_index := 62;

   elsif (list_hit(0) = '1') then     
   clos_index := 0;                

   elsif (list_hit(63) = '1') then   
    clos_index := 63;
   else 
   clos_index := 0;
   end if;

   return clos_index;
   end function closest_hit_fix_range_half;   
   

-- algorithm used to get the closest hit. This assume that at least one hit in the layer is present
function closest_hit_fix_range (list_hit : std_logic_vector(64 downto 0) )
           return integer_fixRange is
           variable clos_index : integer_fixRange;
          begin 


   if (list_hit(31) = '1') then
      clos_index := 31; 
   elsif (list_hit(30) = '1') then
      clos_index := 30;            
   elsif (list_hit(32) = '1') then   
      clos_index := 32;  
      
   elsif (list_hit(29) = '1') then
      clos_index := 29;
   elsif (list_hit(31+2) = '1') then   
      clos_index := 33;      
      
   elsif (list_hit(28) = '1') then
      clos_index := 28;
   elsif (list_hit(34) = '1') then   
      clos_index := 34;      
      
   elsif (list_hit(27) = '1') then
      clos_index := 27;
   elsif (list_hit(35) = '1') then   
      clos_index := 35;     
      
   elsif (list_hit(26) = '1') then
      clos_index := 26;
   elsif (list_hit(36) = '1') then   
      clos_index := 36;
            
   elsif (list_hit(25) = '1') then
      clos_index := 25;
   elsif (list_hit(37) = '1') then   
      clos_index := 37;      
      
   elsif (list_hit(25) = '1') then
      clos_index := 25;
   elsif (list_hit(38) = '1') then   
      clos_index := 38;
    elsif (list_hit(24) = '1') then
      clos_index := 24;
   elsif (list_hit(39) = '1') then   
      clos_index := 39;
   elsif (list_hit(23) = '1') then
      clos_index := 23;
   elsif (list_hit(40) = '1') then   
      clos_index := 40;
   elsif (list_hit(21) = '1') then
      clos_index := 21;
      
      
      
-- elsif (list_hit(41) = '1') then   
--    clos_index := 41;
-- elsif (list_hit(20) = '1') then
--    clos_index := 20;
-- elsif (list_hit(42) = '1') then   
--    clos_index := 42;
-- elsif (list_hit(19) = '1') then
--    clos_index := 19;
-- elsif (list_hit(42) = '1') then   
--    clos_index := 42;
-- elsif (list_hit(31-14) = '1') then
--    clos_index := 18;
-- elsif (list_hit(43) = '1') then   
--    clos_index := 43;
-- elsif (list_hit(17) = '1') then
--    clos_index := 17;
-- elsif (list_hit(44) = '1') then   
--    clos_index := 44;
-- elsif (list_hit(16) = '1') then
--    clos_index := 16;
-- elsif (list_hit(45) = '1') then   
--    clos_index := 45;
-- elsif (list_hit(15) = '1') then
--    clos_index := 15;
-- elsif (list_hit(46) = '1') then   
--    clos_index := 46;
-- elsif (list_hit(14) = '1') then
--    clos_index := 14;
-- elsif (list_hit(47) = '1') then   
--    clos_index := 47;
--    
-- elsif (list_hit(13) = '1') then
--    clos_index := 13;
-- elsif (list_hit(48) = '1') then   
--    clos_index := 48;
--    
-- elsif (list_hit(12) = '1') then
--    clos_index := 12;
-- elsif (list_hit(49) = '1') then   
--    clos_index := 49;
--    
-- elsif (list_hit(11) = '1') then
--    clos_index := 11;
-- elsif (list_hit(50) = '1') then   
--    clos_index := 50;
-- elsif (list_hit(10) = '1') then
--    clos_index := 10;
--    
-- elsif (list_hit(51) = '1') then   
--    clos_index := 51;
-- elsif (list_hit(9) = '1') then
--    clos_index := 9;
-- elsif (list_hit(52) = '1') then   
--    clos_index := 52;
--  elsif (list_hit(8) = '1') then         
--     clos_index := 8;                    --     
--       elsif (list_hit(53) = '1') then   
--    clos_index := 53;
--elsif (list_hit(7) = '1') then          
--   clos_index := 7;       
--          
-- elsif (list_hit(54) = '1') then   
--    clos_index := 54;
--    
-- elsif (list_hit(6) = '1') then                 
--   clos_index := 6;                            
--
--       elsif (list_hit(55) = '1') then   
--    clos_index := 55;
--
-- elsif (list_hit(5) = '1') then         
--    clos_index := 5;     
--            
-- elsif (list_hit(56) = '1') then   
--    clos_index := 56;
--    
--    
--       elsif (list_hit(57) = '1') then   
--    clos_index := 57;
--  elsif (list_hit(4) = '1') then         
--     clos_index := 4;                    
-- elsif (list_hit(58) = '1') then   
--    clos_index := 58;
--  elsif (list_hit(3) = '1') then                 
--     clos_index := 3;                        
--
--
--
--
-- elsif (list_hit(59) = '1') then   
--    clos_index := 59;
-- elsif (list_hit(2) = '1') then     
--    clos_index := 2;   
--          
--       elsif (list_hit(60) = '1') then   
--    clos_index := 60;
--
--elsif (list_hit(1) = '1') then     
--   clos_index := 1;                
--
--
--    
--    
--       elsif (list_hit(61) = '1') then   
--    clos_index := 61;
--
--elsif (list_hit(0) = '1') then     
--   clos_index := 0;                
--
-- elsif (list_hit(62) = '1') then   
--    clos_index := 62;
--    
 
 
 
 
 --  elsif (list_hit(63) = '1') then   
 --     clos_index := 63;
 --     
 
 
 --  elsif (list_hit(1) = '1') then
 --     clos_index := 1;
 --  elsif (list_hit(0) = '1') then   
 --     clos_index := 0;
 --     
 
 
   else 
   clos_index := 0;
   end if;
   

   return clos_index;
   end function closest_hit_fix_range;

-- this shall take 2 clock cycles
procedure fill_trig_candidate (constant potential_candidates: in std_logic_vector; signal candidates : inout std_logic_vector; signal candidate_num : inout integer_fixRange) is
    begin

    -- look for more candidates here - first declaster   
    for i in 20 to 492  loop
     if (potential_candidates(i) = '1') then
       candidates(i)<= '1'; 
   --    candidate_num<= candidate_num + 1; 
     end if;
    end loop;

 end procedure fill_trig_candidate;


procedure priority_encoder(signal tower_cand_20: in tower_candidate; signal tower_cand_15: in tower_candidate;
                           signal tower_cand_10: in tower_candidate; signal tower_cand_5: in tower_candidate; 
                           signal o_tower_cand_20: out tower_candidate; signal o_tower_cand_15: out tower_candidate;
                           signal o_tower_cand_10: out tower_candidate; signal o_tower_cand_5: out tower_candidate;                           
                           signal candidate: out integer_fixRange; signal pT_idx: out integer_fixRangePrio) is
                           begin
 
                           
                           if(tower_cand_20(0) /= 64)  then
                            candidate <= tower_cand_20(0);
                            o_tower_cand_20(0) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(1) /= 64) then
                            candidate <= tower_cand_20(1);--+64;
                            o_tower_cand_20(1) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(2) /= 64) then
                            candidate <= tower_cand_20(2);--+128;
                            o_tower_cand_20(2) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(3) /= 64) then
                            candidate <= tower_cand_20(3);--+192;
                            o_tower_cand_20(3) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(4) /= 64) then
                            candidate <= tower_cand_20(4);--+256;
                            o_tower_cand_20(4) <= 64;
                            pT_idx <= 0;
                            elsif(tower_cand_20(5) /= 64) then
                            candidate <= tower_cand_20(5);--+320;
                            o_tower_cand_20(5) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(6) /= 64) then
                            candidate <= tower_cand_20(4);--+384;
                            o_tower_cand_20(6) <= 64;
                            pT_idx <= 0;
                            elsif(tower_cand_20(7) /= 64) then
                            candidate <= tower_cand_20(4);--+448;
                            o_tower_cand_20(7) <= 64;
                            pT_idx <= 0;
                      
                           elsif(tower_cand_15(0) /= 64) then
                            candidate <= tower_cand_15(0);  
                            o_tower_cand_15(0) <= 64;
                            pT_idx <= 1;                          
                           elsif(tower_cand_15(1) /= 64) then
                            candidate <= tower_cand_15(1);--+64;
                            o_tower_cand_15(1) <= 64;
                            pT_idx <= 1;                            
                            elsif(tower_cand_15(2) /= 64) then
                            candidate <= tower_cand_15(2);--+128;
                            o_tower_cand_15(2) <= 64;
                            pT_idx <= 1;
                           elsif(tower_cand_15(3) /= 64) then
                            candidate <= tower_cand_15(3);--+192;
                            o_tower_cand_15(3) <= 64;
                            pT_idx <= 1;
                            elsif(tower_cand_15(4) /= 64) then
                            candidate <= tower_cand_15(4);--+256; 
                            o_tower_cand_15(4) <= 64;
                            pT_idx <= 1; 
                            elsif(tower_cand_15(5) /= 64) then
                            candidate <= tower_cand_15(5);--+320; 
                            o_tower_cand_15(5) <= 64; 
                            pT_idx <= 1;                         
                            elsif(tower_cand_15(6) /= 64) then
                            candidate <= tower_cand_15(6);--+384;  
                            o_tower_cand_15(6) <= 64;
                            pT_idx <= 1;                         
                           elsif(tower_cand_15(7) /= 64) then
                            candidate <= tower_cand_15(7);--+448;
                            o_tower_cand_15(7) <= 64;
                            pT_idx <= 1;
                            
                            elsif(tower_cand_10(0) /= 64) then
                            candidate <= tower_cand_10(0);   
                            o_tower_cand_10(0) <= 64; 
                            pT_idx <= 2;                        
                           elsif(tower_cand_10(1) /= 64) then
                            candidate <= tower_cand_10(1);--+64; 
                            o_tower_cand_10(1) <= 64; 
                            pT_idx <= 2;                           
                            elsif(tower_cand_10(2) /= 64) then
                            candidate <= tower_cand_10(2);--+128;
                            o_tower_cand_10(2) <= 64; 
                            pT_idx <= 2;
                           elsif(tower_cand_10(3) /= 64) then
                            candidate <= tower_cand_10(3);--+192;
                            o_tower_cand_10(3) <= 64;
                            pT_idx <= 2; 
                            elsif(tower_cand_10(4) /= 64) then
                            candidate <= tower_cand_10(4);--+256; 
                            o_tower_cand_10(4) <= 64;  
                            pT_idx <= 2;
                            elsif(tower_cand_10(5) /= 64) then
                            candidate <= tower_cand_10(5);--+320; 
                            o_tower_cand_10(5) <= 64;
                            pT_idx <= 2;                           
                            elsif(tower_cand_10(6) /= 64) then
                            candidate <= tower_cand_10(6);--+384; 
                            o_tower_cand_10(6) <= 64;  
                            pT_idx <= 2;                         
                           elsif(tower_cand_10(7) /= 64) then
                            candidate <= tower_cand_10(7);--+448; 
                            o_tower_cand_10(7) <= 64;  
                            pT_idx <= 2;                         
                            
                           elsif(tower_cand_5(0) /= 64) then
                            candidate <= tower_cand_5(0);  
                            o_tower_cand_5(0) <= 64;    
                            pT_idx <= 3;                       
                           elsif(tower_cand_5(1) /= 64) then
                            candidate <= tower_cand_5(1);--+64;
                            o_tower_cand_5(1) <= 64;  
                            pT_idx <= 3;                          
                            elsif(tower_cand_5(2) /= 64) then
                            candidate <= tower_cand_5(2);--+128;
                            o_tower_cand_5(2) <= 64;
                            pT_idx <= 3;
                           elsif(tower_cand_5(3) /= 64) then
                            candidate <= tower_cand_5(3);--+192;
                            o_tower_cand_5(3) <= 64;
                            pT_idx <= 3;
                            elsif(tower_cand_5(4) /= 64) then
                            candidate <= tower_cand_5(4);--+256; 
                            o_tower_cand_5(4) <= 64; 
                            pT_idx <= 3;
                            elsif(tower_cand_5(5) /= 64) then
                            candidate <= tower_cand_5(5);--+320;   
                            o_tower_cand_5(5) <= 64;  
                            pT_idx <= 3;                      
                            elsif(tower_cand_5(6) /= 64) then
                            candidate <= tower_cand_5(6);--+384;  
                            o_tower_cand_5(6) <= 64;    
                            pT_idx <= 3;                     
                            elsif(tower_cand_5(7) /= 64) then
                            candidate <= tower_cand_5(7);--+448;
                            o_tower_cand_5(7) <= 64;
                            pT_idx <= 3;
                            else                  
 --                           tower_cand_5 <= tower_cand_5 ; -- shall be a copy of itself
                            candidate <= 0; -- need to adapt this to lower values once I enlarge the input sizes
                            pT_idx <= 4;
                                                                                                                                                                                                      
                           end if;                          
                           

 end procedure priority_encoder;



end package body;



