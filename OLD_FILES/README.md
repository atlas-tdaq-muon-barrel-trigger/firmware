**Old projects**


- `vhdl_SL_trigger`: trigger logic of the Barrel SL FPGA firmware
- `SLReadout`: readout logic of the Barrel SL FPGA firmware
- `vhdl_SL_interface`: IO interface logic of the Barrel SL FPGA firmware
- `LPGBT_FPGA_project`: Vivado project for a communication test between the CERN lpGBT Emulator and the CERN lpGBT-FPGA core using two KC705 Evaluation boards 
- `python_script_pivot_trigger`: scripts used for trigger simulations


-------------------------
Note: in this branch the SLReadout folder contains the readout implementation with RAM memories (updated w.r. to PDR version, using FIFOs).