------------------------------------------------------ 
--File:             top_tb.vhd
--Project:          BMBO DCT firmware
--Author:           Riccardo Vari, Federico Morodei
--Last modified:    2024/12/07
------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.uniform;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

library xil_defaultlib;
use xil_defaultlib.pack.all;



entity top_tb is
end top_tb;

architecture Behavioral of top_tb is

    signal clock40_p, clock40_n : std_logic;

    signal file_hits : std_logic_vector(287 downto 0) := (others => '0');
    signal file_hits_en : std_logic_vector(287 downto 0) := (others => '0');
    
    signal elink_in : std_logic_vector(27 downto 0);
    
    signal bcid : std_logic_vector(11 downto 0);

    signal clock40_edge : std_logic := '0';
    signal clock40_edge320_r : std_logic;
    signal clock40_edge200_r : std_logic;
    signal clock40_rising320 : std_logic;
    signal clock40_rising200 : std_logic;

    signal clock600_mmcm : std_logic;
    signal clock200_mmcm : std_logic;
    signal clock320_mmcm : std_logic;
    
    signal mmcm1_locked : std_logic;
    signal mmcm2_locked : std_logic;
    signal mmcm_locked_r0 : std_logic;
    signal mmcm_locked_r1 : std_logic;
    
    signal int_reset : std_logic;   
    
    signal ctrl_data    : std_logic_vector (31 downto 0);
    signal counter_32   : std_logic_vector(4 downto 0);
    signal ctrl_elink_p : std_logic_vector(3 downto 0) := (others => '0');
    signal ctrl_elink_n : std_logic_vector(3 downto 0) := (others => '1');
    signal header       : std_logic_vector(6 downto 0) := "1010101";
    signal write_cmd    : std_logic;
    signal read_cmd     : std_logic;
    signal reg_idx      : std_logic_vector(2 downto 0);
    signal channel_idx  : std_logic_vector(8 downto 0);
    signal datum        : std_logic_vector(5 downto 0);
    signal datacount    : array_18x5b;
    signal hit_bcid     : std_logic_vector(9 downto 0);
    signal BCreset      : std_logic;
    signal BCreset_word : std_logic_vector(7 downto 0);
    signal counter_8    : std_logic_vector(2 downto 0) := (others => '0');
    
    component mmcm_200_600
    port (
        clk_out600 : out std_logic;
        clk_out200 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

    component mmcm_320
    port (
        clk_out320 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

begin

--  LHC clock frequency 40.079 MHz
    clock40_gen : process
    begin
        clock40_p <= '0';
        clock40_n <= '1';
        wait for 12.48 ns;
        clock40_p <= '1';
        clock40_n <= '0';
        wait for 12.48 ns;
    end process;

    mmcm_for_deserialisers_inst : mmcm_200_600
    port map (
        clk_out600 => clock600_mmcm,
        clk_out200 => clock200_mmcm,
        locked     => mmcm1_locked,
        clk_in40   => clock40_p
    );

    mmcm_for_logic_inst : mmcm_320
    port map (
        clk_out320 => clock320_mmcm,
        locked     => mmcm2_locked,
        clk_in40   => clock40_p
    );

    int_reset_logic : process(clock40_p) begin
        if rising_edge(clock40_p) then
            mmcm_locked_r0 <= mmcm1_locked and mmcm2_locked;
            mmcm_locked_r1 <= mmcm_locked_r0;
            if mmcm_locked_r1 = '1' then
                int_reset <= '0';
            else
                int_reset <= '1';
            end if;
        end if;
    end process;

    clock40_edge_logic : process(clock40_p) begin
        if rising_edge(clock40_p) then
            clock40_edge <= not clock40_edge;
        end if;
    end process;

    clock40_rising320_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            clock40_edge320_r <= clock40_edge;
            if clock40_edge320_r /= clock40_edge then
                clock40_rising320 <= '1';
            else
                clock40_rising320 <= '0';
            end if;
        end if;
    end process;

    clock40_rising200_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            clock40_edge200_r <= clock40_edge;
            if clock40_edge200_r /= clock40_edge then
                clock40_rising200 <= '1';
            else
                clock40_rising200 <= '0';
            end if;
        end if;
    end process;


    top_inst : entity work.top
    port map (
--        CLK40_0     => clock40_p,
--        CLK40_1     => clock40_p,
--        PSCLK0_p    => clock40_p,
--        PSCLK0_n    => clock40_n,
--        PSCLK1_p    => clock40_p,
--        PSCLK1_n    => clock40_n,
        ECLK0_p     => clock40_p,
        ECLK0_n     => clock40_n,
--        ECLK1_p     => clock40_p,
--        ECLK1_n     => clock40_n,
--        TST_CLK0_P  => clock40_p,
--        TST_CLK0_N  => clock40_n,
--        TST_CLK1_P  => clock40_p,
--        TST_CLK1_N  => clock40_n,
        rpc_in      => file_hits,
        elink_in_p  => ctrl_elink_p,
        elink_in_n  => ctrl_elink_n,
        elink_out_p => elink_in,
        elink_out_n => open
        --fifo_datacount => datacount
    );
    
    bcid <= << signal .top_tb.top_inst.bcid : std_logic_vector>>;

    
    
    ---- control data, through 1 downlink elink -------
    write_cmd <= '0';
    read_cmd <= '0';
    reg_idx <= "100";
    channel_idx <= "000000000";
    datum <= "000110";
    
    ctrl_data <= header & "00000" & write_cmd & read_cmd & reg_idx & channel_idx & datum;
    
    generate_ctrl_data : process(clock320_mmcm, int_reset) begin
        if int_reset = '1' then
            counter_32 <= (others => '0');
        elsif rising_edge(clock320_mmcm) then
            counter_32 <= counter_32 + 1;
            ctrl_elink_p(0) <= ctrl_data(to_integer(unsigned(counter_32)));
            ctrl_elink_n(0) <= not ctrl_data(to_integer(unsigned(counter_32)));
        end if;
    end process;
    
    -------- BCreset, through 1 dowlink elink ----
    BCreset_process : process begin
        BCreset <= '1';
        wait for 4 us;
        BCreset <= '0';
        wait;
    end process;
    
    BCreset_word <= header & BCreset;
    
    generate_BCreset_elink : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            counter_8 <= counter_8 + 1;
            ctrl_elink_p(1) <= BCreset_word(to_integer(unsigned(counter_8)));
            ctrl_elink_n(1) <= not BCreset_word(to_integer(unsigned(counter_8)));
        end if;
    end process;
    
    -------------------------------------------
    
    
    file_hits_en_gen: process
        file simulation_hit_file: text;
        file generated_hit_file: text open write_mode is  "../../../../../Data/generated_hits_out.txt";
        variable file_status : FILE_OPEN_STATUS;
        variable file_status2 : FILE_OPEN_STATUS;
        variable hits_line : line;
        variable data_line : line;
        variable hit_time : real;
        variable eta_phi : integer;
        variable l0_l1 : integer;
        variable hit_index : integer;
        variable mu_match : integer;
        constant space : string := " ";
    begin
        file_open(file_status, simulation_hit_file, "../../../../../Data/outfile_BML_6BC_withmu_eta3.txt", read_mode);
        assert file_status = open_ok report FILE_OPEN_STATUS'IMAGE(file_status) & " while opening file!**" severity failure;
        while not endfile(simulation_hit_file) loop
            readline(simulation_hit_file, data_line);
            read(data_line, hit_time);
            read(data_line, eta_phi);
            read(data_line, l0_l1);
            read(data_line, hit_index); -- hit index in range [1,64]
            read(data_line, mu_match);
            while now < hit_time*1ns + 4 us loop
                 wait for 1 ps;
            end loop;
            file_hits_en(hit_index-1 + 144 * l0_l1 + 64 * eta_phi) <= not file_hits_en(hit_index-1 + 144 * l0_l1 + 64 * eta_phi);
            hwrite(hits_line, bcid); -- current bcid
            write(hits_line, space);
            write(hits_line, l0_l1);
            write(hits_line, space);
            write(hits_line, eta_phi);
            write(hits_line, space);
            write(hits_line, hit_index - 1);
            write(hits_line, space);
            write(hits_line, mu_match);
            writeline(generated_hit_file, hits_line);
        end loop;
        file_close(simulation_hit_file);
        assert false report "END OF SIMULATION" severity failure;
        wait;
    end process;

    file_hits_gen : for i in 0 to 287 generate
        eta_random_hit_logic : process(file_hits_en(i))
        begin
            if file_hits_en(i)'event then
                file_hits(i) <= '1', '0' after 10 ns;
            end if;
        end process;
    end generate;
    
    hit_bcid <= elink_in(19 downto 10);

    elink_decoding_logic : process(clock320_mmcm)
        file elinks_data_file: text open write_mode is  "../../../../../Data/elinks_out.txt";
        variable file_status  : FILE_OPEN_STATUS;
        variable elink_line : line;
        variable elink_time : time;
        constant space : string := " ";
    begin
        if rising_edge(clock320_mmcm) then
            if elink_in /= X"5555555" then
                hwrite(elink_line, bcid); -- current bcid
                write(elink_line, space);
                write(elink_line, hit_bcid); --hit bcid
                write(elink_line, space);
                if to_integer(unsigned(bcid(9 downto 8))) - to_integer(unsigned(hit_bcid(9 downto 8))) < 0 then -- this is when bcid>=1024 but hit_bcid<1014 
                    write(elink_line, 1024 + to_integer(unsigned(bcid(9 downto 0))) - to_integer(unsigned(hit_bcid))); -- BCID difference
                else 
                    write(elink_line, to_integer(unsigned(bcid(9 downto 0))) - to_integer(unsigned(hit_bcid))); -- BCID difference
                end if;
                write(elink_line, space);
                write(elink_line, elink_in(27)); -- coordinate (eta or phi)
                write(elink_line, space);
                write(elink_line, to_integer(unsigned(elink_in(26 downto 20)))); -- strip number                
                write(elink_line, space);
                hwrite(elink_line, elink_in(9 downto 5)); -- rising time layer 0              
                write(elink_line, space);
                hwrite(elink_line, elink_in(4 downto 0)); -- rising time layer 1
                writeline(elinks_data_file, elink_line);
            end if;
        end if;
    end process;
    
    
--    write_datacount_fifo : process(clock320_mmcm)
--        file datacount_file: text open write_mode is  "../../../../../Data/fifo_datacount.txt";
--        variable file_status  : FILE_OPEN_STATUS;
--        variable file_line : line;
--        variable elink_time : time;
--        constant space : string := " ";
--    begin
--        if rising_edge(clock320_mmcm) then
--            for i in 17 downto 0 loop
--                hwrite(file_line, datacount(i));
--                write(file_line, space);
--            end loop;
--            writeline(datacount_file, file_line);
--        end if;
--    end process;



end Behavioral;
