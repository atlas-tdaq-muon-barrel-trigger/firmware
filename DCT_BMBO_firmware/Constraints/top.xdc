set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]

set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

create_clock -period 25.000 [get_nets ECLK0_p]
#create_clock -period 25.000 [get_nets PSCLK0_p]
#create_clock -period 25.000 [get_nets CLK40_0]

# BUG in the schematics: the two clocks are connected to the _N pin instead of the _P pin
# if you want to use CLK40_0 and CLK40_1 as clocks you need to set the CLOCK_DEDICATED_ROUTE
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {CLK40_0}]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {CLK40_1}]

#set_property IOSTANDARD LVCMOS33 [get_ports CLK40_0]
#set_property IOSTANDARD LVCMOS33 [get_ports CLK40_1]

#set_property IOSTANDARD DIFF_HSUL_12 [get_ports {PSCLK0_p}]
#set_property IOSTANDARD DIFF_HSUL_12 [get_ports {PSCLK1_p}]

#set_property IOSTANDARD DIFF_HSUL_12 [get_ports {TST_CLK0_P}]
#set_property IOSTANDARD LVCMOS12 [get_ports {TST_CLK0_P}]
#set_property IOSTANDARD DIFF_HSUL_12 [get_ports {TST_CLK1_P}]

set_property IOSTANDARD DIFF_HSUL_12 [get_ports ECLK0_p]
#set_property IOSTANDARD DIFF_HSUL_12 [get_ports {ECLK1_p}]

set_property IOSTANDARD LVCMOS33 [get_ports {rpc_in[*]}]

set_property IOSTANDARD DIFF_HSUL_12 [get_ports {elink_out_p[*]}]

#to do: set pin for elink_ec_out_p
set_property IOSTANDARD DIFF_HSUL_12 [get_ports elink_ec_out_p]

set_property IOSTANDARD DIFF_HSUL_12 [get_ports {elink_in_p[*]}]

set_false_path -through BC_offset_reg[*]
set_false_path -through coarse_time_offset_reg[*][*]
set_false_path -through timing_offset_in_reg[*][*]
set_false_path -through channel_mask[*]
set_false_path -through ctrl_channel_idx_reg[*]
set_false_path -through dead_time_reg[*]

##################################################################
# FPGA pin assignment follows the schematics order, bank by bank #
##################################################################

# BANK 32
#IN5_out_3
set_property PACKAGE_PIN AN1 [get_ports {rpc_in[287]}]
#IN5_out_1
set_property PACKAGE_PIN AP1 [get_ports {rpc_in[286]}]
#IN5_out_24
set_property PACKAGE_PIN AK2 [get_ports {rpc_in[285]}]
#IN5_out_11
set_property PACKAGE_PIN AK1 [get_ports {rpc_in[284]}]
#IN5_out_16
set_property PACKAGE_PIN AM2 [get_ports {rpc_in[283]}]
#IN5_out_14
set_property PACKAGE_PIN AN2 [get_ports {rpc_in[282]}]
#IN5_out_22
set_property PACKAGE_PIN AL2 [get_ports {rpc_in[281]}]
#IN5_out_9
set_property PACKAGE_PIN AM1 [get_ports {rpc_in[280]}]
#IN5_out_8
set_property PACKAGE_PIN AN3 [get_ports {rpc_in[279]}]
#IN4_out_27
set_property PACKAGE_PIN AP3 [get_ports {rpc_in[278]}]
#IN4_out_28
set_property PACKAGE_PIN AK3 [get_ports {rpc_in[277]}]
#IN4_out_26
set_property PACKAGE_PIN AL3 [get_ports {rpc_in[276]}]
#IN5_out_6
set_property PACKAGE_PIN AN4 [get_ports {rpc_in[275]}]
#IN4_out_25
set_property PACKAGE_PIN AP4 [get_ports {rpc_in[274]}]
#IN5_out_2
set_property PACKAGE_PIN AJ5 [get_ports {rpc_in[273]}]
#IN4_out_29
set_property PACKAGE_PIN AK5 [get_ports {rpc_in[272]}]
#IN4_out_17
set_property PACKAGE_PIN AP6 [get_ports {rpc_in[271]}]
#IN4_out_19
set_property PACKAGE_PIN AP5 [get_ports {rpc_in[270]}]
#IN4_out_31
set_property PACKAGE_PIN AL4 [get_ports {rpc_in[269]}]
#IN4_out_20
set_property PACKAGE_PIN AM4 [get_ports {rpc_in[268]}]
#IN4_out_32
set_property PACKAGE_PIN AL5 [get_ports {rpc_in[267]}]
#IN4_out_23
set_property PACKAGE_PIN AM5 [get_ports {rpc_in[266]}]
#IN4_out_18
set_property PACKAGE_PIN AM7 [get_ports {rpc_in[265]}]
#IN4_out_21
set_property PACKAGE_PIN AM6 [get_ports {rpc_in[264]}]
#IN4_out_10
set_property PACKAGE_PIN AJ8 [get_ports {rpc_in[263]}]
#IN4_out_12
set_property PACKAGE_PIN AK8 [get_ports {rpc_in[262]}]
#IN4_out_22
set_property PACKAGE_PIN AN8 [get_ports {rpc_in[261]}]
#IN4_out_11
set_property PACKAGE_PIN AP8 [get_ports {rpc_in[260]}]
#IN4_out_24
set_property PACKAGE_PIN AN7 [get_ports {rpc_in[259]}]
#IN4_out_30
set_property PACKAGE_PIN AN6 [get_ports {rpc_in[258]}]
#IN4_out_16
set_property PACKAGE_PIN AN9 [get_ports {rpc_in[257]}]
#IN4_out_9
set_property PACKAGE_PIN AP9 [get_ports {rpc_in[256]}]
#IN4_out_13
set_property PACKAGE_PIN AK10 [get_ports {rpc_in[255]}]
#IN4_out_1
set_property PACKAGE_PIN AP11 [get_ports {rpc_in[254]}]
#IN4_out_3
set_property PACKAGE_PIN AP10 [get_ports {rpc_in[253]}]
#IN4_out_15
set_property PACKAGE_PIN AL9 [get_ports {rpc_in[252]}]
#IN4_out_4
set_property PACKAGE_PIN AM9 [get_ports {rpc_in[251]}]
#IN4_out_6
set_property PACKAGE_PIN AM11 [get_ports {rpc_in[250]}]
#IN4_out_8
set_property PACKAGE_PIN AN11 [get_ports {rpc_in[249]}]
#IN4_out_7
set_property PACKAGE_PIN AJ11 [get_ports {rpc_in[248]}]
#IN4_out_5
set_property PACKAGE_PIN AK11 [get_ports {rpc_in[247]}]
#IN4_out_2
set_property PACKAGE_PIN AL10 [get_ports {rpc_in[246]}]
#IN4_out_14
set_property PACKAGE_PIN AM10 [get_ports {rpc_in[245]}]
###############################################################################
# MRCC clock pins
#CLK40_1
#set_property PACKAGE_PIN AK6 [get_ports CLK40_1]
#CLK40_0
#set_property PACKAGE_PIN AL7 [get_ports CLK40_0]
###############################################################################
### NOTE: change CLK40_0 and CLK40_1 pins in next version of DCT board   ######
### they are now connected to _N pins, they should be connected to _P    ######
###############################################################################


# BANK 33
#IN6_out_1
set_property PACKAGE_PIN AG1 [get_ports {rpc_in[244]}]
#IN5_out_19
set_property PACKAGE_PIN AH1 [get_ports {rpc_in[243]}]
#IN6_out_9
set_property PACKAGE_PIN AD1 [get_ports {rpc_in[242]}]
#IN6_out_3
set_property PACKAGE_PIN AE1 [get_ports {rpc_in[241]}]
#IN5_out_32
set_property PACKAGE_PIN AH2 [get_ports {rpc_in[240]}]
#IN5_out_17
set_property PACKAGE_PIN AJ1 [get_ports {rpc_in[239]}]
#IN6_out_14
set_property PACKAGE_PIN AE2 [get_ports {rpc_in[238]}]
#IN6_out_8
set_property PACKAGE_PIN AF2 [get_ports {rpc_in[237]}]
#IN6_out_6
set_property PACKAGE_PIN AG2 [get_ports {rpc_in[236]}]
#IN5_out_30
set_property PACKAGE_PIN AJ3 [get_ports {rpc_in[235]}]
#IN5_out_15
set_property PACKAGE_PIN AF4 [get_ports {rpc_in[234]}]
#IN5_out_23
set_property PACKAGE_PIN AD5 [get_ports {rpc_in[233]}]
#IN5_out_5
set_property PACKAGE_PIN AJ4 [get_ports {rpc_in[232]}]
#IN6_out_16
set_property PACKAGE_PIN AE3 [get_ports {rpc_in[231]}]
#IN5_out_13
set_property PACKAGE_PIN AG5 [get_ports {rpc_in[230]}]
#IN5_out_12
set_property PACKAGE_PIN AF7 [get_ports {rpc_in[229]}]
#IN5_out_10
set_property PACKAGE_PIN AG7 [get_ports {rpc_in[228]}]
#IN5_out_26
set_property PACKAGE_PIN AD8 [get_ports {rpc_in[227]}]
#IN5_out_20
set_property PACKAGE_PIN AE8 [get_ports {rpc_in[226]}]
#IN5_out_21
set_property PACKAGE_PIN AE7 [get_ports {rpc_in[225]}]
#IN5_out_4
set_property PACKAGE_PIN AH7 [get_ports {rpc_in[224]}]
#IN5_out_7
set_property PACKAGE_PIN AH6 [get_ports {rpc_in[223]}]
#IN5_out_18
set_property PACKAGE_PIN AF8 [get_ports {rpc_in[222]}]



# BANK 34
#IN6_out_18
set_property PACKAGE_PIN V9 [get_ports {rpc_in[221]}]
#IN6_out_11
set_property PACKAGE_PIN V8 [get_ports {rpc_in[220]}]
#IN6_out_5
set_property PACKAGE_PIN W9 [get_ports {rpc_in[219]}]
#IN6_out_12
set_property PACKAGE_PIN W8 [get_ports {rpc_in[218]}]
#IN6_out_7
set_property PACKAGE_PIN V7 [get_ports {rpc_in[217]}]
#IN6_out_21
set_property PACKAGE_PIN V6 [get_ports {rpc_in[216]}]
#IN5_out_31
set_property PACKAGE_PIN Y8 [get_ports {rpc_in[215]}]
#IN6_out_10
set_property PACKAGE_PIN Y7 [get_ports {rpc_in[214]}]
#IN7_out_3
set_property PACKAGE_PIN W1 [get_ports {rpc_in[213]}]
#IN7_out_1
set_property PACKAGE_PIN Y1 [get_ports {rpc_in[212]}]
#IN7_out_4
set_property PACKAGE_PIN V2 [get_ports {rpc_in[211]}]
#IN7_out_9
set_property PACKAGE_PIN V1 [get_ports {rpc_in[210]}]
#IN6_out_32
set_property PACKAGE_PIN Y2 [get_ports {rpc_in[209]}]
#IN7_out_6
set_property PACKAGE_PIN V3 [get_ports {rpc_in[208]}]
#IN7_out_2
set_property PACKAGE_PIN W3 [get_ports {rpc_in[207]}]
#IN6_out_23
set_property PACKAGE_PIN V4 [get_ports {rpc_in[206]}]
#IN6_out_19
set_property PACKAGE_PIN W5 [get_ports {rpc_in[205]}]
#IN6_out_17
set_property PACKAGE_PIN Y5 [get_ports {rpc_in[204]}]
#IN5_out_29
set_property PACKAGE_PIN AA5 [get_ports {rpc_in[203]}]
#IN6_out_15
set_property PACKAGE_PIN AA4 [get_ports {rpc_in[202]}]
#IN6_out_24
set_property PACKAGE_PIN AB2 [get_ports {rpc_in[201]}]
#IN6_out_27
set_property PACKAGE_PIN AB1 [get_ports {rpc_in[200]}]
#IN6_out_30
set_property PACKAGE_PIN AA2 [get_ports {rpc_in[199]}]
#IN6_out_22
set_property PACKAGE_PIN AC2 [get_ports {rpc_in[198]}]
#IN6_out_25
set_property PACKAGE_PIN AC1 [get_ports {rpc_in[197]}]
#IN6_out_13
set_property PACKAGE_PIN AC3 [get_ports {rpc_in[196]}]
#IN6_out_4
set_property PACKAGE_PIN AA7 [get_ports {rpc_in[195]}]
#IN5_out_25
set_property PACKAGE_PIN AC6 [get_ports {rpc_in[194]}]
#IN6_out_2
set_property PACKAGE_PIN AB7 [get_ports {rpc_in[193]}]
#IN5_out_27
set_property PACKAGE_PIN AB6 [get_ports {rpc_in[192]}]
#IN5_out_28
set_property PACKAGE_PIN AC8 [get_ports {rpc_in[191]}]




# BANK 35
#IN7_out_7
set_property PACKAGE_PIN M7 [get_ports {rpc_in[190]}]
#IN7_out_28
set_property PACKAGE_PIN M6 [get_ports {rpc_in[189]}]
#IN7_out_23
set_property PACKAGE_PIN N7 [get_ports {rpc_in[188]}]
#IN7_out_20
set_property PACKAGE_PIN P6 [get_ports {rpc_in[187]}]
#IN7_out_26
set_property PACKAGE_PIN N6 [get_ports {rpc_in[186]}]
#IN7_out_25
set_property PACKAGE_PIN N1 [get_ports {rpc_in[185]}]
#IN7_out_27
set_property PACKAGE_PIN M1 [get_ports {rpc_in[184]}]
#IN7_out_31
set_property PACKAGE_PIN M5 [get_ports {rpc_in[183]}]
#IN7_out_32
set_property PACKAGE_PIN M4 [get_ports {rpc_in[182]}]
#IN7_out_17
set_property PACKAGE_PIN R1 [get_ports {rpc_in[181]}]
#IN7_out_19
set_property PACKAGE_PIN P1 [get_ports {rpc_in[180]}]
#IN7_out_30
set_property PACKAGE_PIN N3 [get_ports {rpc_in[179]}]
#IN7_out_24
set_property PACKAGE_PIN N2 [get_ports {rpc_in[178]}]
#IN7_out_5
set_property PACKAGE_PIN P4 [get_ports {rpc_in[177]}]
#IN7_out_22
set_property PACKAGE_PIN P3 [get_ports {rpc_in[176]}]
#IN7_out_29
set_property PACKAGE_PIN N4 [get_ports {rpc_in[175]}]
#IN7_out_18
set_property PACKAGE_PIN R6 [get_ports {rpc_in[174]}]
#IN6_out_28
set_property PACKAGE_PIN T5 [get_ports {rpc_in[173]}]
#IN7_out_12
set_property PACKAGE_PIN T4 [get_ports {rpc_in[172]}]
#IN7_out_21
set_property PACKAGE_PIN R3 [get_ports {rpc_in[171]}]
#IN7_out_16
set_property PACKAGE_PIN R2 [get_ports {rpc_in[170]}]
#IN7_out_8
set_property PACKAGE_PIN U2 [get_ports {rpc_in[169]}]
#IN7_out_11
set_property PACKAGE_PIN U1 [get_ports {rpc_in[168]}]
#IN7_out_15
set_property PACKAGE_PIN T3 [get_ports {rpc_in[167]}]
#IN7_out_14
set_property PACKAGE_PIN T2 [get_ports {rpc_in[166]}]
#IN6_out_20
set_property PACKAGE_PIN U5 [get_ports {rpc_in[165]}]
#IN7_out_13
set_property PACKAGE_PIN U4 [get_ports {rpc_in[164]}]
#IN6_out_31
set_property PACKAGE_PIN R8 [get_ports {rpc_in[163]}]
#IN6_out_29
set_property PACKAGE_PIN R7 [get_ports {rpc_in[162]}]
#IN6_out_26
set_property PACKAGE_PIN U7 [get_ports {rpc_in[161]}]
#IN7_out_10
set_property PACKAGE_PIN U6 [get_ports {rpc_in[160]}]




# BANK 16
#dataout13_p
#dataout13_n
set_property PACKAGE_PIN K23 [get_ports {elink_out_p[13]}]
set_property PACKAGE_PIN J23 [get_ports {elink_out_n[13]}]
#dataout15_p
#dataout15_n
set_property PACKAGE_PIN K25 [get_ports {elink_out_p[15]}]
set_property PACKAGE_PIN J25 [get_ports {elink_out_n[15]}]
#dataout16_p
#dataout16_n
set_property PACKAGE_PIN M25 [get_ports {elink_out_p[16]}]
set_property PACKAGE_PIN L25 [get_ports {elink_out_n[16]}]
#dataout14_p
#dataout14_n
set_property PACKAGE_PIN J24 [get_ports {elink_out_p[14]}]
set_property PACKAGE_PIN H24 [get_ports {elink_out_n[14]}]
#datain3_p
#datain3_n
set_property PACKAGE_PIN L27 [get_ports {elink_in_p[3]}]
set_property PACKAGE_PIN K27 [get_ports {elink_in_n[3]}]
#datain2_p
#datain2_n
set_property PACKAGE_PIN K26 [get_ports {elink_in_p[2]}]
set_property PACKAGE_PIN J26 [get_ports {elink_in_n[2]}]
#dataout18_p
#dataout18_n
set_property PACKAGE_PIN L28 [get_ports {elink_out_p[18]}]
set_property PACKAGE_PIN K28 [get_ports {elink_out_n[18]}]
# MRCC clock pins
set_property PACKAGE_PIN J28 [get_ports ECLK0_p]
set_property PACKAGE_PIN H28 [get_ports ECLK0_n]
#set_property PACKAGE_PIN J29 [get_ports {ECLK1_p}]
#set_property PACKAGE_PIN H29 [get_ports {ECLK1_n}]
#
#dataout27_p
#dataout27_n
set_property PACKAGE_PIN K30 [get_ports {elink_out_p[27]}]
set_property PACKAGE_PIN J30 [get_ports {elink_out_n[27]}]
#dataout17_p
#dataout17_n
set_property PACKAGE_PIN G29 [get_ports {elink_out_p[17]}]
set_property PACKAGE_PIN G30 [get_ports {elink_out_n[17]}]
#dataout20_p
#dataout20_n
set_property PACKAGE_PIN K31 [get_ports {elink_out_p[20]}]
set_property PACKAGE_PIN J31 [get_ports {elink_out_n[20]}]
#dataout21_p
#dataout21_n
set_property PACKAGE_PIN H31 [get_ports {elink_out_p[21]}]
set_property PACKAGE_PIN G31 [get_ports {elink_out_n[21]}]
#dataout19_p
#dataout19_n
set_property PACKAGE_PIN L29 [get_ports {elink_out_p[19]}]
set_property PACKAGE_PIN L30 [get_ports {elink_out_n[19]}]
#dataout23_p
#dataout23_n
set_property PACKAGE_PIN H32 [get_ports {elink_out_p[23]}]
set_property PACKAGE_PIN G32 [get_ports {elink_out_n[23]}]
#dataout24_p
#dataout24_n
set_property PACKAGE_PIN H33 [get_ports {elink_out_p[24]}]
set_property PACKAGE_PIN G34 [get_ports {elink_out_n[24]}]
#dataout22_p
#dataout22_n
set_property PACKAGE_PIN L32 [get_ports {elink_out_p[22]}]
set_property PACKAGE_PIN K32 [get_ports {elink_out_n[22]}]
#dataout26_p
#dataout26_n
set_property PACKAGE_PIN J33 [get_ports {elink_out_p[26]}]
set_property PACKAGE_PIN H34 [get_ports {elink_out_n[26]}]
#dataout25_p
#dataout25_n
set_property PACKAGE_PIN L33 [get_ports {elink_out_p[25]}]
set_property PACKAGE_PIN L34 [get_ports {elink_out_n[25]}]



# BANK 36
#dataout12_p
#dataout12_n
set_property PACKAGE_PIN L12 [get_ports {elink_out_p[12]}]
set_property PACKAGE_PIN K12 [get_ports {elink_out_n[12]}]
#dataout11_p
#dataout11_n
set_property PACKAGE_PIN K11 [get_ports {elink_out_p[11]}]
set_property PACKAGE_PIN J11 [get_ports {elink_out_n[11]}]
#dataout10_p
#dataout10_n
set_property PACKAGE_PIN H11 [get_ports {elink_out_p[10]}]
set_property PACKAGE_PIN G11 [get_ports {elink_out_n[10]}]
#dataout5_p
#dataout5_n
set_property PACKAGE_PIN L10 [get_ports {elink_out_p[5]}]
set_property PACKAGE_PIN L9 [get_ports {elink_out_n[5]}]
#dataout9_p
#dataout9_n
set_property PACKAGE_PIN K10 [get_ports {elink_out_p[9]}]
set_property PACKAGE_PIN J10 [get_ports {elink_out_n[9]}]
#dataout8_p
#dataout8_n
set_property PACKAGE_PIN J9 [get_ports {elink_out_p[8]}]
set_property PACKAGE_PIN J8 [get_ports {elink_out_n[8]}]
#dataout7_p
#dataout7_n
set_property PACKAGE_PIN L8 [get_ports {elink_out_p[7]}]
set_property PACKAGE_PIN K8 [get_ports {elink_out_n[7]}]
#dataout6_p
#dataout6_n
set_property PACKAGE_PIN G7 [get_ports {elink_out_p[6]}]
set_property PACKAGE_PIN G6 [get_ports {elink_out_n[6]}]
## SRCC clock pins
#set_property PACKAGE_PIN K7 [get_ports {TST_CLK0_P}]
#set_property PACKAGE_PIN K6 [get_ports {TST_CLK0_N}]
## MRCC clock pins
#set_property PACKAGE_PIN H7 [get_ports {PSCLK1_p}]
#set_property PACKAGE_PIN H6 [get_ports {PSCLK1_n}]
#set_property PACKAGE_PIN G5 [get_ports {PSCLK0_p}]
#set_property PACKAGE_PIN G4 [get_ports {PSCLK0_n}]
## SRCC clock pins
#set_property PACKAGE_PIN J6 [get_ports {TST_CLK1_P}]
#set_property PACKAGE_PIN J5 [get_ports {TST_CLK1_N}]
#
#dataout2_p
#dataout2_n
set_property PACKAGE_PIN F3 [get_ports {elink_out_p[2]}]
set_property PACKAGE_PIN F2 [get_ports {elink_out_n[2]}]
#dataout4_p
#dataout4_n
set_property PACKAGE_PIN L5 [get_ports {elink_out_p[4]}]
set_property PACKAGE_PIN K5 [get_ports {elink_out_n[4]}]
#dataout3_p
#dataout3_n
set_property PACKAGE_PIN H4 [get_ports {elink_out_p[3]}]
set_property PACKAGE_PIN H3 [get_ports {elink_out_n[3]}]
#datain1_p
#datain1_n
set_property PACKAGE_PIN J4 [get_ports {elink_in_p[1]}]
set_property PACKAGE_PIN J3 [get_ports {elink_in_n[1]}]
#dataout1_p
#dataout1_n
set_property PACKAGE_PIN H2 [get_ports {elink_out_p[1]}]
set_property PACKAGE_PIN G2 [get_ports {elink_out_n[1]}]
#dataout0_p
#dataout0_n
set_property PACKAGE_PIN K1 [get_ports {elink_out_p[0]}]
set_property PACKAGE_PIN J1 [get_ports {elink_out_n[0]}]
#datain0_p
#datain0_n
set_property PACKAGE_PIN H1 [get_ports {elink_in_p[0]}]
set_property PACKAGE_PIN G1 [get_ports {elink_in_n[0]}]



# BANK 12
#IN0_out_24
set_property PACKAGE_PIN AJ24 [get_ports {rpc_in[159]}]
#IN1_out_25
set_property PACKAGE_PIN AL34 [get_ports {rpc_in[158]}]
#IN1_out_27
set_property PACKAGE_PIN AM34 [get_ports {rpc_in[157]}]
#IN2_out_35
set_property PACKAGE_PIN AJ33 [get_ports {rpc_in[156]}]
#IN2_out_33
set_property PACKAGE_PIN AJ34 [get_ports {rpc_in[155]}]
#IN1_out_33
set_property PACKAGE_PIN AN34 [get_ports {rpc_in[154]}]
#IN1_out_35
set_property PACKAGE_PIN AP34 [get_ports {rpc_in[153]}]
#IN1_out_17
set_property PACKAGE_PIN AK33 [get_ports {rpc_in[152]}]
#IN1_out_20
set_property PACKAGE_PIN AL33 [get_ports {rpc_in[151]}]
#IN0_out_5
set_property PACKAGE_PIN AN33 [get_ports {rpc_in[150]}]
#IN0_out_7
set_property PACKAGE_PIN AP33 [get_ports {rpc_in[149]}]
#IN0_out_18
set_property PACKAGE_PIN AL32 [get_ports {rpc_in[148]}]
#IN1_out_21
set_property PACKAGE_PIN AM32 [get_ports {rpc_in[147]}]
#IN0_out_10
set_property PACKAGE_PIN AJ31 [get_ports {rpc_in[146]}]
#IN0_out_12
set_property PACKAGE_PIN AK32 [get_ports {rpc_in[145]}]
#IN1_out_37
set_property PACKAGE_PIN AM31 [get_ports {rpc_in[144]}]
#IN0_out_1
set_property PACKAGE_PIN AN32 [get_ports {rpc_in[143]}]
#IN1_out_15
set_property PACKAGE_PIN AJ30 [get_ports {rpc_in[142]}]
#IN1_out_19
set_property PACKAGE_PIN AK31 [get_ports {rpc_in[141]}]
#IN0_out_13
set_property PACKAGE_PIN AN31 [get_ports {rpc_in[140]}]
#IN0_out_15
set_property PACKAGE_PIN AP31 [get_ports {rpc_in[139]}]
#IN1_out_18
set_property PACKAGE_PIN AJ29 [get_ports {rpc_in[138]}]
#IN0_out_3
set_property PACKAGE_PIN AK30 [get_ports {rpc_in[137]}]
#IN0_out_9
set_property PACKAGE_PIN AL30 [get_ports {rpc_in[136]}]
#IN0_out_20
set_property PACKAGE_PIN AM30 [get_ports {rpc_in[135]}]
#IN0_out_34
set_property PACKAGE_PIN AL28 [get_ports {rpc_in[134]}]
#IN0_out_26
set_property PACKAGE_PIN AL29 [get_ports {rpc_in[133]}]
#IN0_out_17
set_property PACKAGE_PIN AJ28 [get_ports {rpc_in[132]}]
#IN0_out_11
set_property PACKAGE_PIN AK28 [get_ports {rpc_in[131]}]
#IN0_out_23
set_property PACKAGE_PIN AP29 [get_ports {rpc_in[130]}]
#IN0_out_21
set_property PACKAGE_PIN AP30 [get_ports {rpc_in[129]}]
#IN0_out_28
set_property PACKAGE_PIN AM29 [get_ports {rpc_in[128]}]
#IN0_out_6
set_property PACKAGE_PIN AN29 [get_ports {rpc_in[127]}]
#IN0_out_8
set_property PACKAGE_PIN AN28 [get_ports {rpc_in[126]}]
#IN0_out_29
set_property PACKAGE_PIN AP28 [get_ports {rpc_in[125]}]
#IN1_out_30
set_property PACKAGE_PIN AK27 [get_ports {rpc_in[124]}]
#IN0_out_36
set_property PACKAGE_PIN AL27 [get_ports {rpc_in[123]}]
#IN0_out_22
set_property PACKAGE_PIN AJ25 [get_ports {rpc_in[122]}]
#IN1_out_36
set_property PACKAGE_PIN AK25 [get_ports {rpc_in[121]}]
#IN0_out_16
set_property PACKAGE_PIN AJ26 [get_ports {rpc_in[120]}]
#IN1_out_29
set_property PACKAGE_PIN AK26 [get_ports {rpc_in[119]}]
#IN0_out_38
set_property PACKAGE_PIN AM26 [get_ports {rpc_in[118]}]
#IN1_out_31
set_property PACKAGE_PIN AN26 [get_ports {rpc_in[117]}]
#IN0_out_14
set_property PACKAGE_PIN AM27 [get_ports {rpc_in[116]}]
#IN0_out_31
set_property PACKAGE_PIN AN27 [get_ports {rpc_in[115]}]
#IN1_out_34
set_property PACKAGE_PIN AL25 [get_ports {rpc_in[114]}]
#IN0_out_40
set_property PACKAGE_PIN AM25 [get_ports {rpc_in[113]}]
#IN0_out_39
set_property PACKAGE_PIN AP25 [get_ports {rpc_in[112]}]
#IN0_out_37
set_property PACKAGE_PIN AP26 [get_ports {rpc_in[111]}]
#IN0_out_32
set_property PACKAGE_PIN AL24 [get_ports {rpc_in[110]}]



# BANK 13
#IN2_out_19
set_property PACKAGE_PIN AF34 [get_ports {rpc_in[109]}]
#IN2_out_25
set_property PACKAGE_PIN AG34 [get_ports {rpc_in[108]}]
#IN1_out_5
set_property PACKAGE_PIN AD33 [get_ports {rpc_in[107]}]
#IN2_out_11
set_property PACKAGE_PIN AD34 [get_ports {rpc_in[106]}]
#IN1_out_12
set_property PACKAGE_PIN AH33 [get_ports {rpc_in[105]}]
#IN2_out_27
set_property PACKAGE_PIN AH34 [get_ports {rpc_in[104]}]
#IN2_out_17
set_property PACKAGE_PIN AE33 [get_ports {rpc_in[103]}]
#IN1_out_40
set_property PACKAGE_PIN AG32 [get_ports {rpc_in[102]}]
#IN0_out_4
set_property PACKAGE_PIN AH32 [get_ports {rpc_in[101]}]
#IN1_out_7
set_property PACKAGE_PIN AE32 [get_ports {rpc_in[100]}]
#IN1_out_23
set_property PACKAGE_PIN AF32 [get_ports {rpc_in[99]}]
#IN1_out_32
set_property PACKAGE_PIN AD31 [get_ports {rpc_in[98]}]
#IN1_out_38
set_property PACKAGE_PIN AE31 [get_ports {rpc_in[97]}]
#IN1_out_10
set_property PACKAGE_PIN AG31 [get_ports {rpc_in[96]}]
#IN0_out_2
set_property PACKAGE_PIN AH31 [get_ports {rpc_in[95]}]
#IN1_out_24
set_property PACKAGE_PIN AF29 [get_ports {rpc_in[94]}]
#IN1_out_26
set_property PACKAGE_PIN AG29 [get_ports {rpc_in[93]}]
#IN1_out_11
set_property PACKAGE_PIN AH28 [get_ports {rpc_in[92]}]
#IN1_out_13
set_property PACKAGE_PIN AH29 [get_ports {rpc_in[91]}]
#IN1_out_28
set_property PACKAGE_PIN AF28 [get_ports {rpc_in[90]}]
#IN2_out_37
set_property PACKAGE_PIN AD26 [get_ports {rpc_in[89]}]
#IN2_out_36
set_property PACKAGE_PIN AC27 [get_ports {rpc_in[88]}]
#IN1_out_9
set_property PACKAGE_PIN AG27 [get_ports {rpc_in[87]}]
#IN0_out_19
set_property PACKAGE_PIN AH27 [get_ports {rpc_in[86]}]
#IN0_out_25
set_property PACKAGE_PIN AG26 [get_ports {rpc_in[85]}]
#IN1_out_39
set_property PACKAGE_PIN AH26 [get_ports {rpc_in[84]}]
#IN1_out_2
set_property PACKAGE_PIN AE23 [get_ports {rpc_in[83]}]
#IN0_out_35
set_property PACKAGE_PIN AF23 [get_ports {rpc_in[82]}]
#IN1_out_3
set_property PACKAGE_PIN AG24 [get_ports {rpc_in[81]}]
#IN0_out_30
set_property PACKAGE_PIN AH24 [get_ports {rpc_in[80]}]
#IN2_out_38
set_property PACKAGE_PIN AC24 [get_ports {rpc_in[79]}]
#IN2_out_40
set_property PACKAGE_PIN AD24 [get_ports {rpc_in[78]}]
#IN1_out_4
set_property PACKAGE_PIN AF25 [get_ports {rpc_in[77]}]
#IN0_out_27
set_property PACKAGE_PIN AG25 [get_ports {rpc_in[76]}]
#IN2_out_39
set_property PACKAGE_PIN AD25 [get_ports {rpc_in[75]}]
#IN1_out_1
set_property PACKAGE_PIN AE25 [get_ports {rpc_in[74]}]
#IN0_out_33
set_property PACKAGE_PIN AF24 [get_ports {rpc_in[73]}]



# BANK 14
#IN2_out_26
set_property PACKAGE_PIN V24 [get_ports {rpc_in[72]}]
#set_property PACKAGE_PIN V28  FPGA_D0
#set_property PACKAGE_PIN V29  FPGA_D1
#set_property PACKAGE_PIN V26  FPGA_D2
#set_property PACKAGE_PIN V27  FPGA_D3
#IN2_out_28
set_property PACKAGE_PIN W28 [get_ports {rpc_in[71]}]
#IN2_out_12
set_property PACKAGE_PIN W29 [get_ports {rpc_in[70]}]
#IN2_out_29
set_property PACKAGE_PIN W25 [get_ports {rpc_in[69]}]
#IN2_out_31
set_property PACKAGE_PIN Y25 [get_ports {rpc_in[68]}]
#IN2_out_13
set_property PACKAGE_PIN Y28 [get_ports {rpc_in[67]}]
#IN1_out_6
set_property PACKAGE_PIN V31 [get_ports {rpc_in[66]}]
#IN2_out_32
set_property PACKAGE_PIN V32 [get_ports {rpc_in[65]}]
#IN3_out_35
set_property PACKAGE_PIN W33 [get_ports {rpc_in[64]}]
#IN3_out_33
set_property PACKAGE_PIN W34 [get_ports {rpc_in[63]}]
#IN3_out_29
set_property PACKAGE_PIN V33 [get_ports {rpc_in[62]}]
#IN3_out_27
set_property PACKAGE_PIN V34 [get_ports {rpc_in[61]}]
#IN3_out_31
set_property PACKAGE_PIN Y32 [get_ports {rpc_in[60]}]
#IN3_out_34
set_property PACKAGE_PIN Y33 [get_ports {rpc_in[59]}]
#IN1_out_8
set_property PACKAGE_PIN W31 [get_ports {rpc_in[58]}]
#IN1_out_14
set_property PACKAGE_PIN Y31 [get_ports {rpc_in[57]}]
#IN3_out_38
set_property PACKAGE_PIN AB30 [get_ports {rpc_in[56]}]
#IN3_out_39
set_property PACKAGE_PIN AB31 [get_ports {rpc_in[55]}]
#IN1_out_16
set_property PACKAGE_PIN AB32 [get_ports {rpc_in[54]}]
#IN1_out_22
set_property PACKAGE_PIN AC33 [get_ports {rpc_in[53]}]
#IN2_out_9
set_property PACKAGE_PIN AC34 [get_ports {rpc_in[52]}]
#IN3_out_36
set_property PACKAGE_PIN AA33 [get_ports {rpc_in[51]}]
#IN2_out_6
set_property PACKAGE_PIN AC31 [get_ports {rpc_in[50]}]
#IN2_out_1
set_property PACKAGE_PIN AA34 [get_ports {rpc_in[49]}]
#IN2_out_3
set_property PACKAGE_PIN AB34 [get_ports {rpc_in[48]}]
#IN2_out_18
set_property PACKAGE_PIN AA24 [get_ports {rpc_in[47]}]
#IN2_out_15
set_property PACKAGE_PIN AA25 [get_ports {rpc_in[46]}]
#IN2_out_20
set_property PACKAGE_PIN AB24 [get_ports {rpc_in[45]}]
#IN2_out_34
set_property PACKAGE_PIN AB25 [get_ports {rpc_in[44]}]
#IN2_out_10
set_property PACKAGE_PIN W24 [get_ports {rpc_in[43]}]



# BANK 13
#IN2_out_5
set_property PACKAGE_PIN T24 [get_ports {rpc_in[42]}]
#IN3_out_32
set_property PACKAGE_PIN R26 [get_ports {rpc_in[41]}]
#IN3_out_10
set_property PACKAGE_PIN P26 [get_ports {rpc_in[40]}]
#IN3_out_5
set_property PACKAGE_PIN N26 [get_ports {rpc_in[39]}]
#IN3_out_2
set_property PACKAGE_PIN M27 [get_ports {rpc_in[38]}]
#IN2_out_7
set_property PACKAGE_PIN U25 [get_ports {rpc_in[37]}]
#IN2_out_4
set_property PACKAGE_PIN T25 [get_ports {rpc_in[36]}]
#IN3_out_6
set_property PACKAGE_PIN P24 [get_ports {rpc_in[35]}]
#IN3_out_8
set_property PACKAGE_PIN N24 [get_ports {rpc_in[34]}]
#IN2_out_23
set_property PACKAGE_PIN U26 [get_ports {rpc_in[33]}]
#IN2_out_21
set_property PACKAGE_PIN U27 [get_ports {rpc_in[32]}]
#IN2_out_2
set_property PACKAGE_PIN R25 [get_ports {rpc_in[31]}]
#IN3_out_15
set_property PACKAGE_PIN P25 [get_ports {rpc_in[30]}]
#IN3_out_37
set_property PACKAGE_PIN T27 [get_ports {rpc_in[29]}]
#IN3_out_4
set_property PACKAGE_PIN N27 [get_ports {rpc_in[28]}]
#IN3_out_7
set_property PACKAGE_PIN N28 [get_ports {rpc_in[27]}]
#IN3_out_24
set_property PACKAGE_PIN N29 [get_ports {rpc_in[26]}]
#IN3_out_13
set_property PACKAGE_PIN M29 [get_ports {rpc_in[25]}]
#IN3_out_12
set_property PACKAGE_PIN P28 [get_ports {rpc_in[24]}]
#IN3_out_18
set_property PACKAGE_PIN P29 [get_ports {rpc_in[23]}]
#IN3_out_21
set_property PACKAGE_PIN R30 [get_ports {rpc_in[22]}]
#IN3_out_30
set_property PACKAGE_PIN P30 [get_ports {rpc_in[21]}]
#IN3_out_26
set_property PACKAGE_PIN U30 [get_ports {rpc_in[20]}]
#IN2_out_22
set_property PACKAGE_PIN T30 [get_ports {rpc_in[19]}]
#IN3_out_14
set_property PACKAGE_PIN M30 [get_ports {rpc_in[18]}]
#IN3_out_16
set_property PACKAGE_PIN M31 [get_ports {rpc_in[17]}]
#IN2_out_16
set_property PACKAGE_PIN R31 [get_ports {rpc_in[16]}]
#IN2_out_14
set_property PACKAGE_PIN P31 [get_ports {rpc_in[15]}]
#IN3_out_40
set_property PACKAGE_PIN N31 [get_ports {rpc_in[14]}]
#IN3_out_22
set_property PACKAGE_PIN M32 [get_ports {rpc_in[13]}]
#IN3_out_28
set_property PACKAGE_PIN U31 [get_ports {rpc_in[12]}]
#IN2_out_30
set_property PACKAGE_PIN U32 [get_ports {rpc_in[11]}]
#IN2_out_24
set_property PACKAGE_PIN T32 [get_ports {rpc_in[10]}]
#IN3_out_23
set_property PACKAGE_PIN R32 [get_ports {rpc_in[9]}]
#IN2_out_8
set_property PACKAGE_PIN N32 [get_ports {rpc_in[8]}]
#IN3_out_3
set_property PACKAGE_PIN N33 [get_ports {rpc_in[7]}]
#IN3_out_17
set_property PACKAGE_PIN R33 [get_ports {rpc_in[6]}]
#IN3_out_9
set_property PACKAGE_PIN N34 [get_ports {rpc_in[5]}]
#IN3_out_1
set_property PACKAGE_PIN M34 [get_ports {rpc_in[4]}]
#IN3_out_25
set_property PACKAGE_PIN U34 [get_ports {rpc_in[3]}]
#IN3_out_19
set_property PACKAGE_PIN T34 [get_ports {rpc_in[2]}]
#IN3_out_20
set_property PACKAGE_PIN P33 [get_ports {rpc_in[1]}]
#IN3_out_11
set_property PACKAGE_PIN P34 [get_ports {rpc_in[0]}]




#set_property PACKAGE_PIN AL5 [get_ports clock40_p]

#set_property PACKAGE_PIN L5   [get_ports {elink_out_p[27]}]
#set_property PACKAGE_PIN H4   [get_ports {elink_out_p[26]}]
#set_property PACKAGE_PIN J4   [get_ports {elink_out_p[25]}]
#set_property PACKAGE_PIN AN3  [get_ports {elink_out_p[24]}]
#set_property PACKAGE_PIN AK3  [get_ports {elink_out_p[23]}]
#set_property PACKAGE_PIN AN4  [get_ports {elink_out_p[22]}]
#set_property PACKAGE_PIN L4   [get_ports {elink_out_p[21]}]
#set_property PACKAGE_PIN AJ5  [get_ports {elink_out_p[20]}]
#set_property PACKAGE_PIN AP6  [get_ports {elink_out_p[19]}]
#set_property PACKAGE_PIN H2   [get_ports {elink_out_p[18]}]
#set_property PACKAGE_PIN AL4  [get_ports {elink_out_p[17]}]
#set_property PACKAGE_PIN AJ6  [get_ports {elink_out_p[16]}]
#set_property PACKAGE_PIN AK7  [get_ports {elink_out_p[15]}]
#set_property PACKAGE_PIN AM7  [get_ports {elink_out_p[14]}]
#set_property PACKAGE_PIN AJ8  [get_ports {elink_out_p[13]}]
#set_property PACKAGE_PIN AN8  [get_ports {elink_out_p[12]}]
#set_property PACKAGE_PIN AN7  [get_ports {elink_out_p[11]}]
#set_property PACKAGE_PIN AN9  [get_ports {elink_out_p[10]}]
#set_property PACKAGE_PIN K1   [get_ports {elink_out_p[9]}]
#set_property PACKAGE_PIN AJ10 [get_ports {elink_out_p[8]}]
#set_property PACKAGE_PIN AP11 [get_ports {elink_out_p[7]}]
#set_property PACKAGE_PIN H1   [get_ports {elink_out_p[6]}]
#set_property PACKAGE_PIN M2   [get_ports {elink_out_p[5]}]
#set_property PACKAGE_PIN K3   [get_ports {elink_out_p[4]}]
#set_property PACKAGE_PIN AL9  [get_ports {elink_out_p[3]}]
#set_property PACKAGE_PIN AM11 [get_ports {elink_out_p[2]}]
#set_property PACKAGE_PIN AJ11 [get_ports {elink_out_p[1]}]
#set_property PACKAGE_PIN AL10 [get_ports {elink_out_p[0]}]


#set_property PACKAGE_PIN AH1  [get_ports {rpc_in[287]}]
#set_property PACKAGE_PIN AD1  [get_ports {rpc_in[286]}]
#set_property PACKAGE_PIN AE1  [get_ports {rpc_in[285]}]
#set_property PACKAGE_PIN AH2  [get_ports {rpc_in[284]}]
#set_property PACKAGE_PIN AJ1  [get_ports {rpc_in[283]}]
#set_property PACKAGE_PIN AE2  [get_ports {rpc_in[282]}]
#set_property PACKAGE_PIN AF2  [get_ports {rpc_in[281]}]
#set_property PACKAGE_PIN AF3  [get_ports {rpc_in[280]}]
#set_property PACKAGE_PIN AG2  [get_ports {rpc_in[279]}]
#set_property PACKAGE_PIN AH3  [get_ports {rpc_in[278]}]
#set_property PACKAGE_PIN AJ3  [get_ports {rpc_in[277]}]
#set_property PACKAGE_PIN AF4  [get_ports {rpc_in[276]}]
#set_property PACKAGE_PIN AG4  [get_ports {rpc_in[275]}]
#set_property PACKAGE_PIN AD5  [get_ports {rpc_in[274]}]
#set_property PACKAGE_PIN AD4  [get_ports {rpc_in[273]}]
#set_property PACKAGE_PIN AH4  [get_ports {rpc_in[272]}]
#set_property PACKAGE_PIN AJ4  [get_ports {rpc_in[271]}]
#set_property PACKAGE_PIN AD3  [get_ports {rpc_in[270]}]
#set_property PACKAGE_PIN AE3  [get_ports {rpc_in[269]}]
#set_property PACKAGE_PIN AG6  [get_ports {rpc_in[268]}]
#set_property PACKAGE_PIN AG5  [get_ports {rpc_in[267]}]
#set_property PACKAGE_PIN AE5  [get_ports {rpc_in[266]}]
#set_property PACKAGE_PIN AF5  [get_ports {rpc_in[265]}]
#set_property PACKAGE_PIN AD6  [get_ports {rpc_in[264]}]
#set_property PACKAGE_PIN AE6  [get_ports {rpc_in[263]}]
#set_property PACKAGE_PIN AF7  [get_ports {rpc_in[262]}]
#set_property PACKAGE_PIN AG7  [get_ports {rpc_in[261]}]
#set_property PACKAGE_PIN AD9  [get_ports {rpc_in[260]}]
#set_property PACKAGE_PIN AD8  [get_ports {rpc_in[259]}]
#set_property PACKAGE_PIN AE8  [get_ports {rpc_in[258]}]
#set_property PACKAGE_PIN AE7  [get_ports {rpc_in[257]}]
#set_property PACKAGE_PIN AH7  [get_ports {rpc_in[256]}]
#set_property PACKAGE_PIN AH6  [get_ports {rpc_in[255]}]
#set_property PACKAGE_PIN AF9  [get_ports {rpc_in[254]}]
#set_property PACKAGE_PIN AF8  [get_ports {rpc_in[253]}]
#set_property PACKAGE_PIN AH9  [get_ports {rpc_in[252]}]
#set_property PACKAGE_PIN AH8  [get_ports {rpc_in[251]}]
#set_property PACKAGE_PIN AF12 [get_ports {rpc_in[250]}]
#set_property PACKAGE_PIN Y10  [get_ports {rpc_in[249]}]
#set_property PACKAGE_PIN V9   [get_ports {rpc_in[248]}]
#set_property PACKAGE_PIN M6   [get_ports {rpc_in[247]}]
#set_property PACKAGE_PIN N9   [get_ports {rpc_in[246]}]
#set_property PACKAGE_PIN M9   [get_ports {rpc_in[245]}]
#set_property PACKAGE_PIN N8   [get_ports {rpc_in[244]}]
#set_property PACKAGE_PIN N7   [get_ports {rpc_in[243]}]
#set_property PACKAGE_PIN M11  [get_ports {rpc_in[242]}]
#set_property PACKAGE_PIN M10  [get_ports {rpc_in[241]}]
#set_property PACKAGE_PIN P9   [get_ports {rpc_in[240]}]
#set_property PACKAGE_PIN P8   [get_ports {rpc_in[239]}]
#set_property PACKAGE_PIN P6   [get_ports {rpc_in[238]}]
#set_property PACKAGE_PIN N6   [get_ports {rpc_in[237]}]
#set_property PACKAGE_PIN N1   [get_ports {rpc_in[236]}]
#set_property PACKAGE_PIN M1   [get_ports {rpc_in[235]}]
#set_property PACKAGE_PIN M5   [get_ports {rpc_in[234]}]
#set_property PACKAGE_PIN M4   [get_ports {rpc_in[233]}]
#set_property PACKAGE_PIN R1   [get_ports {rpc_in[232]}]
#set_property PACKAGE_PIN P1   [get_ports {rpc_in[231]}]
#set_property PACKAGE_PIN N3   [get_ports {rpc_in[230]}]
#set_property PACKAGE_PIN N2   [get_ports {rpc_in[229]}]
#set_property PACKAGE_PIN P4   [get_ports {rpc_in[228]}]
#set_property PACKAGE_PIN P3   [get_ports {rpc_in[227]}]
#set_property PACKAGE_PIN P5   [get_ports {rpc_in[226]}]
#set_property PACKAGE_PIN N4   [get_ports {rpc_in[225]}]
#set_property PACKAGE_PIN R6   [get_ports {rpc_in[224]}]
#set_property PACKAGE_PIN R5   [get_ports {rpc_in[223]}]
#set_property PACKAGE_PIN T5   [get_ports {rpc_in[222]}]
#set_property PACKAGE_PIN T4   [get_ports {rpc_in[221]}]
#set_property PACKAGE_PIN R3   [get_ports {rpc_in[220]}]
#set_property PACKAGE_PIN R2   [get_ports {rpc_in[219]}]
#set_property PACKAGE_PIN U2   [get_ports {rpc_in[218]}]
#set_property PACKAGE_PIN U1   [get_ports {rpc_in[217]}]
#set_property PACKAGE_PIN T3   [get_ports {rpc_in[216]}]
#set_property PACKAGE_PIN T2   [get_ports {rpc_in[215]}]
#set_property PACKAGE_PIN U5   [get_ports {rpc_in[214]}]
#set_property PACKAGE_PIN U4   [get_ports {rpc_in[213]}]
#set_property PACKAGE_PIN R8   [get_ports {rpc_in[212]}]
#set_property PACKAGE_PIN R7   [get_ports {rpc_in[211]}]
#set_property PACKAGE_PIN R10  [get_ports {rpc_in[210]}]
#set_property PACKAGE_PIN P10  [get_ports {rpc_in[209]}]
#set_property PACKAGE_PIN U10  [get_ports {rpc_in[208]}]
#set_property PACKAGE_PIN T10  [get_ports {rpc_in[207]}]
#set_property PACKAGE_PIN U7   [get_ports {rpc_in[206]}]
#set_property PACKAGE_PIN U6   [get_ports {rpc_in[205]}]
#set_property PACKAGE_PIN T8   [get_ports {rpc_in[204]}]
#set_property PACKAGE_PIN T7   [get_ports {rpc_in[203]}]
#set_property PACKAGE_PIN U9   [get_ports {rpc_in[202]}]
#set_property PACKAGE_PIN T9   [get_ports {rpc_in[201]}]
#set_property PACKAGE_PIN U11  [get_ports {rpc_in[200]}]
#set_property PACKAGE_PIN V8   [get_ports {rpc_in[199]}]
#set_property PACKAGE_PIN W9   [get_ports {rpc_in[198]}]
#set_property PACKAGE_PIN P26  [get_ports {rpc_in[197]}]
#set_property PACKAGE_PIN N26  [get_ports {rpc_in[196]}]
#set_property PACKAGE_PIN M27  [get_ports {rpc_in[195]}]
#set_property PACKAGE_PIN U25  [get_ports {rpc_in[194]}]
#set_property PACKAGE_PIN T25  [get_ports {rpc_in[193]}]
#set_property PACKAGE_PIN P24  [get_ports {rpc_in[192]}]
#set_property PACKAGE_PIN N24  [get_ports {rpc_in[191]}]
#set_property PACKAGE_PIN U26  [get_ports {rpc_in[190]}]
#set_property PACKAGE_PIN U27  [get_ports {rpc_in[189]}]
#set_property PACKAGE_PIN R25  [get_ports {rpc_in[188]}]
#set_property PACKAGE_PIN P25  [get_ports {rpc_in[187]}]
#set_property PACKAGE_PIN T27  [get_ports {rpc_in[186]}]
#set_property PACKAGE_PIN R27  [get_ports {rpc_in[185]}]
#set_property PACKAGE_PIN N27  [get_ports {rpc_in[184]}]
#set_property PACKAGE_PIN N28  [get_ports {rpc_in[183]}]
#set_property PACKAGE_PIN T28  [get_ports {rpc_in[182]}]
#set_property PACKAGE_PIN R28  [get_ports {rpc_in[181]}]
#set_property PACKAGE_PIN N29  [get_ports {rpc_in[180]}]
#set_property PACKAGE_PIN M29  [get_ports {rpc_in[179]}]
#set_property PACKAGE_PIN U29  [get_ports {rpc_in[178]}]
#set_property PACKAGE_PIN T29  [get_ports {rpc_in[177]}]
#set_property PACKAGE_PIN P28  [get_ports {rpc_in[176]}]
#set_property PACKAGE_PIN P29  [get_ports {rpc_in[175]}]
#set_property PACKAGE_PIN R30  [get_ports {rpc_in[174]}]
#set_property PACKAGE_PIN P30  [get_ports {rpc_in[173]}]
#set_property PACKAGE_PIN U30  [get_ports {rpc_in[172]}]
#set_property PACKAGE_PIN T30  [get_ports {rpc_in[171]}]
#set_property PACKAGE_PIN M30  [get_ports {rpc_in[170]}]
#set_property PACKAGE_PIN M31  [get_ports {rpc_in[169]}]
#set_property PACKAGE_PIN R31  [get_ports {rpc_in[168]}]
#set_property PACKAGE_PIN P31  [get_ports {rpc_in[167]}]
#set_property PACKAGE_PIN N31  [get_ports {rpc_in[166]}]
#set_property PACKAGE_PIN M32  [get_ports {rpc_in[165]}]
#set_property PACKAGE_PIN U31  [get_ports {rpc_in[164]}]
#set_property PACKAGE_PIN U32  [get_ports {rpc_in[163]}]
#set_property PACKAGE_PIN T32  [get_ports {rpc_in[162]}]
#set_property PACKAGE_PIN R32  [get_ports {rpc_in[161]}]
#set_property PACKAGE_PIN N32  [get_ports {rpc_in[160]}]
#set_property PACKAGE_PIN N33  [get_ports {rpc_in[159]}]
#set_property PACKAGE_PIN T33  [get_ports {rpc_in[158]}]
#set_property PACKAGE_PIN R33  [get_ports {rpc_in[157]}]
#set_property PACKAGE_PIN N34  [get_ports {rpc_in[156]}]
#set_property PACKAGE_PIN M34  [get_ports {rpc_in[155]}]
#set_property PACKAGE_PIN U34  [get_ports {rpc_in[154]}]
#set_property PACKAGE_PIN T34  [get_ports {rpc_in[153]}]
#set_property PACKAGE_PIN P33  [get_ports {rpc_in[152]}]
#set_property PACKAGE_PIN P34  [get_ports {rpc_in[151]}]
#set_property PACKAGE_PIN U24  [get_ports {rpc_in[150]}]
#set_property PACKAGE_PIN V29  [get_ports {rpc_in[149]}]
#set_property PACKAGE_PIN V26  [get_ports {rpc_in[148]}]
#set_property PACKAGE_PIN V27  [get_ports {rpc_in[147]}]
#set_property PACKAGE_PIN W26  [get_ports {rpc_in[146]}]
#set_property PACKAGE_PIN Y26  [get_ports {rpc_in[145]}]
#set_property PACKAGE_PIN W28  [get_ports {rpc_in[144]}]
#set_property PACKAGE_PIN W29  [get_ports {rpc_in[143]}]
#set_property PACKAGE_PIN W25  [get_ports {rpc_in[142]}]
#set_property PACKAGE_PIN Y25  [get_ports {rpc_in[141]}]
#set_property PACKAGE_PIN Y27  [get_ports {rpc_in[140]}]
#set_property PACKAGE_PIN Y28  [get_ports {rpc_in[139]}]
#set_property PACKAGE_PIN V31  [get_ports {rpc_in[138]}]
#set_property PACKAGE_PIN V32  [get_ports {rpc_in[137]}]
#set_property PACKAGE_PIN W33  [get_ports {rpc_in[136]}]
#set_property PACKAGE_PIN W34  [get_ports {rpc_in[135]}]
#set_property PACKAGE_PIN V33  [get_ports {rpc_in[134]}]
#set_property PACKAGE_PIN V34  [get_ports {rpc_in[133]}]
#set_property PACKAGE_PIN Y32  [get_ports {rpc_in[132]}]
#set_property PACKAGE_PIN Y33  [get_ports {rpc_in[131]}]
#set_property PACKAGE_PIN W30  [get_ports {rpc_in[130]}]
#set_property PACKAGE_PIN W31  [get_ports {rpc_in[129]}]
#set_property PACKAGE_PIN Y30  [get_ports {rpc_in[128]}]
#set_property PACKAGE_PIN Y31  [get_ports {rpc_in[127]}]
#set_property PACKAGE_PIN AA30 [get_ports {rpc_in[126]}]
#set_property PACKAGE_PIN AB30 [get_ports {rpc_in[125]}]
#set_property PACKAGE_PIN AB31 [get_ports {rpc_in[124]}]
#set_property PACKAGE_PIN AB32 [get_ports {rpc_in[123]}]
#set_property PACKAGE_PIN AC33 [get_ports {rpc_in[122]}]
#set_property PACKAGE_PIN AC34 [get_ports {rpc_in[121]}]
#set_property PACKAGE_PIN AA32 [get_ports {rpc_in[120]}]
#set_property PACKAGE_PIN AA33 [get_ports {rpc_in[119]}]
#set_property PACKAGE_PIN AC31 [get_ports {rpc_in[118]}]
#set_property PACKAGE_PIN AC32 [get_ports {rpc_in[117]}]
#set_property PACKAGE_PIN AA34 [get_ports {rpc_in[116]}]
#set_property PACKAGE_PIN AB34 [get_ports {rpc_in[115]}]
#set_property PACKAGE_PIN W8   [get_ports {rpc_in[114]}]
#set_property PACKAGE_PIN AC28 [get_ports {rpc_in[113]}]
#set_property PACKAGE_PIN AC29 [get_ports {rpc_in[112]}]
#set_property PACKAGE_PIN AA27 [get_ports {rpc_in[111]}]
#set_property PACKAGE_PIN AA28 [get_ports {rpc_in[110]}]
#set_property PACKAGE_PIN AB26 [get_ports {rpc_in[109]}]
#set_property PACKAGE_PIN AB27 [get_ports {rpc_in[108]}]
#set_property PACKAGE_PIN AA29 [get_ports {rpc_in[107]}]
#set_property PACKAGE_PIN AB29 [get_ports {rpc_in[106]}]
#set_property PACKAGE_PIN AA24 [get_ports {rpc_in[105]}]
#set_property PACKAGE_PIN AA25 [get_ports {rpc_in[104]}]
#set_property PACKAGE_PIN V7   [get_ports {rpc_in[103]}]
#set_property PACKAGE_PIN AB24 [get_ports {rpc_in[102]}]
#set_property PACKAGE_PIN AB25 [get_ports {rpc_in[101]}]
#set_property PACKAGE_PIN W24  [get_ports {rpc_in[100]}]
#set_property PACKAGE_PIN AG34 [get_ports {rpc_in[99]}]
#set_property PACKAGE_PIN AD33 [get_ports {rpc_in[98]}]
#set_property PACKAGE_PIN AD34 [get_ports {rpc_in[97]}]
#set_property PACKAGE_PIN AH33 [get_ports {rpc_in[96]}]
#set_property PACKAGE_PIN AH34 [get_ports {rpc_in[95]}]
#set_property PACKAGE_PIN AE33 [get_ports {rpc_in[94]}]
#set_property PACKAGE_PIN AF33 [get_ports {rpc_in[93]}]
#set_property PACKAGE_PIN AG32 [get_ports {rpc_in[92]}]
#set_property PACKAGE_PIN AH32 [get_ports {rpc_in[91]}]
#set_property PACKAGE_PIN AE32 [get_ports {rpc_in[90]}]
#set_property PACKAGE_PIN AF32 [get_ports {rpc_in[89]}]
#set_property PACKAGE_PIN AD31 [get_ports {rpc_in[88]}]
#set_property PACKAGE_PIN AE31 [get_ports {rpc_in[87]}]
#set_property PACKAGE_PIN AD30 [get_ports {rpc_in[86]}]
#set_property PACKAGE_PIN AE30 [get_ports {rpc_in[85]}]
#set_property PACKAGE_PIN AD28 [get_ports {rpc_in[84]}]
#set_property PACKAGE_PIN AD29 [get_ports {rpc_in[83]}]
#set_property PACKAGE_PIN AG31 [get_ports {rpc_in[82]}]
#set_property PACKAGE_PIN AH31 [get_ports {rpc_in[81]}]
#set_property PACKAGE_PIN AF29 [get_ports {rpc_in[80]}]
#set_property PACKAGE_PIN AF30 [get_ports {rpc_in[79]}]
#set_property PACKAGE_PIN AG29 [get_ports {rpc_in[78]}]
#set_property PACKAGE_PIN AG30 [get_ports {rpc_in[77]}]
#set_property PACKAGE_PIN V6   [get_ports {rpc_in[76]}]
#set_property PACKAGE_PIN AH28 [get_ports {rpc_in[75]}]
#set_property PACKAGE_PIN AH29 [get_ports {rpc_in[74]}]
#set_property PACKAGE_PIN AE28 [get_ports {rpc_in[73]}]
#set_property PACKAGE_PIN AF28 [get_ports {rpc_in[72]}]
#set_property PACKAGE_PIN AD26 [get_ports {rpc_in[71]}]
#set_property PACKAGE_PIN AE26 [get_ports {rpc_in[70]}]
#set_property PACKAGE_PIN AC26 [get_ports {rpc_in[69]}]
#set_property PACKAGE_PIN AC27 [get_ports {rpc_in[68]}]
#set_property PACKAGE_PIN AG27 [get_ports {rpc_in[67]}]
#set_property PACKAGE_PIN AH27 [get_ports {rpc_in[66]}]
#set_property PACKAGE_PIN AE27 [get_ports {rpc_in[65]}]
#set_property PACKAGE_PIN Y8   [get_ports {rpc_in[64]}]
#set_property PACKAGE_PIN AF27 [get_ports {rpc_in[63]}]
#set_property PACKAGE_PIN AG26 [get_ports {rpc_in[62]}]
#set_property PACKAGE_PIN AH26 [get_ports {rpc_in[61]}]
#set_property PACKAGE_PIN AE23 [get_ports {rpc_in[60]}]
#set_property PACKAGE_PIN AF23 [get_ports {rpc_in[59]}]
#set_property PACKAGE_PIN AG24 [get_ports {rpc_in[58]}]
#set_property PACKAGE_PIN AH24 [get_ports {rpc_in[57]}]
#set_property PACKAGE_PIN AC24 [get_ports {rpc_in[56]}]
#set_property PACKAGE_PIN AD24 [get_ports {rpc_in[55]}]
#set_property PACKAGE_PIN AF25 [get_ports {rpc_in[54]}]
#set_property PACKAGE_PIN AG25 [get_ports {rpc_in[53]}]
#set_property PACKAGE_PIN AD25 [get_ports {rpc_in[52]}]
#set_property PACKAGE_PIN AE25 [get_ports {rpc_in[51]}]
#set_property PACKAGE_PIN AF24 [get_ports {rpc_in[50]}]
#set_property PACKAGE_PIN AG12 [get_ports {rpc_in[49]}]
#set_property PACKAGE_PIN AG10 [get_ports {rpc_in[48]}]
#set_property PACKAGE_PIN AG9  [get_ports {rpc_in[47]}]
#set_property PACKAGE_PIN AD11 [get_ports {rpc_in[46]}]
#set_property PACKAGE_PIN AE11 [get_ports {rpc_in[45]}]
#set_property PACKAGE_PIN AG11 [get_ports {rpc_in[44]}]
#set_property PACKAGE_PIN AH11 [get_ports {rpc_in[43]}]
#set_property PACKAGE_PIN AD10 [get_ports {rpc_in[42]}]
#set_property PACKAGE_PIN Y7   [get_ports {rpc_in[41]}]
#set_property PACKAGE_PIN W6   [get_ports {rpc_in[40]}]
#set_property PACKAGE_PIN Y6   [get_ports {rpc_in[39]}]
#set_property PACKAGE_PIN AE10 [get_ports {rpc_in[38]}]
#set_property PACKAGE_PIN AF10 [get_ports {rpc_in[37]}]
#set_property PACKAGE_PIN W1   [get_ports {rpc_in[36]}]
#set_property PACKAGE_PIN Y1   [get_ports {rpc_in[35]}]
#set_property PACKAGE_PIN V2   [get_ports {rpc_in[34]}]
#set_property PACKAGE_PIN V1   [get_ports {rpc_in[33]}]
#set_property PACKAGE_PIN Y3   [get_ports {rpc_in[32]}]
#set_property PACKAGE_PIN Y2   [get_ports {rpc_in[31]}]
#set_property PACKAGE_PIN V3   [get_ports {rpc_in[30]}]
#set_property PACKAGE_PIN W3   [get_ports {rpc_in[29]}]
#set_property PACKAGE_PIN V4   [get_ports {rpc_in[28]}]
#set_property PACKAGE_PIN W4   [get_ports {rpc_in[27]}]
#set_property PACKAGE_PIN W5   [get_ports {rpc_in[26]}]
#set_property PACKAGE_PIN Y5   [get_ports {rpc_in[25]}]
#set_property PACKAGE_PIN AA5  [get_ports {rpc_in[24]}]
#set_property PACKAGE_PIN AA4  [get_ports {rpc_in[23]}]
#set_property PACKAGE_PIN AB5  [get_ports {rpc_in[22]}]
#set_property PACKAGE_PIN AB4  [get_ports {rpc_in[21]}]
#set_property PACKAGE_PIN AB2  [get_ports {rpc_in[20]}]
#set_property PACKAGE_PIN AB1  [get_ports {rpc_in[19]}]
#set_property PACKAGE_PIN AA3  [get_ports {rpc_in[18]}]
#set_property PACKAGE_PIN AA2  [get_ports {rpc_in[17]}]
#set_property PACKAGE_PIN AC2  [get_ports {rpc_in[16]}]
#set_property PACKAGE_PIN AC1  [get_ports {rpc_in[15]}]
#set_property PACKAGE_PIN AC4  [get_ports {rpc_in[14]}]
#set_property PACKAGE_PIN AC3  [get_ports {rpc_in[13]}]
#set_property PACKAGE_PIN AA8  [get_ports {rpc_in[12]}]
#set_property PACKAGE_PIN AA7  [get_ports {rpc_in[11]}]
#set_property PACKAGE_PIN AC7  [get_ports {rpc_in[10]}]
#set_property PACKAGE_PIN AC6  [get_ports {rpc_in[9]}]
#set_property PACKAGE_PIN AB7  [get_ports {rpc_in[8]}]
#set_property PACKAGE_PIN AB6  [get_ports {rpc_in[7]}]
#set_property PACKAGE_PIN AC9  [get_ports {rpc_in[6]}]
#set_property PACKAGE_PIN AC8  [get_ports {rpc_in[5]}]
#set_property PACKAGE_PIN AA10 [get_ports {rpc_in[4]}]
#set_property PACKAGE_PIN AA9  [get_ports {rpc_in[3]}]
#set_property PACKAGE_PIN AB10 [get_ports {rpc_in[2]}]
#set_property PACKAGE_PIN AB9  [get_ports {rpc_in[1]}]
#set_property PACKAGE_PIN AB11 [get_ports {rpc_in[0]}]

