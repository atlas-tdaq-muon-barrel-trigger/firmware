set ProjectDir ./

if {[file exists $ProjectDir/Vivado]} {[file rename $ProjectDir/Vivado $ProjectDir/Vivado.[clock format [clock seconds] -format %Y-%m-%d.%H-%M-%S]]}

file mkdir $ProjectDir/Vivado

create_project DCT_BMBO $ProjectDir/Vivado -part xc7a200tffg1156-2

set_property target_language VHDL [current_project]

add_files -norecurse $ProjectDir/IP/mmcm_200_600/mmcm_200_600.xci
add_files -norecurse $ProjectDir/IP/mmcm_320/mmcm_320.xci
add_files -norecurse $ProjectDir/IP/deserialiser/deserialiser.xci
add_files -norecurse $ProjectDir/IP/fifo_16x20b/fifo_16x20b.xci
add_files -norecurse $ProjectDir/IP/fifo_16x28b/fifo_16x28b.xci
add_files -norecurse $ProjectDir/IP/SEM/SEM.xci

generate_target all [get_files $ProjectDir/IP/mmcm_200_600/mmcm_200_600.xci]
generate_target all [get_files $ProjectDir/IP/mmcm_320/mmcm_320.xci]
generate_target all [get_files $ProjectDir/IP/deserialiser/deserialiser.xci]
generate_target all [get_files $ProjectDir/IP/fifo_16x20b/fifo_16x20b.xci]
generate_target all [get_files $ProjectDir/IP/fifo_16x28b/fifo_16x28b.xci]
generate_target all [get_files $ProjectDir/IP/SEM/SEM.xci]

add_files -norecurse $ProjectDir/IP/SEM/SEM_sem_cfg.vhd
add_files -norecurse $ProjectDir/IP/SEM/SEM_sem_example.vhd
add_files -norecurse $ProjectDir/IP/SEM/SEM_sem_mon.vhd
add_files -norecurse $ProjectDir/IP/SEM/SEM_sem_mon_fifo.vhd
add_files -norecurse $ProjectDir/IP/SEM/SEM_sem_mon_piso.vhd
add_files -norecurse $ProjectDir/IP/SEM/SEM_sem_mon_sipo.vhd

add_files -norecurse $ProjectDir/RTL/top.vhd
add_files -norecurse $ProjectDir/RTL/frame_aligner.vhd
add_files -norecurse $ProjectDir/RTL/pack.vhd

add_files -fileset constrs_1 -norecurse $ProjectDir/Constraints/top.xdc

set_property SOURCE_SET sources_1 [get_filesets sim_1]
add_files -fileset sim_1 -norecurse $ProjectDir/TB/top_tb.vhd
set_property file_type {VHDL 2008} [get_files  $ProjectDir/TB/top_tb.vhd]
set_property file_type {VHDL 2008} [get_files  $ProjectDir/RTL/top.vhd]

set_property strategy Flow_PerfThresholdCarry [get_runs synth_1]
set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

set_property AUTO_INCREMENTAL_CHECKPOINT 0 [get_runs synth_1]
