------------------------------------------------------
--File:             pack.vhd
--Project:          BMBO DCT firmware
--Author:           Federico Morodei <federico.morodei@cern.ch>
--Last modified:    2023/06/28
------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



package pack is
    type array_18x4b is array(17 downto 0) of std_logic_vector(3 downto 0);
    type array_18x3b is array(17 downto 0) of std_logic_vector(2 downto 0);
    type array_288x6b  is array (287 downto 0) of std_logic_vector(5  downto 0);
    type array_288x8b  is array (287 downto 0) of std_logic_vector(7  downto 0);
    type array_288x30b is array (287 downto 0) of std_logic_vector(29 downto 0);
    type array_288x45b is array (287 downto 0) of std_logic_vector(44 downto 0);
    type array_144x20b is array (143 downto 0) of std_logic_vector(19 downto 0);
    type array_144x28b is array (143 downto 0) of std_logic_vector(27 downto 0);
    type array_144x10b is array (143 downto 0) of std_logic_vector(9  downto 0);
    type array_18x28b  is array (17  downto 0) of std_logic_vector(27 downto 0);
    type array_36x4b   is array (35  downto 0) of std_logic_vector(3  downto 0);
    type array_18x5b   is array (17  downto 0) of std_logic_vector(4  downto 0);
    type array_288x5b  is array (287 downto 0) of std_logic_vector(4  downto 0);
    type array_18x12b  is array (17  downto 0) of std_logic_vector(11 downto 0);
    type array_144x12b is array (143 downto 0) of std_logic_vector(11 downto 0);
end package;

package body pack is

end pack;
