------------------------------------------------------ 
--File:             top.vhd
--Project:          BMBO DCT firmware
--Author:           Riccardo Vari, Federico Morodei
--Last modified:    2024/12/07
------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.ALL;
use ieee.std_logic_misc.all;
Library xpm;
use xpm.vcomponents.all;


library UNISIM;
use UNISIM.vcomponents.all;

library xil_defaultlib;
use xil_defaultlib.pack.all;

library STD;
use STD.textio.all;

--  lpGBT clocks:
--
--    Phase/Frequency - 4 programmable clocks:
--      - 4 independent
--      - Phase resolution: 50 ps
--      - Frequencies: 40 / 80 / 160 / 320 / 640 / 1280 MHz
--
--    eLink Clocks:
--      - 28 independent
--      - Fixed phase
--      - Frequency programable: 40 / 80 / 160 / 320 / 640 / 1280 MHz
--
--    Clock jitter < 5 ps rms
--

entity top is
port (
    -- 40 MHz oscillator sent to fan-out chip to produce these two clocks - MRCC FPGA pins (NOTE now connected to _N should be _P)
    --CLK40_0     : in  std_logic;
    --CLK40_1     : in  std_logic;
    -- lpGBT phase shift clocks - MRCC pins
    --PSCLK0_p    : in  std_logic;
    --PSCLK0_n    : in  std_logic;
    --PSCLK1_p    : in  std_logic;
    --PSCLK1_n    : in  std_logic;
    -- lpGBT elink clock - MRCC pins
    ECLK0_p     : in  std_logic;
    ECLK0_n     : in  std_logic;
    --ECLK1_p     : in  std_logic;
    --ECLK1_n     : in  std_logic;
    -- SMA test signals - SRCC pins
    --TST_CLK0_P  : in  std_logic;
    --TST_CLK0_N  : in  std_logic;
    --TST_CLK1_P  : in  std_logic;
    --TST_CLK1_N  : in  std_logic;
    -- RPC FE inputs
    rpc_in      : in  std_logic_vector(287 downto 0);
    -- lpGBT elinks
    elink_in_p  : in  std_logic_vector(3 downto 0);   -- 320 Mb/s each
    elink_in_n  : in  std_logic_vector(3 downto 0);   -- 320 Mb/s each
    elink_out_p : out std_logic_vector(27 downto 0);  -- 320 Mb/s each
    elink_out_n : out std_logic_vector(27 downto 0);  -- 320 Mb/s each
    elink_ec_out_p : out std_logic;                   -- 80 Mb/s
    elink_ec_out_n : out std_logic                    -- 80 Mb/s
    --fifo_datacount : out array_18x5b --for simulation
    );
end top;

--- BMBO DCT data format
--- 1-bit coordinate (eta/phi) + 7-bit strip ID + 10-bit BCID + 5-bit rising time first layer + 5-bit rising time second layer

architecture RTL of top is


    --signal TST_CLK0 : std_logic;
    --signal TST_CLK1 : std_logic;

    --signal PSCLK0 : std_logic;
    --signal PSCLK1 : std_logic;
    
    signal ECLK0 : std_logic;
    --signal ECLK1 : std_logic;

    --signal clock40 : std_logic;
    signal clock40_edge : std_logic := '0';
    signal clock40_edge320_r : std_logic;
    signal clock40_edge200_r : std_logic;
    signal clock40_rising320 : std_logic;
    signal clock40_rising200 : std_logic;
    signal clock40_rising200_r : std_logic;

    signal clock600_mmcm : std_logic;
    signal clock200_mmcm : std_logic;
    signal clock320_mmcm : std_logic;
    signal clock40_mmcm  : std_logic;
    signal clock80_mmcm  : std_logic;
    signal clock100_mmcm : std_logic;
    
    signal mmcm1_locked   : std_logic;
    signal mmcm2_locked   : std_logic;
    signal mmcm_locked_r0 : std_logic;
    signal mmcm_locked_r1 : std_logic;
    
    signal int_reset        : std_logic;   
    signal int_reset200 : std_logic;   
    signal int_reset320 : std_logic;   

    signal elink_out : std_logic_vector(27 downto 0);
    signal elink_in  : std_logic_vector(3 downto 0);

    subtype integer018 is integer range 0 to 18;
    signal fifo_out_num_out : integer018;

    signal bcid      : std_logic_vector(11 downto 0);
    signal subbcid   : array_18x3b := (others => (others => '0'));
    signal subbcid_r : array_18x3b := (others => (others => '0'));
    
    signal datain_288x30b_lsb_old : std_logic_vector(287 downto 0) := (others => '0');

    signal datain_288x6b : array_288x6b := (others => (others => '0'));
    signal datain_288x30b : array_288x30b := (others => (others => '0'));

    signal time_144x10b : array_144x10b := (others => (others => '0'));
    
    signal fifo_in_din : array_144x20b := (others => (others => '0'));
    signal fifo_in_we : std_logic_vector(143 downto 0);
    signal fifo_in_re : std_logic_vector(143 downto 0);
    signal fifo_in_dout : array_144x20b := (others => (others => '0'));
    signal fifo_in_empty : std_logic_vector(143 downto 0);
    signal fifo_in_full : std_logic_vector(143 downto 0) := (others => '0');
    signal fifo_in_almost_full : std_logic_vector(143 downto 0) := (others => '0');

    signal fifo_out_din : array_18x28b := (others => (others => '0'));
    signal fifo_out_we : std_logic_vector(17 downto 0);
    signal fifo_out_re : std_logic_vector(17 downto 0);
    signal fifo_out_dout : array_18x28b := (others => (others => '0'));
    signal fifo_out_empty : std_logic_vector(17 downto 0) := (others => '1');
    signal fifo_out_full : std_logic_vector(17 downto 0) := (others => '0');
    signal fifo_out_almost_full : std_logic_vector(17 downto 0) := (others => '0');
    signal fifo_out_datacount : array_18x5b;
    
    signal fifo_in_full_or : std_logic := '0';
    signal fifo_in_almost_full_or : std_logic := '0';
    signal fifo_out_full_or : std_logic := '0';
    signal fifo_out_almost_full_or : std_logic := '0';
    signal fifo_out_full_r : std_logic_vector(17 downto 0);
    signal fifo_out_almost_full_r : std_logic_vector(17 downto 0);
    signal fifo_in_full_r : std_logic_vector(143 downto 0);
    signal fifo_in_almost_full_r : std_logic_vector(143 downto 0);
    
    signal fifo_out_re_r : std_logic_vector(17 downto 0);
    signal fifo_out_dout_r : array_18x28b := (others => (others => '0'));
        
    signal ECLK0_BUFG : std_logic;
    
    signal channel_mask   : std_logic_vector(287 downto 0) := (others => '0');  --only first 144 bits are used   
    signal timing_offset_in : array_18x5b := (others => (others => '0'));
    signal timing_offset : array_288x5b := (others => (others => '0'));
    signal coarse_time_offset : array_18x4b := (others => (others => '0'));
    signal BC_offset : std_logic_vector(5 downto 0) := (others => '0');
    signal risetime : array_288x5b;
    signal risetime_plus_offset : array_288x5b;
    signal bcid_18x12b : array_18x12b := (others => (others => '0'));
    signal mapped_bcid : array_144x12b := (others => (others => '0'));
    
    signal ctrl_word                 : std_logic_vector(31 downto 0);
    signal ctrl_word_200MHz          : std_logic_vector(31 downto 0);
    signal ctrl_word_valid           : std_logic;
    signal ctrl_word_valid_200MHz    : std_logic;
    signal ctrl_word_valid_200MHz_r  : std_logic;
    signal ctrl_header               : std_logic_vector(11 downto 0);
    signal ctrl_WR                   : std_logic_vector(1 downto 0);
    signal ctrl_reg_idx              : std_logic_vector(2 downto 0);
    signal ctrl_channel_idx          : integer range 0 to 511; 
    signal ctrl_data                 : std_logic_vector(5 downto 0);
    signal read_ctrl_word            : std_logic_vector(27 downto 0);
    signal read_ctrl_word_320MHz     : std_logic_vector(27 downto 0);
    signal read_en_ctrl_fifo         : std_logic;
    signal write_en_ctrl_fifo        : std_logic; 
    signal write_en_ctrl_fifo_320MHz : std_logic; 
    signal ctrl_fifo_dout            : std_logic_vector(27 downto 0);
    signal ctrl_fifo_empty           : std_logic;
    signal ctrl_fifo_empty_r         : std_logic := '1';
    signal prep_ctrl_data_out        : std_logic;
    signal ctrl_data_out             : std_logic_vector(27 downto 0) := (others => '0');
    signal BCreset_frame             : std_logic_vector(7 downto 0) := (others => '0');
    signal BCreset_valid             : std_logic;
    signal BCreset_320               : std_logic := '0';
    signal BCreset_200               : std_logic := '0';
    
    signal dead_time     : std_logic_vector(2 downto 0);  -- given in number of BCs
    signal dead_time_r   : std_logic_vector(2 downto 0);
    signal data_in_valid : std_logic_vector(287 downto 0) := (others => '1');
    signal n_clock_cycles_skipped : array_288x6b;
    
    constant bcid_max : std_logic_vector(11 downto 0) := x"deb"; --3563
    constant delay                   : integer := 7;
    signal BCreset200_delayed        : std_logic_vector(delay downto 0) := (others => '0');
    signal clock40_rising200_delayed : std_logic_vector(delay downto 0) := (others => '0');
    
    signal SEM_status_heartbeat         : std_logic;
    signal SEM_status_initialization    : std_logic;
    signal SEM_status_observation       : std_logic;
    signal SEM_status_correction        : std_logic;
    signal SEM_status_uncorrectable     : std_logic;
    signal SEM_word                     : std_logic_vector(1 downto 0) :=(others => '0');
    
    constant monitoring_header      : std_logic_vector(7 downto 0) := "11111111";
    signal xadc_drdy_out            : std_logic;
    signal xadc_do_out              : std_logic_vector(15 downto 0);
    signal xadc_user_temp_alarm_out : std_logic;
    signal xadc_ot_out              : std_logic;
    signal xadc_channel_out         : std_logic_vector(4 downto 0);
    signal xadc_eoc_out             : std_logic;
    signal xadc_alarm_out           : std_logic;
    signal xadc_eos_out             : std_logic;
    signal xadc_busy_out            : std_logic;
    
    signal onesec_counter_80MHz  : std_logic_vector(26 downto 0) := (others => '0');
    constant monitoring_comma    : std_logic_vector(9 downto 0) := "0011111010";
    signal monitoring_frame      : std_logic_vector(31 downto 0) := (others => '0');
    signal elink_ec_out          : std_logic;
    signal counter17             : std_logic_vector(4 downto 0) := (others => '0');     
    

--  SELECT_IO IP SETTINGS: Template: Custom; Direction: Input; Data rate: DDR; Serialization Factor: 6; External width: 1; IO type: single-ended; IO standard: LVCMOS33; Clock Setup: Internal Clock
    component deserialiser
    port (
        data_in_from_pins : in  std_logic;
        data_in_to_device : out std_logic_vector(5 downto 0);
        bitslip           : in  std_logic;
        clk_in            : in  std_logic;
        clk_div_in        : in  std_logic;
        io_reset          : in  std_logic
    );
    end component;

    component mmcm_200_600
    port (
        clk_out600 : out std_logic;
        clk_out200 : out std_logic;
        clk_out100 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

    component mmcm_320
    port (
        clk_out320 : out std_logic;
        clk_out40  : out std_logic;
        clk_out80  : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

    -- FIFP generator IP settings: common clock distributed RAM standard FIFO
    component fifo_16x28b
    port (
        clk         : in  std_logic;
        din         : in  std_logic_vector(27 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(27 downto 0);
        full        : out std_logic;
        almost_full : out std_logic;
        empty       : out std_logic;
        data_count  : out std_logic_vector(4 downto 0)
    );
    end component;


    -- FIFO generator IP settings: independent clock distributed RAM First Word Fall Through
    component fifo_16x20b  
    port (
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(19 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(19 downto 0);
        full        : out std_logic;
        almost_full : out std_logic;
        empty       : out std_logic
    );
    end component;
    
    
    component framealigner 
    generic (
        header                 : std_logic_vector(6 downto 0);
        requiredCorrectHeaders : integer range 0 to 7;
        maxFalseHeaders        : integer range 0 to 7;
        requiredCheckedHeaders : integer range 0 to 7;
        out_data_width         : integer range 0 to 32;
        counter_width          : integer range 0 to 5 -- counter must count from 0 to out_data_width
    );
    port (
        clock                  : in std_logic;
        reset                  : in std_logic;
        data_in                : in std_logic;
        data_out               : out std_logic_vector(out_data_width-1 downto 0);
        data_out_valid         : out std_logic 
    );
    end component;
    
    component SEM_sem_example is
    port (
      clk                      : in    std_logic;
      status_heartbeat         : out   std_logic;
      status_initialization    : out   std_logic;
      status_observation       : out   std_logic;
      status_correction        : out   std_logic;
      status_classification    : out   std_logic;
      status_injection         : out   std_logic;
      status_essential         : out   std_logic;
      status_uncorrectable     : out   std_logic;
      monitor_tx               : out   std_logic;
      monitor_rx               : in    std_logic
    );
    end component;
    
    COMPONENT xadc_ip
    PORT (
      di_in               : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      daddr_in            : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
      den_in              : IN STD_LOGIC;
      dwe_in              : IN STD_LOGIC;
      drdy_out            : OUT STD_LOGIC;
      do_out              : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      dclk_in             : IN STD_LOGIC;
      reset_in            : IN STD_LOGIC;
      vp_in               : IN STD_LOGIC;
      vn_in               : IN STD_LOGIC;
      user_temp_alarm_out : OUT STD_LOGIC;
      ot_out              : OUT STD_LOGIC;
      channel_out         : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      eoc_out             : OUT STD_LOGIC;
      alarm_out           : OUT STD_LOGIC;
      eos_out             : OUT STD_LOGIC;
      busy_out            : OUT STD_LOGIC 
    );
    END COMPONENT;
    
    
    
    function data2risetime (slv_in : std_logic_vector(30 downto 0)) return std_logic_vector is
    begin
        if    slv_in( 1 downto  0) = "10" then return "00001"; -- LSB is the oldest
        elsif slv_in( 2 downto  1) = "10" then return "00010";
        elsif slv_in( 3 downto  2) = "10" then return "00011";
        elsif slv_in( 4 downto  3) = "10" then return "00100";
        elsif slv_in( 5 downto  4) = "10" then return "00101";
        elsif slv_in( 6 downto  5) = "10" then return "00110";
        elsif slv_in( 7 downto  6) = "10" then return "00111";
        elsif slv_in( 8 downto  7) = "10" then return "01000";
        elsif slv_in( 9 downto  8) = "10" then return "01001";
        elsif slv_in(10 downto  9) = "10" then return "01010";
        elsif slv_in(11 downto 10) = "10" then return "01011";
        elsif slv_in(12 downto 11) = "10" then return "01100";
        elsif slv_in(13 downto 12) = "10" then return "01101";
        elsif slv_in(14 downto 13) = "10" then return "01110";
        elsif slv_in(15 downto 14) = "10" then return "01111";
        elsif slv_in(16 downto 15) = "10" then return "10000";
        elsif slv_in(17 downto 16) = "10" then return "10001";
        elsif slv_in(18 downto 17) = "10" then return "10010";
        elsif slv_in(19 downto 18) = "10" then return "10011";
        elsif slv_in(20 downto 19) = "10" then return "10100";
        elsif slv_in(21 downto 20) = "10" then return "10101";
        elsif slv_in(22 downto 21) = "10" then return "10110";
        elsif slv_in(23 downto 22) = "10" then return "10111";
        elsif slv_in(24 downto 23) = "10" then return "11000";
        elsif slv_in(25 downto 24) = "10" then return "11001";
        elsif slv_in(26 downto 25) = "10" then return "11010";
        elsif slv_in(27 downto 26) = "10" then return "11011";
        elsif slv_in(28 downto 27) = "10" then return "11100";
        elsif slv_in(29 downto 28) = "10" then return "11101";
        elsif slv_in(30 downto 29) = "10" then return "11110";
        else return "00000";
        end if;
    end data2risetime;

--    function data2falltime (slv_in : std_logic_vector(30 downto 0)) return std_logic_vector is
--    begin
--        if    slv_in( 1 downto  0) = "01" then return "00001"; -- LSB is the oldest
--        elsif slv_in( 2 downto  1) = "01" then return "00010";
--        elsif slv_in( 3 downto  2) = "01" then return "00011";
--        elsif slv_in( 4 downto  3) = "01" then return "00100";
--        elsif slv_in( 5 downto  4) = "01" then return "00101";
--        elsif slv_in( 6 downto  5) = "01" then return "00110";
--        elsif slv_in( 7 downto  6) = "01" then return "00111";
--        elsif slv_in( 8 downto  7) = "01" then return "01000";
--        elsif slv_in( 9 downto  8) = "01" then return "01001";
--        elsif slv_in(10 downto  9) = "01" then return "01010";
--        elsif slv_in(11 downto 10) = "01" then return "01011";
--        elsif slv_in(12 downto 11) = "01" then return "01100";
--        elsif slv_in(13 downto 12) = "01" then return "01101";
--        elsif slv_in(14 downto 13) = "01" then return "01110";
--        elsif slv_in(15 downto 14) = "01" then return "01111";
--        elsif slv_in(16 downto 15) = "01" then return "10000";
--        elsif slv_in(17 downto 16) = "01" then return "10001";
--        elsif slv_in(18 downto 17) = "01" then return "10010";
--        elsif slv_in(19 downto 18) = "01" then return "10011";
--        elsif slv_in(20 downto 19) = "01" then return "10100"; 
--        elsif slv_in(21 downto 20) = "01" then return "10101";
--        elsif slv_in(22 downto 21) = "01" then return "10110";
--        elsif slv_in(23 downto 22) = "01" then return "10111";
--        elsif slv_in(24 downto 23) = "01" then return "11000";
--        elsif slv_in(25 downto 24) = "01" then return "11001";
--        elsif slv_in(26 downto 25) = "01" then return "11010";
--        elsif slv_in(27 downto 26) = "01" then return "11011";
--        elsif slv_in(28 downto 27) = "01" then return "11100";
--        elsif slv_in(29 downto 28) = "01" then return "11101";
--        elsif slv_in(30 downto 29) = "01" then return "11110";
--        else return "00000";
--        end if;
--    end data2falltime;

begin

    --fifo_datacount <= fifo_out_datacount;

    elink_out_diff_buff_gen : for i in 27 downto 0 generate
        OBUFDS_inst : OBUFDS
        generic map (
            IOSTANDARD => "DIFF_HSUL_12",
            SLEW       => "SLOW")   -- Specify the output slew rate
        port map (
            O  => elink_out_p(i),
            OB => elink_out_n(i),
            I  => elink_out(i)
        );
    end generate;

    elink_in_diff_buff_gen : for i in 3 downto 0 generate
        IBUFDS_inst : IBUFDS
        generic map (
            IOSTANDARD => "DIFF_HSUL_12")
        port map (
            I  => elink_in_p(i),
            IB => elink_in_n(i),
            O  => elink_in(i)
        );
    end generate;
    
    
    OBUFDS_ec_elink_out_inst : OBUFDS
    generic map (
        IOSTANDARD => "DIFF_HSUL_12",
        SLEW       => "SLOW")   -- Specify the output slew rate
    port map (
        O  => elink_ec_out_p,
        OB => elink_ec_out_n,
        I  => elink_ec_out
    );



    ECLK0_diff_buff : IBUFDS
    generic map (
        DIFF_TERM    => FALSE,
        IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
        IOSTANDARD   => "DIFF_HSUL_12")
    port map (
        I  => ECLK0_p,
        IB => ECLK0_n,
        O  => ECLK0
    );


    clock40_bufg_inst : BUFG
    port map (
      O => ECLK0_BUFG,
      I => ECLK0
    );

    mmcm_for_deserialisers_inst : mmcm_200_600
    port map (
        clk_out600 => clock600_mmcm,
        clk_out200 => clock200_mmcm,
        clk_out100 => clock100_mmcm,
        locked     => mmcm1_locked,
        clk_in40   => ECLK0_BUFG
    );

    mmcm_for_logic_inst : mmcm_320
    port map (
        clk_out320 => clock320_mmcm,
        clk_out40  => clock40_mmcm,
        clk_out80  => clock80_mmcm,
        locked     => mmcm2_locked,
        clk_in40   => ECLK0_BUFG
    );
    
    
    int_reset_logic : process(clock40_mmcm) begin
        if rising_edge(clock40_mmcm) then
            mmcm_locked_r0 <= mmcm1_locked and mmcm2_locked;
            mmcm_locked_r1 <= mmcm_locked_r0;
            if mmcm_locked_r1 = '1' then
                int_reset <= '0';
            else
                int_reset <= '1';
            end if;
        end if;
    end process;
    
    xpm_cdc_sync_rst200_inst : xpm_cdc_sync_rst
    generic map (
       DEST_SYNC_FF => 2,   
       INIT => 1,          
       INIT_SYNC_FF => 0,   
       SIM_ASSERT_CHK => 0  
    )
    port map (
       dest_rst => int_reset200, 
       dest_clk => clock200_mmcm,
       src_rst => int_reset
    );
    
    xpm_cdc_sync_rst320_inst : xpm_cdc_sync_rst
    generic map (
       DEST_SYNC_FF => 4,   
       INIT => 1,          
       INIT_SYNC_FF => 0,   
       SIM_ASSERT_CHK => 0  
    )
    port map (
       dest_rst => int_reset320, 
       dest_clk => clock320_mmcm,
       src_rst => int_reset
    );


    clock40_edge_logic : process(clock40_mmcm) begin
        if rising_edge(clock40_mmcm) then
            clock40_edge <= not clock40_edge;
        end if;
    end process;

    clock40_rising320_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            clock40_edge320_r <= clock40_edge;
            if clock40_edge320_r /= clock40_edge then
                clock40_rising320 <= '1';
            else
                clock40_rising320 <= '0';
            end if;
        end if;
    end process;

    clock40_rising200_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            clock40_edge200_r <= clock40_edge;
            if clock40_edge200_r /= clock40_edge then
                clock40_rising200 <= '1';
            else
                clock40_rising200 <= '0';
            end if;
        end if;
    end process;
    
    
    delay_reset : process(clock200_mmcm) begin
         if rising_edge(clock200_mmcm) then
            BCreset200_delayed <= BCreset200_delayed(delay-1 downto 0) & BCreset_200; --most significant bit is the oldest
            clock40_rising200_delayed <= clock40_rising200_delayed(delay-1 downto 0) & clock40_rising200;
         end if;
    end process;
    
    
    bcid_counter_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if BCreset_200 = '1' then 
                bcid <= "000000" & BC_offset;
            else
                if clock40_rising200 = '1' then
                    if bcid = bcid_max then
                        bcid <= "000000" & BC_offset;
                    else 
                        bcid <= bcid + 1;
                    end if;
                end if;
            end if;
            
            
            for i in 17 downto 0 loop
                if BCreset200_delayed(delay) = '1' then  
                    bcid_18x12b(i) <= ("000000" & BC_offset) + ("00000000" & coarse_time_offset(i));
                else
                    if clock40_rising200_delayed(delay) = '1' then
                        if bcid_18x12b(i) = bcid_max then 
                            bcid_18x12b(i) <= ("000000" & BC_offset) + ("00000000" & coarse_time_offset(i));
                        else
                            bcid_18x12b(i) <= bcid_18x12b(i) +1;
                        end if;
                    end if;
                end if;
            end loop;
        end if;
    end process;

 

    subbcid_counter_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            for i in 17 downto 0 loop
            subbcid_r(i) <= subbcid(i);
                if clock40_rising320 = '1' then
                    subbcid(i) <= "000";
                else
                    subbcid(i) <= subbcid(i) + 1;
                end if;
            end loop;
        end if;
    end process;

    deserialisers : for i in 287 downto 0 generate
        deser_inst : deserialiser
        port map (
            data_in_from_pins => rpc_in(i),
            data_in_to_device => datain_288x6b(i),   -- BIT0 is the OLDEST
            bitslip           => '0',
            clk_in            => clock600_mmcm,
            clk_div_in        => clock200_mmcm,
            io_reset          => int_reset200
        );
    end generate;


    shift_reg_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 287 downto 0 loop
                datain_288x30b(i) <= datain_288x6b(i) & datain_288x30b(i)(29 downto 6);  -- LSB is the OLDEST
            end loop;
        end if;
    end process;
   

     timing_offset_mapping : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 17 downto 0 loop
                for k in 15 downto 0 loop
                    timing_offset(k+16*i) <= timing_offset_in(i);
                end loop;
            end loop;
        end if;
     end process;
     
     dead_time_sampling : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            dead_time_r <= dead_time;
        end if;
    end process;
    

    time_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if clock40_rising200 = '1' then
                for i in 287 downto 0 loop
                    datain_288x30b_lsb_old(i) <= datain_288x30b(i)(29);
                    risetime(i) <= data2risetime(datain_288x30b(i) & datain_288x30b_lsb_old(i));
                end loop;
                
                bcid_mapping : for i in 17 downto 0 loop
                    for k in 7 downto 0 loop
                        mapped_bcid(k+8*i) <= bcid_18x12b(i); -- assume both layers for same (eta,phi) coordinates have same coarse timing offset 
                    end loop;
                end loop;
            end if;
        end if;
    end process;
    
    apply_offset_and_deadtime : process(clock200_mmcm)
    begin  
        if rising_edge(clock200_mmcm) then      
            if int_reset200 = '1' then 
                data_in_valid <= (others => '1');
            else
                for i in 287 downto 0 loop
                    if data_in_valid(i) = '0' then  -- don't accept hits for the dead time
                        n_clock_cycles_skipped(i) <= n_clock_cycles_skipped(i) + 1;
                        if clock40_rising200_r = '1' then
                            risetime_plus_offset(i) <= "00000";
                        end if;
                        if n_clock_cycles_skipped(i) = (to_integer(unsigned(dead_time_r)) * 5) - 1 then
                            data_in_valid(i) <= '1';
                        end if;
                    else
                        if risetime(i) = 0 then
                            risetime_plus_offset(i) <= "00000";
                        else    
                            if ((to_integer(unsigned(risetime(i))) + to_integer(unsigned(timing_offset(i))))) > 30 then
                                risetime_plus_offset(i) <= risetime(i) + timing_offset(i) + 2; --mod32 is done by 5-bit length
                            else
                                risetime_plus_offset(i) <= risetime(i) + timing_offset(i);
                            end if;
                            if dead_time_r /= 0 then
                                data_in_valid(i) <= '0';
                            end if;
                            n_clock_cycles_skipped(i) <= (others => '0'); -- start counting clock cycles to be skipped for dead time
                        end if;
                    end if;
                end loop;
            end if;
        end if;
    end process;
    
    
    
    merge_layers : for i in 143 downto 0 generate
        time_144x10b(i) <= risetime_plus_offset(i) & risetime_plus_offset(i + 144);
    end generate;
    

    fifo_in_we_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 143 downto 0 loop
                fifo_in_din(i) <= mapped_bcid(i)(9 downto 0) & time_144x10b(i);  
            end loop;
            clock40_rising200_r <= clock40_rising200;
            if clock40_rising200_r = '1' then
                for i in 143 downto 0 loop
                    if time_144x10b(i) /= "0000000000"  and int_reset200 = '0' then  -- zero suppression
                        fifo_in_we(i) <= not channel_mask(i);  -- channel mask: config reg to switch off DCT channels if needed
                    end if;
                end loop;
            else
                 fifo_in_we <= (others => '0');
            end if;
        end if;
    end process;
    

    fifo_in_gen : for i in 0 to 143 generate
        fifo_in: fifo_16x20b
        port map (
            wr_clk      => clock200_mmcm,
            rd_clk      => clock320_mmcm,
            din         => fifo_in_din(i),
            wr_en       => fifo_in_we(i),
            rd_en       => fifo_in_re(i),
            dout        => fifo_in_dout(i),
            full        => fifo_in_full(i),
            almost_full => fifo_in_almost_full(i),                  
            empty       => fifo_in_empty(i)
        );
    end generate;

    fifo_out_gen : for i in 0 to 17 generate
        fifo_out : fifo_16x28b
        port map (
            clk         => clock320_mmcm,
            din         => fifo_out_din(i),
            wr_en       => fifo_out_we(i),
            rd_en       => fifo_out_re(i),
            dout        => fifo_out_dout(i),
            full        => fifo_out_full(i),
            almost_full => fifo_out_almost_full(i),      
            empty       => fifo_out_empty(i),
            data_count  => fifo_out_datacount(i)
        );
    end generate;

    fifo_in_re_gen : for i in 0 to 17 generate
        fifo_in_re_logic : process(clock320_mmcm)
        begin
            if rising_edge(clock320_mmcm) then
                if subbcid(i) = "000" and fifo_in_empty(i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"01";
                elsif subbcid(i) = "001" and fifo_in_empty(1+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"02";
                elsif subbcid(i) = "010" and fifo_in_empty(2+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"04";
                elsif subbcid(i) = "011" and fifo_in_empty(3+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"08";
                elsif subbcid(i) = "100" and fifo_in_empty(4+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"10";
                elsif subbcid(i) = "101" and fifo_in_empty(5+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"20";
                elsif subbcid(i) = "110" and fifo_in_empty(6+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"40";
                elsif subbcid(i) = "111" and fifo_in_empty(7+i*8) = '0' and fifo_out_almost_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"80";
                else
                    fifo_in_re(7+i*8 downto i*8) <= X"00";
                end if;
            end if;
        end process;
    end generate;

    fifo_out_we_gen : for i in 0 to 17 generate
        fifo_out_we_logic : process(clock320_mmcm) begin
            if rising_edge(clock320_mmcm) then
                if fifo_in_re(i*8 + to_integer(unsigned(subbcid_r(i)))) = '1' then
                    fifo_out_din(i)(19 downto 0) <= fifo_in_dout(i*8 + to_integer(unsigned(subbcid_r(i))));
                    if (i*8 + to_integer(unsigned(subbcid_r(i)))) < 64 then
                        fifo_out_din(i)(27 downto 20) <= '0' & std_logic_vector(to_unsigned(i*8 + to_integer(unsigned(subbcid_r(i))), 7));
                    else
                        fifo_out_din(i)(27 downto 20) <= '1' & std_logic_vector(to_unsigned(i*8 + to_integer(unsigned(subbcid_r(i))) - 64, 7));
                    end if;
                    fifo_out_we(i) <= '1';
                else
                    fifo_out_we(i) <= '0';
                end if;
            end if;
        end process;
    end generate;
    
    fifo_out_re_logic : process(clock320_mmcm)
    begin
        if rising_edge(clock320_mmcm) then
            fifo_out_re <= (others => '0'); --default value
            for i in 17 downto 0 loop         
                if fifo_out_empty(i) = '0' then   
                    fifo_out_re(i) <='1';
                    exit; --exit the loop once the condition is met
                end if;
            end loop;
        end if;
    end process;


    fifo_out_num_out_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            fifo_out_re_r <= fifo_out_re;
            fifo_out_dout_r <= fifo_out_dout;
            if fifo_out_re_r(17) = '1' then
                fifo_out_num_out <= 17;
            elsif fifo_out_re_r(16) = '1' then
                fifo_out_num_out <= 16;
            elsif fifo_out_re_r(15) = '1' then
                fifo_out_num_out <= 15;
            elsif fifo_out_re_r(14) = '1' then
                fifo_out_num_out <= 14;
            elsif fifo_out_re_r(13) = '1' then
                fifo_out_num_out <= 13;
            elsif fifo_out_re_r(12) = '1' then
                fifo_out_num_out <= 12;
            elsif fifo_out_re_r(11) = '1' then
                fifo_out_num_out <= 11;
            elsif fifo_out_re_r(10) = '1' then
                fifo_out_num_out <= 10;
            elsif fifo_out_re_r(9) = '1' then
                fifo_out_num_out <= 9;
            elsif fifo_out_re_r(8) = '1' then
                fifo_out_num_out <= 8;
            elsif fifo_out_re_r(7) = '1' then
                fifo_out_num_out <= 7;
            elsif fifo_out_re_r(6) = '1' then
                fifo_out_num_out <= 6;
            elsif fifo_out_re_r(5) = '1' then
                fifo_out_num_out <= 5;
            elsif fifo_out_re_r(4) = '1' then
                fifo_out_num_out <= 4;
            elsif fifo_out_re_r(3) = '1' then
                fifo_out_num_out <= 3;
            elsif fifo_out_re_r(2) = '1' then
                fifo_out_num_out <= 2;
            elsif fifo_out_re_r(1) = '1' then
                fifo_out_num_out <= 1;
            elsif fifo_out_re_r(0) = '1' then
                fifo_out_num_out <= 0;
            else
                fifo_out_num_out <= 18;
            end if;
        end if;
    end process;

    elink_out_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            if fifo_out_num_out < 18 then   
                elink_out <= fifo_out_dout_r(fifo_out_num_out);
                read_en_ctrl_fifo <= '0';  
                prep_ctrl_data_out <= '0';
            else
                prep_ctrl_data_out <= '1';
                elink_out <= ctrl_data_out;
            end if;
            
            if prep_ctrl_data_out = '1' then
                ctrl_fifo_empty_r <= ctrl_fifo_empty;
                if ctrl_fifo_empty_r = '0' then
                    ctrl_data_out <= ctrl_fifo_dout;
                    read_en_ctrl_fifo <= '1';
                else
                    ctrl_data_out <= X"5555555"; -- for simulation
                    read_en_ctrl_fifo <= '0';
                end if;
            end if;
        end if;
    end process;
    
    
    ----- Logic to receive BCreset -------------
    
    BCreset_framealigner_inst : framealigner  
    generic map(
        header                 => "1010101",
        requiredCorrectHeaders => 5,
        maxFalseHeaders        => 3,
        requiredCheckedHeaders => 4,
        out_data_width         => 8,
        counter_width          => 3
    )
    port map(
        clock                  => clock320_mmcm,
        reset                  => int_reset320,
        data_in                => elink_in(1),
        data_out               => BCreset_frame,
        data_out_valid         => BCreset_valid
    );
    
    
    read_BCreset : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            if BCreset_valid = '1' then
                BCreset_320 <= BCreset_frame(0);
            end if;
        end if;
    end process;
    
    BCreset_cdc_320to200MHz : xpm_cdc_single
     generic map (
        DEST_SYNC_FF   => 2,   
        INIT_SYNC_FF   => 0,   
        SIM_ASSERT_CHK => 0,  
        SRC_INPUT_REG  => 1
     )
     port map (
        dest_out => BCreset_200, 
        dest_clk => clock200_mmcm,     
        src_clk => clock320_mmcm,       
        src_in => BCreset_320   
     );       

 
    ----- Logic to read and write configuration registers -------------

    ctrl_framealigner_inst : framealigner  
    generic map(
        header                 => "1010101",
        requiredCorrectHeaders => 5,
        maxFalseHeaders        => 3,
        requiredCheckedHeaders => 4,
        out_data_width         => 32,
       counter_width           => 5
    )
    port map(
        clock                  => clock320_mmcm,
        reset                  => int_reset320,
        data_in                => elink_in(0),
        data_out               => ctrl_word,
        data_out_valid         => ctrl_word_valid
    );
 
    ctrl_word_cdc_320to200MHz : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,
        INIT_SYNC_FF   => 0,
        SIM_ASSERT_CHK => 0, 
        SRC_INPUT_REG  => 1,
        WIDTH          => 32
    )
    port map (
        dest_out => ctrl_word_200MHz,
        dest_clk => clock200_mmcm,
        src_clk  => clock320_mmcm,
        src_in   => ctrl_word
    );
    
    ctrl_word_valid_cdc_inst : xpm_cdc_pulse
     generic map (
        DEST_SYNC_FF => 2,   
        INIT_SYNC_FF => 1,   
        REG_OUTPUT => 1,     
        RST_USED => 0,       
        SIM_ASSERT_CHK => 1  
     )
     port map (
        dest_pulse => ctrl_word_valid_200MHz, 
        dest_clk => clock200_mmcm,     
        dest_rst =>'0',     
        src_clk => clock320_mmcm,       
        src_pulse => ctrl_word_valid,   
        src_rst => '0'        
     );       
    
    
    WR_conf_registers_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if int_reset200 = '1' then
                channel_mask          <= (others => '0');
                BC_offset             <= (others => '0');
                timing_offset_in      <= (others => (others => '0'));
                coarse_time_offset <= (others => (others => '0'));
                read_ctrl_word        <= (others => '0');
                dead_time             <= (others => '0');
            else
                ctrl_word_valid_200MHz_r <= ctrl_word_valid_200MHz;
                ctrl_header      <= ctrl_word_200MHz(31 downto 20);
                ctrl_WR          <= ctrl_word_200MHz(19 downto 18);
                ctrl_reg_idx     <= ctrl_word_200MHz(17 downto 15);
                ctrl_channel_idx <= to_integer(unsigned(ctrl_word_200MHz(14 downto 6)));
                ctrl_data        <= ctrl_word_200MHz(5 downto 0);
                if ctrl_word_valid_200MHz_r = '1' then 
                    if ctrl_WR = "10" then -- read from elink in
                        if ctrl_reg_idx = "000" and ctrl_channel_idx < 288 then  -- channel mask
                            channel_mask(ctrl_channel_idx) <= ctrl_data(0);
                        end if;
                        if ctrl_reg_idx = "001" then -- BC offset
                            BC_offset <= ctrl_data(5 downto 0);
                        end if;
                        if ctrl_reg_idx = "010" and ctrl_channel_idx < 18 then -- timing offset
                            timing_offset_in(ctrl_channel_idx) <= ctrl_data(4 downto 0);
                        end if;
                        if ctrl_reg_idx = "011"  and ctrl_channel_idx < 18 then -- coarse timing offset
                            coarse_time_offset(ctrl_channel_idx) <= ctrl_data(3 downto 0);
                        end if;
                        if ctrl_reg_idx = "100" then --dead time
                            dead_time <= ctrl_data(2 downto 0);
                        end if;
                        write_en_ctrl_fifo <= '0';
                    end if;
                    if ctrl_WR = "01" then -- write on elink out
                        if ctrl_reg_idx = "000" and ctrl_channel_idx < 288 then  -- channel mask
                            read_ctrl_word <= monitoring_header & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "0000000" & channel_mask(ctrl_channel_idx);
                        end if;
                        if ctrl_reg_idx = "001" then -- BC offset
                            read_ctrl_word <= monitoring_header & ctrl_reg_idx & "00000000000" & BC_offset;
                        end if;
                        if ctrl_reg_idx = "010" and ctrl_channel_idx < 18 then -- timing offset
                            read_ctrl_word <= monitoring_header& ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "000" & timing_offset_in(ctrl_channel_idx);
                        end if;
                        if ctrl_reg_idx = "011"  and ctrl_channel_idx < 18 then -- coarse timing offset
                            read_ctrl_word <= monitoring_header & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "0000" & coarse_time_offset(ctrl_channel_idx);
                        end if; 
                        if ctrl_reg_idx = "100" then --dead time
                            read_ctrl_word <= monitoring_header & ctrl_reg_idx & "00000000000000" & dead_time;
                        end if; 
                        write_en_ctrl_fifo <= '1';
                    else 
                        write_en_ctrl_fifo <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    read_ctrl_word_cdc_200to320MHz : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,
        INIT_SYNC_FF   => 0,
        SIM_ASSERT_CHK => 0, 
        SRC_INPUT_REG  => 1,
        WIDTH          => 28
    )
    port map (
        dest_out => read_ctrl_word_320MHz,
        dest_clk => clock320_mmcm,
        src_clk  => clock200_mmcm,
        src_in   => read_ctrl_word
    );
    
    write_en_ctrl_fifo_cdc_inst : xpm_cdc_pulse
     generic map (
        DEST_SYNC_FF => 2,   
        INIT_SYNC_FF => 1,   
        REG_OUTPUT => 1,     
        RST_USED => 0,       
        SIM_ASSERT_CHK => 1  
     )
     port map (
        dest_pulse => write_en_ctrl_fifo_320MHz, 
        dest_clk => clock320_mmcm,     
        dest_rst =>'0',     
        src_clk => clock200_mmcm,       
        src_pulse => write_en_ctrl_fifo,   
        src_rst => '0'        
     );   
     
     
     ctrl_fifo_out : fifo_16x28b
    port map (
        clk        => clock320_mmcm,
        din        => read_ctrl_word_320MHz,
        wr_en      => write_en_ctrl_fifo_320MHz,
        rd_en      => read_en_ctrl_fifo,
        dout       => ctrl_fifo_dout,
        full       => open,
        empty      => ctrl_fifo_empty,
        data_count => open
    );
    
    
    
     ----- End of logic to read and write configuration registers -------------
     
     ----- Soft Error Mitigation (SEM) ------------------------------
     
     SEM_inst : SEM_sem_example
     port map (
       clk                      => clock80_mmcm,
       status_heartbeat         => SEM_status_heartbeat,
       status_initialization    => SEM_status_initialization,
       status_observation       => SEM_status_observation,
       status_correction        => SEM_status_correction,
       status_classification    => open,
       status_injection         => open,
       status_essential         => open,
       status_uncorrectable     => SEM_status_uncorrectable,
       monitor_tx               => open,
       monitor_rx               => '1'
       );
    
   
    
    
    process(clock80_mmcm) begin
        if rising_edge(clock80_mmcm) then
            if onesec_counter_80MHz = X"4C71CF4" then
                if SEM_status_correction = '1' then
                    SEM_word(0) <='1';
                else
                    SEM_word(0) <='0';
                end if;
                if SEM_status_uncorrectable = '1' then
                    SEM_word(1) <= '1';
                else
                    SEM_word(1) <= '0';
                end if;
            else 
                if SEM_status_correction = '1' then
                    SEM_word(0) <='1';
                end if;
                if SEM_status_uncorrectable = '1' then
                    SEM_word(1) <= '1';
                end if;
            end if;
        end if;
    end process;
 

    
    ------- FPGA temperature -------------------------------
    
    xadc_inst : xadc_ip  -- comment this for simulation
    PORT MAP (
      di_in => (others => '0'),
      daddr_in => (others => '0'),
      den_in => '1',
      dwe_in => '0',
      drdy_out => xadc_drdy_out,
      do_out => xadc_do_out, -- FPGA temperature (Celsius): (ADC do_out x 503.975 / 4096) - 273.15
      dclk_in => clock80_mmcm,
      reset_in => '0',
      vp_in => '0',
      vn_in => '0',
      user_temp_alarm_out => xadc_user_temp_alarm_out,
      ot_out => xadc_ot_out,
      channel_out => xadc_channel_out,
      eoc_out => xadc_eoc_out,
      alarm_out => xadc_alarm_out,
      eos_out => xadc_eos_out,
      busy_out => xadc_busy_out
    );
    
    
    ------------------------------------------------------------------
    
    ------ SCA elink (80 MHz) for monitoring data ----------------------------
        
    -- 1 sec counter @ 80.15794 MHz clock frequency
    onesec_counter_80MHz_logic : process(clock80_mmcm)
    begin
        if rising_edge(clock80_mmcm) then 
            if onesec_counter_80MHz = X"4C71CF4" then 
                onesec_counter_80MHz <= (others => '0');
            else 
                onesec_counter_80MHz <= onesec_counter_80MHz + 1;
            end if;
        end if;    
    end process;
       
    
    fifo_flag_logic : process(clock80_mmcm) begin
        if rising_edge(clock80_mmcm) then
            fifo_out_full_r <=fifo_out_full;
            fifo_out_almost_full_r <= fifo_out_almost_full;
            if onesec_counter_80MHz = X"131C73D0" then
                fifo_out_full_or <= '0';
                fifo_out_almost_full_or <= '0';
                fifo_in_full_or <= '0';
                fifo_in_almost_full_or <= '0';
            else 
                if or_reduce(fifo_out_full_r) then
                    fifo_out_full_or <='1';
                end if;
                if or_reduce(fifo_out_almost_full_r) then
                    fifo_out_almost_full_or <='1';
                end if;
                if or_reduce(fifo_in_full_r) then
                    fifo_in_full_or <='1';
                end if;
                if or_reduce(fifo_in_almost_full_r) then
                    fifo_in_almost_full_or <='1';
                end if;
            end if;
        end if;
    end process;
    
    
    fifo_in_full_cdc_200to80MHz : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,
        INIT_SYNC_FF   => 0,
        SIM_ASSERT_CHK => 0, 
        SRC_INPUT_REG  => 1,
        WIDTH          => 144
    )
    port map (
        dest_out => fifo_in_full_r,
        dest_clk => clock80_mmcm,
        src_clk  => clock200_mmcm,
        src_in   => fifo_in_full
    );
    
    fifo_in_almost_full_cdc_200to80MHz : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,
        INIT_SYNC_FF   => 0,
        SIM_ASSERT_CHK => 0, 
        SRC_INPUT_REG  => 1,
        WIDTH          => 144
    )
    port map (
        dest_out => fifo_in_almost_full_r,
        dest_clk => clock80_mmcm,
        src_clk  => clock200_mmcm,
        src_in   => fifo_in_almost_full
    );
    
    monitoring_frame <= monitoring_comma & SEM_word & xadc_do_out & fifo_out_full_or & fifo_out_almost_full_or & fifo_in_full_or & fifo_in_almost_full_or;
    
    
    serialise_monitoring_data : process(clock80_mmcm)
    begin
        if rising_edge(clock80_mmcm) then
            if onesec_counter_80MHz < 32 then
                elink_ec_out <= monitoring_frame(31-to_integer(unsigned(onesec_counter_80MHz)));
            else
                elink_ec_out <= '0';
            end if;
        end if;
    end process;
    
    
end RTL;
