# BMBO DCT firmware

To create the BMBO DCT firmware project from scratch with Vivado, write the following line on the TCL console:  

```
source CREATE_PROJECT.tcl
```

**N.B.**: the project has been developed using Vivado 2023.2