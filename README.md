****L0 Muon Barrel Sector Logic repository****



- [SL_firmware](/SL_firmware): complete project of the Barrel SL FPGA firmware
- [DCT_BMBO_firmware](/DCT_BMBO_firmware): project of the BM/BO-DCT FPGA firmware
- [BI_DCT_firmware](/BI_DCT_firmware): project of the BI-DCT FPGA firmware
- [SL_SpecificationsDocument](/SL_SpecificationsDocument): folder for the Barrel SL Specifications Document
- [DCT_specifications](/DCT_specifications): folder for the BMBO-DCT Specification Document
- [SL_board_schematics](/SL_board_schematics): schematics of the first and second version of the SL board prototype
- [SL_prototype_guide](/SL_prototype_guide): guide to install, activate and program the SL board prototype in the Laser Room at P1
- [OLD_FILES](/OLD_FILES): old projects now merged in the complete SL firmware project
