#!/usr/bin/env python
# -*- coding: utf-8 -*-

#for i in range(15):
#    print "                    hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d,  hit_%03d," %(10*i,10*i+1,10*i+2,10*i+3,10*i+4,10*i+5,10*i+6,10*i+7,10*i+8,10*i+9)

for i in range(144):
    print "                when hit_%03d =>" %i
    print "                    if    data_fifo_dout(%d)(24) = '1' then" %i
    print "                        elink_out(23 downto 0) <= data_fifo_dout(%d)(23 downto 0);" %i
    if i+1 <= 143:
        print "                        state <= hit_%03d;" %(i+1)
    elif i+1 == 144:
        print "                        state <= idle;"
    for j in range(3):
        if i+j+1 <= 142:
            print "                    elsif data_fifo_dout(%d)(24) = '1' then" %(i+j+1)
            print "                        elink_out(23 downto 0) <= data_fifo_dout(%d)(23 downto 0);" %(i+j+1)
            print "                        state <= hit_%03d;" %(i+j+2)
        elif i+j+1 == 143:
            print "                    elsif data_fifo_dout(%d)(24) = '1' then" %(i+j+1)
            print "                        elink_out(23 downto 0) <= data_fifo_dout(%d)(23 downto 0);" %(i+j+1)
            print "                        state <= idle;"
    print "                    else"
    print "                        elink_out(23 downto 0) <= (others => '1');"
    if i+j+2 <= 143:
        print "                        state <= hit_%03d;" %(i+j+2)
    else:
        print "                        state <= idle;"
    print "                    end if;"
