                when hit_000 =>
                    if    data_fifo_dout(0)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(0)(23 downto 0);
                        state <= hit_001;
                    elsif data_fifo_dout(1)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(1)(23 downto 0);
                        state <= hit_002;
                    elsif data_fifo_dout(2)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(2)(23 downto 0);
                        state <= hit_003;
                    elsif data_fifo_dout(3)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(3)(23 downto 0);
                        state <= hit_004;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_004;
                    end if;
                when hit_001 =>
                    if    data_fifo_dout(1)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(1)(23 downto 0);
                        state <= hit_002;
                    elsif data_fifo_dout(2)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(2)(23 downto 0);
                        state <= hit_003;
                    elsif data_fifo_dout(3)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(3)(23 downto 0);
                        state <= hit_004;
                    elsif data_fifo_dout(4)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(4)(23 downto 0);
                        state <= hit_005;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_005;
                    end if;
                when hit_002 =>
                    if    data_fifo_dout(2)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(2)(23 downto 0);
                        state <= hit_003;
                    elsif data_fifo_dout(3)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(3)(23 downto 0);
                        state <= hit_004;
                    elsif data_fifo_dout(4)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(4)(23 downto 0);
                        state <= hit_005;
                    elsif data_fifo_dout(5)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(5)(23 downto 0);
                        state <= hit_006;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_006;
                    end if;
                when hit_003 =>
                    if    data_fifo_dout(3)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(3)(23 downto 0);
                        state <= hit_004;
                    elsif data_fifo_dout(4)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(4)(23 downto 0);
                        state <= hit_005;
                    elsif data_fifo_dout(5)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(5)(23 downto 0);
                        state <= hit_006;
                    elsif data_fifo_dout(6)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(6)(23 downto 0);
                        state <= hit_007;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_007;
                    end if;
                when hit_004 =>
                    if    data_fifo_dout(4)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(4)(23 downto 0);
                        state <= hit_005;
                    elsif data_fifo_dout(5)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(5)(23 downto 0);
                        state <= hit_006;
                    elsif data_fifo_dout(6)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(6)(23 downto 0);
                        state <= hit_007;
                    elsif data_fifo_dout(7)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(7)(23 downto 0);
                        state <= hit_008;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_008;
                    end if;
                when hit_005 =>
                    if    data_fifo_dout(5)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(5)(23 downto 0);
                        state <= hit_006;
                    elsif data_fifo_dout(6)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(6)(23 downto 0);
                        state <= hit_007;
                    elsif data_fifo_dout(7)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(7)(23 downto 0);
                        state <= hit_008;
                    elsif data_fifo_dout(8)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(8)(23 downto 0);
                        state <= hit_009;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_009;
                    end if;
                when hit_006 =>
                    if    data_fifo_dout(6)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(6)(23 downto 0);
                        state <= hit_007;
                    elsif data_fifo_dout(7)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(7)(23 downto 0);
                        state <= hit_008;
                    elsif data_fifo_dout(8)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(8)(23 downto 0);
                        state <= hit_009;
                    elsif data_fifo_dout(9)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(9)(23 downto 0);
                        state <= hit_010;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_010;
                    end if;
                when hit_007 =>
                    if    data_fifo_dout(7)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(7)(23 downto 0);
                        state <= hit_008;
                    elsif data_fifo_dout(8)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(8)(23 downto 0);
                        state <= hit_009;
                    elsif data_fifo_dout(9)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(9)(23 downto 0);
                        state <= hit_010;
                    elsif data_fifo_dout(10)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(10)(23 downto 0);
                        state <= hit_011;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_011;
                    end if;
                when hit_008 =>
                    if    data_fifo_dout(8)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(8)(23 downto 0);
                        state <= hit_009;
                    elsif data_fifo_dout(9)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(9)(23 downto 0);
                        state <= hit_010;
                    elsif data_fifo_dout(10)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(10)(23 downto 0);
                        state <= hit_011;
                    elsif data_fifo_dout(11)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(11)(23 downto 0);
                        state <= hit_012;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_012;
                    end if;
                when hit_009 =>
                    if    data_fifo_dout(9)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(9)(23 downto 0);
                        state <= hit_010;
                    elsif data_fifo_dout(10)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(10)(23 downto 0);
                        state <= hit_011;
                    elsif data_fifo_dout(11)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(11)(23 downto 0);
                        state <= hit_012;
                    elsif data_fifo_dout(12)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(12)(23 downto 0);
                        state <= hit_013;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_013;
                    end if;
                when hit_010 =>
                    if    data_fifo_dout(10)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(10)(23 downto 0);
                        state <= hit_011;
                    elsif data_fifo_dout(11)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(11)(23 downto 0);
                        state <= hit_012;
                    elsif data_fifo_dout(12)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(12)(23 downto 0);
                        state <= hit_013;
                    elsif data_fifo_dout(13)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(13)(23 downto 0);
                        state <= hit_014;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_014;
                    end if;
                when hit_011 =>
                    if    data_fifo_dout(11)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(11)(23 downto 0);
                        state <= hit_012;
                    elsif data_fifo_dout(12)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(12)(23 downto 0);
                        state <= hit_013;
                    elsif data_fifo_dout(13)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(13)(23 downto 0);
                        state <= hit_014;
                    elsif data_fifo_dout(14)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(14)(23 downto 0);
                        state <= hit_015;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_015;
                    end if;
                when hit_012 =>
                    if    data_fifo_dout(12)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(12)(23 downto 0);
                        state <= hit_013;
                    elsif data_fifo_dout(13)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(13)(23 downto 0);
                        state <= hit_014;
                    elsif data_fifo_dout(14)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(14)(23 downto 0);
                        state <= hit_015;
                    elsif data_fifo_dout(15)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(15)(23 downto 0);
                        state <= hit_016;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_016;
                    end if;
                when hit_013 =>
                    if    data_fifo_dout(13)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(13)(23 downto 0);
                        state <= hit_014;
                    elsif data_fifo_dout(14)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(14)(23 downto 0);
                        state <= hit_015;
                    elsif data_fifo_dout(15)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(15)(23 downto 0);
                        state <= hit_016;
                    elsif data_fifo_dout(16)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(16)(23 downto 0);
                        state <= hit_017;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_017;
                    end if;
                when hit_014 =>
                    if    data_fifo_dout(14)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(14)(23 downto 0);
                        state <= hit_015;
                    elsif data_fifo_dout(15)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(15)(23 downto 0);
                        state <= hit_016;
                    elsif data_fifo_dout(16)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(16)(23 downto 0);
                        state <= hit_017;
                    elsif data_fifo_dout(17)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(17)(23 downto 0);
                        state <= hit_018;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_018;
                    end if;
                when hit_015 =>
                    if    data_fifo_dout(15)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(15)(23 downto 0);
                        state <= hit_016;
                    elsif data_fifo_dout(16)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(16)(23 downto 0);
                        state <= hit_017;
                    elsif data_fifo_dout(17)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(17)(23 downto 0);
                        state <= hit_018;
                    elsif data_fifo_dout(18)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(18)(23 downto 0);
                        state <= hit_019;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_019;
                    end if;
                when hit_016 =>
                    if    data_fifo_dout(16)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(16)(23 downto 0);
                        state <= hit_017;
                    elsif data_fifo_dout(17)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(17)(23 downto 0);
                        state <= hit_018;
                    elsif data_fifo_dout(18)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(18)(23 downto 0);
                        state <= hit_019;
                    elsif data_fifo_dout(19)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(19)(23 downto 0);
                        state <= hit_020;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_020;
                    end if;
                when hit_017 =>
                    if    data_fifo_dout(17)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(17)(23 downto 0);
                        state <= hit_018;
                    elsif data_fifo_dout(18)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(18)(23 downto 0);
                        state <= hit_019;
                    elsif data_fifo_dout(19)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(19)(23 downto 0);
                        state <= hit_020;
                    elsif data_fifo_dout(20)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(20)(23 downto 0);
                        state <= hit_021;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_021;
                    end if;
                when hit_018 =>
                    if    data_fifo_dout(18)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(18)(23 downto 0);
                        state <= hit_019;
                    elsif data_fifo_dout(19)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(19)(23 downto 0);
                        state <= hit_020;
                    elsif data_fifo_dout(20)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(20)(23 downto 0);
                        state <= hit_021;
                    elsif data_fifo_dout(21)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(21)(23 downto 0);
                        state <= hit_022;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_022;
                    end if;
                when hit_019 =>
                    if    data_fifo_dout(19)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(19)(23 downto 0);
                        state <= hit_020;
                    elsif data_fifo_dout(20)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(20)(23 downto 0);
                        state <= hit_021;
                    elsif data_fifo_dout(21)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(21)(23 downto 0);
                        state <= hit_022;
                    elsif data_fifo_dout(22)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(22)(23 downto 0);
                        state <= hit_023;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_023;
                    end if;
                when hit_020 =>
                    if    data_fifo_dout(20)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(20)(23 downto 0);
                        state <= hit_021;
                    elsif data_fifo_dout(21)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(21)(23 downto 0);
                        state <= hit_022;
                    elsif data_fifo_dout(22)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(22)(23 downto 0);
                        state <= hit_023;
                    elsif data_fifo_dout(23)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(23)(23 downto 0);
                        state <= hit_024;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_024;
                    end if;
                when hit_021 =>
                    if    data_fifo_dout(21)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(21)(23 downto 0);
                        state <= hit_022;
                    elsif data_fifo_dout(22)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(22)(23 downto 0);
                        state <= hit_023;
                    elsif data_fifo_dout(23)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(23)(23 downto 0);
                        state <= hit_024;
                    elsif data_fifo_dout(24)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(24)(23 downto 0);
                        state <= hit_025;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_025;
                    end if;
                when hit_022 =>
                    if    data_fifo_dout(22)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(22)(23 downto 0);
                        state <= hit_023;
                    elsif data_fifo_dout(23)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(23)(23 downto 0);
                        state <= hit_024;
                    elsif data_fifo_dout(24)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(24)(23 downto 0);
                        state <= hit_025;
                    elsif data_fifo_dout(25)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(25)(23 downto 0);
                        state <= hit_026;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_026;
                    end if;
                when hit_023 =>
                    if    data_fifo_dout(23)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(23)(23 downto 0);
                        state <= hit_024;
                    elsif data_fifo_dout(24)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(24)(23 downto 0);
                        state <= hit_025;
                    elsif data_fifo_dout(25)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(25)(23 downto 0);
                        state <= hit_026;
                    elsif data_fifo_dout(26)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(26)(23 downto 0);
                        state <= hit_027;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_027;
                    end if;
                when hit_024 =>
                    if    data_fifo_dout(24)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(24)(23 downto 0);
                        state <= hit_025;
                    elsif data_fifo_dout(25)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(25)(23 downto 0);
                        state <= hit_026;
                    elsif data_fifo_dout(26)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(26)(23 downto 0);
                        state <= hit_027;
                    elsif data_fifo_dout(27)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(27)(23 downto 0);
                        state <= hit_028;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_028;
                    end if;
                when hit_025 =>
                    if    data_fifo_dout(25)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(25)(23 downto 0);
                        state <= hit_026;
                    elsif data_fifo_dout(26)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(26)(23 downto 0);
                        state <= hit_027;
                    elsif data_fifo_dout(27)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(27)(23 downto 0);
                        state <= hit_028;
                    elsif data_fifo_dout(28)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(28)(23 downto 0);
                        state <= hit_029;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_029;
                    end if;
                when hit_026 =>
                    if    data_fifo_dout(26)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(26)(23 downto 0);
                        state <= hit_027;
                    elsif data_fifo_dout(27)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(27)(23 downto 0);
                        state <= hit_028;
                    elsif data_fifo_dout(28)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(28)(23 downto 0);
                        state <= hit_029;
                    elsif data_fifo_dout(29)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(29)(23 downto 0);
                        state <= hit_030;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_030;
                    end if;
                when hit_027 =>
                    if    data_fifo_dout(27)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(27)(23 downto 0);
                        state <= hit_028;
                    elsif data_fifo_dout(28)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(28)(23 downto 0);
                        state <= hit_029;
                    elsif data_fifo_dout(29)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(29)(23 downto 0);
                        state <= hit_030;
                    elsif data_fifo_dout(30)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(30)(23 downto 0);
                        state <= hit_031;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_031;
                    end if;
                when hit_028 =>
                    if    data_fifo_dout(28)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(28)(23 downto 0);
                        state <= hit_029;
                    elsif data_fifo_dout(29)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(29)(23 downto 0);
                        state <= hit_030;
                    elsif data_fifo_dout(30)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(30)(23 downto 0);
                        state <= hit_031;
                    elsif data_fifo_dout(31)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(31)(23 downto 0);
                        state <= hit_032;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_032;
                    end if;
                when hit_029 =>
                    if    data_fifo_dout(29)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(29)(23 downto 0);
                        state <= hit_030;
                    elsif data_fifo_dout(30)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(30)(23 downto 0);
                        state <= hit_031;
                    elsif data_fifo_dout(31)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(31)(23 downto 0);
                        state <= hit_032;
                    elsif data_fifo_dout(32)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(32)(23 downto 0);
                        state <= hit_033;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_033;
                    end if;
                when hit_030 =>
                    if    data_fifo_dout(30)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(30)(23 downto 0);
                        state <= hit_031;
                    elsif data_fifo_dout(31)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(31)(23 downto 0);
                        state <= hit_032;
                    elsif data_fifo_dout(32)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(32)(23 downto 0);
                        state <= hit_033;
                    elsif data_fifo_dout(33)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(33)(23 downto 0);
                        state <= hit_034;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_034;
                    end if;
                when hit_031 =>
                    if    data_fifo_dout(31)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(31)(23 downto 0);
                        state <= hit_032;
                    elsif data_fifo_dout(32)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(32)(23 downto 0);
                        state <= hit_033;
                    elsif data_fifo_dout(33)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(33)(23 downto 0);
                        state <= hit_034;
                    elsif data_fifo_dout(34)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(34)(23 downto 0);
                        state <= hit_035;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_035;
                    end if;
                when hit_032 =>
                    if    data_fifo_dout(32)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(32)(23 downto 0);
                        state <= hit_033;
                    elsif data_fifo_dout(33)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(33)(23 downto 0);
                        state <= hit_034;
                    elsif data_fifo_dout(34)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(34)(23 downto 0);
                        state <= hit_035;
                    elsif data_fifo_dout(35)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(35)(23 downto 0);
                        state <= hit_036;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_036;
                    end if;
                when hit_033 =>
                    if    data_fifo_dout(33)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(33)(23 downto 0);
                        state <= hit_034;
                    elsif data_fifo_dout(34)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(34)(23 downto 0);
                        state <= hit_035;
                    elsif data_fifo_dout(35)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(35)(23 downto 0);
                        state <= hit_036;
                    elsif data_fifo_dout(36)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(36)(23 downto 0);
                        state <= hit_037;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_037;
                    end if;
                when hit_034 =>
                    if    data_fifo_dout(34)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(34)(23 downto 0);
                        state <= hit_035;
                    elsif data_fifo_dout(35)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(35)(23 downto 0);
                        state <= hit_036;
                    elsif data_fifo_dout(36)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(36)(23 downto 0);
                        state <= hit_037;
                    elsif data_fifo_dout(37)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(37)(23 downto 0);
                        state <= hit_038;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_038;
                    end if;
                when hit_035 =>
                    if    data_fifo_dout(35)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(35)(23 downto 0);
                        state <= hit_036;
                    elsif data_fifo_dout(36)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(36)(23 downto 0);
                        state <= hit_037;
                    elsif data_fifo_dout(37)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(37)(23 downto 0);
                        state <= hit_038;
                    elsif data_fifo_dout(38)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(38)(23 downto 0);
                        state <= hit_039;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_039;
                    end if;
                when hit_036 =>
                    if    data_fifo_dout(36)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(36)(23 downto 0);
                        state <= hit_037;
                    elsif data_fifo_dout(37)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(37)(23 downto 0);
                        state <= hit_038;
                    elsif data_fifo_dout(38)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(38)(23 downto 0);
                        state <= hit_039;
                    elsif data_fifo_dout(39)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(39)(23 downto 0);
                        state <= hit_040;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_040;
                    end if;
                when hit_037 =>
                    if    data_fifo_dout(37)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(37)(23 downto 0);
                        state <= hit_038;
                    elsif data_fifo_dout(38)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(38)(23 downto 0);
                        state <= hit_039;
                    elsif data_fifo_dout(39)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(39)(23 downto 0);
                        state <= hit_040;
                    elsif data_fifo_dout(40)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(40)(23 downto 0);
                        state <= hit_041;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_041;
                    end if;
                when hit_038 =>
                    if    data_fifo_dout(38)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(38)(23 downto 0);
                        state <= hit_039;
                    elsif data_fifo_dout(39)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(39)(23 downto 0);
                        state <= hit_040;
                    elsif data_fifo_dout(40)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(40)(23 downto 0);
                        state <= hit_041;
                    elsif data_fifo_dout(41)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(41)(23 downto 0);
                        state <= hit_042;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_042;
                    end if;
                when hit_039 =>
                    if    data_fifo_dout(39)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(39)(23 downto 0);
                        state <= hit_040;
                    elsif data_fifo_dout(40)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(40)(23 downto 0);
                        state <= hit_041;
                    elsif data_fifo_dout(41)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(41)(23 downto 0);
                        state <= hit_042;
                    elsif data_fifo_dout(42)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(42)(23 downto 0);
                        state <= hit_043;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_043;
                    end if;
                when hit_040 =>
                    if    data_fifo_dout(40)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(40)(23 downto 0);
                        state <= hit_041;
                    elsif data_fifo_dout(41)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(41)(23 downto 0);
                        state <= hit_042;
                    elsif data_fifo_dout(42)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(42)(23 downto 0);
                        state <= hit_043;
                    elsif data_fifo_dout(43)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(43)(23 downto 0);
                        state <= hit_044;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_044;
                    end if;
                when hit_041 =>
                    if    data_fifo_dout(41)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(41)(23 downto 0);
                        state <= hit_042;
                    elsif data_fifo_dout(42)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(42)(23 downto 0);
                        state <= hit_043;
                    elsif data_fifo_dout(43)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(43)(23 downto 0);
                        state <= hit_044;
                    elsif data_fifo_dout(44)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(44)(23 downto 0);
                        state <= hit_045;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_045;
                    end if;
                when hit_042 =>
                    if    data_fifo_dout(42)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(42)(23 downto 0);
                        state <= hit_043;
                    elsif data_fifo_dout(43)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(43)(23 downto 0);
                        state <= hit_044;
                    elsif data_fifo_dout(44)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(44)(23 downto 0);
                        state <= hit_045;
                    elsif data_fifo_dout(45)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(45)(23 downto 0);
                        state <= hit_046;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_046;
                    end if;
                when hit_043 =>
                    if    data_fifo_dout(43)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(43)(23 downto 0);
                        state <= hit_044;
                    elsif data_fifo_dout(44)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(44)(23 downto 0);
                        state <= hit_045;
                    elsif data_fifo_dout(45)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(45)(23 downto 0);
                        state <= hit_046;
                    elsif data_fifo_dout(46)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(46)(23 downto 0);
                        state <= hit_047;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_047;
                    end if;
                when hit_044 =>
                    if    data_fifo_dout(44)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(44)(23 downto 0);
                        state <= hit_045;
                    elsif data_fifo_dout(45)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(45)(23 downto 0);
                        state <= hit_046;
                    elsif data_fifo_dout(46)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(46)(23 downto 0);
                        state <= hit_047;
                    elsif data_fifo_dout(47)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(47)(23 downto 0);
                        state <= hit_048;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_048;
                    end if;
                when hit_045 =>
                    if    data_fifo_dout(45)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(45)(23 downto 0);
                        state <= hit_046;
                    elsif data_fifo_dout(46)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(46)(23 downto 0);
                        state <= hit_047;
                    elsif data_fifo_dout(47)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(47)(23 downto 0);
                        state <= hit_048;
                    elsif data_fifo_dout(48)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(48)(23 downto 0);
                        state <= hit_049;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_049;
                    end if;
                when hit_046 =>
                    if    data_fifo_dout(46)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(46)(23 downto 0);
                        state <= hit_047;
                    elsif data_fifo_dout(47)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(47)(23 downto 0);
                        state <= hit_048;
                    elsif data_fifo_dout(48)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(48)(23 downto 0);
                        state <= hit_049;
                    elsif data_fifo_dout(49)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(49)(23 downto 0);
                        state <= hit_050;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_050;
                    end if;
                when hit_047 =>
                    if    data_fifo_dout(47)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(47)(23 downto 0);
                        state <= hit_048;
                    elsif data_fifo_dout(48)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(48)(23 downto 0);
                        state <= hit_049;
                    elsif data_fifo_dout(49)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(49)(23 downto 0);
                        state <= hit_050;
                    elsif data_fifo_dout(50)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(50)(23 downto 0);
                        state <= hit_051;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_051;
                    end if;
                when hit_048 =>
                    if    data_fifo_dout(48)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(48)(23 downto 0);
                        state <= hit_049;
                    elsif data_fifo_dout(49)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(49)(23 downto 0);
                        state <= hit_050;
                    elsif data_fifo_dout(50)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(50)(23 downto 0);
                        state <= hit_051;
                    elsif data_fifo_dout(51)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(51)(23 downto 0);
                        state <= hit_052;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_052;
                    end if;
                when hit_049 =>
                    if    data_fifo_dout(49)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(49)(23 downto 0);
                        state <= hit_050;
                    elsif data_fifo_dout(50)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(50)(23 downto 0);
                        state <= hit_051;
                    elsif data_fifo_dout(51)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(51)(23 downto 0);
                        state <= hit_052;
                    elsif data_fifo_dout(52)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(52)(23 downto 0);
                        state <= hit_053;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_053;
                    end if;
                when hit_050 =>
                    if    data_fifo_dout(50)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(50)(23 downto 0);
                        state <= hit_051;
                    elsif data_fifo_dout(51)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(51)(23 downto 0);
                        state <= hit_052;
                    elsif data_fifo_dout(52)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(52)(23 downto 0);
                        state <= hit_053;
                    elsif data_fifo_dout(53)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(53)(23 downto 0);
                        state <= hit_054;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_054;
                    end if;
                when hit_051 =>
                    if    data_fifo_dout(51)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(51)(23 downto 0);
                        state <= hit_052;
                    elsif data_fifo_dout(52)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(52)(23 downto 0);
                        state <= hit_053;
                    elsif data_fifo_dout(53)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(53)(23 downto 0);
                        state <= hit_054;
                    elsif data_fifo_dout(54)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(54)(23 downto 0);
                        state <= hit_055;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_055;
                    end if;
                when hit_052 =>
                    if    data_fifo_dout(52)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(52)(23 downto 0);
                        state <= hit_053;
                    elsif data_fifo_dout(53)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(53)(23 downto 0);
                        state <= hit_054;
                    elsif data_fifo_dout(54)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(54)(23 downto 0);
                        state <= hit_055;
                    elsif data_fifo_dout(55)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(55)(23 downto 0);
                        state <= hit_056;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_056;
                    end if;
                when hit_053 =>
                    if    data_fifo_dout(53)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(53)(23 downto 0);
                        state <= hit_054;
                    elsif data_fifo_dout(54)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(54)(23 downto 0);
                        state <= hit_055;
                    elsif data_fifo_dout(55)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(55)(23 downto 0);
                        state <= hit_056;
                    elsif data_fifo_dout(56)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(56)(23 downto 0);
                        state <= hit_057;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_057;
                    end if;
                when hit_054 =>
                    if    data_fifo_dout(54)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(54)(23 downto 0);
                        state <= hit_055;
                    elsif data_fifo_dout(55)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(55)(23 downto 0);
                        state <= hit_056;
                    elsif data_fifo_dout(56)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(56)(23 downto 0);
                        state <= hit_057;
                    elsif data_fifo_dout(57)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(57)(23 downto 0);
                        state <= hit_058;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_058;
                    end if;
                when hit_055 =>
                    if    data_fifo_dout(55)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(55)(23 downto 0);
                        state <= hit_056;
                    elsif data_fifo_dout(56)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(56)(23 downto 0);
                        state <= hit_057;
                    elsif data_fifo_dout(57)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(57)(23 downto 0);
                        state <= hit_058;
                    elsif data_fifo_dout(58)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(58)(23 downto 0);
                        state <= hit_059;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_059;
                    end if;
                when hit_056 =>
                    if    data_fifo_dout(56)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(56)(23 downto 0);
                        state <= hit_057;
                    elsif data_fifo_dout(57)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(57)(23 downto 0);
                        state <= hit_058;
                    elsif data_fifo_dout(58)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(58)(23 downto 0);
                        state <= hit_059;
                    elsif data_fifo_dout(59)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(59)(23 downto 0);
                        state <= hit_060;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_060;
                    end if;
                when hit_057 =>
                    if    data_fifo_dout(57)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(57)(23 downto 0);
                        state <= hit_058;
                    elsif data_fifo_dout(58)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(58)(23 downto 0);
                        state <= hit_059;
                    elsif data_fifo_dout(59)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(59)(23 downto 0);
                        state <= hit_060;
                    elsif data_fifo_dout(60)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(60)(23 downto 0);
                        state <= hit_061;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_061;
                    end if;
                when hit_058 =>
                    if    data_fifo_dout(58)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(58)(23 downto 0);
                        state <= hit_059;
                    elsif data_fifo_dout(59)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(59)(23 downto 0);
                        state <= hit_060;
                    elsif data_fifo_dout(60)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(60)(23 downto 0);
                        state <= hit_061;
                    elsif data_fifo_dout(61)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(61)(23 downto 0);
                        state <= hit_062;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_062;
                    end if;
                when hit_059 =>
                    if    data_fifo_dout(59)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(59)(23 downto 0);
                        state <= hit_060;
                    elsif data_fifo_dout(60)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(60)(23 downto 0);
                        state <= hit_061;
                    elsif data_fifo_dout(61)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(61)(23 downto 0);
                        state <= hit_062;
                    elsif data_fifo_dout(62)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(62)(23 downto 0);
                        state <= hit_063;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_063;
                    end if;
                when hit_060 =>
                    if    data_fifo_dout(60)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(60)(23 downto 0);
                        state <= hit_061;
                    elsif data_fifo_dout(61)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(61)(23 downto 0);
                        state <= hit_062;
                    elsif data_fifo_dout(62)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(62)(23 downto 0);
                        state <= hit_063;
                    elsif data_fifo_dout(63)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(63)(23 downto 0);
                        state <= hit_064;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_064;
                    end if;
                when hit_061 =>
                    if    data_fifo_dout(61)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(61)(23 downto 0);
                        state <= hit_062;
                    elsif data_fifo_dout(62)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(62)(23 downto 0);
                        state <= hit_063;
                    elsif data_fifo_dout(63)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(63)(23 downto 0);
                        state <= hit_064;
                    elsif data_fifo_dout(64)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(64)(23 downto 0);
                        state <= hit_065;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_065;
                    end if;
                when hit_062 =>
                    if    data_fifo_dout(62)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(62)(23 downto 0);
                        state <= hit_063;
                    elsif data_fifo_dout(63)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(63)(23 downto 0);
                        state <= hit_064;
                    elsif data_fifo_dout(64)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(64)(23 downto 0);
                        state <= hit_065;
                    elsif data_fifo_dout(65)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(65)(23 downto 0);
                        state <= hit_066;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_066;
                    end if;
                when hit_063 =>
                    if    data_fifo_dout(63)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(63)(23 downto 0);
                        state <= hit_064;
                    elsif data_fifo_dout(64)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(64)(23 downto 0);
                        state <= hit_065;
                    elsif data_fifo_dout(65)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(65)(23 downto 0);
                        state <= hit_066;
                    elsif data_fifo_dout(66)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(66)(23 downto 0);
                        state <= hit_067;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_067;
                    end if;
                when hit_064 =>
                    if    data_fifo_dout(64)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(64)(23 downto 0);
                        state <= hit_065;
                    elsif data_fifo_dout(65)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(65)(23 downto 0);
                        state <= hit_066;
                    elsif data_fifo_dout(66)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(66)(23 downto 0);
                        state <= hit_067;
                    elsif data_fifo_dout(67)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(67)(23 downto 0);
                        state <= hit_068;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_068;
                    end if;
                when hit_065 =>
                    if    data_fifo_dout(65)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(65)(23 downto 0);
                        state <= hit_066;
                    elsif data_fifo_dout(66)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(66)(23 downto 0);
                        state <= hit_067;
                    elsif data_fifo_dout(67)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(67)(23 downto 0);
                        state <= hit_068;
                    elsif data_fifo_dout(68)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(68)(23 downto 0);
                        state <= hit_069;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_069;
                    end if;
                when hit_066 =>
                    if    data_fifo_dout(66)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(66)(23 downto 0);
                        state <= hit_067;
                    elsif data_fifo_dout(67)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(67)(23 downto 0);
                        state <= hit_068;
                    elsif data_fifo_dout(68)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(68)(23 downto 0);
                        state <= hit_069;
                    elsif data_fifo_dout(69)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(69)(23 downto 0);
                        state <= hit_070;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_070;
                    end if;
                when hit_067 =>
                    if    data_fifo_dout(67)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(67)(23 downto 0);
                        state <= hit_068;
                    elsif data_fifo_dout(68)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(68)(23 downto 0);
                        state <= hit_069;
                    elsif data_fifo_dout(69)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(69)(23 downto 0);
                        state <= hit_070;
                    elsif data_fifo_dout(70)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(70)(23 downto 0);
                        state <= hit_071;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_071;
                    end if;
                when hit_068 =>
                    if    data_fifo_dout(68)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(68)(23 downto 0);
                        state <= hit_069;
                    elsif data_fifo_dout(69)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(69)(23 downto 0);
                        state <= hit_070;
                    elsif data_fifo_dout(70)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(70)(23 downto 0);
                        state <= hit_071;
                    elsif data_fifo_dout(71)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(71)(23 downto 0);
                        state <= hit_072;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_072;
                    end if;
                when hit_069 =>
                    if    data_fifo_dout(69)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(69)(23 downto 0);
                        state <= hit_070;
                    elsif data_fifo_dout(70)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(70)(23 downto 0);
                        state <= hit_071;
                    elsif data_fifo_dout(71)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(71)(23 downto 0);
                        state <= hit_072;
                    elsif data_fifo_dout(72)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(72)(23 downto 0);
                        state <= hit_073;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_073;
                    end if;
                when hit_070 =>
                    if    data_fifo_dout(70)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(70)(23 downto 0);
                        state <= hit_071;
                    elsif data_fifo_dout(71)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(71)(23 downto 0);
                        state <= hit_072;
                    elsif data_fifo_dout(72)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(72)(23 downto 0);
                        state <= hit_073;
                    elsif data_fifo_dout(73)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(73)(23 downto 0);
                        state <= hit_074;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_074;
                    end if;
                when hit_071 =>
                    if    data_fifo_dout(71)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(71)(23 downto 0);
                        state <= hit_072;
                    elsif data_fifo_dout(72)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(72)(23 downto 0);
                        state <= hit_073;
                    elsif data_fifo_dout(73)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(73)(23 downto 0);
                        state <= hit_074;
                    elsif data_fifo_dout(74)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(74)(23 downto 0);
                        state <= hit_075;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_075;
                    end if;
                when hit_072 =>
                    if    data_fifo_dout(72)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(72)(23 downto 0);
                        state <= hit_073;
                    elsif data_fifo_dout(73)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(73)(23 downto 0);
                        state <= hit_074;
                    elsif data_fifo_dout(74)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(74)(23 downto 0);
                        state <= hit_075;
                    elsif data_fifo_dout(75)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(75)(23 downto 0);
                        state <= hit_076;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_076;
                    end if;
                when hit_073 =>
                    if    data_fifo_dout(73)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(73)(23 downto 0);
                        state <= hit_074;
                    elsif data_fifo_dout(74)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(74)(23 downto 0);
                        state <= hit_075;
                    elsif data_fifo_dout(75)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(75)(23 downto 0);
                        state <= hit_076;
                    elsif data_fifo_dout(76)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(76)(23 downto 0);
                        state <= hit_077;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_077;
                    end if;
                when hit_074 =>
                    if    data_fifo_dout(74)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(74)(23 downto 0);
                        state <= hit_075;
                    elsif data_fifo_dout(75)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(75)(23 downto 0);
                        state <= hit_076;
                    elsif data_fifo_dout(76)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(76)(23 downto 0);
                        state <= hit_077;
                    elsif data_fifo_dout(77)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(77)(23 downto 0);
                        state <= hit_078;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_078;
                    end if;
                when hit_075 =>
                    if    data_fifo_dout(75)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(75)(23 downto 0);
                        state <= hit_076;
                    elsif data_fifo_dout(76)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(76)(23 downto 0);
                        state <= hit_077;
                    elsif data_fifo_dout(77)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(77)(23 downto 0);
                        state <= hit_078;
                    elsif data_fifo_dout(78)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(78)(23 downto 0);
                        state <= hit_079;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_079;
                    end if;
                when hit_076 =>
                    if    data_fifo_dout(76)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(76)(23 downto 0);
                        state <= hit_077;
                    elsif data_fifo_dout(77)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(77)(23 downto 0);
                        state <= hit_078;
                    elsif data_fifo_dout(78)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(78)(23 downto 0);
                        state <= hit_079;
                    elsif data_fifo_dout(79)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(79)(23 downto 0);
                        state <= hit_080;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_080;
                    end if;
                when hit_077 =>
                    if    data_fifo_dout(77)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(77)(23 downto 0);
                        state <= hit_078;
                    elsif data_fifo_dout(78)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(78)(23 downto 0);
                        state <= hit_079;
                    elsif data_fifo_dout(79)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(79)(23 downto 0);
                        state <= hit_080;
                    elsif data_fifo_dout(80)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(80)(23 downto 0);
                        state <= hit_081;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_081;
                    end if;
                when hit_078 =>
                    if    data_fifo_dout(78)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(78)(23 downto 0);
                        state <= hit_079;
                    elsif data_fifo_dout(79)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(79)(23 downto 0);
                        state <= hit_080;
                    elsif data_fifo_dout(80)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(80)(23 downto 0);
                        state <= hit_081;
                    elsif data_fifo_dout(81)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(81)(23 downto 0);
                        state <= hit_082;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_082;
                    end if;
                when hit_079 =>
                    if    data_fifo_dout(79)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(79)(23 downto 0);
                        state <= hit_080;
                    elsif data_fifo_dout(80)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(80)(23 downto 0);
                        state <= hit_081;
                    elsif data_fifo_dout(81)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(81)(23 downto 0);
                        state <= hit_082;
                    elsif data_fifo_dout(82)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(82)(23 downto 0);
                        state <= hit_083;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_083;
                    end if;
                when hit_080 =>
                    if    data_fifo_dout(80)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(80)(23 downto 0);
                        state <= hit_081;
                    elsif data_fifo_dout(81)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(81)(23 downto 0);
                        state <= hit_082;
                    elsif data_fifo_dout(82)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(82)(23 downto 0);
                        state <= hit_083;
                    elsif data_fifo_dout(83)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(83)(23 downto 0);
                        state <= hit_084;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_084;
                    end if;
                when hit_081 =>
                    if    data_fifo_dout(81)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(81)(23 downto 0);
                        state <= hit_082;
                    elsif data_fifo_dout(82)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(82)(23 downto 0);
                        state <= hit_083;
                    elsif data_fifo_dout(83)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(83)(23 downto 0);
                        state <= hit_084;
                    elsif data_fifo_dout(84)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(84)(23 downto 0);
                        state <= hit_085;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_085;
                    end if;
                when hit_082 =>
                    if    data_fifo_dout(82)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(82)(23 downto 0);
                        state <= hit_083;
                    elsif data_fifo_dout(83)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(83)(23 downto 0);
                        state <= hit_084;
                    elsif data_fifo_dout(84)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(84)(23 downto 0);
                        state <= hit_085;
                    elsif data_fifo_dout(85)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(85)(23 downto 0);
                        state <= hit_086;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_086;
                    end if;
                when hit_083 =>
                    if    data_fifo_dout(83)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(83)(23 downto 0);
                        state <= hit_084;
                    elsif data_fifo_dout(84)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(84)(23 downto 0);
                        state <= hit_085;
                    elsif data_fifo_dout(85)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(85)(23 downto 0);
                        state <= hit_086;
                    elsif data_fifo_dout(86)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(86)(23 downto 0);
                        state <= hit_087;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_087;
                    end if;
                when hit_084 =>
                    if    data_fifo_dout(84)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(84)(23 downto 0);
                        state <= hit_085;
                    elsif data_fifo_dout(85)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(85)(23 downto 0);
                        state <= hit_086;
                    elsif data_fifo_dout(86)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(86)(23 downto 0);
                        state <= hit_087;
                    elsif data_fifo_dout(87)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(87)(23 downto 0);
                        state <= hit_088;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_088;
                    end if;
                when hit_085 =>
                    if    data_fifo_dout(85)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(85)(23 downto 0);
                        state <= hit_086;
                    elsif data_fifo_dout(86)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(86)(23 downto 0);
                        state <= hit_087;
                    elsif data_fifo_dout(87)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(87)(23 downto 0);
                        state <= hit_088;
                    elsif data_fifo_dout(88)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(88)(23 downto 0);
                        state <= hit_089;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_089;
                    end if;
                when hit_086 =>
                    if    data_fifo_dout(86)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(86)(23 downto 0);
                        state <= hit_087;
                    elsif data_fifo_dout(87)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(87)(23 downto 0);
                        state <= hit_088;
                    elsif data_fifo_dout(88)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(88)(23 downto 0);
                        state <= hit_089;
                    elsif data_fifo_dout(89)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(89)(23 downto 0);
                        state <= hit_090;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_090;
                    end if;
                when hit_087 =>
                    if    data_fifo_dout(87)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(87)(23 downto 0);
                        state <= hit_088;
                    elsif data_fifo_dout(88)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(88)(23 downto 0);
                        state <= hit_089;
                    elsif data_fifo_dout(89)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(89)(23 downto 0);
                        state <= hit_090;
                    elsif data_fifo_dout(90)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(90)(23 downto 0);
                        state <= hit_091;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_091;
                    end if;
                when hit_088 =>
                    if    data_fifo_dout(88)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(88)(23 downto 0);
                        state <= hit_089;
                    elsif data_fifo_dout(89)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(89)(23 downto 0);
                        state <= hit_090;
                    elsif data_fifo_dout(90)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(90)(23 downto 0);
                        state <= hit_091;
                    elsif data_fifo_dout(91)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(91)(23 downto 0);
                        state <= hit_092;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_092;
                    end if;
                when hit_089 =>
                    if    data_fifo_dout(89)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(89)(23 downto 0);
                        state <= hit_090;
                    elsif data_fifo_dout(90)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(90)(23 downto 0);
                        state <= hit_091;
                    elsif data_fifo_dout(91)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(91)(23 downto 0);
                        state <= hit_092;
                    elsif data_fifo_dout(92)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(92)(23 downto 0);
                        state <= hit_093;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_093;
                    end if;
                when hit_090 =>
                    if    data_fifo_dout(90)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(90)(23 downto 0);
                        state <= hit_091;
                    elsif data_fifo_dout(91)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(91)(23 downto 0);
                        state <= hit_092;
                    elsif data_fifo_dout(92)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(92)(23 downto 0);
                        state <= hit_093;
                    elsif data_fifo_dout(93)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(93)(23 downto 0);
                        state <= hit_094;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_094;
                    end if;
                when hit_091 =>
                    if    data_fifo_dout(91)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(91)(23 downto 0);
                        state <= hit_092;
                    elsif data_fifo_dout(92)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(92)(23 downto 0);
                        state <= hit_093;
                    elsif data_fifo_dout(93)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(93)(23 downto 0);
                        state <= hit_094;
                    elsif data_fifo_dout(94)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(94)(23 downto 0);
                        state <= hit_095;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_095;
                    end if;
                when hit_092 =>
                    if    data_fifo_dout(92)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(92)(23 downto 0);
                        state <= hit_093;
                    elsif data_fifo_dout(93)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(93)(23 downto 0);
                        state <= hit_094;
                    elsif data_fifo_dout(94)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(94)(23 downto 0);
                        state <= hit_095;
                    elsif data_fifo_dout(95)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(95)(23 downto 0);
                        state <= hit_096;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_096;
                    end if;
                when hit_093 =>
                    if    data_fifo_dout(93)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(93)(23 downto 0);
                        state <= hit_094;
                    elsif data_fifo_dout(94)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(94)(23 downto 0);
                        state <= hit_095;
                    elsif data_fifo_dout(95)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(95)(23 downto 0);
                        state <= hit_096;
                    elsif data_fifo_dout(96)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(96)(23 downto 0);
                        state <= hit_097;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_097;
                    end if;
                when hit_094 =>
                    if    data_fifo_dout(94)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(94)(23 downto 0);
                        state <= hit_095;
                    elsif data_fifo_dout(95)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(95)(23 downto 0);
                        state <= hit_096;
                    elsif data_fifo_dout(96)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(96)(23 downto 0);
                        state <= hit_097;
                    elsif data_fifo_dout(97)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(97)(23 downto 0);
                        state <= hit_098;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_098;
                    end if;
                when hit_095 =>
                    if    data_fifo_dout(95)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(95)(23 downto 0);
                        state <= hit_096;
                    elsif data_fifo_dout(96)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(96)(23 downto 0);
                        state <= hit_097;
                    elsif data_fifo_dout(97)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(97)(23 downto 0);
                        state <= hit_098;
                    elsif data_fifo_dout(98)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(98)(23 downto 0);
                        state <= hit_099;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_099;
                    end if;
                when hit_096 =>
                    if    data_fifo_dout(96)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(96)(23 downto 0);
                        state <= hit_097;
                    elsif data_fifo_dout(97)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(97)(23 downto 0);
                        state <= hit_098;
                    elsif data_fifo_dout(98)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(98)(23 downto 0);
                        state <= hit_099;
                    elsif data_fifo_dout(99)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(99)(23 downto 0);
                        state <= hit_100;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_100;
                    end if;
                when hit_097 =>
                    if    data_fifo_dout(97)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(97)(23 downto 0);
                        state <= hit_098;
                    elsif data_fifo_dout(98)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(98)(23 downto 0);
                        state <= hit_099;
                    elsif data_fifo_dout(99)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(99)(23 downto 0);
                        state <= hit_100;
                    elsif data_fifo_dout(100)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(100)(23 downto 0);
                        state <= hit_101;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_101;
                    end if;
                when hit_098 =>
                    if    data_fifo_dout(98)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(98)(23 downto 0);
                        state <= hit_099;
                    elsif data_fifo_dout(99)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(99)(23 downto 0);
                        state <= hit_100;
                    elsif data_fifo_dout(100)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(100)(23 downto 0);
                        state <= hit_101;
                    elsif data_fifo_dout(101)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(101)(23 downto 0);
                        state <= hit_102;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_102;
                    end if;
                when hit_099 =>
                    if    data_fifo_dout(99)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(99)(23 downto 0);
                        state <= hit_100;
                    elsif data_fifo_dout(100)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(100)(23 downto 0);
                        state <= hit_101;
                    elsif data_fifo_dout(101)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(101)(23 downto 0);
                        state <= hit_102;
                    elsif data_fifo_dout(102)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(102)(23 downto 0);
                        state <= hit_103;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_103;
                    end if;
                when hit_100 =>
                    if    data_fifo_dout(100)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(100)(23 downto 0);
                        state <= hit_101;
                    elsif data_fifo_dout(101)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(101)(23 downto 0);
                        state <= hit_102;
                    elsif data_fifo_dout(102)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(102)(23 downto 0);
                        state <= hit_103;
                    elsif data_fifo_dout(103)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(103)(23 downto 0);
                        state <= hit_104;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_104;
                    end if;
                when hit_101 =>
                    if    data_fifo_dout(101)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(101)(23 downto 0);
                        state <= hit_102;
                    elsif data_fifo_dout(102)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(102)(23 downto 0);
                        state <= hit_103;
                    elsif data_fifo_dout(103)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(103)(23 downto 0);
                        state <= hit_104;
                    elsif data_fifo_dout(104)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(104)(23 downto 0);
                        state <= hit_105;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_105;
                    end if;
                when hit_102 =>
                    if    data_fifo_dout(102)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(102)(23 downto 0);
                        state <= hit_103;
                    elsif data_fifo_dout(103)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(103)(23 downto 0);
                        state <= hit_104;
                    elsif data_fifo_dout(104)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(104)(23 downto 0);
                        state <= hit_105;
                    elsif data_fifo_dout(105)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(105)(23 downto 0);
                        state <= hit_106;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_106;
                    end if;
                when hit_103 =>
                    if    data_fifo_dout(103)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(103)(23 downto 0);
                        state <= hit_104;
                    elsif data_fifo_dout(104)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(104)(23 downto 0);
                        state <= hit_105;
                    elsif data_fifo_dout(105)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(105)(23 downto 0);
                        state <= hit_106;
                    elsif data_fifo_dout(106)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(106)(23 downto 0);
                        state <= hit_107;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_107;
                    end if;
                when hit_104 =>
                    if    data_fifo_dout(104)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(104)(23 downto 0);
                        state <= hit_105;
                    elsif data_fifo_dout(105)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(105)(23 downto 0);
                        state <= hit_106;
                    elsif data_fifo_dout(106)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(106)(23 downto 0);
                        state <= hit_107;
                    elsif data_fifo_dout(107)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(107)(23 downto 0);
                        state <= hit_108;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_108;
                    end if;
                when hit_105 =>
                    if    data_fifo_dout(105)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(105)(23 downto 0);
                        state <= hit_106;
                    elsif data_fifo_dout(106)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(106)(23 downto 0);
                        state <= hit_107;
                    elsif data_fifo_dout(107)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(107)(23 downto 0);
                        state <= hit_108;
                    elsif data_fifo_dout(108)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(108)(23 downto 0);
                        state <= hit_109;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_109;
                    end if;
                when hit_106 =>
                    if    data_fifo_dout(106)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(106)(23 downto 0);
                        state <= hit_107;
                    elsif data_fifo_dout(107)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(107)(23 downto 0);
                        state <= hit_108;
                    elsif data_fifo_dout(108)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(108)(23 downto 0);
                        state <= hit_109;
                    elsif data_fifo_dout(109)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(109)(23 downto 0);
                        state <= hit_110;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_110;
                    end if;
                when hit_107 =>
                    if    data_fifo_dout(107)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(107)(23 downto 0);
                        state <= hit_108;
                    elsif data_fifo_dout(108)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(108)(23 downto 0);
                        state <= hit_109;
                    elsif data_fifo_dout(109)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(109)(23 downto 0);
                        state <= hit_110;
                    elsif data_fifo_dout(110)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(110)(23 downto 0);
                        state <= hit_111;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_111;
                    end if;
                when hit_108 =>
                    if    data_fifo_dout(108)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(108)(23 downto 0);
                        state <= hit_109;
                    elsif data_fifo_dout(109)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(109)(23 downto 0);
                        state <= hit_110;
                    elsif data_fifo_dout(110)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(110)(23 downto 0);
                        state <= hit_111;
                    elsif data_fifo_dout(111)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(111)(23 downto 0);
                        state <= hit_112;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_112;
                    end if;
                when hit_109 =>
                    if    data_fifo_dout(109)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(109)(23 downto 0);
                        state <= hit_110;
                    elsif data_fifo_dout(110)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(110)(23 downto 0);
                        state <= hit_111;
                    elsif data_fifo_dout(111)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(111)(23 downto 0);
                        state <= hit_112;
                    elsif data_fifo_dout(112)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(112)(23 downto 0);
                        state <= hit_113;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_113;
                    end if;
                when hit_110 =>
                    if    data_fifo_dout(110)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(110)(23 downto 0);
                        state <= hit_111;
                    elsif data_fifo_dout(111)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(111)(23 downto 0);
                        state <= hit_112;
                    elsif data_fifo_dout(112)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(112)(23 downto 0);
                        state <= hit_113;
                    elsif data_fifo_dout(113)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(113)(23 downto 0);
                        state <= hit_114;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_114;
                    end if;
                when hit_111 =>
                    if    data_fifo_dout(111)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(111)(23 downto 0);
                        state <= hit_112;
                    elsif data_fifo_dout(112)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(112)(23 downto 0);
                        state <= hit_113;
                    elsif data_fifo_dout(113)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(113)(23 downto 0);
                        state <= hit_114;
                    elsif data_fifo_dout(114)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(114)(23 downto 0);
                        state <= hit_115;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_115;
                    end if;
                when hit_112 =>
                    if    data_fifo_dout(112)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(112)(23 downto 0);
                        state <= hit_113;
                    elsif data_fifo_dout(113)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(113)(23 downto 0);
                        state <= hit_114;
                    elsif data_fifo_dout(114)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(114)(23 downto 0);
                        state <= hit_115;
                    elsif data_fifo_dout(115)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(115)(23 downto 0);
                        state <= hit_116;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_116;
                    end if;
                when hit_113 =>
                    if    data_fifo_dout(113)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(113)(23 downto 0);
                        state <= hit_114;
                    elsif data_fifo_dout(114)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(114)(23 downto 0);
                        state <= hit_115;
                    elsif data_fifo_dout(115)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(115)(23 downto 0);
                        state <= hit_116;
                    elsif data_fifo_dout(116)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(116)(23 downto 0);
                        state <= hit_117;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_117;
                    end if;
                when hit_114 =>
                    if    data_fifo_dout(114)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(114)(23 downto 0);
                        state <= hit_115;
                    elsif data_fifo_dout(115)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(115)(23 downto 0);
                        state <= hit_116;
                    elsif data_fifo_dout(116)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(116)(23 downto 0);
                        state <= hit_117;
                    elsif data_fifo_dout(117)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(117)(23 downto 0);
                        state <= hit_118;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_118;
                    end if;
                when hit_115 =>
                    if    data_fifo_dout(115)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(115)(23 downto 0);
                        state <= hit_116;
                    elsif data_fifo_dout(116)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(116)(23 downto 0);
                        state <= hit_117;
                    elsif data_fifo_dout(117)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(117)(23 downto 0);
                        state <= hit_118;
                    elsif data_fifo_dout(118)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(118)(23 downto 0);
                        state <= hit_119;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_119;
                    end if;
                when hit_116 =>
                    if    data_fifo_dout(116)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(116)(23 downto 0);
                        state <= hit_117;
                    elsif data_fifo_dout(117)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(117)(23 downto 0);
                        state <= hit_118;
                    elsif data_fifo_dout(118)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(118)(23 downto 0);
                        state <= hit_119;
                    elsif data_fifo_dout(119)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(119)(23 downto 0);
                        state <= hit_120;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_120;
                    end if;
                when hit_117 =>
                    if    data_fifo_dout(117)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(117)(23 downto 0);
                        state <= hit_118;
                    elsif data_fifo_dout(118)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(118)(23 downto 0);
                        state <= hit_119;
                    elsif data_fifo_dout(119)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(119)(23 downto 0);
                        state <= hit_120;
                    elsif data_fifo_dout(120)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(120)(23 downto 0);
                        state <= hit_121;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_121;
                    end if;
                when hit_118 =>
                    if    data_fifo_dout(118)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(118)(23 downto 0);
                        state <= hit_119;
                    elsif data_fifo_dout(119)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(119)(23 downto 0);
                        state <= hit_120;
                    elsif data_fifo_dout(120)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(120)(23 downto 0);
                        state <= hit_121;
                    elsif data_fifo_dout(121)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(121)(23 downto 0);
                        state <= hit_122;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_122;
                    end if;
                when hit_119 =>
                    if    data_fifo_dout(119)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(119)(23 downto 0);
                        state <= hit_120;
                    elsif data_fifo_dout(120)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(120)(23 downto 0);
                        state <= hit_121;
                    elsif data_fifo_dout(121)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(121)(23 downto 0);
                        state <= hit_122;
                    elsif data_fifo_dout(122)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(122)(23 downto 0);
                        state <= hit_123;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_123;
                    end if;
                when hit_120 =>
                    if    data_fifo_dout(120)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(120)(23 downto 0);
                        state <= hit_121;
                    elsif data_fifo_dout(121)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(121)(23 downto 0);
                        state <= hit_122;
                    elsif data_fifo_dout(122)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(122)(23 downto 0);
                        state <= hit_123;
                    elsif data_fifo_dout(123)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(123)(23 downto 0);
                        state <= hit_124;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_124;
                    end if;
                when hit_121 =>
                    if    data_fifo_dout(121)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(121)(23 downto 0);
                        state <= hit_122;
                    elsif data_fifo_dout(122)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(122)(23 downto 0);
                        state <= hit_123;
                    elsif data_fifo_dout(123)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(123)(23 downto 0);
                        state <= hit_124;
                    elsif data_fifo_dout(124)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(124)(23 downto 0);
                        state <= hit_125;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_125;
                    end if;
                when hit_122 =>
                    if    data_fifo_dout(122)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(122)(23 downto 0);
                        state <= hit_123;
                    elsif data_fifo_dout(123)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(123)(23 downto 0);
                        state <= hit_124;
                    elsif data_fifo_dout(124)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(124)(23 downto 0);
                        state <= hit_125;
                    elsif data_fifo_dout(125)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(125)(23 downto 0);
                        state <= hit_126;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_126;
                    end if;
                when hit_123 =>
                    if    data_fifo_dout(123)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(123)(23 downto 0);
                        state <= hit_124;
                    elsif data_fifo_dout(124)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(124)(23 downto 0);
                        state <= hit_125;
                    elsif data_fifo_dout(125)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(125)(23 downto 0);
                        state <= hit_126;
                    elsif data_fifo_dout(126)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(126)(23 downto 0);
                        state <= hit_127;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_127;
                    end if;
                when hit_124 =>
                    if    data_fifo_dout(124)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(124)(23 downto 0);
                        state <= hit_125;
                    elsif data_fifo_dout(125)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(125)(23 downto 0);
                        state <= hit_126;
                    elsif data_fifo_dout(126)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(126)(23 downto 0);
                        state <= hit_127;
                    elsif data_fifo_dout(127)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(127)(23 downto 0);
                        state <= hit_128;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_128;
                    end if;
                when hit_125 =>
                    if    data_fifo_dout(125)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(125)(23 downto 0);
                        state <= hit_126;
                    elsif data_fifo_dout(126)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(126)(23 downto 0);
                        state <= hit_127;
                    elsif data_fifo_dout(127)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(127)(23 downto 0);
                        state <= hit_128;
                    elsif data_fifo_dout(128)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(128)(23 downto 0);
                        state <= hit_129;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_129;
                    end if;
                when hit_126 =>
                    if    data_fifo_dout(126)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(126)(23 downto 0);
                        state <= hit_127;
                    elsif data_fifo_dout(127)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(127)(23 downto 0);
                        state <= hit_128;
                    elsif data_fifo_dout(128)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(128)(23 downto 0);
                        state <= hit_129;
                    elsif data_fifo_dout(129)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(129)(23 downto 0);
                        state <= hit_130;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_130;
                    end if;
                when hit_127 =>
                    if    data_fifo_dout(127)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(127)(23 downto 0);
                        state <= hit_128;
                    elsif data_fifo_dout(128)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(128)(23 downto 0);
                        state <= hit_129;
                    elsif data_fifo_dout(129)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(129)(23 downto 0);
                        state <= hit_130;
                    elsif data_fifo_dout(130)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(130)(23 downto 0);
                        state <= hit_131;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_131;
                    end if;
                when hit_128 =>
                    if    data_fifo_dout(128)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(128)(23 downto 0);
                        state <= hit_129;
                    elsif data_fifo_dout(129)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(129)(23 downto 0);
                        state <= hit_130;
                    elsif data_fifo_dout(130)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(130)(23 downto 0);
                        state <= hit_131;
                    elsif data_fifo_dout(131)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(131)(23 downto 0);
                        state <= hit_132;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_132;
                    end if;
                when hit_129 =>
                    if    data_fifo_dout(129)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(129)(23 downto 0);
                        state <= hit_130;
                    elsif data_fifo_dout(130)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(130)(23 downto 0);
                        state <= hit_131;
                    elsif data_fifo_dout(131)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(131)(23 downto 0);
                        state <= hit_132;
                    elsif data_fifo_dout(132)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(132)(23 downto 0);
                        state <= hit_133;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_133;
                    end if;
                when hit_130 =>
                    if    data_fifo_dout(130)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(130)(23 downto 0);
                        state <= hit_131;
                    elsif data_fifo_dout(131)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(131)(23 downto 0);
                        state <= hit_132;
                    elsif data_fifo_dout(132)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(132)(23 downto 0);
                        state <= hit_133;
                    elsif data_fifo_dout(133)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(133)(23 downto 0);
                        state <= hit_134;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_134;
                    end if;
                when hit_131 =>
                    if    data_fifo_dout(131)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(131)(23 downto 0);
                        state <= hit_132;
                    elsif data_fifo_dout(132)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(132)(23 downto 0);
                        state <= hit_133;
                    elsif data_fifo_dout(133)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(133)(23 downto 0);
                        state <= hit_134;
                    elsif data_fifo_dout(134)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(134)(23 downto 0);
                        state <= hit_135;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_135;
                    end if;
                when hit_132 =>
                    if    data_fifo_dout(132)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(132)(23 downto 0);
                        state <= hit_133;
                    elsif data_fifo_dout(133)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(133)(23 downto 0);
                        state <= hit_134;
                    elsif data_fifo_dout(134)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(134)(23 downto 0);
                        state <= hit_135;
                    elsif data_fifo_dout(135)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(135)(23 downto 0);
                        state <= hit_136;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_136;
                    end if;
                when hit_133 =>
                    if    data_fifo_dout(133)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(133)(23 downto 0);
                        state <= hit_134;
                    elsif data_fifo_dout(134)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(134)(23 downto 0);
                        state <= hit_135;
                    elsif data_fifo_dout(135)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(135)(23 downto 0);
                        state <= hit_136;
                    elsif data_fifo_dout(136)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(136)(23 downto 0);
                        state <= hit_137;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_137;
                    end if;
                when hit_134 =>
                    if    data_fifo_dout(134)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(134)(23 downto 0);
                        state <= hit_135;
                    elsif data_fifo_dout(135)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(135)(23 downto 0);
                        state <= hit_136;
                    elsif data_fifo_dout(136)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(136)(23 downto 0);
                        state <= hit_137;
                    elsif data_fifo_dout(137)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(137)(23 downto 0);
                        state <= hit_138;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_138;
                    end if;
                when hit_135 =>
                    if    data_fifo_dout(135)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(135)(23 downto 0);
                        state <= hit_136;
                    elsif data_fifo_dout(136)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(136)(23 downto 0);
                        state <= hit_137;
                    elsif data_fifo_dout(137)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(137)(23 downto 0);
                        state <= hit_138;
                    elsif data_fifo_dout(138)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(138)(23 downto 0);
                        state <= hit_139;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_139;
                    end if;
                when hit_136 =>
                    if    data_fifo_dout(136)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(136)(23 downto 0);
                        state <= hit_137;
                    elsif data_fifo_dout(137)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(137)(23 downto 0);
                        state <= hit_138;
                    elsif data_fifo_dout(138)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(138)(23 downto 0);
                        state <= hit_139;
                    elsif data_fifo_dout(139)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(139)(23 downto 0);
                        state <= hit_140;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_140;
                    end if;
                when hit_137 =>
                    if    data_fifo_dout(137)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(137)(23 downto 0);
                        state <= hit_138;
                    elsif data_fifo_dout(138)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(138)(23 downto 0);
                        state <= hit_139;
                    elsif data_fifo_dout(139)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(139)(23 downto 0);
                        state <= hit_140;
                    elsif data_fifo_dout(140)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(140)(23 downto 0);
                        state <= hit_141;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_141;
                    end if;
                when hit_138 =>
                    if    data_fifo_dout(138)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(138)(23 downto 0);
                        state <= hit_139;
                    elsif data_fifo_dout(139)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(139)(23 downto 0);
                        state <= hit_140;
                    elsif data_fifo_dout(140)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(140)(23 downto 0);
                        state <= hit_141;
                    elsif data_fifo_dout(141)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(141)(23 downto 0);
                        state <= hit_142;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_142;
                    end if;
                when hit_139 =>
                    if    data_fifo_dout(139)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(139)(23 downto 0);
                        state <= hit_140;
                    elsif data_fifo_dout(140)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(140)(23 downto 0);
                        state <= hit_141;
                    elsif data_fifo_dout(141)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(141)(23 downto 0);
                        state <= hit_142;
                    elsif data_fifo_dout(142)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(142)(23 downto 0);
                        state <= hit_143;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= hit_143;
                    end if;
                when hit_140 =>
                    if    data_fifo_dout(140)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(140)(23 downto 0);
                        state <= hit_141;
                    elsif data_fifo_dout(141)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(141)(23 downto 0);
                        state <= hit_142;
                    elsif data_fifo_dout(142)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(142)(23 downto 0);
                        state <= hit_143;
                    elsif data_fifo_dout(143)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(143)(23 downto 0);
                        state <= idle;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= idle;
                    end if;
                when hit_141 =>
                    if    data_fifo_dout(141)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(141)(23 downto 0);
                        state <= hit_142;
                    elsif data_fifo_dout(142)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(142)(23 downto 0);
                        state <= hit_143;
                    elsif data_fifo_dout(143)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(143)(23 downto 0);
                        state <= idle;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= idle;
                    end if;
                when hit_142 =>
                    if    data_fifo_dout(142)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(142)(23 downto 0);
                        state <= hit_143;
                    elsif data_fifo_dout(143)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(143)(23 downto 0);
                        state <= idle;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= idle;
                    end if;
                when hit_143 =>
                    if    data_fifo_dout(143)(24) = '1' then
                        elink_out(23 downto 0) <= data_fifo_dout(143)(23 downto 0);
                        state <= idle;
                    else
                        elink_out(23 downto 0) <= (others => '1');
                        state <= idle;
                    end if;
