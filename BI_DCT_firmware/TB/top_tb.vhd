library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.uniform;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;




entity top_tb is
end top_tb;

architecture Behavioral of top_tb is

    constant SIM : integer := 1;  -- 0: do simulation of the firmware to be implemented (hit data carry time measurement)
                                  -- 1: do simulation of the firmware with hit data carrying an unique ID instead of time measurement

    signal clock40_p, clock40_n : std_logic;
    signal clock320  : std_logic;
    signal clock_manchester : std_logic;

    signal mmcm_locked : std_logic;

    signal reset : std_logic;

    signal file_hit : std_logic_vector(287 downto 0) := (others => '0');
    signal file_hit_r : std_logic_vector(287 downto 0) := (others => '0');
    signal file_hit_r2 : std_logic_vector(287 downto 0) := (others => '0');
    
    type array288xReal is array(287 downto 0) of real;
    signal time_over_threshold : array288xReal := (others => 0.0);
    signal manchester_start_time : array288xReal := (others => 0.0);
    signal rising_time : array288xReal := (others => 0.0);
    signal falling_time : array288xReal := (others => 0.0);

    type manchester_states is (send_idle, send_error, send_data_idle, send_data);
    type manchester_states_array is array (287 downto 0) of manchester_states;
    signal manchester_state : manchester_states_array;

    type array288x18b is array(287 downto 0) of std_logic_vector(17 downto 0);
    signal tdc_data : array288x18b;

    type array288xinteger is array(287 downto 0) of integer;
    signal manchester_counter : array288xinteger := (others => 0);

    signal manchester_lines : std_logic_vector(287 downto 0);
    
    signal elink_in : std_logic_vector(27 downto 0);

    signal bcid_in : std_logic_vector(11 downto 0);
    
    signal ctrl_data    : std_logic_vector (31 downto 0);
    signal counter_32   : std_logic_vector(4 downto 0);
    signal ctrl_elink_p : std_logic_vector(3 downto 0) := (others => '0');
    signal ctrl_elink_n : std_logic_vector(3 downto 0) := (others => '1');
    signal header       : std_logic_vector(9 downto 0);
    signal write_cmd    : std_logic;
    signal read_cmd     : std_logic;
    signal reg_idx      : std_logic_vector(2 downto 0);
    signal channel_idx  : std_logic_vector(8 downto 0);
    signal datum        : std_logic_vector(7 downto 0);

    signal counter      : unsigned(11 downto 0):=(others => '0');
    signal discrim_or   : std_logic;
    
    signal word_id : array288x18b :=(others => (others=>'0'));
    signal out_word_id : std_logic_vector(17 downto 0);
  
    component mmcm_320
    port (
        clk_out320 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

begin

--  manchester transmitter clock
    clock300_gen : process
    begin
        for i in 0 to 14 loop
		    clock_manchester <= '1';
			wait for 1.69 ns;
			clock_manchester <= '0';
			wait for 1.69 ns;
		end loop;
		wait for 700 ps;
    end process;

--  LHC clock frequency 40.079 MHz
    clock40_gen : process
    begin
        clock40_p <= '0';
        clock40_n <= '1';
        wait for 12.48 ns;
        clock40_p <= '1';
        clock40_n <= '0';
        wait for 12.48 ns;
    end process;

    mmcm_logic_inst : mmcm_320
    port map (
        clk_out320 => clock320,
        locked     => mmcm_locked,
        clk_in40   => clock40_p
    );

    reset_logic : process(clock40_p) begin
        if rising_edge(clock40_p) then
            if mmcm_locked = '1' then
                reset <= '0';
            else
                reset <= '1';
            end if;
        end if;
    end process;

    top_inst : entity work.top
    generic map (
        SIM              => SIM
    )
    port map (
        eclk0_p          => clock40_p,
        eclk0_n          => clock40_n,
        discrim_or       => (others=>discrim_or),
        eta_in           => manchester_lines,
        elink_in_p       => ctrl_elink_p,
        elink_in_n       => ctrl_elink_n,
        elink_out_p      => elink_in,
        elink_out_n      => open,
        fe_clock40_p     => open,
        fe_clock40_n     => open
    );
    
    bcid_in <= << signal .top_tb.top_inst.bcid : std_logic_vector>>;
    
    --in order to generate a 8kHz signal
    discrim_or_gen: process(clock40_p)
    begin
        if rising_edge(clock40_p) then
            if counter = (5000 - 1) then
               counter <= (others => '0');
               discrim_or <= '1';
            else
               counter <= counter + 1;
               discrim_or <= '1';
            end if;
        end if;
    end process;
    

    manchester_start_logic : process(clock_manchester) begin
        if rising_edge(clock_manchester) then
            for i in 287 downto 0 loop
                if now > manchester_start_time(i)*1ns then
                    file_hit_r(i) <= file_hit(i);
                end if;
            end loop;
        end if;
    end process;
    

    manchester_lines_gen : for i in 287 downto 0 generate
        manchester_lines(i) <= '0'              when manchester_state(i) = send_error else
                               clock_manchester when manchester_state(i) = send_idle  else
                               clock_manchester when manchester_state(i) = send_data_idle  else
                               tdc_data(i)(manchester_counter(i)) XOR clock_manchester;
        manchester_fsm : process(clock_manchester) begin
            if rising_edge(clock_manchester) then
              file_hit_r2(i) <= file_hit_r(i);
                case manchester_state(i) is
                    when send_idle =>
                        tdc_data(i) <= (others => '0');
                        manchester_counter(i) <= 18;
                        if file_hit_r2(i) /= file_hit_r(i) then
                            manchester_state(i) <= send_error;
                        end if;
                    when send_error =>
                        manchester_state(i) <= send_data_idle;
                    when send_data_idle =>
                        manchester_state(i) <= send_data;
                        if SIM = 0 then
                            tdc_data(i)(17 downto 16) <= "01" when time_over_threshold(i) > 25.0 else "00";  -- BCID
                            tdc_data(i)(15 downto 8) <= std_logic_vector(to_unsigned(integer(rising_time(i)), 8));  -- rising time
                            tdc_data(i)( 7 downto 0) <= std_logic_vector(to_unsigned(integer(falling_time(i)), 8));  -- falling time
						elsif SIM = 1 then
						    tdc_data(i) <= word_id(i);
						end if;
						manchester_counter(i) <= manchester_counter(i) - 1;
                    when send_data =>
                        if manchester_counter(i) /= 0 then
                            manchester_counter(i) <= manchester_counter(i) - 1;
                        else
                            manchester_state(i) <= send_idle;
                        end if;
                    when others =>
                end case;
            end if;
        end process;
    end generate;
    
    

    file_hit_read_logic: process
        file simulation_hit_file: text;
        file generated_hit_file: text open write_mode is  "../../../../../Data/bi_generated_hits_out.txt";--the relative path in VHDL tb are relative to xsim folder location
        variable file_status : FILE_OPEN_STATUS;
        variable file_status2 : FILE_OPEN_STATUS;
        variable hits_line : line;
        variable data_line : line;
        variable hit_time : real;
        variable eta_phi : integer;
        variable l0_l1 : integer;
        variable hit_index : integer;
        variable mu_match : integer;
        variable ToT : real;
        variable hit_counter : integer := 0;
        constant space : string := " ";
    begin
        file_open(file_status, simulation_hit_file, "../../../../../Data/outfile_v2_BIL_6BC_withmu_eta3_ToT.txt", read_mode);
        assert file_status = open_ok report FILE_OPEN_STATUS'IMAGE(file_status) & " while opening file!**" severity failure;
        while not endfile(simulation_hit_file) loop
            readline(simulation_hit_file, data_line);
            read(data_line, hit_time);
            read(data_line, eta_phi);
            read(data_line, l0_l1);
            read(data_line, hit_index);
            read(data_line, mu_match);
            read(data_line, ToT); -- time over threshold
            hit_counter := hit_counter + 1;
            while now < hit_time*1ns + 4 us loop
                 wait for 1 ps;
            end loop;
            
            file_hit(hit_index + 48 * l0_l1 + 144 * eta_phi) <= not file_hit(hit_index + 48 * l0_l1 + 144 * eta_phi);
            time_over_threshold(hit_index + 48 * l0_l1 + 144 * eta_phi) <= ToT;
            manchester_start_time(hit_index + 48 * l0_l1 + 144 * eta_phi) <= hit_time + 4000.0 + ToT + 25.0 - (real(integer(hit_time + 4000.0 + ToT) mod 25) + hit_time + 4000.0 + ToT - real(integer(hit_time + 4000.0 + ToT)));
            rising_time(hit_index + 48 * l0_l1 + 144 * eta_phi) <= (real(integer(hit_time + 4000.0) mod 50) + hit_time + 4000.0 - real(integer(hit_time + 4000.0))) / 0.424;
            falling_time(hit_index + 48 * l0_l1 + 144 * eta_phi) <= (real(integer(hit_time + 4000.0 + ToT) mod 50) + hit_time + 4000.0 + ToT - real(integer(hit_time + 4000.0 + ToT))) / 0.424;
            word_id(hit_index + 48 * l0_l1 + 144 * eta_phi) <= std_logic_vector(to_unsigned(hit_counter, 18));
            
            hwrite(hits_line, bcid_in); -- current bcid
            write(hits_line, space);
            write(hits_line, hit_index + 48 * l0_l1 + 144 * eta_phi);
            write(hits_line, space);
            write(hits_line, mu_match);
            write(hits_line, space);
            write(hits_line, now);
            write(hits_line, space);
            write(hits_line, hit_counter);
   
            writeline(generated_hit_file, hits_line);
        end loop;
        file_close(simulation_hit_file);
        assert false report "END OF SIMULATION" severity failure;
        wait;
    end process;
    
    
    ---- control data, through 1 downlink elink -------
    header <= "1010101011";
    write_cmd <= '0';
    read_cmd <= '0';
    reg_idx <= "100";
    channel_idx <= "000000000";
    datum <= "00000110";
    
    ctrl_data <= header & write_cmd & read_cmd & reg_idx & channel_idx & datum;
    
    generate_ctrl_data : process(clock320, reset) begin
        if reset = '1' then
            counter_32 <= (others => '0');
        elsif rising_edge(clock320) then
            counter_32 <= counter_32 + 1;
            ctrl_elink_p(0) <= ctrl_data(to_integer(unsigned(counter_32)));
            ctrl_elink_n(0) <= not ctrl_data(to_integer(unsigned(counter_32)));
        end if;
    end process;
    
    
    write_FE_latency : process(clock_manchester)
        file FE_latency_file: text open write_mode is  "../../../../../Data/bi_FE_latency.txt";
        variable file_status  : FILE_OPEN_STATUS;
        variable elink_line : line;
        variable elink_time : time;
        constant space : string := " ";
    begin
        if rising_edge(clock_manchester) then
            for i in 287 downto 0 loop
                if file_hit_r2(i) /= file_hit_r(i) then
                    hwrite(elink_line, bcid_in);
                    write(elink_line, space);
                    write(elink_line, to_integer(unsigned(word_id(i))));
                    write(elink_line, space);
                    write(elink_line, now+70ns);
                    writeline(FE_latency_file, elink_line);
                end if;
            end loop;
        end if;
    end process;
    

    out_word_id_gen : if SIM = 1 generate
        out_word_id <= elink_in(25 downto 18) & elink_in(9 downto 0);
    end generate;

    elink_decoding_logic : process(clock320)
        file elinks_data_file: text open write_mode is  "../../../../../Data/bi_elinks_out.txt";
        variable file_status  : FILE_OPEN_STATUS;
        variable elink_line : line;
        variable elink_time : time;
        constant space : string := " ";
    begin
        if rising_edge(clock320) then
            if elink_in /= X"5555555" and elink_in(27 downto 26) = "01" then
                hwrite(elink_line, bcid_in); -- current bcid
                write(elink_line, space);
                
                if SIM = 1 then
                    write(elink_line, now);
                    write(elink_line, space);
                    write(elink_line, to_integer(unsigned(out_word_id)));
                else
                    write(elink_line, elink_in(25 downto 18)); -- hit bcid
                    write(elink_line, space);
                    if to_integer(unsigned(bcid_in(7 downto 6))) - to_integer(unsigned(elink_in(25 downto 24))) < 0 then --this is when bcid_in is >256 
                        write(elink_line, 256 + to_integer(unsigned(bcid_in(7 downto 0))) - to_integer(unsigned(elink_in(25 downto 18)))); -- BCID difference
                    else
                        write(elink_line, to_integer(unsigned(bcid_in(7 downto 0) - elink_in(25 downto 18)))); -- BCID difference
                    end if;
                    write(elink_line, space);
                    write(elink_line, to_integer(unsigned(elink_in(17 downto 10)))); -- strip number                
                    write(elink_line, space);
                    write(elink_line, elink_in(9 downto 2)); -- phi coord
                end if;
                writeline(elinks_data_file, elink_line);
            end if;
        end if;
    end process;
    
    
end Behavioral;
