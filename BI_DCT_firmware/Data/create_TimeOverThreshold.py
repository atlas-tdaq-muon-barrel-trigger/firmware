import random

# Parameters for the Gaussian distribution
mean = 20.75
stddev = 3.244

# Open the input and output files
with open('outfile_v2_BIL_6BC_withmu_eta3_addsides_sorted.txt', 'r') as infile, open('outfile_v2_BIL_6BC_withmu_eta3_ToT.txt', 'w') as outfile:
    for line in infile:
        # Generate a random number from the Gaussian distribution
        random_number = random.gauss(mean, stddev)
        # Write the line to the output file with the random number appended
        outfile.write(f"{line.strip()} {random_number:.3f}\n")