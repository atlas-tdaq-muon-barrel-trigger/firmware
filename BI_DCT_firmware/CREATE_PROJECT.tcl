set ProjectDir ./

if {[file exists $ProjectDir/Vivado]} {[file rename $ProjectDir/Vivado $ProjectDir/Vivado.[clock format [clock seconds] -format %Y-%m-%d.%H-%M-%S]]}

file mkdir $ProjectDir/Vivado

#create_project DCT_BI $ProjectDir/Vivado -part xc7k325tffg900-2
create_project DCT_BI $ProjectDir/Vivado -part xc7k325tfbg900-2

set_property target_language VHDL [current_project]

add_files -norecurse $ProjectDir/IP/mmcm_200_600/mmcm_200_600.xci
add_files -norecurse $ProjectDir/IP/mmcm_320/mmcm_320.xci
add_files -norecurse $ProjectDir/IP/deserialiser/deserialiser.xci
add_files -norecurse $ProjectDir/IP/fifo_16x28b/fifo_16x28b.xci
add_files -norecurse $ProjectDir/IP/fifo_16x56b/fifo_16x56b.xci
add_files -norecurse $ProjectDir/IP/fifo_16x48b/fifo_16x48b.xci

generate_target all [get_files $ProjectDir/IP/mmcm_200_600/mmcm_200_600.xci]
generate_target all [get_files $ProjectDir/IP/mmcm_320/mmcm_320.xci]
generate_target all [get_files $ProjectDir/IP/deserialiser/deserialiser.xci]
generate_target all [get_files $ProjectDir/IP/fifo_16x28b/fifo_16x28b.xci]
generate_target all [get_files $ProjectDir/IP/fifo_16x56b/fifo_16x56b.xci]
generate_target all [get_files $ProjectDir/IP/fifo_16x48b/fifo_16x48b.xci]

add_files -norecurse $ProjectDir/RTL/manchester_decoding.vhd
add_files -norecurse $ProjectDir/RTL/user_pkg.vhd
add_files -norecurse $ProjectDir/RTL/rate_counter.vhd
add_files -norecurse $ProjectDir/RTL/edge_detector.vhd
add_files -norecurse $ProjectDir/RTL/top.vhd
add_files -norecurse $ProjectDir/RTL/frame_aligner.vhd
add_files -norecurse $ProjectDir/RTL/data_handler.vhd
add_files -norecurse $ProjectDir/RTL/data_derandomizer.vhd

add_files -fileset constrs_1 -norecurse $ProjectDir/Constraints/top.xdc

set_property SOURCE_SET sources_1 [get_filesets sim_1]
add_files -fileset sim_1 -norecurse $ProjectDir/TB/top_tb.vhd

set_property AUTO_INCREMENTAL_CHECKPOINT 0 [get_runs synth_1]
