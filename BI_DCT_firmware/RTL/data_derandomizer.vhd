-------------------------------------------------------
-- File:          data_derandomizer.vhd
-- Project:       BI DCT firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/16
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
library xil_defaultlib;

entity data_derandomizer is
    generic (logic_index : integer range 0 to 7);
    port (
        clock_in      : in  std_logic;
        reset_in      : in  std_logic;
        we_in         : in  std_logic;
        data_in       : in  std_logic_vector(21 downto 0); -- 8 bit BCID + 8 bit rising time + 6 bit falling time
        BCID_data_in  : in  integer range 0 to 7;
        data_out      : out std_logic_vector(21 downto 0) 
    );
end data_derandomizer;

architecture RTL of data_derandomizer is

    signal data : std_logic_vector(21 downto 0) := (others => '0');
    
begin

    
    process (clock_in)
    begin
        if rising_edge(clock_in) then
            if reset_in = '1' then
                data <= (others => '0');
            elsif we_in = '1' and BCID_data_in = logic_index then 
                data(21 downto 14) <= data_in(21 downto 14); --bcid
                -- the following "if" is in case rising and falling time do not arrive at same clock cycle 
                if data_in(13 downto 6) /= 0 then --rising time
                    data(13 downto 6) <= data_in(13 downto 6); 
                end if;
                if data_in(5 downto 0) /= 0 then -- falling time
                    data(5 downto 0) <= data_in(5 downto 0); 
                end if;
            end if;
        end if;
    end process;
    
    data_out <= data;
    
    

end RTL;
