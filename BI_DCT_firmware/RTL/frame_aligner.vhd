-------------------------------------------------------
-- File:          frame_aligner.vhd
-- Project:       BI DCT firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/04/03
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.ALL;


entity framealigner is 
    generic (
        header                 : std_logic_vector(9 downto 0);
        requiredCorrectHeaders : integer range 0 to 7;
        maxFalseHeaders        : integer range 0 to 7;
        requiredCheckedHeaders : integer range 0 to 7
    );
    port (
        clock           : in std_logic;
        reset           : in std_logic;
        data_in         : in std_logic;
        ctrl_word       : out std_logic_vector(31 downto 0);
        ctrl_word_valid : out std_logic 
    );
 end framealigner;
 
 
 architecture behavioral of framealigner is 
    
    type machine is (UNLOCKED, GOING_LOCKED, LOCKED, GOING_UNLOCKED);
    
    signal data_32b             : std_logic_vector(31 downto 0);
    signal bitslip              : std_logic;
    signal state                : machine;
    signal consecCorrectHeaders : integer range 0 to 7;
    signal consecFalseHeaders   : integer range 0 to 7;
    signal checkedHeaders       : integer range 0 to 7;
    signal counter              : std_logic_vector(4 downto 0) := (others => '0');
 
 begin 
 
    shift_reg : process(clock) begin
        if rising_edge(clock) then
            data_32b <= data_in & data_32b(31 downto 1);  -- LSB is the OLDEST
        end if;
    end process;
    
    


    framealigner_FSM : process(clock, reset) begin
        if reset = '1' then
            bitslip <= '0';
            state <= UNLOCKED;
            ctrl_word_valid <= '0';
        elsif rising_edge(clock) then
            if bitslip = '1' then
                counter <= counter;
            else
                counter <= counter + 1;
            end if;
            
            if counter = "00000" then
            
                case state is
                
                    when UNLOCKED => if data_32b(data_32b'length - 1 downto  data_32b'length - header'length ) /= header then
                                         bitslip <= '1';
                                     else
                                         state <= GOING_LOCKED;
                                         consecCorrectHeaders <= 0;
                                     end if;
                    
                    when GOING_LOCKED => if data_32b(data_32b'length - 1 downto  data_32b'length - header'length ) /= header then
                                           state <= UNLOCKED;
                                       else
                                           consecCorrectHeaders <= consecCorrectHeaders +1;
                                           if  consecCorrectHeaders >= requiredCorrectHeaders then
                                               state <= LOCKED;
                                               ctrl_word_valid <= '1';
                                           end if;
                                       end if;
                    
                    when LOCKED => if data_32b(data_32b'length - 1 downto  data_32b'length - header'length) /= header then
                                       consecFalseHeaders <= 0;
                                       checkedHeaders <= 0;
                                       state <= GOING_UNLOCKED; 
                                   else
                                       ctrl_word_valid <= '1';
                                   end if;
                    
                    when GOING_UNLOCKED => if data_32b(data_32b'length - 1 downto  data_32b'length - header'length) = header then
                                               if checkedHeaders = requiredCheckedHeaders then
                                                   state <= LOCKED;
                                               else 
                                                   checkedHeaders <= checkedHeaders +1;
                                               end if;
                                           else 
                                               consecFalseHeaders <= consecFalseHeaders + 1;
                                               if consecFalseHeaders >= maxFalseHeaders then 
                                                   state <= UNLOCKED;
                                               end if;
                                         end if;  
                    when others => state <= UNLOCKED;
                end case;
             else 
                bitslip <= '0';
                ctrl_word_valid <= '0';
            end if;
        end if;
    end process;
    
    ctrl_word <= data_32b;

 
 end behavioral;
