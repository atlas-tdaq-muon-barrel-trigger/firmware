------------------------------------------------------- 
-- File:          top.vhd
-- Project:       BI DCT firmware
-- Author:        Riccardo Vari, Federico Morodei
-- Last modified: 2023/10/04
-------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

library STD;
use STD.textio.all;

use work.user_pkg.all;

--  lpGBT clocks:
--
--    Phase/Frequency - 4 programmable clocks:
--      - 4 independent
--      - Phase resolution: 50 ps
--      - Frequencies: 40 / 80 / 160 / 320 / 640 / 1280 MHz
--
--    eLink Clocks:
--      - 28 independent
--      - Fixed phase
--      - Frequency programable: 40 / 80 / 160 / 320 / 640 / 1280 MHz
--
--    Clock jitter < 5 ps rms
--

entity top is
generic(
    SIM : integer := 0  -- 0 for firmware implementation
                        -- 1 for simulation
);
port (
    eclk0_p     : in  std_logic;
    eclk0_n     : in  std_logic;
    discrim_or  : in  std_logic_vector(35 downto 0);
    eta_in      : in  std_logic_vector(287 downto 0);
    elink_in_p  : in  std_logic_vector(3 downto 0);
    elink_in_n  : in  std_logic_vector(3 downto 0);
    elink_out_p : out std_logic_vector(27 downto 0);
    elink_out_n : out std_logic_vector(27 downto 0);
    fe_clock40_p : out std_logic_vector(35 downto 0);
    fe_clock40_n : out std_logic_vector(35 downto 0)
    );
end top;



architecture RTL of top is

    -- BI DCT data format (2 consecutive 28-bit words)
    -- 1st word: 2-bit word ID ("01") + 8-bit BCID + 8-bit strip ID + 8-bit virtual phi coordinate  + 2-bit right falling time (first 2 bits of the 6 MSB)
    -- 2nd word: 2-bit word ID ("10") + 8-bit left rising time + 6-bit (MSB) left falling time + 8-bit right rising time + 4-bit right falling time (last 4 bits of the 6 MSB)
    
    -- monitoring data format (28-bit word)
    -- 2-bit word ID + 26-bit monitoring data
        
    -- strip index scheme of FE-channels
    -- 47-0:    eta first layer,  left  reading
    -- 95-48:   eta second layer, left  reading 
    -- 143-96:  eta third layer,  left  reading
    -- 191-144: eta first layer,  right reading
    -- 239-192: eta second layer, right reading 
    -- 287-240: eta third layer,  right reading
    
    signal clock40      : std_logic;
    signal clock40_bufg : std_logic;

    signal clock40_edge      : std_logic := '0';
    signal clock40_edge320_r : std_logic;
    signal clock40_edge200_r : std_logic;
    signal clock40_rising320 : std_logic;
    signal clock40_rising200 : std_logic;

    signal clock600_mmcm : std_logic;
    signal clock200_mmcm : std_logic;
    signal clock320_mmcm : std_logic;

    signal mmcm1_locked   : std_logic;
    signal mmcm2_locked   : std_logic;
    signal mmcm_locked_r0 : std_logic;
    signal mmcm_locked_r1 : std_logic;

    signal int_reset : std_logic;

    signal elink_out : std_logic_vector(27 downto 0);

    subtype integer018 is integer range 0 to 18;
    signal fifo_out_num_out : integer018;
    
    signal bcid         : std_logic_vector(11 downto 0);
    signal subbcid      : array_18x3b := (others => (others => '0'));
    signal subbcid_r    : array_18x3b := (others => (others => '0'));
    signal bcid_18x12b  : array_18x12b := (others => (others => '0'));
    signal bcid_init    : array_18x12b := (others => (others => '0'));
    signal bcid_288x12b : array_288x12b;

    signal datain_288x6b : array_288x6b := (others => (others => '0'));

    signal fifo_in_we    : std_logic_vector(143 downto 0);
    signal fifo_in_re    : std_logic_vector(143 downto 0);
    signal fifo_in_dout  : array_144x48b := (others => (others => '0'));
    signal fifo_in_empty : std_logic_vector(143 downto 0);
    signal fifo_in_full  : std_logic_vector(143 downto 0) := (others => '0');

    signal fifo_out_din       : array_18x56b := (others => (others => '0'));
    signal fifo_out_we        : std_logic_vector(17 downto 0);
    signal fifo_out_re        : std_logic_vector(17 downto 0);
    signal fifo_out_dout      : array_18x56b := (others => (others => '0'));
    signal fifo_out_empty     : std_logic_vector(17 downto 0) := (others => '1');
    signal fifo_out_full      : std_logic_vector(17 downto 0) := (others => '0');
    signal fifo_out_datacount : array_18x4b;

    signal fifo_out_re_r   : std_logic_vector(17 downto 0);
    signal fifo_out_dout_r : array_18x56b := (others => (others => '0'));

    signal manchester_288x18b      : array_288x18b := (others => (others => '0'));
    signal manchester_BCID         : array_288x2b  := (others => (others => '0'));
    signal manchester_valid        : std_logic_vector(287 downto 0);
    signal masked_manchester_valid : std_logic_vector(287 downto 0);
    
    signal elink_in : std_logic_vector(3 downto 0);
    
    signal channel_mask                 : std_logic_vector(287 downto 0) := (others => '0');
    signal timing_offset_in             : array_18x8b := (others => (others => '0'));
    signal timing_offset                : array_288x8b := (others => (others => '0'));
    signal coarse_time_offset           : array_18x4b := (others => (others => '0'));
    signal BC_offset                    : std_logic_vector(5 downto 0) := (others => '0');
    
    signal phi                       : array_144x8b := (others => (others => '0'));
    signal rising_time               : array_288x8b;
    signal falling_time              : array_288x8b;
    signal fifo_out_second_half_en   : std_logic;
    signal fifo_out_dout_second_half : std_logic_vector(27 downto 0);
    signal ordered_time              : array_288x14b := (others => (others => '0'));
    signal sim_data                  : array_288x18b := (others => (others => '0'));
    signal sim_data_sel              : array_144x18b := (others => (others => '0'));

    signal ctrl_word          : std_logic_vector(31 downto 0);
    signal ctrl_word_valid    : std_logic;
    signal ctrl_header        : std_logic_vector(9 downto 0);
    signal ctrl_WR            : std_logic_vector(1 downto 0);
    signal ctrl_reg_idx       : std_logic_vector(2 downto 0);
    signal ctrl_channel_idx   : integer range 0 to 511; 
    signal ctrl_data          : std_logic_vector(7 downto 0);
    signal read_ctrl_word     : std_logic_vector(27 downto 0);
    signal read_en_ctrl_fifo  : std_logic;
    signal write_en_ctrl_fifo : std_logic := '0';
    signal ctrl_fifo_dout     : std_logic_vector(27 downto 0);
    signal ctrl_fifo_empty    : std_logic := '1';
    
    signal dead_time              : std_logic_vector(2 downto 0);  -- given in number of BCs
    signal data_in_valid          : std_logic_vector(287 downto 0) := (others => '1');
    signal n_clock_cycles_skipped : array_288x6b := (others => (others => '0'));
    signal we_data_handler        : std_logic_vector(287 downto 0);
    signal ordered_bcid           : array_144x8b;
    
    signal prep_ctrl_data_out : std_logic;
    signal ctrl_data_out      : std_logic_vector(27 downto 0);
    signal ctrl_fifo_empty_r  : std_logic;
    
    signal feclock40 : std_logic_vector(or_signals-1 downto 0);
    signal or_pulse  : std_logic_vector(or_signals-1 downto 0);
    signal or_pulse_40MHz_domain : std_logic_vector(or_signals-1 downto 0);
    signal counter_rate : array_pulse_rate_counters;

    attribute keep : string;
    attribute keep of counter_rate : signal is "TRUE";
     
    constant bcid_max             : std_logic_vector (11 downto 0) := x"deb"; --3563
    signal out_index_data_handler : integer range 0 to 7 := 0;
    signal ctrl_word_200MHz       : std_logic_vector(31 downto 0);
    signal ctrl_word_valid_200MHz : std_logic;
    signal read_ctrl_word_320MHz  : std_logic_vector(27 downto 0);
    signal write_en_ctrl_fifo_320MHz : std_logic;
    
    signal int_reset200              : std_logic;
    signal int_reset320              : std_logic;
    constant delay                   : integer := 25;
    signal int_reset200_delayed      : std_logic_vector(delay downto 0) := (others => '0');
    signal clock40_rising200_delayed : std_logic_vector(delay downto 0) := (others => '0');

    component deserialiser
    port (
        data_in_from_pins : in  std_logic;
        data_in_to_device : out std_logic_vector(5 downto 0);
        bitslip           : in  std_logic;
        clk_in            : in  std_logic;
        clk_div_in        : in  std_logic;
        io_reset          : in  std_logic
    );
    end component;

    component mmcm_200_600
    port (
        clk_out600 : out std_logic;
        clk_out200 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

    component mmcm_320
    port (
        clk_out320 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

    component fifo_16x56b
    port (
        clk        : in  std_logic;
        din        : in  std_logic_vector(55 downto 0);
        wr_en      : in  std_logic;
        rd_en      : in  std_logic;
        dout       : out std_logic_vector(55 downto 0);
        full       : out std_logic;
        empty      : out std_logic;
        data_count : out std_logic_vector(3 downto 0)
    );
    end component;
    
    component fifo_16x28b
    port (
        clk        : in  std_logic;
        din        : in  std_logic_vector(27 downto 0);
        wr_en      : in  std_logic;
        rd_en      : in  std_logic;
        dout       : out std_logic_vector(27 downto 0);
        full       : out std_logic;
        empty      : out std_logic;
        data_count : out std_logic_vector(3 downto 0)
    );
    end component;

    component fifo_16x48b
    port (
      wr_clk : in std_logic;
      rd_clk : in std_logic;
      din    : in std_logic_vector(47 downto 0);
      wr_en  : in std_logic;
      rd_en  : in std_logic;
      dout   : out std_logic_vector(47 downto 0);
      full   : out std_logic;
      empty  : out std_logic
    );
    end component;
    
    
    
    component framealigner 
    generic (
        header                 : std_logic_vector(9 downto 0);
        requiredCorrectHeaders : integer range 0 to 7;
        maxFalseHeaders        : integer range 0 to 7;
        requiredCheckedHeaders : integer range 0 to 7
    );
    port (
        clock           : in std_logic;
        reset           : in std_logic;
        data_in         : in std_logic;
        ctrl_word       : out std_logic_vector(31 downto 0);
        ctrl_word_valid : out std_logic 
    );
    end component;
    

begin

    
    elink_in_diff_buff_gen : for i in 3 downto 0 generate
        IBUFDS_inst : IBUFDS
        generic map (
            IOSTANDARD => "DIFF_HSUL_12")
        port map (
            I  => elink_in_p(i),
            IB => elink_in_n(i),
            O  => elink_in(i)
        );
    end generate;

    elink_out_diff_buff_gen : for i in 27 downto 0 generate
        OBUFDS_inst : OBUFDS
        generic map (
            IOSTANDARD => "DIFF_HSUL_12",
            SLEW       => "SLOW")   -- Specify the output slew rate
        port map (
            O  => elink_out_p(i),
            OB => elink_out_n(i),
            I  => elink_out(i)
        );
    end generate;

    clock40_diff_buff : IBUFDS
    generic map (
        DIFF_TERM    => FALSE,
        IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
        IOSTANDARD   => "DIFF_HSUL_12")
    port map (
        I  => eclk0_p,
        IB => eclk0_n,
        O  => clock40
    );

    bufg_inst : BUFG
    port map (
      O => clock40_bufg,
      I => clock40
    );

    mmcm_for_deserialisers_inst : mmcm_200_600
    port map (
        clk_out600 => clock600_mmcm,
        clk_out200 => clock200_mmcm,
        locked     => mmcm1_locked,
        clk_in40   => clock40_bufg
    );

    mmcm_for_logic_inst : mmcm_320
    port map (
        clk_out320 => clock320_mmcm,
        locked     => mmcm2_locked,
        clk_in40   => clock40_bufg
    );

    int_reset_logic : process(clock40_bufg) begin
        if rising_edge(clock40_bufg) then
            mmcm_locked_r0 <= mmcm1_locked and mmcm2_locked;
            mmcm_locked_r1 <= mmcm_locked_r0;
            if mmcm_locked_r1 = '1' then
                int_reset <= '0';
            else
                int_reset <= '1';
            end if;
        end if;
    end process;
    
   xpm_cdc_sync_rst200_inst : xpm_cdc_sync_rst
   generic map (
      DEST_SYNC_FF => 4,   
      INIT => 1,          
      INIT_SYNC_FF => 0,   
      SIM_ASSERT_CHK => 0  
   )
   port map (
      dest_rst => int_reset200, 
      dest_clk => clock200_mmcm,
      src_rst => int_reset
   );
   
   xpm_cdc_sync_rst320_inst : xpm_cdc_sync_rst
   generic map (
      DEST_SYNC_FF => 4,   
      INIT => 1,          
      INIT_SYNC_FF => 0,   
      SIM_ASSERT_CHK => 0  
   )
   port map (
      dest_rst => int_reset320, 
      dest_clk => clock320_mmcm,
      src_rst => int_reset
   );


    clock40_edge_logic : process(clock40_bufg) begin
        if rising_edge(clock40_bufg) then
            clock40_edge <= not clock40_edge;
        end if;
    end process;


    clock40_rising200_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            clock40_edge200_r <= clock40_edge;
            if clock40_edge200_r /= clock40_edge then
                clock40_rising200 <= '1';
            else
                clock40_rising200 <= '0';
            end if;
        end if;
    end process;
    
    
    clock40_rising320_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            clock40_edge320_r <= clock40_edge;
            if clock40_edge320_r /= clock40_edge then
                clock40_rising320 <= '1';
            else
                clock40_rising320 <= '0';
            end if;
        end if;
    end process;
    
    bcid_shift_reg : process(clock200_mmcm) begin
         if rising_edge(clock200_mmcm) then
            int_reset200_delayed <= int_reset200_delayed(delay-1 downto 0) & int_reset200; --bit 21 is the oldest
            clock40_rising200_delayed <= clock40_rising200_delayed(delay-1 downto 0) & clock40_rising200;
         end if;
    end process;
    
    bcid_counter_logic : process(clock200_mmcm) begin -- to sync bcid with last incoming bit. To do: sync wrt new decoding logic
        if rising_edge(clock200_mmcm) then
            for i in 17 downto 0 loop
                bcid_init(i) <= ("000000" & BC_offset) + ("00000000" & coarse_time_offset(i)); 
            end loop;
        
            if int_reset200 = '1' then --to do: replace with reset from elink
                bcid <= "000000" & BC_offset;
            else
                if clock40_rising200 = '1' then
                    if bcid = bcid_max then
                        bcid <= "000000" & BC_offset;
                    else 
                        bcid <= bcid + 1;
                    end if;
                end if;
            end if;
            
            if int_reset200_delayed(delay) = '1' then
                for i in 17 downto 0 loop
                    bcid_18x12b(i) <= bcid_init(i);
                end loop;
            else
                if clock40_rising200_delayed(delay) = '1' then
                    for i in 17 downto 0 loop
                        if bcid_18x12b(i) = bcid_max then
                            bcid_18x12b(i) <= bcid_init(i);
                        else
                            bcid_18x12b(i) <= bcid_18x12b(i) + 1;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;
    
    


    bcid_mapping : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 17 downto 0 loop
                for k in 15 downto 0 loop
                    bcid_288x12b(k+16*i) <= bcid_18x12b(i);
                end loop;
            end loop;
        end if;
    end process;

    
    timing_offset_mapping : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 17 downto 0 loop
                for k in 15 downto 0 loop
                    timing_offset(k+16*i) <= timing_offset_in(i);
                end loop;
            end loop;
        end if;
    end process;
    
    subbcid_counter_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            for i in 17 downto 0 loop
            subbcid_r(i) <= subbcid(i);
                if clock40_rising320 = '1' then
                    subbcid(i) <= "000";
                else
                    subbcid(i) <= subbcid(i) + 1;
                end if;
            end loop;
        end if;
    end process;
     

    deserialisers : for i in 287 downto 0 generate
        deser_inst : deserialiser
        port map (
            data_in_from_pins => eta_in(i),
            data_in_to_device => datain_288x6b(i),   -- BIT0 is the OLDEST
            bitslip           => '0',
            clk_in            => clock600_mmcm,
            clk_div_in        => clock200_mmcm,
            io_reset          => int_reset200
        );
    end generate;
    
    manchester_gen : for i in 287 downto 0 generate
        manchester_decoding_inst : entity work.manchester_decoding
        port map (
            clock          => clock200_mmcm,
            reset          => int_reset200,
            data6b_in      => datain_288x6b(i),
            data18b_out    => manchester_288x18b(i), -- 2-bit BCID + 8-bit rising time + 8-bit falling time 
            data18b_we_out => manchester_valid(i),
            debug_m_100b_out                => open,
            debug_start_counter_out         => open,
            debug_fsm_error_counter_out     => open,
            debug_decoded_words_counter_out => open
        );
    end generate;

    data_handler_we_gen : for i in 287 downto 0 generate 
        masked_manchester_valid(i) <= manchester_valid(i) and (not channel_mask(i));
    end generate;
    
    apply_dead_time : process(clock200_mmcm)
    begin
        if rising_edge(clock200_mmcm) then
            if int_reset200 = '1' then
                data_in_valid <= (others => '1');
            else
                for i in 287 downto 0 loop
                    rising_time(i)  <= manchester_288x18b(i)(15 downto 8) + timing_offset(i);
                    falling_time(i) <= manchester_288x18b(i)(7 downto 0) + timing_offset(i);
                    manchester_BCID(i) <= manchester_288x18b(i)(17 downto 16);
                    
                    if data_in_valid(i) = '0' then
                        n_clock_cycles_skipped(i) <= n_clock_cycles_skipped(i) + 1;
                        we_data_handler(i) <= '0';
                        if n_clock_cycles_skipped(i) = (to_integer(unsigned(dead_time)) * 5) - 1 then
                            data_in_valid(i) <= '1';
                        end if;
                    else
                        we_data_handler(i) <= masked_manchester_valid(i);
                        if masked_manchester_valid(i) = '1' and dead_time /= 0 then
                            data_in_valid(i) <= '0';
                            n_clock_cycles_skipped(i) <= (others => '0'); -- start counting clock cycles to be skipped for dead time
                        end if;
                    end if;
                end loop;
            end if;
        end if;
    end process;
    
    process(clock200_mmcm) 
    begin
        if rising_edge(clock200_mmcm) then
            for i in 287 downto 0 loop
                if clock40_rising200 = '1' then
                    if we_data_handler(i) = '1' then
                        ordered_time(i) <= rising_time(i) & falling_time(i)(7 downto 2);
                        if SIM = 1 then
                            sim_data(i) <= manchester_BCID(i) & rising_time(i) & falling_time(i);
                        end if;
                    else 
                        ordered_time(i) <= (others => '0');
                        if SIM = 1 then
                            sim_data(i) <= (others => '0');
                        end if;
                    end if;
                elsif we_data_handler(i) = '1' then
                    ordered_time(i) <= rising_time(i) & falling_time(i)(7 downto 2);
                    if SIM = 1 then
                        sim_data(i) <= manchester_BCID(i) & rising_time(i) & falling_time(i);
                    end if;
                end if;
            end loop;
        end if;            
    end process;
    
    
    phi_computation: for i in 143 downto 0 generate --left rising time - right rising time
        phi(i) <= ordered_time(i)(13 downto 6) - ordered_time(i+144)(13 downto 6); 
    end generate;
    
    define_bcid : process(clock200_mmcm) 
    begin
        if rising_edge(clock200_mmcm) then
            for i in 143 downto 0 loop
                if we_data_handler(i) = '1' then
                    if manchester_BCID(i) = "01" then
                        ordered_bcid(i) <= bcid_288x12b(i)(7 downto 0) - 1;
                    else
                        ordered_bcid(i) <= bcid_288x12b(i)(7 downto 0);
                    end if;
                elsif we_data_handler(i+144) = '1' then
                    if manchester_BCID(i+144) = "01" then
                        ordered_bcid(i) <= bcid_288x12b(i+144)(7 downto 0) - 1;
                    else
                        ordered_bcid(i) <= bcid_288x12b(i+144)(7 downto 0);
                    end if;
                end if;
            end loop;
        end if;
    end process;
    
        
    simulation_fifo_in_gen : if SIM = 1 generate
    
        fifo_in_we_gen: for i in 143 downto 0 generate -- zero suppression after reordering, write enable each 25 ns
            fifo_in_we(i) <= clock40_rising200 and (or_reduce(sim_data(i)) or or_reduce(sim_data(i+144)));
        end generate;
     
        select_fifo_input :for i in 143 downto 0 generate
            sim_data_sel(i) <= sim_data(i) when sim_data(i) /= 0 else sim_data(i+144);
        end generate;
        
     
        fifo_in_gen: for i in 143 downto 0 generate   
            fifo_in: fifo_16x48b
            port map (
                wr_clk            => clock200_mmcm,
                rd_clk            => clock320_mmcm,
                din(47 downto 46) => "01",                             -- 1st word ID
                din(45 downto 28) => sim_data_sel(i),
                din(27 downto 26) => "10",                             -- 2nd word ID
                din(25 downto 0)  => (others => '0'),
                wr_en             => fifo_in_we(i), 
                rd_en             => fifo_in_re(i),
                dout              => fifo_in_dout(i),
                full              => fifo_in_full(i),
                empty             => fifo_in_empty(i)
            );
        end generate;
    
    end generate;
    
    implementation_fifo_in_gen : if SIM = 0 generate
    
        fifo_in_we_gen: for i in 143 downto 0 generate -- zero suppression after reordering, write enable each 25 ns
            fifo_in_we(i) <= clock40_rising200 and (or_reduce(ordered_time(i)) or or_reduce(ordered_time(i+144)));
        end generate;
     
        fifo_in_gen: for i in 143 downto 0 generate   
            fifo_in: fifo_16x48b
            port map (
                wr_clk            => clock200_mmcm,
                rd_clk            => clock320_mmcm,
                din(47 downto 46) => "01",                             -- 1st word ID
                din(45 downto 38) => ordered_bcid(i),                  -- bcid
                din(37 downto 30) => phi(i),                           -- "virtual" phi coordinate
                din(29 downto 28) => ordered_time(i+144)(5 downto 4),  -- first 2 bits of falling time right reading
                din(27 downto 26) => "10",                             -- 2nd word ID
                din(25 downto 18) => ordered_time(i)(13 downto 6),     -- rising time left reading
                din(17 downto 12) => ordered_time(i)(5 downto 0),      -- falling time left reading
                din(11 downto 4)  => ordered_time(i+144)(13 downto 6), -- rising time right reading
                din(3 downto 0)   => ordered_time(i+144)(3 downto 0),  -- last 4 bits of falling time right reading
                wr_en             => fifo_in_we(i), 
                rd_en             => fifo_in_re(i),
                dout              => fifo_in_dout(i),
                full              => fifo_in_full(i),
                empty             => fifo_in_empty(i)
            );
        end generate;
    
    end generate;
    
    
    
    fifo_out_gen : for i in 0 to 17 generate 
        fifo_out : fifo_16x56b   
        port map (
            clk        => clock320_mmcm,
            din        => fifo_out_din(i),
            wr_en      => fifo_out_we(i),
            rd_en      => fifo_out_re(i),
            dout       => fifo_out_dout(i),
            full       => fifo_out_full(i),
            empty      => fifo_out_empty(i),
            data_count => fifo_out_datacount(i)
        );
    end generate;

    fifo_in_re_gen : for i in 0 to 17 generate  
        fifo_in_re_logic : process(clock320_mmcm)
        begin
            if rising_edge(clock320_mmcm) then
                if subbcid(i) = "000" and fifo_in_empty(i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"01";
                elsif subbcid(i) = "001" and fifo_in_empty(1+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"02";
                elsif subbcid(i) = "010" and fifo_in_empty(2+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"04";
                elsif subbcid(i) = "011" and fifo_in_empty(3+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"08";
                elsif subbcid(i) = "100" and fifo_in_empty(4+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"10";
                elsif subbcid(i) = "101" and fifo_in_empty(5+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"20";
                elsif subbcid(i) = "110" and fifo_in_empty(6+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"40";
                elsif subbcid(i) = "111" and fifo_in_empty(7+i*8) = '0' and fifo_out_datacount(i) < x"e" and fifo_out_full(i) = '0' then 
                    fifo_in_re(7+i*8 downto i*8) <= X"80";
                else
                    fifo_in_re(7+i*8 downto i*8) <= X"00";
                end if;
            end if;
        end process;
    end generate;

    fifo_out_we_gen : for i in 0 to 17 generate  
        fifo_out_we_logic : process(clock320_mmcm)
        begin
            if rising_edge(clock320_mmcm) then
                if    fifo_in_re(7+i*8 downto i*8) = X"01" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"02" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+1)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+1, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+1)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"04" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+2)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+2, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+2)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"08" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+3)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+3, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+3)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"10" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+4)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+4, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+4)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"20" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+5)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+5, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+5)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"40" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+6)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+6, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+6)(37 downto 0);
                    fifo_out_we(i) <= '1';
                elsif fifo_in_re(7+i*8 downto i*8) = X"80" then
                    fifo_out_din(i)(55 downto 46) <= fifo_in_dout(i*8+7)(47 downto 38);
                    fifo_out_din(i)(45 downto 38) <= std_logic_vector(to_unsigned(i*8+7, 8));
                    fifo_out_din(i)(37 downto 0) <= fifo_in_dout(i*8+7)(37 downto 0);
                    fifo_out_we(i) <= '1';
                else
                    fifo_out_we(i) <= '0';
                end if;
            end if;
        end process;
    end generate;


    fifo_out_re_logic : process(clock320_mmcm) 
    begin
        if rising_edge(clock320_mmcm) then
            fifo_out_re <= (others => '0');  --default value
            for i in 17 downto 0 loop
                if fifo_out_empty(i) = '0' and fifo_out_re = 0 and fifo_out_re_r(i) = '0' then 
                    fifo_out_re(i) <= '1';
                    exit; --exit the loop once the condition is met
                end if;
            end loop;
        end if;
    end process;



    fifo_out_num_out_logic : process(clock320_mmcm)
    begin
        if rising_edge(clock320_mmcm) then
            fifo_out_num_out <= 18; --default value
            for i in 17 downto 0 loop
                if fifo_out_re_r(i) = '1' then
                    fifo_out_num_out <= i;
                    exit; --exit loop once the first '1' is found
                end if; 
            end loop;
            fifo_out_re_r <= fifo_out_re;
            fifo_out_dout_r <= fifo_out_dout;
        end if;
    end process;

    elink_out_logic : process(clock320_mmcm)
    begin
        if rising_edge(clock320_mmcm) then
            if fifo_out_second_half_en = '1' then
                elink_out <= fifo_out_dout_second_half;
                fifo_out_second_half_en <= '0';
            else
                if fifo_out_num_out < 18 then 
                    elink_out <= fifo_out_dout_r(fifo_out_num_out)(55 downto 28);  -- first half of fifo out word
                    fifo_out_dout_second_half <= fifo_out_dout_r(fifo_out_num_out)(27 downto 0);  -- second half of fifo out word
                    read_en_ctrl_fifo <= '0';
                    fifo_out_second_half_en <= '1';
                    prep_ctrl_data_out <= '0';
                else
                    fifo_out_second_half_en <= '0';
                    prep_ctrl_data_out <= '1';
                    elink_out <= ctrl_data_out;
                end if;
                
                if prep_ctrl_data_out = '1' then
                    ctrl_fifo_empty_r <= ctrl_fifo_empty;
                    if ctrl_fifo_empty_r = '0' then
                        ctrl_data_out <= ctrl_fifo_dout;
                        read_en_ctrl_fifo <= '1';
                    else
                        ctrl_data_out <= X"5555555"; -- for simulation
                        read_en_ctrl_fifo <= '0';
                    end if;
                end if;
                
            end if;
        end if;
    end process;
    
   


    ----- Logic to read and write configuration registers -------------

    framealigner_inst : framealigner  
    generic map(
        header                 => "1010101011",
        requiredCorrectHeaders => 5,
        maxFalseHeaders        => 3,
        requiredCheckedHeaders => 4
    )
    port map(
        clock           => clock320_mmcm,
        reset           => int_reset320,
        data_in         => elink_in(0),
        ctrl_word       => ctrl_word,
        ctrl_word_valid => ctrl_word_valid
    );
    
    ctrl_word_cdc_320to200MHz : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,
        INIT_SYNC_FF   => 0,
        SIM_ASSERT_CHK => 0, 
        SRC_INPUT_REG  => 1,
        WIDTH          => 32
    )
    port map (
        dest_out => ctrl_word_200MHz,
        dest_clk => clock200_mmcm,
        src_clk  => clock320_mmcm,
        src_in   => ctrl_word
    );
    
    
    ctrl_word_valid_cdc_inst : xpm_cdc_pulse
     generic map (
        DEST_SYNC_FF => 2,   
        INIT_SYNC_FF => 0,   
        REG_OUTPUT => 0,     
        RST_USED => 0,       
        SIM_ASSERT_CHK => 0  
     )
     port map (
        dest_pulse => ctrl_word_valid_200MHz, 
        dest_clk => clock200_mmcm,     
        dest_rst =>'0',     
        src_clk => clock320_mmcm,       
        src_pulse => ctrl_word_valid,   
        src_rst => '0'        
     );       
     
    
    WR_conf_registers_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if int_reset200 = '1' then
                channel_mask          <= (others => '0');
                BC_offset             <= (others => '0');
                timing_offset_in      <= (others => (others => '0'));
                coarse_time_offset <= (others => (others => '0'));
                read_ctrl_word        <= (others => '0');
                dead_time             <= (others => '0');
            else
                ctrl_header      <= ctrl_word_200MHz(31 downto 22);
                ctrl_WR          <= ctrl_word_200MHz(21 downto 20);
                ctrl_reg_idx     <= ctrl_word_200MHz(19 downto 17);
                ctrl_channel_idx <= to_integer(unsigned(ctrl_word_200MHz(16 downto 8)));
                ctrl_data        <= ctrl_word_200MHz(7 downto 0);  
                if ctrl_word_valid_200MHz = '1' then 
                    if ctrl_WR = "10" then
                        if ctrl_reg_idx = "000" and ctrl_channel_idx < 288 then  -- channel mask
                            channel_mask(ctrl_channel_idx) <= ctrl_data(0);
                        end if;
                        if ctrl_reg_idx = "001" then -- BC offset
                            BC_offset <= ctrl_data(5 downto 0);
                        end if;
                        if ctrl_reg_idx = "010" and ctrl_channel_idx < 18 then -- timing offset
                            timing_offset_in(ctrl_channel_idx) <= ctrl_data(7 downto 0);
                        end if;
                        if ctrl_reg_idx = "011"  and ctrl_channel_idx < 18 then -- coarse timing offset
                            coarse_time_offset(ctrl_channel_idx) <= ctrl_data(3 downto 0);
                        end if;
                        if ctrl_reg_idx = "100" then -- dead time
                            dead_time <= ctrl_data(2 downto 0);
                        end if;
                    end if;
                    if ctrl_WR = "01" then
                        if ctrl_reg_idx = "000" and ctrl_channel_idx < 288 then  -- channel mask
                            read_ctrl_word <= "11000000" & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "0000000" & channel_mask(ctrl_channel_idx);
                        end if;
                        if ctrl_reg_idx = "001" then -- BC offset
                            read_ctrl_word <= "11000000" & ctrl_reg_idx & "000000000" & "00" & BC_offset;
                        end if;
                        if ctrl_reg_idx = "010" and ctrl_channel_idx < 18 then -- timing offset
                            read_ctrl_word <= "11000000" & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & timing_offset_in(ctrl_channel_idx);
                        end if;
                        if ctrl_reg_idx = "011"  and ctrl_channel_idx < 18 then -- coarse timing offset
                            read_ctrl_word <= "11000000" & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "0000" & coarse_time_offset(ctrl_channel_idx);
                        end if;  
                        if ctrl_reg_idx = "100" then --dead time
                            read_ctrl_word <= "11000000" & ctrl_reg_idx & "000000000" & "00000" & dead_time;
                        end if; 
                        write_en_ctrl_fifo <= '1';
                    else 
                        write_en_ctrl_fifo <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    
    
     read_ctrl_word_cdc_200to320MHz : xpm_cdc_array_single
    generic map (
        DEST_SYNC_FF   => 2,
        INIT_SYNC_FF   => 0,
        SIM_ASSERT_CHK => 0, 
        SRC_INPUT_REG  => 1,
        WIDTH          => 28
    )
    port map (
        dest_out => read_ctrl_word_320MHz,
        dest_clk => clock320_mmcm,
        src_clk  => clock200_mmcm,
        src_in   => read_ctrl_word
    );
    
        
        
     write_en_ctrl_fifo_cdc_inst : xpm_cdc_pulse
     generic map (
        DEST_SYNC_FF => 2,   
        INIT_SYNC_FF => 0,   
        REG_OUTPUT => 0,     
        RST_USED => 0,       
        SIM_ASSERT_CHK => 0  
     )
     port map (
        dest_pulse => write_en_ctrl_fifo_320MHz, 
        dest_clk => clock320_mmcm,     
        dest_rst =>'0',     
        src_clk => clock200_mmcm,       
        src_pulse => write_en_ctrl_fifo,   
        src_rst => '0'        
     );   
    
    ctrl_fifo_out : fifo_16x28b
    port map (
        clk        => clock320_mmcm,
        din        => read_ctrl_word_320MHz,
        wr_en      => write_en_ctrl_fifo_320MHz,
        rd_en      => read_en_ctrl_fifo,
        dout       => ctrl_fifo_dout,
        full       => open,
        empty      => ctrl_fifo_empty,
        data_count => open
    );
    
     ----- End of logic to read and write configuration registers -------------

    -- forwarded 40 MHz clcok to the FE
    feclock40_gen : for i in 0 to 35 generate
      ODDR_inst : ODDR
      generic map(
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
      port map (
        Q  => feclock40(i),
        C  => clock40_bufg,
        CE => '1',
        D1 => '1',
        D2 => '0',
        R  => '0',
        S  => '0'
      );

      OBUFDS_inst : OBUFDS
      generic map (
        IOSTANDARD => "DEFAULT",
        SLEW       => "SLOW")
      port map (
        O  => fe_clock40_p(i),
        OB => fe_clock40_n(i),
        I  => feclock40(i)
      );
    end generate;

    -- forwarded 40 MHz clcok to the FE
    discrim_or_rates_gen : for i in 0 to 35 generate
        -- Looking for the edge of the discriminated or pulse (10/50 ns)
        or_edge_detector_inst : entity work.edge_detector
        generic map(
            synch_enable        => TRUE,
            edge_triggered_type => '1'
        )
        port map(
        clk_i    => clock200_mmcm,
        signal_i => discrim_or(i),
        r_edge_o => or_pulse(i),
        f_edge_o => open
    );

        -- Primitive to safetly strecht the 200MHz pulse into the 40MHz domaind
        xpm_cdc_pulse_inst : xpm_cdc_pulse
        generic map (
            DEST_SYNC_FF   => 3,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            REG_OUTPUT     => 1,   -- DECIMAL; 0=disable registered output, 1=enable registered output. The out is combinatorialif disabled
            RST_USED       => 0,   -- DECIMAL; 0=no reset, 1=implement reset
            SIM_ASSERT_CHK => 1  -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        )
        port map (
            dest_pulse => or_pulse_40MHz_domain(i),
            dest_clk   => clock40_bufg,
            dest_rst   => '0',
            src_clk    => clock200_mmcm,
            src_pulse  => or_pulse(i),   -- 1-bit input: Rising edge of this signal initiates a pulse transfer to the
                                    -- destination clock domain. The minimum gap between each pulse transfer must
                                    -- be at the minimum 2*(larger(src_clk period, dest_clk period)). This is
                                    -- measured between the falling edge of a src_pulse to the rising edge of the
                                    -- next src_pulse. This minimum gap will guarantee that each rising edge of
                                    -- src_pulse will generate a pulse the size of one dest_clk period in the
                                    -- destination clock domain. When RST_USED = 1, pulse transfers will not be
                                    -- guaranteed while src_rst and/or dest_rst are asserted.

            src_rst => '0'
        );

        -- pulse counter rate logic
        or_rate_counters_inst : entity work.rate_counter
        port map(
            clk_i   => clock40_bufg,
            pulse_i => or_pulse_40MHz_domain(i),
            rate_o  => counter_rate(i)
        );

    end generate;


end RTL;
