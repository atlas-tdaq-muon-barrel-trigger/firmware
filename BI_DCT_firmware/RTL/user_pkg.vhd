library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package user_pkg is


    type array_288x6b is array (287 downto 0) of std_logic_vector(5 downto 0);
    type array_288x8b is array (287 downto 0) of std_logic_vector(7 downto 0);
    type array_288x18b is array (287 downto 0) of std_logic_vector(17 downto 0);
    type array_18x4b is array (17 downto 0) of std_logic_vector(3 downto 0);
    type array_18x3b is array (17 downto 0) of std_logic_vector(2 downto 0);
    type array_144x8b is array (143 downto 0) of std_logic_vector(7 downto 0);
    type array_144x48b is array (143 downto 0) of std_logic_vector(47 downto 0);
    type array_18x56b is array (17 downto 0) of std_logic_vector(55 downto 0);
    type array_18x8b is array (17 downto 0) of std_logic_vector(7 downto 0);
    type array_18x12b is array (17 downto 0) of std_logic_vector(11 downto 0);
    type array_288x12b is array(287 downto 0) of std_logic_vector(11 downto 0);
    type array_288x14b is array(287 downto 0) of std_logic_vector(13 downto 0);
    type array_288x2b is array(287 downto 0) of std_logic_vector(1 downto 0);
    type array_144x18b is array (143 downto 0) of std_logic_vector(17 downto 0);

    constant counters_width : integer := 12;
    -- 12 bit can reach 8000 pulses (8 kHz=1khz on one channel x 8 channels)
    constant or_signals : integer := 36;
    type array_pulse_rate_counters is array(or_signals-1 downto 0) of std_logic_vector(counters_width-1 downto 0);

end package user_pkg;

package body user_pkg is

end package body user_pkg;
