library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.user_pkg.all;

library xpm;
use xpm.vcomponents.all;

entity rate_counter is
port(
    clk_i   : in  std_logic;
    pulse_i : in  std_logic;
    rate_o  : out std_logic_vector(counters_width-1 downto 0)
);
end rate_counter;

architecture behavioral of rate_counter is

    signal one_second_counter : unsigned(25 downto 0);
    signal pulse_counter_rate : unsigned(counters_width-1 downto 0);

begin

-- Process to generate a 1-second enable signal using 40 MHz clock
process(clk_i)--clock40_bufg
begin
    if rising_edge(clk_i) then
        if one_second_counter = 40078970 then
            one_second_counter <= (others=>'0');
        else
            one_second_counter <= one_second_counter + 1;
        end if;
    end if;
end process;

-- Process to count pulses rate
counter_or_pulses: process(clk_i)
begin
if rising_edge(clk_i) then
    if one_second_counter = 0 then
        pulse_counter_rate <= (others=>'0');
        rate_o             <= std_logic_vector(pulse_counter_rate);
    elsif pulse_i ='1' then
        pulse_counter_rate <= pulse_counter_rate + 1;
    end if;
end if;
end process;

end architecture;
