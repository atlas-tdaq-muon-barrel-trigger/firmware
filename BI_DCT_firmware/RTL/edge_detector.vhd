library ieee;
use ieee.std_logic_1164.all;

library xpm;
use xpm.vcomponents.all;

entity edge_detector is
    generic(
        synch_enable        : boolean := TRUE;
        edge_triggered_type : std_logic := '1'
    );
    port(
        clk_i    : in  std_logic;
        signal_i : in  std_logic;
        r_edge_o : out std_logic;
        f_edge_o : out std_logic
    );
end edge_detector;

architecture behavioral of edge_detector is

    signal synch_signal_i     : std_logic;
    signal synch_signal_i_reg : std_logic;

    signal r_edge : std_logic;
    signal f_edge : std_logic;

begin

    synch_enable_true : if (synch_enable = TRUE) generate
        xpm_cdc_single_inst : xpm_cdc_single
        generic map (
            DEST_SYNC_FF   => 2,   -- DECIMAL; range: 2-10
            INIT_SYNC_FF   => 1,   -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 1, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG  => 0   -- DECIMAL; 0=do not register input, 1=register input
        )
        port map (
            dest_out => synch_signal_i,
            dest_clk => clk_i,
            src_clk  => '0',
            src_in   => signal_i
        );
    end generate;

    synch_enable_false: if (synch_enable = FALSE) generate
        synch_signal_i <= signal_i;
    end generate;

    pos_edge : if (edge_triggered_type = '1') generate
        edge_detector_proc : process(clk_i)
        begin
            if rising_edge(clk_i) then
                synch_signal_i_reg <= synch_signal_i;

                if (synch_signal_i_reg = '0' and synch_signal_i = '1') then
                    r_edge <= '1';
                    f_edge <= '0';
                elsif (synch_signal_i_reg = '1' and synch_signal_i = '0') then
                    r_edge <= '0';
                    f_edge <= '1';
                else
                    r_edge <= '0';
                    f_edge <= '0';
                end if;

            end if;
        end process;

        r_edge_o <= r_edge;
        f_edge_o <= f_edge;

    end generate;

    neg_edge : if (edge_triggered_type = '0') generate
        edge_detector_proc : process(clk_i)
        begin
            if falling_edge(clk_i) then

                synch_signal_i_reg <= synch_signal_i;

                if (synch_signal_i_reg = '0' and synch_signal_i = '1') then
                    r_edge <= '1';
                    f_edge <= '0';
                elsif (synch_signal_i_reg = '1' and synch_signal_i = '0') then
                    r_edge <= '0';
                    f_edge <= '1';
                else
                    r_edge <= '0';
                    f_edge <= '0';
                end if;

            end if;
        end process;

        r_edge_o <= r_edge;
        f_edge_o <= f_edge;

    end generate;

end behavioral;
