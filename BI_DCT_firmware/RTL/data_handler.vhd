-------------------------------------------------------
-- File:          data_handler.vhd
-- Project:       BI DCT firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/16
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
library xil_defaultlib;



entity data_handler is
    port (
        clock_in        : in  std_logic;                     
--        BCID_in         : in  std_logic_vector(11 downto 0);
        data_bcid_in    : in  std_logic_vector(11 downto 0);
        data_in         : in  std_logic_vector(13 downto 0);
        write_enable_in : in  std_logic;
        read_enable_in  : in  std_logic;
        out_index       : in integer range 0 to 7;
        data_out        : out std_logic_vector(21 downto 0) -- 8 bit BCID + 8 bit rising time + 6 bit falling time
    );
end data_handler;

architecture RTL of data_handler is

    component data_derandomizer is
        generic (logic_index : integer range 0 to 7);
        port (
            clock_in      : in std_logic;
            reset_in      : in std_logic;
            we_in         : in std_logic;
            data_in       : in std_logic_vector(21 downto 0);
            BCID_data_in  : in integer range 0 to 7;
            data_out      : out std_logic_vector(21 downto 0)
        );
    end component;
    
    type array_8x22b is array(7 downto 0) of std_logic_vector(21 downto 0);
    signal derandomized_data  : array_8x22b;
    signal derandomizer_reset : std_logic_vector(7 downto 0);
    signal reset_index        : integer range 0 to 7 := 0;
    signal reset_index_r      : integer range 0 to 7 := 0;
--    signal out_index          : integer range 0 to 4 := 0;
--    signal out_index_r        : integer range 0 to 4 := 0;
--    signal out_index_r2       : integer range 0 to 4 := 0;
--    signal out_index_r3       : integer range 0 to 4 := 0;
    signal BCID_data          : integer range 0 to 7 := 0;
    signal data               : std_logic_vector(21 downto 0);
       
begin
      
    GEN_data_derandomizer : for i in 0 to 7 generate
        data_derandomizer_inst : data_derandomizer
        generic map (logic_index => i)
        port map(
            clock_in              => clock_in,
            reset_in              => derandomizer_reset(i),
            we_in                 => write_enable_in,
            data_in(21 downto 14) => data_bcid_in(7 downto 0),
            data_in(13 downto 0)  => data_in,
            BCID_data_in          => BCID_data,
            data_out              => derandomized_data(i)
        );
    end generate;
    
    BCID_data <= to_integer(unsigned(data_bcid_in)) mod 8; --problems near the max BCID value??
          
    
    process(clock_in)
    begin
        if rising_edge(clock_in) then
        
--            out_index <= (to_integer(unsigned(BCID_in - (0+4)))) mod 5; --replace 0 with minimum latency for data to arrive to DCT
--            out_index_r <= out_index;
--            out_index_r2 <= out_index_r;
--            out_index_r3 <= out_index_r2;
--            reset_index <= out_index_r3;
            reset_index   <= out_index;
            reset_index_r <= reset_index;
            
            if read_enable_in = '1' then
--                data <= derandomized_data(out_index_r3);   --problemi all'inizio e alla fine dei possibili valori del BC di LHC per sovrapposizioni logiche
                data <= derandomized_data(out_index);   --problemi all'inizio e alla fine dei possibili valori del BC di LHC per sovrapposizioni logiche
            else
                data <= data;
            end if;
            derandomizer_reset <= (others=>'0');
            if reset_index_r /= reset_index then
                derandomizer_reset(reset_index) <= '1';
            end if;
        end if;
    end process;
    
    data_out <= data;
    
end RTL;
