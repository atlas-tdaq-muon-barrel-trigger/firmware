
create_project trigalgotest trigalgotest_vivado_project -part xcvu13p-flga2577-1-e
set_property target_language VHDL [current_project]

add_files -scan_for_includes {
    sources/design_sources/my_lib.vhd
    sources/design_sources/trigger_logic/tb_trigalgotest_forimpl.vhd
    sources/design_sources/trigger_logic/SLtrigger.vhd
    sources/design_sources/trigger_logic/trigalgopkg.vhd
    }

set_property file_type {VHDL 2008} [get_files  sources/design_sources/trigger_logic/SLtrigger.vhd]
set_property file_type {VHDL 2008} [get_files  sources/design_sources/trigger_logic/tb_trigalgotest_forimpl.vhd]
set_property file_type {VHDL 2008} [get_files  sources/design_sources/trigger_logic/trigalgopkg.vhd]

add_files -fileset sim_1 -norecurse {
    sources/design_sources/trigger_logic/sim/tb_trigalgotest_forsim.vhd
}

add_files -fileset constrs_1 -norecurse {
    sources/constraints/constr_trigalgotest.xdc
}

set_property strategy Flow_PerfThresholdCarry [get_runs synth_1]
set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

set_property top top_SLtrigger_forsim [get_filesets sim_1]
set_property top_lib xil_defaultlib [get_filesets sim_1]