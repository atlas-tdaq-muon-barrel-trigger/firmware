-------------------------------------------------------        
-- File:          SL_tb.vhd                                   
-- Project:       Barrel SL firmware                           
-- Author:        Federico Morodei <federico.morodei@cern.ch>  
-- Last modified: 2023/08/24     
-- Description:   testbench for SL firmware. Include one BMBO DCT and lpGBT emulator to  
--                simulate communication between one DCT and SL board                              
-------------------------------------------------------        

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SL_tb is

end SL_tb;

architecture Behavioral of SL_tb is

constant emulate_DCT : integer := 1;  -- 1: emulate BMBO DCT and lpGBT. Use simulated DCT hits. 
                                      -- 0: emulate lpGBT only. Use simple counter as uplink data.

constant clk_40_period  : time := 25.0 ns;
constant clk_80_period  : time := 12.5 ns;
constant clk_320_period : time := 3.125 ns;
constant clk_240_period : time := 4.167 ns;
constant clk_160_period : time := 6.25 ns;
constant clk_200_period : time := 5.0 ns;

signal clk_40_n  : std_logic;
signal clk_40_p  : std_logic;
signal clk_320_n : std_logic;
signal clk_320_p : std_logic;
signal clk_240_n : std_logic;
signal clk_240_p : std_logic;
signal clk_160_n : std_logic;
signal clk_160_p : std_logic;
signal clk_200_n : std_logic;
signal clk_200_p : std_logic;
signal clk_80_n  : std_logic;
signal clk_80_p  : std_logic;
signal clk_600   : std_logic;

signal lpgbt_rxn   : std_logic;
signal lpgbt_rxp   : std_logic;
signal lpgbt_txn   : std_logic;
signal lpgbt_txp   : std_logic;
signal SL_rxn      : std_logic;
signal SL_rxp      : std_logic;
signal SL_txn      : std_logic;
signal SL_txp      : std_logic;
signal SL_txn_out  : std_logic_vector(19 downto 0);
signal SL_txp_out  : std_logic_vector(19 downto 0);
signal SL_rxn_20GT : std_logic_vector(19 downto 0);
signal SL_rxp_20GT : std_logic_vector(19 downto 0);
signal reset       : std_logic;

component mmcm_200_600
    port (
        clk_out600 : out std_logic;
        clk_out200 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;



begin

process
begin
    clk_40_n <= '1';
    wait for clk_40_period/2;
    clk_40_n <= '0';
    wait for clk_40_period/2;
end process;

clk_40_p <= not clk_40_n;



process
begin
    clk_80_n <= '1';
    wait for clk_80_period/2;
    clk_80_n <= '0';
    wait for clk_80_period/2;
end process;

clk_80_p <= not clk_80_n;

process
begin
    clk_320_n <= '1';
    wait for clk_320_period/2;
    clk_320_n <= '0';
    wait for clk_320_period/2;
end process;

clk_320_p <= not clk_320_n;

process
begin
    clk_240_n <= '1';
    wait for clk_240_period/2;
    clk_240_n <= '0';
    wait for clk_240_period/2;
end process;

clk_240_p <= not clk_240_n;

process
begin
    clk_160_n <= '1';
    wait for clk_160_period/2;
    clk_160_n <= '0';
    wait for clk_160_period/2;
end process;

clk_160_p <= not clk_160_n;

process
begin
    clk_200_n <= '1';
    wait for clk_200_period/2;
    clk_200_n <= '0';
    wait for clk_200_period/2;
end process;

clk_200_p <= not clk_200_n;

mmcm_clk_600 : mmcm_200_600
    port map (
        clk_out600 => clk_600,
        clk_out200 => open,
        locked     => open,
        clk_in40   => clk_40_p
    );

process
begin
    reset <= '1';
    wait for 500 ns;
    reset <= '0';
    wait;
end process;



-- connect lpGBT TX to SL RX and SL TX to lpGBT RX
SL_rxn    <= lpgbt_txn;
SL_rxp    <= lpgbt_txp;
lpgbt_rxn <= SL_txn;
lpgbt_rxp <= SL_txp;

SL_txn <= SL_txn_out(0);
SL_txp <= SL_txp_out(0);

lpgbt_emul_exdes_inst : entity work.lpgbt_emul_gt_exdes
generic map (
    emulate_DCT => emulate_DCT
)
port map (
    Q2_CLK1_GTREFCLK_PAD_N_IN  => clk_160_n,
    Q2_CLK1_GTREFCLK_PAD_P_IN  => clk_160_p,
    DRP_CLK_IN_P               => clk_200_p,
    DRP_CLK_IN_N               => clk_200_n,
    RXN_IN                     => lpgbt_rxn,
    RXP_IN                     => lpgbt_rxp,
    TXN_OUT                    => lpgbt_txn,
    TXP_OUT                    => lpgbt_txp,
    clk_40_in_p                => clk_40_p,
    clk_40_in_n                => clk_40_n,
    clk_80                     => clk_80_p,
    clk_320                    => clk_320_p,
    clk_200                    => clk_200_p,
    clk_600                    => clk_600,
    reset                      => reset
);

generate_20_SL_GT_inputs : for i in 19 downto 0 generate
    SL_rxn_20GT(i) <= SL_rxn;
    SL_rxp_20GT(i) <= SL_rxp;
end generate;


SL_top_inst : entity work.SL_top
port map (
    base_clock_40_p          => clk_40_p,
    base_clock_40_n          => clk_40_n,
    mgtrefclk0_x0y2_p_SLR0   => clk_240_p,
    mgtrefclk0_x0y2_n_SLR0   => clk_240_n,
    mgtrefclk0_x0y3_p_SLR0   => clk_240_p,
    mgtrefclk0_x0y3_n_SLR0   => clk_240_n,
    mgtrefclk0_x1y1_p_SLR0   => clk_320_p,
    mgtrefclk0_x1y1_n_SLR0   => clk_320_n,
    gtyrxn_DCT_in_SLR0       => SL_rxn_20GT(9 downto 0),
    gtyrxp_DCT_in_SLR0       => SL_rxp_20GT(9 downto 0),
    gtytxn_DCT_out_SLR0      => open,
    gtytxp_DCT_out_SLR0      => open,
    gtyrxn_TDAQ_in_SLR0      => "000000000",
    gtyrxp_TDAQ_in_SLR0      => "000000000", 
    gtytxn_TDAQ_out_SLR0     => open,
    gtytxp_TDAQ_out_SLR0     => open,
                            
    mgtrefclk0_x0y5_p_SLR1   => clk_320_p,
    mgtrefclk0_x0y5_n_SLR1   => clk_320_n,
    mgtrefclk0_x0y7_p_SLR1   => clk_240_p,
    mgtrefclk0_x0y7_n_SLR1   => clk_240_n,
    mgtrefclk0_x1y6_p_SLR1   => clk_320_p,
    mgtrefclk0_x1y6_n_SLR1   => clk_320_n,
    gtyrxn_DCT_in_SLR1       => SL_rxn_20GT,
    gtyrxp_DCT_in_SLR1       => SL_rxp_20GT,
    gtytxn_DCT_out_SLR1      => SL_txn_out, 
    gtytxp_DCT_out_SLR1      => SL_txp_out,
    gtyrxn_TDAQ_in_SLR1      => "000000",
    gtyrxp_TDAQ_in_SLR1      => "000000", 
    gtytxn_TDAQ_out_SLR1     => open,
    gtytxp_TDAQ_out_SLR1     => open,
                             
    mgtrefclk0_x0y9_p_SLR2   => clk_320_p,
    mgtrefclk0_x0y9_n_SLR2   => clk_320_n,
    mgtrefclk0_x0y11_p_SLR2  => clk_240_p,
    mgtrefclk0_x0y11_n_SLR2  => clk_240_n,
    mgtrefclk0_x1y10_p_SLR2  => clk_320_p,
    mgtrefclk0_x1y10_n_SLR2  => clk_320_n,
    gtyrxn_DCT_in_SLR2       => SL_rxn_20GT,
    gtyrxp_DCT_in_SLR2       => SL_rxp_20GT,
    gtytxn_DCT_out_SLR2      => open,
    gtytxp_DCT_out_SLR2      => open, 
    gtyrxn_TDAQ_in_SLR2      => "000000", 
    gtyrxp_TDAQ_in_SLR2      => "000000",
    gtytxn_TDAQ_out_SLR2     => open,
    gtytxp_TDAQ_out_SLR2     => open,
                            
    mgtrefclk0_x0y13_p_SLR3  => clk_240_p,
    mgtrefclk0_x0y13_n_SLR3  => clk_240_n,
    gtyrxn_TDAQ_in_SLR3      => "0000",
    gtyrxp_TDAQ_in_SLR3      => "0000", 
    gtytxn_TDAQ_out_SLR3     => open,
    gtytxp_TDAQ_out_SLR3     => open,
                             
    L0_acc                   => '0',
    reset                    => reset
);

end Behavioral;
