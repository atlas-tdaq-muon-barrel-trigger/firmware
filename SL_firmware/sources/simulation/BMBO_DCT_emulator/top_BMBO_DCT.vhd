------------------------------------------------------
--File:             top.vhd
--Project:          BMBO DCT firmware
--Author:           Riccardo Vari, Federico Morodei
--Last modified:    2023/06/29
------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.ALL;

library UNISIM;
use UNISIM.vcomponents.all;

library xil_defaultlib;
use xil_defaultlib.pack.all;

library STD;
use STD.textio.all;

--  lpGBT clocks:
--
--    Phase/Frequency - 4 programmable clocks:
--      - 4 independent
--      - Phase resolution: 50 ps
--      - Frequencies: 40 / 80 / 160 / 320 / 640 / 1280 MHz
--
--    eLink Clocks:
--      - 28 independent
--      - Fixed phase
--      - Frequency programable: 40 / 80 / 160 / 320 / 640 / 1280 MHz
--
--    Clock jitter < 5 ps rms
--

entity top_BMBO_DCT is
port (
    -- 40 MHz oscillator sent to fan-out chip to produce these two clocks - MRCC FPGA pins (NOTE now connected to _N should be _P)
    --CLK40_0     : in  std_logic;
    --CLK40_1     : in  std_logic;
    -- lpGBT phase shift clocks - MRCC pins
    --PSCLK0_p    : in  std_logic;
    --PSCLK0_n    : in  std_logic;
    --PSCLK1_p    : in  std_logic;
    --PSCLK1_n    : in  std_logic;
    -- lpGBT elink clock - MRCC pins
    ECLK0_p     : in  std_logic;
    ECLK0_n     : in  std_logic;
    --ECLK1_p     : in  std_logic;
    --ECLK1_n     : in  std_logic;
    -- SMA test signals - SRCC pins
    --TST_CLK0_P  : in  std_logic;
    --TST_CLK0_N  : in  std_logic;
    --TST_CLK1_P  : in  std_logic;
    --TST_CLK1_N  : in  std_logic;
    -- RPC FE inputs
    rpc_in      : in  std_logic_vector(287 downto 0);
    -- lpGBT elinks
    elink_in_p  : in  std_logic_vector(3 downto 0);
    elink_in_n  : in  std_logic_vector(3 downto 0);
    elink_out_p : out std_logic_vector(27 downto 0);
    elink_out_n : out std_logic_vector(27 downto 0)
    --fifo_datacount : out array_18x4b --for simulation
    );
end top_BMBO_DCT;

--- BMBO DCT data format
--- 1-bit coordinate (eta/phi) + 7-bit strip ID + 10-bit BCID + 5-bit rising time first lauer + 5-bit rising time second layer

architecture RTL of top_BMBO_DCT is

    type array_288x6b is array (287 downto 0) of std_logic_vector(5 downto 0);
    type array_288x8b is array (287 downto 0) of std_logic_vector(7 downto 0);
    type array_288x30b is array (287 downto 0) of std_logic_vector(29 downto 0);
    type array_288x45b is array (287 downto 0) of std_logic_vector(44 downto 0);
    type array_144x20b is array (143 downto 0) of std_logic_vector(19 downto 0);
    type array_144x28b is array (143 downto 0) of std_logic_vector(27 downto 0);
    type array_144x10b is array (143 downto 0) of std_logic_vector(9 downto 0);
    type array_18x28b is array (17 downto 0) of std_logic_vector(27 downto 0);
    type array_18x4b is array (17 downto 0) of std_logic_vector(3 downto 0);
    type array_36x4b is array (35 downto 0) of std_logic_vector(3 downto 0);
    type array_18x5b is array (17 downto 0) of std_logic_vector(4 downto 0);
    type array_288x5b is array (287 downto 0) of std_logic_vector(4 downto 0);
    type array_18x12b is array(17 downto 0) of std_logic_vector(11 downto 0);
    type array_144x12b is array(143 downto 0) of std_logic_vector(11 downto 0);
    -- type array_288x9b is array (287 downto 0) of std_logic_vector(8 downto 0);

    --signal TST_CLK0 : std_logic;
    --signal TST_CLK1 : std_logic;

    --signal PSCLK0 : std_logic;
    --signal PSCLK1 : std_logic;
    
    signal ECLK0 : std_logic;
    --signal ECLK1 : std_logic;

    --signal clock40 : std_logic;
    signal clock40_edge : std_logic := '0';
    signal clock40_edge320_r : std_logic;
    signal clock40_edge200_r : std_logic;
    signal clock40_rising320 : std_logic;
    signal clock40_rising200 : std_logic;
    signal clock40_rising200_r : std_logic;

    signal clock600_mmcm : std_logic;
    signal clock200_mmcm : std_logic;
    signal clock320_mmcm : std_logic;
    signal clock40_mmcm  : std_logic;
    
    signal mmcm1_locked : std_logic;
    signal mmcm2_locked : std_logic;
    signal mmcm_locked_r0 : std_logic;
    signal mmcm_locked_r1 : std_logic;
    
    signal int_reset : std_logic;   

    signal elink_out : std_logic_vector(27 downto 0);
    signal elink_in : std_logic_vector(3 downto 0);

    subtype integer018 is integer range 0 to 18;
    signal fifo_out_num_out : integer018;

    signal bcid320 : std_logic_vector(11 downto 0) := (others => '0');
    signal subbcid : std_logic_vector(2 downto 0) := (others => '0');
    signal subbcid_r : std_logic_vector(2 downto 0) := (others => '0');
    
    signal datain_288x30b_lsb_old : std_logic_vector(287 downto 0);

    signal datain_288x6b : array_288x6b := (others => (others => '0'));
    signal datain_288x30b : array_288x30b := (others => (others => '0'));

    signal time_144x10b : array_144x10b := (others => (others => '0'));
    
    signal fifo_in_din : array_144x20b := (others => (others => '0'));
    signal fifo_in_we : std_logic_vector(143 downto 0);
    signal fifo_in_re : std_logic_vector(143 downto 0);
    signal fifo_in_dout : array_144x20b := (others => (others => '0'));
    signal fifo_in_empty : std_logic_vector(143 downto 0);

    signal fifo_out_din : array_18x28b := (others => (others => '0'));
    signal fifo_out_we : std_logic_vector(17 downto 0);
    signal fifo_out_re : std_logic_vector(17 downto 0);
    signal fifo_out_dout : array_18x28b := (others => (others => '0'));
    signal fifo_out_empty : std_logic_vector(17 downto 0);
    signal fifo_out_datacount : array_18x4b;
    
    signal fifo_out_re_r : std_logic_vector(17 downto 0);
    signal fifo_out_dout_r : array_18x28b := (others => (others => '0'));
        
    signal ECLK0_BUFG : std_logic;
    
    signal channel_mask   : std_logic_vector(287 downto 0) := (others => '0');  --only first 144 bits are used   
    signal timing_offset_in : array_18x5b := (others => (others => '0'));
    signal timing_offset : array_288x5b := (others => (others => '0'));
    signal coarse_time_offset_in : array_18x4b := (others => (others => '0'));
    signal BC_offset : std_logic_vector(5 downto 0) := (others => '0');
    signal risetime : array_288x5b;
    signal risetime_plus_offset : array_288x5b;
    signal bcid : array_18x12b := (others => (others => '0'));
    signal mapped_bcid : array_144x12b := (others => (others => '0'));
    signal mapped_bcid_r : array_144x12b := (others => (others => '0'));
    
    signal ctrl_word          : std_logic_vector(31 downto 0);
    signal ctrl_word_valid    : std_logic;
    signal ctrl_header        : std_logic_vector(11 downto 0);
    signal ctrl_WR            : std_logic_vector(1 downto 0);
    signal ctrl_reg_idx       : std_logic_vector(2 downto 0);
    signal ctrl_channel_idx   : integer range 0 to 511; 
    signal ctrl_data          : std_logic_vector(5 downto 0);
    signal read_ctrl_word     : std_logic_vector(27 downto 0);
    signal read_en_ctrl_fifo  : std_logic;
    signal write_en_ctrl_fifo : std_logic; 
    signal ctrl_fifo_dout     : std_logic_vector(27 downto 0);
    signal ctrl_fifo_empty    : std_logic;
    signal ctrl_fifo_empty_r  : std_logic := '1';
    signal prep_ctrl_data_out : std_logic;
    signal ctrl_data_out      : std_logic_vector(27 downto 0) := (others => '0');
    
    signal deser_shift_reg : array_288x6b;
    

--  SELECT_IO IP SETTINGS: Template: Custom; Direction: Input; Data rate: DDR; Serialization Factor: 6; External width: 1; IO type: single-ended; IO standard: LVCMOS33; Clock Setup: Internal Clock
    

    component mmcm_200_600
    port (
        clk_out600 : out std_logic;
        clk_out200 : out std_logic;
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;

    component mmcm_320
    port (
        clk_out320 : out std_logic;
        clk_out40  : out std_logic; 
        locked     : out std_logic;
        clk_in40   : in  std_logic
    );
    end component;
    
    COMPONENT fifo_16x28b
    PORT (
      wr_clk : IN STD_LOGIC;
      rd_clk : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
    END COMPONENT;

   

    component fifo_16x20b  
    port ( 
        wr_clk : in  std_logic;
        rd_clk : in  std_logic;
        din    : in  std_logic_vector(19 downto 0);
        wr_en  : in  std_logic;
        rd_en  : in  std_logic;
        dout   : out std_logic_vector(19 downto 0);
        full   : out std_logic;
        empty  : out std_logic
    );
    end component;
    
    
    component framealigner 
    generic (
        header                 : std_logic_vector(11 downto 0);
        requiredCorrectHeaders : integer range 0 to 7;
        maxFalseHeaders        : integer range 0 to 7;
        requiredCheckedHeaders : integer range 0 to 7
    );
    port (
        clock           : in std_logic;
        reset           : in std_logic;
        data_in         : in std_logic;
        ctrl_word       : out std_logic_vector(31 downto 0);
        ctrl_word_valid : out std_logic 
    );
    end component;
    
    function data2risetime (slv_in : std_logic_vector(30 downto 0)) return std_logic_vector is
    begin
        if    slv_in( 1 downto  0) = "10" then return "00001"; -- LSB is the oldest
        elsif slv_in( 2 downto  1) = "10" then return "00010";
        elsif slv_in( 3 downto  2) = "10" then return "00011";
        elsif slv_in( 4 downto  3) = "10" then return "00100";
        elsif slv_in( 5 downto  4) = "10" then return "00101";
        elsif slv_in( 6 downto  5) = "10" then return "00110";
        elsif slv_in( 7 downto  6) = "10" then return "00111";
        elsif slv_in( 8 downto  7) = "10" then return "01000";
        elsif slv_in( 9 downto  8) = "10" then return "01001";
        elsif slv_in(10 downto  9) = "10" then return "01010";
        elsif slv_in(11 downto 10) = "10" then return "01011";
        elsif slv_in(12 downto 11) = "10" then return "01100";
        elsif slv_in(13 downto 12) = "10" then return "01101";
        elsif slv_in(14 downto 13) = "10" then return "01110";
        elsif slv_in(15 downto 14) = "10" then return "01111";
        elsif slv_in(16 downto 15) = "10" then return "10000";
        elsif slv_in(17 downto 16) = "10" then return "10001";
        elsif slv_in(18 downto 17) = "10" then return "10010";
        elsif slv_in(19 downto 18) = "10" then return "10011";
        elsif slv_in(20 downto 19) = "10" then return "10100";
        elsif slv_in(21 downto 20) = "10" then return "10101";
        elsif slv_in(22 downto 21) = "10" then return "10110";
        elsif slv_in(23 downto 22) = "10" then return "10111";
        elsif slv_in(24 downto 23) = "10" then return "11000";
        elsif slv_in(25 downto 24) = "10" then return "11001";
        elsif slv_in(26 downto 25) = "10" then return "11010";
        elsif slv_in(27 downto 26) = "10" then return "11011";
        elsif slv_in(28 downto 27) = "10" then return "11100";
        elsif slv_in(29 downto 28) = "10" then return "11101";
        elsif slv_in(30 downto 29) = "10" then return "11110";
        else return "00000";
        end if;
    end data2risetime;


begin



    elink_out_diff_buff_gen : for i in 27 downto 0 generate
        OBUFDS_inst : OBUFDS
        generic map (
            IOSTANDARD => "DIFF_HSUL_12",
            SLEW       => "SLOW")   -- Specify the output slew rate
        port map (
            O  => elink_out_p(i),
            OB => elink_out_n(i),
            I  => elink_out(i)
        );
    end generate;

    elink_in_diff_buff_gen : for i in 3 downto 0 generate
        IBUFDS_inst : IBUFDS
        generic map (
            IOSTANDARD => "DIFF_HSUL_12")
        port map (
            I  => elink_in_p(i),
            IB => elink_in_n(i),
            O  => elink_in(i)
        );
    end generate;


    ECLK0_diff_buff : IBUFDS
    generic map (
        DIFF_TERM    => FALSE,
        IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
        IOSTANDARD   => "DIFF_HSUL_12")
    port map (
        I  => ECLK0_p,
        IB => ECLK0_n,
        O  => ECLK0
    );



    clock40_bufg_inst : BUFG
    port map (
      O => ECLK0_BUFG,
      I => ECLK0
    );

    mmcm_for_deserialisers_inst : mmcm_200_600
    port map (
        clk_out600 => clock600_mmcm,
        clk_out200 => clock200_mmcm,
        locked     => mmcm1_locked,
        clk_in40   => ECLK0_BUFG
    );

    mmcm_for_logic_inst : mmcm_320
    port map (
        clk_out320 => clock320_mmcm,
        clk_out40  => clock40_mmcm,
        locked     => mmcm2_locked,
        clk_in40   => ECLK0_BUFG
    );
    
    
    
    
    int_reset_logic : process(clock40_mmcm) begin
        if rising_edge(clock40_mmcm) then
            mmcm_locked_r0 <= mmcm1_locked and mmcm2_locked;
            mmcm_locked_r1 <= mmcm_locked_r0;
            if mmcm_locked_r1 = '1' then
                int_reset <= '0';
            else
                int_reset <= '1';
            end if;
        end if;
    end process;

    clock40_edge_logic : process(clock40_mmcm) begin
        if rising_edge(clock40_mmcm) then
            clock40_edge <= not clock40_edge;
        end if;
    end process;

    clock40_rising320_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            clock40_edge320_r <= clock40_edge;
            if clock40_edge320_r /= clock40_edge then
                clock40_rising320 <= '1';
            else
                clock40_rising320 <= '0';
            end if;
        end if;
    end process;

    clock40_rising200_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            clock40_edge200_r <= clock40_edge;
            if clock40_edge200_r /= clock40_edge then
                clock40_rising200 <= '1';
            else
                clock40_rising200 <= '0';
            end if;
        end if;
    end process;
    
    
    bcid_counter_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 17 downto 0 loop
                if int_reset = '1' then
                    bcid(i) <= ("000000" & BC_offset) + ("00000000" & coarse_time_offset_in(i));
                else
                    if clock40_rising200 = '1' then
                        bcid(i) <= std_logic_vector(to_unsigned(to_integer(unsigned(bcid(i) + 1)) mod 3563, 12));  --3563 = X"deb"
                    end if;
                end if;
            end loop;
        end if;
    end process;

    

    bcid320_counter_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            if int_reset = '1' then
                bcid320 <= "000000" & BC_offset; 
            else
                if clock40_rising320 = '1' then
                    bcid320 <= std_logic_vector(to_unsigned(to_integer(unsigned(bcid320 + 1)) mod 3563, 12));  --3563 = X"deb"
                end if;
            end if;
        end if;
    end process;

    subbcid_counter_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            subbcid_r <= subbcid;
            if clock40_rising320 = '1' then
                subbcid <= "000";
            else
                subbcid <= subbcid + 1;
            end if;
        end if;
    end process;
    
    deserialiser_logic : process(clock600_mmcm, clock200_mmcm) begin
        if rising_edge(clock600_mmcm) or falling_edge(clock600_mmcm) then
            for i in 287 downto 0 loop
                deser_shift_reg(i) <= rpc_in(i) & deser_shift_reg(i)(5 downto 1); -- BIT0 is the OLDEST
            end loop;
        end if;
        if rising_edge(clock200_mmcm) then
            for i in 287 downto 0 loop
                datain_288x6b(i) <= deser_shift_reg(i);
            end loop;
        end if;
    end process;




    shift_reg_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 287 downto 0 loop
                datain_288x30b(i) <= datain_288x6b(i) & datain_288x30b(i)(29 downto 6);  -- LSB is the OLDEST
            end loop;
        end if;
    end process;
   

     timing_offset_mapping : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 17 downto 0 loop
                for k in 15 downto 0 loop
                    timing_offset(k+16*i) <= timing_offset_in(i);
                end loop;
            end loop;
        end if;
     end process;
    

    time_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            if clock40_rising200 = '1' then
                for i in 287 downto 0 loop
                    datain_288x30b_lsb_old(i) <= datain_288x30b(i)(29);
                    risetime(i) <= data2risetime(datain_288x30b(i) & datain_288x30b_lsb_old(i));
                end loop;
                
                bcid_mapping : for i in 17 downto 0 loop
                    for k in 7 downto 0 loop
                        mapped_bcid(k+8*i) <= bcid(i); -- assume both layers for same (eta,phi) coordinates have same coarse timing offset 
                    end loop;
                end loop;
     
            end if;
            for i in 287 downto 0 loop
                if risetime(i) = 0 then
                    risetime_plus_offset(i) <= "00000";
                elsif ((to_integer(unsigned(risetime(i))) + to_integer(unsigned(timing_offset(i))))) > 30 then
                    risetime_plus_offset(i) <= risetime(i) + timing_offset(i) + 2; --mod32 is done by 5-bit length
                else
                    risetime_plus_offset(i) <= risetime(i) + timing_offset(i);
                end if;
            end loop;
            
            for i in 143 downto 0 loop
                mapped_bcid_r(i) <= mapped_bcid(i);
            end loop;
        end if;
    end process;
    
    
    merge_layers : for i in 143 downto 0 generate
        time_144x10b(i) <= risetime_plus_offset(i) & risetime_plus_offset(i + 144);
    end generate;
    

    fifo_in_we_logic : process(clock200_mmcm) begin
        if rising_edge(clock200_mmcm) then
            for i in 143 downto 0 loop
                fifo_in_din(i) <= mapped_bcid(i)(9 downto 0) & time_144x10b(i);  
            end loop;
            clock40_rising200_r <= clock40_rising200;
            if clock40_rising200_r = '1' then
                for i in 143 downto 0 loop
                    if time_144x10b(i) /= "0000000000" then  -- zero suppression
                        fifo_in_we(i) <= not channel_mask(i);  -- channel mask: config reg to switch off DCT channels if needed
                    end if;
                end loop;
            else
                 fifo_in_we <= (others => '0');
            end if;
        end if;
    end process;
    

    fifo_in_gen : for i in 0 to 143 generate
        fifo_in: fifo_16x20b
        port map (
            wr_clk     => clock200_mmcm,
            rd_clk     => clock320_mmcm,
            din        => fifo_in_din(i),
            wr_en      => fifo_in_we(i),
            rd_en      => fifo_in_re(i),
            dout       => fifo_in_dout(i),
            full       => open,
            empty      => fifo_in_empty(i)
        );
    end generate;

    fifo_out_gen : for i in 0 to 17 generate
        fifo_out : fifo_16x28b
        port map (
            wr_clk     => clock320_mmcm,
            rd_clk     => clock320_mmcm,
            din        => fifo_out_din(i),
            wr_en      => fifo_out_we(i),
            rd_en      => fifo_out_re(i),
            dout       => fifo_out_dout(i),
            full       => open,
            empty      => fifo_out_empty(i)
        );
    end generate;

    fifo_in_re_gen : for i in 0 to 17 generate
        fifo_in_re_logic : process(clock320_mmcm)
        begin
            if rising_edge(clock320_mmcm) then
                if subbcid = "000" and fifo_in_empty(i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"01";
                elsif subbcid = "001" and fifo_in_empty(1+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"02";
                elsif subbcid = "010" and fifo_in_empty(2+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"04";
                elsif subbcid = "011" and fifo_in_empty(3+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"08";
                elsif subbcid = "100" and fifo_in_empty(4+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"10";
                elsif subbcid = "101" and fifo_in_empty(5+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"20";
                elsif subbcid = "110" and fifo_in_empty(6+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"40";
                elsif subbcid = "111" and fifo_in_empty(7+i*8) = '0' then
                    fifo_in_re(7+i*8 downto i*8) <= X"80";
                else
                    fifo_in_re(7+i*8 downto i*8) <= X"00";
                end if;
            end if;
        end process;
    end generate;

    fifo_out_we_gen : for i in 0 to 17 generate
        fifo_out_we_logic : process(clock320_mmcm) begin
            if rising_edge(clock320_mmcm) then
                if fifo_in_re(i*8 + to_integer(unsigned(subbcid_r))) = '1' then
                    fifo_out_din(i)(19 downto 0) <= fifo_in_dout(i*8 + to_integer(unsigned(subbcid_r)));
                    if (i*8 + to_integer(unsigned(subbcid_r))) < 64 then
                        fifo_out_din(i)(27 downto 20) <= '0' & std_logic_vector(to_unsigned(i*8 + to_integer(unsigned(subbcid_r)), 7));
                    else
                        fifo_out_din(i)(27 downto 20) <= '1' & std_logic_vector(to_unsigned(i*8 + to_integer(unsigned(subbcid_r)) - 64, 7));
                    end if;
                    fifo_out_we(i) <= '1';
                else
                    fifo_out_we(i) <= '0';
                end if;
            end if;
        end process;
    end generate;

    fifo_out_re_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            if fifo_out_empty(17) = '0' and fifo_out_re(17) = '0' then
                fifo_out_re <= "100000000000000000";
            elsif fifo_out_empty(16) = '0' and fifo_out_re(16) = '0' then
                fifo_out_re <= "010000000000000000";
            elsif fifo_out_empty(15) = '0' and fifo_out_re(15) = '0' then
                fifo_out_re <= "001000000000000000";
            elsif fifo_out_empty(14) = '0' and fifo_out_re(14) = '0' then
                fifo_out_re <= "000100000000000000";
            elsif fifo_out_empty(13) = '0' and fifo_out_re(13) = '0' then
                fifo_out_re <= "000010000000000000";
            elsif fifo_out_empty(12) = '0' and fifo_out_re(12) = '0' then
                fifo_out_re <= "000001000000000000";
            elsif fifo_out_empty(11) = '0' and fifo_out_re(11) = '0' then
                fifo_out_re <= "000000100000000000";
            elsif fifo_out_empty(10) = '0' and fifo_out_re(10) = '0' then
                fifo_out_re <= "000000010000000000";
            elsif fifo_out_empty(9) = '0' and fifo_out_re(9) = '0' then
                fifo_out_re <= "000000001000000000";
            elsif fifo_out_empty(8) = '0' and fifo_out_re(8) = '0' then
                fifo_out_re <= "000000000100000000";
            elsif fifo_out_empty(7) = '0' and fifo_out_re(7) = '0' then
                fifo_out_re <= "000000000010000000";
            elsif fifo_out_empty(6) = '0' and fifo_out_re(6) = '0' then
                fifo_out_re <= "000000000001000000";
            elsif fifo_out_empty(5) = '0' and fifo_out_re(5) = '0' then
                fifo_out_re <= "000000000000100000";
            elsif fifo_out_empty(4) = '0' and fifo_out_re(4) = '0' then
                fifo_out_re <= "000000000000010000";
            elsif fifo_out_empty(3) = '0' and fifo_out_re(3) = '0' then
                fifo_out_re <= "000000000000001000";
            elsif fifo_out_empty(2) = '0' and fifo_out_re(2) = '0' then
                fifo_out_re <= "000000000000000100";
            elsif fifo_out_empty(1) = '0' and fifo_out_re(1) = '0' then
                fifo_out_re <= "000000000000000010";
            elsif fifo_out_empty(0) = '0' and fifo_out_re(0) = '0' then
                fifo_out_re <= "000000000000000001";
            else
                fifo_out_re <= (others => '0');
            end if;
        end if;
    end process;

    fifo_out_num_out_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            fifo_out_re_r <= fifo_out_re;
            fifo_out_dout_r <= fifo_out_dout;
            if fifo_out_re_r(17) = '1' then
                fifo_out_num_out <= 17;
            elsif fifo_out_re_r(16) = '1' then
                fifo_out_num_out <= 16;
            elsif fifo_out_re_r(15) = '1' then
                fifo_out_num_out <= 15;
            elsif fifo_out_re_r(14) = '1' then
                fifo_out_num_out <= 14;
            elsif fifo_out_re_r(13) = '1' then
                fifo_out_num_out <= 13;
            elsif fifo_out_re_r(12) = '1' then
                fifo_out_num_out <= 12;
            elsif fifo_out_re_r(11) = '1' then
                fifo_out_num_out <= 11;
            elsif fifo_out_re_r(10) = '1' then
                fifo_out_num_out <= 10;
            elsif fifo_out_re_r(9) = '1' then
                fifo_out_num_out <= 9;
            elsif fifo_out_re_r(8) = '1' then
                fifo_out_num_out <= 8;
            elsif fifo_out_re_r(7) = '1' then
                fifo_out_num_out <= 7;
            elsif fifo_out_re_r(6) = '1' then
                fifo_out_num_out <= 6;
            elsif fifo_out_re_r(5) = '1' then
                fifo_out_num_out <= 5;
            elsif fifo_out_re_r(4) = '1' then
                fifo_out_num_out <= 4;
            elsif fifo_out_re_r(3) = '1' then
                fifo_out_num_out <= 3;
            elsif fifo_out_re_r(2) = '1' then
                fifo_out_num_out <= 2;
            elsif fifo_out_re_r(1) = '1' then
                fifo_out_num_out <= 1;
            elsif fifo_out_re_r(0) = '1' then
                fifo_out_num_out <= 0;
            else
                fifo_out_num_out <= 18;
            end if;
        end if;
    end process;

    elink_out_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            if fifo_out_num_out < 18 then   
                elink_out <= fifo_out_dout_r(fifo_out_num_out);
                read_en_ctrl_fifo <= '0';  
            else
                
                prep_ctrl_data_out <= '1';
                elink_out <= ctrl_data_out;
            end if;
            
            if prep_ctrl_data_out = '1' then
                ctrl_fifo_empty_r <= ctrl_fifo_empty;
                if ctrl_fifo_empty_r = '0' then
                    ctrl_data_out <= ctrl_fifo_dout;
                    read_en_ctrl_fifo <= '1';
                else
                    ctrl_data_out <= (others => '0'); -- for simulation
                    --ctrl_out(27 downto 12) <= X"FFFF";   
                    --ctrl_out(11 downto 0) <= bcid320;
                    read_en_ctrl_fifo <= '0';
                end if;
            end if;
        end if;
    end process;


 
    ----- Logic to read and write configuration registers -------------

    framealigner_inst : framealigner  
    generic map(
        header                 => "101010101011",
        requiredCorrectHeaders => 5,
        maxFalseHeaders        => 3,
        requiredCheckedHeaders => 4
    )
    port map(
        clock           => clock320_mmcm,
        reset           => int_reset,
        data_in         => elink_in(0),
        ctrl_word       => ctrl_word,
        ctrl_word_valid => ctrl_word_valid
    );
 
    
    
    
    WR_conf_registers_logic : process(clock320_mmcm) begin
        if rising_edge(clock320_mmcm) then
            if int_reset = '1' then
                channel_mask <= (others => '0');
                BC_offset <= (others => '0');
                timing_offset_in <= (others => (others => '0'));
                coarse_time_offset_in <= (others => (others => '0'));
                read_ctrl_word <= (others => '0');
            else
                ctrl_header      <= ctrl_word(31 downto 20);
                ctrl_WR          <= ctrl_word(19 downto 18);
                ctrl_reg_idx     <= ctrl_word(17 downto 15);
                ctrl_channel_idx <= to_integer(unsigned(ctrl_word(14 downto 6)));
                ctrl_data        <= ctrl_word(5 downto 0);
                if ctrl_word_valid = '1' then 
                    if ctrl_WR = "10" then
                        if ctrl_reg_idx = "000" and ctrl_channel_idx < 288 then  -- channel mask
                            channel_mask(ctrl_channel_idx) <= ctrl_data(0);
                        end if;
                        if ctrl_reg_idx = "001" then -- BC offset
                            BC_offset <= ctrl_data(5 downto 0);
                        end if;
                        if ctrl_reg_idx = "010" and ctrl_channel_idx < 18 then -- timing offset
                            timing_offset_in(ctrl_channel_idx) <= ctrl_data(4 downto 0);
                        end if;
                        if ctrl_reg_idx = "011"  and ctrl_channel_idx < 18 then -- coarse timing offset
                            coarse_time_offset_in(ctrl_channel_idx) <= ctrl_data(3 downto 0);
                        end if;
                        write_en_ctrl_fifo <= '0';
                    elsif ctrl_WR = "01" then
                        if ctrl_reg_idx = "000" and ctrl_channel_idx < 288 then  -- channel mask
                            read_ctrl_word <= ctrl_header(9 downto 0) & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "00000" & channel_mask(ctrl_channel_idx);
                        end if;
                        if ctrl_reg_idx = "001" then -- BC offset
                            read_ctrl_word <= ctrl_header(9 downto 0) & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & BC_offset;
                        end if;
                        if ctrl_reg_idx = "010" and ctrl_channel_idx < 18 then -- timing offset
                            read_ctrl_word <= ctrl_header(9 downto 0) & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "0" & timing_offset_in(ctrl_channel_idx);
                        end if;
                        if ctrl_reg_idx = "011"  and ctrl_channel_idx < 18 then -- coarse timing offset
                            read_ctrl_word <= ctrl_header(9 downto 0) & ctrl_reg_idx & std_logic_vector(to_unsigned(ctrl_channel_idx, 9)) & "00" & coarse_time_offset_in(ctrl_channel_idx);
                        end if;  
                        write_en_ctrl_fifo <= '1';
                    else 
                        write_en_ctrl_fifo <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    
    ctrl_fifo_out : fifo_16x28b
    port map (
        wr_clk     => clock320_mmcm,
        rd_clk     => clock320_mmcm,
        din        => read_ctrl_word,
        wr_en      => write_en_ctrl_fifo,
        rd_en      => read_en_ctrl_fifo,
        dout       => ctrl_fifo_dout,
        full       => open,
        empty      => ctrl_fifo_empty
    );
    
     ----- End of logic to read and write configuration registers -------------


end RTL;
