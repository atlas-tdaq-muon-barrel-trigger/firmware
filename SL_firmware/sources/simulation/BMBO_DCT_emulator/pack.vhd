------------------------------------------------------
--File:             pack.vhd
--Project:          BMBO DCT firmware
--Author:           Federico Morodei <federico.morodei@cern.ch>
--Last modified:    2023/06/28
------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



package pack is
    type array_18x4b is array(17 downto 0) of std_logic_vector(3 downto 0);
end package;

package body pack is

end pack;
