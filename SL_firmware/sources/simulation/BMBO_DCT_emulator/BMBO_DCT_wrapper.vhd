library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.math_real.uniform;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

library STD;
use STD.textio.all;

library xil_defaultlib;
use xil_defaultlib.pack.all;



entity BMBO_DCT_wrapper is
    port(
        clk_40      : in std_logic;
        clk_200     : in std_logic;
        clk_600     : in std_logic;
        clk_320     : in std_logic;
        reset       : in std_logic;
        elink28_out : out std_logic_vector(27 downto 0)
    );
end BMBO_DCT_wrapper;

architecture Behavioral of BMBO_DCT_wrapper is

    signal file_hits : std_logic_vector(287 downto 0) := (others => '0');
    signal file_hits_en : std_logic_vector(287 downto 0) := (others => '0');
        
    signal bcid : std_logic_vector(11 downto 0);

    signal clock40_edge : std_logic := '0';
    signal clock40_edge320_r : std_logic;
    signal clock40_edge200_r : std_logic;
    signal clock40_rising320 : std_logic;
    signal clock40_rising200 : std_logic;
    --signal clock40_rising200_r : std_logic;
        
    signal ctrl_data    : std_logic_vector (31 downto 0);
    signal counter_32   : std_logic_vector(4 downto 0);
    signal ctrl_elink_p : std_logic_vector(3 downto 0) := (others => '0');
    signal ctrl_elink_n : std_logic_vector(3 downto 0) := (others => '0');
    signal header       : std_logic_vector(11 downto 0);
    signal write_cmd    : std_logic;
    signal read_cmd     : std_logic;
    signal reg_idx      : std_logic_vector(2 downto 0);
    signal channel_idx  : std_logic_vector(8 downto 0);
    signal datum        : std_logic_vector(5 downto 0);
    --signal datacount    : array_18x4b;
    
    signal clk_40_n : std_logic;

begin


    clock40_edge_logic : process(clk_40) begin
        if rising_edge(clk_40) then
            clock40_edge <= not clock40_edge;
        end if;
    end process;

    clock40_rising320_logic : process(clk_320) begin
        if rising_edge(clk_320) then
            clock40_edge320_r <= clock40_edge;
            if clock40_edge320_r /= clock40_edge then
                clock40_rising320 <= '1';
            else
                clock40_rising320 <= '0';
            end if;
        end if;
    end process;

    clock40_rising200_logic : process(clk_200) begin
        if rising_edge(clk_200) then
            clock40_edge200_r <= clock40_edge;
            if clock40_edge200_r /= clock40_edge then
                clock40_rising200 <= '1';
            else
                clock40_rising200 <= '0';
            end if;
        end if;
    end process;

    bcid320_counter_logic : process(clk_320) begin
        if rising_edge(clk_320) then
            if reset = '1' then
                bcid <= X"000";
            else
                if clock40_rising320 = '1' then
                    bcid <= std_logic_vector(to_unsigned(to_integer(unsigned(bcid + 1)) mod 3563, 12));
                end if;
            end if;
        end if;
    end process;
    
    clk_40_n <= not clk_40;

    top_BMBO_DCT_inst : entity work.top_BMBO_DCT
    port map (
--        CLK40_0     => clock40_p,
--        CLK40_1     => clock40_p,
--        PSCLK0_p    => clock40_p,
--        PSCLK0_n    => clock40_n,
--        PSCLK1_p    => clock40_p,
--        PSCLK1_n    => clock40_n,
        ECLK0_p     => clk_40,
        ECLK0_n     => clk_40_n,
--        ECLK1_p     => clock40_p,
--        ECLK1_n     => clock40_n,
--        TST_CLK0_P  => clock40_p,
--        TST_CLK0_N  => clock40_n,
--        TST_CLK1_P  => clock40_p,
--        TST_CLK1_N  => clock40_n,
        rpc_in      => file_hits, --random_hits,
        elink_in_p  => ctrl_elink_p,
        elink_in_n  => ctrl_elink_n,
        elink_out_p => elink28_out,
        elink_out_n => open
        --fifo_datacount => datacount
    );


    
    
    ---- control data, through 1 downlink elink -------
    header <= "101010101011";
    write_cmd <= '1';
    read_cmd <= '0';
    reg_idx <= "010";
    channel_idx <= "000000000";
    datum <= "000010";
    
    ctrl_data <= header & write_cmd & read_cmd & reg_idx & channel_idx & datum;
    
    generate_ctrl_data : process(clk_320, reset) begin
        if reset = '1' then
            counter_32 <= (others => '0');
        elsif rising_edge(clk_320) then
            counter_32 <= counter_32 + 1;
            ctrl_elink_p(0) <= ctrl_data(to_integer(unsigned(counter_32)));
            ctrl_elink_n(0) <= not ctrl_data(to_integer(unsigned(counter_32)));
        end if;
    end process;
    
    
    file_hits_en_gen: process
        file simulation_hit_file: text;
        variable file_status : FILE_OPEN_STATUS;
        variable data_line : line;
        variable hit_time : real;
        variable eta_phi : integer;
        variable l0_l1 : integer;
        variable hit_index : integer;
        variable mu_match : integer;
    begin
        file_open(file_status, simulation_hit_file, "/home/fmorodei/data/firmware/SL_firmware/sources/simulation/BMBO_DCT_emulator/Data/outfile_BML_6BC_withmu_eta3.txt", read_mode);
        assert file_status = open_ok report FILE_OPEN_STATUS'IMAGE(file_status) & " while opening file!**" severity failure;
        while not endfile(simulation_hit_file) loop
            readline(simulation_hit_file, data_line);
            read(data_line, hit_time);
            read(data_line, eta_phi);
            read(data_line, l0_l1);
            read(data_line, hit_index); -- hit index in range [1,64]
            read(data_line, mu_match);
            while now < hit_time*1ns + 4 us loop
                 wait for 1 ps;
            end loop;
            file_hits_en(hit_index-1 + 144 * l0_l1 + 64 * eta_phi) <= not file_hits_en(hit_index-1 + 144 * l0_l1 + 64 * eta_phi);
        end loop;
        file_close(simulation_hit_file);
        assert false report "END OF SIMULATION" severity failure;
        wait;
    end process;

    file_hits_gen : for i in 0 to 287 generate
        eta_random_hit_logic : process(file_hits_en(i))
        begin
            if file_hits_en(i)'event then
                file_hits(i) <= '1', '0' after 10 ns;
            end if;
        end process;
    end generate;

    
   
    

end Behavioral;
