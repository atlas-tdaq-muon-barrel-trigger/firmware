library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;



entity lpgbt_emul_gt_exdes is
    generic
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP   : string    := "TRUE"; -- simulation setting for GT SecureIP model
        STABLE_CLOCK_PERIOD           : integer   := 6;
        emulate_DCT                   : integer   := 1     -- 1: emulate BMBO DCT and lpGBT. Use simulated DCT hits. 
                                                           -- 0: emulate lpGBT only. Use simple counter as uplink data.
    );
    port
    (
        Q2_CLK1_GTREFCLK_PAD_N_IN     : in   std_logic;
        Q2_CLK1_GTREFCLK_PAD_P_IN     : in   std_logic;
        DRP_CLK_IN_P                  : in   std_logic;
        DRP_CLK_IN_N                  : in   std_logic;
        RXN_IN                        : in   std_logic;
        RXP_IN                        : in   std_logic;
        TXN_OUT                       : out  std_logic;
        TXP_OUT                       : out  std_logic;
        clk_40_in_p                   : in std_logic;
        clk_40_in_n                   : in std_logic;
        clk_80                        : in std_logic;
        clk_320                       : in std_logic;
        clk_200                       : in std_logic;
        clk_600                       : in std_logic;
        reset                         : in std_logic
    );


end lpgbt_emul_gt_exdes;


    
architecture RTL of lpgbt_emul_gt_exdes is

    constant DLY : time := 1 ns;

    attribute ASYNC_REG : string;

    signal gt0_txfsmresetdone_i : std_logic;
    signal gt0_txfsmresetdone_r : std_logic;
    signal gt0_txfsmresetdone_r2 : std_logic;
    attribute ASYNC_REG of gt0_txfsmresetdone_r  : signal is "TRUE";
    attribute ASYNC_REG of gt0_txfsmresetdone_r2 : signal is "TRUE";
    signal gt0_rxfsmresetdone_i : std_logic;
    signal gt0_rxresetdone_r  : std_logic;
    signal gt0_rxresetdone_r2 : std_logic;
    signal gt0_rxresetdone_r3 : std_logic;
    attribute ASYNC_REG of gt0_rxresetdone_r  : signal is "TRUE";
    attribute ASYNC_REG of gt0_rxresetdone_r2 : signal is "TRUE";
    attribute ASYNC_REG of gt0_rxresetdone_r3 : signal is "TRUE";

    signal gt0_rxdata_i                    : std_logic_vector(31 downto 0);
    signal gt0_rxslide_i                   : std_logic;
    signal gt0_rxresetdone_i               : std_logic;
    signal gt0_txdata_i                    : std_logic_vector(31 downto 0);
    --signal gt0_txresetdone_i               : std_logic;
    signal gt0_tx_system_reset_c           : std_logic;
    signal gt0_rx_system_reset_c           : std_logic;
    signal drpclk_in_i                     : std_logic;
    signal DRPCLK_IN                       : std_logic;
    signal gt0_txusrclk2_i                 : std_logic; 
    signal gt0_rxusrclk2_i                 : std_logic;
    
    signal gt0_error_count_i               : std_logic_vector(7 downto 0);
    signal gt0_rx_data_valid               : std_logic;
    signal rxslide_counter : std_logic_vector(5 downto 0);

    signal downlink_clock_en       : std_logic; 
    signal downlink_data_group_0   : std_logic_vector(15 downto 0);
    signal downlink_data_group_1   : std_logic_vector(15 downto 0);
    signal downlink_data_ec        : std_logic_vector(1 downto 0);
    signal downlink_data_ic        : std_logic_vector(1 downto 0);
    signal downlink_ready          : std_logic;
    signal downlink_decoded_data   : std_logic_vector(31 downto 0);
    signal downlink_decoded_data_r : std_logic_vector(31 downto 0);
    
    signal uplink_user_data      : std_logic_vector(223 downto 0);
    signal uplink_ic_data        : std_logic_vector(1 downto 0);
    signal uplink_ec_data        : std_logic_vector(1 downto 0);
    signal uplink_ready_i        : std_logic;
    signal uplink_clk_en         : std_logic;
    signal uplink_clk_counter    : std_logic_vector(2 downto 0);
    
    signal gtwiz_userclk_tx_reset_int_DCT              : std_logic;
    signal gtwiz_userclk_rx_reset_int_DCT              : std_logic;
    signal qpll0outclk_int_DCT            : std_logic;
    signal qpll0outrefclk_int_DCT         : std_logic;
    signal gtpowergood_int_DCT            : std_logic;
   
   signal elink28   : std_logic_vector(27 downto 0);
   signal elink28x8 : std_logic_vector(223 downto 0);
   signal enable_data_from_file : std_logic;
    
begin

    

    gt0_tx_system_reset_c <= not gt0_txfsmresetdone_r2;
    gt0_rx_system_reset_c <= not gt0_rxresetdone_r3;

    IBUFDS_DRP_CLK : IBUFDS
    port map
    (
        I  => DRP_CLK_IN_P,
        IB => DRP_CLK_IN_N,
        O  => DRPCLK_IN
    );
    
    DRP_CLK_BUFG : BUFG 
    port map 
    (
        I    => DRPCLK_IN,
        O    => drpclk_in_i 
    );
    
    
    gtwiz_userclk_tx_reset_int_DCT  <= not gt0_txfsmresetdone_i;
    gtwiz_userclk_rx_reset_int_DCT  <= not gt0_rxfsmresetdone_i;
    
    
    
    GT_lpgbt_wrapper_inst: entity work.lpgbt_GT_example_wrapper
 port map
 (
   gtyrxn_in                               => RXN_IN,
   gtyrxp_in                               => RXP_IN,
   gtytxn_out                              => TXN_OUT,
   gtytxp_out                              => TXP_OUT,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_DCT,
   gtwiz_userclk_tx_srcclk_out             => open, 
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => gt0_txusrclk2_i,
   gtwiz_userclk_tx_active_out             => open,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_DCT,
   gtwiz_userclk_rx_srcclk_out             => open, 
   gtwiz_userclk_rx_usrclk_out             => open, 
   gtwiz_userclk_rx_usrclk2_out            => gt0_rxusrclk2_i,
   gtwiz_userclk_rx_active_out             => open, 
   gtwiz_reset_clk_freerun_in              => clk_80,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset,   
   gtwiz_reset_tx_datapath_in              => reset,   
   gtwiz_reset_rx_pll_and_datapath_in      => reset,   
   gtwiz_reset_rx_datapath_in              => reset,   
   gtwiz_reset_rx_cdr_stable_out           => open,
   gtwiz_reset_tx_done_out                 => gt0_rxresetdone_i,
   gtwiz_reset_rx_done_out                 => open,
   gtwiz_userdata_tx_in                    => gt0_txdata_i,
   gtwiz_userdata_rx_out                   => gt0_rxdata_i,
   gtrefclk00_in                           => clk_320,
   qpll0outclk_out                         => qpll0outclk_int_DCT,
   qpll0outrefclk_out                      => qpll0outrefclk_int_DCT,
   rxslide_in                              => gt0_rxslide_i,
   gtpowergood_out                         => gtpowergood_int_DCT,
   rxpmaresetdone_out                      => gt0_rxfsmresetdone_i,
   txpmaresetdone_out                      => gt0_txfsmresetdone_i
);

    rx_reset : process(gt0_rxusrclk2_i, gt0_rxresetdone_i)
    begin
        if (gt0_rxresetdone_i = '0') then
            gt0_rxresetdone_r  <= '0'   after DLY;
            gt0_rxresetdone_r2 <= '0'   after DLY;
            gt0_rxresetdone_r3 <= '0'   after DLY;
        elsif rising_edge(gt0_rxusrclk2_i) then
            gt0_rxresetdone_r  <= gt0_rxresetdone_i  after DLY;
            gt0_rxresetdone_r2 <= gt0_rxresetdone_r  after DLY;
            gt0_rxresetdone_r3 <= gt0_rxresetdone_r2 after DLY;
        end if;
    end process;
    
    tx_reset : process(gt0_txusrclk2_i, gt0_txfsmresetdone_i)
    begin
        if (gt0_txfsmresetdone_i = '0') then
            gt0_txfsmresetdone_r  <= '0'   after DLY;
            gt0_txfsmresetdone_r2 <= '0'   after DLY;
        elsif rising_edge(gt0_txusrclk2_i) then
            gt0_txfsmresetdone_r  <= gt0_txfsmresetdone_i after DLY;
            gt0_txfsmresetdone_r2 <= gt0_txfsmresetdone_r after DLY;
        end if;
    end process;


    BMBO_DCT_wrapper_inst : entity work.BMBO_DCT_wrapper
    port map (
        clk_40     => clk_40_in_p,
        clk_200    => clk_200,
        clk_600    => clk_600,
        clk_320    => clk_320,
        reset      => reset,
        elink28_out => elink28
    );
    
    elink_out_shift_reg : process(clk_320)
    begin
        if rising_edge(clk_320) then
            elink28x8 <= elink28x8(195 downto 0) & elink28;
        end if;
    end process;


    uplink_data_gen : process(clk_320)
    begin
        if rising_edge(clk_320) then
            if gt0_tx_system_reset_c = '1' then
                uplink_user_data <= (others => '0');
                uplink_ic_data <= (others => '0');
                uplink_ec_data <= (others => '0');
                uplink_clk_en <= '0';
                uplink_clk_counter <= (others => '0');
            else
                if uplink_clk_counter = "111" then
                    uplink_clk_en <= '1';
                    uplink_clk_counter <= (others => '0');
                    
                    if emulate_DCT = 0 then
                        uplink_user_data <= uplink_user_data + 1;
                        uplink_ic_data   <= uplink_ic_data + 1;
                        uplink_ec_data   <= uplink_ec_data + 1;
                    else
                        if enable_data_from_file = '1' then
                            uplink_user_data <= elink28x8;
                            uplink_ic_data   <= "00";
                            uplink_ec_data   <= "00";
                        end if;
                    end if;
                else
                    uplink_clk_counter <= uplink_clk_counter +1;
                    uplink_clk_en <= '0';
                end if;
            end if;
        end if;
    end process;
    
    process
    begin
        enable_data_from_file <= '0';
        wait for 20 us;
        enable_data_from_file <= '1';
        wait;
    end process;

    rxdata_check : process(gt0_rxusrclk2_i)
    begin
        if rising_edge(gt0_rxusrclk2_i) then
            if gt0_rx_system_reset_c = '1' then
                gt0_error_count_i <= (others => '0');
                gt0_rx_data_valid <= '0';
                rxslide_counter <= (others => '0');
            else
                gt0_rx_data_valid <= '1';
                downlink_decoded_data_r <= downlink_decoded_data;
                if gt0_rxfsmresetdone_i = '1' then
                    if downlink_decoded_data = downlink_decoded_data_r + 1 then
                        rxslide_counter <= (others => '0');
                    else
                        rxslide_counter <= rxslide_counter + 1;
                        if rxslide_counter = "000000" then
                            gt0_error_count_i <= gt0_error_count_i + 1;
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process;

    lpgbt_emul_inst : entity work.lpgbtemul_top
    generic map (
        rxslide_pulse_duration =>  1,  -- Duration of GT_RXSLIDE_OUT pulse
        rxslide_pulse_delay    =>  32  -- Minimum time between two GT_RXSLIDE_OUT pulses
    )
    port map (
        -- DownLink
        downlinkClkEn_o    => downlink_clock_en,
        downLinkDataGroup0 => downlink_data_group_0,
        downLinkDataGroup1 => downlink_data_group_1,
        downLinkDataEc     => downlink_data_ec,
        downLinkDataIc     => downlink_data_ic,
        downlinkRdy_o      => downlink_ready,
                           
        -- Uplink          
        uplinkClkEn_i      => uplink_clk_en,
        upLinkData0        => uplink_user_data(31 downto 0),
        upLinkData1        => uplink_user_data(63 downto 32),
        upLinkData2        => uplink_user_data(95 downto 64),
        upLinkData3        => uplink_user_data(127 downto 96),
        upLinkData4        => uplink_user_data(159 downto 128),
        upLinkData5        => uplink_user_data(191 downto 160),
        upLinkData6        => uplink_user_data(223 downto 192),
        upLinkDataIC       => uplink_ic_data,
        upLinkDataEC       => uplink_ec_data,
        uplinkRdy_o        => uplink_ready_i,
                           
        -- Uplink mode     
        fecMode            => '0',
        txDataRate         => '1',

		-- Transceiver     
        GT_RXUSRCLK_IN     => gt0_rxusrclk2_i,
        GT_TXUSRCLK_IN     => gt0_txusrclk2_i,
        GT_RXSLIDE_OUT     => gt0_rxslide_i,
        GT_TXREADY_IN      => gt0_txfsmresetdone_r2,
        GT_RXREADY_IN      => gt0_rxfsmresetdone_i,
        GT_TXDATA_OUT      => gt0_txdata_i,
        GT_RXDATA_IN       => gt0_rxdata_i

    ); 
    
    downlink_decoded_data <= downlink_data_group_1 & downlink_data_group_0;

end RTL;