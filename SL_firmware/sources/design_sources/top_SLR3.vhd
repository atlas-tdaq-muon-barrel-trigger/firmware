-------------------------------------------------------
-- File:          top_SLR3.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/18
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;


entity SLR3_top is
    Port (
    
        mgtrefclk0_x0y13_p  : std_logic;
        mgtrefclk0_x0y13_n  : std_logic;
        
         -- Serial data ports for transceivers to FELIX
        gtyrxn_TDAQ_in        : in std_logic_vector(3 downto 0);
        gtyrxp_TDAQ_in        : in std_logic_vector(3 downto 0);
        gtytxn_TDAQ_out       : out std_logic_vector(3 downto 0);
        gtytxp_TDAQ_out       : out std_logic_vector(3 downto 0);
       
        -- DCT data
        word_28_SLR0_in : in array_10x28b;
        word_28_SLR1_in : in array_20x28b;
        word_28_SLR2_in : in array_20x28b;
        
        -- Trigger candidates
        word_trigger_SLR1_in : in std_logic_vector(255 downto 0);
        word_trigger_SLR2_in : in std_logic_vector(255 downto 0);
        
        clock_320_in    : in std_logic;
        clock_240_in    : in std_logic;
        clock_80_in     : in std_logic;
        clock_40_in     : in std_logic;
        L0_acc          : in std_logic;
        reset           : in std_logic
    );
end SLR3_top;

architecture RTL of SLR3_top is
            
   component top_readout is
   Port ( 
         clock : in std_logic;
         bcid          : in std_logic_vector(11 downto 0);
         reset         : in std_logic;
         L0A           : in std_logic;
         BM_L_in       : in array_slv27to0x19DCT;
         BM_R_in       : in array_slv27to0x19DCT;
         BI_in         : in array_slv27to0x10DCT;
         BM_L_out      : out std_logic_vector(31 downto 0);
         BM_R_out      : out std_logic_vector(31 downto 0);
         BI_out        : out std_logic_vector(31 downto 0) 
   );
   end component;
   
   COMPONENT fifo_16x28b_sameclock is
        port (
            clk : IN STD_LOGIC;
            din    : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
            wr_en  : IN STD_LOGIC;
            rd_en  : IN STD_LOGIC;
            dout   : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
            full   : OUT STD_LOGIC;
            empty  : OUT STD_LOGIC
        );
    end COMPONENT;
    
    signal gtwiz_userclk_tx_reset_int_TDAQ      : std_logic;
    signal gtwiz_userclk_rx_reset_int_TDAQ      : std_logic;
    signal gtwiz_reset_rx_done_int_TDAQ         : std_logic;

    signal gtwiz_userdata_tx_int_TDAQ           : std_logic_vector (127 downto 0);
    signal gtwiz_userdata_rx_int_TDAQ           : std_logic_vector (127 downto 0);
    
    signal gtrefclk00_int_TDAQ                  : std_logic;
    signal qpll0outclk_int_TDAQ                 : std_logic;
    signal qpll0outrefclk_int_TDAQ              : std_logic;
    
    signal gtpowergood_int_TDAQ                 : std_logic_vector(3 downto 0);
    signal rxpmaresetdone_int_TDAQ              : std_logic_vector(3 downto 0);  
    signal txpmaresetdone_int_TDAQ              : std_logic_vector(3 downto 0);
        
    signal mgtrefclk0_x0y13_int                 : std_logic;
    
    signal en_8b10b                             : std_logic_vector(3 downto 0);
    signal txctrl                               : std_logic_vector(63 downto 0);  
    signal reset_gt_int_SLR3                    : std_logic;

    signal word_28_SLR0     : array_slv27to0x10DCT;
    signal word_28_SLR1     : array_slv27to0x19DCT;
    signal word_28_SLR2     : array_slv27to0x19DCT;
    
    signal word_28_SLR0_tmp :  array_10x28b;
    signal word_28_SLR1_tmp :  array_20x28b;
    signal word_28_SLR2_tmp :  array_20x28b;
    
    signal readout_data_SLR0             : std_logic_vector(31 downto 0);
    signal readout_data_SLR0_r           : std_logic_vector(31 downto 0);
    signal readout_data_SLR1             : std_logic_vector(31 downto 0);
    signal readout_data_SLR1_r           : std_logic_vector(31 downto 0);
    signal readout_data_SLR2             : std_logic_vector(31 downto 0);
    signal readout_data_SLR2_r           : std_logic_vector(31 downto 0);
    signal readout_data_SLR3             : std_logic_vector(31 downto 0);
    
    signal cdc_readout_write_enable_SLR0 : std_logic;
    signal cdc_readout_read_enable_SLR0  : std_logic;  
    signal cdc_readout_empty_SLR0        : std_logic;
    signal cdc_readout_write_enable_SLR1 : std_logic;
    signal cdc_readout_read_enable_SLR1  : std_logic;  
    signal cdc_readout_empty_SLR1        : std_logic;
    signal cdc_readout_write_enable_SLR2 : std_logic;
    signal cdc_readout_read_enable_SLR2  : std_logic;  
    signal cdc_readout_empty_SLR2        : std_logic;
    
    signal bcid      : std_logic_vector(11 downto 0) := (others => '0');
    signal bcid_240  : std_logic_vector(11 downto 0) := (others => '0');
    
    signal trigger_cand_toRAM : std_logic_vector(511 downto 0);
    signal trigger_addr       : std_logic_vector(8 downto 0) := "101111001";
    signal trigger_SLR3_out   : std_logic_vector(511 downto 0);
    
    type state_type is (IDLE, HEADER,
                        WORD1, WORD2, WORD3, WORD4,
                        WORD5, WORD6, WORD7, WORD8, 
                        WORD9, WORD10, WORD11, WORD12,
                        WORD13, WORD14, WORD15, WORD16,
                        FOOTER);
    signal state : state_type := IDLE;
    
    
begin
    
    en_8b10b <= "1111";
    txctrl   <= (others=>'0'); 
    
    gtrefclk00_int_TDAQ <= mgtrefclk0_x0y13_int;
    
    -- ===================================================================================================================
    -- BUFFERS
    -- ===================================================================================================================
 
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y13_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y13_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y13_p,         
      IB => mgtrefclk0_x0y13_n        
    );
    
    gtwiz_userclk_tx_reset_int_TDAQ <= not (and_reduce(txpmaresetdone_int_TDAQ));
    gtwiz_userclk_rx_reset_int_TDAQ <= not (and_reduce(rxpmaresetdone_int_TDAQ));

    reset_gt_int_SLR3 <= '0';
        
    GT_FELIX_wrapper_inst: entity work.GT_FELIX_SLR3_example_wrapper 
     port map 
     ( 
       gtyrxn_in                               => gtyrxn_TDAQ_in,
       gtyrxp_in                               => gtyrxp_TDAQ_in,
       gtytxn_out                              => gtytxn_TDAQ_out,
       gtytxp_out                              => gtytxp_TDAQ_out,
       gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_TDAQ,
       gtwiz_userclk_tx_srcclk_out             => open,
       gtwiz_userclk_tx_usrclk_out             => open, 
       gtwiz_userclk_tx_usrclk2_out            => open,
       gtwiz_userclk_tx_active_out             => open,
       gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_TDAQ,
       gtwiz_userclk_rx_srcclk_out             => open,
       gtwiz_userclk_rx_usrclk_out             => open,
       gtwiz_userclk_rx_usrclk2_out            => open,
       gtwiz_userclk_rx_active_out             => open,
       gtwiz_reset_clk_freerun_in              => clock_80_in,
       gtwiz_reset_all_in                      => reset,
       gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR3,   -- from VIO
       gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR3,   -- from VIO
       gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR3,   -- set to 0 by exdes
       gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR3,   -- from initializatio module and VIO
       gtwiz_reset_rx_cdr_stable_out           => open,
       gtwiz_reset_tx_done_out                 => open,
       gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_TDAQ,
       gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_TDAQ,
       gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_TDAQ,
       gtrefclk01_in                           => gtrefclk00_int_TDAQ,
       qpll1outclk_out                         => qpll0outclk_int_TDAQ,
       qpll1outrefclk_out                      => qpll0outrefclk_int_TDAQ,
       rx8b10ben_in                            => en_8b10b,
       tx8b10ben_in                            => en_8b10b,
       txctrl0_in                              => txctrl,
       txctrl1_in                              => txctrl,
       txctrl2_in                              => txctrl(31 downto 0),
       gtpowergood_out                         => gtpowergood_int_TDAQ,
       rxctrl0_out                             => open,
       rxctrl1_out                             => open,
       rxctrl2_out                             => open,
       rxctrl3_out                             => open,
       rxpmaresetdone_out                      => rxpmaresetdone_int_TDAQ,
       txpmaresetdone_out                      => txpmaresetdone_int_TDAQ,
       clock_240_in                            => clock_240_in
     );

    GEN_fifo_readout_SLR0 : for k in 0 to 9 generate
        fifo_readout_SLR0 : fifo_16x28b_sameclock
        port map(
            clk => clock_240_in,
            din    => word_28_SLR0_in(k), 
            wr_en  => '1', 
            rd_en  => '1', 
            dout   => word_28_SLR0_tmp(k),
            full   => open,
            empty  => open
        );
    end generate;
    
    GEN_fifo_readout_SLR1 : for k in 0 to 19 generate
        fifo_readout_SLR1 : fifo_16x28b_sameclock
        port map(
            clk => clock_240_in,
            din    => word_28_SLR1_in(k), 
            wr_en  => '1', 
            rd_en  => '1', 
            dout   => word_28_SLR1_tmp(k),
            full   => open,
            empty  => open
        );
    end generate;
    
    GEN_fifo_readout_SLR2 : for k in 0 to 19 generate
        fifo_readout_SLR2 : fifo_16x28b_sameclock
        port map(
            clk => clock_240_in,
            din    => word_28_SLR2_in(k), 
            wr_en  => '1', 
            rd_en  => '1', 
            dout   => word_28_SLR2_tmp(k),
            full   => open,
            empty  => open
        );
    end generate;

    trigger_cand_toRAM <= word_trigger_SLR1_in & word_trigger_SLR2_in;
    
    word_28_SLR0 <= array_slv27to0x10DCT(word_28_SLR0_tmp);
    word_28_SLR1 <= array_slv27to0x19DCT(word_28_SLR1_tmp(18 downto 0));
    word_28_SLR2 <= array_slv27to0x19DCT(word_28_SLR2_tmp(18 downto 0));


    top_readout_inst : top_readout 
    Port map( 
         clock        => clock_240_in,
         bcid         => bcid_240,
         reset        => reset,
         L0A          => L0_acc,
         BM_L_in      => word_28_SLR1,
         BM_R_in      => word_28_SLR2,
         BI_in        => word_28_SLR0,
         BM_L_out     => readout_data_SLR1,
         BM_R_out     => readout_data_SLR2,
         BI_out       => readout_data_SLR0 
    );
    
    cand_readout_inst: entity work.cand_readout
    Port map(
         clock     => clock_240_in,
         bcid_BM_L => bcid_240,   
         reset     => reset,
         L0A       => L0_acc,
         din       => trigger_cand_toRAM,
         dout      => trigger_SLR3_out
    );
    

    gtwiz_userdata_tx_int_TDAQ(31 downto 0)   <= readout_data_SLR0;
    gtwiz_userdata_tx_int_TDAQ(63 downto 32)  <= readout_data_SLR1;
    gtwiz_userdata_tx_int_TDAQ(95 downto 64)  <= readout_data_SLR2;
    gtwiz_userdata_tx_int_TDAQ(127 downto 96) <= readout_data_SLR3;
    
    
    divide_trigger_cand_readout : process(clock_240_in)
    begin
        if rising_edge(clock_240_in) then
            case state is 
                when IDLE =>
                    if L0_acc = '1' then
                        state <= HEADER;
                    elsif L0_acc = '0' then
                        state <= IDLE;
                    end if;
                when HEADER =>
                    readout_data_SLR3 <= (others => '0');
                    state <= WORD1;
                when WORD1 => 
                    readout_data_SLR3 <= trigger_SLR3_out(511 downto 480);
                    state <= WORD2;
                when WORD2 => 
                    readout_data_SLR3 <= trigger_SLR3_out(479 downto 448);
                    state <= WORD3;
                when WORD3 => 
                    readout_data_SLR3 <= trigger_SLR3_out(447 downto 416);
                    state <= WORD4;
                when WORD4 => 
                    readout_data_SLR3 <= trigger_SLR3_out(415 downto 384);
                    state <= WORD5;
                when WORD5 => 
                    readout_data_SLR3 <= trigger_SLR3_out(383 downto 352);
                    state <= WORD6;
                when WORD6 => 
                    readout_data_SLR3 <= trigger_SLR3_out(351 downto 320);
                    state <= WORD7;
                when WORD7 => 
                    readout_data_SLR3 <= trigger_SLR3_out(319 downto 288);
                    state <= WORD8;
                when WORD8 => 
                    readout_data_SLR3 <= trigger_SLR3_out(287 downto 256);
                    state <= WORD9;
                when WORD9 => 
                    readout_data_SLR3 <= trigger_SLR3_out(255 downto 224);
                    state <= WORD10;
                when WORD10 => 
                    readout_data_SLR3 <= trigger_SLR3_out(223 downto 192);
                    state <= WORD11;
                when WORD11 => 
                    readout_data_SLR3 <= trigger_SLR3_out(191 downto 160);
                    state <= WORD12;
                when WORD12 => 
                    readout_data_SLR3 <= trigger_SLR3_out(159 downto 128);
                    state <= WORD13;
                when WORD13 => 
                    readout_data_SLR3 <= trigger_SLR3_out(127 downto 96);
                    state <= WORD14;
                when WORD14 => 
                    readout_data_SLR3 <= trigger_SLR3_out(95 downto 64);
                    state <= WORD15;
                when WORD15 =>
                    readout_data_SLR3 <= trigger_SLR3_out(63 downto 32);
                    state <= WORD16;
                when WORD16 => 
                    readout_data_SLR3 <= trigger_SLR3_out(31 downto 0);
                    state <= FOOTER;
                when FOOTER =>
                    readout_data_SLR3 <= (others => '0');
                    if L0_acc = '0' then
                        state <= IDLE;
                    elsif L0_acc = '1' then
                        state <= HEADER;
                    end if;
                when others =>
                    state <= IDLE;
            end case;
        end if;
    end process;

    GEN_bcid: process(clock_40_in)
        begin
            if rising_edge(clock_40_in) then
                bcid <= bcid + 1;
            end if;
        end process;
        
    GEN_bcid_240 : process(clock_240_in)
        begin
            if rising_edge(clock_240_in) then
                bcid_240 <= bcid;
            end if;
        end process;
        
    
end RTL;

