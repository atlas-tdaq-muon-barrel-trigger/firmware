-------------------------------------------------------
-- File:          top_SLR0.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/18
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;
use xil_defaultlib.trigalgopkg.all;


entity SLR0_top is
    Port (
        -- Differential reference clock inputs SLR0
        mgtrefclk0_x0y2_p               : in std_logic;
        mgtrefclk0_x0y2_n               : in std_logic;
        mgtrefclk0_x1y1_p               : in std_logic;
        mgtrefclk0_x1y1_n               : in std_logic;
        
        -- Serial data ports for DCT transceivers
        gtyrxn_DCT_in         : in std_logic_vector(9 downto 0);
        gtyrxp_DCT_in         : in std_logic_vector(9 downto 0);
        gtytxn_DCT_out        : out std_logic_vector(9 downto 0);
        gtytxp_DCT_out        : out std_logic_vector(9 downto 0);
  
        -- Serial data ports for other transceivers
        gtyrxn_TDAQ_in        : in std_logic_vector(8 downto 0);
        gtyrxp_TDAQ_in        : in std_logic_vector(8 downto 0);
        gtytxn_TDAQ_out       : out std_logic_vector(8 downto 0);
        gtytxp_TDAQ_out       : out std_logic_vector(8 downto 0);
                   
        -- eta hits from BI DCTs                           
        eta_hits_out : out std_logic_vector(479 downto 0);
                                                           
        -- phi hits from BI DCTs                           
        phi_hits_out : out t_PHISTRIP_BI;
        
        -- data to readout
        word_28_out  : out array_10x28b;
        
        clock_320_in : in std_logic;
        clock_240_in : in std_logic;
        clock_80_in  : in std_logic;
        clock_40_in  : in std_logic;
        reset        : in std_logic
    );
end SLR0_top;

architecture RTL of SLR0_top is

    COMPONENT uplink_data_divider is
        port (
            clock_in            : in  std_logic;
            uplink_user_data_in : in  std_logic_vector(223 downto 0); 
            uplink_clk_en_in    : in  std_logic;
            word_28_out         : out std_logic_vector(27 downto 0)
        );
    end COMPONENT;
    
    component data_handler_BI is
        port (
            clock_in             : in std_logic;
            BCID_in              : in std_logic_vector(11 downto 0);
            word_28_in           : in std_logic_vector(27 downto 0);  
            read_enable_in       : in std_logic;
            eta_data_out         : out std_logic_vector(47 downto 0);  --  If the bit is 1, there is a hit in the corresponding strip
            phi_data_out         : out std_logic_vector(127 downto 0)
        );
    end component;


  COMPONENT lpgbtfpga_downlink IS
   GENERIC(
        -- Expert parameters
        c_multicyleDelay              : integer RANGE 0 to 7 := 1; --3                      --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                  : integer := 2;              --8                      --! Clock ratio is clock_out / 40 (shall be an integer - E.g.: 320/40 = 8)
        c_outputWidth                 : integer                                             --! Transceiver's word size (Typically 32 bits)
   );
   PORT (
        -- Clocks
        clk_i                         : in  std_logic;                                      --! Downlink datapath clock (Transceiver Tx User clock, typically 320MHz)
        clkEn_i                       : in  std_logic;                                      --! Clock enable (1 pulse over 8 clock cycles when encoding runs @ 320Mhz)
        rst_n_i                       : in  std_logic;                                      --! Downlink reset SIGNAL (Tx ready from the transceiver)

        -- Down link
        userData_i                    : in  std_logic_vector(31 downto 0);                  --! Downlink data (User)
        ECData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink EC field
        ICData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink IC field

        -- Output
        mgt_word_o                    : out std_logic_vector((c_outputWidth-1) downto 0);   --! Downlink encoded frame (IC + EC + User Data + FEC)

        -- Configuration
        interleaverBypass_i           : in  std_logic;                                      --! Bypass downlink interleaver (test purpose only)
        encoderBypass_i               : in  std_logic;                                      --! Bypass downlink FEC (test purpose only)
        scramblerBypass_i             : in  std_logic;                                      --! Bypass downlink scrambler (test purpose only)

        -- Status
        rdy_o                         : out std_logic                                       --! Downlink ready status
   );
  END COMPONENT;  

  COMPONENT lpgbtfpga_uplink IS
   GENERIC(
        -- General configuration
        DATARATE                        : integer RANGE 0 to 2;                               --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             : integer RANGE 0 to 2;                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                : integer RANGE 0 to 7 := 3;                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    : integer;                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  : integer;                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            : integer;                                            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       : integer;                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            : integer;                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                : integer := 1;                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               : integer := 40                                       --! Number of clock cycle required before being back in a stable state
   );
   PORT (
        -- Clock and reset
        uplinkClk_i                     : in  std_logic;                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                : out std_logic;                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   : in  std_logic;                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      : in  std_logic_vector((c_mgtWordWidth-1) downto 0);  --! Input frame coming from the MGT

        -- Data
        userData_o                      : out std_logic_vector(229 downto 0);                 --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        : out std_logic_vector(1 downto 0);                   --! EC field value received from the LpGBT
        IcData_o                        : out std_logic_vector(1 downto 0);                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             : in  std_logic;                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              : in  std_logic;                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               : in  std_logic;                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               : out std_logic;                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 : out std_logic_vector(229 downto 0);                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           : out std_logic;                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              : out std_logic                                       --! Number of bit slip is even (required only for advanced applications)

   );
END COMPONENT;

    
    COMPONENT fifo_16x28b is
        port (
            wr_clk   : IN STD_LOGIC;
            rd_clk   : IN STD_LOGIC;
            din   : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout  : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
            full  : OUT STD_LOGIC;
            empty : OUT STD_LOGIC
        );
    end COMPONENT;

  signal gtwiz_userclk_tx_reset_int_DCT              : std_logic;
  signal gtwiz_userclk_tx_active_int_DCT             : std_logic;
  signal gtwiz_userclk_rx_reset_int_DCT              : std_logic;
  signal gtwiz_reset_rx_done_int_DCT                 : std_logic;
  
  signal gtwiz_userclk_tx_reset_int_TDAQ             : std_logic;
  signal gtwiz_userclk_rx_reset_int_TDAQ             : std_logic;
  signal gtwiz_reset_rx_done_int_TDAQ                : std_logic_vector(1 downto 0);
  
  signal reset_gt_int_SLR0                           : std_logic;
    
  signal gtwiz_userdata_tx_int_DCT                   : std_logic_vector (319 downto 0);
  signal gtwiz_userdata_rx_int_DCT                   : std_logic_vector (319 downto 0);
  
  signal gtwiz_userdata_tx_int_TDAQ                  : std_logic_vector (287 downto 0);
  signal gtwiz_userdata_rx_int_TDAQ                  : std_logic_vector (287 downto 0);

  signal gtrefclk00_int_DCT             : std_logic_vector (2 downto 0);
  signal qpll0outclk_int_DCT            : std_logic_vector (2 downto 0);
  signal qpll0outrefclk_int_DCT         : std_logic_vector (2 downto 0);
  
  signal gtrefclk00_int_TDAQ            : std_logic_vector (2 downto 0);
  signal qpll0outclk_int_TDAQ           : std_logic_vector (2 downto 0);
  signal qpll0outrefclk_int_TDAQ        : std_logic_vector (2 downto 0);

  signal rxslide_int_DCT                : std_logic_vector(9 downto 0);
  signal gtpowergood_int_DCT            : std_logic_vector(9 downto 0);
  signal rxpmaresetdone_int_DCT         : std_logic_vector(9 downto 0);  
  signal txpmaresetdone_int_DCT         : std_logic_vector(9 downto 0);
  
  signal gtpowergood_int_TDAQ           : std_logic_vector(8 downto 0);
  signal rxpmaresetdone_int_TDAQ        : std_logic_vector(8 downto 0);  
  signal txpmaresetdone_int_TDAQ        : std_logic_vector(8 downto 0);
 
  signal mgtrefclk0_x0y2_int            : std_logic;
  signal mgtrefclk0_x1y1_int            : std_logic;
    
  signal downlink_clock_en_gen_SLR0     : std_logic;
  signal downlink_user_data_gen_SLR0    : std_logic_vector(31 downto 0);
  signal downlink_ec_data_gen_SLR0      : std_logic_vector(1 downto 0);
  signal downlink_ic_data_gen_SLR0      : std_logic_vector(1 downto 0);
  
  signal downlink_clock_en_SLR0         : std_logic_vector(9 downto 0);
  signal downlink_user_data_SLR0        : array_10x32b;
  signal downlink_ec_data_SLR0          : array_10x2b;
  signal downlink_ic_data_SLR0          : array_10x2b;
  signal downlink_ready_SLR0            : std_logic_vector(9 downto 0);
  
  signal uplink_user_data_SLR0          : array_10x230b;
  signal uplink_ic_data_SLR0            : array_10x2b;
  signal uplink_ec_data_SLR0            : array_10x2b; 
  signal uplink_dataCorrected_SLR0      : array_10x230b;
  signal uplink_IcCorrected_SLR0        : array_10x2b;
  signal uplink_EcCorrected_SLR0        : array_10x2b; 
  signal uplink_rdy_i_SLR0              : std_logic_vector(9 downto 0);
  signal uplink_clk_en_i_SLR0           : std_logic_vector(9 downto 0);
  
  signal word_28_int                    : array_10x28b;
  signal word_28_int_r                  : array_10x28b;
  signal eta_data_SLR0                  : array_10x48b;
  signal phi_data_SLR0                  : array_10x128b;
  signal uplink_data_valid              : std_logic_vector(9 downto 0);
  signal uplink_data_valid_r            : std_logic_vector(9 downto 0);
  
  signal bcid                           :  std_logic_vector(11 downto 0) := (others => '0');
  signal bcid_320                       : std_logic_vector(11 downto 0) := (others => '0'); 
  signal bcid_to_data_handler           :  std_logic_vector(11 downto 0) := (others => '0');
  type array_10x12b is array(9 downto 0) of std_logic_vector(11 downto 0);
  signal bcid_to_data_handler_r         : array_10x12b;
  
  signal read_en_data_handler    : std_logic;
  signal read_en_data_handler_r  : std_logic;
  signal read_en_data_handler_r2 : std_logic_vector(9 downto 0);
  
  signal en_8b10b   : std_logic_vector(8 downto 0);
  signal txctrl     : std_logic_vector(143 downto 0); 
    
  begin
  
    en_8b10b <= "111111111";
    txctrl   <= (others=>'0');
    
    gtrefclk00_int_DCT(0) <= mgtrefclk0_x1y1_int;
    gtrefclk00_int_DCT(1) <= mgtrefclk0_x1y1_int;
    gtrefclk00_int_DCT(2) <= mgtrefclk0_x1y1_int;
    
    gtrefclk00_int_TDAQ(0) <= mgtrefclk0_x0y2_int;
    gtrefclk00_int_TDAQ(1) <= mgtrefclk0_x0y2_int;
    gtrefclk00_int_TDAQ(2) <= mgtrefclk0_x0y2_int;
    
   
    -- ===================================================================================================================
    -- BUFFERS
    -- ===================================================================================================================
 
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y2_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y2_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y2_p,         
      IB => mgtrefclk0_x0y2_n        
    );
    
    
    IBUFDS_GTE4_MGTREFCLK0_X1Y1_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x1y1_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x1y1_p,         
      IB => mgtrefclk0_x1y1_n        
    );
    
   
    gtwiz_userclk_tx_reset_int_DCT <= not (and_reduce(txpmaresetdone_int_DCT));
    gtwiz_userclk_rx_reset_int_DCT <= not (and_reduce(rxpmaresetdone_int_DCT));
    gtwiz_userclk_tx_reset_int_TDAQ <= not (and_reduce(txpmaresetdone_int_TDAQ));
    gtwiz_userclk_rx_reset_int_TDAQ <= not (and_reduce(rxpmaresetdone_int_TDAQ));
    
    reset_gt_int_SLR0 <= '0';
   
 GT_DCT_wrapper_inst: entity work.GT_DCT_SLR0_example_wrapper
 port map
 (
   gtyrxn_in                               => gtyrxn_DCT_in,
   gtyrxp_in                               => gtyrxp_DCT_in,
   gtytxn_out                              => gtytxn_DCT_out,
   gtytxp_out                              => gtytxp_DCT_out,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_DCT,
   gtwiz_userclk_tx_srcclk_out             => open, 
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => gtwiz_userclk_tx_active_int_DCT,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_DCT,
   gtwiz_userclk_rx_srcclk_out             => open, 
   gtwiz_userclk_rx_usrclk_out             => open, 
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open, 
   gtwiz_reset_clk_freerun_in              => clock_80_in,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR0,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open, 
   gtwiz_reset_tx_done_out                 => open, 
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_DCT,
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_DCT,
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_DCT,
   gtrefclk00_in                           => gtrefclk00_int_DCT,
   qpll0outclk_out                         => qpll0outclk_int_DCT,
   qpll0outrefclk_out                      => qpll0outrefclk_int_DCT,
   rxslide_in                              => rxslide_int_DCT,
   gtpowergood_out                         => gtpowergood_int_DCT,
   rxpmaresetdone_out                      => rxpmaresetdone_int_DCT,
   txpmaresetdone_out                      => txpmaresetdone_int_DCT,
   clock_320_in                            => clock_320_in,
   clock_80_in                             => clock_80_in
);
 

 GT_TILECAL_wrapper_inst: entity work.GT_TILECAL_SLR0_example_wrapper
 port map 
 ( 
   gtyrxn_in                               => gtyrxn_TDAQ_in(5 downto 0),
   gtyrxp_in                               => gtyrxp_TDAQ_in(5 downto 0),
   gtytxn_out                              => gtytxn_TDAQ_out(5 downto 0),
   gtytxp_out                              => gtytxp_TDAQ_out(5 downto 0),
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_TDAQ,
   gtwiz_userclk_tx_srcclk_out             => open,
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => open,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_TDAQ,
   gtwiz_userclk_rx_srcclk_out             => open,
   gtwiz_userclk_rx_usrclk_out             => open,
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open,
   gtwiz_reset_clk_freerun_in              => clock_80_in,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR0,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open,
   gtwiz_reset_tx_done_out                 => open,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_TDAQ(0),
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_TDAQ(191 downto 0),
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_TDAQ(191 downto 0),
   gtrefclk01_in                           => gtrefclk00_int_TDAQ(1 downto 0),
   qpll1outclk_out                         => qpll0outclk_int_TDAQ(1 downto 0),
   qpll1outrefclk_out                      => qpll0outrefclk_int_TDAQ(1 downto 0),
   rx8b10ben_in                            => en_8b10b(5 downto 0),
   tx8b10ben_in                            => en_8b10b(5 downto 0),
   txctrl0_in                              => txctrl(95 downto 0),
   txctrl1_in                              => txctrl(95 downto 0),
   txctrl2_in                              => txctrl(47 downto 0),
   gtpowergood_out                         => gtpowergood_int_TDAQ(5 downto 0),
   rxctrl0_out                             => open,
   rxctrl1_out                             => open,
   rxctrl2_out                             => open,
   rxctrl3_out                             => open,
   rxpmaresetdone_out                      => rxpmaresetdone_int_TDAQ(5 downto 0),
   txpmaresetdone_out                      => txpmaresetdone_int_TDAQ(5 downto 0),
   clock_240_in                            => clock_240_in
);


GT_ENDCAP_wrapper_inst: entity work.GT_ENDCAP_SLR0_example_wrapper
 port map 
 ( 
   gtyrxn_in                               => gtyrxn_TDAQ_in(8 downto 6),
   gtyrxp_in                               => gtyrxp_TDAQ_in(8 downto 6),
   gtytxn_out                              => gtytxn_TDAQ_out(8 downto 6),
   gtytxp_out                              => gtytxp_TDAQ_out(8 downto 6),
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_TDAQ,
   gtwiz_userclk_tx_srcclk_out             => open,
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => open,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_TDAQ,
   gtwiz_userclk_rx_srcclk_out             => open,
   gtwiz_userclk_rx_usrclk_out             => open,
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open,
   gtwiz_reset_clk_freerun_in              => clock_80_in,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR0,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR0,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR0,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open,
   gtwiz_reset_tx_done_out                 => open,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_TDAQ(1),
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_TDAQ(287 downto 192),
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_TDAQ(287 downto 192),
   gtrefclk01_in                           => gtrefclk00_int_TDAQ(2),
   qpll1outclk_out                         => qpll0outclk_int_TDAQ(2),
   qpll1outrefclk_out                      => qpll0outrefclk_int_TDAQ(2),
   rx8b10ben_in                            => en_8b10b(8 downto 6),
   tx8b10ben_in                            => en_8b10b(8 downto 6),
   txctrl0_in                              => txctrl(143 downto 96),
   txctrl1_in                              => txctrl(143 downto 96),
   txctrl2_in                              => txctrl(119 downto 96),
   gtpowergood_out                         => gtpowergood_int_TDAQ(8 downto 6),
   rxctrl0_out                             => open,
   rxctrl1_out                             => open,
   rxctrl2_out                             => open,
   rxctrl3_out                             => open,
   rxpmaresetdone_out                      => rxpmaresetdone_int_TDAQ(8 downto 6),
   txpmaresetdone_out                      => txpmaresetdone_int_TDAQ(8 downto 6),
   clock_240_in                            => clock_240_in
);
 
GEN_DOWNLINK_SLR0: for i in 0 to 9 generate
downlink_inst_SLR0 : lpgbtfpga_downlink
    generic map (
        c_multicyleDelay => 1,
        c_clockRatio     => 2,
        c_outputWidth    => 32
    )
    port map (
        clk_i                 => clock_80_in,    
        clkEn_i               => downlink_clock_en_SLR0(i),
        rst_n_i               => txpmaresetdone_int_DCT(i), -- active low reset
        userData_i            => downlink_user_data_SLR0(i),
        ECData_i              => downlink_ec_data_SLR0(i),
        ICData_i              => downlink_ic_data_SLR0(i),
        mgt_word_o            => gtwiz_userdata_tx_int_DCT(31+32*i downto 32*i),
        interleaverBypass_i   => '0',
        encoderBypass_i       => '0',
        scramblerBypass_i     => '0',
        rdy_o                 => downlink_ready_SLR0(i)
    );
 end generate;
 
 
 GEN_UPLINK_SLR0: for i in 0 to 9 generate
   uplink_inst_SLR0 : lpgbtfpga_uplink 
   generic map(
        -- General configuration
        DATARATE                        => 2,            --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             => 1,            --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                => 3,            --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    => 8,            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  => 32,           --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            => 5,            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       => 64,           --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            => 30,           --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                => 1,            --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               => 40            --! Number of clock cycle required before being back in a stable state
   )
   port map (
        -- Clock and reset
        uplinkClk_i                     => clock_320_in,                                    --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                => uplink_clk_en_i_SLR0(i),                         --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   => gtwiz_reset_rx_done_int_DCT,                     --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      => gtwiz_userdata_rx_int_DCT(31+32*i downto 32*i),  --! Input frame coming from the MGT

        -- Data
        userData_o                      => uplink_user_data_SLR0(i),      --! User output (decoded data). The payload size varies depending on the
                                                                          --! datarate/FEC configuration:
                                                                          --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                          --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                          --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                          --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        => uplink_ec_data_SLR0(i),        --! EC field value received from the LpGBT
        IcData_o                        => uplink_ic_data_SLR0(i),        --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             => '0',                           --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              => '0',                           --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               => '0',                           --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               => rxslide_int_DCT(i),            --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 => uplink_dataCorrected_SLR0(i),  --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   => uplink_IcCorrected_SLR0(i),    --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   => uplink_EcCorrected_SLR0(i),    --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           => uplink_rdy_i_SLR0(i),          --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              => open                           --! Number of bit slip is even (required only for advanced applications)
   );
   end generate;
   
 
        GEN_uplink_data_divider: for i in 0 to 9 generate
            uplink_data_divider_inst_SLR0 : uplink_data_divider
                port map (
                    clock_in             => clock_320_in, 
                    uplink_user_data_in  => uplink_user_data_SLR0(i)(223 downto 0),
                    uplink_clk_en_in     => uplink_clk_en_i_SLR0(i),
                    word_28_out          => word_28_int(i)
                );
        end generate;
    
    
        GEN_bcid: process(clock_40_in)
        begin
            if rising_edge(clock_40_in) then
                bcid <= bcid + 1;
            end if;
        end process;
        
        
        
        GEN_bcid_to_data_handler : process(clock_320_in) 
        begin
            if rising_edge(clock_320_in) then
                bcid_320 <= bcid;
                bcid_to_data_handler <= bcid_320;
                word_28_int_r <= word_28_int; 
                uplink_data_valid_r <= uplink_data_valid;
                for i in 0 to 9 loop
                    bcid_to_data_handler_r(i) <= bcid_to_data_handler;
                end loop;
            end if;
        end process;
        
        
     read_enable_data_handler : process(clock_320_in)
     begin
        if rising_edge(clock_320_in) then
            if bcid_320 /= bcid_to_data_handler then 
                read_en_data_handler <= '1';
            else 
                read_en_data_handler <= '0';
            end if;
            read_en_data_handler_r <= read_en_data_handler;
            for i in 0 to 9 loop
                read_en_data_handler_r2(i) <= read_en_data_handler_r;
            end loop;
        end if;
    end process;    
  
    GEN_data_handler_BI: for i in 0 to 9 generate
        data_handler_inst_SLR0 : data_handler_BI
            port map(
                clock_in       => clock_320_in, 
                BCID_in        => bcid_to_data_handler_r(i),
                word_28_in     => word_28_int_r(i),
                read_enable_in => read_en_data_handler_r2(i),
                eta_data_out   => eta_data_SLR0(i), 
                phi_data_out   => phi_data_SLR0(i)
            );
    end generate;
    


    eta_hits_out <= eta_data_SLR0(9) & eta_data_SLR0(8) & eta_data_SLR0(7) & eta_data_SLR0(6) & eta_data_SLR0(5) & 
                    eta_data_SLR0(4) & eta_data_SLR0(3) & eta_data_SLR0(2) & eta_data_SLR0(1) & eta_data_SLR0(0);
                    
    phi_hits_out(0) <= phi_data_SLR0(0);
    phi_hits_out(1) <= phi_data_SLR0(1);
    phi_hits_out(2) <= phi_data_SLR0(2);
    phi_hits_out(3) <= phi_data_SLR0(3);
    phi_hits_out(4) <= phi_data_SLR0(4);
    phi_hits_out(5) <= phi_data_SLR0(5);
    phi_hits_out(6) <= phi_data_SLR0(6);
    phi_hits_out(7) <= phi_data_SLR0(7);
    phi_hits_out(8) <= phi_data_SLR0(8);
    phi_hits_out(9) <= phi_data_SLR0(9);
    
    
    
    GEN_fifo_readout_SLR0 : for k in 0 to 9 generate  -- needed to pass readout data to SLR3
        fifo_readout_SLR0 : fifo_16x28b
        port map(
            wr_clk  => clock_320_in,
            rd_clk  => clock_240_in,
            din     => word_28_int(k), 
            wr_en   => '1', 
            rd_en   => '1', 
            dout    => word_28_out(k),
            full    => open,
            empty   => open
        );
    end generate;

    
  
  ----- creation of data to be sent to downlink : TO BE REMOVED---
   downlink_data_gen_SLR0 : process(clock_80_in)    
    begin
        if rising_edge(clock_80_in) then          
            if gtwiz_userclk_tx_active_int_DCT = '0' then
                downlink_user_data_gen_SLR0 <= (others => '0');
                downlink_ec_data_gen_SLR0 <= (others => '0');
                downlink_ic_data_gen_SLR0 <= (others => '0');
                downlink_clock_en_gen_SLR0 <= '0';
            else
                    downlink_clock_en_gen_SLR0 <= not downlink_clock_en_gen_SLR0;
                    if downlink_clock_en_gen_SLR0 = '0' then
                        downlink_user_data_gen_SLR0 <= downlink_user_data_gen_SLR0 + 1;
                        downlink_ec_data_gen_SLR0 <= downlink_ec_data_gen_SLR0 + 1;
                        downlink_ic_data_gen_SLR0 <= downlink_ic_data_gen_SLR0 + 1;
                    end if;
            end if;
        end if;
    end process;
    
    
 
        DOWNLINK_ASSIGNMENT_SLR0: for i in 0 to 9 generate
           downlink_user_data_SLR0(i) <= downlink_user_data_gen_SLR0;
           downlink_ec_data_SLR0(i) <= downlink_ec_data_gen_SLR0;
           downlink_ic_data_SLR0(i) <= downlink_ic_data_gen_SLR0;
           downlink_clock_en_SLR0(i) <= downlink_clock_en_gen_SLR0;
        end generate;
        
    -------------------------------------------------------------------
end RTL;

