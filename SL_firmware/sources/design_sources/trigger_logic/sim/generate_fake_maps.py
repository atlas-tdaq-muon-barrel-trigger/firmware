'''    constant eta2tower : t_MAP_ETA_TOWER := (
        -- insert
    );
    constant chamber2tower : t_MAP_CHAMBER_TOWER := (
        -- insert
    );

    constant eta2chamber : t_MAP_ETA_CHAMBER := (
        -- insert
    );

    --------------------------------------
    -- pattern windows
    --------------------------------------
    constant windows_eta : t_WINDOW_TOWER_LAYER_PT := (
        -- insert
    );
    constant windows_phi : t_WINDOW_TOWER_LAYER := (
        -- insert
    );
'''

def run(n_layers, n_towers, n_ptbins, n_chambers, n_etabins):
    print('constant eta2tower : t_MAP_ETA_TOWER := (', end='')
    print(','.join([f'{int((eta/n_etabins)*n_towers):d}' for eta in range(n_etabins)]), end='')
    print(');')

    print('constant chamber2tower : t_MAP_CHAMBER_TOWER := (', end='')
    print(','.join([f'0' for chamber in range(n_chambers)]), end='')
    print(');')

    print('constant eta2chamber : t_MAP_ETA_CHAMBER := (', end='')
    print(','.join([f'{int((eta/n_etabins)*n_chambers):d}' for eta in range(n_etabins)]), end='')
    print(');')


    print('constant windows_eta : t_WINDOW_TOWER_LAYER_PT := (')
    for pt in range(n_ptbins):
        print(f'( -- pt bin {pt}')
        for layer in range(n_layers):
            print (f'    ( -- layer {layer}')
            toprint = [ f'(5, 5)' for tower in range(n_towers) ]
            print (','.join(toprint), end='')
            print('    ),')
        print('),')
    print(');')

    print('constant windows_phi : t_WINDOW_TOWER_LAYER := (')
    for layer in range(n_layers):
        print(f'( -- layer {layer}')
        toprint = [ f'(5, 5)' for tower in range(n_towers) ]
        print (','.join(toprint), end='')
        print('),')
    print(');')


if __name__ == '__main__':
    run(n_layers=4, n_towers=20, n_ptbins=4, n_chambers=10, n_etabins=512)