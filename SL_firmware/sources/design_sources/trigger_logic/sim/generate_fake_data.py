def run(N_strips, which_strips, layers, N_empty=1, repeat=10):
    hits = {}
    empty = {}
    for layer in layers:
        hits[layer] = [0 for i in range(N_strips[layer])]
        empty[layer] = list(hits[layer])
        for which_strip in which_strips[layer]:
            hits[layer][which_strip] = 1

    with open('prova.txt', 'w') as f:
        for i in range(repeat):
            # odd events are full
            for layer in layers:
                f.write(''.join(map(str, hits[layer][::-1]))) # write inverted
                f.write(' ')
            f.write('\n')

            # even events are empty
            for j in range(N_empty):
                for layer in layers:
                    f.write(''.join(map(str, empty[layer][::-1]))) # write inverted
                    f.write(' ')
            f.write('\n')



if __name__ == '__main__':
    layers = ['BI', 'BM1', 'BM2', 'BO'] # ordered
    N_strips = { 'BI': 320, 'BM1': 384, 'BM2': 448, 'BO': 384}
    #which_strips = { 'BI': [50, 100],  'BM1': [13, 100], 'BM2': [15, 100], 'BO': [22, 100]} # inferred from pack.vhd for infty-pt muon; BM2 is 51, not 50
    which_strips = { 'BI':  [50, 200], 
                     'BM1': [49, 200],
                     'BM2': [51, 200],
                     'BO':  [52, 200]} # brand-new, based on trigalgopkg.vhd mapping
    run(N_strips, which_strips, layers, N_empty=1, repeat=20)