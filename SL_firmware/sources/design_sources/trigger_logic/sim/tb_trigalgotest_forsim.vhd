----------------------------------------------------------------------------------
-- Company: Istituto Nazionale di Fisica Nucleare, Sezione di Roma
-- Engineer: Valerio Ippolito
-- 
-- Create Date: 02/07/2023 11:50:50 AM
-- Design Name: 
-- Module Name: tb_trigalgotest_forsim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Simulation test bench to speed up development of trig algorithm
--              This version implements only the trigger entity as defined in the
--              SLtrigger file. This test bench is dedicated to the task of validating
--              how the algorithm behaves, and reads input data from test files.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- operation with std_logic_vector
use IEEE.math_real.uniform; -- rand in [0,1)

-- load current code
library work;
use work.trigalgopkg.all;

library xil_defaultlib;
use xil_defaultlib.my_lib.all;

use IEEE.std_logic_textio.all;
use std.textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_SLtrigger_forsim is
--  Port ( );
end top_SLtrigger_forsim;

architecture Behavioral of top_SLtrigger_forsim is
    signal clock : std_logic := '0';
    signal eta_BI : t_ETASTRIP_BI := (others => '0'); -- init eta strips with no hits
    signal eta_BM1 : t_ETASTRIP_BM1 := (others => '0');
    signal eta_BM2 : t_ETASTRIP_BM2 := (others => '0');
    signal eta_BO : t_ETASTRIP_BO := (others => '0');
    signal phi_BI    :  t_PHISTRIP_BI := (others=> (others => '0'));
    signal phi_BM1   :   t_PHISTRIP_BM1 := (others=> (others => '0'));
    signal phi_BM2   :  t_PHISTRIP_BM2 := (others=> (others => '0')); 
    signal phi_BO    :  t_PHISTRIP_BO := (others=> (others => '0'));
    signal cand_1 : t_CANDIDATE := (others => '0');
    signal cand_2 : t_CANDIDATE := (others => '0');

    component SLtrigger is
    Port ( file_eta_hits_BI_orig    :  in t_ETASTRIP_BI; 
           file_eta_hits_BM1_orig   :  in t_ETASTRIP_BM1; 
           file_eta_hits_BM2_orig   :  in t_ETASTRIP_BM2;
           file_eta_hits_BO_orig    :  in t_ETASTRIP_BO;
           file_phi_hits_BI_orig    :  in t_PHISTRIP_BI; 
           file_phi_hits_BM1_orig   :  in t_PHISTRIP_BM1;
           file_phi_hits_BM2_orig   :  in t_PHISTRIP_BM2; 
           file_phi_hits_BO_orig    :  in t_PHISTRIP_BO; 
           cand_1    :  out   t_CANDIDATE;
           cand_2    :  out   t_CANDIDATE;
           clock_h    : in std_logic);
    end component;

begin
    pmap_SLtrigger : SLtrigger 
        port map (
            file_eta_hits_BI_orig => eta_BI,
            file_eta_hits_BM1_orig => eta_BM1,
            file_eta_hits_BM2_orig => eta_BM2,
            file_eta_hits_BO_orig => eta_BO,
            file_phi_hits_BI_orig => phi_BI,
            file_phi_hits_BM1_orig => phi_BM1,
            file_phi_hits_BM2_orig => phi_BM2,
            file_phi_hits_BO_orig => phi_BO,
            clock_h => clock,
            cand_1 => cand_1,
            cand_2 => cand_2
        );

    clock_gen : process
        constant period : Time := 5 ns;
    begin
        clock <= '1';
        wait for period;
        clock <= '0';
        wait for period;
    end process;

    file_readout : process(clock)
        variable N : integer := 0;
        file input_file : text open read_mode is "/data/vippolit/prova.txt";
        variable this_line : line;
        variable file_hits_BI: t_ETASTRIP_BI;
        variable file_hits_BM1: t_ETASTRIP_BM1;
        variable file_hits_BM2: t_ETASTRIP_BM2;
        variable file_hits_BO: t_ETASTRIP_BO;
    begin
        if rising_edge(clock) then
--          report "test " & integer'image(N);
            -- simulate readout of eta and phi hits
            if N = 20 then
                N := 0;
            end if;
            if N = 0 or N = 10 then
                if not endfile(input_file) then
                    readline(input_file, this_line);
                    read(this_line, file_hits_BI);
                    read(this_line, file_hits_BM1);
                    read(this_line, file_hits_BM2);
                    read(this_line, file_hits_BO);
                    eta_BI <= file_hits_BI;
                    eta_BM1 <= file_hits_BM1;
                    eta_BM2 <= file_hits_BM2;
                    eta_BO <= file_hits_BO;

                    -- for phi, we momentarily just change polarity
                    -- to assess timing
                    if N = 0 then
                        phi_BI <= (c_CHAMBER_BI_MAX downto 0 => (c_PHISTRIP_BI_MAX downto 0 => '0'));
                        phi_BM1 <= (c_CHAMBER_BM1_MAX downto 0 => (c_PHISTRIP_BM1_MAX downto 0 => '0'));
                        phi_BM2 <= (c_CHAMBER_BM2_MAX downto 0 => (c_PHISTRIP_BM2_MAX downto 0 => '0'));
                        phi_BO <= (c_CHAMBER_BO_MAX downto 0 => (c_PHISTRIP_BO_MAX downto 0 => '0'));
                    elsif N = 10 then
                        phi_BI <= (c_CHAMBER_BI_MAX downto 0 => (c_PHISTRIP_BI_MAX downto 0 => '1'));
                        phi_BM1 <= (c_CHAMBER_BM1_MAX downto 0 => (c_PHISTRIP_BM1_MAX downto 0 => '1'));
                        phi_BM2 <= (c_CHAMBER_BM2_MAX downto 0 => (c_PHISTRIP_BM2_MAX downto 0 => '1'));
                        phi_BO <= (c_CHAMBER_BO_MAX downto 0 => (c_PHISTRIP_BO_MAX downto 0 => '1'));
                    end if;
                end if;
            end if;
            N := N + 1;
        end if;
    end process;

end Behavioral;
