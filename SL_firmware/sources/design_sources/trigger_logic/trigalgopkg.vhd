library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--library xil_defaultlib;
--use xil_defaultlib.my_lib.all;


package trigalgopkg is

    -------------------------------------
    -- common constants
    -------------------------------------
    constant c_PT_THR_MAX : integer := 3; -- maximum pt threshold
    constant c_GETA_OFFSET_L : integer := 32; -- first global eta bin with meaningful info (offset is applied to avoid boundary effects)
    constant c_GETA_OFFSET_R : integer := 32; -- equivalent of c_ETA_OFFSET_R but to the right
    constant c_GPHI_OFFSET_L : integer := 32; -- first global phi bin with meaningful info (offset is applied to avoid boundary effects)
    constant c_GPHI_OFFSET_R : integer := 32; -- equivalent of c_GPHI_OFFSET_R but to the right
    constant c_GETA_MAX : integer := 511 + c_GETA_OFFSET_L + c_GETA_OFFSET_R; -- maximum bin number for global eta
    constant c_GPHI_MAX : integer := 127 + c_GPHI_OFFSET_L + c_GPHI_OFFSET_R; -- maximum bin number for global phi
--  constant c_GVAR_MAX : integer := c_GETA_MAX; -- maximum bin number for global eta and phi
    constant c_ETASTRIP_BI_MAX : integer := 319; -- maximum strip number for BI eta hits
    constant c_ETASTRIP_BI_MAX_WIDER : integer := 479; -- used by Federico before feeding to us
    constant c_ETASTRIP_BM1_MAX : integer := 383; -- maximum strip number for BM1 eta hits
    constant c_ETASTRIP_BM2_MAX : integer := 447; -- maximum strip number for BM2 eta hits
    constant c_ETASTRIP_BO_MAX : integer := 383; -- maximum strip number for BO eta hits
    constant c_CHAMBER_BI_MAX : integer := 9; -- maximum chamber index for BI
    constant c_CHAMBER_BM1_MAX : integer := 5; -- maximum chamber index for BM1
    constant c_CHAMBER_BM2_MAX : integer := 6; -- maximum chamber index for BM2
    constant c_CHAMBER_BO_MAX : integer := 5; -- maximum chamber index for BO
    constant c_CHAMBER_ALL_MAX : integer := 9; -- maximum defined chamber index (across all layers)
    constant c_PHISTRIP_OFFSET_L : integer := 20; -- first hit with meaningful info (offset is applied to avoid boundary effects)
    constant c_PHISTRIP_OFFSET_R : integer := 20; -- equivalent of c_PHISTRIP_OFFSET_L but to the right
    constant c_PHISTRIP_BI_MAX : integer := 127; -- maximum strip number for BI phi hits
    constant c_PHISTRIP_BM1_MAX : integer := 80+c_PHISTRIP_OFFSET_L+c_PHISTRIP_OFFSET_R; -- maximum strip number for BM1 phi hits
    constant c_PHISTRIP_BM2_MAX : integer := 80+c_PHISTRIP_OFFSET_L+c_PHISTRIP_OFFSET_R; -- maximum strip number for BM2 phi hits
    constant c_PHISTRIP_BO_MAX : integer := 80+c_PHISTRIP_OFFSET_L+c_PHISTRIP_OFFSET_R; -- maximum strip number for BO phi hits
    constant c_CAND_WMAX : integer := 127; -- last bit of candidate word
    constant c_LAYER_MAX : integer := 3; -- index of last layer (used to store windows)
    constant c_TOWER_MAX : integer := 19; -- index of last eta tower used to compute windows
    constant c_PTBIN_MAX : integer := 3; -- index of last pt bin used to compute windows
    constant c_PRIORITY_MAX : integer := 7; -- maximum priority value for candidates
    constant c_ETASPLIT_MAX : integer := 7; -- index of last etasplit used to find unique candidates
    constant c_ETASPLIT_SIZE : integer := 64; -- width of each etasplit, in eta bins
    constant c_BITS_AROUND_CANDETA : integer := c_GETA_OFFSET_L; -- bits before and after candeta used to determine closest hit in layer; cannot be higher than c_GETA_OFFSET_L
    constant c_MAXETA_FOR_CLOSESTHIT : integer := c_GETA_MAX - c_BITS_AROUND_CANDETA; -- used for finding closest hit to candidate
    constant c_NO_HIT_CLOSE_TO_CANDIDATE : integer := 2*c_BITS_AROUND_CANDETA+1; -- total number of bits used to determine closest hit in layer
    constant c_BIT_FOR_CANDETA : integer := c_BITS_AROUND_CANDETA; -- bit of the 64-bit word used for priority encoding which contains the eta of the candidate
    constant c_CANDIDATE_NOT_FOUND : integer := c_GETA_MAX+1; -- value used to indicate that no candidate was found
    constant c_PT_NOT_FOUND : integer := c_PT_THR_MAX+1; -- value used to indicate that no candidate was found
    

    -------------------------------------
    -- common types
    -------------------------------------
    subtype t_GETA is std_logic_vector(c_GETA_MAX downto 0);
    type t_GPHI is array(0 to c_CHAMBER_ALL_MAX) of std_logic_vector(c_GPHI_MAX downto 0);
    subtype t_ETASTRIP_BI is std_logic_vector(c_ETASTRIP_BI_MAX downto 0);
    subtype t_ETASTRIP_BI_WIDER is std_logic_vector(c_ETASTRIP_BI_MAX_WIDER downto 0);
    subtype t_ETASTRIP_BM1 is std_logic_vector(c_ETASTRIP_BM1_MAX downto 0);
    subtype t_ETASTRIP_BM2 is std_logic_vector(c_ETASTRIP_BM2_MAX downto 0);
    subtype t_ETASTRIP_BO is std_logic_vector(c_ETASTRIP_BO_MAX downto 0);
    type t_PHISTRIP_BI is array(0 to c_CHAMBER_BI_MAX) of std_logic_vector(c_PHISTRIP_BI_MAX downto 0);
    type t_PHISTRIP_BM1 is array(0 to c_CHAMBER_BM1_MAX) of std_logic_vector(c_PHISTRIP_BM1_MAX downto 0);
    type t_PHISTRIP_BM2 is array(0 to c_CHAMBER_BM2_MAX) of std_logic_vector(c_PHISTRIP_BM2_MAX downto 0);
    type t_PHISTRIP_BO is array(0 to c_CHAMBER_BO_MAX) of std_logic_vector(c_PHISTRIP_BO_MAX downto 0);
    type t_ETADEC is array(0 to c_CHAMBER_ALL_MAX) of std_logic_vector(3 downto 0);
    subtype t_CANDIDATE is std_logic_vector(c_CAND_WMAX downto 0);
    type t_CANDIDATE_DELAYED is array (integer range <>) of t_CANDIDATE;
    subtype t_LAYER is integer range 0 to c_LAYER_MAX;
    subtype t_INT_ETABIN is integer range 0 to c_GETA_MAX;
    type t_WINDOW is array(1 downto 0) of t_INT_ETABIN;
    type t_WINDOW_TOWER is array(c_TOWER_MAX downto 0) of t_WINDOW;
    type t_WINDOW_TOWER_LAYER is array(c_LAYER_MAX downto 0) of t_WINDOW_TOWER;
    type t_WINDOW_TOWER_LAYER_PT is array(c_PTBIN_MAX downto 0) of t_WINDOW_TOWER_LAYER;
    type t_MAP_ETA_TOWER is array(0 to c_GETA_MAX) of integer range 0 to c_TOWER_MAX;
    type t_MAP_CHAMBER_TOWER is array(0 to c_CHAMBER_ALL_MAX) of integer range 0 to c_TOWER_MAX;
    type t_MAP_ETA_CHAMBER is array(0 to c_GETA_MAX) of integer range 0 to c_CHAMBER_ALL_MAX;
    type t_ETAPAT is array (0 to c_PT_THR_MAX) of t_GETA;
    type t_PHIPAT is array(0 to c_CHAMBER_ALL_MAX) of std_logic_vector(c_GPHI_MAX downto 0);
    type t_CHAMBDEC is array (0 to c_CHAMBER_ALL_MAX+2) of std_logic; -- replicate two chambers to avoid boundary effects
    type t_ETAPAT_DELAYED is array (integer range <>) of t_ETAPAT;
    type t_PRIORITY is array(0 to c_GETA_MAX) of integer range 0 to c_PRIORITY_MAX;
    type t_PRIORITY_PT is array(0 to c_PT_THR_MAX) of t_PRIORITY;
    type t_ETAPATPRIO is array(0 to c_PRIORITY_MAX) of t_ETAPAT;
    type t_INDEX_ETASPLIT is array(c_ETASPLIT_MAX downto 0) of t_INT_ETABIN;
    type t_INDEX_ETASPLIT_PT is array(0 to c_PT_THR_MAX) of t_INDEX_ETASPLIT;
    subtype t_INT_PT is integer range 0 to c_PT_THR_MAX;
    subtype t_INT_CANDETABIN is integer range 0 to c_GETA_MAX+1; -- enlarged by 1 to represent "not found"
    subtype t_INT_CANDPT is integer range 0 to c_PT_THR_MAX+1; -- enlarged by 1 to represent "not found"
    type t_GETA_DELAYED is array (integer range <>) of t_GETA;
    type t_INT_CANDETABIN_DELAYED is array (integer range <>) of t_INT_CANDETABIN;
    type t_INT_CANDPT_DELAYED is array (integer range <>) of t_INT_CANDPT;

    -------------------------------------
    -- utilities for ambiguity resolution
    -------------------------------------
    function decluster_first_pass(word: std_logic_vector(6 downto 0)) return std_logic;
    function decluster_second_pass(in5, in4, in3, in2, in1, in0: std_logic_vector(6 downto 0)) return std_logic;
    function find_first_nonzero(signal word :  std_logic_vector(63 downto 0)) return t_INT_ETABIN;
    function find_closest_nonzero(signal word :  std_logic_vector(2*c_BITS_AROUND_CANDETA downto 0)) return t_INT_ETABIN; -- no need for -1 as we include the candidate eta bin

    -------------------------------------
    -- utilities for coordinate mapping
    -------------------------------------

    function loc2glo_BI_eta (signal strips: in t_ETASTRIP_BI) return t_GETA;
    function loc2glo_BM1_eta (signal strips: in t_ETASTRIP_BM1) return t_GETA;
    function loc2glo_BM2_eta (signal strips: in t_ETASTRIP_BM2) return t_GETA;
    function loc2glo_BO_eta (signal strips: in t_ETASTRIP_BO) return t_GETA;
    function loc2glo_BI_phi (signal strips: in t_PHISTRIP_BI) return t_GPHI;
    function loc2glo_BM1_phi (signal strips: in t_PHISTRIP_BM1) return t_GPHI;
    function loc2glo_BM2_phi (signal strips: in t_PHISTRIP_BM2) return t_GPHI;
    function loc2glo_BO_phi (signal strips: in t_PHISTRIP_BO) return t_GPHI;

    -------------------------------------
    -- sector geometry
    -------------------------------------
    constant eta2tower : t_MAP_ETA_TOWER := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    constant chamber2tower : t_MAP_CHAMBER_TOWER := (0,0,0,0,0,0,0,0,0,0);
    constant eta2chamber : t_MAP_ETA_CHAMBER := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    --------------------------------------
    -- pattern windows
    --------------------------------------
    constant windows_eta : t_WINDOW_TOWER_LAYER_PT := (
     ( -- pt bin 0
         ( -- layer 0
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 1
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 2
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 3
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    )
     ),
     ( -- pt bin 1
         ( -- layer 0
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 1
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 2
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 3
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    )
     ),
     ( -- pt bin 2
         ( -- layer 0
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 1
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 2
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 3
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    )
     ),
     ( -- pt bin 3
         ( -- layer 0
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 1
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 2
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    ),
         ( -- layer 3
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)    )
     )
     );
     constant windows_phi : t_WINDOW_TOWER_LAYER := (
     ( -- layer 0
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)),
     ( -- layer 1
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)),
     ( -- layer 2
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5)),
     ( -- layer 3
     (5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5),(5, 5))
     );

end package trigalgopkg;

package body trigalgopkg is
    -------------------------------------
    -- decluster_first_pass: first pass of declustering algorithm
    -- this function takes a fixed number of eta bins, 7, where in3 is the eta bin
    -- to be tested, and returns a bit telling us whether this eta bin contains
    -- a candidate (i.e. it's the cluster center in a +/- 3 window) or not
    -------------------------------------
    function decluster_first_pass(
        word: std_logic_vector(6 downto 0)
    )
    return std_logic is
        variable result : std_logic;
    begin
        if      (word = "0001001" )  then
            result := '1'; 
          elsif (word = "0111110" ) then
            result := '1'; 
          elsif (word = "1101001" ) then
            result := '1'; 
          elsif (word = "0111101" ) then
            result := '1'; 
          elsif (word = "1011001" ) then
            result := '1'; 
          elsif (word = "0011100" ) then
            result := '1'; 
          elsif (word = "0001000" ) then
            result := '1'; 
          elsif (word = "0001010" ) then
            result := '1'; 
          elsif (word = "0101000" ) then
            result := '1'; 
          elsif (word = "1101010" ) then
            result := '1'; 
          elsif (word = "0011101" ) then
            result := '1'; 
          elsif (word = "0111111" ) then
            result := '1'; 
          elsif (word = "1001001" ) then
            result := '1'; 
          elsif (word = "0101001" ) then
            result := '1'; 
          elsif (word = "0011001" ) then
            result := '1'; 
          elsif (word = "0011011" ) then
            result := '1'; 
          elsif (word = "1011000" ) then
            result := '1'; 
          elsif (word = "1001010" ) then
            result := '1'; 
          elsif (word = "0101011" ) then
            result := '1'; 
          elsif (word = "1101000" ) then
            result := '1'; 
          elsif (word = "0011000" ) then
            result := '1'; 
          elsif (word = "1001000" ) then
            result := '1'; 
          elsif (word = "1001011" ) then
            result := '1'; 
          elsif (word = "0011010" ) then
            result := '1'; 
          elsif (word = "1101011" ) then
            result := '1'; 
          elsif (word = "1011011" ) then
            result := '1'; 
          elsif (word = "1011100" ) then
            result := '1'; 
          elsif (word = "0001011" ) then
            result := '1'; 
          elsif (word = "0101010" ) then
            result := '1'; 
          elsif (word = "0111100" ) then
            result := '1'; 
          elsif (word = "1011101" ) then
            result := '1'; 
          elsif (word = "1011010" ) then
            result := '1'; 
          else 
            result := '0';
          end if;
              
        return result;
    end function decluster_first_pass;


    -------------------------------------
    -- decluster_second_pass: second pass of declustering algorithm
    -- this function checks if the cluster associated to the bin in3
    -- is the highest priority cluster in the window, or not
    -- and returns a 1 if it is, 0 otherwise
    -- the current implementation takes a fixed number of priorities (6)
    -- and a fixed eta window size (+/- 3 bins)
    -------------------------------------

    function decluster_second_pass(
        in5, in4, in3, in2, in1, in0: std_logic_vector(6 downto 0)
    ) return std_logic is
        variable result : std_logic;
    begin
        if(in5(3) = '1') then
            result := '1';
        elsif (in5 /= "0000000") then
            result := '0';
        elsif (in4(3) = '1') then
            result := '1';
        elsif (in4 /= "0000000") then
            result := '0';
        elsif (in3(3) = '1') then
            result := '1';
        elsif (in3 /= "0000000") then
            result := '0';
        elsif (in2(3) = '1') then
            result := '1';
        elsif (in2 /= "0000000") then
            result := '0';
        elsif (in1(3) = '1') then
            result := '1';
        elsif (in1 /= "0000000") then
            result := '0';
        elsif (in0(3) = '1') then
            result := '1';
        else 
            result := '0';
        end if;
        
        return result;
    end function decluster_second_pass;


    -------------------------------------
    -- find_first_nonzero: finds the first non-zero bit in a 64-bit vector
    -- returns 64 if all bits are zero
    -------------------------------------
    function find_first_nonzero(
        signal word :  std_logic_vector(63 downto 0)
    )
    return t_INT_ETABIN is
        variable result : t_INT_ETABIN;
    begin
        result := 64;

        for i in 63 downto 0 loop
            if word(i) = '1' then
                result := i;
            end if;
        end loop;

        return result;
    end function find_first_nonzero;


    -------------------------------------
    -- find_closest_nonzero: returns the index of the closest non-zero bit to the 0-th one
    -- a fixed window of +/- 5 bits is used (i.e. the rest of the (2*c_BITS_AROUND_CANDETA+1)-bit word is neglected)
    -- the window is centered around c_BIT_FOR_CANDETA
    -- returns c_NO_HIT_CLOSE_TO_CANDIDATE if all bits within the window are zero
    -------------------------------------
    function find_closest_nonzero(
        signal word :  std_logic_vector(2*c_BITS_AROUND_CANDETA downto 0)
    )
    return t_INT_ETABIN is
        variable result : t_INT_ETABIN;
    begin
        if word(c_BIT_FOR_CANDETA) = '1' then
            result := c_BIT_FOR_CANDETA;
        elsif word(c_BIT_FOR_CANDETA+1) = '1' then
            result := c_BIT_FOR_CANDETA + 1;
        elsif word(c_BIT_FOR_CANDETA-1) = '1' then
            result := c_BIT_FOR_CANDETA - 1;
        elsif word(c_BIT_FOR_CANDETA+2) = '1' then
            result := c_BIT_FOR_CANDETA + 2;
        elsif word(c_BIT_FOR_CANDETA-2) = '1' then
            result := c_BIT_FOR_CANDETA - 2;
        elsif word(c_BIT_FOR_CANDETA+3) = '1' then
            result := c_BIT_FOR_CANDETA + 3;
        elsif word(c_BIT_FOR_CANDETA-3) = '1' then
            result := c_BIT_FOR_CANDETA - 3;
        elsif word(c_BIT_FOR_CANDETA+4) = '1' then
            result := c_BIT_FOR_CANDETA + 4;
        elsif word(c_BIT_FOR_CANDETA-4) = '1' then
            result := c_BIT_FOR_CANDETA - 4;
        elsif word(c_BIT_FOR_CANDETA+5) = '1' then
            result := c_BIT_FOR_CANDETA + 5;
        elsif word(c_BIT_FOR_CANDETA-5) = '1' then
            result := c_BIT_FOR_CANDETA - 5;
        else
            result := c_NO_HIT_CLOSE_TO_CANDIDATE;
        end if;

        return result;
    end function find_closest_nonzero;

    

    -------------------------------------
    -- loc2glo_BI_eta: map (local) strip index to global eta bin
    -------------------------------------
    function loc2glo_BI_eta (
        signal strips: in t_ETASTRIP_BI
    )
    return t_GETA is
        variable result : t_GETA;
    begin
        -- dummy identity map
        for i in 0 to c_ETASTRIP_BI_MAX loop
            result(i + c_GETA_OFFSET_L) := strips(i);
        end loop;

        -- "zero-padding"
        for i in c_ETASTRIP_BI_MAX+1+c_GETA_OFFSET_L to c_GETA_MAX loop
            result(i) := '0';
        end loop;
        for i in 0 to c_GETA_OFFSET_L-1 loop
            result(i) := '0';
        end loop;

        return result;
    end function loc2glo_BI_eta;

    -------------------------------------
    -- loc2glo_BM1_eta: map (local) strip index to global eta bin
    -------------------------------------
    function loc2glo_BM1_eta (
        signal strips: in t_ETASTRIP_BM1
    )
    return t_GETA is
        variable result : t_GETA;
    begin
        -- dummy identity map
        for i in 0 to c_ETASTRIP_BM1_MAX loop
            result(i + c_GETA_OFFSET_L) := strips(i);
        end loop;

        -- "zero-padding"
        for i in c_ETASTRIP_BM1_MAX+1+c_GETA_OFFSET_L to c_GETA_MAX loop
            result(i) := '0';
        end loop;
        for i in 0 to c_GETA_OFFSET_L-1 loop
            result(i) := '0';
        end loop;

        return result;
    end function loc2glo_BM1_eta;

    -------------------------------------
    -- loc2glo_BM2_eta: map (local) strip index to global eta bin
    -------------------------------------
    function loc2glo_BM2_eta (
        signal strips: in t_ETASTRIP_BM2
    )
    return t_GETA is
        variable result : t_GETA;
    begin
        -- dummy identity map
        for i in 0 to c_ETASTRIP_BM2_MAX loop
            result(i + c_GETA_OFFSET_L) := strips(i);
        end loop;

        -- "zero-padding"
        for i in c_ETASTRIP_BM2_MAX+1+c_GETA_OFFSET_L to c_GETA_MAX loop
            result(i) := '0';
        end loop;
        for i in 0 to c_GETA_OFFSET_L-1 loop
            result(i) := '0';
        end loop;

        return result;
    end function loc2glo_BM2_eta;
    -------------------------------------
    -- loc2glo_BO_eta: map (local) strip index to global eta bin
    -------------------------------------
    function loc2glo_BO_eta (
        signal strips: in t_ETASTRIP_BO
    )
    return t_GETA is
        variable result : t_GETA;
    begin
        -- dummy identity map
        for i in 0 to c_ETASTRIP_BO_MAX loop
            result(i + c_GETA_OFFSET_L) := strips(i);
        end loop;

        -- "zero-padding"
        for i in c_ETASTRIP_BO_MAX+1+c_GETA_OFFSET_L to c_GETA_MAX loop
            result(i) := '0';
        end loop;
        for i in 0 to c_GETA_OFFSET_L-1 loop
            result(i) := '0';
        end loop;

        return result;
    end function loc2glo_BO_eta;

    -------------------------------------
    -- loc2glo_BI_phi: map (local) strip index to global phi bin
    -------------------------------------
    function loc2glo_BI_phi (
        signal strips: in t_PHISTRIP_BI
    )
    return t_GPHI is
        variable result : t_GPHI;
    begin
        -- dummy identity map
        for chamber in 0 to c_CHAMBER_BI_MAX loop
            for i in 0 to c_PHISTRIP_BI_MAX loop
                result(chamber)(i + c_GPHI_OFFSET_L) := strips(chamber)(i);
            end loop;

            -- "zero-padding"
            for i in c_PHISTRIP_BI_MAX+1+c_GPHI_OFFSET_L to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
            for i in 0 to c_GPHI_OFFSET_L-1 loop
                result(chamber)(i) := '0';
            end loop;
        end loop;
        -- "zero-padding"
        for chamber in c_CHAMBER_BI_MAX to c_CHAMBER_ALL_MAX loop
            for i in 0 to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
        end loop;

        return result;
    end function loc2glo_BI_phi;

    -------------------------------------
    -- loc2glo_BM1_phi: map (local) strip index to global phi bin
    -------------------------------------
    function loc2glo_BM1_phi (
        signal strips: in t_PHISTRIP_BM1
    )
    return t_GPHI is
        variable result : t_GPHI;
    begin
        -- dummy identity map
        for chamber in 0 to c_CHAMBER_BM1_MAX loop
            for i in 0 to c_PHISTRIP_BM1_MAX loop
                result(chamber)(i + c_GPHI_OFFSET_L) := strips(chamber)(i);
            end loop;

            -- "zero-padding"
            for i in c_PHISTRIP_BM1_MAX+1+c_GPHI_OFFSET_L to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
            for i in 0 to c_GPHI_OFFSET_L-1 loop
                result(chamber)(i) := '0';
            end loop;
        end loop;
        -- "zero-padding"
        for chamber in c_CHAMBER_BM1_MAX to c_CHAMBER_ALL_MAX loop
            for i in 0 to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
        end loop;

        return result;
    end function loc2glo_BM1_phi;
    -------------------------------------
    -- loc2glo_BM2_phi: map (local) strip index to global phi bin
    -------------------------------------
    function loc2glo_BM2_phi (
        signal strips: in t_PHISTRIP_BM2
    )
    return t_GPHI is
        variable result : t_GPHI;
    begin
        -- dummy identity map
        for chamber in 0 to c_CHAMBER_BM2_MAX loop
            for i in 0 to c_PHISTRIP_BM2_MAX loop
                result(chamber)(i + c_GPHI_OFFSET_L) := strips(chamber)(i);
            end loop;

            -- "zero-padding"
            for i in c_PHISTRIP_BM2_MAX+1+c_GPHI_OFFSET_L to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
            for i in 0 to c_GPHI_OFFSET_L-1 loop
                result(chamber)(i) := '0';
            end loop;
        end loop;
        -- "zero-padding"
        for chamber in c_CHAMBER_BM2_MAX to c_CHAMBER_ALL_MAX loop
            for i in 0 to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
        end loop;

        return result;
    end function loc2glo_BM2_phi;
    -------------------------------------
    -- loc2glo_BO_phi: map (local) strip index to global phi bin
    -------------------------------------
    function loc2glo_BO_phi (
        signal strips: in t_PHISTRIP_BO
    )
    return t_GPHI is
        variable result : t_GPHI;
    begin
        -- dummy identity map
        for chamber in 0 to c_CHAMBER_BO_MAX loop
            for i in 0 to c_PHISTRIP_BO_MAX loop
                result(chamber)(i + c_GPHI_OFFSET_L) := strips(chamber)(i);
            end loop;

            -- "zero-padding"
            for i in c_PHISTRIP_BO_MAX+1+c_GPHI_OFFSET_L to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
            for i in 0 to c_GPHI_OFFSET_L-1 loop
                result(chamber)(i) := '0';
            end loop;
        end loop;
        -- "zero-padding"
        for chamber in c_CHAMBER_BO_MAX to c_CHAMBER_ALL_MAX loop
            for i in 0 to c_GPHI_MAX loop
                result(chamber)(i) := '0';
            end loop;
        end loop;

        return result;
    end function loc2glo_BO_phi;
    
end package body trigalgopkg;
