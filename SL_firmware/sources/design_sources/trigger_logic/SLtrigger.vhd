----------------------------------------------------------------------------------
-- Company: Istituto Nazionale di Fisica Nucleare, Sezione di Roma
-- Engineer: Valerio Ippolito, Francesco Armando Di Bello (first implementation)
-- 
-- Create Date: 07/02/2023 15:09:00 AM (CET)
-- Design Name: SLtrigger
-- Module Name: SLtrigger - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Implementation of the pattern finder trigger algorithm
--              The module defines the entity SLtrigger, which implements
--              the trigger algorithm for a single sector logic sub-region
--              (i.e. the entity provides up to 2 candidates, and so is used
--              for reading out RPC data from a half-sector).
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - Moving from four to two muons per SL trigger instance
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;

-- PatFinder, the candidate finder
-- this will be an entity called PatFinder, which takes as input
-- the global eta bin and phi bin indexes, and returns four t_GETA and
-- four t_GPHI signals, where each bit represents whether a window on
-- each layer was found
entity PatFinder is 
    generic (
        LAYER : t_LAYER := 0 -- layer to use
    );
    Port (
        geta : in t_GETA;
        gphi : in t_GPHI;
        pat_eta : out t_ETAPAT;
        pat_phi : out t_PHIPAT;
        clock_h : in std_logic
    );
end PatFinder;

architecture Behavioral of PatFinder is
    signal pat_eta_reg : t_ETAPAT;
    signal pat_phi_reg : t_PHIPAT;
begin
    P_ETA_PATFINDER : process(clock_h)
    begin
        if rising_edge(clock_h) then
            for pt in 0 to c_PT_THR_MAX loop
                for eta in c_GETA_OFFSET_L to c_GETA_MAX - c_GETA_OFFSET_R loop
                    -- main logic
                    if geta(eta + windows_eta(pt)(LAYER)(eta2tower(eta))(1) downto eta - windows_eta(pt)(LAYER)(eta2tower(eta))(0)) /= (eta + windows_eta(pt)(LAYER)(eta2tower(eta))(1) downto eta - windows_eta(pt)(LAYER)(eta2tower(eta))(0) => '0') then
                        pat_eta_reg(pt)(eta) <= '1';
                    else
                        pat_eta_reg(pt)(eta) <= '0';
                    end if;
                end loop;
            end loop;
        end if;
    end process;

    P_PHI_PATFINDER : process(clock_h)
    begin
        if rising_edge(clock_h) then
            for chamber in 0 to c_CHAMBER_ALL_MAX loop
                for phi in c_GPHI_OFFSET_L to c_GPHI_MAX - c_GPHI_OFFSET_R loop
                    if gphi(chamber)(phi + windows_phi(LAYER)(chamber2tower(chamber))(1) downto phi - windows_phi(LAYER)(chamber2tower(chamber))(0)) /= (phi + windows_phi(LAYER)(chamber2tower(chamber))(1) downto phi - windows_phi(LAYER)(chamber2tower(chamber))(0) => '0') then
                        pat_phi_reg(chamber)(phi) <= '1';
                    else
                        pat_phi_reg(chamber)(phi) <= '0';
                    end if;
                end loop;
            end loop;
        end if;
    end process;

    -- eta pattern finder
    pat_eta <= pat_eta_reg;

    -- phi pattern finder
    pat_phi <= pat_phi_reg;
end Behavioral;


----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- Majority, the majority calculator
-- this will be an entity called Majority, which takes as input
-- the eta and phi patterns of the four PatFinder instances, and returns
-- a t_PHIPAT and a t_ETAPAT signal, where each bit represents whether
-- a 2/4 pattern was found for each eta bin
-- TODO: currently this operation is factored out from the priority
-- calculation; it might be brought back in to save resources, if latency
-- is not an issue

entity Majority is
    Port (
        pat_eta_1 : in t_ETAPAT;
        pat_eta_2 : in t_ETAPAT;
        pat_eta_3 : in t_ETAPAT;
        pat_eta_4 : in t_ETAPAT;
        pat_phi_1 : in t_PHIPAT;
        pat_phi_2 : in t_PHIPAT;
        pat_phi_3 : in t_PHIPAT;
        pat_phi_4 : in t_PHIPAT;
        pat_eta : out t_ETAPAT;
        pat_phi : out t_PHIPAT;
        clock_h : in std_logic
    );
end Majority;

architecture Behavioral of Majority is
    signal pat_eta_reg : t_ETAPAT;
    signal pat_phi_reg : t_PHIPAT;
begin

    P_ETA : process(clock_h)
    begin
        if rising_edge(clock_h) then
            for pt in 0 to c_PT_THR_MAX loop
                for eta in c_GETA_OFFSET_L to c_GETA_MAX - c_GETA_OFFSET_R loop
                    pat_eta_reg(pt)(eta) <= (pat_eta_1(pt)(eta) and pat_eta_2(pt)(eta)) or
                                                (pat_eta_1(pt)(eta) and pat_eta_3(pt)(eta)) or
                                                (pat_eta_1(pt)(eta) and pat_eta_4(pt)(eta)) or
                                                (pat_eta_2(pt)(eta) and pat_eta_3(pt)(eta)) or
                                                (pat_eta_2(pt)(eta) and pat_eta_4(pt)(eta)) or
                                                (pat_eta_3(pt)(eta) and pat_eta_4(pt)(eta));
                end loop;
            end loop;
        end if;
    end process;

    P_PHI : process(clock_h)
    begin
        if rising_edge(clock_h) then
            for chamber in 0 to c_CHAMBER_ALL_MAX loop
                for phi in c_GPHI_OFFSET_L to c_GPHI_MAX - c_GPHI_OFFSET_R loop
                    pat_phi_reg(chamber)(phi) <= (pat_phi_1(chamber)(phi) and pat_phi_2(chamber)(phi)) or
                                                     (pat_phi_1(chamber)(phi) and pat_phi_3(chamber)(phi)) or
                                                     (pat_phi_1(chamber)(phi) and pat_phi_4(chamber)(phi)) or
                                                     (pat_phi_2(chamber)(phi) and pat_phi_3(chamber)(phi)) or
                                                     (pat_phi_2(chamber)(phi) and pat_phi_4(chamber)(phi)) or
                                                     (pat_phi_3(chamber)(phi) and pat_phi_4(chamber)(phi));
                end loop;
            end loop;
        end if;
    end process;

    pat_eta <= pat_eta_reg;
    pat_phi <= pat_phi_reg;
end Behavioral;


----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- PhiChamberDecision, the entity which calculates the OR of the phi patterns
-- along the phi direction, for each chamber

entity PhiChamberDecision is
    Port (
        pat_phi : in t_PHIPAT;
        dec_phi : out t_CHAMBDEC;
        clock_h        : in std_logic
    );
end PhiChamberDecision;

architecture Behavioral of PhiChamberDecision is
    signal dec_phi_reg : t_CHAMBDEC;
begin
    P_DEC_PHI : process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- replicate first chamber to the left
            dec_phi_reg(0) <= or pat_phi(0)(c_GPHI_MAX - c_GPHI_OFFSET_R downto c_GPHI_OFFSET_L);

            for chamber in 0 to c_CHAMBER_ALL_MAX loop
                dec_phi_reg(chamber+1) <= or pat_phi(chamber)(c_GPHI_MAX - c_GPHI_OFFSET_R downto c_GPHI_OFFSET_L);
            end loop;

            -- replicate last chamber to the right
            dec_phi_reg(c_CHAMBER_ALL_MAX+2) <= or pat_phi(c_CHAMBER_ALL_MAX)(c_GPHI_MAX - c_GPHI_OFFSET_R downto c_GPHI_OFFSET_L);
        end if;
    end process;

    dec_phi <= dec_phi_reg;
end Behavioral;



----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- EtaPhiDecision, the entity which calculates for each eta bin the AND between
-- the eta decision and the OR of the phi decisions of the three corresponding 
-- chambers

entity EtaPhiDecision is
    Port (
        pat_eta : in t_ETAPAT;
        dec_phi : in t_CHAMBDEC;
        dec     : out t_ETAPAT;
        clock_h        : in std_logic
    );
end EtaPhiDecision;

architecture Behavioral of EtaPhiDecision is
    signal dec_reg : t_ETAPAT;
begin
        PT: for pt in 0 to c_PT_THR_MAX generate
          ETA: for eta in c_GETA_OFFSET_L to c_GETA_MAX - c_GETA_OFFSET_R generate
                      P_DEC : process(clock_h)
            begin
                if rising_edge(clock_h) then

                    -- since the first and last element of dec_phi is a copy of the first and last chamber,
                    -- we need to shift the chamber index by 1 - so we use eta2chamber(eta) + 1
                    -- and we look at this +/- 1 chamber as well, so that the two
                    -- adjacent chambers are considered
                    dec_reg(pt)(eta) <= pat_eta(pt)(eta) and (
                                            dec_phi(eta2chamber(eta)) or
                                            dec_phi(eta2chamber(eta) + 1) or
                                            dec_phi(eta2chamber(eta) + 2)
                                        );
                end if;
            end process;
          end generate;
    end generate;

    dec <= dec_reg;
end Behavioral;

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- PriorityGetter, the entity which determines the priority of the candidate in each
-- eta bin based on which layers fired

entity PriorityGetter is
    Port (
        dec : in t_ETAPAT;
        lay1 : in t_ETAPAT;
        lay2 : in t_ETAPAT;
        lay3 : in t_ETAPAT;
        lay4 : in t_ETAPAT;
        dec_prio : out t_ETAPATPRIO;
        clock_h  : in std_logic
    );
end PriorityGetter;

architecture Behavioral of PriorityGetter is
    signal priority : t_PRIORITY_PT;
    signal dec_prio_reg : t_ETAPATPRIO;
begin
    P_PRIORITY : process(clock_h)
    -- first clock cycle: determine priority
    begin
        if rising_edge(clock_h) then
            for pt in 0 to c_PT_THR_MAX loop
                for eta in c_GETA_OFFSET_L to c_GETA_MAX - c_GETA_OFFSET_R loop
                    if dec(pt)(eta) = '1' then
                        if lay1(pt)(eta) = '1' and lay2(pt)(eta) = '1' and lay3(pt)(eta) = '1' and lay4(pt)(eta) = '1' then
                            priority(pt)(eta) <= c_PRIORITY_MAX;
                        elsif lay2(pt)(eta) = '1' and lay3(pt)(eta) = '1' and lay4(pt)(eta) = '1' then
                            priority(pt)(eta) <= c_PRIORITY_MAX - 1;
                        elsif lay1(pt)(eta) = '1' and lay3(pt)(eta) = '1' and lay4(pt)(eta) = '1' then
                            priority(pt)(eta) <= c_PRIORITY_MAX - 2;
                        elsif lay1(pt)(eta) = '1' and lay2(pt)(eta) = '1' and lay4(pt)(eta) = '1' then
                            priority(pt)(eta) <= c_PRIORITY_MAX - 3;
                        elsif lay1(pt)(eta) = '1' and lay2(pt)(eta) = '1' and lay3(pt)(eta) = '1' then
                            priority(pt)(eta) <= c_PRIORITY_MAX - 4;
                        elsif lay1(pt)(eta) = '1' and lay4(pt)(eta) = '1' then
                            priority(pt)(eta) <= c_PRIORITY_MAX - 5; 
                        elsif (lay2(pt)(eta) = '1' and lay4(pt)(eta) = '1') or (lay3(pt)(eta) = '1' and lay4(pt)(eta) = '1') then
                            priority(pt)(eta) <= c_PRIORITY_MAX - 6;
                        else
                            priority(pt)(eta) <= 0; 
                        end if;
                    else
                        priority(pt)(eta) <= 0;
                    end if;
                end loop;
            end loop;
        end if;
    end process;

    P_DEC_PRIO : process(clock_h)
    -- second clock cycle: assign bit of dec_prio_reg for each
    -- priority level, pt and eta bin
    begin
        if rising_edge(clock_h) then
            for pt in 0 to c_PT_THR_MAX loop
                for eta in c_GETA_OFFSET_L to c_GETA_MAX - c_GETA_OFFSET_R loop
                    for prio in 0 to c_PRIORITY_MAX loop
                        if priority(pt)(eta) = prio then
                            dec_prio_reg(prio)(pt)(eta) <= '1';
                        else
                            dec_prio_reg(prio)(pt)(eta) <= '0';
                        end if;
                    end loop;
                end loop;
            end loop;
        end if;
    end process;

    dec_prio <= dec_prio_reg;
end Behavioral;

----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;
--use work.pack_old.all; -- for testing the simplified 2nd pass of declustering

-- Declustering, the entity which performs declustering to
-- remove duplicate candidates
-- a fixed window size of +/- 3 bins, as in the helper functions
-- decluster_first_pass and decluster_second_pass, and 
-- a fixed number of priorities, 7, is used

entity Declustering is
    Port (
        dec_prio : in t_ETAPATPRIO;
        dec_declus : out t_ETAPAT;
        clock_h  : in std_logic
    );
end Declustering;

architecture Behavioral of Declustering is
    signal dec_prio_declus_reg : t_ETAPATPRIO;
    constant c_WINDOW_SIZE : integer := 3; -- NOTE: this is hardcoded in the helper functions! must be lower than c_GETA_OFFSET_L and c_GETA_OFFSET_R
begin
    -- first declustering pass
    DECLUSTERING_FIRSTPASS_PRIO: for prio in 0 to c_PRIORITY_MAX generate -- faster synthesis than 3 for loop
	    DECLUSTERING_FIRSTPASS_PRIO_PT: for pt in 0 to c_PT_THR_MAX generate
		    DECLUSTERING_FIRSTPASS_PRIO_ETA: for eta in c_GETA_OFFSET_L to c_GETA_MAX-c_GETA_OFFSET_R generate
			    FORPRIOPTETA: process(clock_h)
			    begin
				if rising_edge(clock_h) then
				    -- first pass
					    dec_prio_declus_reg(prio)(pt)(eta) <= decluster_first_pass(dec_prio(prio)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE));
				end if;
			    end process;
		    end generate;
	    end generate;
    end generate;


    -- second pass
    DECLUSTERING_SECONDPASS_PT: for pt in 0 to c_PT_THR_MAX generate
	    DECLUSTERING_SECONDPASS_ETA: for eta in c_GETA_OFFSET_L to c_GETA_MAX-c_GETA_OFFSET_R generate
		    FORPTETA: process(clock_h)
		    begin
			if rising_edge(clock_h) then
			    -- second pass
			    dec_declus(pt)(eta) <= decluster_second_pass(
				dec_prio_declus_reg(c_PRIORITY_MAX)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE),
				dec_prio_declus_reg(c_PRIORITY_MAX-1)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE),
				dec_prio_declus_reg(c_PRIORITY_MAX-2)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE),
				dec_prio_declus_reg(c_PRIORITY_MAX-3)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE),
				dec_prio_declus_reg(c_PRIORITY_MAX-4)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE),
				dec_prio_declus_reg(c_PRIORITY_MAX-5)(pt)(eta+c_WINDOW_SIZE downto eta-c_WINDOW_SIZE)
			    );
			end if;
		    end process;
	    end generate;
    end generate;
end Behavioral;


----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- CandidateSelector, the entity that selects the bin etas which
-- contain the best muon candidates, and returns their eta value
-- and maximum pT threshold passed

entity CandidateSelector is
    Port (
        dec_declus : in t_ETAPAT;
        cand1_eta : out t_INT_CANDETABIN;
        cand1_pt : out t_INT_CANDPT;
        cand2_eta : out t_INT_CANDETABIN;
        cand2_pt : out t_INT_CANDPT;
        clock_h  : in std_logic
    );
end CandidateSelector;

architecture Behavioral of CandidateSelector is
    signal etasplit_cand : t_INDEX_ETASPLIT_PT;
    signal etasplit_cand_copy : t_INDEX_ETASPLIT_PT;
    signal etasplit_cand_copy2 : t_INDEX_ETASPLIT_PT; -- a copy to delay
    signal cand1_eta_reg : t_INT_CANDETABIN;
    signal cand1_pt_reg : t_INT_CANDPT;
    signal cand1_eta_reg2 : t_INT_CANDETABIN; -- a copy to sync with cand2
    signal cand1_pt_reg2 : t_INT_CANDPT; -- a copy to sync with cand2
    signal cand2_eta_reg : t_INT_CANDETABIN;
    signal cand2_pt_reg : t_INT_CANDPT;
begin
    -- find best candidates in each etasplit
    ETASPLIT_PT :  for pt in 0 to c_PT_THR_MAX generate
        ETASPLIT_PT_ETA : for etasplit in 0 to c_ETASPLIT_MAX generate
            P_ETASPLIT : process(clock_h)
            begin
                if rising_edge(clock_h) then
                    etasplit_cand(pt)(etasplit) <= find_first_nonzero(dec_declus(pt)((etasplit+1)*c_ETASPLIT_SIZE-1+c_GETA_OFFSET_L downto etasplit*c_ETASPLIT_SIZE+c_GETA_OFFSET_L));

                    -- a second copy of the signal, for the second candidate
                    etasplit_cand_copy(pt)(etasplit) <= find_first_nonzero(dec_declus(pt)((etasplit+1)*c_ETASPLIT_SIZE-1+c_GETA_OFFSET_L downto etasplit*c_ETASPLIT_SIZE+c_GETA_OFFSET_L));
                end if;
            end process;
        end generate;
    end generate;


    P_CANDIDATE1 : process(clock_h)
    begin        
        if rising_edge(clock_h) then
            -- final statement, in case no hit was found
            -- this will be overridden by the first hit found
            cand1_eta_reg <= c_CANDIDATE_NOT_FOUND;
            cand1_pt_reg <= c_PT_NOT_FOUND;

            -- copy signal to avoid timing issues
            etasplit_cand_copy2 <= etasplit_cand_copy;

            PT_LOOP1: for pt in 0 to c_PT_THR_MAX loop
                ETASPLIT_LOOP1: for etasplit in 0 to c_ETASPLIT_MAX loop
                    -- c_ETASPLIT_SIZE means no match was found in this tower for this pT threshold
                    if etasplit_cand(pt)(etasplit) /= c_ETASPLIT_SIZE then
                        cand1_eta_reg <= etasplit*c_ETASPLIT_SIZE + etasplit_cand(pt)(etasplit) + c_GETA_OFFSET_L;
                        cand1_pt_reg <= pt;
                        -- we update the copy for the second pass (next clock cycle)
                        etasplit_cand_copy2(pt)(etasplit) <= c_ETASPLIT_SIZE;
                        exit PT_LOOP1; -- we take the highest-priority, lowest-etasplit candidate
                    end if;
                end loop;
            end loop;
        end if;
    end process;

    P_CANDIDATE2 : process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- propagate cand1 to next cycle
            cand1_eta_reg2 <= cand1_eta_reg;
            cand1_pt_reg2 <= cand1_pt_reg;

            -- final statement, in case no hit was found
            -- this will be overridden by the first hit found
            cand2_eta_reg <= c_CANDIDATE_NOT_FOUND;
            cand2_pt_reg <= c_PT_NOT_FOUND;
    
            PT_LOOP2: for pt in 0 to c_PT_THR_MAX loop
                ETASPLIT_LOOP2: for etasplit in 0 to c_ETASPLIT_MAX loop
                    -- c_ETASPLIT_SIZE means no match was found in this tower for this pT threshold
                    if etasplit_cand_copy2(pt)(etasplit) /= c_ETASPLIT_SIZE then
                        cand2_eta_reg <= etasplit*c_ETASPLIT_SIZE + etasplit_cand_copy2(pt)(etasplit) + c_GETA_OFFSET_L;
                        cand2_pt_reg <= pt;
                        exit PT_LOOP2;
                    end if;
                end loop;
            end loop;
        end if;
    end process;

    cand1_eta <= cand1_eta_reg2; -- use delayed
    cand1_pt <= cand1_pt_reg2; -- use delayed
    cand2_eta <= cand2_eta_reg; -- is naturally delayed
    cand2_pt <= cand2_pt_reg; -- is naturally delayed
end Behavioral;



----------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- ClosestHitFinder, the entity that finds the hit closest to the candidate

entity ClosestHitFinder is
    Port (
        geta : in t_GETA;
        cand_eta : in t_INT_CANDETABIN;
        closest_geta : out t_INT_CANDETABIN;
        clock_h  : in std_logic
    );
end ClosestHitFinder;

architecture Behavioral of ClosestHitFinder is
    signal cand_eta_delayed : t_INT_CANDETABIN_DELAYED(1 downto 0);
    signal closest_geta_reg : t_INT_CANDETABIN;
    signal closest_geta_woffset_reg : t_INT_CANDETABIN;
    signal is_cand_valid : std_logic;
    signal geta_window : std_logic_vector(2*c_BITS_AROUND_CANDETA downto 0);
begin
    P_DELAY_CAND_ETA: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- delay cand_eta by one clock cycle
            -- latency: 1 clock cycle
            cand_eta_delayed <= cand_eta_delayed(cand_eta_delayed'high-1 downto 0) & cand_eta;
            end if;
    end process;

    P_CLOSEST_GETA_HIT_STEP1: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- first, we determine if the candidate is in the first or last c_BITS_AROUND_CANDETA bits
            -- latency: 1 clock cycle
            if (cand_eta > c_BITS_AROUND_CANDETA) and (cand_eta < c_MAXETA_FOR_CLOSESTHIT) then
                is_cand_valid <= '1';
            else
                is_cand_valid <= '0';
            end if;
            -- window of +/- c_BITS_AROUND_CANDETA around the eta candidate
            geta_window <= geta(cand_eta+c_BITS_AROUND_CANDETA downto cand_eta-c_BITS_AROUND_CANDETA);
        end if;
    end process;

    P_CLOSEST_GETA_HIT_STEP2: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- global eta of closest hit on a layer (12 bits)
            -- latency: 1 clock cycle
            -- TODO: treat boundary cases
            
            if is_cand_valid = '1' then
                closest_geta_reg <= find_closest_nonzero(geta_window);
            else
                -- TODO: consider slowing down the assignment to avoid timing issues? TODO: check if real issue!
                closest_geta_reg <= c_NO_HIT_CLOSE_TO_CANDIDATE;
            end if;
        end if;
    end process;

    P_APPLY_OFFSET: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- apply offset to closest geta
            -- latency: 1 clock cycle
            
            if (closest_geta_reg = c_NO_HIT_CLOSE_TO_CANDIDATE) then
                -- we return c_CANDIDATE_NOT_FOUND instead of c_NO_HIT_CLOSE_TO_CANDIDATE
                -- because here we apply the offset!
                closest_geta_woffset_reg <= c_CANDIDATE_NOT_FOUND;
            else
                closest_geta_woffset_reg <= closest_geta_reg + cand_eta_delayed(cand_eta_delayed'high) - c_BIT_FOR_CANDETA;
            end if;
        end if;
    end process;

    -- return the value after applying the offset
    closest_geta <= closest_geta_woffset_reg;
end Behavioral;


----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
library work;
use work.trigalgopkg.all;

-- CandidateComposer, the entity that extracts the information
-- needed for each candidate muon

entity CandidateComposer is
    Port (
        cand_eta : in t_INT_CANDETABIN;
        cand_pt : in t_INT_CANDPT;
        geta_1 : in t_GETA;
        geta_2 : in t_GETA;
        geta_3 : in t_GETA;
        geta_4 : in t_GETA;
        word : out t_CANDIDATE;
        clock_h  : in std_logic
    );
end CandidateComposer;

architecture Behavioral of CandidateComposer is
    signal cand_reg : t_CANDIDATE;
    signal cand_reg_delayed : t_CANDIDATE_DELAYED(3 downto 0);
    signal closest_geta_1 : t_INT_CANDETABIN;
    signal closest_geta_2 : t_INT_CANDETABIN;
    signal closest_geta_3 : t_INT_CANDETABIN;
    signal closest_geta_4 : t_INT_CANDETABIN;
    signal geta_1_delayed : t_GETA_DELAYED(0 downto 0); -- necessary to avoid timing issues
    signal geta_2_delayed : t_GETA_DELAYED(0 downto 0);
    signal geta_3_delayed : t_GETA_DELAYED(0 downto 0);
    signal geta_4_delayed : t_GETA_DELAYED(0 downto 0);
    signal cand_eta_copy1 : t_INT_CANDETABIN;
    signal cand_eta_copy2 : t_INT_CANDETABIN;
    signal cand_eta_copy3 : t_INT_CANDETABIN;
    signal cand_eta_copy4 : t_INT_CANDETABIN;

    component ClosestHitFinder is
        Port (
            geta : in t_GETA;
            cand_eta : in t_INT_CANDETABIN;
            closest_geta : out t_INT_CANDETABIN;
            clock_h  : in std_logic
        );
    end component;

begin
    P_CANDIDATE_ID: process(clock_h)
    begin
        if rising_edge(clock_h) then
            --- from https://edms.cern.ch/ui/file/2377909/1/backend.20211112.pdf
            --- first three bits: number of candidate
            --- fourth bit: 1 if candidate sent to MDTTP, 0 if not
            --- latency: 1 clock cycle

            if cand_eta = c_CANDIDATE_NOT_FOUND then
                cand_reg(2 downto 0) <= std_logic_vector(to_unsigned(0, 3));
                cand_reg(3) <= '0';
            else
                cand_reg(2 downto 0) <= std_logic_vector(to_unsigned(1, 3));
                cand_reg(3) <= '1';
            end if;
        end if;
    end process;

    P_GETA_INNERMOST_LAYER: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- then: eta (14 bits, in global eta of the innermost layer)
            -- latency: 1 clock cycle

            cand_reg(4+13 downto 4) <= std_logic_vector(to_unsigned(cand_eta, 14));
        end if;
    end process;

    P_GPHI_INNERMOST_LAYER: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- phi (9 bits, in global phi of the innermost layer)
            -- TODO: to be implemented, it's currently just the phi chamber index!
            -- latency: 1 clock cycle (currently)
            cand_reg(17+9 downto 18) <= std_logic_vector(to_unsigned(eta2chamber(cand_eta), 9)) when cand_eta /= c_CANDIDATE_NOT_FOUND else std_logic_vector(to_unsigned(c_CANDIDATE_NOT_FOUND, 9));
        end if;
    end process;

    P_RPC_PT: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- RPC pT estimate (8 bits)
            -- TODO: to be implemented
            -- latency: 1 clock cycle

            cand_reg(26+8 downto 27) <= std_logic_vector(to_unsigned(cand_pt, 8));
        end if;
    end process;

    P_RPC_THRESHOLD: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- RPC threshold (4 bits)
            -- latency: 1 clock cycle
            cand_reg(34+4 downto 35) <= std_logic_vector(to_unsigned(cand_pt, 4));
        end if;
    end process;

    P_CHARGE: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- charge (1 bit)
            -- TODO: implement
            -- latency: 1 clock cycle (currently)
            cand_reg(39) <= '0';
        end if;
    end process;

    P_PRIORITY: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- priority (3 bits)
            -- TODO: implement
            -- latency: 1 clock cycle (currently)
            cand_reg(39+3 downto 40) <= std_logic_vector(to_unsigned(0, 3));
        end if;
    end process;

    -- split the cand_eta signal into 4 copies, to ease matching timing constraints
    -- same for the geta signals
    -- latency: 1 clock cycle
    P_MATCH_TIMINGCONSTR: process(clock_h)
    begin
        if rising_edge(clock_h) then
            cand_eta_copy1 <= cand_eta;
            cand_eta_copy2 <= cand_eta;
            cand_eta_copy3 <= cand_eta;
            cand_eta_copy4 <= cand_eta;
            geta_1_delayed <= geta_1_delayed(geta_1_delayed'high-1 downto 0) & geta_1;
            geta_2_delayed <= geta_2_delayed(geta_2_delayed'high-1 downto 0) & geta_2;
            geta_3_delayed <= geta_3_delayed(geta_3_delayed'high-1 downto 0) & geta_3;
            geta_4_delayed <= geta_4_delayed(geta_4_delayed'high-1 downto 0) & geta_4;
        end if;
    end process;

    -- ClosestHitFinder: first layer
    -- latency: 3 clock cycles
    P_CLOSEST_GETA_HIT_LAY1 : ClosestHitFinder
        port map (
            geta => geta_1_delayed(geta_1_delayed'high),
            cand_eta => cand_eta_copy1,
            closest_geta => closest_geta_1,
            clock_h => clock_h
        );

    -- ClosestHitFinder: second layer
    -- latency: 3 clock cycles
    P_CLOSEST_GETA_HIT_LAY2 : ClosestHitFinder
        port map (
            geta => geta_2_delayed(geta_2_delayed'high),
            cand_eta => cand_eta_copy2,
            closest_geta => closest_geta_2,
            clock_h => clock_h
        );
    
    -- ClosestHitFinder: third layer
    -- latency: 3 clock cycles
    P_CLOSEST_GETA_HIT_LAY3 : ClosestHitFinder
        port map (
            geta => geta_3_delayed(geta_3_delayed'high),
            cand_eta => cand_eta_copy3,
            closest_geta => closest_geta_3,
            clock_h => clock_h
        );
    
    -- ClosestHitFinder: fourth layer
    -- latency: 3 clock cycles
    P_CLOSEST_GETA_HIT_LAY4 : ClosestHitFinder
        port map (
            geta => geta_4_delayed(geta_4_delayed'high),
            cand_eta => cand_eta_copy4,
            closest_geta => closest_geta_4,
            clock_h => clock_h
        );

    P_CLOSEST_GETA_HIT_ALL_LAYERS: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- closest hit (12 bits, in global eta)
            -- latency: 1 clock cycle

            cand_reg(42+12 downto 43) <= std_logic_vector(to_unsigned(closest_geta_1, 12));
            cand_reg(54+12 downto 55) <= std_logic_vector(to_unsigned(closest_geta_2, 12));
            cand_reg(66+12 downto 67) <= std_logic_vector(to_unsigned(closest_geta_3, 12));
            cand_reg(78+12 downto 79) <= std_logic_vector(to_unsigned(closest_geta_4, 12));
        end if;
    end process;

    P_RESERVEDBITS: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- reserved bits (37 bits)
            cand_reg(90+37 downto 91) <= std_logic_vector(to_unsigned(0, 37));
        end if;
    end process;

    -- delay the candidate to sync its parts
    P_DELAY_CAND : process(clock_h)
    begin
        if rising_edge(clock_h) then
            cand_reg_delayed <= cand_reg_delayed(cand_reg_delayed'high-1 downto 0) & cand_reg;
        end if;
    end process;

    -- sync the candidate fragments so that the full information arrives altogether
    -- latency: 1 clock cycle
    P_CANDIDATESYNC : process(clock_h)
    begin
        if rising_edge(clock_h) then  
            -- parts which were slower to determine
            word(78+12 downto 43) <= cand_reg(78+12 downto 43);
            -- parts which were faster to determine
            word(42 downto 0) <= cand_reg_delayed(cand_reg_delayed'high)(42 downto 0);
            word(90+37 downto 91) <= cand_reg_delayed(cand_reg_delayed'high)(90+37 downto 91);
        end if;
    end process;

end Behavioral;



----------------------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
-- use ieee.std_logic_textio.all;
use IEEE.NUMERIC_STD.ALL;
library work;
use work.trigalgopkg.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;

-- SLtrigger, the main entity
entity SLtrigger is
    Port ( file_eta_hits_BI_orig    :  in t_ETASTRIP_BI; 
           file_eta_hits_BM1_orig   :  in t_ETASTRIP_BM1; 
           file_eta_hits_BM2_orig   :  in t_ETASTRIP_BM2;
           file_eta_hits_BO_orig    :  in t_ETASTRIP_BO;
           file_phi_hits_BI_orig   :  in t_PHISTRIP_BI;
           file_phi_hits_BM1_orig    :  in t_PHISTRIP_BM1; 
           file_phi_hits_BM2_orig   :  in t_PHISTRIP_BM2; 
           file_phi_hits_BO_orig    :  in t_PHISTRIP_BO; 
           cand_1    :  out   t_CANDIDATE;
           cand_2    :  out   t_CANDIDATE;
           clock_h    : in std_logic);
end SLtrigger;

architecture Behavioral of SLtrigger is
    signal geta_BI : t_GETA;
    signal geta_BM1: t_GETA;
    signal geta_BM2 : t_GETA;
    signal geta_BO : t_GETA;
    signal gphi_BI : t_GPHI;
    signal gphi_BM1: t_GPHI;
    signal gphi_BM2 : t_GPHI;
    signal gphi_BO : t_GPHI;
    signal pat_eta_BI : t_ETAPAT;
    signal pat_eta_BM1: t_ETAPAT;
    signal pat_eta_BM2 : t_ETAPAT;
    signal pat_eta_BO : t_ETAPAT;
    signal pat_phi_BI : t_PHIPAT;
    signal pat_phi_BM1: t_PHIPAT;
    signal pat_phi_BM2 : t_PHIPAT;
    signal pat_phi_BO : t_PHIPAT;
    signal pat_eta : t_ETAPAT;
    signal pat_phi : t_PHIPAT;
    signal dec_phi : t_CHAMBDEC;
    signal dec : t_ETAPAT;
    signal priority : t_PRIORITY_PT;
    signal dec_prio : t_ETAPATPRIO;
    signal dec_declus : t_ETAPAT;
    signal cand1_eta : t_INT_CANDETABIN;
    signal cand1_pt : t_INT_CANDPT;
    signal cand2_eta : t_INT_CANDETABIN;
    signal cand2_pt : t_INT_CANDPT;
    signal cand1_reg : t_CANDIDATE;
    signal cand2_reg : t_CANDIDATE;


    -- delayed signals
    signal pat_eta_delayed : t_ETAPAT_DELAYED(0 downto 0); -- Ncycles-1 to 0; order matters!
    signal pat_eta_BI_delayed : t_ETAPAT_DELAYED(2 downto 0);
    signal pat_eta_BM1_delayed : t_ETAPAT_DELAYED(2 downto 0);
    signal pat_eta_BM2_delayed : t_ETAPAT_DELAYED(2 downto 0);
    signal pat_eta_BO_delayed : t_ETAPAT_DELAYED(2 downto 0);
    signal geta_BI_delayed : t_GETA_DELAYED(11 downto 0); -- to synch with cand*_eta
    signal geta_BM1_delayed : t_GETA_DELAYED(11 downto 0);
    signal geta_BM2_delayed : t_GETA_DELAYED(11 downto 0);
    signal geta_BO_delayed : t_GETA_DELAYED(11 downto 0);
    -- TODO: consider using the delays below, here, instead of having them in the components
    --signal cand1_eta_delayed : t_INT_CANDETABIN_DELAYED(0 downto 0);
    --signal cand1_pt_delayed : t_INT_CANDPT_DELAYED(0 downto 0);
    --signal cand2_eta_delayed : t_INT_CANDETABIN_DELAYED(0 downto 0);
    --signal cand2_pt_delayed : t_INT_CANDPT_DELAYED(0 downto 0);

    component PatFinder is 
    generic (
        LAYER : t_LAYER -- layer
    );
    Port (
        geta : in t_GETA;
        gphi : in t_GPHI;
        pat_eta : out t_ETAPAT;
        pat_phi : out t_PHIPAT;
        clock_h : in std_logic
    );
    end component;

    component Majority is
    Port (
        pat_eta_1 : in t_ETAPAT;
        pat_eta_2 : in t_ETAPAT;
        pat_eta_3 : in t_ETAPAT;
        pat_eta_4 : in t_ETAPAT;   
        pat_phi_1 : in t_PHIPAT;
        pat_phi_2 : in t_PHIPAT;
        pat_phi_3 : in t_PHIPAT;
        pat_phi_4 : in t_PHIPAT;
        pat_eta : out t_ETAPAT;
        pat_phi : out t_PHIPAT;
        clock_h : in std_logic
    );
    end component;

    component PhiChamberDecision is
    Port (
        pat_phi : in t_PHIPAT;
        dec_phi : out t_CHAMBDEC;
        clock_h : in std_logic
    );
    end component;

    component EtaPhiDecision is
        Port (
            pat_eta : in t_ETAPAT;
            dec_phi : in t_CHAMBDEC;
            dec     : out t_ETAPAT;
            clock_h     : in std_logic
        );
    end component;

    component PriorityGetter is
        Port (
            dec : in t_ETAPAT;
            lay1 : in t_ETAPAT;
            lay2 : in t_ETAPAT;
            lay3 : in t_ETAPAT;
            lay4 : in t_ETAPAT;
            dec_prio : out t_ETAPATPRIO;
            clock_h  : in std_logic
        );
    end component;

    component Declustering is
        Port (
            dec_prio : in t_ETAPATPRIO;
            dec_declus : out t_ETAPAT;
            clock_h  : in std_logic
        );
    end component;

    component CandidateSelector is
        Port (
            dec_declus : in t_ETAPAT;
            cand1_eta : out t_INT_CANDETABIN;
            cand1_pt : out t_INT_CANDPT;
            cand2_eta : out t_INT_CANDETABIN;
            cand2_pt : out t_INT_CANDPT;
            clock_h  : in std_logic
        );
    end component;

    component CandidateComposer is
        Port (
            cand_eta : in t_INT_CANDETABIN;
            cand_pt : in t_INT_CANDPT;
            geta_1 : in t_GETA;
            geta_2 : in t_GETA;
            geta_3 : in t_GETA;
            geta_4 : in t_GETA;
            word : out t_CANDIDATE;
            clock_h  : in std_logic
        );
    end component;
    
    
begin
    -- DelaySignals: delay signals by N clock cycles
    P_DELAYSIGNALS : process(clock_h)
    begin
        if rising_edge(clock_h) then
            pat_eta_delayed <= pat_eta_delayed(pat_eta_delayed'high-1 downto 0) & pat_eta;
            pat_eta_BI_delayed <= pat_eta_BI_delayed(pat_eta_BI_delayed'high-1 downto 0) & pat_eta_BI;
            pat_eta_BM1_delayed <= pat_eta_BM1_delayed(pat_eta_BM1_delayed'high-1 downto 0) & pat_eta_BM1;
            pat_eta_BM2_delayed <= pat_eta_BM2_delayed(pat_eta_BM2_delayed'high-1 downto 0) & pat_eta_BM2;
            pat_eta_BO_delayed <= pat_eta_BO_delayed(pat_eta_BO_delayed'high-1 downto 0) & pat_eta_BO;
            geta_BI_delayed <= geta_BI_delayed(geta_BI_delayed'high-1 downto 0) & geta_BI;
            geta_BM1_delayed <= geta_BM1_delayed(geta_BM1_delayed'high-1 downto 0) & geta_BM1;
            geta_BM2_delayed <= geta_BM2_delayed(geta_BM2_delayed'high-1 downto 0) & geta_BM2;
            geta_BO_delayed <= geta_BO_delayed(geta_BO_delayed'high-1 downto 0) & geta_BO;
            --cand1_eta_delayed <= cand1_eta_delayed(cand1_eta_delayed'high-1 downto 0) & cand1_eta;
            --cand1_pt_delayed <= cand1_pt_delayed(cand1_pt_delayed'high-1 downto 0) & cand1_pt;
            --cand2_eta_delayed <= cand2_eta_delayed(cand2_eta_delayed'high-1 downto 0) & cand2_eta;
            --cand2_pt_delayed <= cand2_pt_delayed(cand2_pt_delayed'high-1 downto 0) & cand2_pt;
        end if;
    end process;

    -- CoordMapping: map local strip number to global bin indexes
    -- latency: 1 clock cycle
    P_COORDMAPPING : process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- eta mapping
            geta_BI <= loc2glo_BI_eta(file_eta_hits_BI_orig);
            geta_BM1 <= loc2glo_BM1_eta(file_eta_hits_BM1_orig);
            geta_BM2 <= loc2glo_BM2_eta(file_eta_hits_BM2_orig);
            geta_BO <= loc2glo_BO_eta(file_eta_hits_BO_orig);

            -- phi mapping
            gphi_BI <= loc2glo_BI_phi(file_phi_hits_BI_orig);
            gphi_BM1 <= loc2glo_BM1_phi(file_phi_hits_BM1_orig);
            gphi_BM2 <= loc2glo_BM2_phi(file_phi_hits_BM2_orig);
            gphi_BO <= loc2glo_BO_phi(file_phi_hits_BO_orig);
        end if;
    end process;

    -- Patfinder: BI
    -- latency: 1 clock cycle
    P_PATFINDER_BI : PatFinder
        generic map (LAYER => 0)
        port map (
            geta => geta_BI,
            gphi => gphi_BI,
            pat_eta => pat_eta_BI,
            pat_phi => pat_phi_BI,
            clock_h => clock_h
        );

    -- Patfinder: BM1
    -- latency: 1 clock cycle
    P_PATFINDER_BM1 : PatFinder
        generic map (LAYER => 1)
        port map (
            geta => geta_BM1,
            gphi => gphi_BM1,
            pat_eta => pat_eta_BM1,
            pat_phi => pat_phi_BM1,
            clock_h => clock_h
        );

    -- Patfinder: BM2
    -- latency: 1 clock cycle
    P_PATFINDER_BM2 : PatFinder
        generic map (LAYER => 2)
        port map (
            geta => geta_BM2,
            gphi => gphi_BM2,
            pat_eta => pat_eta_BM2,
            pat_phi => pat_phi_BM2,
            clock_h => clock_h
        );

    -- Patfinder: BO
    -- latency: 1 clock cycle
    P_PATFINDER_BO : PatFinder
        generic map (LAYER => 3)
        port map (
            geta => geta_BO,
            gphi => gphi_BO,
            pat_eta => pat_eta_BO,
            pat_phi => pat_phi_BO,
            clock_h => clock_h
        );
        
    -- Majority
    -- latency: 1 clock cycle
    P_MAJORITY : Majority
        port map (
            pat_eta_1 => pat_eta_BI,
            pat_eta_2 => pat_eta_BM1,
            pat_eta_3 => pat_eta_BM2,
            pat_eta_4 => pat_eta_BO,
            pat_phi_1 => pat_phi_BI,
            pat_phi_2 => pat_phi_BM1,
            pat_phi_3 => pat_phi_BM2,
            pat_phi_4 => pat_phi_BO,
            pat_eta => pat_eta,
            pat_phi => pat_phi,
            clock_h => clock_h
        );

    -- ChamberDecision
    -- latency: 1 clock cycle
    P_CHAMBERDECISION : PhiChamberDecision
        port map (
            pat_phi => pat_phi,
            dec_phi => dec_phi,
            clock_h => clock_h
        );

    -- EtaPhiDecision
    -- latency: 1 clock cycle
    P_ETAPHIDECISION : EtaPhiDecision
        port map (
            pat_eta => pat_eta_delayed(pat_eta_delayed'high), -- compensate delay in phi chamber decision
            dec_phi => dec_phi,
            dec => dec,
            clock_h => clock_h
        );

    -- PriorityGetter
    -- latency: 2 clock cycles
    P_PRIORITYGETTER : PriorityGetter
        port map (
            dec => dec,
            lay1 => pat_eta_BI_delayed(pat_eta_BI_delayed'high),
            lay2 => pat_eta_BM1_delayed(pat_eta_BM1_delayed'high),
            lay3 => pat_eta_BM2_delayed(pat_eta_BM2_delayed'high),
            lay4 => pat_eta_BO_delayed(pat_eta_BO_delayed'high),
            dec_prio => dec_prio,
            clock_h => clock_h
        );

    -- Declustering
    -- latency: 2 clock cycles
    P_DECLUSTERING : Declustering
        port map (
            dec_prio => dec_prio,
            dec_declus => dec_declus,
            clock_h => clock_h
        );

    -- CandidateSelector
    -- latency: 3 clock cycles
    P_CANDIDATESELECTOR : CandidateSelector
        port map (
            dec_declus => dec_declus,
            cand1_eta => cand1_eta,
            cand1_pt => cand1_pt,
            cand2_eta => cand2_eta,
            cand2_pt => cand2_pt,
            clock_h => clock_h
        );

    -- candidate filling
    -- latency: 2 clock cycles
    -- TODO: check again timing when fully implemented
    P_CANDIDATECOMPOSER_1 : CandidateComposer
        port map (
            cand_eta => cand1_eta,
            cand_pt => cand1_pt,
            geta_1 => geta_BI_delayed(geta_BI_delayed'high),
            geta_2 => geta_BM1_delayed(geta_BM1_delayed'high),
            geta_3 => geta_BM2_delayed(geta_BM2_delayed'high),
            geta_4 => geta_BO_delayed(geta_BO_delayed'high),
            word => cand1_reg,
            clock_h => clock_h
        );
    P_CANDIDATECOMPOSER_2 : CandidateComposer
        port map (
            cand_eta => cand2_eta,
            cand_pt => cand2_pt,
            geta_1 => geta_BI_delayed(geta_BI_delayed'high),
            geta_2 => geta_BM1_delayed(geta_BM1_delayed'high),
            geta_3 => geta_BM2_delayed(geta_BM2_delayed'high),
            geta_4 => geta_BO_delayed(geta_BO_delayed'high),
            word => cand2_reg,
            clock_h => clock_h
        );

    -- assign output candidates
    cand_1 <= cand1_reg;
    cand_2 <= cand2_reg;
end Behavioral;

