library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;




package pack is

subtype integer_fixRange is integer range 0 to 511;
subtype integer_fixRange_map is integer range 0 to 800;
subtype integer_fixWindow is integer range 0 to 63;
subtype integer_fixRangePrio is integer range 0 to 6;
-- 2 values in case of asymmetric windows
type integer_array is array (1 downto 0) of integer_fixRange;
-- 21 towers
type integer_array2d is array (20 downto 0) of integer_array;
--5 pT values
type integer_array3d is array (3 downto 0) of integer_array2d;

type tower_candidate is array (0 to 7) of integer_fixRange;

type t_p_clos_vector is array (0 to 3) of integer_fixRange;
type t_vector_prio is array (0 to 4) of integer_fixRangePrio;



type mapphi is array (9 downto 0) of integer_fixRange;
type maptower is array (511 downto 0) of integer_fixRange;
type maptower_extended is array (531 downto 0) of integer_fixRange;


type map_eta_layer_BM1 is array (572 downto 0) of integer_fixRange_map;
type map_eta_layer_BM2 is array (596 downto 0) of integer_fixRange_map;
type map_eta_layer_BO is array (674 downto 0) of integer_fixRange_map;

-- map definitions -- S1A  --
--  BI  --

subtype t_inputs is std_logic_vector(511 downto 0);

function define_map_BI (signal file_eta_hits_input: in std_logic_vector(319 downto 0) ) return t_inputs;
--  BM1 --
function define_map_BM1 (signal file_eta_hits_input: in std_logic_vector(383 downto 0) ) return t_inputs;
--  BM2 --
function define_map_BM2 (signal file_eta_hits_input: in std_logic_vector(447 downto 0) ) return t_inputs;
--  BO  --
function define_map_BO (signal file_eta_hits_input: in std_logic_vector(383 downto 0) ) return t_inputs;
----------------------------


function trigger_decluster_7_global (in5,in4,in3,in2,in1,in0 : std_logic_vector(6 downto 0) ) return std_logic;
function select_candidate_w50 (signal list_candidates : in std_logic_vector(63 downto 0)) return integer_fixRange;



 
function trigger_decluster_7 (in0, in1, in2, in3, in4, in5, in6 : std_logic) return std_logic;
procedure priority_encoder(signal tower_cand_20: in tower_candidate; signal tower_cand_15: in tower_candidate;
                           signal tower_cand_10: in tower_candidate; signal tower_cand_5: in tower_candidate; 
                           signal o_tower_cand_20: out tower_candidate; signal o_tower_cand_15: out tower_candidate;
                           signal o_tower_cand_10: out tower_candidate; signal o_tower_cand_5: out tower_candidate; 
                           signal candidate: out integer_fixRange; signal pT_idx: out integer_fixRangePrio);
                                                      
procedure coin_type (constant RPC0: in std_logic;constant RPC1: in std_logic;constant RPC2: in std_logic;constant RPC3: in std_logic; signal prior : out integer_fixRange);


procedure closest_hit (constant window : in integer;
                       constant pivot_index : in integer;
                       signal list_hit : in std_logic_vector;
                       signal clos_index : out integer_fixRange);
                                     
function closest_hit_fix_range ( list_hit : in std_logic_vector(64 downto 0)) return integer_fixRange;
function closest_hit_fix_range_half ( list_hit : in std_logic_vector(64 downto 0)) return integer_fixRange;

                       

procedure fill_trig_candidate (constant potential_candidates: in std_logic_vector; signal candidates : inout std_logic_vector; signal candidate_num : inout integer);


constant map_phi_BM1 : mapphi := (0,0,1,2,3,4,5,5,5,5);
constant map_phi_BM2 : mapphi := (0,0,1,2,3,4,5,6,6,6);
constant map_phi_BO  : mapphi := (0,0,1,2,3,4,5,5,5,5);

--procedure PARITY
--   (signal X : in std_logic_vector;
--    signal Y : out std_logic);


constant trigger_map_BM1 : map_eta_layer_BM1 := (43 ,42 ,41 ,41 ,40 ,39 ,38 ,37 ,36 ,36 ,35 ,34 ,33 ,32 ,31 ,31 ,30 ,29 ,28 ,27 ,26 ,26 ,25 ,24 ,23 ,22 ,21 ,20 ,19 ,19 ,18 ,17 ,16 ,15 ,14 ,13 ,13 ,12 ,11 ,10 ,9 ,8 ,8 ,7 ,6 ,5 ,4 ,3 ,77 ,76 ,75 ,74 ,74 ,73 ,72 ,71 ,70 ,70 ,69 ,68 ,67 ,66 ,66 ,65 ,64 ,63 ,62 ,61 ,61 ,60 ,59 ,58 ,57 ,57 ,56 ,55 ,54 ,53 ,53 ,52 ,51 ,50 ,49 ,48 ,48 ,47 ,46 ,45 ,111 ,110 ,109 ,108 ,107 ,107 ,106 ,105 ,104 ,103 ,102 ,102 ,101 ,100 ,99 ,98 ,98 ,97 ,96 ,95 ,94 ,94 ,93 ,92 ,91 ,90 ,89 ,89 ,88 ,87 ,86 ,85 ,85 ,84 ,83 ,82 ,81 ,81 ,80 ,79 ,158 ,157 ,156 ,155 ,155 ,154 ,153 ,153 ,152 ,151 ,150 ,150 ,149 ,148 ,147 ,147 ,146 ,145 ,144 ,144 ,143 ,142 ,142 ,140 ,139 ,139 ,138 ,137 ,136 ,135 ,135 ,134 ,133 ,132 ,132 ,130 ,129 ,129 ,128 ,127 ,126 ,126 ,125 ,124 ,124 ,123 ,122 ,121 ,121 ,120 ,119 ,118 ,118 ,117 ,116 ,115 ,115 ,114 ,113 ,112 ,112 ,204 ,203 ,202 ,202 ,201 ,200 ,200 ,199 ,198 ,197 ,196 ,195 ,194 ,194 ,193 ,192 ,191 ,190 ,188 ,187 ,186 ,186 ,185 ,184 ,183 ,183 ,182 ,182 ,181 ,180 ,179 ,178 ,177 ,177 ,176 ,175 ,174 ,174 ,173 ,172 ,172 ,170 ,169 ,169 ,168 ,167 ,166 ,166 ,165 ,163 ,163 ,162 ,161 ,160 ,159 ,229 ,229 ,228 ,227 ,226 ,226 ,220 ,218 ,218 ,217 ,216 ,215 ,215 ,214 ,213 ,212 ,211 ,210 ,209 ,209 ,208 ,207 ,207 ,263 ,264 ,264 ,265 ,266 ,266 ,267 ,268 ,269 ,269 ,270 ,272 ,272 ,273 ,274 ,275 ,275 ,276 ,277 ,277 ,280 ,280 ,281 ,282 ,283 ,284 ,284 ,285 ,285 ,286 ,287 ,288 ,288 ,289 ,290 ,291 ,292 ,293 ,293 ,294 ,295 ,296 ,296 ,297 ,298 ,299 ,299 ,300 ,301 ,302 ,302 ,303 ,304 ,305 ,305 ,306 ,307 ,308 ,309 ,310 ,311 ,311 ,312 ,314 ,315 ,316 ,317 ,318 ,319 ,319 ,321 ,322 ,322 ,323 ,324 ,324 ,325 ,326 ,327 ,327 ,328 ,329 ,330 ,330 ,331 ,331 ,332 ,333 ,333 ,334 ,335 ,336 ,336 ,337 ,338 ,339 ,339 ,340 ,341 ,343 ,344 ,344 ,345 ,346 ,347 ,347 ,348 ,349 ,350 ,350 ,351 ,352 ,353 ,353 ,354 ,355 ,356 ,357 ,358 ,358 ,359 ,360 ,360 ,361 ,362 ,363 ,363 ,364 ,365 ,366 ,366 ,367 ,368 ,369 ,369 ,371 ,371 ,372 ,373 ,374 ,374 ,375 ,376 ,377 ,377 ,378 ,378 ,379 ,380 ,381 ,381 ,382 ,383 ,384 ,384 ,385 ,386 ,387 ,387 ,388 ,389 ,389 ,390 ,391 ,392 ,392 ,393 ,394 ,395 ,395 ,396 ,397 ,398 ,398 ,399 ,400 ,401 ,401 ,402 ,403 ,404 ,405 ,406 ,406 ,407 ,408 ,409 ,410 ,411 ,411 ,412 ,413 ,414 ,415 ,415 ,416 ,417 ,418 ,419 ,419 ,420 ,421 ,422 ,423 ,424 ,424 ,425 ,426 ,427 ,428 ,428 ,429 ,430 ,431 ,432 ,432 ,433 ,434 ,436 ,437 ,438 ,439 ,439 ,440 ,441 ,442 ,443 ,443 ,444 ,445 ,446 ,447 ,447 ,448 ,449 ,450 ,451 ,452 ,452 ,453 ,454 ,455 ,456 ,456 ,457 ,458 ,459 ,460 ,460 ,461 ,462 ,463 ,464 ,465 ,465 ,466 ,467 ,468 ,470 ,471 ,472 ,472 ,473 ,474 ,475 ,476 ,477 ,477 ,478 ,479 ,480 ,481 ,482 ,482 ,483 ,484 ,485 ,486 ,487 ,487 ,488 ,489 ,490 ,491 ,492 ,493 ,494 ,494 ,495 ,496 ,497 ,498 ,499 ,500 ,500 ,501 ,502 ,503 ,504 ,505 ,505 ,506 ,507 ,508 ,509 ,510);



constant trigger_map_BM2 : map_eta_layer_BM2 := (13 ,12 ,11 ,10 ,10 ,9 ,8 ,7 ,7 ,6 ,5 ,4 ,4 ,3 ,2 ,1 ,52 ,51 ,50 ,50 ,49 ,48 ,47 ,46 ,46 ,45 ,44 ,43 ,42 ,42 ,41 ,40 ,39 ,38 ,38 ,37 ,36 ,35 ,34 ,33 ,32 ,32 ,31 ,30 ,29 ,28 ,28 ,27 ,26 ,25 ,24 ,24 ,23 ,22 ,21 ,20 ,20 ,19 ,18 ,17 ,16 ,16 ,15 ,14 ,83 ,83 ,82 ,81 ,80 ,79 ,79 ,78 ,77 ,76 ,76 ,75 ,74 ,73 ,72 ,72 ,71 ,70 ,69 ,68 ,68 ,67 ,66 ,65 ,65 ,64 ,63 ,62 ,61 ,61 ,60 ,59 ,58 ,58 ,57 ,56 ,55 ,54 ,54 ,53 ,116 ,115 ,114 ,114 ,113 ,112 ,111 ,110 ,110 ,109 ,108 ,107 ,106 ,106 ,105 ,104 ,103 ,103 ,102 ,101 ,100 ,99 ,99 ,98 ,97 ,96 ,96 ,95 ,94 ,93 ,92 ,92 ,91 ,90 ,89 ,88 ,88 ,87 ,86 ,85 ,162 ,161 ,160 ,160 ,159 ,158 ,158 ,157 ,156 ,155 ,155 ,154 ,153 ,151 ,151 ,150 ,149 ,148 ,146 ,145 ,144 ,143 ,143 ,142 ,141 ,141 ,140 ,140 ,139 ,138 ,138 ,137 ,136 ,135 ,135 ,134 ,133 ,133 ,132 ,131 ,130 ,130 ,129 ,128 ,128 ,127 ,126 ,126 ,125 ,124 ,123 ,123 ,122 ,121 ,120 ,119 ,118 ,207 ,206 ,205 ,205 ,204 ,203 ,203 ,202 ,201 ,200 ,200 ,199 ,198 ,198 ,196 ,196 ,195 ,193 ,193 ,192 ,191 ,191 ,190 ,189 ,189 ,188 ,187 ,186 ,186 ,185 ,185 ,184 ,184 ,183 ,182 ,181 ,181 ,180 ,179 ,179 ,178 ,177 ,176 ,176 ,175 ,174 ,173 ,172 ,171 ,170 ,169 ,169 ,168 ,167 ,167 ,166 ,165 ,164 ,164 ,163 ,230 ,230 ,228 ,227 ,227 ,226 ,225 ,225 ,224 ,223 ,222 ,220 ,220 ,219 ,218 ,218 ,216 ,215 ,215 ,214 ,213 ,213 ,212 ,211 ,210 ,210 ,209 ,208 ,262 ,263 ,265 ,266 ,267 ,268 ,270 ,271 ,272 ,273 ,274 ,274 ,275 ,276 ,277 ,277 ,278 ,279 ,279 ,280 ,281 ,283 ,285 ,286 ,286 ,287 ,289 ,290 ,290 ,291 ,292 ,293 ,293 ,294 ,295 ,295 ,296 ,297 ,298 ,298 ,299 ,300 ,301 ,302 ,303 ,303 ,304 ,305 ,306 ,307 ,308 ,308 ,309 ,310 ,310 ,311 ,312 ,313 ,313 ,315 ,316 ,317 ,317 ,318 ,319 ,320 ,320 ,321 ,322 ,322 ,324 ,324 ,325 ,326 ,327 ,328 ,328 ,329 ,329 ,330 ,331 ,332 ,332 ,333 ,334 ,334 ,335 ,336 ,337 ,338 ,339 ,340 ,341 ,342 ,344 ,344 ,345 ,346 ,346 ,347 ,348 ,349 ,349 ,350 ,351 ,352 ,353 ,353 ,354 ,355 ,355 ,357 ,358 ,358 ,359 ,360 ,360 ,361 ,362 ,362 ,364 ,365 ,365 ,366 ,367 ,367 ,368 ,369 ,370 ,370 ,371 ,372 ,372 ,373 ,373 ,374 ,375 ,375 ,376 ,377 ,378 ,378 ,380 ,380 ,381 ,382 ,383 ,383 ,384 ,385 ,385 ,386 ,387 ,387 ,388 ,389 ,390 ,390 ,391 ,392 ,392 ,393 ,394 ,395 ,395 ,397 ,398 ,399 ,399 ,400 ,401 ,402 ,403 ,403 ,404 ,405 ,406 ,407 ,407 ,408 ,409 ,410 ,410 ,411 ,412 ,413 ,414 ,414 ,415 ,416 ,417 ,417 ,418 ,419 ,420 ,421 ,421 ,422 ,423 ,424 ,425 ,425 ,426 ,427 ,428 ,430 ,430 ,431 ,432 ,433 ,434 ,434 ,435 ,436 ,437 ,437 ,438 ,439 ,440 ,441 ,441 ,442 ,443 ,444 ,445 ,445 ,446 ,447 ,448 ,448 ,449 ,450 ,451 ,452 ,452 ,453 ,454 ,455 ,455 ,456 ,457 ,458 ,459 ,459 ,460 ,461 ,462 ,463 ,463 ,464 ,465 ,466 ,467 ,467 ,468 ,469 ,470 ,471 ,471 ,472 ,473 ,474 ,475 ,475 ,476 ,477 ,478 ,479 ,480 ,481 ,481 ,482 ,483 ,484 ,485 ,485 ,486 ,487 ,488 ,489 ,489 ,490 ,491 ,492 ,493 ,493 ,494 ,495 ,496 ,497 ,497 ,498 ,499 ,500 ,501 ,502 ,503 ,503 ,504 ,505 ,506 ,506 ,507 ,508 ,509 ,509 ,510 ,511);


constant trigger_map_BO : map_eta_layer_BO  := (36 ,35 ,34 ,34 ,33 ,32 ,31 ,31 ,30 ,29 ,28 ,28 ,27 ,26 ,26 ,25 ,24 ,23 ,23 ,22 ,21 ,20 ,20 ,19 ,18 ,17 ,16 ,16 ,15 ,14 ,14 ,13 ,12 ,11 ,11 ,10 ,9 ,8 ,8 ,7 ,6 ,5 ,5 ,4 ,3 ,2 ,2 ,1 ,81 ,81 ,80 ,79 ,78 ,78 ,77 ,76 ,76 ,75 ,74 ,73 ,73 ,72 ,71 ,71 ,70 ,69 ,68 ,68 ,67 ,66 ,66 ,65 ,64 ,64 ,63 ,62 ,61 ,61 ,60 ,59 ,59 ,58 ,57 ,56 ,56 ,55 ,54 ,54 ,53 ,52 ,51 ,51 ,50 ,49 ,49 ,48 ,47 ,46 ,46 ,45 ,44 ,44 ,43 ,42 ,41 ,41 ,40 ,39 ,39 ,38 ,37 ,37 ,127 ,126 ,125 ,124 ,124 ,123 ,122 ,122 ,121 ,120 ,120 ,119 ,118 ,117 ,117 ,116 ,115 ,115 ,114 ,113 ,112 ,112 ,111 ,110 ,110 ,109 ,108 ,107 ,107 ,106 ,105 ,105 ,104 ,103 ,103 ,102 ,101 ,101 ,100 ,99 ,98 ,98 ,97 ,96 ,96 ,95 ,94 ,93 ,93 ,92 ,91 ,91 ,90 ,89 ,88 ,88 ,87 ,86 ,86 ,85 ,84 ,84 ,83 ,82 ,162 ,161 ,161 ,160 ,159 ,158 ,158 ,157 ,156 ,155 ,155 ,154 ,153 ,152 ,152 ,151 ,150 ,149 ,149 ,148 ,147 ,147 ,146 ,145 ,145 ,144 ,143 ,142 ,142 ,141 ,140 ,139 ,139 ,138 ,137 ,137 ,136 ,135 ,134 ,134 ,133 ,132 ,131 ,131 ,130 ,129 ,128 ,128 ,207 ,207 ,206 ,205 ,205 ,204 ,203 ,202 ,202 ,201 ,200 ,200 ,199 ,198 ,197 ,197 ,196 ,195 ,195 ,194 ,193 ,193 ,192 ,191 ,190 ,190 ,189 ,188 ,188 ,187 ,185 ,185 ,184 ,183 ,182 ,182 ,181 ,180 ,180 ,179 ,178 ,177 ,177 ,176 ,175 ,175 ,174 ,173 ,172 ,172 ,171 ,170 ,170 ,169 ,168 ,167 ,167 ,166 ,165 ,165 ,164 ,163 ,238 ,237 ,237 ,236 ,235 ,235 ,234 ,233 ,233 ,232 ,231 ,231 ,230 ,230 ,229 ,227 ,226 ,226 ,225 ,225 ,224 ,223 ,222 ,221 ,221 ,220 ,219 ,219 ,218 ,218 ,217 ,216 ,216 ,215 ,214 ,214 ,213 ,213 ,212 ,211 ,211 ,210 ,209 ,260 ,262 ,262 ,263 ,264 ,264 ,265 ,267 ,267 ,268 ,269 ,269 ,270 ,271 ,272 ,272 ,273 ,274 ,274 ,275 ,276 ,276 ,277 ,278 ,279 ,279 ,280 ,281 ,281 ,282 ,283 ,284 ,284 ,285 ,286 ,286 ,287 ,288 ,289 ,289 ,290 ,291 ,291 ,292 ,293 ,294 ,294 ,295 ,296 ,296 ,297 ,298 ,299 ,299 ,301 ,301 ,302 ,303 ,304 ,304 ,306 ,306 ,307 ,308 ,308 ,309 ,310 ,311 ,311 ,312 ,313 ,313 ,314 ,315 ,316 ,316 ,317 ,318 ,318 ,319 ,320 ,320 ,321 ,322 ,323 ,323 ,324 ,325 ,325 ,326 ,327 ,328 ,328 ,328 ,329 ,330 ,331 ,331 ,332 ,333 ,333 ,334 ,335 ,336 ,336 ,337 ,338 ,338 ,339 ,340 ,341 ,341 ,342 ,343 ,343 ,344 ,345 ,346 ,346 ,347 ,348 ,348 ,349 ,350 ,351 ,352 ,352 ,353 ,354 ,355 ,355 ,356 ,357 ,358 ,358 ,359 ,361 ,361 ,362 ,363 ,364 ,364 ,365 ,366 ,366 ,367 ,368 ,368 ,369 ,370 ,371 ,371 ,372 ,373 ,374 ,374 ,375 ,376 ,376 ,377 ,378 ,379 ,379 ,380 ,381 ,382 ,382 ,383 ,384 ,385 ,385 ,386 ,387 ,388 ,389 ,389 ,390 ,391 ,391 ,392 ,393 ,393 ,394 ,395 ,396 ,396 ,397 ,398 ,398 ,399 ,400 ,401 ,401 ,402 ,403 ,403 ,404 ,405 ,406 ,406 ,407 ,408 ,408 ,409 ,410 ,410 ,411 ,412 ,412 ,413 ,414 ,415 ,415 ,416 ,417 ,417 ,418 ,419 ,420 ,420 ,421 ,422 ,422 ,423 ,424 ,425 ,425 ,426 ,427 ,427 ,428 ,429 ,429 ,430 ,431 ,432 ,432 ,433 ,434 ,435 ,435 ,436 ,437 ,437 ,438 ,439 ,440 ,440 ,441 ,442 ,442 ,443 ,444 ,445 ,445 ,446 ,447 ,447 ,448 ,449 ,449 ,450 ,451 ,452 ,452 ,453 ,454 ,454 ,455 ,456 ,457 ,457 ,458 ,459 ,459 ,460 ,461 ,462 ,462 ,463 ,464 ,464 ,465 ,466 ,467 ,467 ,468 ,469 ,469 ,470 ,471 ,472 ,472 ,473 ,474 ,474 ,475 ,476 ,476 ,477 ,478 ,479 ,479 ,480 ,481 ,482 ,482 ,483 ,484 ,485 ,485 ,486 ,487 ,487 ,488 ,489 ,490 ,490 ,491 ,492 ,493 ,493 ,494 ,495 ,496 ,497 ,497 ,498 ,499 ,499 ,500 ,501 ,502 ,502 ,503 ,504 ,505 ,505 ,506 ,507 ,508 ,508 ,509 ,510 ,511 ,511);


constant trigger_map : maptower := (
20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

constant trigger_map_phi : maptower_extended := (
0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,3 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,5 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,6 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7 ,7);                      


--type integer_array is array (natural range<>) of integer;
--type integer_array2d is array (natural range<>) of integer_array;

constant trigger_patterns_BI : integer_array2d := (
(2,2),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(2,2),
(2,2),
(2,2),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(2,2)
);



constant trigger_patterns_phi_9l : integer_array2d := (
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);
constant trigger_patterns_phi_8l : integer_array2d := (
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);

constant trigger_patterns_phi_7l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);
constant trigger_patterns_phi_6l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);
constant trigger_patterns_phi_5l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);
constant trigger_patterns_phi_4l : integer_array2d := (
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1),
(1,1)
);


constant trigger_patterns_phi_3l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5)
);

constant trigger_patterns_phi_2l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5)
);
constant trigger_patterns_phi_1l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5)
);


constant trigger_patterns_1l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
);
constant trigger_patterns_2l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
);
constant trigger_patterns_3l : integer_array2d := (
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
);
constant trigger_patterns_4l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);
constant trigger_patterns_5l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
);
constant trigger_patterns_6l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
);
constant trigger_patterns_7l : integer_array2d := (
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
);
constant trigger_patterns_8l : integer_array2d := (
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
);


constant trigger_patterns_1l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),
--(
--(23,23),
--(22,22),
--(19,19),
--(18,18),
--(16,16),
--(15,15),
--(14,14),
--(13,13),
--(12,12),
--(13,13),
--(13,13),
--(12,12),
--(12,12),
--(13,13),
--(14,14),
--(15,15),
--(16,16),
--(18,18),
--(19,19),
--(22,22),
--(22,22)
-- ),
-- pT 10
(
(12,12),
(11,11),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(7,7),
(7,7),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12)
 ),
-- pT 15
(
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(5,5),
(5,5),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8)
 ),
-- pT 20
(
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
 )
);
constant trigger_patterns_2l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),

--(
--(23,23),
--(22,22),
--(19,19),
--(18,18),
--(16,16),
--(15,15),
--(14,14),
--(13,13),
--(12,12),
--(13,13),
--(13,13),
--(12,12),
--(12,12),
--(13,13),
--(14,14),
--(15,15),
--(16,16),
--(18,18),
--(19,19),
--(22,22),
--(22,22)
-- ),
-- pT 10
(
(12,12),
(11,11),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(7,7),
(7,7),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12)
 ),
-- pT 15
(
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(5,5),
(5,5),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8)
 ),
-- pT 20
(
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
 )
);
constant trigger_patterns_3l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),

--(
--(23,23),
--(22,22),
--(19,19),
--(18,18),
--(16,16),
--(15,15),
--(14,14),
--(13,13),
--(12,12),
--(13,13),
--(13,13),
--(12,12),
--(12,12),
--(13,13),
--(14,14),
--(15,15),
--(16,16),
--(18,18),
--(19,19),
--(22,22),
--(22,22)
-- ),
-- pT 10
(
(12,12),
(11,11),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(7,7),
(7,7),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(11,11),
(12,12)
 ),
-- pT 15
(
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(5,5),
(5,5),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(7,7),
(8,8)
 ),
-- pT 20
(
(6,6),
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(6,6),
(6,6)
 )
);
constant trigger_patterns_4l_3d : integer_array3d := ( 
-- pT 5
(
(10,10),
(10,10),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(7,7),
(7,7),
(8,8),
(10,10),
(9,9)
 ),
-- pT 10
(
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5)
 ),
-- pT 15
(
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
 )
);
constant trigger_patterns_5l_3d : integer_array3d := (
-- pT 5
(
(10,10),
(10,10),
(8,8),
(8,8),
(7,7),
(7,7),
(6,6),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(7,7),
(7,7),
(8,8),
(10,10),
(9,9)
 ),
-- pT 10
(
(5,5),
(5,5),
(5,5),
(4,4),
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4),
(4,4),
(5,5),
(5,5),
(5,5)
 ),
-- pT 15
(
(4,4),
(4,4),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(3,3),
(4,4),
(4,4)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3)
 )
);
constant trigger_patterns_6l_3d : integer_array3d := ( 
-- pT 5
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 10
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 15
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
 )
);
constant trigger_patterns_7l_3d : integer_array3d := (
-- pT 5
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 10
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 15
(
(0,0),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(3,3),
(3,3),
(0,0)
 ),
-- pT 20
(
(3,3),
(3,3),
(3,3),
(3,3),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(2,2),
(3,3),
(2,2),
(3,3),
(3,3)
 )
);




constant trigger_patterns_8l_3d : integer_array3d := (
-- pT 5
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
),
--(
--(33,33),
--(33,33),
--(30,30),
--(25,25),
--(23,23),
--(21,21),
--(19,19),
--(17,17),
--(17,17),
--(16,16),
--(16,16),
--(16,16),
--(17,17),
--(17,17),
--(19,19),
--(21,21),
--(23,23),
--(25,25),
--(29,29),
--(33,33),
--(32,32)
-- ),
-- pT 10
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
 ),
-- pT 15
(
(10,10),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(10,10)
 ),
-- pT 20
(
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
 )
);




signal trigger_patterns_9l : integer_array2d := (
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
);



constant trigger_patterns_9l_3d : integer_array3d :=  (
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
--(33,33),
--(33,33),
--(30,30),
--(25,25),
--(23,23),
--(21,21),
--(19,19),
--(17,17),
--(17,17),
--(16,16),
--(16,16),
--(16,16),
--(17,17),
--(17,17),
--(19,19),
--(21,21),
--(23,23),
--(25,25),
--(29,29),
--(33,33),
--(32,32)
 ),
-- pT 10
(
(16,16),
(16,16),
(14,14),
(12,12),
(12,12),
(10,10),
(9,9),
(9,9),
(8,8),
(8,8),
(8,8),
(8,8),
(8,8),
(9,9),
(9,9),
(10,10),
(12,12),
(12,12),
(14,14),
(16,16),
(16,16)
 ),
-- pT 15
(
(10,10),
(10,10),
(9,9),
(8,8),
(8,8),
(7,7),
(6,6),
(6,6),
(5,5),
(5,5),
(5,5),
(5,5),
(5,5),
(6,6),
(6,6),
(7,7),
(8,8),
(8,8),
(9,9),
(10,10),
(10,10)
 ),
-- pT 20
(
(7,7),
(7,7),
(7,7),
(6,6),
(6,6),
(5,5),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(4,4),
(5,5),
(6,6),
(6,6),
(6,6),
(7,7),
(8,8)
 ) -- close for pT loop
);





end pack;


package body pack is

-- function definitions 
function define_map_BI (signal file_eta_hits_input : in std_logic_vector(319 downto 0) ) 
    return t_inputs  is
    variable file_eta_hits : t_inputs;
    begin 

	-- the simulations do not support BI -- so this is a dummy map that needs to be changed once simulations will be ready
	file_eta_hits(0) := file_eta_hits_input(0);
	file_eta_hits(1) := file_eta_hits_input(1);
	file_eta_hits(2) := file_eta_hits_input(2);
	file_eta_hits(3) := file_eta_hits_input(3);
	file_eta_hits(4) := file_eta_hits_input(4);
	file_eta_hits(5) := file_eta_hits_input(5);
	file_eta_hits(6) := file_eta_hits_input(6);
	file_eta_hits(7) := file_eta_hits_input(7);
	file_eta_hits(8) := file_eta_hits_input(8);
	file_eta_hits(9) := file_eta_hits_input(9);
	file_eta_hits(10) := file_eta_hits_input(10);
	file_eta_hits(11) := file_eta_hits_input(11);
	file_eta_hits(12) := file_eta_hits_input(12);
	file_eta_hits(13) := file_eta_hits_input(13);
	file_eta_hits(14) := file_eta_hits_input(14);
	file_eta_hits(15) := file_eta_hits_input(15);
	file_eta_hits(16) := file_eta_hits_input(16);
	file_eta_hits(17) := file_eta_hits_input(17);
	file_eta_hits(18) := file_eta_hits_input(18);
	file_eta_hits(19) := file_eta_hits_input(19);
	file_eta_hits(20) := file_eta_hits_input(20);
	file_eta_hits(21) := file_eta_hits_input(21);
	file_eta_hits(22) := file_eta_hits_input(22);
	file_eta_hits(23) := file_eta_hits_input(23);
	file_eta_hits(24) := file_eta_hits_input(24);
	file_eta_hits(25) := file_eta_hits_input(25);
	file_eta_hits(26) := file_eta_hits_input(26);
	file_eta_hits(27) := file_eta_hits_input(27);
	file_eta_hits(28) := file_eta_hits_input(28);
	file_eta_hits(29) := file_eta_hits_input(29);
	file_eta_hits(30) := file_eta_hits_input(30);
	file_eta_hits(31) := file_eta_hits_input(31);
	file_eta_hits(32) := file_eta_hits_input(32);
	file_eta_hits(33) := file_eta_hits_input(33);
	file_eta_hits(34) := file_eta_hits_input(34);
	file_eta_hits(35) := file_eta_hits_input(35);
	file_eta_hits(36) := file_eta_hits_input(36);
	file_eta_hits(37) := file_eta_hits_input(37);
	file_eta_hits(38) := file_eta_hits_input(38);
	file_eta_hits(39) := file_eta_hits_input(39);
	file_eta_hits(40) := file_eta_hits_input(40);
	file_eta_hits(41) := file_eta_hits_input(41);
	file_eta_hits(42) := file_eta_hits_input(42);
	file_eta_hits(43) := file_eta_hits_input(43);
	file_eta_hits(44) := file_eta_hits_input(44);
	file_eta_hits(45) := file_eta_hits_input(45);
	file_eta_hits(46) := file_eta_hits_input(46);
	file_eta_hits(47) := file_eta_hits_input(47);
	file_eta_hits(48) := file_eta_hits_input(48);
	file_eta_hits(49) := file_eta_hits_input(49);
	file_eta_hits(50) := file_eta_hits_input(50);
	file_eta_hits(51) := file_eta_hits_input(51);
	file_eta_hits(52) := file_eta_hits_input(52);
	file_eta_hits(53) := file_eta_hits_input(53);
	file_eta_hits(54) := file_eta_hits_input(54);
	file_eta_hits(55) := file_eta_hits_input(55);
	file_eta_hits(56) := file_eta_hits_input(56);
	file_eta_hits(57) := file_eta_hits_input(57);
	file_eta_hits(58) := file_eta_hits_input(58);
	file_eta_hits(59) := file_eta_hits_input(59);
	file_eta_hits(60) := file_eta_hits_input(60);
	file_eta_hits(61) := file_eta_hits_input(61);
	file_eta_hits(62) := file_eta_hits_input(62);
	file_eta_hits(63) := file_eta_hits_input(63);
	file_eta_hits(64) := file_eta_hits_input(64);
	file_eta_hits(65) := file_eta_hits_input(65);
	file_eta_hits(66) := file_eta_hits_input(66);
	file_eta_hits(67) := file_eta_hits_input(67);
	file_eta_hits(68) := file_eta_hits_input(68);
	file_eta_hits(69) := file_eta_hits_input(69);
	file_eta_hits(70) := file_eta_hits_input(70);
	file_eta_hits(71) := file_eta_hits_input(71);
	file_eta_hits(72) := file_eta_hits_input(72);
	file_eta_hits(73) := file_eta_hits_input(73);
	file_eta_hits(74) := file_eta_hits_input(74);
	file_eta_hits(75) := file_eta_hits_input(75);
	file_eta_hits(76) := file_eta_hits_input(76);
	file_eta_hits(77) := file_eta_hits_input(77);
	file_eta_hits(78) := file_eta_hits_input(78);
	file_eta_hits(79) := file_eta_hits_input(79);
	file_eta_hits(80) := file_eta_hits_input(80);
	file_eta_hits(81) := file_eta_hits_input(81);
	file_eta_hits(82) := file_eta_hits_input(82);
	file_eta_hits(83) := file_eta_hits_input(83);
	file_eta_hits(84) := file_eta_hits_input(84);
	file_eta_hits(85) := file_eta_hits_input(85);
	file_eta_hits(86) := file_eta_hits_input(86);
	file_eta_hits(87) := file_eta_hits_input(87);
	file_eta_hits(88) := file_eta_hits_input(88);
	file_eta_hits(89) := file_eta_hits_input(89);
	file_eta_hits(90) := file_eta_hits_input(90);
	file_eta_hits(91) := file_eta_hits_input(91);
	file_eta_hits(92) := file_eta_hits_input(92);
	file_eta_hits(93) := file_eta_hits_input(93);
	file_eta_hits(94) := file_eta_hits_input(94);
	file_eta_hits(95) := file_eta_hits_input(95);
	file_eta_hits(96) := file_eta_hits_input(96);
	file_eta_hits(97) := file_eta_hits_input(97);
	file_eta_hits(98) := file_eta_hits_input(98);
	file_eta_hits(99) := file_eta_hits_input(99);
	file_eta_hits(100) := file_eta_hits_input(100);
	file_eta_hits(101) := file_eta_hits_input(101);
	file_eta_hits(102) := file_eta_hits_input(102);
	file_eta_hits(103) := file_eta_hits_input(103);
	file_eta_hits(104) := file_eta_hits_input(104);
	file_eta_hits(105) := file_eta_hits_input(105);
	file_eta_hits(106) := file_eta_hits_input(106);
	file_eta_hits(107) := file_eta_hits_input(107);
	file_eta_hits(108) := file_eta_hits_input(108);
	file_eta_hits(109) := file_eta_hits_input(109);
	file_eta_hits(110) := file_eta_hits_input(110);
	file_eta_hits(111) := file_eta_hits_input(111);
	file_eta_hits(112) := file_eta_hits_input(112);
	file_eta_hits(113) := file_eta_hits_input(113);
	file_eta_hits(114) := file_eta_hits_input(114);
	file_eta_hits(115) := file_eta_hits_input(115);
	file_eta_hits(116) := file_eta_hits_input(116);
	file_eta_hits(117) := file_eta_hits_input(117);
	file_eta_hits(118) := file_eta_hits_input(118);
	file_eta_hits(119) := file_eta_hits_input(119);
	file_eta_hits(120) := file_eta_hits_input(120);
	file_eta_hits(121) := file_eta_hits_input(121);
	file_eta_hits(122) := file_eta_hits_input(122);
	file_eta_hits(123) := file_eta_hits_input(123);
	file_eta_hits(124) := file_eta_hits_input(124);
	file_eta_hits(125) := file_eta_hits_input(125);
	file_eta_hits(126) := file_eta_hits_input(126);
	file_eta_hits(127) := file_eta_hits_input(127);
	file_eta_hits(128) := file_eta_hits_input(128);
	file_eta_hits(129) := file_eta_hits_input(129);
	file_eta_hits(130) := file_eta_hits_input(130);
	file_eta_hits(131) := file_eta_hits_input(131);
	file_eta_hits(132) := file_eta_hits_input(132);
	file_eta_hits(133) := file_eta_hits_input(133);
	file_eta_hits(134) := file_eta_hits_input(134);
	file_eta_hits(135) := file_eta_hits_input(135);
	file_eta_hits(136) := file_eta_hits_input(136);
	file_eta_hits(137) := file_eta_hits_input(137);
	file_eta_hits(138) := file_eta_hits_input(138);
	file_eta_hits(139) := file_eta_hits_input(139);
	file_eta_hits(140) := file_eta_hits_input(140);
	file_eta_hits(141) := file_eta_hits_input(141);
	file_eta_hits(142) := file_eta_hits_input(142);
	file_eta_hits(143) := file_eta_hits_input(143);
	file_eta_hits(144) := file_eta_hits_input(144);
	file_eta_hits(145) := file_eta_hits_input(145);
	file_eta_hits(146) := file_eta_hits_input(146);
	file_eta_hits(147) := file_eta_hits_input(147);
	file_eta_hits(148) := file_eta_hits_input(148);
	file_eta_hits(149) := file_eta_hits_input(149);
	file_eta_hits(150) := file_eta_hits_input(150);
	file_eta_hits(151) := file_eta_hits_input(151);
	file_eta_hits(152) := file_eta_hits_input(152);
	file_eta_hits(153) := file_eta_hits_input(153);
	file_eta_hits(154) := file_eta_hits_input(154);
	file_eta_hits(155) := file_eta_hits_input(155);
	file_eta_hits(156) := file_eta_hits_input(156);
	file_eta_hits(157) := file_eta_hits_input(157);
	file_eta_hits(158) := file_eta_hits_input(158);
	file_eta_hits(159) := file_eta_hits_input(159);
	file_eta_hits(160) := file_eta_hits_input(160);
	file_eta_hits(161) := file_eta_hits_input(161);
	file_eta_hits(162) := file_eta_hits_input(162);
	file_eta_hits(163) := file_eta_hits_input(163);
	file_eta_hits(164) := file_eta_hits_input(164);
	file_eta_hits(165) := file_eta_hits_input(165);
	file_eta_hits(166) := file_eta_hits_input(166);
	file_eta_hits(167) := file_eta_hits_input(167);
	file_eta_hits(168) := file_eta_hits_input(168);
	file_eta_hits(169) := file_eta_hits_input(169);
	file_eta_hits(170) := file_eta_hits_input(170);
	file_eta_hits(171) := file_eta_hits_input(171);
	file_eta_hits(172) := file_eta_hits_input(172);
	file_eta_hits(173) := file_eta_hits_input(173);
	file_eta_hits(174) := file_eta_hits_input(174);
	file_eta_hits(175) := file_eta_hits_input(175);
	file_eta_hits(176) := file_eta_hits_input(176);
	file_eta_hits(177) := file_eta_hits_input(177);
	file_eta_hits(178) := file_eta_hits_input(178);
	file_eta_hits(179) := file_eta_hits_input(179);
	file_eta_hits(180) := file_eta_hits_input(180);
	file_eta_hits(181) := file_eta_hits_input(181);
	file_eta_hits(182) := file_eta_hits_input(182);
	file_eta_hits(183) := file_eta_hits_input(183);
	file_eta_hits(184) := file_eta_hits_input(184);
	file_eta_hits(185) := file_eta_hits_input(185);
	file_eta_hits(186) := file_eta_hits_input(186);
	file_eta_hits(187) := file_eta_hits_input(187);
	file_eta_hits(188) := file_eta_hits_input(188);
	file_eta_hits(189) := file_eta_hits_input(189);
	file_eta_hits(190) := file_eta_hits_input(190);
	file_eta_hits(191) := file_eta_hits_input(191);
	file_eta_hits(192) := file_eta_hits_input(192);
	file_eta_hits(193) := file_eta_hits_input(193);
	file_eta_hits(194) := file_eta_hits_input(194);
	file_eta_hits(195) := file_eta_hits_input(195);
	file_eta_hits(196) := file_eta_hits_input(196);
	file_eta_hits(197) := file_eta_hits_input(197);
	file_eta_hits(198) := file_eta_hits_input(198);
	file_eta_hits(199) := file_eta_hits_input(199);
	file_eta_hits(200) := file_eta_hits_input(200);
	file_eta_hits(201) := file_eta_hits_input(201);
	file_eta_hits(202) := file_eta_hits_input(202);
	file_eta_hits(203) := file_eta_hits_input(203);
	file_eta_hits(204) := file_eta_hits_input(204);
	file_eta_hits(205) := file_eta_hits_input(205);
	file_eta_hits(206) := file_eta_hits_input(206);
	file_eta_hits(207) := file_eta_hits_input(207);
	file_eta_hits(208) := file_eta_hits_input(208);
	file_eta_hits(209) := file_eta_hits_input(209);
	file_eta_hits(210) := file_eta_hits_input(210);
	file_eta_hits(211) := file_eta_hits_input(211);
	file_eta_hits(212) := file_eta_hits_input(212);
	file_eta_hits(213) := file_eta_hits_input(213);
	file_eta_hits(214) := file_eta_hits_input(214);
	file_eta_hits(215) := file_eta_hits_input(215);
	file_eta_hits(216) := file_eta_hits_input(216);
	file_eta_hits(217) := file_eta_hits_input(217);
	file_eta_hits(218) := file_eta_hits_input(218);
	file_eta_hits(219) := file_eta_hits_input(219);
	file_eta_hits(220) := file_eta_hits_input(220);
	file_eta_hits(221) := file_eta_hits_input(221);
	file_eta_hits(222) := file_eta_hits_input(222);
	file_eta_hits(223) := file_eta_hits_input(223);
	file_eta_hits(224) := file_eta_hits_input(224);
	file_eta_hits(225) := file_eta_hits_input(225);
	file_eta_hits(226) := file_eta_hits_input(226);
	file_eta_hits(227) := file_eta_hits_input(227);
	file_eta_hits(228) := file_eta_hits_input(228);
	file_eta_hits(229) := file_eta_hits_input(229);
	file_eta_hits(230) := file_eta_hits_input(230);
	file_eta_hits(231) := file_eta_hits_input(231);
	file_eta_hits(232) := file_eta_hits_input(232);
	file_eta_hits(233) := file_eta_hits_input(233);
	file_eta_hits(234) := file_eta_hits_input(234);
	file_eta_hits(235) := file_eta_hits_input(235);
	file_eta_hits(236) := file_eta_hits_input(236);
	file_eta_hits(237) := file_eta_hits_input(237);
	file_eta_hits(238) := file_eta_hits_input(238);
	file_eta_hits(239) := file_eta_hits_input(239);
	file_eta_hits(240) := file_eta_hits_input(240);
	file_eta_hits(241) := file_eta_hits_input(241);
	file_eta_hits(242) := file_eta_hits_input(242);
	file_eta_hits(243) := file_eta_hits_input(243);
	file_eta_hits(244) := file_eta_hits_input(244);
	file_eta_hits(245) := file_eta_hits_input(245);
	file_eta_hits(246) := file_eta_hits_input(246);
	file_eta_hits(247) := file_eta_hits_input(247);
	file_eta_hits(248) := file_eta_hits_input(248);
	file_eta_hits(249) := file_eta_hits_input(249);
	file_eta_hits(250) := file_eta_hits_input(250);
	file_eta_hits(251) := file_eta_hits_input(251);
	file_eta_hits(252) := file_eta_hits_input(252);
	file_eta_hits(253) := file_eta_hits_input(253);
	file_eta_hits(254) := file_eta_hits_input(254);
	file_eta_hits(255) := file_eta_hits_input(255);
	file_eta_hits(256) := file_eta_hits_input(256);
	file_eta_hits(257) := file_eta_hits_input(257);
	file_eta_hits(258) := file_eta_hits_input(258);
	file_eta_hits(259) := file_eta_hits_input(259);
	file_eta_hits(260) := file_eta_hits_input(260);
	file_eta_hits(261) := file_eta_hits_input(261);
	file_eta_hits(262) := file_eta_hits_input(262);
	file_eta_hits(263) := file_eta_hits_input(263);
	file_eta_hits(264) := file_eta_hits_input(264);
	file_eta_hits(265) := file_eta_hits_input(265);
	file_eta_hits(266) := file_eta_hits_input(266);
	file_eta_hits(267) := file_eta_hits_input(267);
	file_eta_hits(268) := file_eta_hits_input(268);
	file_eta_hits(269) := file_eta_hits_input(269);
	file_eta_hits(270) := file_eta_hits_input(270);
	file_eta_hits(271) := file_eta_hits_input(271);
	file_eta_hits(272) := file_eta_hits_input(272);
	file_eta_hits(273) := file_eta_hits_input(273);
	file_eta_hits(274) := file_eta_hits_input(274);
	file_eta_hits(275) := file_eta_hits_input(275);
	file_eta_hits(276) := file_eta_hits_input(276);
	file_eta_hits(277) := file_eta_hits_input(277);
	file_eta_hits(278) := file_eta_hits_input(278);
	file_eta_hits(279) := file_eta_hits_input(279);
	file_eta_hits(280) := file_eta_hits_input(280);
	file_eta_hits(281) := file_eta_hits_input(281);
	file_eta_hits(282) := file_eta_hits_input(282);
	file_eta_hits(283) := file_eta_hits_input(283);
	file_eta_hits(284) := file_eta_hits_input(284);
	file_eta_hits(285) := file_eta_hits_input(285);
	file_eta_hits(286) := file_eta_hits_input(286);
	file_eta_hits(287) := file_eta_hits_input(287);
	file_eta_hits(288) := file_eta_hits_input(288);
	file_eta_hits(289) := file_eta_hits_input(289);
	file_eta_hits(290) := file_eta_hits_input(290);
	file_eta_hits(291) := file_eta_hits_input(291);
	file_eta_hits(292) := file_eta_hits_input(292);
	file_eta_hits(293) := file_eta_hits_input(293);
	file_eta_hits(294) := file_eta_hits_input(294);
	file_eta_hits(295) := file_eta_hits_input(295);
	file_eta_hits(296) := file_eta_hits_input(296);
	file_eta_hits(297) := file_eta_hits_input(297);
	file_eta_hits(298) := file_eta_hits_input(298);
	file_eta_hits(299) := file_eta_hits_input(299);
	file_eta_hits(300) := file_eta_hits_input(300);
	file_eta_hits(301) := file_eta_hits_input(301);
	file_eta_hits(302) := file_eta_hits_input(302);
	file_eta_hits(303) := file_eta_hits_input(303);
	file_eta_hits(304) := file_eta_hits_input(304);
	file_eta_hits(305) := file_eta_hits_input(305);
	file_eta_hits(306) := file_eta_hits_input(306);
	file_eta_hits(307) := file_eta_hits_input(307);
	file_eta_hits(308) := file_eta_hits_input(308);
	file_eta_hits(309) := file_eta_hits_input(309);
	file_eta_hits(310) := file_eta_hits_input(310);
	file_eta_hits(311) := file_eta_hits_input(311);
	file_eta_hits(312) := file_eta_hits_input(312);
	file_eta_hits(313) := file_eta_hits_input(313);
	file_eta_hits(314) := file_eta_hits_input(314);
	file_eta_hits(315) := file_eta_hits_input(315);
	file_eta_hits(316) := file_eta_hits_input(316);
	file_eta_hits(317) := file_eta_hits_input(317);
	file_eta_hits(318) := file_eta_hits_input(318);
	file_eta_hits(319) := file_eta_hits_input(319);
	file_eta_hits(320) := '0';
	file_eta_hits(321) := '0';
	file_eta_hits(322) := '0';
	file_eta_hits(323) := '0';
	file_eta_hits(324) := '0';
	file_eta_hits(325) := '0';
	file_eta_hits(326) := '0';
	file_eta_hits(327) := '0';
	file_eta_hits(328) := '0';
	file_eta_hits(329) := '0';
	file_eta_hits(330) := '0';
	file_eta_hits(331) := '0';
	file_eta_hits(332) := '0';
	file_eta_hits(333) := '0';
	file_eta_hits(334) := '0';
	file_eta_hits(335) := '0';
	file_eta_hits(336) := '0';
	file_eta_hits(337) := '0';
	file_eta_hits(338) := '0';
	file_eta_hits(339) := '0';
	file_eta_hits(340) := '0';
	file_eta_hits(341) := '0';
	file_eta_hits(342) := '0';
	file_eta_hits(343) := '0';
	file_eta_hits(344) := '0';
	file_eta_hits(345) := '0';
	file_eta_hits(346) := '0';
	file_eta_hits(347) := '0';
	file_eta_hits(348) := '0';
	file_eta_hits(349) := '0';
	file_eta_hits(350) := '0';
	file_eta_hits(351) := '0';
	file_eta_hits(352) := '0';
	file_eta_hits(353) := '0';
	file_eta_hits(354) := '0';
	file_eta_hits(355) := '0';
	file_eta_hits(356) := '0';
	file_eta_hits(357) := '0';
	file_eta_hits(358) := '0';
	file_eta_hits(359) := '0';
	file_eta_hits(360) := '0';
	file_eta_hits(361) := '0';
	file_eta_hits(362) := '0';
	file_eta_hits(363) := '0';
	file_eta_hits(364) := '0';
	file_eta_hits(365) := '0';
	file_eta_hits(366) := '0';
	file_eta_hits(367) := '0';
	file_eta_hits(368) := '0';
	file_eta_hits(369) := '0';
	file_eta_hits(370) := '0';
	file_eta_hits(371) := '0';
	file_eta_hits(372) := '0';
	file_eta_hits(373) := '0';
	file_eta_hits(374) := '0';
	file_eta_hits(375) := '0';
	file_eta_hits(376) := '0';
	file_eta_hits(377) := '0';
	file_eta_hits(378) := '0';
	file_eta_hits(379) := '0';
	file_eta_hits(380) := '0';
	file_eta_hits(381) := '0';
	file_eta_hits(382) := '0';
	file_eta_hits(383) := '0';
	file_eta_hits(384) := '0';
	file_eta_hits(385) := '0';
	file_eta_hits(386) := '0';
	file_eta_hits(387) := '0';
	file_eta_hits(388) := '0';
	file_eta_hits(389) := '0';
	file_eta_hits(390) := '0';
	file_eta_hits(391) := '0';
	file_eta_hits(392) := '0';
	file_eta_hits(393) := '0';
	file_eta_hits(394) := '0';
	file_eta_hits(395) := '0';
	file_eta_hits(396) := '0';
	file_eta_hits(397) := '0';
	file_eta_hits(398) := '0';
	file_eta_hits(399) := '0';
	file_eta_hits(400) := '0';
	file_eta_hits(401) := '0';
	file_eta_hits(402) := '0';
	file_eta_hits(403) := '0';
	file_eta_hits(404) := '0';
	file_eta_hits(405) := '0';
	file_eta_hits(406) := '0';
	file_eta_hits(407) := '0';
	file_eta_hits(408) := '0';
	file_eta_hits(409) := '0';
	file_eta_hits(410) := '0';
	file_eta_hits(411) := '0';
	file_eta_hits(412) := '0';
	file_eta_hits(413) := '0';
	file_eta_hits(414) := '0';
	file_eta_hits(415) := '0';
	file_eta_hits(416) := '0';
	file_eta_hits(417) := '0';
	file_eta_hits(418) := '0';
	file_eta_hits(419) := '0';
	file_eta_hits(420) := '0';
	file_eta_hits(421) := '0';
	file_eta_hits(422) := '0';
	file_eta_hits(423) := '0';
	file_eta_hits(424) := '0';
	file_eta_hits(425) := '0';
	file_eta_hits(426) := '0';
	file_eta_hits(427) := '0';
	file_eta_hits(428) := '0';
	file_eta_hits(429) := '0';
	file_eta_hits(430) := '0';
	file_eta_hits(431) := '0';
	file_eta_hits(432) := '0';
	file_eta_hits(433) := '0';
	file_eta_hits(434) := '0';
	file_eta_hits(435) := '0';
	file_eta_hits(436) := '0';
	file_eta_hits(437) := '0';
	file_eta_hits(438) := '0';
	file_eta_hits(439) := '0';
	file_eta_hits(440) := '0';
	file_eta_hits(441) := '0';
	file_eta_hits(442) := '0';
	file_eta_hits(443) := '0';
	file_eta_hits(444) := '0';
	file_eta_hits(445) := '0';
	file_eta_hits(446) := '0';
	file_eta_hits(447) := '0';
	file_eta_hits(448) := '0';
	file_eta_hits(449) := '0';
	file_eta_hits(450) := '0';
	file_eta_hits(451) := '0';
	file_eta_hits(452) := '0';
	file_eta_hits(453) := '0';
	file_eta_hits(454) := '0';
	file_eta_hits(455) := '0';
	file_eta_hits(456) := '0';
	file_eta_hits(457) := '0';
	file_eta_hits(458) := '0';
	file_eta_hits(459) := '0';
	file_eta_hits(460) := '0';
	file_eta_hits(461) := '0';
	file_eta_hits(462) := '0';
	file_eta_hits(463) := '0';
	file_eta_hits(464) := '0';
	file_eta_hits(465) := '0';
	file_eta_hits(466) := '0';
	file_eta_hits(467) := '0';
	file_eta_hits(468) := '0';
	file_eta_hits(469) := '0';
	file_eta_hits(470) := '0';
	file_eta_hits(471) := '0';
	file_eta_hits(472) := '0';
	file_eta_hits(473) := '0';
	file_eta_hits(474) := '0';
	file_eta_hits(475) := '0';
	file_eta_hits(476) := '0';
	file_eta_hits(477) := '0';
	file_eta_hits(478) := '0';
	file_eta_hits(479) := '0';
	file_eta_hits(480) := '0';
	file_eta_hits(481) := '0';
	file_eta_hits(482) := '0';
	file_eta_hits(483) := '0';
	file_eta_hits(484) := '0';
	file_eta_hits(485) := '0';
	file_eta_hits(486) := '0';
	file_eta_hits(487) := '0';
	file_eta_hits(488) := '0';
	file_eta_hits(489) := '0';
	file_eta_hits(490) := '0';
	file_eta_hits(491) := '0';
	file_eta_hits(492) := '0';
	file_eta_hits(493) := '0';
	file_eta_hits(494) := '0';
	file_eta_hits(495) := '0';
	file_eta_hits(496) := '0';
	file_eta_hits(497) := '0';
	file_eta_hits(498) := '0';
	file_eta_hits(499) := '0';
	file_eta_hits(500) := '0';
	file_eta_hits(501) := '0';
	file_eta_hits(502) := '0';
	file_eta_hits(503) := '0';
	file_eta_hits(504) := '0';
	file_eta_hits(505) := '0';
	file_eta_hits(506) := '0';
	file_eta_hits(507) := '0';
	file_eta_hits(508) := '0';
	file_eta_hits(509) := '0';
	file_eta_hits(510) := '0';
	file_eta_hits(511) := '0';



    return file_eta_hits;
  end function define_map_BI;





function define_map_BM1 (signal file_eta_hits_input : in std_logic_vector(383 downto 0) ) 
    return t_inputs  is
    variable file_eta_hits : t_inputs;
    begin 

	--these numbers are extracted from simulations -- depending on the sectors, it might need updates


	file_eta_hits(25) := file_eta_hits_input(0);
	file_eta_hits(27) := file_eta_hits_input(1);
	file_eta_hits(29) := file_eta_hits_input(2);
	file_eta_hits(30) := file_eta_hits_input(3);
	file_eta_hits(32) := file_eta_hits_input(4);
	file_eta_hits(37) := file_eta_hits_input(5);
	file_eta_hits(39) := file_eta_hits_input(6);
	file_eta_hits(40) := file_eta_hits_input(7);
	file_eta_hits(42) := file_eta_hits_input(8);
	file_eta_hits(44) := file_eta_hits_input(9);
	file_eta_hits(45) := file_eta_hits_input(10);
	file_eta_hits(47) := file_eta_hits_input(11);
	file_eta_hits(49) := file_eta_hits_input(12);
	file_eta_hits(50) := file_eta_hits_input(13);
	file_eta_hits(52) := file_eta_hits_input(14);
	file_eta_hits(54) := file_eta_hits_input(15);
	file_eta_hits(55) := file_eta_hits_input(16);
	file_eta_hits(57) := file_eta_hits_input(17);
	file_eta_hits(59) := file_eta_hits_input(18);
	file_eta_hits(60) := file_eta_hits_input(19);
	file_eta_hits(62) := file_eta_hits_input(20);
	file_eta_hits(62) := file_eta_hits_input(21);
	file_eta_hits(64) := file_eta_hits_input(22);
	file_eta_hits(65) := file_eta_hits_input(23);
	file_eta_hits(67) := file_eta_hits_input(24);
	file_eta_hits(69) := file_eta_hits_input(25);
	file_eta_hits(70) := file_eta_hits_input(26);
	file_eta_hits(72) := file_eta_hits_input(27);
	file_eta_hits(74) := file_eta_hits_input(28);
	file_eta_hits(76) := file_eta_hits_input(29);
	file_eta_hits(77) := file_eta_hits_input(30);
	file_eta_hits(79) := file_eta_hits_input(31);
	file_eta_hits(81) := file_eta_hits_input(32);
	file_eta_hits(82) := file_eta_hits_input(33);
	file_eta_hits(84) := file_eta_hits_input(34);
	file_eta_hits(86) := file_eta_hits_input(35);
	file_eta_hits(87) := file_eta_hits_input(36);
	file_eta_hits(89) := file_eta_hits_input(37);
	file_eta_hits(91) := file_eta_hits_input(38);
	file_eta_hits(92) := file_eta_hits_input(39);
	file_eta_hits(94) := file_eta_hits_input(40);
	file_eta_hits(96) := file_eta_hits_input(41);
	file_eta_hits(97) := file_eta_hits_input(42);
	file_eta_hits(99) := file_eta_hits_input(43);
	file_eta_hits(101) := file_eta_hits_input(44);
	file_eta_hits(104) := file_eta_hits_input(45);
	file_eta_hits(106) := file_eta_hits_input(46);
	file_eta_hits(107) := file_eta_hits_input(47);
	file_eta_hits(109) := file_eta_hits_input(48);
	file_eta_hits(110) := file_eta_hits_input(49);
	file_eta_hits(111) := file_eta_hits_input(50);
	file_eta_hits(113) := file_eta_hits_input(51);
	file_eta_hits(114) := file_eta_hits_input(52);
	file_eta_hits(116) := file_eta_hits_input(53);
	file_eta_hits(117) := file_eta_hits_input(54);
	file_eta_hits(119) := file_eta_hits_input(55);
	file_eta_hits(120) := file_eta_hits_input(56);
	file_eta_hits(122) := file_eta_hits_input(57);
	file_eta_hits(123) := file_eta_hits_input(58);
	file_eta_hits(125) := file_eta_hits_input(59);
	file_eta_hits(126) := file_eta_hits_input(60);
	file_eta_hits(128) := file_eta_hits_input(61);
	file_eta_hits(129) := file_eta_hits_input(62);
	file_eta_hits(131) := file_eta_hits_input(63);
	file_eta_hits(132) := file_eta_hits_input(64);
	file_eta_hits(133) := file_eta_hits_input(65);
	file_eta_hits(135) := file_eta_hits_input(66);
	file_eta_hits(136) := file_eta_hits_input(67);
	file_eta_hits(138) := file_eta_hits_input(68);
	file_eta_hits(139) := file_eta_hits_input(69);
	file_eta_hits(141) := file_eta_hits_input(70);
	file_eta_hits(142) := file_eta_hits_input(71);
	file_eta_hits(144) := file_eta_hits_input(72);
	file_eta_hits(145) := file_eta_hits_input(73);
	file_eta_hits(147) := file_eta_hits_input(74);
	file_eta_hits(148) := file_eta_hits_input(75);
	file_eta_hits(150) := file_eta_hits_input(76);
	file_eta_hits(150) := file_eta_hits_input(77);
	file_eta_hits(151) := file_eta_hits_input(78);
	file_eta_hits(153) := file_eta_hits_input(79);
	file_eta_hits(154) := file_eta_hits_input(80);
	file_eta_hits(156) := file_eta_hits_input(81);
	file_eta_hits(157) := file_eta_hits_input(82);
	file_eta_hits(159) := file_eta_hits_input(83);
	file_eta_hits(160) := file_eta_hits_input(84);
	file_eta_hits(162) := file_eta_hits_input(85);
	file_eta_hits(163) := file_eta_hits_input(86);
	file_eta_hits(165) := file_eta_hits_input(87);
	file_eta_hits(166) := file_eta_hits_input(88);
	file_eta_hits(167) := file_eta_hits_input(89);
	file_eta_hits(169) := file_eta_hits_input(90);
	file_eta_hits(170) := file_eta_hits_input(91);
	file_eta_hits(172) := file_eta_hits_input(92);
	file_eta_hits(173) := file_eta_hits_input(93);
	file_eta_hits(175) := file_eta_hits_input(94);
	file_eta_hits(176) := file_eta_hits_input(95);
	file_eta_hits(178) := file_eta_hits_input(96);
	file_eta_hits(179) := file_eta_hits_input(97);
	file_eta_hits(181) := file_eta_hits_input(98);
	file_eta_hits(182) := file_eta_hits_input(99);
	file_eta_hits(184) := file_eta_hits_input(100);
	file_eta_hits(185) := file_eta_hits_input(101);
	file_eta_hits(187) := file_eta_hits_input(102);
	file_eta_hits(188) := file_eta_hits_input(103);
	file_eta_hits(190) := file_eta_hits_input(104);
	file_eta_hits(191) := file_eta_hits_input(105);
	file_eta_hits(193) := file_eta_hits_input(106);
	file_eta_hits(194) := file_eta_hits_input(107);
	file_eta_hits(195) := file_eta_hits_input(108);
	file_eta_hits(198) := file_eta_hits_input(109);
	file_eta_hits(200) := file_eta_hits_input(110);
	file_eta_hits(201) := file_eta_hits_input(111);
	file_eta_hits(203) := file_eta_hits_input(112);
	file_eta_hits(204) := file_eta_hits_input(113);
	file_eta_hits(205) := file_eta_hits_input(114);
	file_eta_hits(207) := file_eta_hits_input(115);
	file_eta_hits(208) := file_eta_hits_input(116);
	file_eta_hits(210) := file_eta_hits_input(117);
	file_eta_hits(211) := file_eta_hits_input(118);
	file_eta_hits(213) := file_eta_hits_input(119);
	file_eta_hits(214) := file_eta_hits_input(120);
	file_eta_hits(216) := file_eta_hits_input(121);
	file_eta_hits(217) := file_eta_hits_input(122);
	file_eta_hits(220) := file_eta_hits_input(123);
	file_eta_hits(222) := file_eta_hits_input(124);
	file_eta_hits(223) := file_eta_hits_input(125);
	file_eta_hits(225) := file_eta_hits_input(126);
	file_eta_hits(226) := file_eta_hits_input(127);
	file_eta_hits(227) := file_eta_hits_input(128);
	file_eta_hits(229) := file_eta_hits_input(129);
	file_eta_hits(230) := file_eta_hits_input(130);
	file_eta_hits(232) := file_eta_hits_input(131);
	file_eta_hits(233) := file_eta_hits_input(132);
	file_eta_hits(235) := file_eta_hits_input(133);
	file_eta_hits(236) := file_eta_hits_input(134);
	file_eta_hits(238) := file_eta_hits_input(135);
	file_eta_hits(239) := file_eta_hits_input(136);
	file_eta_hits(241) := file_eta_hits_input(137);
	file_eta_hits(242) := file_eta_hits_input(138);
	file_eta_hits(244) := file_eta_hits_input(139);
	file_eta_hits(244) := file_eta_hits_input(140);
	file_eta_hits(246) := file_eta_hits_input(141);
	file_eta_hits(247) := file_eta_hits_input(142);
	file_eta_hits(249) := file_eta_hits_input(143);
	file_eta_hits(250) := file_eta_hits_input(144);
	file_eta_hits(252) := file_eta_hits_input(145);
	file_eta_hits(253) := file_eta_hits_input(146);
	file_eta_hits(255) := file_eta_hits_input(147);
	file_eta_hits(256) := file_eta_hits_input(148);
	file_eta_hits(258) := file_eta_hits_input(149);
	file_eta_hits(259) := file_eta_hits_input(150);
	file_eta_hits(261) := file_eta_hits_input(151);
	file_eta_hits(265) := file_eta_hits_input(152);
	file_eta_hits(266) := file_eta_hits_input(153);
	file_eta_hits(268) := file_eta_hits_input(154);
	file_eta_hits(269) := file_eta_hits_input(155);
	file_eta_hits(271) := file_eta_hits_input(156);
	file_eta_hits(272) := file_eta_hits_input(157);
	file_eta_hits(274) := file_eta_hits_input(158);
	file_eta_hits(275) := file_eta_hits_input(159);
	file_eta_hits(277) := file_eta_hits_input(160);
	file_eta_hits(278) := file_eta_hits_input(161);
	file_eta_hits(280) := file_eta_hits_input(162);
	file_eta_hits(281) := file_eta_hits_input(163);
	file_eta_hits(283) := file_eta_hits_input(164);
	file_eta_hits(284) := file_eta_hits_input(165);
	file_eta_hits(286) := file_eta_hits_input(166);
	file_eta_hits(287) := file_eta_hits_input(167);
	file_eta_hits(289) := file_eta_hits_input(168);
	file_eta_hits(290) := file_eta_hits_input(169);
	file_eta_hits(292) := file_eta_hits_input(170);
	file_eta_hits(294) := file_eta_hits_input(171);
	file_eta_hits(296) := file_eta_hits_input(172);
	file_eta_hits(297) := file_eta_hits_input(173);
	file_eta_hits(299) := file_eta_hits_input(174);
	file_eta_hits(300) := file_eta_hits_input(175);
	file_eta_hits(302) := file_eta_hits_input(176);
	file_eta_hits(304) := file_eta_hits_input(177);
	file_eta_hits(305) := file_eta_hits_input(178);
	file_eta_hits(307) := file_eta_hits_input(179);
	file_eta_hits(309) := file_eta_hits_input(180);
	file_eta_hits(310) := file_eta_hits_input(181);
	file_eta_hits(312) := file_eta_hits_input(182);
	file_eta_hits(313) := file_eta_hits_input(183);
	file_eta_hits(315) := file_eta_hits_input(184);
	file_eta_hits(317) := file_eta_hits_input(185);
	file_eta_hits(318) := file_eta_hits_input(186);
	file_eta_hits(320) := file_eta_hits_input(187);
	file_eta_hits(322) := file_eta_hits_input(188);
	file_eta_hits(323) := file_eta_hits_input(189);
	file_eta_hits(325) := file_eta_hits_input(190);
	file_eta_hits(326) := file_eta_hits_input(191);
	file_eta_hits(328) := file_eta_hits_input(192);
	file_eta_hits(330) := file_eta_hits_input(193);
	file_eta_hits(331) := file_eta_hits_input(194);
	file_eta_hits(333) := file_eta_hits_input(195);
	file_eta_hits(335) := file_eta_hits_input(196);
	file_eta_hits(336) := file_eta_hits_input(197);
	file_eta_hits(338) := file_eta_hits_input(198);
	file_eta_hits(339) := file_eta_hits_input(199);
	file_eta_hits(341) := file_eta_hits_input(200);
	file_eta_hits(343) := file_eta_hits_input(201);
	file_eta_hits(344) := file_eta_hits_input(202);
	file_eta_hits(346) := file_eta_hits_input(203);
	file_eta_hits(348) := file_eta_hits_input(204);
	file_eta_hits(349) := file_eta_hits_input(205);
	file_eta_hits(351) := file_eta_hits_input(206);
	file_eta_hits(352) := file_eta_hits_input(207);
	file_eta_hits(354) := file_eta_hits_input(208);
	file_eta_hits(356) := file_eta_hits_input(209);
	file_eta_hits(360) := file_eta_hits_input(210);
	file_eta_hits(361) := file_eta_hits_input(211);
	file_eta_hits(363) := file_eta_hits_input(212);
	file_eta_hits(365) := file_eta_hits_input(213);
	file_eta_hits(366) := file_eta_hits_input(214);
	file_eta_hits(368) := file_eta_hits_input(215);
	file_eta_hits(369) := file_eta_hits_input(216);
	file_eta_hits(371) := file_eta_hits_input(217);
	file_eta_hits(373) := file_eta_hits_input(218);
	file_eta_hits(374) := file_eta_hits_input(219);
	file_eta_hits(376) := file_eta_hits_input(220);
	file_eta_hits(378) := file_eta_hits_input(221);
	file_eta_hits(379) := file_eta_hits_input(222);
	file_eta_hits(381) := file_eta_hits_input(223);
	file_eta_hits(382) := file_eta_hits_input(224);
	file_eta_hits(384) := file_eta_hits_input(225);
	file_eta_hits(386) := file_eta_hits_input(226);
	file_eta_hits(387) := file_eta_hits_input(227);
	file_eta_hits(389) := file_eta_hits_input(228);
	file_eta_hits(391) := file_eta_hits_input(229);
	file_eta_hits(392) := file_eta_hits_input(230);
	file_eta_hits(394) := file_eta_hits_input(231);
	file_eta_hits(395) := file_eta_hits_input(232);
	file_eta_hits(397) := file_eta_hits_input(233);
	file_eta_hits(399) := file_eta_hits_input(234);
	file_eta_hits(400) := file_eta_hits_input(235);
	file_eta_hits(402) := file_eta_hits_input(236);
	file_eta_hits(404) := file_eta_hits_input(237);
	file_eta_hits(405) := file_eta_hits_input(238);
	file_eta_hits(407) := file_eta_hits_input(239);
	file_eta_hits(408) := file_eta_hits_input(240);
	file_eta_hits(410) := file_eta_hits_input(241);
	file_eta_hits(412) := file_eta_hits_input(242);
	file_eta_hits(413) := file_eta_hits_input(243);
	file_eta_hits(415) := file_eta_hits_input(244);
	file_eta_hits(417) := file_eta_hits_input(245);
	file_eta_hits(418) := file_eta_hits_input(246);
	file_eta_hits(420) := file_eta_hits_input(247);
	file_eta_hits(421) := file_eta_hits_input(248);
	file_eta_hits(423) := file_eta_hits_input(249);
	file_eta_hits(427) := file_eta_hits_input(250);
	file_eta_hits(429) := file_eta_hits_input(251);
	file_eta_hits(431) := file_eta_hits_input(252);
	file_eta_hits(432) := file_eta_hits_input(253);
	file_eta_hits(434) := file_eta_hits_input(254);
	file_eta_hits(436) := file_eta_hits_input(255);
	file_eta_hits(437) := file_eta_hits_input(256);
	file_eta_hits(439) := file_eta_hits_input(257);
	file_eta_hits(441) := file_eta_hits_input(258);
	file_eta_hits(442) := file_eta_hits_input(259);
	file_eta_hits(444) := file_eta_hits_input(260);
	file_eta_hits(446) := file_eta_hits_input(261);
	file_eta_hits(447) := file_eta_hits_input(262);
	file_eta_hits(449) := file_eta_hits_input(263);
	file_eta_hits(451) := file_eta_hits_input(264);
	file_eta_hits(452) := file_eta_hits_input(265);
	file_eta_hits(454) := file_eta_hits_input(266);
	file_eta_hits(456) := file_eta_hits_input(267);
	file_eta_hits(457) := file_eta_hits_input(268);
	file_eta_hits(459) := file_eta_hits_input(269);
	file_eta_hits(461) := file_eta_hits_input(270);
	file_eta_hits(462) := file_eta_hits_input(271);
	file_eta_hits(464) := file_eta_hits_input(272);
	file_eta_hits(466) := file_eta_hits_input(273);
	file_eta_hits(468) := file_eta_hits_input(274);
	file_eta_hits(470) := file_eta_hits_input(275);
	file_eta_hits(471) := file_eta_hits_input(276);
	file_eta_hits(473) := file_eta_hits_input(277);
	file_eta_hits(475) := file_eta_hits_input(278);
	file_eta_hits(476) := file_eta_hits_input(279);
	file_eta_hits(478) := file_eta_hits_input(280);
	file_eta_hits(480) := file_eta_hits_input(281);
	file_eta_hits(482) := file_eta_hits_input(282);
	file_eta_hits(483) := file_eta_hits_input(283);
	file_eta_hits(485) := file_eta_hits_input(284);
	file_eta_hits(487) := file_eta_hits_input(285);
	file_eta_hits(488) := file_eta_hits_input(286);
	file_eta_hits(490) := file_eta_hits_input(287);
	file_eta_hits(492) := file_eta_hits_input(288);
	file_eta_hits(493) := file_eta_hits_input(289);
	file_eta_hits(495) := file_eta_hits_input(290);
	file_eta_hits(497) := file_eta_hits_input(291);
	file_eta_hits(498) := file_eta_hits_input(292);
	file_eta_hits(500) := file_eta_hits_input(293);
	file_eta_hits(502) := file_eta_hits_input(294);
	file_eta_hits(503) := file_eta_hits_input(295);
	file_eta_hits(505) := file_eta_hits_input(296);
	file_eta_hits(507) := file_eta_hits_input(297);
	file_eta_hits(0) := '0';
	file_eta_hits(1) := '0';
	file_eta_hits(2) := '0';
	file_eta_hits(3) := '0';
	file_eta_hits(4) := '0';
	file_eta_hits(5) := '0';
	file_eta_hits(6) := '0';
	file_eta_hits(7) := '0';
	file_eta_hits(8) := '0';
	file_eta_hits(9) := '0';
	file_eta_hits(10) := '0';
	file_eta_hits(11) := '0';
	file_eta_hits(12) := '0';
	file_eta_hits(13) := '0';
	file_eta_hits(14) := '0';
	file_eta_hits(15) := '0';
	file_eta_hits(16) := '0';
	file_eta_hits(17) := '0';
	file_eta_hits(18) := '0';
	file_eta_hits(19) := '0';
	file_eta_hits(20) := '0';
	file_eta_hits(21) := '0';
	file_eta_hits(22) := '0';
	file_eta_hits(23) := '0';
	file_eta_hits(24) := '0';
	file_eta_hits(26) := '0';
	file_eta_hits(28) := '0';
	file_eta_hits(31) := '0';
	file_eta_hits(33) := '0';
	file_eta_hits(34) := '0';
	file_eta_hits(35) := '0';
	file_eta_hits(36) := '0';
	file_eta_hits(38) := '0';
	file_eta_hits(41) := '0';
	file_eta_hits(43) := '0';
	file_eta_hits(46) := '0';
	file_eta_hits(48) := '0';
	file_eta_hits(51) := '0';
	file_eta_hits(53) := '0';
	file_eta_hits(56) := '0';
	file_eta_hits(58) := '0';
	file_eta_hits(61) := '0';
	file_eta_hits(63) := '0';
	file_eta_hits(66) := '0';
	file_eta_hits(68) := '0';
	file_eta_hits(71) := '0';
	file_eta_hits(73) := '0';
	file_eta_hits(75) := '0';
	file_eta_hits(78) := '0';
	file_eta_hits(80) := '0';
	file_eta_hits(83) := '0';
	file_eta_hits(85) := '0';
	file_eta_hits(88) := '0';
	file_eta_hits(90) := '0';
	file_eta_hits(93) := '0';
	file_eta_hits(95) := '0';
	file_eta_hits(98) := '0';
	file_eta_hits(100) := '0';
	file_eta_hits(102) := '0';
	file_eta_hits(103) := '0';
	file_eta_hits(105) := '0';
	file_eta_hits(108) := '0';
	file_eta_hits(112) := '0';
	file_eta_hits(115) := '0';
	file_eta_hits(118) := '0';
	file_eta_hits(121) := '0';
	file_eta_hits(124) := '0';
	file_eta_hits(127) := '0';
	file_eta_hits(130) := '0';
	file_eta_hits(134) := '0';
	file_eta_hits(137) := '0';
	file_eta_hits(140) := '0';
	file_eta_hits(143) := '0';
	file_eta_hits(146) := '0';
	file_eta_hits(149) := '0';
	file_eta_hits(152) := '0';
	file_eta_hits(155) := '0';
	file_eta_hits(158) := '0';
	file_eta_hits(161) := '0';
	file_eta_hits(164) := '0';
	file_eta_hits(168) := '0';
	file_eta_hits(171) := '0';
	file_eta_hits(174) := '0';
	file_eta_hits(177) := '0';
	file_eta_hits(180) := '0';
	file_eta_hits(183) := '0';
	file_eta_hits(186) := '0';
	file_eta_hits(189) := '0';
	file_eta_hits(192) := '0';
	file_eta_hits(196) := '0';
	file_eta_hits(197) := '0';
	file_eta_hits(199) := '0';
	file_eta_hits(202) := '0';
	file_eta_hits(206) := '0';
	file_eta_hits(209) := '0';
	file_eta_hits(212) := '0';
	file_eta_hits(215) := '0';
	file_eta_hits(218) := '0';
	file_eta_hits(219) := '0';
	file_eta_hits(221) := '0';
	file_eta_hits(224) := '0';
	file_eta_hits(228) := '0';
	file_eta_hits(231) := '0';
	file_eta_hits(234) := '0';
	file_eta_hits(237) := '0';
	file_eta_hits(240) := '0';
	file_eta_hits(243) := '0';
	file_eta_hits(245) := '0';
	file_eta_hits(248) := '0';
	file_eta_hits(251) := '0';
	file_eta_hits(254) := '0';
	file_eta_hits(257) := '0';
	file_eta_hits(260) := '0';
	file_eta_hits(262) := '0';
	file_eta_hits(263) := '0';
	file_eta_hits(264) := '0';
	file_eta_hits(267) := '0';
	file_eta_hits(270) := '0';
	file_eta_hits(273) := '0';
	file_eta_hits(276) := '0';
	file_eta_hits(279) := '0';
	file_eta_hits(282) := '0';
	file_eta_hits(285) := '0';
	file_eta_hits(288) := '0';
	file_eta_hits(291) := '0';
	file_eta_hits(293) := '0';
	file_eta_hits(295) := '0';
	file_eta_hits(298) := '0';
	file_eta_hits(301) := '0';
	file_eta_hits(303) := '0';
	file_eta_hits(306) := '0';
	file_eta_hits(308) := '0';
	file_eta_hits(311) := '0';
	file_eta_hits(314) := '0';
	file_eta_hits(316) := '0';
	file_eta_hits(319) := '0';
	file_eta_hits(321) := '0';
	file_eta_hits(324) := '0';
	file_eta_hits(327) := '0';
	file_eta_hits(329) := '0';
	file_eta_hits(332) := '0';
	file_eta_hits(334) := '0';
	file_eta_hits(337) := '0';
	file_eta_hits(340) := '0';
	file_eta_hits(342) := '0';
	file_eta_hits(345) := '0';
	file_eta_hits(347) := '0';
	file_eta_hits(350) := '0';
	file_eta_hits(353) := '0';
	file_eta_hits(355) := '0';
	file_eta_hits(357) := '0';
	file_eta_hits(358) := '0';
	file_eta_hits(359) := '0';
	file_eta_hits(362) := '0';
	file_eta_hits(364) := '0';
	file_eta_hits(367) := '0';
	file_eta_hits(370) := '0';
	file_eta_hits(372) := '0';
	file_eta_hits(375) := '0';
	file_eta_hits(377) := '0';
	file_eta_hits(380) := '0';
	file_eta_hits(383) := '0';
	file_eta_hits(385) := '0';
	file_eta_hits(388) := '0';
	file_eta_hits(390) := '0';
	file_eta_hits(393) := '0';
	file_eta_hits(396) := '0';
	file_eta_hits(398) := '0';
	file_eta_hits(401) := '0';
	file_eta_hits(403) := '0';
	file_eta_hits(406) := '0';
	file_eta_hits(409) := '0';
	file_eta_hits(411) := '0';
	file_eta_hits(414) := '0';
	file_eta_hits(416) := '0';
	file_eta_hits(419) := '0';
	file_eta_hits(422) := '0';
	file_eta_hits(424) := '0';
	file_eta_hits(425) := '0';
	file_eta_hits(426) := '0';
	file_eta_hits(428) := '0';
	file_eta_hits(430) := '0';
	file_eta_hits(433) := '0';
	file_eta_hits(435) := '0';
	file_eta_hits(438) := '0';
	file_eta_hits(440) := '0';
	file_eta_hits(443) := '0';
	file_eta_hits(445) := '0';
	file_eta_hits(448) := '0';
	file_eta_hits(450) := '0';
	file_eta_hits(453) := '0';
	file_eta_hits(455) := '0';
	file_eta_hits(458) := '0';
	file_eta_hits(460) := '0';
	file_eta_hits(463) := '0';
	file_eta_hits(465) := '0';
	file_eta_hits(467) := '0';
	file_eta_hits(469) := '0';
	file_eta_hits(472) := '0';
	file_eta_hits(474) := '0';
	file_eta_hits(477) := '0';
	file_eta_hits(479) := '0';
	file_eta_hits(481) := '0';
	file_eta_hits(484) := '0';
	file_eta_hits(486) := '0';
	file_eta_hits(489) := '0';
	file_eta_hits(491) := '0';
	file_eta_hits(494) := '0';
	file_eta_hits(496) := '0';
	file_eta_hits(499) := '0';
	file_eta_hits(501) := '0';
	file_eta_hits(504) := '0';
	file_eta_hits(506) := '0';
	file_eta_hits(508) := '0';
	file_eta_hits(509) := '0';
	file_eta_hits(510) := '0';
	file_eta_hits(511) := '0';

    return file_eta_hits;
  end function define_map_BM1;


function define_map_BM2 (signal file_eta_hits_input : in std_logic_vector(447 downto 0) ) 
    return t_inputs  is
    variable file_eta_hits : t_inputs;
    begin 



	file_eta_hits(23) := file_eta_hits_input(0);
	file_eta_hits(24) := file_eta_hits_input(1);
	file_eta_hits(26) := file_eta_hits_input(2);
	file_eta_hits(27) := file_eta_hits_input(3);
	file_eta_hits(29) := file_eta_hits_input(4);
	file_eta_hits(31) := file_eta_hits_input(5);
	file_eta_hits(32) := file_eta_hits_input(6);
	file_eta_hits(34) := file_eta_hits_input(7);
	file_eta_hits(35) := file_eta_hits_input(8);
	file_eta_hits(37) := file_eta_hits_input(9);
	file_eta_hits(40) := file_eta_hits_input(10);
	file_eta_hits(42) := file_eta_hits_input(11);
	file_eta_hits(43) := file_eta_hits_input(12);
	file_eta_hits(45) := file_eta_hits_input(13);
	file_eta_hits(48) := file_eta_hits_input(14);
	file_eta_hits(51) := file_eta_hits_input(15);
	file_eta_hits(53) := file_eta_hits_input(16);
	file_eta_hits(55) := file_eta_hits_input(17);
	file_eta_hits(56) := file_eta_hits_input(18);
	file_eta_hits(58) := file_eta_hits_input(19);
	file_eta_hits(60) := file_eta_hits_input(20);
	file_eta_hits(59) := file_eta_hits_input(21);
	file_eta_hits(61) := file_eta_hits_input(22);
	file_eta_hits(63) := file_eta_hits_input(23);
	file_eta_hits(64) := file_eta_hits_input(24);
	file_eta_hits(66) := file_eta_hits_input(25);
	file_eta_hits(68) := file_eta_hits_input(26);
	file_eta_hits(69) := file_eta_hits_input(27);
	file_eta_hits(71) := file_eta_hits_input(28);
	file_eta_hits(72) := file_eta_hits_input(29);
	file_eta_hits(74) := file_eta_hits_input(30);
	file_eta_hits(76) := file_eta_hits_input(31);
	file_eta_hits(77) := file_eta_hits_input(32);
	file_eta_hits(79) := file_eta_hits_input(33);
	file_eta_hits(80) := file_eta_hits_input(34);
	file_eta_hits(82) := file_eta_hits_input(35);
	file_eta_hits(84) := file_eta_hits_input(36);
	file_eta_hits(85) := file_eta_hits_input(37);
	file_eta_hits(87) := file_eta_hits_input(38);
	file_eta_hits(89) := file_eta_hits_input(39);
	file_eta_hits(90) := file_eta_hits_input(40);
	file_eta_hits(92) := file_eta_hits_input(41);
	file_eta_hits(93) := file_eta_hits_input(42);
	file_eta_hits(95) := file_eta_hits_input(43);
	file_eta_hits(97) := file_eta_hits_input(44);
	file_eta_hits(100) := file_eta_hits_input(45);
	file_eta_hits(101) := file_eta_hits_input(46);
	file_eta_hits(103) := file_eta_hits_input(47);
	file_eta_hits(104) := file_eta_hits_input(48);
	file_eta_hits(105) := file_eta_hits_input(49);
	file_eta_hits(107) := file_eta_hits_input(50);
	file_eta_hits(108) := file_eta_hits_input(51);
	file_eta_hits(110) := file_eta_hits_input(52);
	file_eta_hits(111) := file_eta_hits_input(53);
	file_eta_hits(113) := file_eta_hits_input(54);
	file_eta_hits(114) := file_eta_hits_input(55);
	file_eta_hits(115) := file_eta_hits_input(56);
	file_eta_hits(117) := file_eta_hits_input(57);
	file_eta_hits(118) := file_eta_hits_input(58);
	file_eta_hits(120) := file_eta_hits_input(59);
	file_eta_hits(121) := file_eta_hits_input(60);
	file_eta_hits(122) := file_eta_hits_input(61);
	file_eta_hits(124) := file_eta_hits_input(62);
	file_eta_hits(125) := file_eta_hits_input(63);
	file_eta_hits(127) := file_eta_hits_input(64);
	file_eta_hits(128) := file_eta_hits_input(65);
	file_eta_hits(129) := file_eta_hits_input(66);
	file_eta_hits(131) := file_eta_hits_input(67);
	file_eta_hits(132) := file_eta_hits_input(68);
	file_eta_hits(134) := file_eta_hits_input(69);
	file_eta_hits(135) := file_eta_hits_input(70);
	file_eta_hits(136) := file_eta_hits_input(71);
	file_eta_hits(138) := file_eta_hits_input(72);
	file_eta_hits(139) := file_eta_hits_input(73);
	file_eta_hits(141) := file_eta_hits_input(74);
	file_eta_hits(142) := file_eta_hits_input(75);
	file_eta_hits(143) := file_eta_hits_input(76);
	file_eta_hits(144) := file_eta_hits_input(77);
	file_eta_hits(145) := file_eta_hits_input(78);
	file_eta_hits(146) := file_eta_hits_input(79);
	file_eta_hits(148) := file_eta_hits_input(80);
	file_eta_hits(149) := file_eta_hits_input(81);
	file_eta_hits(151) := file_eta_hits_input(82);
	file_eta_hits(152) := file_eta_hits_input(83);
	file_eta_hits(153) := file_eta_hits_input(84);
	file_eta_hits(155) := file_eta_hits_input(85);
	file_eta_hits(156) := file_eta_hits_input(86);
	file_eta_hits(158) := file_eta_hits_input(87);
	file_eta_hits(159) := file_eta_hits_input(88);
	file_eta_hits(161) := file_eta_hits_input(89);
	file_eta_hits(162) := file_eta_hits_input(90);
	file_eta_hits(163) := file_eta_hits_input(91);
	file_eta_hits(165) := file_eta_hits_input(92);
	file_eta_hits(166) := file_eta_hits_input(93);
	file_eta_hits(168) := file_eta_hits_input(94);
	file_eta_hits(169) := file_eta_hits_input(95);
	file_eta_hits(170) := file_eta_hits_input(96);
	file_eta_hits(172) := file_eta_hits_input(97);
	file_eta_hits(173) := file_eta_hits_input(98);
	file_eta_hits(175) := file_eta_hits_input(99);
	file_eta_hits(176) := file_eta_hits_input(100);
	file_eta_hits(177) := file_eta_hits_input(101);
	file_eta_hits(179) := file_eta_hits_input(102);
	file_eta_hits(180) := file_eta_hits_input(103);
	file_eta_hits(182) := file_eta_hits_input(104);
	file_eta_hits(183) := file_eta_hits_input(105);
	file_eta_hits(186) := file_eta_hits_input(106);
	file_eta_hits(187) := file_eta_hits_input(107);
	file_eta_hits(190) := file_eta_hits_input(108);
	file_eta_hits(193) := file_eta_hits_input(109);
	file_eta_hits(194) := file_eta_hits_input(110);
	file_eta_hits(196) := file_eta_hits_input(111);
	file_eta_hits(197) := file_eta_hits_input(112);
	file_eta_hits(198) := file_eta_hits_input(113);
	file_eta_hits(200) := file_eta_hits_input(114);
	file_eta_hits(201) := file_eta_hits_input(115);
	file_eta_hits(203) := file_eta_hits_input(116);
	file_eta_hits(204) := file_eta_hits_input(117);
	file_eta_hits(205) := file_eta_hits_input(118);
	file_eta_hits(207) := file_eta_hits_input(119);
	file_eta_hits(208) := file_eta_hits_input(120);
	file_eta_hits(210) := file_eta_hits_input(121);
	file_eta_hits(211) := file_eta_hits_input(122);
	file_eta_hits(212) := file_eta_hits_input(123);
	file_eta_hits(214) := file_eta_hits_input(124);
	file_eta_hits(215) := file_eta_hits_input(125);
	file_eta_hits(217) := file_eta_hits_input(126);
	file_eta_hits(218) := file_eta_hits_input(127);
	file_eta_hits(219) := file_eta_hits_input(128);
	file_eta_hits(221) := file_eta_hits_input(129);
	file_eta_hits(222) := file_eta_hits_input(130);
	file_eta_hits(224) := file_eta_hits_input(131);
	file_eta_hits(225) := file_eta_hits_input(132);
	file_eta_hits(227) := file_eta_hits_input(133);
	file_eta_hits(228) := file_eta_hits_input(134);
	file_eta_hits(229) := file_eta_hits_input(135);
	file_eta_hits(231) := file_eta_hits_input(136);
	file_eta_hits(232) := file_eta_hits_input(137);
	file_eta_hits(234) := file_eta_hits_input(138);
	file_eta_hits(234) := file_eta_hits_input(139);
	file_eta_hits(236) := file_eta_hits_input(140);
	file_eta_hits(237) := file_eta_hits_input(141);
	file_eta_hits(238) := file_eta_hits_input(142);
	file_eta_hits(240) := file_eta_hits_input(143);
	file_eta_hits(241) := file_eta_hits_input(144);
	file_eta_hits(243) := file_eta_hits_input(145);
	file_eta_hits(244) := file_eta_hits_input(146);
	file_eta_hits(245) := file_eta_hits_input(147);
	file_eta_hits(247) := file_eta_hits_input(148);
	file_eta_hits(248) := file_eta_hits_input(149);
	file_eta_hits(250) := file_eta_hits_input(150);
	file_eta_hits(251) := file_eta_hits_input(151);
	file_eta_hits(253) := file_eta_hits_input(152);
	file_eta_hits(254) := file_eta_hits_input(153);
	file_eta_hits(255) := file_eta_hits_input(154);
	file_eta_hits(257) := file_eta_hits_input(155);
	file_eta_hits(258) := file_eta_hits_input(156);
	file_eta_hits(260) := file_eta_hits_input(157);
	file_eta_hits(261) := file_eta_hits_input(158);
	file_eta_hits(262) := file_eta_hits_input(159);
	file_eta_hits(264) := file_eta_hits_input(160);
	file_eta_hits(265) := file_eta_hits_input(161);
	file_eta_hits(267) := file_eta_hits_input(162);
	file_eta_hits(268) := file_eta_hits_input(163);
	file_eta_hits(271) := file_eta_hits_input(164);
	file_eta_hits(272) := file_eta_hits_input(165);
	file_eta_hits(274) := file_eta_hits_input(166);
	file_eta_hits(275) := file_eta_hits_input(167);
	file_eta_hits(277) := file_eta_hits_input(168);
	file_eta_hits(278) := file_eta_hits_input(169);
	file_eta_hits(282) := file_eta_hits_input(170);
	file_eta_hits(283) := file_eta_hits_input(171);
	file_eta_hits(285) := file_eta_hits_input(172);
	file_eta_hits(286) := file_eta_hits_input(173);
	file_eta_hits(288) := file_eta_hits_input(174);
	file_eta_hits(290) := file_eta_hits_input(175);
	file_eta_hits(291) := file_eta_hits_input(176);
	file_eta_hits(293) := file_eta_hits_input(177);
	file_eta_hits(294) := file_eta_hits_input(178);
	file_eta_hits(296) := file_eta_hits_input(179);
	file_eta_hits(297) := file_eta_hits_input(180);
	file_eta_hits(299) := file_eta_hits_input(181);
	file_eta_hits(301) := file_eta_hits_input(182);
	file_eta_hits(302) := file_eta_hits_input(183);
	file_eta_hits(304) := file_eta_hits_input(184);
	file_eta_hits(305) := file_eta_hits_input(185);
	file_eta_hits(307) := file_eta_hits_input(186);
	file_eta_hits(308) := file_eta_hits_input(187);
	file_eta_hits(310) := file_eta_hits_input(188);
	file_eta_hits(312) := file_eta_hits_input(189);
	file_eta_hits(313) := file_eta_hits_input(190);
	file_eta_hits(315) := file_eta_hits_input(191);
	file_eta_hits(316) := file_eta_hits_input(192);
	file_eta_hits(318) := file_eta_hits_input(193);
	file_eta_hits(319) := file_eta_hits_input(194);
	file_eta_hits(321) := file_eta_hits_input(195);
	file_eta_hits(322) := file_eta_hits_input(196);
	file_eta_hits(324) := file_eta_hits_input(197);
	file_eta_hits(326) := file_eta_hits_input(198);
	file_eta_hits(327) := file_eta_hits_input(199);
	file_eta_hits(329) := file_eta_hits_input(200);
	file_eta_hits(330) := file_eta_hits_input(201);
	file_eta_hits(332) := file_eta_hits_input(202);
	file_eta_hits(333) := file_eta_hits_input(203);
	file_eta_hits(335) := file_eta_hits_input(204);
	file_eta_hits(337) := file_eta_hits_input(205);
	file_eta_hits(338) := file_eta_hits_input(206);
	file_eta_hits(340) := file_eta_hits_input(207);
	file_eta_hits(341) := file_eta_hits_input(208);
	file_eta_hits(343) := file_eta_hits_input(209);
	file_eta_hits(347) := file_eta_hits_input(210);
	file_eta_hits(348) := file_eta_hits_input(211);
	file_eta_hits(350) := file_eta_hits_input(212);
	file_eta_hits(351) := file_eta_hits_input(213);
	file_eta_hits(353) := file_eta_hits_input(214);
	file_eta_hits(355) := file_eta_hits_input(215);
	file_eta_hits(356) := file_eta_hits_input(216);
	file_eta_hits(358) := file_eta_hits_input(217);
	file_eta_hits(359) := file_eta_hits_input(218);
	file_eta_hits(361) := file_eta_hits_input(219);
	file_eta_hits(362) := file_eta_hits_input(220);
	file_eta_hits(364) := file_eta_hits_input(221);
	file_eta_hits(366) := file_eta_hits_input(222);
	file_eta_hits(367) := file_eta_hits_input(223);
	file_eta_hits(369) := file_eta_hits_input(224);
	file_eta_hits(370) := file_eta_hits_input(225);
	file_eta_hits(372) := file_eta_hits_input(226);
	file_eta_hits(373) := file_eta_hits_input(227);
	file_eta_hits(375) := file_eta_hits_input(228);
	file_eta_hits(377) := file_eta_hits_input(229);
	file_eta_hits(378) := file_eta_hits_input(230);
	file_eta_hits(380) := file_eta_hits_input(231);
	file_eta_hits(381) := file_eta_hits_input(232);
	file_eta_hits(383) := file_eta_hits_input(233);
	file_eta_hits(384) := file_eta_hits_input(234);
	file_eta_hits(386) := file_eta_hits_input(235);
	file_eta_hits(387) := file_eta_hits_input(236);
	file_eta_hits(389) := file_eta_hits_input(237);
	file_eta_hits(391) := file_eta_hits_input(238);
	file_eta_hits(392) := file_eta_hits_input(239);
	file_eta_hits(394) := file_eta_hits_input(240);
	file_eta_hits(395) := file_eta_hits_input(241);
	file_eta_hits(397) := file_eta_hits_input(242);
	file_eta_hits(398) := file_eta_hits_input(243);
	file_eta_hits(400) := file_eta_hits_input(244);
	file_eta_hits(402) := file_eta_hits_input(245);
	file_eta_hits(403) := file_eta_hits_input(246);
	file_eta_hits(405) := file_eta_hits_input(247);
	file_eta_hits(406) := file_eta_hits_input(248);
	file_eta_hits(408) := file_eta_hits_input(249);
	file_eta_hits(410) := file_eta_hits_input(250);
	file_eta_hits(411) := file_eta_hits_input(251);
	file_eta_hits(413) := file_eta_hits_input(252);
	file_eta_hits(414) := file_eta_hits_input(253);
	file_eta_hits(416) := file_eta_hits_input(254);
	file_eta_hits(418) := file_eta_hits_input(255);
	file_eta_hits(419) := file_eta_hits_input(256);
	file_eta_hits(421) := file_eta_hits_input(257);
	file_eta_hits(422) := file_eta_hits_input(258);
	file_eta_hits(424) := file_eta_hits_input(259);
	file_eta_hits(426) := file_eta_hits_input(260);
	file_eta_hits(427) := file_eta_hits_input(261);
	file_eta_hits(429) := file_eta_hits_input(262);
	file_eta_hits(430) := file_eta_hits_input(263);
	file_eta_hits(432) := file_eta_hits_input(264);
	file_eta_hits(434) := file_eta_hits_input(265);
	file_eta_hits(435) := file_eta_hits_input(266);
	file_eta_hits(437) := file_eta_hits_input(267);
	file_eta_hits(438) := file_eta_hits_input(268);
	file_eta_hits(440) := file_eta_hits_input(269);
	file_eta_hits(442) := file_eta_hits_input(270);
	file_eta_hits(443) := file_eta_hits_input(271);
	file_eta_hits(445) := file_eta_hits_input(272);
	file_eta_hits(447) := file_eta_hits_input(273);
	file_eta_hits(449) := file_eta_hits_input(274);
	file_eta_hits(450) := file_eta_hits_input(275);
	file_eta_hits(452) := file_eta_hits_input(276);
	file_eta_hits(453) := file_eta_hits_input(277);
	file_eta_hits(455) := file_eta_hits_input(278);
	file_eta_hits(457) := file_eta_hits_input(279);
	file_eta_hits(458) := file_eta_hits_input(280);
	file_eta_hits(460) := file_eta_hits_input(281);
	file_eta_hits(461) := file_eta_hits_input(282);
	file_eta_hits(463) := file_eta_hits_input(283);
	file_eta_hits(465) := file_eta_hits_input(284);
	file_eta_hits(466) := file_eta_hits_input(285);
	file_eta_hits(468) := file_eta_hits_input(286);
	file_eta_hits(470) := file_eta_hits_input(287);
	file_eta_hits(471) := file_eta_hits_input(288);
	file_eta_hits(473) := file_eta_hits_input(289);
	file_eta_hits(474) := file_eta_hits_input(290);
	file_eta_hits(476) := file_eta_hits_input(291);
	file_eta_hits(478) := file_eta_hits_input(292);
	file_eta_hits(479) := file_eta_hits_input(293);
	file_eta_hits(481) := file_eta_hits_input(294);
	file_eta_hits(482) := file_eta_hits_input(295);
	file_eta_hits(484) := file_eta_hits_input(296);
	file_eta_hits(486) := file_eta_hits_input(297);
	file_eta_hits(488) := file_eta_hits_input(298);
	file_eta_hits(490) := file_eta_hits_input(299);
	file_eta_hits(491) := file_eta_hits_input(300);
	file_eta_hits(493) := file_eta_hits_input(301);
	file_eta_hits(494) := file_eta_hits_input(302);
	file_eta_hits(496) := file_eta_hits_input(303);
	file_eta_hits(497) := file_eta_hits_input(304);
	file_eta_hits(499) := file_eta_hits_input(305);
	file_eta_hits(500) := file_eta_hits_input(306);
	file_eta_hits(502) := file_eta_hits_input(307);
	file_eta_hits(503) := file_eta_hits_input(308);
	file_eta_hits(505) := file_eta_hits_input(309);
	file_eta_hits(506) := file_eta_hits_input(310);
	file_eta_hits(508) := file_eta_hits_input(311);
	file_eta_hits(509) := file_eta_hits_input(312);
	file_eta_hits(511) := file_eta_hits_input(313);
	file_eta_hits(0) := '0';
	file_eta_hits(1) := '0';
	file_eta_hits(2) := '0';
	file_eta_hits(3) := '0';
	file_eta_hits(4) := '0';
	file_eta_hits(5) := '0';
	file_eta_hits(6) := '0';
	file_eta_hits(7) := '0';
	file_eta_hits(8) := '0';
	file_eta_hits(9) := '0';
	file_eta_hits(10) := '0';
	file_eta_hits(11) := '0';
	file_eta_hits(12) := '0';
	file_eta_hits(13) := '0';
	file_eta_hits(14) := '0';
	file_eta_hits(15) := '0';
	file_eta_hits(16) := '0';
	file_eta_hits(17) := '0';
	file_eta_hits(18) := '0';
	file_eta_hits(19) := '0';
	file_eta_hits(20) := '0';
	file_eta_hits(21) := '0';
	file_eta_hits(22) := '0';
	file_eta_hits(25) := '0';
	file_eta_hits(28) := '0';
	file_eta_hits(30) := '0';
	file_eta_hits(33) := '0';
	file_eta_hits(36) := '0';
	file_eta_hits(38) := '0';
	file_eta_hits(39) := '0';
	file_eta_hits(41) := '0';
	file_eta_hits(44) := '0';
	file_eta_hits(46) := '0';
	file_eta_hits(47) := '0';
	file_eta_hits(49) := '0';
	file_eta_hits(50) := '0';
	file_eta_hits(52) := '0';
	file_eta_hits(54) := '0';
	file_eta_hits(57) := '0';
	file_eta_hits(62) := '0';
	file_eta_hits(65) := '0';
	file_eta_hits(67) := '0';
	file_eta_hits(70) := '0';
	file_eta_hits(73) := '0';
	file_eta_hits(75) := '0';
	file_eta_hits(78) := '0';
	file_eta_hits(81) := '0';
	file_eta_hits(83) := '0';
	file_eta_hits(86) := '0';
	file_eta_hits(88) := '0';
	file_eta_hits(91) := '0';
	file_eta_hits(94) := '0';
	file_eta_hits(96) := '0';
	file_eta_hits(98) := '0';
	file_eta_hits(99) := '0';
	file_eta_hits(102) := '0';
	file_eta_hits(106) := '0';
	file_eta_hits(109) := '0';
	file_eta_hits(112) := '0';
	file_eta_hits(116) := '0';
	file_eta_hits(119) := '0';
	file_eta_hits(123) := '0';
	file_eta_hits(126) := '0';
	file_eta_hits(130) := '0';
	file_eta_hits(133) := '0';
	file_eta_hits(137) := '0';
	file_eta_hits(140) := '0';
	file_eta_hits(147) := '0';
	file_eta_hits(150) := '0';
	file_eta_hits(154) := '0';
	file_eta_hits(157) := '0';
	file_eta_hits(160) := '0';
	file_eta_hits(164) := '0';
	file_eta_hits(167) := '0';
	file_eta_hits(171) := '0';
	file_eta_hits(174) := '0';
	file_eta_hits(178) := '0';
	file_eta_hits(181) := '0';
	file_eta_hits(184) := '0';
	file_eta_hits(185) := '0';
	file_eta_hits(188) := '0';
	file_eta_hits(189) := '0';
	file_eta_hits(191) := '0';
	file_eta_hits(192) := '0';
	file_eta_hits(195) := '0';
	file_eta_hits(199) := '0';
	file_eta_hits(202) := '0';
	file_eta_hits(206) := '0';
	file_eta_hits(209) := '0';
	file_eta_hits(213) := '0';
	file_eta_hits(216) := '0';
	file_eta_hits(220) := '0';
	file_eta_hits(223) := '0';
	file_eta_hits(226) := '0';
	file_eta_hits(230) := '0';
	file_eta_hits(233) := '0';
	file_eta_hits(235) := '0';
	file_eta_hits(239) := '0';
	file_eta_hits(242) := '0';
	file_eta_hits(246) := '0';
	file_eta_hits(249) := '0';
	file_eta_hits(252) := '0';
	file_eta_hits(256) := '0';
	file_eta_hits(259) := '0';
	file_eta_hits(263) := '0';
	file_eta_hits(266) := '0';
	file_eta_hits(269) := '0';
	file_eta_hits(270) := '0';
	file_eta_hits(273) := '0';
	file_eta_hits(276) := '0';
	file_eta_hits(279) := '0';
	file_eta_hits(280) := '0';
	file_eta_hits(281) := '0';
	file_eta_hits(284) := '0';
	file_eta_hits(287) := '0';
	file_eta_hits(289) := '0';
	file_eta_hits(292) := '0';
	file_eta_hits(295) := '0';
	file_eta_hits(298) := '0';
	file_eta_hits(300) := '0';
	file_eta_hits(303) := '0';
	file_eta_hits(306) := '0';
	file_eta_hits(309) := '0';
	file_eta_hits(311) := '0';
	file_eta_hits(314) := '0';
	file_eta_hits(317) := '0';
	file_eta_hits(320) := '0';
	file_eta_hits(323) := '0';
	file_eta_hits(325) := '0';
	file_eta_hits(328) := '0';
	file_eta_hits(331) := '0';
	file_eta_hits(334) := '0';
	file_eta_hits(336) := '0';
	file_eta_hits(339) := '0';
	file_eta_hits(342) := '0';
	file_eta_hits(344) := '0';
	file_eta_hits(345) := '0';
	file_eta_hits(346) := '0';
	file_eta_hits(349) := '0';
	file_eta_hits(352) := '0';
	file_eta_hits(354) := '0';
	file_eta_hits(357) := '0';
	file_eta_hits(360) := '0';
	file_eta_hits(363) := '0';
	file_eta_hits(365) := '0';
	file_eta_hits(368) := '0';
	file_eta_hits(371) := '0';
	file_eta_hits(374) := '0';
	file_eta_hits(376) := '0';
	file_eta_hits(379) := '0';
	file_eta_hits(382) := '0';
	file_eta_hits(385) := '0';
	file_eta_hits(388) := '0';
	file_eta_hits(390) := '0';
	file_eta_hits(393) := '0';
	file_eta_hits(396) := '0';
	file_eta_hits(399) := '0';
	file_eta_hits(401) := '0';
	file_eta_hits(404) := '0';
	file_eta_hits(407) := '0';
	file_eta_hits(409) := '0';
	file_eta_hits(412) := '0';
	file_eta_hits(415) := '0';
	file_eta_hits(417) := '0';
	file_eta_hits(420) := '0';
	file_eta_hits(423) := '0';
	file_eta_hits(425) := '0';
	file_eta_hits(428) := '0';
	file_eta_hits(431) := '0';
	file_eta_hits(433) := '0';
	file_eta_hits(436) := '0';
	file_eta_hits(439) := '0';
	file_eta_hits(441) := '0';
	file_eta_hits(444) := '0';
	file_eta_hits(446) := '0';
	file_eta_hits(448) := '0';
	file_eta_hits(451) := '0';
	file_eta_hits(454) := '0';
	file_eta_hits(456) := '0';
	file_eta_hits(459) := '0';
	file_eta_hits(462) := '0';
	file_eta_hits(464) := '0';
	file_eta_hits(467) := '0';
	file_eta_hits(469) := '0';
	file_eta_hits(472) := '0';
	file_eta_hits(475) := '0';
	file_eta_hits(477) := '0';
	file_eta_hits(480) := '0';
	file_eta_hits(483) := '0';
	file_eta_hits(485) := '0';
	file_eta_hits(487) := '0';
	file_eta_hits(489) := '0';
	file_eta_hits(492) := '0';
	file_eta_hits(495) := '0';
	file_eta_hits(498) := '0';
	file_eta_hits(501) := '0';
	file_eta_hits(504) := '0';
	file_eta_hits(507) := '0';
	file_eta_hits(510) := '0';

    return file_eta_hits;
  end function define_map_BM2;


function define_map_BO (signal file_eta_hits_input : in std_logic_vector(383 downto 0) ) 
    return t_inputs is
    variable file_eta_hits : t_inputs;
    begin 


	file_eta_hits(18) := file_eta_hits_input(0);
	file_eta_hits(19) := file_eta_hits_input(1);
	file_eta_hits(21) := file_eta_hits_input(2);
	file_eta_hits(22) := file_eta_hits_input(3);
	file_eta_hits(24) := file_eta_hits_input(4);
	file_eta_hits(25) := file_eta_hits_input(5);
	file_eta_hits(27) := file_eta_hits_input(6);
	file_eta_hits(28) := file_eta_hits_input(7);
	file_eta_hits(30) := file_eta_hits_input(8);
	file_eta_hits(31) := file_eta_hits_input(9);
	file_eta_hits(33) := file_eta_hits_input(10);
	file_eta_hits(34) := file_eta_hits_input(11);
	file_eta_hits(36) := file_eta_hits_input(12);
	file_eta_hits(37) := file_eta_hits_input(13);
	file_eta_hits(39) := file_eta_hits_input(14);
	file_eta_hits(40) := file_eta_hits_input(15);
	file_eta_hits(41) := file_eta_hits_input(16);
	file_eta_hits(43) := file_eta_hits_input(17);
	file_eta_hits(44) := file_eta_hits_input(18);
	file_eta_hits(46) := file_eta_hits_input(19);
	file_eta_hits(47) := file_eta_hits_input(20);
	file_eta_hits(49) := file_eta_hits_input(21);
	file_eta_hits(50) := file_eta_hits_input(22);
	file_eta_hits(52) := file_eta_hits_input(23);
	file_eta_hits(53) := file_eta_hits_input(24);
	file_eta_hits(55) := file_eta_hits_input(25);
	file_eta_hits(56) := file_eta_hits_input(26);
	file_eta_hits(58) := file_eta_hits_input(27);
	file_eta_hits(59) := file_eta_hits_input(28);
	file_eta_hits(60) := file_eta_hits_input(29);
	file_eta_hits(62) := file_eta_hits_input(30);
	file_eta_hits(63) := file_eta_hits_input(31);
	file_eta_hits(65) := file_eta_hits_input(32);
	file_eta_hits(66) := file_eta_hits_input(33);
	file_eta_hits(67) := file_eta_hits_input(34);
	file_eta_hits(69) := file_eta_hits_input(35);
	file_eta_hits(70) := file_eta_hits_input(36);
	file_eta_hits(72) := file_eta_hits_input(37);
	file_eta_hits(73) := file_eta_hits_input(38);
	file_eta_hits(75) := file_eta_hits_input(39);
	file_eta_hits(76) := file_eta_hits_input(40);
	file_eta_hits(77) := file_eta_hits_input(41);
	file_eta_hits(79) := file_eta_hits_input(42);
	file_eta_hits(82) := file_eta_hits_input(43);
	file_eta_hits(83) := file_eta_hits_input(44);
	file_eta_hits(85) := file_eta_hits_input(45);
	file_eta_hits(86) := file_eta_hits_input(46);
	file_eta_hits(87) := file_eta_hits_input(47);
	file_eta_hits(89) := file_eta_hits_input(48);
	file_eta_hits(90) := file_eta_hits_input(49);
	file_eta_hits(92) := file_eta_hits_input(50);
	file_eta_hits(93) := file_eta_hits_input(51);
	file_eta_hits(95) := file_eta_hits_input(52);
	file_eta_hits(96) := file_eta_hits_input(53);
	file_eta_hits(99) := file_eta_hits_input(54);
	file_eta_hits(100) := file_eta_hits_input(55);
	file_eta_hits(102) := file_eta_hits_input(56);
	file_eta_hits(103) := file_eta_hits_input(57);
	file_eta_hits(104) := file_eta_hits_input(58);
	file_eta_hits(106) := file_eta_hits_input(59);
	file_eta_hits(107) := file_eta_hits_input(60);
	file_eta_hits(109) := file_eta_hits_input(61);
	file_eta_hits(110) := file_eta_hits_input(62);
	file_eta_hits(111) := file_eta_hits_input(63);
	file_eta_hits(113) := file_eta_hits_input(64);
	file_eta_hits(114) := file_eta_hits_input(65);
	file_eta_hits(116) := file_eta_hits_input(66);
	file_eta_hits(117) := file_eta_hits_input(67);
	file_eta_hits(119) := file_eta_hits_input(68);
	file_eta_hits(120) := file_eta_hits_input(69);
	file_eta_hits(121) := file_eta_hits_input(70);
	file_eta_hits(123) := file_eta_hits_input(71);
	file_eta_hits(124) := file_eta_hits_input(72);
	file_eta_hits(126) := file_eta_hits_input(73);
	file_eta_hits(127) := file_eta_hits_input(74);
	file_eta_hits(128) := file_eta_hits_input(75);
	file_eta_hits(130) := file_eta_hits_input(76);
	file_eta_hits(131) := file_eta_hits_input(77);
	file_eta_hits(133) := file_eta_hits_input(78);
	file_eta_hits(134) := file_eta_hits_input(79);
	file_eta_hits(136) := file_eta_hits_input(80);
	file_eta_hits(137) := file_eta_hits_input(81);
	file_eta_hits(138) := file_eta_hits_input(82);
	file_eta_hits(140) := file_eta_hits_input(83);
	file_eta_hits(141) := file_eta_hits_input(84);
	file_eta_hits(143) := file_eta_hits_input(85);
	file_eta_hits(143) := file_eta_hits_input(86);
	file_eta_hits(144) := file_eta_hits_input(87);
	file_eta_hits(146) := file_eta_hits_input(88);
	file_eta_hits(149) := file_eta_hits_input(89);
	file_eta_hits(150) := file_eta_hits_input(90);
	file_eta_hits(152) := file_eta_hits_input(91);
	file_eta_hits(153) := file_eta_hits_input(92);
	file_eta_hits(154) := file_eta_hits_input(93);
	file_eta_hits(156) := file_eta_hits_input(94);
	file_eta_hits(157) := file_eta_hits_input(95);
	file_eta_hits(159) := file_eta_hits_input(96);
	file_eta_hits(160) := file_eta_hits_input(97);
	file_eta_hits(161) := file_eta_hits_input(98);
	file_eta_hits(163) := file_eta_hits_input(99);
	file_eta_hits(164) := file_eta_hits_input(100);
	file_eta_hits(166) := file_eta_hits_input(101);
	file_eta_hits(167) := file_eta_hits_input(102);
	file_eta_hits(169) := file_eta_hits_input(103);
	file_eta_hits(170) := file_eta_hits_input(104);
	file_eta_hits(171) := file_eta_hits_input(105);
	file_eta_hits(173) := file_eta_hits_input(106);
	file_eta_hits(174) := file_eta_hits_input(107);
	file_eta_hits(176) := file_eta_hits_input(108);
	file_eta_hits(177) := file_eta_hits_input(109);
	file_eta_hits(179) := file_eta_hits_input(110);
	file_eta_hits(180) := file_eta_hits_input(111);
	file_eta_hits(181) := file_eta_hits_input(112);
	file_eta_hits(183) := file_eta_hits_input(113);
	file_eta_hits(184) := file_eta_hits_input(114);
	file_eta_hits(186) := file_eta_hits_input(115);
	file_eta_hits(187) := file_eta_hits_input(116);
	file_eta_hits(190) := file_eta_hits_input(117);
	file_eta_hits(191) := file_eta_hits_input(118);
	file_eta_hits(192) := file_eta_hits_input(119);
	file_eta_hits(194) := file_eta_hits_input(120);
	file_eta_hits(195) := file_eta_hits_input(121);
	file_eta_hits(197) := file_eta_hits_input(122);
	file_eta_hits(198) := file_eta_hits_input(123);
	file_eta_hits(200) := file_eta_hits_input(124);
	file_eta_hits(201) := file_eta_hits_input(125);
	file_eta_hits(203) := file_eta_hits_input(126);
	file_eta_hits(204) := file_eta_hits_input(127);
	file_eta_hits(206) := file_eta_hits_input(128);
	file_eta_hits(207) := file_eta_hits_input(129);
	file_eta_hits(209) := file_eta_hits_input(130);
	file_eta_hits(210) := file_eta_hits_input(131);
	file_eta_hits(212) := file_eta_hits_input(132);
	file_eta_hits(213) := file_eta_hits_input(133);
	file_eta_hits(215) := file_eta_hits_input(134);
	file_eta_hits(216) := file_eta_hits_input(135);
	file_eta_hits(217) := file_eta_hits_input(136);
	file_eta_hits(219) := file_eta_hits_input(137);
	file_eta_hits(220) := file_eta_hits_input(138);
	file_eta_hits(222) := file_eta_hits_input(139);
	file_eta_hits(223) := file_eta_hits_input(140);
	file_eta_hits(224) := file_eta_hits_input(141);
	file_eta_hits(226) := file_eta_hits_input(142);
	file_eta_hits(227) := file_eta_hits_input(143);
	file_eta_hits(229) := file_eta_hits_input(144);
	file_eta_hits(230) := file_eta_hits_input(145);
	file_eta_hits(232) := file_eta_hits_input(146);
	file_eta_hits(233) := file_eta_hits_input(147);
	file_eta_hits(235) := file_eta_hits_input(148);
	file_eta_hits(236) := file_eta_hits_input(149);
	file_eta_hits(237) := file_eta_hits_input(150);
	file_eta_hits(239) := file_eta_hits_input(151);
	file_eta_hits(240) := file_eta_hits_input(152);
	file_eta_hits(242) := file_eta_hits_input(153);
	file_eta_hits(243) := file_eta_hits_input(154);
	file_eta_hits(245) := file_eta_hits_input(155);
	file_eta_hits(246) := file_eta_hits_input(156);
	file_eta_hits(248) := file_eta_hits_input(157);
	file_eta_hits(249) := file_eta_hits_input(158);
	file_eta_hits(251) := file_eta_hits_input(159);
	file_eta_hits(252) := file_eta_hits_input(160);
	file_eta_hits(254) := file_eta_hits_input(161);
	file_eta_hits(255) := file_eta_hits_input(162);
	file_eta_hits(257) := file_eta_hits_input(163);
	file_eta_hits(258) := file_eta_hits_input(164);
	file_eta_hits(260) := file_eta_hits_input(165);
	file_eta_hits(262) := file_eta_hits_input(166);
	file_eta_hits(263) := file_eta_hits_input(167);
	file_eta_hits(265) := file_eta_hits_input(168);
	file_eta_hits(266) := file_eta_hits_input(169);
	file_eta_hits(267) := file_eta_hits_input(170);
	file_eta_hits(269) := file_eta_hits_input(171);
	file_eta_hits(270) := file_eta_hits_input(172);
	file_eta_hits(272) := file_eta_hits_input(173);
	file_eta_hits(273) := file_eta_hits_input(174);
	file_eta_hits(274) := file_eta_hits_input(175);
	file_eta_hits(276) := file_eta_hits_input(176);
	file_eta_hits(277) := file_eta_hits_input(177);
	file_eta_hits(279) := file_eta_hits_input(178);
	file_eta_hits(280) := file_eta_hits_input(179);
	file_eta_hits(282) := file_eta_hits_input(180);
	file_eta_hits(283) := file_eta_hits_input(181);
	file_eta_hits(284) := file_eta_hits_input(182);
	file_eta_hits(286) := file_eta_hits_input(183);
	file_eta_hits(287) := file_eta_hits_input(184);
	file_eta_hits(289) := file_eta_hits_input(185);
	file_eta_hits(290) := file_eta_hits_input(186);
	file_eta_hits(291) := file_eta_hits_input(187);
	file_eta_hits(293) := file_eta_hits_input(188);
	file_eta_hits(294) := file_eta_hits_input(189);
	file_eta_hits(296) := file_eta_hits_input(190);
	file_eta_hits(297) := file_eta_hits_input(191);
	file_eta_hits(299) := file_eta_hits_input(192);
	file_eta_hits(300) := file_eta_hits_input(193);
	file_eta_hits(301) := file_eta_hits_input(194);
	file_eta_hits(303) := file_eta_hits_input(195);
	file_eta_hits(304) := file_eta_hits_input(196);
	file_eta_hits(305) := file_eta_hits_input(197);
	file_eta_hits(307) := file_eta_hits_input(198);
	file_eta_hits(308) := file_eta_hits_input(199);
	file_eta_hits(310) := file_eta_hits_input(200);
	file_eta_hits(311) := file_eta_hits_input(201);
	file_eta_hits(312) := file_eta_hits_input(202);
	file_eta_hits(314) := file_eta_hits_input(203);
	file_eta_hits(315) := file_eta_hits_input(204);
	file_eta_hits(317) := file_eta_hits_input(205);
	file_eta_hits(318) := file_eta_hits_input(206);
	file_eta_hits(319) := file_eta_hits_input(207);
	file_eta_hits(321) := file_eta_hits_input(208);
	file_eta_hits(322) := file_eta_hits_input(209);
	file_eta_hits(324) := file_eta_hits_input(210);
	file_eta_hits(325) := file_eta_hits_input(211);
	file_eta_hits(327) := file_eta_hits_input(212);
	file_eta_hits(328) := file_eta_hits_input(213);
	file_eta_hits(329) := file_eta_hits_input(214);
	file_eta_hits(331) := file_eta_hits_input(215);
	file_eta_hits(332) := file_eta_hits_input(216);
	file_eta_hits(334) := file_eta_hits_input(217);
	file_eta_hits(335) := file_eta_hits_input(218);
	file_eta_hits(337) := file_eta_hits_input(219);
	file_eta_hits(338) := file_eta_hits_input(220);
	file_eta_hits(339) := file_eta_hits_input(221);
	file_eta_hits(341) := file_eta_hits_input(222);
	file_eta_hits(342) := file_eta_hits_input(223);
	file_eta_hits(344) := file_eta_hits_input(224);
	file_eta_hits(345) := file_eta_hits_input(225);
	file_eta_hits(346) := file_eta_hits_input(226);
	file_eta_hits(348) := file_eta_hits_input(227);
	file_eta_hits(349) := file_eta_hits_input(228);
	file_eta_hits(351) := file_eta_hits_input(229);
	file_eta_hits(352) := file_eta_hits_input(230);
	file_eta_hits(354) := file_eta_hits_input(231);
	file_eta_hits(355) := file_eta_hits_input(232);
	file_eta_hits(357) := file_eta_hits_input(233);
	file_eta_hits(358) := file_eta_hits_input(234);
	file_eta_hits(360) := file_eta_hits_input(235);
	file_eta_hits(361) := file_eta_hits_input(236);
	file_eta_hits(362) := file_eta_hits_input(237);
	file_eta_hits(364) := file_eta_hits_input(238);
	file_eta_hits(365) := file_eta_hits_input(239);
	file_eta_hits(367) := file_eta_hits_input(240);
	file_eta_hits(368) := file_eta_hits_input(241);
	file_eta_hits(369) := file_eta_hits_input(242);
	file_eta_hits(371) := file_eta_hits_input(243);
	file_eta_hits(372) := file_eta_hits_input(244);
	file_eta_hits(374) := file_eta_hits_input(245);
	file_eta_hits(375) := file_eta_hits_input(246);
	file_eta_hits(377) := file_eta_hits_input(247);
	file_eta_hits(378) := file_eta_hits_input(248);
	file_eta_hits(379) := file_eta_hits_input(249);
	file_eta_hits(381) := file_eta_hits_input(250);
	file_eta_hits(382) := file_eta_hits_input(251);
	file_eta_hits(384) := file_eta_hits_input(252);
	file_eta_hits(385) := file_eta_hits_input(253);
	file_eta_hits(386) := file_eta_hits_input(254);
	file_eta_hits(388) := file_eta_hits_input(255);
	file_eta_hits(389) := file_eta_hits_input(256);
	file_eta_hits(391) := file_eta_hits_input(257);
	file_eta_hits(392) := file_eta_hits_input(258);
	file_eta_hits(394) := file_eta_hits_input(259);
	file_eta_hits(395) := file_eta_hits_input(260);
	file_eta_hits(396) := file_eta_hits_input(261);
	file_eta_hits(398) := file_eta_hits_input(262);
	file_eta_hits(399) := file_eta_hits_input(263);
	file_eta_hits(401) := file_eta_hits_input(264);
	file_eta_hits(402) := file_eta_hits_input(265);
	file_eta_hits(404) := file_eta_hits_input(266);
	file_eta_hits(405) := file_eta_hits_input(267);
	file_eta_hits(406) := file_eta_hits_input(268);
	file_eta_hits(408) := file_eta_hits_input(269);
	file_eta_hits(409) := file_eta_hits_input(270);
	file_eta_hits(411) := file_eta_hits_input(271);
	file_eta_hits(412) := file_eta_hits_input(272);
	file_eta_hits(413) := file_eta_hits_input(273);
	file_eta_hits(415) := file_eta_hits_input(274);
	file_eta_hits(416) := file_eta_hits_input(275);
	file_eta_hits(418) := file_eta_hits_input(276);
	file_eta_hits(419) := file_eta_hits_input(277);
	file_eta_hits(421) := file_eta_hits_input(278);
	file_eta_hits(422) := file_eta_hits_input(279);
	file_eta_hits(423) := file_eta_hits_input(280);
	file_eta_hits(425) := file_eta_hits_input(281);
	file_eta_hits(426) := file_eta_hits_input(282);
	file_eta_hits(428) := file_eta_hits_input(283);
	file_eta_hits(429) := file_eta_hits_input(284);
	file_eta_hits(431) := file_eta_hits_input(285);
	file_eta_hits(432) := file_eta_hits_input(286);
	file_eta_hits(433) := file_eta_hits_input(287);
	file_eta_hits(435) := file_eta_hits_input(288);
	file_eta_hits(436) := file_eta_hits_input(289);
	file_eta_hits(438) := file_eta_hits_input(290);
	file_eta_hits(439) := file_eta_hits_input(291);
	file_eta_hits(440) := file_eta_hits_input(292);
	file_eta_hits(442) := file_eta_hits_input(293);
	file_eta_hits(443) := file_eta_hits_input(294);
	file_eta_hits(445) := file_eta_hits_input(295);
	file_eta_hits(446) := file_eta_hits_input(296);
	file_eta_hits(448) := file_eta_hits_input(297);
	file_eta_hits(449) := file_eta_hits_input(298);
	file_eta_hits(451) := file_eta_hits_input(299);
	file_eta_hits(452) := file_eta_hits_input(300);
	file_eta_hits(454) := file_eta_hits_input(301);
	file_eta_hits(455) := file_eta_hits_input(302);
	file_eta_hits(457) := file_eta_hits_input(303);
	file_eta_hits(458) := file_eta_hits_input(304);
	file_eta_hits(460) := file_eta_hits_input(305);
	file_eta_hits(461) := file_eta_hits_input(306);
	file_eta_hits(462) := file_eta_hits_input(307);
	file_eta_hits(464) := file_eta_hits_input(308);
	file_eta_hits(465) := file_eta_hits_input(309);
	file_eta_hits(467) := file_eta_hits_input(310);
	file_eta_hits(468) := file_eta_hits_input(311);
	file_eta_hits(470) := file_eta_hits_input(312);
	file_eta_hits(471) := file_eta_hits_input(313);
	file_eta_hits(473) := file_eta_hits_input(314);
	file_eta_hits(474) := file_eta_hits_input(315);
	file_eta_hits(476) := file_eta_hits_input(316);
	file_eta_hits(478) := file_eta_hits_input(317);
	file_eta_hits(479) := file_eta_hits_input(318);
	file_eta_hits(481) := file_eta_hits_input(319);
	file_eta_hits(482) := file_eta_hits_input(320);
	file_eta_hits(483) := file_eta_hits_input(321);
	file_eta_hits(485) := file_eta_hits_input(322);
	file_eta_hits(486) := file_eta_hits_input(323);
	file_eta_hits(488) := file_eta_hits_input(324);
	file_eta_hits(489) := file_eta_hits_input(325);
	file_eta_hits(491) := file_eta_hits_input(326);
	file_eta_hits(492) := file_eta_hits_input(327);
	file_eta_hits(494) := file_eta_hits_input(328);
	file_eta_hits(495) := file_eta_hits_input(329);
	file_eta_hits(497) := file_eta_hits_input(330);
	file_eta_hits(498) := file_eta_hits_input(331);
	file_eta_hits(500) := file_eta_hits_input(332);
	file_eta_hits(501) := file_eta_hits_input(333);
	file_eta_hits(503) := file_eta_hits_input(334);
	file_eta_hits(504) := file_eta_hits_input(335);
	file_eta_hits(506) := file_eta_hits_input(336);
	file_eta_hits(507) := file_eta_hits_input(337);
	file_eta_hits(509) := file_eta_hits_input(338);
	file_eta_hits(510) := file_eta_hits_input(339);
	file_eta_hits(0) := '0';
	file_eta_hits(1) := '0';
	file_eta_hits(2) := '0';
	file_eta_hits(3) := '0';
	file_eta_hits(4) := '0';
	file_eta_hits(5) := '0';
	file_eta_hits(6) := '0';
	file_eta_hits(7) := '0';
	file_eta_hits(8) := '0';
	file_eta_hits(9) := '0';
	file_eta_hits(10) := '0';
	file_eta_hits(11) := '0';
	file_eta_hits(12) := '0';
	file_eta_hits(13) := '0';
	file_eta_hits(14) := '0';
	file_eta_hits(15) := '0';
	file_eta_hits(16) := '0';
	file_eta_hits(17) := '0';
	file_eta_hits(20) := '0';
	file_eta_hits(23) := '0';
	file_eta_hits(26) := '0';
	file_eta_hits(29) := '0';
	file_eta_hits(32) := '0';
	file_eta_hits(35) := '0';
	file_eta_hits(38) := '0';
	file_eta_hits(42) := '0';
	file_eta_hits(45) := '0';
	file_eta_hits(48) := '0';
	file_eta_hits(51) := '0';
	file_eta_hits(54) := '0';
	file_eta_hits(57) := '0';
	file_eta_hits(61) := '0';
	file_eta_hits(64) := '0';
	file_eta_hits(68) := '0';
	file_eta_hits(71) := '0';
	file_eta_hits(74) := '0';
	file_eta_hits(78) := '0';
	file_eta_hits(80) := '0';
	file_eta_hits(81) := '0';
	file_eta_hits(84) := '0';
	file_eta_hits(88) := '0';
	file_eta_hits(91) := '0';
	file_eta_hits(94) := '0';
	file_eta_hits(97) := '0';
	file_eta_hits(98) := '0';
	file_eta_hits(101) := '0';
	file_eta_hits(105) := '0';
	file_eta_hits(108) := '0';
	file_eta_hits(112) := '0';
	file_eta_hits(115) := '0';
	file_eta_hits(118) := '0';
	file_eta_hits(122) := '0';
	file_eta_hits(125) := '0';
	file_eta_hits(129) := '0';
	file_eta_hits(132) := '0';
	file_eta_hits(135) := '0';
	file_eta_hits(139) := '0';
	file_eta_hits(142) := '0';
	file_eta_hits(145) := '0';
	file_eta_hits(147) := '0';
	file_eta_hits(148) := '0';
	file_eta_hits(151) := '0';
	file_eta_hits(155) := '0';
	file_eta_hits(158) := '0';
	file_eta_hits(162) := '0';
	file_eta_hits(165) := '0';
	file_eta_hits(168) := '0';
	file_eta_hits(172) := '0';
	file_eta_hits(175) := '0';
	file_eta_hits(178) := '0';
	file_eta_hits(182) := '0';
	file_eta_hits(185) := '0';
	file_eta_hits(188) := '0';
	file_eta_hits(189) := '0';
	file_eta_hits(193) := '0';
	file_eta_hits(196) := '0';
	file_eta_hits(199) := '0';
	file_eta_hits(202) := '0';
	file_eta_hits(205) := '0';
	file_eta_hits(208) := '0';
	file_eta_hits(211) := '0';
	file_eta_hits(214) := '0';
	file_eta_hits(218) := '0';
	file_eta_hits(221) := '0';
	file_eta_hits(225) := '0';
	file_eta_hits(228) := '0';
	file_eta_hits(231) := '0';
	file_eta_hits(234) := '0';
	file_eta_hits(238) := '0';
	file_eta_hits(241) := '0';
	file_eta_hits(244) := '0';
	file_eta_hits(247) := '0';
	file_eta_hits(250) := '0';
	file_eta_hits(253) := '0';
	file_eta_hits(256) := '0';
	file_eta_hits(259) := '0';
	file_eta_hits(261) := '0';
	file_eta_hits(264) := '0';
	file_eta_hits(268) := '0';
	file_eta_hits(271) := '0';
	file_eta_hits(275) := '0';
	file_eta_hits(278) := '0';
	file_eta_hits(281) := '0';
	file_eta_hits(285) := '0';
	file_eta_hits(288) := '0';
	file_eta_hits(292) := '0';
	file_eta_hits(295) := '0';
	file_eta_hits(298) := '0';
	file_eta_hits(302) := '0';
	file_eta_hits(306) := '0';
	file_eta_hits(309) := '0';
	file_eta_hits(313) := '0';
	file_eta_hits(316) := '0';
	file_eta_hits(320) := '0';
	file_eta_hits(323) := '0';
	file_eta_hits(326) := '0';
	file_eta_hits(330) := '0';
	file_eta_hits(333) := '0';
	file_eta_hits(336) := '0';
	file_eta_hits(340) := '0';
	file_eta_hits(343) := '0';
	file_eta_hits(347) := '0';
	file_eta_hits(350) := '0';
	file_eta_hits(353) := '0';
	file_eta_hits(356) := '0';
	file_eta_hits(359) := '0';
	file_eta_hits(363) := '0';
	file_eta_hits(366) := '0';
	file_eta_hits(370) := '0';
	file_eta_hits(373) := '0';
	file_eta_hits(376) := '0';
	file_eta_hits(380) := '0';
	file_eta_hits(383) := '0';
	file_eta_hits(387) := '0';
	file_eta_hits(390) := '0';
	file_eta_hits(393) := '0';
	file_eta_hits(397) := '0';
	file_eta_hits(400) := '0';
	file_eta_hits(403) := '0';
	file_eta_hits(407) := '0';
	file_eta_hits(410) := '0';
	file_eta_hits(414) := '0';
	file_eta_hits(417) := '0';
	file_eta_hits(420) := '0';
	file_eta_hits(424) := '0';
	file_eta_hits(427) := '0';
	file_eta_hits(430) := '0';
	file_eta_hits(434) := '0';
	file_eta_hits(437) := '0';
	file_eta_hits(441) := '0';
	file_eta_hits(444) := '0';
	file_eta_hits(447) := '0';
	file_eta_hits(450) := '0';
	file_eta_hits(453) := '0';
	file_eta_hits(456) := '0';
	file_eta_hits(459) := '0';
	file_eta_hits(463) := '0';
	file_eta_hits(466) := '0';
	file_eta_hits(469) := '0';
	file_eta_hits(472) := '0';
	file_eta_hits(475) := '0';
	file_eta_hits(477) := '0';
	file_eta_hits(480) := '0';
	file_eta_hits(484) := '0';
	file_eta_hits(487) := '0';
	file_eta_hits(490) := '0';
	file_eta_hits(493) := '0';
	file_eta_hits(496) := '0';
	file_eta_hits(499) := '0';
	file_eta_hits(502) := '0';
	file_eta_hits(505) := '0';
	file_eta_hits(508) := '0';
	file_eta_hits(511) := '0';




    return file_eta_hits;
  end function define_map_BO;
---------------------------------------------
---------------------------------------------
---------------------------------------------


-- first stage to remove redudancy -- assumere there is only one muon within +- 3 strips
function trigger_decluster_7_global (in5,in4,in3,in2,in1,in0 : std_logic_vector(6 downto 0) ) 
   return std_logic is
   variable pivot_decl : std_logic;
    begin
      -- 3 is the pivot of interest
      if(in5(3) = '1') then
        pivot_decl := '1';
      -- it is not priority 0, is there any other one available?
      elsif (in5(0) = '1' or in5(1) = '1' or in5(2) = '1' or in5(4) = '1' or in5(5) = '1' or in5(6) = '1') then
        pivot_decl := '0';
--      -- there is no priority 5 - so are we looking at a priority 4?
      elsif (in4(3) = '1') then
--        pivot_decl := '1';
      elsif (in4(0) = '1' or in4(1) = '1' or in4(2) = '1' or in4(4) = '1' or in4(5) = '1' or in4(6) = '1') then
--        pivot_decl := '0';
      elsif (in3(3) = '1') then
--        pivot_decl := '1';
      elsif (in3(0) = '1' or in3(1) = '1' or in3(2) = '1' or in3(4) = '1' or in3(5) = '1' or in3(6) = '1') then
--        pivot_decl := '0';
      elsif (in2(3) = '1') then
--        pivot_decl := '1';
      elsif (in2(0) = '1' or in2(1) = '1' or in2(2) = '1' or in2(4) = '1' or in2(5) = '1' or in2(6) = '1') then
--        pivot_decl := '0';
      elsif (in1(3) = '1') then
--        pivot_decl := '1';
      elsif (in1(0) = '1' or in1(1) = '1' or in1(2) = '1' or in1(4) = '1' or in1(5) = '1' or in1(6) = '1') then
--        pivot_decl := '0';
      elsif (in0(3) = '1') then
--        pivot_decl := '1';
      else
       pivot_decl := '0';
      end if; 

    return pivot_decl;
  end function trigger_decluster_7_global;

--function used to select the first candidate in a tower of 50 indeces

function  select_candidate_w50 (signal list_candidates :  std_logic_vector(63 downto 0))
    return integer_fixRange is
    variable i_cand : integer_fixRange;

    begin
            if (list_candidates(0)  = '1') then
       i_cand := 0;  
            elsif (list_candidates(1) = '1') then
       i_cand := 1;
            elsif (list_candidates(2) = '1') then
       i_cand := 2;
            elsif (list_candidates(3) = '1') then
       i_cand := 3;
            elsif (list_candidates(4) = '1') then
       i_cand := 4;
            elsif (list_candidates(5) = '1') then
       i_cand := 5; 
            elsif (list_candidates(6) = '1') then
       i_cand := 6; 
            elsif (list_candidates(7) = '1') then
       i_cand := 7;
            elsif (list_candidates(8) = '1') then
       i_cand := 8; 
            elsif (list_candidates(9) = '1') then
       i_cand := 9;  
            elsif (list_candidates(10) = '1') then
       i_cand := 10; 
            elsif (list_candidates(11) = '1') then
       i_cand := 11; 
            elsif (list_candidates(12) = '1') then
       i_cand := 12;  
            elsif (list_candidates(13) = '1') then
       i_cand := 13;  
            elsif (list_candidates(14) = '1') then
       i_cand := 14;  
            elsif (list_candidates(15) = '1') then
       i_cand := 15;  
            elsif (list_candidates(16) = '1') then
       i_cand := 16;  
            elsif (list_candidates(17) = '1') then
       i_cand := 17;  
            elsif (list_candidates(18) = '1') then
       i_cand := 18;  
            elsif (list_candidates(19) = '1') then
       i_cand := 19;  
            elsif (list_candidates(20) = '1') then
       i_cand := 20;  
            elsif (list_candidates(21) = '1') then
       i_cand := 21;  
            elsif (list_candidates(22) = '1') then
       i_cand := 22; 
            elsif (list_candidates(23) = '1') then
       i_cand := 23; 
            elsif (list_candidates(24) = '1') then
       i_cand := 24;  
            elsif (list_candidates(25) = '1') then
       i_cand := 25;  
            elsif (list_candidates(26) = '1') then
       i_cand := 26; 
            elsif (list_candidates(27) = '1') then
       i_cand := 27; 
            elsif (list_candidates(28) = '1') then
       i_cand := 28; 
            elsif (list_candidates(29) = '1') then
       i_cand := 29; 
            elsif (list_candidates(30) = '1') then
       i_cand := 30; 
            elsif (list_candidates(31) = '1') then
       i_cand := 31; 
            elsif (list_candidates(32) = '1') then
       i_cand := 32; 
            elsif (list_candidates(33) = '1') then
       i_cand := 33; 
            elsif (list_candidates(34) = '1') then
       i_cand := 34; 
            elsif (list_candidates(35) = '1') then
       i_cand := 35; 
            elsif (list_candidates(36) = '1') then
       i_cand := 36; 
            elsif (list_candidates(37) = '1') then
       i_cand := 37; 
            elsif (list_candidates(38) = '1') then
       i_cand := 38; 
            elsif (list_candidates(39) = '1') then
       i_cand := 39; 
            elsif (list_candidates(40) = '1') then
       i_cand := 40; 
            elsif (list_candidates(41) = '1') then
       i_cand := 41;
                   elsif (list_candidates(41) = '1') then
       i_cand := 41;
                   elsif (list_candidates(42) = '1') then
       i_cand := 42;
                   elsif (list_candidates(43) = '1') then
       i_cand := 43;
                   elsif (list_candidates(44) = '1') then
       i_cand := 44;
                   elsif (list_candidates(45) = '1') then
       i_cand := 45;
                   elsif (list_candidates(46) = '1') then
       i_cand := 46;
                  elsif (list_candidates(47) = '1') then
       i_cand := 47;
                   elsif (list_candidates(48) = '1') then
       i_cand := 48;
                   elsif (list_candidates(49) = '1') then
       i_cand := 49;
                   elsif (list_candidates(50) = '1') then
       i_cand := 50;
                 elsif (list_candidates(51) = '1') then
       i_cand := 51;  
                   elsif (list_candidates(52) = '1') then
       i_cand := 52;
                   elsif (list_candidates(53) = '1') then
       i_cand := 53;
                   elsif (list_candidates(54) = '1') then
       i_cand := 54;
                   elsif (list_candidates(55) = '1') then
       i_cand := 55;  
                   elsif (list_candidates(56) = '1') then
       i_cand := 56;
                   elsif (list_candidates(57) = '1') then
       i_cand := 57;
                   elsif (list_candidates(58) = '1') then
       i_cand := 58;
                   elsif (list_candidates(59) = '1') then
       i_cand := 59;
                   elsif (list_candidates(60) = '1') then
       i_cand := 60;
                   elsif (list_candidates(61) = '1') then
       i_cand := 61;  
                   elsif (list_candidates(62) = '1') then
       i_cand := 62;
                   elsif (list_candidates(63) = '1') then
       i_cand := 63;
       else
        i_cand := 64;
     end if;
     return i_cand;
  end function select_candidate_w50;






-- this is the lookup table for declustering
function trigger_decluster_7 (in0, in1, in2, in3, in4, in5, in6 : std_logic) 
   return std_logic is
   variable pivot_decl : std_logic;
    begin
      if ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' )  then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='0' and in2='0' and in3='1' and in4='0' and in5='1' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='0' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='0' and in1='1' and in2='1' and in3='1' and in4='1' and in5='0' and in6='0' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='1' and in5='0' and in6='1' ) then
        pivot_decl := '1'; 
      elsif ( in0='1' and in1='0' and in2='1' and in3='1' and in4='0' and in5='1' and in6='0' ) then
        pivot_decl := '1'; 
      else 
        pivot_decl := '0';
      end if;
      return pivot_decl;
  end function trigger_decluster_7;


--define the priority
procedure coin_type (constant RPC0: in std_logic;constant RPC1: in std_logic;constant RPC2: in std_logic;constant RPC3: in std_logic; signal prior : out integer_fixRangePrio) is
	begin 

        --RPC0-RPC1-RPC2-RPC3 
        if(RPC0='1' and RPC1='1' and RPC2='1' and RPC3='1') then
         prior <= 6;
	--RPC1-RPC2-RPC3
        elsif(RPC1='1' and RPC2='1' and RPC3='1') then
         prior <= 5;
	--RPC0-RPC2-RPC3
        elsif(RPC0='1' and RPC2='1' and RPC3='1') then
         prior <= 4;
	--RPC0-RPC1-RPC3
        elsif(RPC0='0' and RPC1='1' and RPC3='1') then
         prior <= 3;
	--RPC0-RPC1-RPC2
        elsif(RPC0='1' and RPC1='1' and RPC2='1') then
         prior <= 2;
	--RPC0-RPC3
        elsif(RPC0='1' and RPC3='1') then
         prior <= 1;
         else 
         prior <= 0;
        end if;

	--transform an integer into a std_logic_vector?
	
    end procedure coin_type;


-- algorithm used to get the closest hit. This assume that at least one hit in the layer is present
procedure closest_hit (constant window : in integer_fixRange; -- this can be reduced even
                       constant pivot_index : in integer_fixRange;
                       signal list_hit : in std_logic_vector;
                       signal clos_index : out integer_fixRange) is
          variable min :  integer_fixRange;
          begin 

          --min and max are the same
          min := pivot_index - window;
          for i in min to pivot_index loop
           --check right and left part, arbitrarily, give preference to left
           if ( list_hit(pivot_index+window-i) = '1' ) then
             clos_index <= pivot_index-i ;
           --now look from left to right
           elsif ( list_hit(i) = '1') then
             clos_index <= i;
           end if;   
          end loop;

   end procedure closest_hit;
   
   
-- algorithm used to get the closest hit. This assume that at least one hit in the layer is present
function closest_hit_fix_range_half (list_hit : std_logic_vector(64 downto 0) )
           return integer_fixRange is
           variable clos_index : integer_fixRange;
          begin    
   -- elsif (list_hit(41) = '1') then
  if (list_hit(41) = '1') then
    clos_index := 41;
 elsif (list_hit(20) = '1') then
    clos_index := 20;
 elsif (list_hit(42) = '1') then   
    clos_index := 42;
 elsif (list_hit(19) = '1') then
    clos_index := 19;
 elsif (list_hit(43) = '1') then   
    clos_index := 43;
 elsif (list_hit(18) = '1') then
    clos_index := 18;
 elsif (list_hit(44) = '1') then   
    clos_index := 44;
 elsif (list_hit(17) = '1') then
    clos_index := 17;
 elsif (list_hit(45) = '1') then   
    clos_index := 45;
 elsif (list_hit(16) = '1') then
    clos_index := 16;
 elsif (list_hit(46) = '1') then   
    clos_index := 46;
 elsif (list_hit(15) = '1') then
    clos_index := 15;
 elsif (list_hit(47) = '1') then   
    clos_index := 47;
 elsif (list_hit(14) = '1') then
    clos_index := 14;
 elsif (list_hit(48) = '1') then   
    clos_index := 48;
    
 elsif (list_hit(13) = '1') then
    clos_index := 13;
 elsif (list_hit(49) = '1') then   
    clos_index := 49;
    
 elsif (list_hit(12) = '1') then
    clos_index := 12;
 elsif (list_hit(50) = '1') then   
    clos_index := 50;
    
 elsif (list_hit(11) = '1') then
    clos_index := 11;
 elsif (list_hit(51) = '1') then   
    clos_index := 51;
 elsif (list_hit(10) = '1') then
    clos_index := 10;
    
 elsif (list_hit(52) = '1') then   
    clos_index := 52;
 elsif (list_hit(9) = '1') then
    clos_index := 9;
 elsif (list_hit(53) = '1') then   
    clos_index := 53;
  elsif (list_hit(8) = '1') then         
     clos_index := 8;                    --     
       elsif (list_hit(54) = '1') then   
    clos_index := 54;
elsif (list_hit(7) = '1') then          
   clos_index := 7;       
          
 elsif (list_hit(55) = '1') then   
    clos_index := 55;
    
 elsif (list_hit(6) = '1') then                 
   clos_index := 6;                            

       elsif (list_hit(56) = '1') then   
    clos_index := 56;

 elsif (list_hit(5) = '1') then         
    clos_index := 5;     
            
 elsif (list_hit(57) = '1') then   
    clos_index := 57;
    
    
       elsif (list_hit(58) = '1') then   
    clos_index := 58;
  elsif (list_hit(4) = '1') then         
     clos_index := 4;                    
 elsif (list_hit(59) = '1') then   
    clos_index := 59;
  elsif (list_hit(3) = '1') then                 
     clos_index := 3;                        




 elsif (list_hit(60) = '1') then   
    clos_index := 60;
 elsif (list_hit(2) = '1') then     
    clos_index := 2;   
          
   elsif (list_hit(61) = '1') then   
    clos_index := 61;

   elsif (list_hit(1) = '1') then     
   clos_index := 1;                


    
    
    elsif (list_hit(62) = '1') then   
    clos_index := 62;

   elsif (list_hit(0) = '1') then     
   clos_index := 0;                

   elsif (list_hit(63) = '1') then   
    clos_index := 63;
   else 
   clos_index := 0;
   end if;

   return clos_index;
   end function closest_hit_fix_range_half;   
   

-- algorithm used to get the closest hit. This assume that at least one hit in the layer is present
function closest_hit_fix_range (list_hit : std_logic_vector(64 downto 0) )
           return integer_fixRange is
           variable clos_index : integer_fixRange;
          begin 


   if (list_hit(31) = '1') then
      clos_index := 31; 
   elsif (list_hit(30) = '1') then
      clos_index := 30;            
   elsif (list_hit(32) = '1') then   
      clos_index := 32;  
      
   elsif (list_hit(29) = '1') then
      clos_index := 29;
   elsif (list_hit(31+2) = '1') then   
      clos_index := 33;      
      
   elsif (list_hit(28) = '1') then
      clos_index := 28;
   elsif (list_hit(34) = '1') then   
      clos_index := 34;      
 -- here works    
     
      
   elsif (list_hit(27) = '1') then
      clos_index := 27;
   elsif (list_hit(35) = '1') then   
      clos_index := 35;     
      
--   elsif (list_hit(26) = '1') then
--      clos_index := 26;
--   elsif (list_hit(36) = '1') then   
--      clos_index := 36;
--            
--   elsif (list_hit(25) = '1') then
--      clos_index := 25;
--   elsif (list_hit(37) = '1') then   
--      clos_index := 37;      
--      
--   elsif (list_hit(25) = '1') then
--      clos_index := 25;
--   elsif (list_hit(38) = '1') then   
--      clos_index := 38;
--    elsif (list_hit(24) = '1') then
--      clos_index := 24;
--   elsif (list_hit(39) = '1') then   
--      clos_index := 39;
--   elsif (list_hit(23) = '1') then
--      clos_index := 23;
--   elsif (list_hit(40) = '1') then   
--      clos_index := 40;
--   elsif (list_hit(21) = '1') then
--      clos_index := 21;
    
--    
-- --here works   
-- elsif (list_hit(41) = '1') then   
--    clos_index := 41;
-- elsif (list_hit(20) = '1') then
--    clos_index := 20;
-- elsif (list_hit(42) = '1') then   
--    clos_index := 42;
-- elsif (list_hit(19) = '1') then
--    clos_index := 19;
-- elsif (list_hit(42) = '1') then   
--    clos_index := 42;
-- elsif (list_hit(31-14) = '1') then
--    clos_index := 18;
-- elsif (list_hit(43) = '1') then   
--    clos_index := 43;
-- elsif (list_hit(17) = '1') then
--    clos_index := 17;
-- elsif (list_hit(44) = '1') then   
--    clos_index := 44;
-- elsif (list_hit(16) = '1') then
--    clos_index := 16;
-- elsif (list_hit(45) = '1') then   
--    clos_index := 45;
-- elsif (list_hit(15) = '1') then
--    clos_index := 15;
-- elsif (list_hit(46) = '1') then   
--    clos_index := 46;
-- elsif (list_hit(14) = '1') then
--    clos_index := 14;
-- elsif (list_hit(47) = '1') then   
--    clos_index := 47;
--    
-- elsif (list_hit(13) = '1') then
--    clos_index := 13;
-- elsif (list_hit(48) = '1') then   
--    clos_index := 48;
--    
-- elsif (list_hit(12) = '1') then
--    clos_index := 12;
-- elsif (list_hit(49) = '1') then   
--    clos_index := 49;
--    
-- elsif (list_hit(11) = '1') then
--    clos_index := 11;
-- elsif (list_hit(50) = '1') then   
--    clos_index := 50;
-- elsif (list_hit(10) = '1') then
--    clos_index := 10;
--  
-- elsif (list_hit(51) = '1') then   
--    clos_index := 51;
-- elsif (list_hit(9) = '1') then
--    clos_index := 9;
-- elsif (list_hit(52) = '1') then   
--    clos_index := 52;
--  elsif (list_hit(8) = '1') then         
--     clos_index := 8;                    --     
--       elsif (list_hit(53) = '1') then   
--    clos_index := 53;
--elsif (list_hit(7) = '1') then          
--   clos_index := 7;       
--          
-- elsif (list_hit(54) = '1') then   
--    clos_index := 54;
--    
-- elsif (list_hit(6) = '1') then                 
--   clos_index := 6;                            
--
--       elsif (list_hit(55) = '1') then   
--    clos_index := 55;
--
-- elsif (list_hit(5) = '1') then         
--    clos_index := 5;     
--            
-- elsif (list_hit(56) = '1') then   
--    clos_index := 56;
--    
--    
--       elsif (list_hit(57) = '1') then   
--    clos_index := 57;
--  elsif (list_hit(4) = '1') then         
--     clos_index := 4;                    
-- elsif (list_hit(58) = '1') then   
--    clos_index := 58;
--  elsif (list_hit(3) = '1') then                 
--     clos_index := 3;                        
--
--
--
--
-- elsif (list_hit(59) = '1') then   
--    clos_index := 59;
-- elsif (list_hit(2) = '1') then     
--    clos_index := 2;   
--          
--       elsif (list_hit(60) = '1') then   
--    clos_index := 60;
--
--elsif (list_hit(1) = '1') then     
--   clos_index := 1;                
--
--
--    
--    
--       elsif (list_hit(61) = '1') then   
--    clos_index := 61;
--
--elsif (list_hit(0) = '1') then     
--   clos_index := 0;                
--
-- elsif (list_hit(62) = '1') then   
--    clos_index := 62;
--    
-- 
 
 
 
 --  elsif (list_hit(63) = '1') then   
 --     clos_index := 63;
 --     
 
 
 --  elsif (list_hit(1) = '1') then
 --     clos_index := 1;
 --  elsif (list_hit(0) = '1') then   
 --     clos_index := 0;
 --     
 
 
   else 
   clos_index := 0;
   end if;
   

   return clos_index;
   end function closest_hit_fix_range;

-- this shall take 2 clock cycles
procedure fill_trig_candidate (constant potential_candidates: in std_logic_vector; signal candidates : inout std_logic_vector; signal candidate_num : inout integer_fixRange) is
    begin

    -- look for more candidates here - first declaster   
    for i in 20 to 492  loop
     if (potential_candidates(i) = '1') then
       candidates(i)<= '1'; 
   --    candidate_num<= candidate_num + 1; 
     end if;
    end loop;

 end procedure fill_trig_candidate;


procedure priority_encoder(signal tower_cand_20: in tower_candidate; signal tower_cand_15: in tower_candidate;
                           signal tower_cand_10: in tower_candidate; signal tower_cand_5: in tower_candidate; 
                           signal o_tower_cand_20: out tower_candidate; signal o_tower_cand_15: out tower_candidate;
                           signal o_tower_cand_10: out tower_candidate; signal o_tower_cand_5: out tower_candidate;                           
                           signal candidate: out integer_fixRange; signal pT_idx: out integer_fixRangePrio) is
                           begin
 
                           
                           if(tower_cand_20(0) /= 64)  then
                            candidate <= tower_cand_20(0);
                            o_tower_cand_20(0) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(1) /= 64) then
                            candidate <= tower_cand_20(1);--+64;
                            o_tower_cand_20(1) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(2) /= 64) then
                            candidate <= tower_cand_20(2);--+128;
                            o_tower_cand_20(2) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(3) /= 64) then
                            candidate <= tower_cand_20(3);--+192;
                            o_tower_cand_20(3) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(4) /= 64) then
                            candidate <= tower_cand_20(4);--+256;
                            o_tower_cand_20(4) <= 64;
                            pT_idx <= 0;
                            elsif(tower_cand_20(5) /= 64) then
                            candidate <= tower_cand_20(5);--+320;
                            o_tower_cand_20(5) <= 64;
                            pT_idx <= 0;
                           elsif(tower_cand_20(6) /= 64) then
                            candidate <= tower_cand_20(4);--+384;
                            o_tower_cand_20(6) <= 64;
                            pT_idx <= 0;
                            elsif(tower_cand_20(7) /= 64) then
                            candidate <= tower_cand_20(4);--+448;
                            o_tower_cand_20(7) <= 64;
                            pT_idx <= 0;
                      
                           elsif(tower_cand_15(0) /= 64) then
                            candidate <= tower_cand_15(0);  
                            o_tower_cand_15(0) <= 64;
                            pT_idx <= 1;                          
                           elsif(tower_cand_15(1) /= 64) then
                            candidate <= tower_cand_15(1);--+64;
                            o_tower_cand_15(1) <= 64;
                            pT_idx <= 1;                            
                            elsif(tower_cand_15(2) /= 64) then
                            candidate <= tower_cand_15(2);--+128;
                            o_tower_cand_15(2) <= 64;
                            pT_idx <= 1;
                           elsif(tower_cand_15(3) /= 64) then
                            candidate <= tower_cand_15(3);--+192;
                            o_tower_cand_15(3) <= 64;
                            pT_idx <= 1;
                            elsif(tower_cand_15(4) /= 64) then
                            candidate <= tower_cand_15(4);--+256; 
                            o_tower_cand_15(4) <= 64;
                            pT_idx <= 1; 
                            elsif(tower_cand_15(5) /= 64) then
                            candidate <= tower_cand_15(5);--+320; 
                            o_tower_cand_15(5) <= 64; 
                            pT_idx <= 1;                         
                            elsif(tower_cand_15(6) /= 64) then
                            candidate <= tower_cand_15(6);--+384;  
                            o_tower_cand_15(6) <= 64;
                            pT_idx <= 1;                         
                           elsif(tower_cand_15(7) /= 64) then
                            candidate <= tower_cand_15(7);--+448;
                            o_tower_cand_15(7) <= 64;
                            pT_idx <= 1;
                            
                            elsif(tower_cand_10(0) /= 64) then
                            candidate <= tower_cand_10(0);   
                            o_tower_cand_10(0) <= 64; 
                            pT_idx <= 2;                        
                           elsif(tower_cand_10(1) /= 64) then
                            candidate <= tower_cand_10(1);--+64; 
                            o_tower_cand_10(1) <= 64; 
                            pT_idx <= 2;                           
                            elsif(tower_cand_10(2) /= 64) then
                            candidate <= tower_cand_10(2);--+128;
                            o_tower_cand_10(2) <= 64; 
                            pT_idx <= 2;
                           elsif(tower_cand_10(3) /= 64) then
                            candidate <= tower_cand_10(3);--+192;
                            o_tower_cand_10(3) <= 64;
                            pT_idx <= 2; 
                            elsif(tower_cand_10(4) /= 64) then
                            candidate <= tower_cand_10(4);--+256; 
                            o_tower_cand_10(4) <= 64;  
                            pT_idx <= 2;
                            elsif(tower_cand_10(5) /= 64) then
                            candidate <= tower_cand_10(5);--+320; 
                            o_tower_cand_10(5) <= 64;
                            pT_idx <= 2;                           
                            elsif(tower_cand_10(6) /= 64) then
                            candidate <= tower_cand_10(6);--+384; 
                            o_tower_cand_10(6) <= 64;  
                            pT_idx <= 2;                         
                           elsif(tower_cand_10(7) /= 64) then
                            candidate <= tower_cand_10(7);--+448; 
                            o_tower_cand_10(7) <= 64;  
                            pT_idx <= 2;                         
                            
                           elsif(tower_cand_5(0) /= 64) then
                            candidate <= tower_cand_5(0);  
                            o_tower_cand_5(0) <= 64;    
                            pT_idx <= 3;                       
                           elsif(tower_cand_5(1) /= 64) then
                            candidate <= tower_cand_5(1);--+64;
                            o_tower_cand_5(1) <= 64;  
                            pT_idx <= 3;                          
                            elsif(tower_cand_5(2) /= 64) then
                            candidate <= tower_cand_5(2);--+128;
                            o_tower_cand_5(2) <= 64;
                            pT_idx <= 3;
                           elsif(tower_cand_5(3) /= 64) then
                            candidate <= tower_cand_5(3);--+192;
                            o_tower_cand_5(3) <= 64;
                            pT_idx <= 3;
                            elsif(tower_cand_5(4) /= 64) then
                            candidate <= tower_cand_5(4);--+256; 
                            o_tower_cand_5(4) <= 64; 
                            pT_idx <= 3;
                            elsif(tower_cand_5(5) /= 64) then
                            candidate <= tower_cand_5(5);--+320;   
                            o_tower_cand_5(5) <= 64;  
                            pT_idx <= 3;                      
                            elsif(tower_cand_5(6) /= 64) then
                            candidate <= tower_cand_5(6);--+384;  
                            o_tower_cand_5(6) <= 64;    
                            pT_idx <= 3;                     
                            elsif(tower_cand_5(7) /= 64) then
                            candidate <= tower_cand_5(7);--+448;
                            o_tower_cand_5(7) <= 64;
                            pT_idx <= 3;
                            else                  
 --                           tower_cand_5 <= tower_cand_5 ; -- shall be a copy of itself
                            candidate <= 0; -- need to adapt this to lower values once I enlarge the input sizes
                            pT_idx <= 4;
                                                                                                                                                                                                      
                           end if;                          
                           

 end procedure priority_encoder;



end package body;



