----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/11/2021 11:15:14 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.ALL;
library work;
use work.pack.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--entity top is
--    Port ( clock : in STD_LOGIC;
--           A,B,C : in std_logic;
--           eta_hits : in std_logic_vector(9 downto 0);
--           F_h  : out std_logic;
--           F    : out std_logic);
--end top;
--
--
---- clock generation --
--architecture Behavioral of top is
--
--begin
--
--F <= NOT(A AND B AND C);
--
--F_h <= eta_hits(0);
--
--end Behavioral;

-- define the input random data collector--
entity top_trigger is
    Port ( file_eta_hits_BI_orig    :  in std_logic_vector(319 downto 0); 
           file_eta_hits_BM1_orig   :  in std_logic_vector(383 downto 0); 
           file_eta_hits_BM2_orig   :  in std_logic_vector(447 downto 0);
           file_eta_hits_BO_orig    :  in std_logic_vector(383 downto 0);
           file_phi_hits_BM1_orig   :  in  phi_chamber_extended_BM1;
           file_phi_hits_BI_orig    :  in phi_chamber_extended_BI; 
           file_phi_hits_BM2_orig   :  in phi_chamber_extended_BM2; 
           file_phi_hits_BO_orig    :  in phi_chamber_extended_BO; 
           cand_1    :  out   std_logic_vector(127 downto 0);
           cand_2    :  out   std_logic_vector(127 downto 0);
           cand_3    :  out   std_logic_vector(127 downto 0);
           cand_4    :  out   std_logic_vector(127 downto 0);
           
           clock_h    : in std_logic);
           --dummy_output: out std_logic);
--           decision_lay1 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay2 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay3 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay4 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay5 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay6 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay7 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay8 : inout std_logic_vector(512 downto 0) := (others => '0');
--           decision_lay9 : inout std_logic_vector(512 downto 0) := (others => '0');
--           trigger_decision : inout std_logic_vector(512 downto 0) := (others => '0');
           --global_trig    : inout std_logic;
           --need a vector if integer cause we have up to 3 muon candidates
           --clos_index_lay1    : out t_p_clos_vector;
           --clos_index_lay2    : out t_p_clos_vector;
           --clos_index_lay4    : out t_p_clos_vector;
           --clos_index_lay3    : out t_p_clos_vector);
            --pT_triggered : out t_vector_prio);
           --clos_index_lay4    : out integer;
           --clos_index_lay5    : out integer;
           --clos_index_lay6    : out integer;
           --clos_index_lay7    : out integer;
           --clos_index_lay8    : out integer;
           --clos_index_lay9    : out integer;
           --pivot_idx          : out integer;
           --trig_cand          : inout std_logic_vector(512 downto 0);
           --trig_cand_5        : inout std_logic_vector(512 downto 0);
           --trig_cand_4        : inout std_logic_vector(512 downto 0);
           --trig_cand_3        : inout std_logic_vector(512 downto 0);
           --trig_cand_2        : inout std_logic_vector(512 downto 0);
           --trig_cand_1        : inout std_logic_vector(512 downto 0);
           --trig_cand_0        : inout std_logic_vector(512 downto 0));
           -- TODO: re-check why inout is required for these 
           -- shall I use a buffer ? trig_cand      : out std_logic_vector(512 downto 0));
end top_trigger;


architecture Behavioral_sim of top_trigger is

type phi_chamber is array (0 to 7) of std_logic_vector(80 downto 0);
--type phi_chamber_extended_BI is array (0 to 9) of std_logic_vector(127 downto 0);
--type phi_chamber_extended_BM1 is array (0 to 5) of std_logic_vector(80+40 downto 0);
--type phi_chamber_extended_BM2 is array (0 to 6) of std_logic_vector(80+40 downto 0);
--type phi_chamber_extended_BO is array (0 to 5) of std_logic_vector(80+40 downto 0);
type pt_array is array (0 to 3) of std_logic_vector(511 downto 0);
type t_vector is array (0 to 511) of integer_fixRangePrio;
type t_p_vector is array (0 to 3) of t_vector;
type t_clos_vector is array (0 to 511) of integer_fixRange;
--type t_p_clos_vector is array (0 to 3) of t_clos_vector;



type tower_candidate_logic is array (0 to 3) of std_logic_vector(8 downto 0); 
type tower_candidate_incl is array (0 to 3) of tower_candidate;

type final_candidates is array (0 to 3) of integer_fixRange;


attribute dont_touch : string;


--initialize the signals such that we can later map them correctly
signal file_eta_hits_l1   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l2   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l3   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l4   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l5   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l6   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l7   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l8   :  std_logic_vector(511 downto 0) := (others => '0');
signal file_eta_hits_l9   :  std_logic_vector(511 downto 0) := (others => '0');


-- BI
signal file_eta_hits_l1_orig   :  std_logic_vector(319 downto 0);
signal file_eta_hits_l2_orig   :  std_logic_vector(319 downto 0);
signal file_eta_hits_l3_orig   :  std_logic_vector(319 downto 0);
-- BM1
signal file_eta_hits_l4_orig   :  std_logic_vector(383 downto 0);
signal file_eta_hits_l5_orig   :  std_logic_vector(383 downto 0);
-- BM2
signal file_eta_hits_l6_orig   :  std_logic_vector(447 downto 0);
signal file_eta_hits_l7_orig   :  std_logic_vector(447 downto 0);
-- BO
signal file_eta_hits_l8_orig   :  std_logic_vector(383 downto 0);
signal file_eta_hits_l9_orig   :  std_logic_vector(383 downto 0);


signal file_phi_hits_l1   :  phi_chamber_extended_BI;
signal file_phi_hits_l2   :  phi_chamber_extended_BI;
signal file_phi_hits_l3   :  phi_chamber_extended_BI;
signal file_phi_hits_l4   :  phi_chamber_extended_BM1;
signal file_phi_hits_l5   :  phi_chamber_extended_BM1;
signal file_phi_hits_l6   :  phi_chamber_extended_BM2;
signal file_phi_hits_l7   :  phi_chamber_extended_BM2;
signal file_phi_hits_l8   :  phi_chamber_extended_BO;
signal file_phi_hits_l9   :  phi_chamber_extended_BO;
                                        
signal file_phi_hits_BO   :  phi_chamber_extended_BO;
signal file_phi_hits_BM1  :  phi_chamber_extended_BM1;
signal file_phi_hits_BM2  :  phi_chamber_extended_BM2;
signal file_phi_hits_BI   :  phi_chamber_extended_BI;


signal file_eta_hits_BO   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_1   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_1  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_1  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_1   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_2   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_2  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_2  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_2   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_3   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_3  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_3  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_3   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_4   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_4  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_4  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_4   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_5   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_5  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_5  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_5  :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_6   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_6  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_6  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_6   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_7   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_7  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_7  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_7   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_8   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_8  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_8  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_8   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_9   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_9  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_9  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_9   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_10   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_10 :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_10  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_10   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_11   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_11 :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_11  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_11   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_12   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_12 :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_12  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_12   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BO_13   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM1_13 :  std_logic_vector(511 downto 0);
signal file_eta_hits_BM2_13  :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_13   :  std_logic_vector(511 downto 0);

signal file_eta_hits_BI_coin_0   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_coin_1   :  std_logic_vector(511 downto 0);
signal file_eta_hits_BI_coin_2   :  std_logic_vector(511 downto 0);



signal global_trig    :     std_logic_vector(3 downto 0);                          
signal clos_index_lay1    :  t_p_clos_vector;                          
signal clos_index_lay2    :  t_p_clos_vector;                          
signal clos_index_lay3    :  t_p_clos_vector;                          
signal clos_index_lay4    :  t_p_clos_vector;      

signal clos_index_lay1_half    :  t_p_clos_vector;                          
signal clos_index_lay2_half    :  t_p_clos_vector;                          
signal clos_index_lay3_half    :  t_p_clos_vector;                          
signal clos_index_lay4_half    :  t_p_clos_vector;

signal clos_index_lay1_final    :  t_p_clos_vector;                          
signal clos_index_lay2_final    :  t_p_clos_vector;                          
signal clos_index_lay3_final    :  t_p_clos_vector;                          
signal clos_index_lay4_final    :  t_p_clos_vector;

                    
--signal clos_index_lay5    :  t_clos_vector;                          
--signal clos_index_lay6    :  t_clos_vector;                          
--signal clos_index_lay7    :  t_clos_vector;                          
--signal clos_index_lay8    :  t_clos_vector;                          
--signal clos_index_lay9    :  t_clos_vector;                          
signal pivot_idx          :  t_clos_vector;                          
signal trig_cand          :  pt_array; 
signal trig_cand_5        :  pt_array; 
signal trig_cand_4        :  pt_array; 
signal trig_cand_3        :  pt_array; 
signal trig_cand_2        :  pt_array; 
signal trig_cand_1        :  pt_array; 
signal trig_cand_0        :  pt_array;

--attribute dont_touch of global_trig: signal is "true";
--attribute dont_touch of pivot_idx: signal is "true";
  
  -- BI
--signal file_eta_hits_BI_orig   :  std_logic_vector(319 downto 0);
--signal file_phi_hits_BI_orig   :  phi_chamber_extended_BI;


-- BM1
--signal file_eta_hits_BM1_orig   :  std_logic_vector(383 downto 0);
--signal file_phi_hits_BM1_orig   :  phi_chamber_extended_BM1;

-- BM2
--signal file_eta_hits_BM2_orig   :  std_logic_vector(447 downto 0);
--signal file_phi_hits_BM2_orig   :  phi_chamber_extended_BM2;

-- BO
--signal file_eta_hits_BO_orig   :  std_logic_vector(383 downto 0);
--signal file_phi_hits_BO_orig   :  phi_chamber_extended_BO;
  
  
  
--  attribute dont_touch of file_eta_hits_BI_orig: signal is "true";    
--  attribute dont_touch of file_eta_hits_BM1_orig: signal is "true"; 
--  attribute dont_touch of file_eta_hits_BM2_orig: signal is "true"; 
--  attribute dont_touch of file_eta_hits_BO_orig: signal is "true";    
  
--  attribute dont_touch of file_phi_hits_BI_orig: signal is "true";
--  attribute dont_touch of file_phi_hits_BM1_orig: signal is "true";
--  attribute dont_touch of file_phi_hits_BM2_orig: signal is "true";
--  attribute dont_touch of file_phi_hits_BO_orig: signal is "true";                            

--attribute dont_touch of file_eta_hits_l1_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l2_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l3_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l4_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l5_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l6_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l7_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l8_orig: signal is "true";
--attribute dont_touch of file_eta_hits_l9_orig: signal is "true";
--
--attribute dont_touch of file_phi_hits_l1: signal is "true";
--attribute dont_touch of file_phi_hits_l2: signal is "true";
--attribute dont_touch of file_phi_hits_l3: signal is "true";
--attribute dont_touch of file_phi_hits_l4: signal is "true";
--attribute dont_touch of file_phi_hits_l5: signal is "true";
--attribute dont_touch of file_phi_hits_l6: signal is "true";
--attribute dont_touch of file_phi_hits_l7: signal is "true";
--attribute dont_touch of file_phi_hits_l8: signal is "true";
--attribute dont_touch of file_phi_hits_l9: signal is "true";


--signal cand_1    :     std_logic_vector(127 downto 0);
--signal cand_2    :     std_logic_vector(127 downto 0);
--signal cand_3    :     std_logic_vector(127 downto 0);
--signal cand_4    :     std_logic_vector(127 downto 0);

-- these are the output for the 4 candidates
--attribute dont_touch of cand_1: signal is "true";
--attribute dont_touch of cand_2: signal is "true";
--attribute dont_touch of cand_3: signal is "true";
--attribute dont_touch of cand_4: signal is "true";




signal   decision_lay1 :  pt_array;
signal   decision_lay2 :  pt_array;
signal   decision_lay3 :  pt_array;
signal   decision_lay4 :  pt_array;

signal   decision_lay1_1 :  pt_array;
signal   decision_lay2_1:  pt_array;
signal   decision_lay3_1 :  pt_array;
signal   decision_lay4_1 :  pt_array;


signal   decision_lay1_2 :  pt_array;
signal   decision_lay2_2 :  pt_array;
signal   decision_lay3_2 :  pt_array;
signal   decision_lay4_2 :  pt_array;


signal   decision_lay1_3 :  pt_array;
signal   decision_lay2_3:  pt_array;
signal   decision_lay3_3 :  pt_array;
signal   decision_lay4_3 :  pt_array;


-- signals in phi now

signal   decision_phi_lay1 :  phi_chamber_extended_BI;
signal   decision_phi_lay2 :  phi_chamber_extended_BM1;
signal   decision_phi_lay3 :  phi_chamber_extended_BM2;
signal   decision_phi_lay4 :  phi_chamber_extended_BO;

--signal   decision_lay5 :  pt_array;
--signal   decision_lay6 :  pt_array;
--signal   decision_lay7 :  pt_array;
--signal   decision_lay8 :  pt_array;
--signal   decision_lay9 :  pt_array;
--signal   trigger_patterns_9l_3d : pt_array;
signal   trig_decl_cand :   pt_array ;
signal   trig_decl_cand_5 : pt_array;
signal   trig_decl_cand_4 : pt_array;
signal   trig_decl_cand_3 : pt_array;
signal   trig_decl_cand_2 : pt_array;
signal   trig_decl_cand_1 : pt_array;
signal   trig_decl_cand_0 : pt_array;



signal pT_triggered : t_vector_prio;
signal  tower_cand : tower_candidate_incl;
signal  tower_cand_2 : tower_candidate_incl;
signal  tower_cand_3 : tower_candidate_incl;
signal  tower_cand_4 : tower_candidate_incl;
signal  tower_cand_5 : tower_candidate_incl;



signal tower_cand_logic: tower_candidate_logic;
signal f_cand : final_candidates;

signal   RPC0 : std_logic_vector(511 downto 0) := (others => '0');
signal   RPC1 : std_logic_vector(511 downto 0) := (others => '0');
signal   RPC2 : std_logic_vector(511 downto 0) := (others => '0');
signal   RPC3 : std_logic_vector(511 downto 0) := (others => '0');

signal   trigger_decision :  pt_array;
signal   trigger_decision_pre :  pt_array;
signal   trigger_decision_pre_1 :  pt_array;
signal   trigger_decision_phi :  phi_chamber_extended_BI;
signal   trigger_decision_phi_chamber : std_logic_vector(9+2 downto 0);
signal   priority : t_p_vector; 
signal   candidate_pT : integer := 0 ; 
signal   reset : std_logic; 
signal   candidate_num_tot : integer ; 



--attribute dont_touch of pT_triggered: signal is "true";
--signal   priority_array : t_vector :=(others => 0);
-- for because we store 4 candidates
--signal priority is array (0 to 4) of std_logic_vector(7 downto 0);

begin

-- initialize the counter of the candidates for each event



P_INCREMENT : process (clock_h)
begin


if rising_edge(clock_h) then
--  if rising_edge(clock_h) then
    --initialize-- what to do with boundary cells? --
  

-- loop to map to global eta bins phi on the other hands does not need any map

    file_eta_hits_BI <= define_map_BI(file_eta_hits_BI_orig);
    file_eta_hits_BM1 <= define_map_BM1(file_eta_hits_BM1_orig);
    file_eta_hits_BM2 <= define_map_BM2(file_eta_hits_BM2_orig);
    file_eta_hits_BO <= define_map_BO(file_eta_hits_BO_orig);

-- will need a map for phi
    file_phi_hits_BI <= file_phi_hits_BI_orig;
    file_phi_hits_BM1 <= file_phi_hits_BM1_orig;
    file_phi_hits_BM2 <= file_phi_hits_BM2_orig;
    file_phi_hits_BO <= file_phi_hits_BO_orig;

--    for i in 0 to 319 loop
--        file_eta_hits_l1(trigger_map_BM1(i)) <= file_eta_hits_l1_orig(i);   
--        file_eta_hits_l2(trigger_map_BM1(i)) <= file_eta_hits_l2_orig(i);   
--        file_eta_hits_l3(trigger_map_BM1(i)) <= file_eta_hits_l3_orig(i);   
--    end loop;  
--
--
--    for i in 0 to 316 loop
--        file_eta_hits_l4(trigger_map_BM1(i)) <= file_eta_hits_l4_orig(i);   
--        file_eta_hits_l5(trigger_map_BM1(i)) <= file_eta_hits_l5_orig(i);   
--        file_eta_hits_l6(trigger_map_BM1(i)) <= file_eta_hits_l6_orig(i);   
--        file_eta_hits_l7(trigger_map_BM1(i)) <= file_eta_hits_l7_orig(i);   
--        file_eta_hits_l8(trigger_map_BM1(i)) <= file_eta_hits_l8_orig(i); 
--        file_eta_hits_l9(trigger_map_BM1(i)) <= file_eta_hits_l9_orig(i);   
--    end loop;  

-- here we put together the triplets and doublets --
  
  
--   for i in 20 to 492  loop
--   
--   file_eta_hits_BO(i)  <= file_eta_hits_l9(i) OR file_eta_hits_l8(i);
--   file_eta_hits_BM2(i) <= file_eta_hits_l7(i) OR file_eta_hits_l6(i);
--   file_eta_hits_BM1(i) <= file_eta_hits_l5(i) OR file_eta_hits_l4(i);
--   -- for the BI we need to look also at the closest hits
--   file_eta_hits_BI(i)  <= ( ( file_eta_hits_l3(i) OR file_eta_hits_l3(i-1) OR file_eta_hits_l3(i+1)) AND 
--   (file_eta_hits_l2(i) OR file_eta_hits_l2(i+1) OR file_eta_hits_l2(i-1))) OR  
--   ((file_eta_hits_l1(i) OR file_eta_hits_l1(i+1) OR file_eta_hits_l1(i-1)) AND 
--   (file_eta_hits_l2(i) OR file_eta_hits_l2(i+1) OR file_eta_hits_l2(i-1))) OR
--   ((file_eta_hits_l3(i) OR file_eta_hits_l3(i+1) OR file_eta_hits_l3(i-1)) AND 
--   (file_eta_hits_l1(i) OR file_eta_hits_l1(i+1) OR file_eta_hits_l1(i-1)));
--   end loop;
   
   
-- copy to get the closest index
   file_eta_hits_BO_1 <= file_eta_hits_BO;
   file_eta_hits_BO_2 <= file_eta_hits_BO_1;
   file_eta_hits_BO_3 <= file_eta_hits_BO_2;
   file_eta_hits_BO_4 <= file_eta_hits_BO_3;
   file_eta_hits_BO_5 <= file_eta_hits_BO_4;
   file_eta_hits_BO_6 <= file_eta_hits_BO_5;
   file_eta_hits_BO_7 <= file_eta_hits_BO_6;
   file_eta_hits_BO_8 <= file_eta_hits_BO_7;
   file_eta_hits_BO_9 <= file_eta_hits_BO_8;
   file_eta_hits_BO_10 <= file_eta_hits_BO_9;
   file_eta_hits_BO_11 <= file_eta_hits_BO_10;
   file_eta_hits_BO_12 <= file_eta_hits_BO_11;
   file_eta_hits_BO_13 <= file_eta_hits_BO_12;

   file_eta_hits_BM1_1 <= file_eta_hits_BM1;
   file_eta_hits_BM1_2 <= file_eta_hits_BM1_1;
   file_eta_hits_BM1_3 <= file_eta_hits_BM1_2;
   file_eta_hits_BM1_4 <= file_eta_hits_BM1_3;
   file_eta_hits_BM1_5 <= file_eta_hits_BM1_4;
   file_eta_hits_BM1_6 <= file_eta_hits_BM1_5;
   file_eta_hits_BM1_7 <= file_eta_hits_BM1_6;
   file_eta_hits_BM1_8 <= file_eta_hits_BM1_7;
   file_eta_hits_BM1_9 <= file_eta_hits_BM1_8;
   file_eta_hits_BM1_10 <= file_eta_hits_BM1_9;
   file_eta_hits_BM1_11 <= file_eta_hits_BM1_10;
   file_eta_hits_BM1_12 <= file_eta_hits_BM1_11;
   file_eta_hits_BM1_13 <= file_eta_hits_BM1_12;

   file_eta_hits_BM2_1 <= file_eta_hits_BM2;
   file_eta_hits_BM2_2 <= file_eta_hits_BM2_1;
   file_eta_hits_BM2_3 <= file_eta_hits_BM2_2;
   file_eta_hits_BM2_4 <= file_eta_hits_BM2_3;
   file_eta_hits_BM2_5 <= file_eta_hits_BM2_4;
   file_eta_hits_BM2_6 <= file_eta_hits_BM2_5;
   file_eta_hits_BM2_7 <= file_eta_hits_BM2_6;
   file_eta_hits_BM2_8 <= file_eta_hits_BM2_7;
   file_eta_hits_BM2_9 <= file_eta_hits_BM2_8;
   file_eta_hits_BM2_10 <= file_eta_hits_BM2_9;
   file_eta_hits_BM2_11 <= file_eta_hits_BM2_10;
   file_eta_hits_BM2_12 <= file_eta_hits_BM2_11;
   file_eta_hits_BM2_13 <= file_eta_hits_BM2_12;


   file_eta_hits_BI_1 <= file_eta_hits_BI;
   file_eta_hits_BI_2 <= file_eta_hits_BI_1;
   file_eta_hits_BI_3 <= file_eta_hits_BI_2;
   file_eta_hits_BI_4 <= file_eta_hits_BI_3;
   file_eta_hits_BI_5 <= file_eta_hits_BI_4;
   file_eta_hits_BI_6 <= file_eta_hits_BI_5;
   file_eta_hits_BI_7 <= file_eta_hits_BI_6;
   file_eta_hits_BI_8 <= file_eta_hits_BI_7;
   file_eta_hits_BI_9 <= file_eta_hits_BI_8;
   file_eta_hits_BI_10 <= file_eta_hits_BI_9;
   file_eta_hits_BI_11 <= file_eta_hits_BI_10;
   file_eta_hits_BI_12 <= file_eta_hits_BI_11;
   file_eta_hits_BI_13 <= file_eta_hits_BI_12;




--- compress the phi regions now --
--  for i in 0 to 87+20 loop
--   for cha in 0 to 9 loop
--      --  -- replace with 2/3
--      file_phi_hits_BI(cha)(i)  <= (file_phi_hits_l3(cha)(i) AND file_phi_hits_l2(cha)(i))
--      OR  (file_phi_hits_l3(cha)(i) AND file_phi_hits_l1(cha)(i))
--      OR  (file_phi_hits_l1(cha)(i) AND file_phi_hits_l2(cha)(i));
--   end loop;
--  end loop;
--
---aggiungi dummy values per avere eta e phi in fase
--
--
--  for i in 0 to 80+20 loop
--   for cha in 0 to 5 loop
--      file_phi_hits_BM1(cha)(i) <= file_phi_hits_l5(cha)(i) OR file_phi_hits_l4(cha)(i);
--   end loop;
--  end loop;
--  
--  for i in 0 to 80+20 loop
--   for cha in 0 to 6 loop
--      file_phi_hits_BM2(cha)(i) <= file_phi_hits_l7(cha)(i) OR file_phi_hits_l6(cha)(i);
--   end loop;
--  end loop;   
--  
--  
--  for i in 0 to 80+20 loop
--   for cha in 0 to 5 loop
--      file_phi_hits_BO(cha)(i)  <= file_phi_hits_l9(cha)(i) OR file_phi_hits_l8(cha)(i);
--   end loop;
--  end loop;





--trigger_patterns_phi_7l -- phi coincidence -- everything is shifted by 20 to avoid edge problems - shall we run any overlap removal of some sort?
-- single coincidence function of the pT for now using looser pT cut to define the coincidence
   for i in 20 to 80+20 loop 
   
    for cha in 0 to 5 loop
      if file_phi_hits_BO(cha)(i+trigger_patterns_phi_8l(trigger_map(i))(0) downto i-trigger_patterns_phi_8l(trigger_map(i))(1)) 
      /= (i+trigger_patterns_phi_8l(trigger_map(i))(0) downto i-trigger_patterns_phi_8l(trigger_map(i))(1) => '0') then
       decision_phi_lay4(cha)(i) <= '1';
      else 
       decision_phi_lay4(cha)(i) <= '0';
      end if;
      end loop; 
      
      for cha in 0 to 6 loop
      if file_phi_hits_BM2(cha)(i+trigger_patterns_phi_6l(trigger_map(i))(0) downto i-trigger_patterns_phi_6l(trigger_map(i))(1)) 
            /= (i+trigger_patterns_phi_6l(trigger_map(i))(0) downto i-trigger_patterns_phi_6l(trigger_map(i))(1) => '0') then
       decision_phi_lay3(cha)(i) <= '1';
      else 
       decision_phi_lay3(cha)(i) <= '0';
      end if; 
      end loop;
      
      for cha in 0 to 5 loop 
        if file_phi_hits_BM1(cha)(i+trigger_patterns_phi_4l(trigger_map(i))(0) downto i-trigger_patterns_phi_4l(trigger_map(i))(1)) 
        /= (i+trigger_patterns_phi_4l(trigger_map(i))(0) downto i-trigger_patterns_phi_4l(trigger_map(i))(1) => '0') then
       decision_phi_lay2(cha)(i) <= '1';
        else 
         decision_phi_lay2(cha)(i) <= '0';
        end if;  
      end loop;            
   end loop;   

   for i in 20 to 87+20 loop 
      for cha in 0 to 9 loop   
       if file_phi_hits_BI(cha)(i+trigger_patterns_phi_1l(trigger_map(i))(0) downto i-trigger_patterns_phi_1l(trigger_map(i))(1)) 
      /= (i+trigger_patterns_phi_1l(trigger_map(i))(0) downto i-trigger_patterns_phi_1l(trigger_map(i))(1) => '0') then
       decision_phi_lay1(cha)(i) <= '1';
       else 
       decision_phi_lay1(cha)(i) <= '0';
       end if;
      end loop;
    end loop;
    
    
    

for pT_index in 0 to 3 loop
--end loop;
    for i in 20 to 492  loop

      if file_eta_hits_BO(i+trigger_patterns_8l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_8l_3d(pT_index)(trigger_map(i))(1)) /= (i+trigger_patterns_8l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_8l_3d(pT_index)(trigger_map(i))(1) => '0') then
       decision_lay4(pT_index)(i) <= '1';
      else 
       decision_lay4(pT_index)(i) <= '0';
      end if; 

      if file_eta_hits_BM2(i+trigger_patterns_6l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_6l_3d(pT_index)(trigger_map(i))(1)) /= (i+trigger_patterns_6l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_6l_3d(pT_index)(trigger_map(i))(1) => '0') then
       decision_lay3(pT_index)(i) <= '1';
      else 
       decision_lay3(pT_index)(i) <= '0';
      end if; 

      if file_eta_hits_BM1(i+trigger_patterns_4l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_4l_3d(pT_index)(trigger_map(i))(1)) /= (i+trigger_patterns_4l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_4l_3d(pT_index)(trigger_map(i))(1) => '0') then
       decision_lay2(pT_index)(i) <= '1';
      else 
       decision_lay2(pT_index)(i) <= '0';
      end if; 

      if file_eta_hits_BI(i+trigger_patterns_1l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_1l_3d(pT_index)(trigger_map(i))(1)) /= (i+trigger_patterns_1l_3d(pT_index)(trigger_map(i))(0) downto i-trigger_patterns_1l_3d(pT_index)(trigger_map(i))(1) => '0') then
       decision_lay1(pT_index)(i) <= '1';
      else 
       decision_lay1(pT_index)(i) <= '0';
      end if; 
--
    end loop;
   end loop; -- close the loop on the pT
 -- end if; --close if on the rising edge


decision_lay1_1 <= decision_lay1;
decision_lay1_2 <= decision_lay1_1;
decision_lay1_3 <= decision_lay1_2;

decision_lay2_1 <= decision_lay1;
decision_lay2_2 <= decision_lay1_1;
decision_lay2_3 <= decision_lay1_2;

decision_lay3_1 <= decision_lay1;
decision_lay3_2 <= decision_lay1_1;
decision_lay3_3 <= decision_lay1_2;

decision_lay4_1 <= decision_lay1;
decision_lay4_2 <= decision_lay1_1;
decision_lay4_3 <= decision_lay1_2;



--end if; -- close if for the candidate_pT indicating the number of pT threshold to be run
    --now increase the candidate number to check the following pT cut

--end process;




--P_INCREMENT1 : process (clock_h)
--begin
  -- 200 MHz clock --
  
-- if rising_edge(clock_h) then
 
 
  for i in 20 to 80+20 loop 
    for cha in 0 to 9 loop
        -- define a map on phi to make them compatible -- will have to also define BI/BP -- shall have a windor
        trigger_decision_phi(cha)(i) <=  
       (decision_phi_lay3(map_phi_BM2(cha))(i) 
       AND decision_phi_lay2(map_phi_BM1(cha))(i) AND decision_phi_lay1(cha)(i)) OR 
       (decision_phi_lay4(map_phi_BO(cha))(i) AND decision_phi_lay2(map_phi_BM1(cha))(i) AND decision_phi_lay1(cha)(i)) OR 
       (decision_phi_lay4(map_phi_BO(cha))(i) AND decision_phi_lay3(map_phi_BM2(cha))(i) AND decision_phi_lay1(cha)(i)) OR 
       (decision_phi_lay4(map_phi_BO(cha))(i) AND decision_phi_lay2(map_phi_BM1(cha))(i) AND decision_phi_lay3(map_phi_BM2(cha))(i));


     end loop;
   end loop;
 
  -- eta trigger coincidence
 

 
 
    --initialize-- what to do with boundary cells? --
  for pT_index in 0 to 3 loop
    for i in 20 to 492  loop
        -- second layer BI  == 10 --
      
      -- Aggiungere 3/4 - per ora messo tutto assieme
--      trigger_decision_pre(pT_index)(i)  <=  decision_lay4(pT_index)(i) AND decision_lay3(pT_index)(i) AND decision_lay2(pT_index)(i) AND decision_lay1(pT_index)(i);

       trigger_decision_pre(pT_index)(i)  <=  (decision_lay3(pT_index)(i) 
       AND decision_lay2(pT_index)(i) AND decision_lay1(pT_index)(i)) OR 
       (decision_lay4(pT_index)(i) AND decision_lay2(pT_index)(i) AND decision_lay1(pT_index)(i)) OR 
       (decision_lay4(pT_index)(i) AND decision_lay3(pT_index)(i) AND decision_lay1(pT_index)(i)) OR 
       (decision_lay4(pT_index)(i) AND decision_lay2(pT_index)(i) AND decision_lay3(pT_index)(i));
       

    end loop;
   end loop;
    
    
   trigger_decision_pre_1 <= trigger_decision_pre;
    
   -- define intermediate decision on phi for each chamber
   trigger_decision_phi_chamber(0)   <= or trigger_decision_phi(0)(80+20 downto 20);
   trigger_decision_phi_chamber(1)   <= or trigger_decision_phi(0)(80+20 downto 20);
   trigger_decision_phi_chamber(2)   <= or trigger_decision_phi(1)(80+20 downto 20);
   trigger_decision_phi_chamber(3)   <= or trigger_decision_phi(2)(80+20 downto 20);
   trigger_decision_phi_chamber(4)   <= or trigger_decision_phi(3)(80+20 downto 20);
   trigger_decision_phi_chamber(5)   <= or trigger_decision_phi(4)(80+20 downto 20);
   trigger_decision_phi_chamber(6)   <= or trigger_decision_phi(5)(80+20 downto 20);
   trigger_decision_phi_chamber(7)   <= or trigger_decision_phi(6)(80+20 downto 20);
   trigger_decision_phi_chamber(8)   <= or trigger_decision_phi(7)(80+20 downto 20);
   trigger_decision_phi_chamber(9)   <= or trigger_decision_phi(8)(80+20 downto 20);
   trigger_decision_phi_chamber(10)  <= or trigger_decision_phi(9)(80+20 downto 20);
   trigger_decision_phi_chamber(11)  <= or trigger_decision_phi(9)(80+20 downto 20);

   
    
   -- implement eta-phi--
   for pT_index in 0 to 3 loop
    for i in 20 to 492  loop
        --this needs changing
        if trigger_decision_pre_1(pT_index)(i) = '1' AND (trigger_decision_phi_chamber(trigger_map_phi(i)+1)  = '1' 
        OR trigger_decision_phi_chamber(trigger_map_phi(i)) = '1' 
        OR trigger_decision_phi_chamber(trigger_map_phi(i)+2) = '1') then
       -- if trigger_decision_pre(pT_index)(i) = '1'  AND (trigger_decision_phi(trigger_map_phi(i))(119+20 downto 20) /= (119+20 downto 20 => '0') 
       -- OR trigger_decision_phi(trigger_map_phi(i)+1)(119+20 downto 20) /= (119+20 downto 20 => '0')
       -- OR trigger_decision_phi(trigger_map_phi(i)-1)(119+20 downto 20) /= (119+20 downto 20 => '0')) then  
        trigger_decision(pT_index)(i) <= '1';
        else 
         trigger_decision(pT_index)(i) <= '0';
       end if;
       
    end loop;
   end loop;



    --different clock cycle to order the candidates -
       --new clock cycle, get now the  
   for pT_index in 0 to 3 loop

   -- if trigger_decision(pT_index) /= (512 downto 0 => '0') then
     for i in 20 to 492  loop
        if trigger_decision(pT_index)(i) = '1' then
         coin_type(decision_lay1_3(pT_index)(i), decision_lay2_3(pT_index)(i), decision_lay3_3(pT_index)(i), decision_lay4_3(pT_index)(i),priority(pT_index)(i));
         else
         priority(pT_index)(i) <= 0;
      end if;
    end loop;
  --  end if;
   end loop;


      --assign the corresponding priority 
    for pT_index in 0 to 3 loop
     for i in 20 to 492  loop
         if priority(pT_index)(i) = 6 then
            trig_cand_5(pT_index)(i) <= '1';
            trig_cand_0(pT_index)(i) <= '0';
            trig_cand_1(pT_index)(i) <= '0';
            trig_cand_2(pT_index)(i) <= '0';
            trig_cand_3(pT_index)(i) <= '0';
            trig_cand_4(pT_index)(i) <= '0';
         elsif priority(pT_index)(i) = 5 then
            trig_cand_4(pT_index)(i) <= '1';
            trig_cand_0(pT_index)(i) <= '0';
            trig_cand_1(pT_index)(i) <= '0';
            trig_cand_2(pT_index)(i) <= '0';
            trig_cand_3(pT_index)(i) <= '0';
            trig_cand_5(pT_index)(i) <= '0';
         elsif priority(pT_index)(i) = 4 then
            trig_cand_3(pT_index)(i) <= '1';
            trig_cand_0(pT_index)(i) <= '0';
            trig_cand_1(pT_index)(i) <= '0';
            trig_cand_2(pT_index)(i) <= '0';
            trig_cand_4(pT_index)(i) <= '0';
            trig_cand_5(pT_index)(i) <= '0';
         elsif priority(pT_index)(i) = 3 then
            trig_cand_2(pT_index)(i) <= '1';
            trig_cand_0(pT_index)(i) <= '0';
            trig_cand_1(pT_index)(i) <= '0';
            trig_cand_3(pT_index)(i) <= '0';
            trig_cand_4(pT_index)(i) <= '0';
            trig_cand_5(pT_index)(i) <= '0';
         elsif priority(pT_index)(i) = 2 then
            trig_cand_1(pT_index)(i) <= '1';
            trig_cand_0(pT_index)(i) <= '0';
            trig_cand_2(pT_index)(i) <= '0';
            trig_cand_3(pT_index)(i) <= '0';
            trig_cand_4(pT_index)(i) <= '0';
            trig_cand_5(pT_index)(i) <= '0';
         elsif priority(pT_index)(i) = 1 then
            trig_cand_0(pT_index)(i) <= '1';
            trig_cand_1(pT_index)(i) <= '0';
            trig_cand_2(pT_index)(i) <= '0';
            trig_cand_3(pT_index)(i) <= '0';
            trig_cand_4(pT_index)(i) <= '0';
            trig_cand_5(pT_index)(i) <= '0';
         else
            trig_cand_0(pT_index)(i) <= '0';
            trig_cand_1(pT_index)(i) <= '0';
            trig_cand_2(pT_index)(i) <= '0';
            trig_cand_3(pT_index)(i) <= '0';
            trig_cand_4(pT_index)(i) <= '0';
            trig_cand_5(pT_index)(i) <= '0';
        end if;
       end loop;
      end loop;



--    --running declustering for each priority separate first
   for pT_index in 0 to 3 loop

    if trigger_decision(pT_index) /= (512 downto 0 => '0') then
     global_trig(pT_index) <= '1';
    else 
     global_trig(pT_index) <= '0';
    end if;
 
    for i in 20 to 492  loop
       trig_decl_cand_5(pT_index)(i) <= trigger_decluster_7(trig_cand_5(pT_index)(i-3),trig_cand_5(pT_index)(i-2),trig_cand_5(pT_index)(i-1),trig_cand_5(pT_index)(i),trig_cand_5(pT_index)(i+1),trig_cand_5(pT_index)(i+2),trig_cand_5(pT_index)(i+3));
       trig_decl_cand_4(pT_index)(i) <= trigger_decluster_7(trig_cand_4(pT_index)(i-3),trig_cand_4(pT_index)(i-2),trig_cand_4(pT_index)(i-1),trig_cand_4(pT_index)(i),trig_cand_4(pT_index)(i+1),trig_cand_4(pT_index)(i+2),trig_cand_4(pT_index)(i+3));
       trig_decl_cand_3(pT_index)(i) <= trigger_decluster_7(trig_cand_3(pT_index)(i-3),trig_cand_3(pT_index)(i-2),trig_cand_3(pT_index)(i-1),trig_cand_3(pT_index)(i),trig_cand_3(pT_index)(i+1),trig_cand_3(pT_index)(i+2),trig_cand_3(pT_index)(i+3));
       trig_decl_cand_2(pT_index)(i) <= trigger_decluster_7(trig_cand_2(pT_index)(i-3),trig_cand_2(pT_index)(i-2),trig_cand_2(pT_index)(i-1),trig_cand_2(pT_index)(i),trig_cand_2(pT_index)(i+1),trig_cand_2(pT_index)(i+2),trig_cand_2(pT_index)(i+3));
       trig_decl_cand_1(pT_index)(i) <= trigger_decluster_7(trig_cand_1(pT_index)(i-3),trig_cand_1(pT_index)(i-2),trig_cand_1(pT_index)(i-1),trig_cand_1(pT_index)(i),trig_cand_1(pT_index)(i+1),trig_cand_1(pT_index)(i+2),trig_cand_1(pT_index)(i+3));
       trig_decl_cand_0(pT_index)(i) <= trigger_decluster_7(trig_cand_0(pT_index)(i-3),trig_cand_0(pT_index)(i-2),trig_cand_0(pT_index)(i-1),trig_cand_0(pT_index)(i),trig_cand_0(pT_index)(i+1),trig_cand_0(pT_index)(i+2),trig_cand_0(pT_index)(i+3));
     end loop;
    end loop;
   


--    --run second level of declustering
   for pT_index in 0 to 3 loop
    for i in 20 to 492  loop
        --TODO: watch out this is not great at the boundary. Need to increase the size at the boundary by 3 on both side
        trig_cand(pT_index)(i) <= trigger_decluster_7_global(trig_decl_cand_5(pT_index)(i+3 downto i-3),trig_decl_cand_4(pT_index)(i+3 downto i-3),trig_decl_cand_3(pT_index)(i+3 downto i-3),trig_decl_cand_2(pT_index)(i+3 downto i-3),trig_decl_cand_1(pT_index)(i+3 downto i-3),trig_decl_cand_0(pT_index)(i+3 downto i-3));
    end loop;
   end loop;



      -- need here to define the four candidates -- so first divide into 8 eta towers - then select only 4 candidates 
      -- the assumption here is that for each tower there is only one candidate and it is chosen independently from the priority 

     for pT_index in 0 to 3 loop
     
       tower_cand(pT_index)(0) <= select_candidate_w50(trig_cand(pT_index)(63  downto   0));  
       tower_cand(pT_index)(1) <= select_candidate_w50(trig_cand(pT_index)(127  downto   64));  
       tower_cand(pT_index)(2) <= select_candidate_w50(trig_cand(pT_index)(191  downto   128));  
       tower_cand(pT_index)(3) <= select_candidate_w50(trig_cand(pT_index)(255  downto   192));  
       tower_cand(pT_index)(4) <= select_candidate_w50(trig_cand(pT_index)(319  downto   256));  
       tower_cand(pT_index)(5) <= select_candidate_w50(trig_cand(pT_index)(383  downto   320));  
       tower_cand(pT_index)(6) <= select_candidate_w50(trig_cand(pT_index)(447  downto   384));  
       tower_cand(pT_index)(7) <= select_candidate_w50(trig_cand(pT_index)(511  downto   448));  -- tower 8 -- missing 1 correct


       tower_cand_2(pT_index)(0) <= select_candidate_w50(trig_cand(pT_index)(63  downto   0));  
       tower_cand_2(pT_index)(1) <= select_candidate_w50(trig_cand(pT_index)(127  downto   64));  
       tower_cand_2(pT_index)(2) <= select_candidate_w50(trig_cand(pT_index)(191  downto   128));  
       tower_cand_2(pT_index)(3) <= select_candidate_w50(trig_cand(pT_index)(255  downto   192));  
       tower_cand_2(pT_index)(4) <= select_candidate_w50(trig_cand(pT_index)(319  downto   256));  
       tower_cand_2(pT_index)(5) <= select_candidate_w50(trig_cand(pT_index)(383  downto   320));  
       tower_cand_2(pT_index)(6) <= select_candidate_w50(trig_cand(pT_index)(447  downto   384));  
       tower_cand_2(pT_index)(7) <= select_candidate_w50(trig_cand(pT_index)(511  downto   448)); 

       tower_cand_3(pT_index)(0) <= select_candidate_w50(trig_cand(pT_index)(63  downto   0));  
       tower_cand_3(pT_index)(1) <= select_candidate_w50(trig_cand(pT_index)(127  downto   64));  
       tower_cand_3(pT_index)(2) <= select_candidate_w50(trig_cand(pT_index)(191  downto   128));  
       tower_cand_3(pT_index)(3) <= select_candidate_w50(trig_cand(pT_index)(255  downto   192));  
       tower_cand_3(pT_index)(4) <= select_candidate_w50(trig_cand(pT_index)(319  downto   256));  
       tower_cand_3(pT_index)(5) <= select_candidate_w50(trig_cand(pT_index)(383  downto   320));  
       tower_cand_3(pT_index)(6) <= select_candidate_w50(trig_cand(pT_index)(447  downto   384));  
       tower_cand_3(pT_index)(7) <= select_candidate_w50(trig_cand(pT_index)(511  downto   448)); 

       tower_cand_4(pT_index)(0) <= select_candidate_w50(trig_cand(pT_index)(63  downto   0));  
       tower_cand_4(pT_index)(1) <= select_candidate_w50(trig_cand(pT_index)(127  downto   64));  
       tower_cand_4(pT_index)(2) <= select_candidate_w50(trig_cand(pT_index)(191  downto   128));  
       tower_cand_4(pT_index)(3) <= select_candidate_w50(trig_cand(pT_index)(255  downto   192));  
       tower_cand_4(pT_index)(4) <= select_candidate_w50(trig_cand(pT_index)(319  downto   256));  
       tower_cand_4(pT_index)(5) <= select_candidate_w50(trig_cand(pT_index)(383  downto   320));  
       tower_cand_4(pT_index)(6) <= select_candidate_w50(trig_cand(pT_index)(447  downto   384));  
       tower_cand_4(pT_index)(7) <= select_candidate_w50(trig_cand(pT_index)(511  downto   448)); 

       tower_cand_5(pT_index)(0) <= select_candidate_w50(trig_cand(pT_index)(63  downto   0));  
       tower_cand_5(pT_index)(1) <= select_candidate_w50(trig_cand(pT_index)(127  downto   64));  
       tower_cand_5(pT_index)(2) <= select_candidate_w50(trig_cand(pT_index)(191  downto   128));  
       tower_cand_5(pT_index)(3) <= select_candidate_w50(trig_cand(pT_index)(255  downto   192));  
       tower_cand_5(pT_index)(4) <= select_candidate_w50(trig_cand(pT_index)(319  downto   256));  
       tower_cand_5(pT_index)(5) <= select_candidate_w50(trig_cand(pT_index)(383  downto   320));  
       tower_cand_5(pT_index)(6) <= select_candidate_w50(trig_cand(pT_index)(447  downto   384));  
       tower_cand_5(pT_index)(7) <= select_candidate_w50(trig_cand(pT_index)(511  downto   448)); 

                 
     end loop;

     --select the 4 candidates, order in pT first -- from one to the other I need to copy the register, change this to make it sure
     
     -- fai nuovo segnale tower_cand --
     priority_encoder(tower_cand(0),tower_cand(1),tower_cand(2),tower_cand(3),tower_cand_2(0),
     tower_cand_2(1),tower_cand_2(2),tower_cand_2(3),f_cand(0),pT_triggered(0));
     
     priority_encoder(tower_cand_2(0),tower_cand_2(1),tower_cand_2(2),tower_cand_2(3),tower_cand_3(0),
     tower_cand_3(1),tower_cand_3(2),tower_cand_3(3),f_cand(1),pT_triggered(1));
     
     priority_encoder(tower_cand_3(0),tower_cand_3(1),tower_cand_3(2),tower_cand_3(3),tower_cand_4(0),
     tower_cand_4(1),tower_cand_4(2),tower_cand_4(3),f_cand(2),pT_triggered(2));
     
     priority_encoder(tower_cand_4(0),tower_cand_4(1),tower_cand_4(2),tower_cand_4(3),tower_cand_5(0),
     tower_cand_5(1),tower_cand_5(2),tower_cand_5(3),f_cand(3),pT_triggered(3));
     -- eta coordinate  
     cand_1(13 downto 4) <= std_logic_vector(to_unsigned(f_cand(0),10));
     cand_2(13 downto 4) <= std_logic_vector(to_unsigned(f_cand(1),10));
     cand_3(13 downto 4) <= std_logic_vector(to_unsigned(f_cand(2),10));  
     cand_4(13 downto 4) <= std_logic_vector(to_unsigned(f_cand(3),10));  
     -- phi coordinate         
     cand_1(21 downto 14) <= std_logic_vector(to_unsigned(trigger_map_phi(f_cand(0)),8));
     cand_2(21 downto 14) <= std_logic_vector(to_unsigned(trigger_map_phi(f_cand(1)),8));
     cand_3(21 downto 14) <= std_logic_vector(to_unsigned(trigger_map_phi(f_cand(2)),8));  
     cand_4(21 downto 14) <= std_logic_vector(to_unsigned(trigger_map_phi(f_cand(3)),8));    
     -- pT of the candidate taken as the highest pT 
     cand_1(25 downto 22) <= std_logic_vector(to_unsigned(pT_triggered(0),4));
     cand_2(25 downto 22) <= std_logic_vector(to_unsigned(pT_triggered(1),4));
     cand_3(25 downto 22) <= std_logic_vector(to_unsigned(pT_triggered(2),4));
     cand_4(25 downto 22) <= std_logic_vector(to_unsigned(pT_triggered(3),4));
    
     if f_cand(0) > 32 then
        clos_index_lay1(0) <= closest_hit_fix_range(file_eta_hits_BI_13(f_cand(0)+32 downto f_cand(0)-32));
        clos_index_lay2(0) <= closest_hit_fix_range(file_eta_hits_BM1_13(f_cand(0)+32 downto f_cand(0)-32));
        clos_index_lay3(0) <= closest_hit_fix_range(file_eta_hits_BM2_13(f_cand(0)+32 downto f_cand(0)-32));
        clos_index_lay4(0) <= closest_hit_fix_range(file_eta_hits_BO_13(f_cand(0)+32 downto f_cand(0)-32));
        cand_1(56 downto 43) <= std_logic_vector(to_unsigned(clos_index_lay1(0),14));
        cand_1(70 downto 57) <= std_logic_vector(to_unsigned(clos_index_lay2(0),14));
        cand_1(84 downto 71) <= std_logic_vector(to_unsigned(clos_index_lay3(0),14));
        cand_1(98 downto 85) <= std_logic_vector(to_unsigned(clos_index_lay4(0),14));
        --pT_triggered(0)
        cand_1(2 downto 1)<= std_logic_vector(to_unsigned(1,2));
        cand_1(3) <= '1'; 
--        clos_index_lay1_half(0) <= closest_hit_fix_range_half(file_eta_hits_BI(f_cand(0)+32 downto f_cand(0)-32));
--       clos_index_lay2_half(0) <= closest_hit_fix_range_half(file_eta_hits_BM1(f_cand(0)+32 downto f_cand(0)-32));
--        clos_index_lay3_half(0) <= closest_hit_fix_range_half(file_eta_hits_BM2(f_cand(0)+32 downto f_cand(0)-32));
--        clos_index_lay4_half(0) <= closest_hit_fix_range_half(file_eta_hits_BO(f_cand(0)+32 downto f_cand(0)-32));       
     else
      cand_1(0) <= '0';  
      cand_1(3) <= '0'; 
     end if;
     
 
--  --  
    if f_cand(1) > 32 then
       clos_index_lay1(1) <= closest_hit_fix_range(file_eta_hits_BI_13(f_cand(1)+32 downto f_cand(1)-32));
       clos_index_lay2(1) <= closest_hit_fix_range(file_eta_hits_BM1_13(f_cand(1)+32 downto f_cand(1)-32));
       clos_index_lay3(1) <= closest_hit_fix_range(file_eta_hits_BM2_13(f_cand(1)+32 downto f_cand(1)-32));
       clos_index_lay4(1) <= closest_hit_fix_range(file_eta_hits_BO_13(f_cand(1)+32 downto f_cand(1)-32));
       cand_2(56 downto 43) <= std_logic_vector(to_unsigned(clos_index_lay1(1),14));
       cand_2(70 downto 57) <= std_logic_vector(to_unsigned(clos_index_lay2(1),14));
       cand_2(84 downto 71) <= std_logic_vector(to_unsigned(clos_index_lay3(1),14));
       cand_2(98 downto 85) <= std_logic_vector(to_unsigned(clos_index_lay4(1),14));
       cand_2(2 downto 1)<= std_logic_vector(to_unsigned(1,2));
       cand_2(3) <= '1';
       
       else
         cand_2(0) <= '0';
         
         cand_2(3) <= '0';
  --     clos_index_lay1_half(1) <= closest_hit_fix_range_half(file_eta_hits_BI(f_cand(0)+32 downto f_cand(0)-32));
  --     clos_index_lay2_half(1) <= closest_hit_fix_range_half(file_eta_hits_BM1(f_cand(0)+32 downto f_cand(0)-32));
  --     clos_index_lay3_half(1) <= closest_hit_fix_range_half(file_eta_hits_BM2(f_cand(0)+32 downto f_cand(0)-32));
  --     clos_index_lay4_half(1) <= closest_hit_fix_range_half(file_eta_hits_BO(f_cand(0)+32 downto f_cand(0)-32)); 
       
    end if;
   if f_cand(2) > 32 then
       clos_index_lay1(2) <= closest_hit_fix_range(file_eta_hits_BI_13(f_cand(2)+32 downto f_cand(2)-32));
        clos_index_lay2(2) <= closest_hit_fix_range(file_eta_hits_BM1_13(f_cand(2)+32 downto f_cand(2)-32));
      clos_index_lay3(2) <= closest_hit_fix_range(file_eta_hits_BM2_13(f_cand(2)+32 downto f_cand(2)-32));
      clos_index_lay4(2) <= closest_hit_fix_range(file_eta_hits_BO_13(f_cand(2)+32 downto f_cand(2)-32));
      cand_3(56 downto 43) <= std_logic_vector(to_unsigned(clos_index_lay1(2),14));
      cand_3(70 downto 57) <= std_logic_vector(to_unsigned(clos_index_lay2(2),14));
      cand_3(84 downto 71) <= std_logic_vector(to_unsigned(clos_index_lay3(2),14));
      cand_3(98 downto 85) <= std_logic_vector(to_unsigned(clos_index_lay4(2),14));
      cand_3(2 downto 1)<= std_logic_vector(to_unsigned(1,2));
      cand_3(3) <= '1';
     else
       cand_3(0) <= '0';   
       cand_3(3) <= '0';  
      
  --      clos_index_lay1_half(2) <= closest_hit_fix_range_half(file_eta_hits_BI(f_cand(0)+32 downto f_cand(0)-32));
  --      clos_index_lay2_half(2) <= closest_hit_fix_range_half(file_eta_hits_BM1(f_cand(0)+32 downto f_cand(0)-32));
  --      clos_index_lay3_half(2) <= closest_hit_fix_range_half(file_eta_hits_BM2(f_cand(0)+32 downto f_cand(0)-32));
  --      clos_index_lay4_half(2) <= closest_hit_fix_range_half(file_eta_hits_BO(f_cand(0)+32 downto f_cand(0)-32)); 
    end if;
   if f_cand(3) > 32 then
         clos_index_lay1(3) <= closest_hit_fix_range(file_eta_hits_BI_13(f_cand(3)+32 downto f_cand(3)-32));
         clos_index_lay2(3) <= closest_hit_fix_range(file_eta_hits_BM1_13(f_cand(3)+32 downto f_cand(3)-32));
         clos_index_lay3(3) <= closest_hit_fix_range(file_eta_hits_BM2_13(f_cand(3)+32 downto f_cand(3)-32));
         clos_index_lay4(3) <= closest_hit_fix_range(file_eta_hits_BO_13(f_cand(3)+32 downto f_cand(3)-32));
         cand_4(56 downto 43) <= std_logic_vector(to_unsigned(clos_index_lay1(3),14));
         cand_4(70 downto 57) <= std_logic_vector(to_unsigned(clos_index_lay2(3),14));
         cand_4(84 downto 71) <= std_logic_vector(to_unsigned(clos_index_lay3(3),14));
         cand_4(98 downto 85) <= std_logic_vector(to_unsigned(clos_index_lay4(3),14));
         cand_4(2 downto 1)<= std_logic_vector(to_unsigned(1,2));
         cand_4(3) <= '1';
         
               
  --      clos_index_lay1_half(3) <= closest_hit_fix_range_half(file_eta_hits_BI(f_cand(0)+32 downto f_cand(0)-32));
  --      clos_index_lay2_half(3) <= closest_hit_fix_range_half(file_eta_hits_BM1(f_cand(0)+32 downto f_cand(0)-32));
  --      clos_index_lay3_half(3) <= closest_hit_fix_range_half(file_eta_hits_BM2(f_cand(0)+32 downto f_cand(0)-32));
  --      clos_index_lay4_half(3) <= closest_hit_fix_range_half(file_eta_hits_BO(f_cand(0)+32 downto f_cand(0)-32)); 
    else
     cand_4(0) <= '0';
     cand_4(3) <= '0';
    end if;
 
 
 
  
 -- convert to 128 bits 
 -- convert phi, convert eta , pT
 
 
 
 
  --   if(clos_index_lay1(0) > 0) then
  --    clos_index_lay1_final(0) <= clos_index_lay1(0);
  --   else 
  --    clos_index_lay1_final(0) <= clos_index_lay1_half(0);
  --   end if;
  --   
  --   if(clos_index_lay1(1) > 0) then
  --    clos_index_lay1_final(1) <= clos_index_lay1(1);
  --   else 
  --    clos_index_lay1_final(1) <= clos_index_lay1_half(1);
  --   end if;   
  --   
  --   if(clos_index_lay1(2) > 0) then
  --    clos_index_lay1_final(2) <= clos_index_lay1(2);
  --   else 
  --    clos_index_lay1_final(2) <= clos_index_lay1_half(2);
  --   end if;     
  --   if(clos_index_lay1(3) > 0) then
  --    clos_index_lay1_final(3) <= clos_index_lay1(3);
  --   else 
  --    clos_index_lay1_final(3) <= clos_index_lay1_half(3);
  --   end if;  
 
 
          
   --  if(clos_index_lay2(0) > 0) then
   --   clos_index_lay2_final(0) <= clos_index_lay2(0);
   --  else 
   --   clos_index_lay2_final(0) <= clos_index_lay2_half(0);
   --  end if;
   --  
   --  if(clos_index_lay2(1) > 0) then
   --   clos_index_lay2_final(1) <= clos_index_lay2(1);
   --  else 
   --   clos_index_lay2_final(1) <= clos_index_lay2_half(1);
   --  end if;   
   --  
   --  if(clos_index_lay2(2) > 0) then
   --   clos_index_lay2_final(2) <= clos_index_lay2(2);
   --  else 
   --   clos_index_lay2_final(2) <= clos_index_lay2_half(2);
   --  end if;     
   --  if(clos_index_lay2(3) > 0) then
   --   clos_index_lay2_final(3) <= clos_index_lay2(3);
   --  else 
   --   clos_index_lay2_final(3) <= clos_index_lay2_half(3);
   --  end if;   


 
    
    
--     for n_cand in 0 to 3 loop
--      if pT_triggered(n_cand) /= 4 then
--       closest_hit_fix_range(trigger_patterns_1l_3d(pT_triggered(n_cand))(trigger_map(f_cand(n_cand)))(0),f_cand(n_cand),file_eta_hits_BI,clos_index_lay1(n_cand));
--       closest_hit_fix_range(trigger_patterns_4l_3d(pT_triggered(n_cand))(trigger_map(f_cand(n_cand)))(0),f_cand(n_cand),file_eta_hits_BM1,clos_index_lay2(n_cand));
--       closest_hit_fix_range(trigger_patterns_6l_3d(pT_triggered(n_cand))(trigger_map(f_cand(n_cand)))(0),f_cand(n_cand),file_eta_hits_BM2,clos_index_lay3(n_cand));
--       closest_hit_fix_range(trigger_patterns_8l_3d(pT_triggered(n_cand))(trigger_map(f_cand(n_cand)))(0),f_cand(n_cand),file_eta_hits_BO,clos_index_lay4(n_cand));
--      else 
--       f_cand(n_cand) <= 0 ; --shal we give another default value?
--      end if;
--     end loop;


--   for pT_index in 0 to 3 loop
--
--    if global_trig(pT_index) = '1' then
--    -- count the candidates
--    for i in 20 to 492  loop
--    --for i in 200 to 220  loop
--      -- window, pivot indexing, full list of hits, closest index (output)
--      if trig_cand(pT_index)(i) = '1' then
--        if decision_lay1(pT_index)(i) = '1' then
--        --closest_hit_(trigger_patterns_1l_3d(0)(trigger_map(i))(0),i,file_eta_hits_l1,clos_index_lay1(i));
--          closest_hit_fix_range(trigger_patterns_1l_3d(pT_index)(trigger_map(i))(0),i,file_eta_hits_BI,clos_index_lay1(pT_index)(i));
--        else
--        clos_index_lay1(pT_index)(i) <= 0;
--       end if;
--       if decision_lay2(pT_index)(i) = '1' then
--           closest_hit_fix_range(trigger_patterns_2l_3d(pT_index)(trigger_map(i))(0),i,file_eta_hits_BM1,clos_index_lay2(pT_index)(i));
--       else
--        clos_index_lay2(pT_index)(i) <= 0;
--       end if;
--       if decision_lay3(pT_index)(i) = '1' then
--           closest_hit_fix_range(trigger_patterns_3l_3d(pT_index)(trigger_map(i))(0),i,file_eta_hits_BM2,clos_index_lay3(pT_index)(i));
--       else
--        clos_index_lay3(pT_index)(i) <= 0;
--       end if;
--       if decision_lay4(pT_index)(i)  = '1' then
--           closest_hit_fix_range(trigger_patterns_4l_3d(pT_index)(trigger_map(i))(0),i,file_eta_hits_BO,clos_index_lay4(pT_index)(i));
--       else
--        clos_index_lay4(pT_index)(i) <= 0;
--       end if;
--        if decision_lay5(i)  = '1'  then
--        closest_hit_fix_range(i,file_eta_hits_l5,clos_index_lay5(i));
--        else
--        clos_index_lay5(i) <= 0;
--        end if;
--        if decision_lay6(i)  = '1'  then
--        closest_hit_fix_range(i,file_eta_hits_l6,clos_index_lay6(i));
--        else
--        clos_index_lay6(i) <= 0;
--        end if;
--        if decision_lay7(i)  = '1' then
--        closest_hit_fix_range(i,file_eta_hits_l7,clos_index_lay7(i));
--        else
--        clos_index_lay7(i) <= 0;
--        end if;
--        if decision_lay8(i)  = '1' then
--        closest_hit_fix_range(i,file_eta_hits_l8,clos_index_lay8(i));
--        else
--        clos_index_lay8(i) <= 0;
--        end if;
--        if decision_lay9(i)  = '1'  then
--        closest_hit_fix_range(i,file_eta_hits_l9,clos_index_lay9(i));
--        else
--        clos_index_lay9(i) <= 0;
--        end if;
--      end if;
--    end loop;

--    end if; 
--   end loop;


-----------------uncomment here

--we now need to choose the charge of the muon
--first check the outermost layers
--but we need to store the index associated to the layer, ask Riccardo 


-- now we need to choose which hit we pass on  
   
--     for i in 20 to 492  loop
--       if  trig_cand(i)  => '1' then 
--         --the windows in the last layer BO are the largest so use that as a proxy and for now we assume right and left windows are the same
--         for j in 0 to trigger_patterns_9l(trigger_map(i))(0):
--         --write here a look up table as well  
--
--        --get the window  and then get the candidate for each layer
--    


 
  end if; --close if on the rising edge
end process;

end Behavioral_sim;
