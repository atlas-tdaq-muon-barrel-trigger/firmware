----------------------------------------------------------------------------------
-- Company: Istituto Nazionale di Fisica Nucleare, Sezione di Roma
-- Engineer: Valerio Ippolito
-- 
-- Create Date: 02/10/2023 10:32:00 AM
-- Design Name: 
-- Module Name: tb_trigalgotest_forimpl - Structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Implementation test bench to speed up development of trig algorithm
--              This version implements only the trigger entity as defined in the
--              SLtrigger file. In case you run this in simulation mode, you need
--              to comment out the part which generates the clock. Please note that
--              there is another dedicated test bench for simulation, which implements
--              a readout from file of the input hits.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- operation with std_logic_vector
use IEEE.math_real.uniform; -- rand in [0,1)

-- load current code
library work;
use work.trigalgopkg.all;

library xil_defaultlib;
use xil_defaultlib.my_lib.all;

use IEEE.std_logic_textio.all;
use std.textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_SLtrigger_forimpl is
    Port (
        clock_h : in std_logic; -- clock
        dummy_out : out std_logic_vector(1 downto 0) -- dummy output to enable implementation
        );
end top_SLtrigger_forimpl;

architecture Structural of top_SLtrigger_forimpl is
    signal eta_BI : t_ETASTRIP_BI := (others => '0'); -- init eta strips with no hits
    signal eta_BM1 : t_ETASTRIP_BM1 := (others => '0');
    signal eta_BM2 : t_ETASTRIP_BM2 := (others => '0');
    signal eta_BO : t_ETASTRIP_BO := (others => '0');
    signal phi_BI    :  t_PHISTRIP_BI := (others=> (others => '0'));
    signal phi_BM1   :   t_PHISTRIP_BM1 := (others=> (others => '0'));
    signal phi_BM2   :  t_PHISTRIP_BM2 := (others=> (others => '0')); 
    signal phi_BO    :  t_PHISTRIP_BO := (others=> (others => '0'));
    signal cand_1 : t_CANDIDATE := (others => '0');
    signal cand_2 : t_CANDIDATE := (others => '0');
    signal count : std_logic_vector(0 to 3) := (others => '0');
    signal sr_count : integer range 0 to c_CAND_WMAX := 0;
    signal cand_1_sr : t_CANDIDATE_DELAYED(c_CAND_WMAX downto 0);
    signal cand_2_sr : t_CANDIDATE_DELAYED(c_CAND_WMAX downto 0);
--  signal clock_h : std_logic := '0';

    component SLtrigger is
    Port ( file_eta_hits_BI_orig    :  in t_ETASTRIP_BI; 
           file_eta_hits_BM1_orig   :  in t_ETASTRIP_BM1; 
           file_eta_hits_BM2_orig   :  in t_ETASTRIP_BM2;
           file_eta_hits_BO_orig    :  in t_ETASTRIP_BO;
           file_phi_hits_BI_orig    :  in t_PHISTRIP_BI; 
           file_phi_hits_BM1_orig   :  in t_PHISTRIP_BM1;
           file_phi_hits_BM2_orig   :  in t_PHISTRIP_BM2; 
           file_phi_hits_BO_orig    :  in t_PHISTRIP_BO; 
           cand_1    :  out   t_CANDIDATE;
           cand_2    :  out   t_CANDIDATE;
           clock_h    : in std_logic);
    end component;

begin
--  CLOCKSIM : process
--  begin
--      clock_h <= '0';
--      wait for 5 ns;
--      clock_h <= '1';
--      wait for 5 ns;
--  end process;

    INPUT_FILL: process(clock_h)
    begin
        if rising_edge(clock_h) then
            count <= count + 1;
            if count = "0001" then
                eta_BI <= (c_ETASTRIP_BI_MAX-3 downto 0 => '0') & "111";
                eta_BM1 <= (c_ETASTRIP_BM1_MAX-3 downto 0 => '0') & "111";
                eta_BM2 <= (c_ETASTRIP_BM2_MAX-3 downto 0 => '0') & "111";
                eta_BO <= (c_ETASTRIP_BO_MAX-3 downto 0 => '0') & "111";
                phi_BI <= (others => ((c_PHISTRIP_BI_MAX-3 downto 0 => '0') & "111"));
                phi_BM1 <= (others => ((c_PHISTRIP_BM1_MAX-3 downto 0 => '0') & "111"));
                phi_BM2 <= (others => ((c_PHISTRIP_BM2_MAX-3 downto 0 => '0') & "111"));
                phi_BO <= (others => ((c_PHISTRIP_BO_MAX-3 downto 0 => '0') & "111"));
            elsif count = "1111" then
                count <= "0000";
            else
                eta_BI <= eta_BI(c_ETASTRIP_BI_MAX-3 downto 0) & eta_BI(c_ETASTRIP_BI_MAX downto c_ETASTRIP_BI_MAX-2); -- shift the 111
                eta_BM1 <= eta_BM1(c_ETASTRIP_BM1_MAX-3 downto 0) & eta_BM1(c_ETASTRIP_BM1_MAX downto c_ETASTRIP_BM1_MAX-2);
                eta_BM2 <= eta_BM2(c_ETASTRIP_BM2_MAX-3 downto 0) & eta_BM2(c_ETASTRIP_BM2_MAX downto c_ETASTRIP_BM2_MAX-2);
                eta_BO <= eta_BO(c_ETASTRIP_BO_MAX-3 downto 0) & eta_BO(c_ETASTRIP_BO_MAX downto c_ETASTRIP_BO_MAX-2);
                for i in 0 to c_CHAMBER_BI_MAX loop
                    phi_BI(i) <= phi_BI(i)(c_PHISTRIP_BI_MAX-3 downto 0) & phi_BI(i)(c_PHISTRIP_BI_MAX downto c_PHISTRIP_BI_MAX-2);
                end loop;
                for i in 0 to c_CHAMBER_BM1_MAX loop
                    phi_BM1(i) <= phi_BM1(i)(c_PHISTRIP_BM1_MAX-3 downto 0) & phi_BM1(i)(c_PHISTRIP_BM1_MAX downto c_PHISTRIP_BM1_MAX-2);
                end loop;
                for i in 0 to c_CHAMBER_BM2_MAX loop
                    phi_BM2(i) <= phi_BM2(i)(c_PHISTRIP_BM2_MAX-3 downto 0) & phi_BM2(i)(c_PHISTRIP_BM2_MAX downto c_PHISTRIP_BM2_MAX-2);
                end loop;
                for i in 0 to c_CHAMBER_BO_MAX loop
                    phi_BO(i) <= phi_BO(i)(c_PHISTRIP_BO_MAX-3 downto 0) & phi_BO(i)(c_PHISTRIP_BO_MAX downto c_PHISTRIP_BO_MAX-2);
                end loop;
            end if;
        end if;
    end process;

    -- here we store each candidate for c_CAND_WMAX clock cycles
    -- so that we can output their bits one at a time
    SHIFTREG: process(clock_h)
    begin
        if rising_edge(clock_h) then
            -- store the last c_CAND_WMAX candidates in a shift register
            cand_1_sr <= cand_1_sr(cand_1_sr'high-1 downto cand_1_sr'low) & cand_1;
            cand_2_sr <= cand_1_sr(cand_2_sr'high-1 downto cand_2_sr'low) & cand_2;
            
            -- counter of which bit of these candidates should be output
            if sr_count < c_CAND_WMAX then
                sr_count <= sr_count + 1;
            else
                sr_count <= 0;
            end if;
        end if;
    end process;

    -- here we connect one FPGA output ports per candidate to the chosen eta bin
    DUMMYER: process(clock_h)
    begin
        if rising_edge(clock_h) then
            dummy_out(0) <= cand_1_sr(cand_1_sr'high)(sr_count);
            dummy_out(1) <= cand_2_sr(cand_2_sr'high)(sr_count);
        end if;
    end process;


    -- DUT
    -- instantiate top level
    pmap_SLtrigger : SLtrigger
            port map (
            file_eta_hits_BI_orig => eta_BI,
            file_eta_hits_BM1_orig => eta_BM1,
            file_eta_hits_BM2_orig => eta_BM2,
            file_eta_hits_BO_orig => eta_BO,
            file_phi_hits_BI_orig => phi_BI,
            file_phi_hits_BM1_orig => phi_BM1,
            file_phi_hits_BM2_orig => phi_BM2,
            file_phi_hits_BO_orig => phi_BO,
            clock_h => clock_h,
            cand_1 => cand_1,
            cand_2 => cand_2
        );
end Structural;
