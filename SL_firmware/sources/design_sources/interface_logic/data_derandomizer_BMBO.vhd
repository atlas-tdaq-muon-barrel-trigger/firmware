-------------------------------------------------------
-- File:          data_derandomizer_BMBO.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2022/07/30
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;

entity data_derandomizer_BMBO is
    generic (logic_index : integer_0to16);
    port (
        clock_in      : in  std_logic;
        reset_in      : in  std_logic;
        stripID_in    : in integer_0to127;
        coordinate_in : in std_logic;
        BCID_data_in  : in integer_0to3563;
        data_out      : out std_logic_vector(143 downto 0)  -- 80 bits for phi strips, 64 bits for eta strips 
    );
end data_derandomizer_BMBO;

architecture RTL of data_derandomizer_BMBO is

    signal eta_hit_data : std_logic_vector(63 downto 0);
    signal phi_hit_data : std_logic_vector(79 downto 0);
    
begin

    -- coordinate index:
    -- 0 -> eta strip (64 in total)
    -- 1 -> phi strip (80 in total)
   
    
    process (clock_in)
    begin
        if rising_edge(clock_in) then
            if reset_in = '1' then
                eta_hit_data <= (others => '0');
                phi_hit_data <= (others => '0');
            else 
                if BCID_data_in = logic_index then
                    if coordinate_in = '0' then
                        if stripID_in < 64 then
                            eta_hit_data(stripID_in) <= '1';
                        end if;
                    else
                      if stripID_in < 80 then
                          phi_hit_data(stripID_in) <= '1';
                      end if;
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    data_out <= phi_hit_data & eta_hit_data;
    
    

end RTL;
