-------------------------------------------------------
-- File:          uplink_data_divider.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2022/07/30
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;





entity uplink_data_divider is
    port (
        clock_in            : in  std_logic;
        uplink_user_data_in : in  std_logic_vector(223 downto 0); 
        uplink_clk_en_in    : in  std_logic;
        word_28_out         : out std_logic_vector(27 downto 0) 
    );
end uplink_data_divider;




architecture RTL of uplink_data_divider is

    signal uplink_user_data_r : std_logic_vector(223 downto 0);
    signal division_counter : std_logic_vector(2 downto 0) := "000";
    
begin

        process(clock_in)
        begin
            if rising_edge(clock_in) then
                if uplink_clk_en_in = '1' then
                    division_counter <= (others => '0');
                else
                    division_counter <= division_counter - 1;
                end if;

                uplink_user_data_r <= uplink_user_data_in;
                word_28_out <= uplink_user_data_r(27+28*(to_integer(unsigned(division_counter))) downto 28*(to_integer(unsigned(division_counter))) );
            end if;
        end process;

end RTL;
