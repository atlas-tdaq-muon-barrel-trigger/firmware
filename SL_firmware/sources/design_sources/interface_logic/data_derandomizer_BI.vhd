-------------------------------------------------------
-- File:          data_derandomizer_BI.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/17
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;

entity data_derandomizer_BI is
    generic (logic_index : integer_0to16);
    port (
        clock_in           : in  std_logic;
        reset_in           : in  std_logic;
        stripID_in         : in  integer_0to255;
        phi_coord          : in  integer_0to255;
        BCID_data_in       : in  integer_0to16;
        word_ID_in         : in  std_logic_vector(1 downto 0);
        eta_strip_data_out : out std_logic_vector(47 downto 0);   
        phi_strip_data_out : out std_logic_vector(127 downto 0)
    );
end data_derandomizer_BI;

architecture RTL of data_derandomizer_BI is

    signal eta_strip_data : std_logic_vector(143 downto 0);
    signal phi_strip_data : array_3x128b;  
    
begin


    -- strip index (BI-DCT data):  
    -- 47-0:    eta first layer
    -- 95-48:   eta second layer
    -- 143-96:  eta third layer
    
    
    
    process (clock_in)
    begin
        if rising_edge(clock_in) then
            if reset_in = '1' then
                eta_strip_data <= (others => '0');
                phi_strip_data <= (others => (others => '0'));
            else 
                if BCID_data_in  = logic_index and word_ID_in = "01" then --accept BI data only if it's 1st word
                    if stripID_in < 48 then 
                        eta_strip_data(stripID_in) <= '1';
                        phi_strip_data(0)(phi_coord) <= '1';
                    elsif stripID_in < 96 then 
                        eta_strip_data(stripID_in) <= '1';
                        phi_strip_data(1)(phi_coord) <= '1';
                    elsif stripID_in < 144 then 
                        eta_strip_data(stripID_in) <= '1';
                        phi_strip_data(2)(phi_coord) <= '1';
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    
            
    
  coincidence_2over3 : process(clock_in)
    begin
        if rising_edge(clock_in) then
        
        -- require a hit in at least 2 layers out of 3
       
            for k in 0 to 47  loop  
                eta_strip_data_out(k) <= (eta_strip_data(k) AND eta_strip_data(k+48)) OR 
                                         (eta_strip_data(k) AND eta_strip_data(k+96)) OR
                                         (eta_strip_data(k+48) AND eta_strip_data(k+96));
            end loop;
                
            for k in 0 to 127 loop
                phi_strip_data_out(k) <= (phi_strip_data(0)(k) AND phi_strip_data(1)(k)) OR 
                                         (phi_strip_data(0)(k) AND phi_strip_data(2)(k)) OR
                                         (phi_strip_data(1)(k) AND phi_strip_data(2)(k));
            end loop;
        end if;
    end process;

end RTL;
