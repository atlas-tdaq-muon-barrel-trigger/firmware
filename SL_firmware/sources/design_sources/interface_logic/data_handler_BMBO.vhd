-------------------------------------------------------
-- File:          data_handler_BMBO.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2022/07/30
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;



entity data_handler_BMBO is
    port (
        clock_in       : in  std_logic;                     -- 320 MHz clock (uplink clock)
        BCID_in        : in  std_logic_vector(11 downto 0);
        word_28_in     : in  std_logic_vector(27 downto 0); -- 1 b coordinate (eta/phi) + 7 b strip ID + 10 b BCID + 5 b rising time first layer + 5 b rising time second layer @ 320 MHz
        read_enable_in : in  std_logic;
        data_out       : out std_logic_vector(143 downto 0) --  80 bits for phi strips, 64 bits for eta strips.  If the bit is 1, there is a hit in the corresponding strip
    );
end data_handler_BMBO;

architecture RTL of data_handler_BMBO is

    component data_derandomizer_BMBO is
        generic (logic_index : integer_0to16);
        port (
            clock_in      : in std_logic;
            reset_in      : in std_logic;
            stripID_in    : in integer_0to127;
            coordinate_in : in std_logic; 
            BCID_data_in  : in integer_0to16;
            data_out      : out std_logic_vector(143 downto 0)
        );
    end component;
    
    type array_17x144b is array(16 downto 0) of std_logic_vector(143 downto 0);
    signal derandomized_data  : array_17x144b;
    signal derandomizer_reset : std_logic_vector(16 downto 0);
    signal bcid_tmp           : std_logic_vector(11 downto 0) := (others => '0');
    signal reset_index        : integer_0to16 := 0;
    signal out_index          : integer_0to16;
    signal stripID            : integer_0to79:= 0;
    signal BCID_data          : integer_0to16:= 0;
    signal data               : std_logic_vector(143 downto 0);
    signal coordinate         : std_logic;
       
begin
      
    GEN_data_derandomizer_BMBO : for i in 0 to 16 generate
        data_derandomizer_BMBO_inst : data_derandomizer_BMBO
        generic map (logic_index => i)
        port map(
            clock_in      => clock_in,
            reset_in      => derandomizer_reset(i),
            stripID_in    => stripID,
            coordinate_in => coordinate,
            BCID_data_in  => BCID_data,
            data_out      => derandomized_data(i)
        );
    end generate;
          
    bcid_tmp <= BCID_in(11 downto 10) & word_28_in(19 downto 10); --problemi vicino al valore massimo. fai tipo un mod deb
    
    process(clock_in)
    begin
        if rising_edge(clock_in) then
        
            stripID <= to_integer(unsigned(word_28_in(26 downto 20)));
            coordinate <= word_28_in(27);
            BCID_data <= to_integer(unsigned(bcid_tmp)) mod 17;
            out_index <= (to_integer(unsigned(BCID_in - 21))) mod 17; --assume DCT data can arrive to SL with a latency between 5 and 20 BCs 
            reset_index <= out_index; 
            
            if read_enable_in = '1' then
                data <= derandomized_data(out_index);   --problemi all'inizio e alla fine dei possibili valori del BC di LHC per sovrapposizioni logiche
            else
                data <= data;
            end if;
            derandomizer_reset <= (others=>'0');
            derandomizer_reset(reset_index) <= '1';
        end if;
    end process;
    
    data_out <= data;
    
end RTL;
