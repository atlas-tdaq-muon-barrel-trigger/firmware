-------------------------------------------------------
-- File:          data_handler_BI.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/17
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;


entity data_handler_BI is
    port (
        clock_in             : in std_logic;                       -- 320 MHz
        BCID_in              : in std_logic_vector(11 downto 0);
        word_28_in           : in std_logic_vector(27 downto 0);   
        read_enable_in       : in std_logic;
        eta_data_out         : out std_logic_vector(47 downto 0);  -- If the bit is 1, there is a hit in the corresponding strip
        phi_data_out         : out std_logic_vector(127 downto 0)
    );
end data_handler_BI;


architecture RTL of data_handler_BI is

    -- BI DCT data format (2 consecutive 28-bit words)
    -- 1st word: 2-bit word ID ("01") + 8-bit BCID + 8-bit strip ID + 8-bit virtual phi coordinate  + 2-bit right falling time (first 2 bits of the 6 MSB)
    -- 2nd word: 2-bit word ID ("10") + 8-bit left rising time + 6-bit (MSB) left falling time + 8-bit right rising time + 4-bit right falling time (last 4 bits of the 6 MSB)

    component data_derandomizer_BI is
        generic (logic_index : integer_0to16);
        port (
            clock_in           : in  std_logic;
            reset_in           : in  std_logic;
            stripID_in         : in integer_0to255;
            phi_coord          : in integer_0to255;
            BCID_data_in       : in integer_0to16;
            word_ID_in         : in std_logic_vector(1 downto 0);
            eta_strip_data_out : out std_logic_vector(47 downto 0);   
            phi_strip_data_out : out std_logic_vector(127 downto 0)
        );
    end component;
    
    
    type array_17x48b is array(16 downto 0) of std_logic_vector(47 downto 0);
    type array_17x128b is array(16 downto 0) of std_logic_vector(127 downto 0);
    signal derandomized_eta_strip_data : array_17x48b;
    signal derandomized_phi_strip_data : array_17x128b;
    signal derandomizer_reset : std_logic_vector(16 downto 0);
    signal reset_index : integer_0to16;
    signal out_index : integer_0to16;
    signal stripID : integer_0to255;
    signal BCID_data : integer_0to16;
    signal bcid_tmp : std_logic_vector(11 downto 0);
    signal eta_data : std_logic_vector(47 downto 0); 
    signal phi_data : std_logic_vector(127 downto 0);    
    signal bcid_subtraction : std_logic_vector(11 downto 0);
    signal phi_coord : integer_0to255;
    signal word_ID   : std_logic_vector(1 downto 0);
    signal read_enable_r : std_logic;
    
    begin
        
        
    GEN_data_derandomizer_BI : for i in 0 to 16 generate
      data_derandomizer_BI_inst : data_derandomizer_BI
        generic map (logic_index => i)
        port map(
            clock_in           => clock_in,
            reset_in           => derandomizer_reset(i),
            stripID_in         => stripID,
            phi_coord          => phi_coord,
            BCID_data_in       => BCID_data,
            word_ID_in         => word_ID,
            eta_strip_data_out => derandomized_eta_strip_data(i),
            phi_strip_data_out => derandomized_phi_strip_data(i)
            
        );
    end generate;
    
    bcid_tmp <= BCID_in(11 downto 8) & word_28_in(25 downto 18);
    
    process(clock_in)   
    begin
        if rising_edge(clock_in) then
            word_ID   <= word_28_in(27 downto 26);
            stripID   <= to_integer(unsigned(word_28_in(17 downto 10)));
            phi_coord <= to_integer(unsigned(word_28_in(9 downto 3))); -- using 7 most signif. bits of phi coord for now 
            BCID_data <= to_integer(unsigned(bcid_tmp)) mod 17;
            bcid_subtraction <= BCID_in  - 21;  
            out_index <= (to_integer(unsigned(bcid_subtraction))) mod 17; 
            --out_index <= (to_integer(unsigned(BCID_in))  - 21) mod 17;
            reset_index <= out_index;

            read_enable_r <= read_enable_in;
            if read_enable_r = '1' then
                eta_data <= derandomized_eta_strip_data(out_index);
                phi_data <= derandomized_phi_strip_data(out_index);
            else
                eta_data <= eta_data;
                phi_data <= phi_data;
            end if;
            derandomizer_reset <= (others=>'0');
            derandomizer_reset(reset_index) <= '1';
            
         end if;
    end process;
    
    eta_data_out <= eta_data;
    phi_data_out <= phi_data;
        
   
  end RTL;
