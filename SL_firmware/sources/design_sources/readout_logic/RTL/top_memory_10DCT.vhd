-- project: SLReadout
-- top_memory_10DCT.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

entity top_memory_10DCT is
    Port ( clock : in std_logic;
           data_in : array_slv27to0x10DCT;
           read_BCID : in std_logic_vector(8 downto 0);
           FIFO_L0A_empty : in std_logic;
           data_out : out std_logic_vector(31 downto 0);
           tm_FIFO_L0A_read_en : out std_logic );
end top_memory_10DCT;


architecture Behavioral of top_memory_10DCT is

signal data_out_bus : array_slv31to0x10DCT;
signal read_en_bus : array_slx10DCT := (others => '0');
signal read_done_bus : array_slx10DCT;


component multiplexer_dOut_10DCTto1 is
    Port ( clock : in std_logic;
           key : in std_logic_vector(4 downto 0);
           sgn_in : in array_slv31to0x10DCT;
           sgn_out : out std_logic_Vector(31 downto 0) );
end component;

component memory_block is
    Port ( clock : in std_logic;
           data_in : in std_logic_vector(27 downto 0);
           read_BCID : in std_logic_vector(8 downto 0);
           SL_BCID : in std_logic_vector(11 downto 0);
           read_en : in std_logic;
           data_out : out std_logic_vector(31 downto 0);
           read_done : out std_logic
           );
end component;

signal nRAM : std_logic_vector(4 downto 0) := "11111";
signal nRAM_del1clk : std_logic_vector(4 downto 0);
signal nRAM_del2clk : std_logic_vector(4 downto 0);
signal nRAM_del3clk : std_logic_vector(4 downto 0);
signal nRAM_del4clk : std_logic_vector(4 downto 0);
signal nRAM_del5clk : std_logic_vector(4 downto 0);
signal nRAM_del6clk : std_logic_vector(4 downto 0); -- verificare se necessario

type state_type is (idle, ST0, ST1);
signal CS : state_type := idle;

type array_slv11to0x10 is array (9 downto 0) of std_logic_vector(11 downto 0);
signal SL_BCID   : array_slv11to0x10;

begin

gen_SL_BCID: for I in 0 to 9 generate
    SL_BCID(I) <= "000" & data_in(I)(18 downto 10);
end generate;

gen_memory_block : for I in 0 to 9 generate
    m_memory_block : memory_block port map (
        clock => clock,
        data_in => data_in(I),
        read_BCID => read_BCID,
        SL_BCID => SL_BCID(I),
        read_en => read_en_bus(I),
        data_out => data_out_bus(I),
        read_done => read_done_bus(I)
        );
end generate gen_memory_block;


pmap_multiplexer_dOut : multiplexer_dOut_10DCTto1 port map (
    clock => clock,
    key => nRAM_del6clk,
    sgn_in => data_out_bus,
    sgn_out => data_out );


-- FSM to cycle through memory_blocks. Think about removing ST1... [G. for now I think ST1 it's OK]
retrieve_data : process(clock)
begin
    if rising_edge(clock) then
        case CS is
            when idle =>
                tm_FIFO_L0A_read_en <= '0';
                nRAM <= "11111";
                if FIFO_L0A_empty = '0' then
                    nRAM <= "00000";
                    tm_FIFO_L0A_read_en <= '1';
                    CS <= ST0;
                end if;
            when ST0 =>
                tm_FIFO_L0A_read_en <= '0';
                if read_done_bus(to_integer(unsigned(nRAM))) = '1' then
                    read_en_bus(to_integer(unsigned(nRAM))) <= '0';
                    CS <= ST1;
                else
                    read_en_bus(to_integer(unsigned(nRAM))) <= '1';
                    CS <= ST0;
                end if;
            when ST1 =>
                if to_integer(unsigned(nRAM)) = 9 then
                    CS <= idle;
                else
                    nRAM <= nRAM +1;
                    CS <= ST0;
                end if;
        end case;
    end if;
end process;


nRAM_del_gen : process(clock)
begin   
    if rising_edge(clock) then
        nRAM_del1clk <= nRAM;
        nRAM_del2clk <= nRAM_del1clk;
        nRAM_del3clk <= nRAM_del2clk;
        nRAM_del4clk <= nRAM_del3clk;
        nRAM_del5clk <= nRAM_del4clk;
        nRAM_del6clk <= nRAM_del5clk;
    end if;
end process;




end Behavioral;
