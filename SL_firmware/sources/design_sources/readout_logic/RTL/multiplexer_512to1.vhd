-- project: SLReadout
-- multiplexer_512to1.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto:  giovanni.padovano@cern.ch
--          matteo.bauce@cern.ch
--          riccardo.vari@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;


entity multiplexer_512to1 is
    Port ( clock : in std_logic;
           key : in std_logic_vector(8 downto 0);
           sgn_in : in array_slv3to0x512;
           sgn_out : out std_logic_vector(3 downto 0) );
end multiplexer_512to1;

architecture Behavioral of multiplexer_512to1 is

begin

multiplexer_gen : process(clock) 
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            if I = to_integer(unsigned(key)) then
                sgn_out <= sgn_in(I);
            end if;
        end loop;
    end if;
end process;

end Behavioral;
