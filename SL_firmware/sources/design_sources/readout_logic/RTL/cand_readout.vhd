-- project: SLReadout
-- cand_readout.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma & Sapienza universita
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

library work;
use work.all;

entity cand_readout is
    Port ( clock     : in std_logic;
           bcid_BM_L : in std_logic_vector(11 downto 0);
           reset     : in std_logic;
           L0A       : in std_logic;
           din       : in std_logic_vector(511 downto 0);
           dout      : out std_logic_vector(511 downto 0) );
end cand_readout;

architecture Behavioral of cand_readout is

    signal trig_BCID : std_logic_vector(8 downto 0);
    signal SL_BCID : std_logic_vector(11 downto 0);
    signal read_BCID : std_logic_vector(8 downto 0);
    
    signal FIFO_L0A_read_en : std_logic;
    signal FIFO_L0A_write_en : std_logic;
    signal FIFO_L0A_full : std_logic;
    signal FIFO_L0A_empty : std_logic;
    signal RAM_write_en : std_logic;
    signal RAM_write_en_del1clk : std_logic;
    signal RAM_WEA : std_logic_vector(0 downto 0);
    signal null_512 : std_logic_vector(511 downto 0) := (others => '0');

    
    
    component RAM_512bx512 is
        port (
            --Inputs - Port A
            ENA   : IN STD_LOGIC;  --opt port
            WEA   : IN STD_LOGIC_VECTOR(0 downto 0);
            ADDRA : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
            DINA  : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
            CLKA  : IN STD_LOGIC;
            
            --Input - Port B
            ENB   : IN STD_LOGIC;  --opt port
            ADDRB : IN STD_LOGIC_VECTOR(8 downto 0);
            DOUTB : OUT STD_LOGIC_VECTOR(511 downto 0);
            CLKB  : IN STD_LOGIC
            
        );
    end component;
    
    component FIFO_L0A
        Port ( clk : IN STD_LOGIC;
               srst : IN STD_LOGIC;
               din : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
               wr_en : IN STD_LOGIC;
               rd_en : IN STD_LOGIC;
               dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
               full : OUT STD_LOGIC;
               empty : OUT STD_LOGIC;
               wr_rst_busy : OUT STD_LOGIC;
               rd_rst_busy : OUT STD_LOGIC );
    end component;

begin

    RAM_512bx512_inst : RAM_512bx512
        port map(
          ENA   => RAM_write_en_del1clk,
          WEA   => RAM_WEA,
          ADDRA => SL_BCID(8 downto 0),
          DINA  => din,
          CLKA  => clock,
          ENB   => '1',
          ADDRB => read_BCID,
          DOUTB => dout,
          CLKB  => clock
        );
        
    FIFO_L0A_inst : FIFO_L0A
        port map(
          clk         => clock,
          srst        => reset,
          din         => trig_BCID,
          wr_en       => FIFO_L0A_write_en,
          rd_en       => FIFO_L0A_read_en,
          dout        => read_BCID,
          full        => FIFO_L0A_full,
          empty       => FIFO_L0A_empty,
          wr_rst_busy => open,
          rd_rst_busy => open
        );
        
    FIFO_L0A_write_en_gen : process(clock)
    begin
      if rising_edge(clock) then
          FIFO_L0A_write_en <= L0A;
      end if;
    end process;
    
    SL_BCID <= bcid_BM_L;
    
    trig_BCID_gen : process(clock)
    begin
      if rising_edge(clock) then
          trig_BCID <= SL_BCID(8 downto 0) - std_logic_vector(to_unsigned(400, trig_BCID'length));
      end if;
    end process;
    
    FIFO_L0A_read_en_gen : process(clock)
    begin
      if rising_edge(clock) then
          if FIFO_L0A_empty = '0' then
            FIFO_L0A_read_en <= '1';
          else 
            FIFO_L0A_read_en <= '0';
          end if;
      end if;
    end process;  
    
    RAM_write_en_gen : process(clock)
    begin
      if rising_edge(clock) then
          if din = null_512 then
            RAM_write_en <= '0';
          else
            RAM_write_en <= '1';
          end if;
      end if;
    end process;
    
    RAM_write_en_del1clk_gen : process(clock)
    begin
      if rising_edge(clock) then
          RAM_write_en_del1clk <= RAM_write_en;
      end if;
    end process;
    
    RAM_WEA_gen : process(clock)
    begin
      if rising_edge(clock) then
        if RAM_write_en = '1' then
            RAM_WEA <= "1";
        else
            RAM_WEA <= "0";
        end if;
      end if;
    end process;
    
    

end Behavioral;
