-- project: SLReadout
-- memory_block.vhd (source file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all; -- library for operations on std_logic_vector
use IEEE.numeric_std.all;

library xil_defaultlib; -- FIFO library
use xil_defaultlib.my_library.all;

entity memory_block is
  Port ( clock : in std_logic;
         data_in : in std_logic_vector(27 downto 0);
         read_BCID : in std_logic_vector(8 downto 0);
         SL_BCID : in std_logic_vector(11 downto 0);
         read_en : in std_logic;
         data_out : out std_logic_vector(31 downto 0);
         read_done : out std_logic );
end memory_block;

architecture Behavioral of memory_block is

signal hit_counter_bus : array_slv3to0x512 := (others => (others => '0'));
signal hit_counter : std_logic_vector(3 downto 0);
signal address_in : std_logic_vector(12 downto 0) := (others => '0');
signal address_rst : std_logic_vector(8 downto 0) := (others => '0');
signal data_in_del1clk : std_logic_vector(27 downto 0);
signal address_out : std_logic_vector(12 downto 0);
signal nHit : std_logic_vector(3 downto 0) := (others => '0');
signal nHitMax : std_logic_vector(3 downto 0);
signal data_toRAM : std_logic_vector(31 downto 0);
signal read_BCID_cp : std_logic_vector(8 downto 0);
signal read_BCID_cp_del1clk : std_logic_vector(8 downto 0);
signal RAM_write_en : std_logic := '0';
signal RAM_write_en_del1clk : std_logic := '0';
signal RAM_WEA : std_logic_vector(0 downto 0);
signal SL_BCID_del1clk : std_logic_vector(11 downto 0);
signal m_read_en : std_logic;
signal m_read_done : std_logic;
signal good_output : std_logic := '0';
signal good_output_del1clk : std_logic;
signal good_output_del2clk : std_logic; -- verificare se necessario
signal m_data_out : std_logic_vector(31 downto 0);



type state_type is (idle, subidle, ST0);
signal CS : state_type := idle;

component RAM_32bx8192
  PORT (
      --Inputs - Port A
    ENA            : IN STD_LOGIC;  --opt port
    WEA            : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    ADDRA          : IN STD_LOGIC_VECTOR(12 DOWNTO 0);
    DINA           : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    CLKA           : IN STD_LOGIC;

      --Inputs - Port B
    ENB            : IN STD_LOGIC;  --opt port
    ADDRB          : IN STD_LOGIC_VECTOR(12 DOWNTO 0);
    DOUTB          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    CLKB           : IN STD_LOGIC

  );
end component;

component multiplexer_512to1
  Port ( clock : in std_logic;
         key : in std_logic_vector(8 downto 0);
         sgn_in : in array_slv3to0x512;
         sgn_out : out std_logic_vector(3 downto 0) );
end component;

begin


m_RAM32bx8192 : RAM_32bx8192 port map (
    ENA => RAM_write_en_del1clk,
    WEA => RAM_WEA,
    ADDRA => address_in,
    DINA => data_toRAM,
    CLKA => clock,
    ENB => '1',
    ADDRB => address_out,
    DOUTB => m_data_out,
    CLKB => clock );

pmap_multiplexer_hit_counter : multiplexer_512to1 port map (
    clock => clock,
    key => data_in(18 downto 10),
    sgn_in => hit_counter_bus,
    sgn_out => hit_counter );
    
pmap_multiplexer_nHitMax : multiplexer_512to1 port map (
    clock => clock,
    key => read_BCID_cp,
    sgn_in => hit_counter_bus,
    sgn_out => nHitMax );

counter_handler : process(clock)
begin
    if rising_edge(clock) then
        for I in 0 to 511 loop
            if I = to_integer(unsigned(data_in(18 downto 10))) then
                --if not ( data_in(27 downto 19) = "111111111" ) then -- NEW condition for data from input-file
                if not ( data_in(27 downto 19) = "000000000" and data_in(9 downto 0) = "0000000000" ) then -- OLD condition when using data-generator
                    if to_integer(unsigned(hit_counter_bus(I))) < 15 then
                        hit_counter_bus(I) <= hit_counter_bus(I) + 1; -- increment counter if data is non-null and counter has not max. value of 15
                    end if;
                end if;
            elsif I = to_integer(unsigned(address_rst)) then 
                hit_counter_bus(I) <= (others => '0'); -- reset to cancel old pkgs. 
            end if;
        end loop;
    end if;
end process;

address_rst_gen : process(clock)
begin
    if rising_edge(clock) then
        address_rst <= SL_BCID(8 downto 0) - std_logic_vector(to_unsigned(480, address_rst'length));
    end if;
end process;

address_in_gen : process(clock)
begin
    if rising_edge(clock) then
        address_in <= data_in_del1clk(18 downto 10) & hit_counter;
    end if;
end process;

data_in_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        data_in_del1clk <= data_in;
    end if;
end process;


data_toRAM_gen : process(clock)
begin
    if rising_edge(clock) then
        data_toRAM <= "0" & data_in_del1clk(27 downto 19) & SL_BCID_del1clk(11 downto 9) & data_in_del1clk(18 downto 0);
    end if;
end process;


address_out <= read_BCID_cp_del1clk & nHit;


hit_serializer : process(clock) -- finite-state-machine to read pkgs of the same BCID
begin
    if rising_edge(clock) then
        case CS is
            when idle =>
                m_read_done <= '0';
                nHit <= (others => '0');
                if m_read_en = '1' then
                    CS <= subidle;
                end if;
            when subidle =>
                good_output <= '1';
                CS <= ST0; -- wait 1 clk to update nHitMax
            when ST0 =>
                if ( to_integer(unsigned(nHit)) >= (to_integer(unsigned(nHitMax))-1 ) ) then
                    good_output <= '0';
                end if;
                if ( to_integer(unsigned(nHit)) < (to_integer(unsigned(nHitMax)) ) ) then
                    nHit <= nHit + 1;
                    CS <= ST0;
                else
                    nHit <= (others => '0');
                    m_read_done <= '1';
                    CS <=idle;
                end if;         
        end case;
    end if;
end process;

read_done <= m_read_done;
m_read_en <= read_en and not m_read_done;

read_BCID_gen : process(clock)
begin
    if rising_edge(clock) then
        read_BCID_cp <= read_BCID;
    end if;
end process;

read_BCID_cp_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        read_BCID_cp_del1clk <= read_BCID_cp;
    end if;
end process;

RAM_write_en_gen : process(clock)
begin
    if rising_edge(clock) then
        if not ( data_in(27 downto 19) = "000000000" and data_in(9 downto 0) = "0000000000" ) then 
            RAM_write_en <= '1';
        else
            RAM_write_en <= '0';
        end if;
    end if;
end process;

RAM_write_en_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        RAM_write_en_del1clk <= RAM_write_en;
    end if;
end process;

RAM_WEA_gen : process(clock)
begin
    if rising_edge(clock) then
        if RAM_write_en = '1' then
            RAM_WEA <= "1";
        else
            RAM_WEA <= "0";
        end if;
    end if;
end process;


SL_BCID_del1clk_gen : process(clock)
begin
    if rising_edge(clock) then
        SL_BCID_del1clk <= SL_BCID;
    end if;
end process;

good_output_del_gen : process(clock)
begin
    if rising_edge(clock) then
        good_output_del1clk <= good_output;
        good_output_del2clk <= good_output_del1clk;
    end if;
end process;


data_out_gen : process(clock)
begin
    if rising_edge(clock) then
        if good_output_del2clk = '1' then
            data_out <= m_data_out;
        else
            data_out <= (others => '0');
        end if;
    end if;
end process;


end Behavioral;
