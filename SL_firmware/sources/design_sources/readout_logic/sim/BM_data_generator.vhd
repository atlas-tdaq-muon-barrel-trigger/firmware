-- project SLReadout
-- BM_data_generator.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch 


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

library xil_defaultlib;
use xil_defaultlib.my_library.all;

entity BM_data_generator is
    Port (clock : in std_logic;
          enable : in std_logic;
          DCT_data_bus : out array_slv27to0x19DCT);
end BM_data_generator;

architecture Behavioral of BM_data_generator is

signal mask_enable : std_logic_vector(18 downto 0) := (others => '1'); -- mask to enable only some hit_generator
signal m_enable : std_logic_vector(18 downto 0);

component Hit_generator is
    Port (
    CLK : in std_logic;
    ENA : in std_logic;
    CNT : in std_logic_vector(4 downto 0);
    DOUT : out std_logic_vector(27 downto 0) );
end component;


begin

gen_Hit_generator : for I in 0 to 18 generate
    m_enable(I) <= mask_enable(I) AND enable;
    m_Hit_generator : Hit_generator port map (
        CLK => clock,
        ENA => m_enable(I),
        CNT => std_logic_vector(to_unsigned(I,5)),
        DOUT => DCT_data_bus(I) );
end generate gen_Hit_generator; 

end Behavioral;
