-- project SLReadout
-- L0A_generator.vhd (simulation file)

-- authors: G. Padovano, M. Bauce, R. Vari - INFN Roma
-- mailto: giovanni.padovano@cern.ch
--         matteo.bauce@cern.ch
--         riccardo.vari@cern.ch 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity L0A_generator is
    Port (clock : in std_logic;
          enable : in std_logic;
          L0A : out std_logic );
end L0A_generator;


architecture Behavioral of L0A_generator is

begin
    L0A_gen : process
    begin
        L0A <= '0';
        wait for 996.875 ns;
        wait until rising_edge(clock);
        if enable = '1' then
            L0A <= '1';
        else
            L0A <= '0';
        end if;
        wait until rising_edge(clock);
        L0A <= '0';
    end process; 

end Behavioral;
