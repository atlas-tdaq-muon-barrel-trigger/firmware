-------------------------------------------------------
-- File:          top_SLR2.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/18
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.my_library.all;
use xil_defaultlib.trigalgopkg.all;


entity SLR2_top is
    Port (
        -- Differential reference clock inputs SLR2
        mgtrefclk0_x0y9_p               : in std_logic;
        mgtrefclk0_x0y9_n               : in std_logic;
        mgtrefclk0_x0y11_p              : in std_logic;
        mgtrefclk0_x0y11_n              : in std_logic;
        mgtrefclk0_x1y10_p              : in std_logic;
        mgtrefclk0_x1y10_n              : in std_logic;
        
        -- Serial data ports for DCT transceivers
        gtyrxn_DCT_in         : in std_logic_vector(19 downto 0);
        gtyrxp_DCT_in         : in std_logic_vector(19 downto 0);
        gtytxn_DCT_out        : out std_logic_vector(19 downto 0);
        gtytxp_DCT_out        : out std_logic_vector(19 downto 0);
  
        -- Serial data ports for other transceivers
        gtyrxn_TDAQ_in        : in std_logic_vector(5 downto 0);
        gtyrxp_TDAQ_in        : in std_logic_vector(5 downto 0);
        gtytxn_TDAQ_out       : out std_logic_vector(5 downto 0);
        gtytxp_TDAQ_out       : out std_logic_vector(5 downto 0);
                
        -- eta hits from BI DCTs                           
        eta_hits_BI_fromSLR1 : in t_ETASTRIP_BI_WIDER;
                                                           
        -- phi hits from BI DCTs                           
        phi_hits_BI_fromSLR1 : in t_PHISTRIP_BI;
        
        -- data to readout
        word_28_out      : out array_20x28b;
        word_28_SLR0_in  : in array_10x28b;
        word_28_SLR0_out : out array_10x28b;
        word_28_SLR1_in  : in array_20x28b;
        word_28_SLR1_out : out array_20x28b;
        word_trigger_SLR1_in  : in std_logic_vector(255 downto 0);
        word_trigger_SLR1_out : out std_logic_vector(255 downto 0);
        word_trigger_SLR2_out : out std_logic_vector(255 downto 0);
        
        clock_320_in     : in std_logic;
        clock_240_in     : in std_logic;
        clock_80_in      : in std_logic;
        clock_40_in      : in std_logic;
        reset            : in std_logic
    );
end SLR2_top;

architecture RTL of SLR2_top is   

    COMPONENT uplink_data_divider is
        port (
            clock_in            : in  std_logic;
            uplink_user_data_in : in  std_logic_vector(223 downto 0); 
            uplink_clk_en_in    : in  std_logic;
            word_28_out         : out std_logic_vector(27 downto 0) 
        );
    end COMPONENT;
    
    COMPONENT data_handler_BMBO is 
        port (
            clock_in       : in  std_logic;
            BCID_in        : in  std_logic_vector(11 downto 0);
            word_28_in     : in  std_logic_vector(27 downto 0);
            read_enable_in : in  std_logic;
            data_out       : out std_logic_vector(143 downto 0)
        );
    end COMPONENT;

    

  COMPONENT lpgbtfpga_downlink IS
   GENERIC(
        -- Expert parameters
        c_multicyleDelay              : integer RANGE 0 to 7 := 1; --3                      --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                  : integer := 2;              --8                      --! Clock ratio is clock_out / 40 (shall be an integer - E.g.: 320/40 = 8)
        c_outputWidth                 : integer                                             --! Transceiver's word size (Typically 32 bits)
   );
   PORT (
        -- Clocks
        clk_i                         : in  std_logic;                                      --! Downlink datapath clock (Transceiver Tx User clock, typically 320MHz)
        clkEn_i                       : in  std_logic;                                      --! Clock enable (1 pulse over 8 clock cycles when encoding runs @ 320Mhz)
        rst_n_i                       : in  std_logic;                                      --! Downlink reset SIGNAL (Tx ready from the transceiver)

        -- Down link
        userData_i                    : in  std_logic_vector(31 downto 0);                  --! Downlink data (User)
        ECData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink EC field
        ICData_i                      : in  std_logic_vector(1 downto 0);                   --! Downlink IC field

        -- Output
        mgt_word_o                    : out std_logic_vector((c_outputWidth-1) downto 0);   --! Downlink encoded frame (IC + EC + User Data + FEC)

        -- Configuration
        interleaverBypass_i           : in  std_logic;                                      --! Bypass downlink interleaver (test purpose only)
        encoderBypass_i               : in  std_logic;                                      --! Bypass downlink FEC (test purpose only)
        scramblerBypass_i             : in  std_logic;                                      --! Bypass downlink scrambler (test purpose only)

        -- Status
        rdy_o                         : out std_logic                                       --! Downlink ready status
   );
END COMPONENT;  

  COMPONENT lpgbtfpga_uplink IS
   GENERIC(
        -- General configuration
        DATARATE                        : integer RANGE 0 to 2;                               --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             : integer RANGE 0 to 2;                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                : integer RANGE 0 to 7 := 3;                          --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    : integer;                                            --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  : integer;                                            --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            : integer;                                            --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       : integer;                                            --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            : integer;                                            --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                : integer := 1;                                       --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               : integer := 40                                       --! Number of clock cycle required before being back in a stable state
   );
   PORT (
        -- Clock and reset
        uplinkClk_i                     : in  std_logic;                                      --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                : out std_logic;                                      --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   : in  std_logic;                                      --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      : in  std_logic_vector((c_mgtWordWidth-1) downto 0);  --! Input frame coming from the MGT

        -- Data
        userData_o                      : out std_logic_vector(229 downto 0);                 --! User output (decoded data). The payload size varies depending on the
                                                                                              --! datarate/FEC configuration:
                                                                                              --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                              --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                              --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                              --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        : out std_logic_vector(1 downto 0);                   --! EC field value received from the LpGBT
        IcData_o                        : out std_logic_vector(1 downto 0);                   --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             : in  std_logic;                                      --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              : in  std_logic;                                      --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               : in  std_logic;                                      --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               : out std_logic;                                      --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 : out std_logic_vector(229 downto 0);                 --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   : out std_logic_vector(1 downto 0);                   --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           : out std_logic;                                      --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              : out std_logic                                       --! Number of bit slip is even (required only for advanced applications)

   );
END COMPONENT;


component SLtrigger is
    Port ( file_eta_hits_BI_orig    :  in t_ETASTRIP_BI; 
           file_eta_hits_BM1_orig   :  in t_ETASTRIP_BM1; 
           file_eta_hits_BM2_orig   :  in t_ETASTRIP_BM2;
           file_eta_hits_BO_orig    :  in t_ETASTRIP_BO;
           file_phi_hits_BM1_orig   :  in t_PHISTRIP_BM1;
           file_phi_hits_BI_orig    :  in t_PHISTRIP_BI; 
           file_phi_hits_BM2_orig   :  in t_PHISTRIP_BM2; 
           file_phi_hits_BO_orig    :  in t_PHISTRIP_BO; 
           cand_1    :  out   t_CANDIDATE;
           cand_2    :  out   t_CANDIDATE;
           clock_h    : in std_logic);
 end component;

    component fifo_128b_cdc_320to240MHz is
      port (
        wr_clk : in std_logic;
        rd_clk : in std_logic;
        din    : in std_logic_vector(127 downto 0);
        wr_en  : in std_logic;
        rd_en  : in std_logic;
        dout   : out std_logic_vector(127 downto 0);
        full   : out std_logic;
        empty  : out std_logic
      );
    end component;
    
    
    COMPONENT fifo_16x28b is
        port (
            wr_clk   : IN STD_LOGIC;
            rd_clk   : IN STD_LOGIC;
            din   : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout  : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
            full  : OUT STD_LOGIC;
            empty : OUT STD_LOGIC
        );
    end COMPONENT;
    
    COMPONENT fifo_16x28b_sameclock is
        port (
            clk   : IN STD_LOGIC;
            din   : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout  : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
            full  : OUT STD_LOGIC;
            empty : OUT STD_LOGIC
        );
    end COMPONENT;
    
    COMPONENT fifo_16x256b is
        port (
            clk : IN STD_LOGIC;
            din    : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
            wr_en  : IN STD_LOGIC;
            rd_en  : IN STD_LOGIC;
            dout   : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
            full   : OUT STD_LOGIC;
            empty  : OUT STD_LOGIC
        );
    end COMPONENT;

  signal gtwiz_userclk_tx_reset_int_DCT              : std_logic;
  signal gtwiz_userclk_tx_active_int_DCT             : std_logic;
  signal gtwiz_userclk_rx_reset_int_DCT              : std_logic;
  signal gtwiz_reset_rx_done_int_DCT                 : std_logic;
  
  signal gtwiz_userclk_tx_reset_int_TDAQ             : std_logic;
  signal gtwiz_userclk_rx_reset_int_TDAQ             : std_logic;
  signal gtwiz_reset_rx_done_int_TDAQ                : std_logic_vector(1 downto 0);
  
  signal reset_gt_int_SLR2                           : std_logic;
    
  signal gtwiz_userdata_tx_int_DCT                   : std_logic_vector (639 downto 0);
  signal gtwiz_userdata_rx_int_DCT                   : std_logic_vector (639 downto 0);
  
  signal gtwiz_userdata_tx_int_TDAQ                  : std_logic_vector (191 downto 0);
  signal gtwiz_userdata_rx_int_TDAQ                  : std_logic_vector (191 downto 0);

  signal gtrefclk00_int_DCT             : std_logic_vector (4 downto 0);
  signal qpll0outclk_int_DCT            : std_logic_vector (4 downto 0);
  signal qpll0outrefclk_int_DCT         : std_logic_vector(4 downto 0);
  
  signal gtrefclk00_int_TDAQ            : std_logic_vector (1 downto 0);
  signal qpll0outclk_int_TDAQ           : std_logic_vector (1 downto 0);
  signal qpll0outrefclk_int_TDAQ        : std_logic_vector(1 downto 0);
  

  signal rxslide_int_DCT                : std_logic_vector(19 downto 0);
  signal gtpowergood_int_DCT            : std_logic_vector(19 downto 0);
  signal rxpmaresetdone_int_DCT         : std_logic_vector(19 downto 0);  
  signal txpmaresetdone_int_DCT         : std_logic_vector(19 downto 0);
  
  signal gtpowergood_int_TDAQ           : std_logic_vector(5 downto 0);
  signal rxpmaresetdone_int_TDAQ        : std_logic_vector(5 downto 0);  
  signal txpmaresetdone_int_TDAQ        : std_logic_vector(5 downto 0);
  
  signal mgtrefclk0_x0y9_int            : std_logic;
  signal mgtrefclk0_x0y11_int           : std_logic;
  signal mgtrefclk0_x1y10_int           : std_logic;
    
  signal downlink_clock_en_gen_SLR2     : std_logic;
  signal downlink_user_data_gen_SLR2    : std_logic_vector(31 downto 0);
  signal downlink_ec_data_gen_SLR2      : std_logic_vector(1 downto 0);
  signal downlink_ic_data_gen_SLR2      : std_logic_vector(1 downto 0);
  
  signal downlink_clock_en_SLR2         : std_logic_vector(19 downto 0);
  signal downlink_user_data_SLR2        : array_20x32b;
  signal downlink_ec_data_SLR2          : array_20x2b;
  signal downlink_ic_data_SLR2          : array_20x2b;
  signal downlink_ready_SLR2            : std_logic_vector(19 downto 0);
  
  signal uplink_user_data_SLR2          : array_20x230b;
  signal uplink_ic_data_SLR2            : array_20x2b;
  signal uplink_ec_data_SLR2            : array_20x2b; 
  signal uplink_dataCorrected_SLR2      : array_20x230b;
  signal uplink_IcCorrected_SLR2        : array_20x2b;
  signal uplink_EcCorrected_SLR2        : array_20x2b; 
  signal uplink_rdy_i_SLR2              : std_logic_vector(19 downto 0);
  signal uplink_clk_en_i_SLR2           : std_logic_vector(19 downto 0);
  
  signal word_28_int                    : array_20x28b;
  signal word_28_r                      : array_20x28b;
  signal bcid_ordered_data_SLR2         : array_20x144b;
  
  signal eta_hits_BI                    : t_ETASTRIP_BI;
  signal eta_hits_BM1                   : t_ETASTRIP_BM1;
  signal eta_hits_BM2                   : t_ETASTRIP_BM2;
  signal eta_hits_BO                    : t_ETASTRIP_BO;
  signal phi_hits_BM1                   : t_PHISTRIP_BM1 := (others => (others => '0'));
  signal phi_hits_BM2                   : t_PHISTRIP_BM2 := (others => (others => '0'));
  signal phi_hits_BO                    : t_PHISTRIP_BO := (others => (others => '0'));

  signal trigger_cand                   : array_2x128b;
  signal trigger_cand_r                 : array_2x128b;
  signal trigger_cand_toGTY             : array_2x128b;
  signal trigger_cand_to_readout        : array_2x128b;  
  signal cdc_trigger_write_enable       : std_logic;
  signal cdc_trigger_read_enable        : std_logic_vector(1 downto 0);
  signal cdc_empty                      : std_logic_vector(1 downto 0);
  
  signal bcid                           : std_logic_vector(11 downto 0) := (others => '0');
  signal bcid_320                       : std_logic_vector(11 downto 0) := (others => '0');
  signal bcid_to_data_handler           : std_logic_vector(11 downto 0) := (others => '0');
  signal bcid_to_data_handler_r         : array_20x12b;
  signal read_en_data_handler           : std_logic;
  signal read_en_data_handler_r         : std_logic;
  signal read_en_data_handler_r2        : std_logic_vector(19 downto 0);
  
  type state_type is (IDLE, HEADER, WORD1, WORD2, WORD3, WORD4, FOOTER);
  type array_4xstate_type is array(3 downto 0) of state_type;
  signal state : array_4xstate_type := (others => IDLE);
  
  signal word_28_SLR0_int   : array_10x28b;
  signal word_28_SLR0_int_r : array_10x28b;
  signal word_28_SLR1_int   : array_20x28b;
  signal word_28_SLR1_int_r : array_20x28b;
  
  signal word_28_int_r      : array_20x28b;
  
  signal en_8b10b           : std_logic_vector(5 downto 0);
  signal txctrl             : std_logic_vector(95 downto 0);  
  
  begin
  
    en_8b10b <= "111111";
    txctrl   <= (others=>'0');
    
    gtrefclk00_int_DCT(0) <= mgtrefclk0_x0y9_int;
    gtrefclk00_int_DCT(1) <= mgtrefclk0_x1y10_int;
    gtrefclk00_int_DCT(2) <= mgtrefclk0_x1y10_int;
    gtrefclk00_int_DCT(3) <= mgtrefclk0_x1y10_int;
    gtrefclk00_int_DCT(4) <= mgtrefclk0_x1y10_int;
    
    gtrefclk00_int_TDAQ(0) <= mgtrefclk0_x0y11_int;
    gtrefclk00_int_TDAQ(1) <= mgtrefclk0_x0y11_int;
    
    
    -- ===================================================================================================================
    -- BUFFERS
    -- ===================================================================================================================
 
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y9_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y9_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y9_p,         
      IB => mgtrefclk0_x0y9_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X0Y11_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x0y11_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x0y11_p,         
      IB => mgtrefclk0_x0y11_n        
    );
    
    IBUFDS_GTE4_MGTREFCLK0_X1Y10_inst : IBUFDS_GTE4
    generic map (
      REFCLK_EN_TX_PATH => '0',   
      REFCLK_HROW_CK_SEL => "00", 
      REFCLK_ICNTL_RX => "00"     
    )
    port map (
      O => mgtrefclk0_x1y10_int,         
      ODIV2 => open, 
      CEB => '0',     
      I => mgtrefclk0_x1y10_p,         
      IB => mgtrefclk0_x1y10_n        
    );
    

    gtwiz_userclk_tx_reset_int_DCT  <= not (and_reduce(txpmaresetdone_int_DCT));
    gtwiz_userclk_rx_reset_int_DCT  <= not (and_reduce(rxpmaresetdone_int_DCT));
    gtwiz_userclk_tx_reset_int_TDAQ <= not (and_reduce(txpmaresetdone_int_TDAQ));
    gtwiz_userclk_rx_reset_int_TDAQ <= not (and_reduce(rxpmaresetdone_int_TDAQ));
    
    reset_gt_int_SLR2 <= '0';
   
 GT_DCT_SLR2_wrapper_inst: entity work.GT_DCT_SLR2_example_wrapper
 port map
 (
   gtyrxn_in                               => gtyrxn_DCT_in,
   gtyrxp_in                               => gtyrxp_DCT_in,
   gtytxn_out                              => gtytxn_DCT_out,
   gtytxp_out                              => gtytxp_DCT_out,
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_DCT,
   gtwiz_userclk_tx_srcclk_out             => open, 
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => gtwiz_userclk_tx_active_int_DCT,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_DCT,
   gtwiz_userclk_rx_srcclk_out             => open, 
   gtwiz_userclk_rx_usrclk_out             => open, 
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open, 
   gtwiz_reset_clk_freerun_in              => clock_80_in,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR2,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR2,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR2,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR2,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open, 
   gtwiz_reset_tx_done_out                 => open, 
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_DCT,
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_DCT,
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_DCT,
   gtrefclk00_in                           => gtrefclk00_int_DCT,
   qpll0outclk_out                         => qpll0outclk_int_DCT,
   qpll0outrefclk_out                      => qpll0outrefclk_int_DCT,
   rxslide_in                              => rxslide_int_DCT,
   gtpowergood_out                         => gtpowergood_int_DCT,
   rxpmaresetdone_out                      => rxpmaresetdone_int_DCT,
   txpmaresetdone_out                      => txpmaresetdone_int_DCT,
   clock_320_in                            => clock_320_in,
   clock_80_in                             => clock_80_in
);
 

GT_MDTTP_wrapper_inst: entity work.GT_MDTTP_SLR2_example_wrapper 
 port map  
 ( 
   gtyrxn_in                               => gtyrxn_TDAQ_in (3 downto 0),
   gtyrxp_in                               => gtyrxp_TDAQ_in (3 downto 0),
   gtytxn_out                              => gtytxn_TDAQ_out (3 downto 0),
   gtytxp_out                              => gtytxp_TDAQ_out (3 downto 0),
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_TDAQ,
   gtwiz_userclk_tx_srcclk_out             => open,
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => open,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_TDAQ,
   gtwiz_userclk_rx_srcclk_out             => open,
   gtwiz_userclk_rx_usrclk_out             => open,
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open,
   gtwiz_reset_clk_freerun_in              => clock_80_in,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR2,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR2,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR2,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR2,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open,
   gtwiz_reset_tx_done_out                 => open,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_TDAQ(0),
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_TDAQ (127 downto 0),
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_TDAQ (127 downto 0),
   gtrefclk01_in                           => gtrefclk00_int_TDAQ(0),
   qpll1outclk_out                         => qpll0outclk_int_TDAQ(0),
   qpll1outrefclk_out                      => qpll0outrefclk_int_TDAQ(0),
   rx8b10ben_in                            => en_8b10b (3 downto 0),
   tx8b10ben_in                            => en_8b10b (3 downto 0),
   txctrl0_in                              => txctrl (63 downto 0),
   txctrl1_in                              => txctrl (63 downto 0),
   txctrl2_in                              => txctrl (31 downto 0),
   gtpowergood_out                         => gtpowergood_int_TDAQ (3 downto 0),
   rxctrl0_out                             => open,
   rxctrl1_out                             => open,
   rxctrl2_out                             => open,
   rxctrl3_out                             => open,
   rxpmaresetdone_out                      => rxpmaresetdone_int_TDAQ (3 downto 0),
   txpmaresetdone_out                      => txpmaresetdone_int_TDAQ (3 downto 0),
   clock_240_in                            => clock_240_in
);

GT_MUCTPI_wrapper_inst: entity work.GT_MUCTPI_SLR2_example_wrapper 
 port map  
 ( 
   gtyrxn_in                               => gtyrxn_TDAQ_in (5 downto 4),
   gtyrxp_in                               => gtyrxp_TDAQ_in (5 downto 4),
   gtytxn_out                              => gtytxn_TDAQ_out (5 downto 4),
   gtytxp_out                              => gtytxp_TDAQ_out (5 downto 4),
   gtwiz_userclk_tx_reset_in               => gtwiz_userclk_tx_reset_int_TDAQ,
   gtwiz_userclk_tx_srcclk_out             => open,
   gtwiz_userclk_tx_usrclk_out             => open, 
   gtwiz_userclk_tx_usrclk2_out            => open,
   gtwiz_userclk_tx_active_out             => open,
   gtwiz_userclk_rx_reset_in               => gtwiz_userclk_rx_reset_int_TDAQ,
   gtwiz_userclk_rx_srcclk_out             => open,
   gtwiz_userclk_rx_usrclk_out             => open,
   gtwiz_userclk_rx_usrclk2_out            => open,
   gtwiz_userclk_rx_active_out             => open,
   gtwiz_reset_clk_freerun_in              => clock_80_in,
   gtwiz_reset_all_in                      => reset,
   gtwiz_reset_tx_pll_and_datapath_in      => reset_gt_int_SLR2,   -- from VIO
   gtwiz_reset_tx_datapath_in              => reset_gt_int_SLR2,   -- from VIO
   gtwiz_reset_rx_pll_and_datapath_in      => reset_gt_int_SLR2,   -- set to 0 by exdes
   gtwiz_reset_rx_datapath_in              => reset_gt_int_SLR2,   -- from initializatio module and VIO
   gtwiz_reset_rx_cdr_stable_out           => open,
   gtwiz_reset_tx_done_out                 => open,
   gtwiz_reset_rx_done_out                 => gtwiz_reset_rx_done_int_TDAQ(1),
   gtwiz_userdata_tx_in                    => gtwiz_userdata_tx_int_TDAQ (191 downto 128),
   gtwiz_userdata_rx_out                   => gtwiz_userdata_rx_int_TDAQ (191 downto 128),
   gtrefclk01_in                           => gtrefclk00_int_TDAQ(1),
   qpll1outclk_out                         => qpll0outclk_int_TDAQ(1),
   qpll1outrefclk_out                      => qpll0outrefclk_int_TDAQ(1),
   rx8b10ben_in                            => en_8b10b (5 downto 4),
   tx8b10ben_in                            => en_8b10b (5 downto 4),
   txctrl0_in                              => txctrl (95 downto 64),
   txctrl1_in                              => txctrl (95 downto 64),
   txctrl2_in                              => txctrl (79 downto 64),
   gtpowergood_out                         => gtpowergood_int_TDAQ (5 downto 4),
   rxctrl0_out                             => open,
   rxctrl1_out                             => open,
   rxctrl2_out                             => open,
   rxctrl3_out                             => open,
   rxpmaresetdone_out                      => rxpmaresetdone_int_TDAQ (5 downto 4),
   txpmaresetdone_out                      => txpmaresetdone_int_TDAQ (5 downto 4),
   clock_240_in                            => clock_240_in
);
 
GEN_DOWNLINK_SLR2: for i in 0 to 19 generate
downlink_inst_SLR2 : lpgbtfpga_downlink
    generic map (
        c_multicyleDelay => 1,
        c_clockRatio     => 2,
        c_outputWidth    => 32
    )
    port map (
        clk_i                 => clock_80_in, 
        clkEn_i               => downlink_clock_en_SLR2(i),
        rst_n_i               => txpmaresetdone_int_DCT(i), -- active low reset
        userData_i            => downlink_user_data_SLR2(i),
        ECData_i              => downlink_ec_data_SLR2(i),
        ICData_i              => downlink_ic_data_SLR2(i),
        mgt_word_o            => gtwiz_userdata_tx_int_DCT(31+32*i downto 32*i),
        interleaverBypass_i   => '0',
        encoderBypass_i       => '0',
        scramblerBypass_i     => '0',
        rdy_o                 => downlink_ready_SLR2(i)
    );
 end generate;
 
 
 GEN_UPLINK_SLR2: for i in 0 to 19 generate
   uplink_inst_SLR2 : lpgbtfpga_uplink 
   generic map(
        -- General configuration
        DATARATE                        => 2,                                               --! Datarate selection can be: DATARATE_10G24 or DATARATE_5G12
        FEC                             => 1,                                               --! FEC selection can be: FEC5 or FEC12

        -- Expert parameters
        c_multicyleDelay                => 3,                                               --! Multicycle delay: Used to relax the timing constraints
        c_clockRatio                    => 8,                                               --! Clock ratio is mgt_Userclk / 40 (shall be an integer)
        c_mgtWordWidth                  => 32,                                              --! Bus size of the input word (typically 32 bits)
        c_allowedFalseHeader            => 5,                                               --! Number of false header allowed (among c_allowedFalseHeaderOverN) to avoid unlock on frame error
        c_allowedFalseHeaderOverN       => 64,                                              --! Number of header checked to know wether the lock is lost or not
        c_requiredTrueHeader            => 30,                                              --! Number of consecutive correct header required to go in locked state
        c_bitslip_mindly                => 1,                                               --! Number of clock cycle required when asserting the bitslip signal
        c_bitslip_waitdly               => 40                                               --! Number of clock cycle required before being back in a stable state
   )
   port map (
        -- Clock and reset
        uplinkClk_i                     => clock_320_in,                                    --! Uplink datapath clock (Transceiver Rx User clock, typically 320MHz)
        uplinkClkOutEn_o                => uplink_clk_en_i_SLR2(i),                         --! Clock enable indicating a new data is valid
        uplinkRst_n_i                   =>  gtwiz_reset_rx_done_int_DCT,                    --! Uplink reset signal (Rx ready from the transceiver)

        -- Input
        mgt_word_i                      => gtwiz_userdata_rx_int_DCT(31+32*i downto 32*i),  --! Input frame coming from the MGT

        -- Data
        userData_o                      => uplink_user_data_SLR2(i),                        --! User output (decoded data). The payload size varies depending on the
                                                                                            --! datarate/FEC configuration:
                                                                                            --!     * *FEC5 / 5.12 Gbps*: 112bit
                                                                                            --!     * *FEC12 / 5.12 Gbps*: 98bit
                                                                                            --!     * *FEC5 / 10.24 Gbps*: 230bit
                                                                                            --!     * *FEC12 / 10.24 Gbps*: 202bit
        EcData_o                        => uplink_ec_data_SLR2(i),                          --! EC field value received from the LpGBT
        IcData_o                        => uplink_ic_data_SLR2(i),                          --! IC field value received from the LpGBT

        -- Control
        bypassInterleaver_i             => '0',                                             --! Bypass uplink interleaver (test purpose only)
        bypassFECEncoder_i              => '0',                                             --! Bypass uplink FEC (test purpose only)
        bypassScrambler_i               => '0',                                             --! Bypass uplink scrambler (test purpose only)

        -- Transceiver control
        mgt_bitslipCtrl_o               => rxslide_int_DCT(i),                              --! Control the Bitslip/RxSlide port of the Mgt

        -- Status
        dataCorrected_o                 => uplink_dataCorrected_SLR2(i),                    --! Flag allowing to know which bit(s) were toggled by the FEC
        IcCorrected_o                   => uplink_IcCorrected_SLR2(i),                      --! Flag allowing to know which bit(s) of the IC field were toggled by the FEC
        EcCorrected_o                   => uplink_EcCorrected_SLR2(i),                      --! Flag allowing to know which bit(s) of the EC field  were toggled by the FEC
        rdy_o                           => uplink_rdy_i_SLR2(i),                            --! Ready SIGNAL from the uplink decoder
        frameAlignerEven_o              => open                                             --! Number of bit slip is even (required only for advanced applications)

   );
   end generate;
   
   
   
    GEN_uplink_data_divider: for i in 0 to 19 generate
        uplink_data_divider_inst_SLR2 : uplink_data_divider
            port map (
                clock_in             => clock_320_in, 
                uplink_user_data_in  => uplink_user_data_SLR2(i)(223 downto 0),
                uplink_clk_en_in     => uplink_clk_en_i_SLR2(i),
                word_28_out          => word_28_int(i)
            );
    end generate;

  
        GEN_bcid: process(clock_40_in)
        begin
            if rising_edge(clock_40_in) then
                bcid <= bcid + 1;
            end if;
        end process;
        
        
        
        GEN_bcid_to_data_handler : process(clock_320_in) 
        begin
            if rising_edge(clock_320_in) then
                bcid_320 <= bcid;
                bcid_to_data_handler <= bcid_320;
                word_28_r <= word_28_int;
                for i in 0 to 19 loop
                    bcid_to_data_handler_r(i) <= bcid_to_data_handler;
                end loop;
            end if;
        end process;
        
        
        
     read_enable_data_handler: process(clock_320_in)
     begin
        if rising_edge(clock_320_in) then
            if bcid_320 /= bcid_to_data_handler then 
                read_en_data_handler <= '1';
            else 
                read_en_data_handler <= '0';
            end if;
            read_en_data_handler_r <= read_en_data_handler;
            for i in 0 to 19 loop
                read_en_data_handler_r2(i) <= read_en_data_handler_r;
            end loop;
        end if;
     end process;
  
    GEN_data_handler_BMBO: for i in 0 to 19 generate
        data_handler_BMBO_inst_SLR2 : data_handler_BMBO
            port map(
                clock_in       => clock_320_in, 
                BCID_in        => bcid_to_data_handler_r(i),
                word_28_in     => word_28_r(i),
                read_enable_in => read_en_data_handler_r2(i),
                data_out       => bcid_ordered_data_SLR2(i)
            );
    end generate;
    

            eta_hits_BM1 <= bcid_ordered_data_SLR2(5)(63 downto 0) & bcid_ordered_data_SLR2(4)(63 downto 0) & bcid_ordered_data_SLR2(3)(63 downto 0) & bcid_ordered_data_SLR2(2)(63 downto 0) & bcid_ordered_data_SLR2(1)(63 downto 0) & bcid_ordered_data_SLR2(0)(63 downto 0);
            eta_hits_BM2 <= bcid_ordered_data_SLR2(12)(63 downto 0) & bcid_ordered_data_SLR2(11)(63 downto 0) & bcid_ordered_data_SLR2(10)(63 downto 0) & bcid_ordered_data_SLR2(9)(63 downto 0) & bcid_ordered_data_SLR2(8)(63 downto 0) & bcid_ordered_data_SLR2(7)(63 downto 0) & bcid_ordered_data_SLR2(6)(63 downto 0);
            eta_hits_BO  <= bcid_ordered_data_SLR2(18)(63 downto 0) & bcid_ordered_data_SLR2(17)(63 downto 0) & bcid_ordered_data_SLR2(16)(63 downto 0) & bcid_ordered_data_SLR2(15)(63 downto 0) & bcid_ordered_data_SLR2(14)(63 downto 0) & bcid_ordered_data_SLR2(13)(63 downto 0);   
    
            phi_hits_BM1(0)(100 downto 21) <= bcid_ordered_data_SLR2(0)(143 downto 64);
            phi_hits_BM1(1)(100 downto 21) <= bcid_ordered_data_SLR2(1)(143 downto 64);
            phi_hits_BM1(2)(100 downto 21) <= bcid_ordered_data_SLR2(2)(143 downto 64);
            phi_hits_BM1(3)(100 downto 21) <= bcid_ordered_data_SLR2(3)(143 downto 64);
            phi_hits_BM1(4)(100 downto 21) <= bcid_ordered_data_SLR2(4)(143 downto 64);
            phi_hits_BM1(5)(100 downto 21) <= bcid_ordered_data_SLR2(5)(143 downto 64);
            
            phi_hits_BM2(0)(100 downto 21) <= bcid_ordered_data_SLR2(6)(143 downto 64);
            phi_hits_BM2(1)(100 downto 21) <= bcid_ordered_data_SLR2(7)(143 downto 64);
            phi_hits_BM2(2)(100 downto 21) <= bcid_ordered_data_SLR2(8)(143 downto 64);
            phi_hits_BM2(3)(100 downto 21) <= bcid_ordered_data_SLR2(9)(143 downto 64);
            phi_hits_BM2(4)(100 downto 21) <= bcid_ordered_data_SLR2(10)(143 downto 64);
            phi_hits_BM2(5)(100 downto 21) <= bcid_ordered_data_SLR2(11)(143 downto 64);
            phi_hits_BM2(6)(100 downto 21) <= bcid_ordered_data_SLR2(12)(143 downto 64);
            
            phi_hits_BO(0)(100 downto 21) <= bcid_ordered_data_SLR2(13)(143 downto 64);
            phi_hits_BO(1)(100 downto 21) <= bcid_ordered_data_SLR2(14)(143 downto 64);
            phi_hits_BO(2)(100 downto 21) <= bcid_ordered_data_SLR2(15)(143 downto 64);
            phi_hits_BO(3)(100 downto 21) <= bcid_ordered_data_SLR2(16)(143 downto 64);
            phi_hits_BO(4)(100 downto 21) <= bcid_ordered_data_SLR2(17)(143 downto 64);
            phi_hits_BO(5)(100 downto 21) <= bcid_ordered_data_SLR2(18)(143 downto 64);


    -------------------------- TRIGGER LOGIC --------------------------- 
  
    trigger_inst_SLR2 :  SLtrigger  
        port map(
            file_eta_hits_BI_orig  =>   eta_hits_BI,
            file_eta_hits_BM1_orig =>   eta_hits_BM1,  
            file_eta_hits_BM2_orig =>   eta_hits_BM2,  
            file_eta_hits_BO_orig  =>   eta_hits_BO,  
            file_phi_hits_BI_orig  =>   phi_hits_BI_fromSLR1,
            file_phi_hits_BM1_orig =>   phi_hits_BM1,  
            file_phi_hits_BM2_orig =>   phi_hits_BM2,  
            file_phi_hits_BO_orig  =>   phi_hits_BO,  
            cand_1                 =>   trigger_cand(0),
            cand_2                 =>   trigger_cand(1),
            clock_h                =>   clock_320_in
        );
    
    
    eta_BI_assignment : for i in 0 to 9 generate
        eta_hits_BI(31+32*i downto 0+32*i) <= eta_hits_BI_fromSLR1(31+48*i downto 0+48*i);
    end generate;
    
    
    cdc_fifo_trigger_read_enable_process : process(clock_320_in)
    begin
        if rising_edge(clock_320_in) then 
            for i in 0 to c_MAX_CAND_INDEX loop
                trigger_cand_r(i) <= trigger_cand(i);
            end loop;
            if trigger_cand(c_MAX_CAND_INDEX) /= trigger_cand_r(c_MAX_CAND_INDEX) then 
                cdc_trigger_write_enable <= '1';
            else
                cdc_trigger_write_enable <= '0';
            end if;
        end if;
    end process;
    
    GEN_fifo_cdc_cand : for i in 0 to c_MAX_CAND_INDEX generate
        fifo_cdc_trigger_cand_inst :  fifo_128b_cdc_320to240MHz 
          port map(
            wr_clk  =>  clock_320_in,
            rd_clk  =>  clock_240_in,      
            din     =>  trigger_cand_r(i),
            wr_en   =>  cdc_trigger_write_enable,
            rd_en   =>  cdc_trigger_read_enable(i),
            dout    =>  trigger_cand_toGTY(i),
            full    =>  open,
            empty   =>  cdc_empty(i)
          );
    end generate;
    
    GEN_fifo_cdc_cand_readout : for i in 0 to c_MAX_CAND_INDEX generate
        fifo_cdc_cand_readout_inst : fifo_128b_cdc_320to240MHz
          port map(
            wr_clk  =>  clock_320_in,
            rd_clk  =>  clock_240_in,
            din     =>  trigger_cand(i),
            wr_en   =>  '1',
            rd_en   =>  '1',
            dout    =>  trigger_cand_to_readout(i),
            full    =>  open,
            empty   =>  open
          );
      end generate;
    
    word_trigger_SLR2_out <= trigger_cand_to_readout(0) & trigger_cand_to_readout(1);
    
    divide_trigger_cand : process(clock_240_in)   -- 4 trigger candidates sent to MDTTP           
    begin
        if rising_edge(clock_240_in) then                 
            for i in 0 to c_MAX_CAND_INDEX loop
                case state(i) is 
                    when IDLE =>
                        if cdc_empty(i) = '0' then
                            cdc_trigger_read_enable(i) <= '1';
                            state(i) <= HEADER;
                        else 
                            cdc_trigger_read_enable(i) <= '0'; 
                        end if;
                    when HEADER => 
                        gtwiz_userdata_tx_int_TDAQ(31+32*i downto 32*i) <= (others => '0'); -- header
                        cdc_trigger_read_enable(i) <= '0'; 
                        state(i) <= WORD1;
                    when WORD1 =>
                        gtwiz_userdata_tx_int_TDAQ(31+32*i downto 32*i) <= trigger_cand_toGTY(i)(127 downto 96);
                        state(i)  <= WORD2;
                    when WORD2 =>
                        gtwiz_userdata_tx_int_TDAQ(31+32*i downto 32*i) <= trigger_cand_toGTY(i)(95 downto 64);
                        state(i)  <= WORD3;
                    when WORD3 =>
                        gtwiz_userdata_tx_int_TDAQ(31+32*i downto 32*i) <= trigger_cand_toGTY(i)(63 downto 32);
                        state(i)  <= WORD4;
                    when WORD4 =>
                        gtwiz_userdata_tx_int_TDAQ(31+32*i downto 32*i) <= trigger_cand_toGTY(i)(31 downto 0);
                        state(i)  <= FOOTER;
                    when FOOTER =>
                        gtwiz_userdata_tx_int_TDAQ(31+32*i downto 32*i) <= (others => '0'); --footer
                        if cdc_empty(0) = '1' then
                            state(i) <= IDLE;
                        else 
                            cdc_trigger_read_enable(i) <= '1';
                            state(i) <= HEADER;
                        end if;
                    when others =>
                        state(i) <= IDLE;
                end case; 
            end loop;   
        end if;
    end process;
    
    
    ------------------------------------ END OF TRIGGER LOGIC ----------------------------------
    
    ------  FIFOs and registers to pass readout data to SLR3 --------
    
    GEN_fifo_readout_SLR2 : for k in 0 to 19 generate
        fifo_readout_SLR2 : fifo_16x28b
        port map(
            wr_clk   => clock_320_in,
            rd_clk   => clock_240_in,
            din   => word_28_int(k), 
            wr_en => '1', 
            rd_en => '1', 
            dout  => word_28_int_r(k),
            full  => open,
            empty => open
        );
    end generate;
    
    process(clock_240_in)
    begin
        if rising_edge(clock_240_in) then
            word_28_out      <= word_28_int_r;
            word_28_SLR0_int <= word_28_SLR0_in;
            word_28_SLR1_int <= word_28_SLR1_in;
            word_28_SLR1_out <= word_28_SLR1_int_r;
        end if;
    end process;
    
    GEN_fifo_readout_SLR0 : for k in 0 to 9 generate
        fifo_readout_SLR0 : fifo_16x28b_sameclock
        port map(
            clk => clock_240_in,
            din    => word_28_SLR0_int(k),
            wr_en  => '1', 
            rd_en  => '1', 
            dout   => word_28_SLR0_out(k),
            full   => open,
            empty  => open
        );
    end generate;
    
    GEN_fifo_readout_SLR1 : for k in 0 to 19 generate
        fifo_readout_SLR1 : fifo_16x28b_sameclock
        port map(
            clk => clock_240_in,
            din    => word_28_SLR1_int(k), 
            wr_en  => '1', 
            rd_en  => '1', 
            dout   => word_28_SLR1_int_r(k),
            full   => open,
            empty  => open
        );
    end generate;
    
    fifo_trigger_readout_SLR1 : fifo_16x256b
    port map(
        clk => clock_240_in,
        din    => word_trigger_SLR1_in,
        wr_en  => '1',
        rd_en  => '1',
        dout   => word_trigger_SLR1_out,
        full   => open,
        empty  => open
    );
    
    
 
  ----- creation of data to be sent to downlink : TO BE REMOVED---
   
   downlink_data_gen_SLR2 : process(clock_80_in)   
    begin
        if rising_edge(clock_80_in) then             
            if gtwiz_userclk_tx_active_int_DCT = '0' then
                downlink_user_data_gen_SLR2 <= (others => '0');
                downlink_ec_data_gen_SLR2   <= (others => '0');
                downlink_ic_data_gen_SLR2   <= (others => '0');
                downlink_clock_en_gen_SLR2  <= '0';
            else
                    downlink_clock_en_gen_SLR2 <= not downlink_clock_en_gen_SLR2;
                    if downlink_clock_en_gen_SLR2 = '0' then
                        downlink_user_data_gen_SLR2 <= downlink_user_data_gen_SLR2 + 1;
                        downlink_ec_data_gen_SLR2   <= downlink_ec_data_gen_SLR2 + 1;
                        downlink_ic_data_gen_SLR2   <= downlink_ic_data_gen_SLR2 + 1;
                    end if;
            end if;
        end if;
    end process;
    
 
 DOWNLINK_ASSIGNMENT_SLR2: for i in 0 to 19 generate
    downlink_user_data_SLR2(i) <= downlink_user_data_gen_SLR2;
    downlink_ec_data_SLR2(i)   <= downlink_ec_data_gen_SLR2;
    downlink_ic_data_SLR2(i)   <= downlink_ic_data_gen_SLR2;
    downlink_clock_en_SLR2(i)  <= downlink_clock_en_gen_SLR2;
 end generate;
 
---------------------------------------------------------------------------

        
end RTL;

