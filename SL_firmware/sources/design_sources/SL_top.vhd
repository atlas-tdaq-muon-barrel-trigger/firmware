-------------------------------------------------------
-- File:          SL_top.vhd
-- Project:       Barrel SL firmware
-- Author:        Federico Morodei <federico.morodei@cern.ch>
-- Last modified: 2023/05/18
-------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
library xil_defaultlib;
use xil_defaultlib.my_lib.all;
use xil_defaultlib.trigalgopkg.all;


entity SL_top is
  Port ( 
        base_clock_40_p                      : in std_logic;
        base_clock_40_n                      : in std_logic;
        mgtrefclk0_x0y2_p_SLR0               : in std_logic;
        mgtrefclk0_x0y2_n_SLR0               : in std_logic;
        mgtrefclk0_x0y3_p_SLR0               : in std_logic;
        mgtrefclk0_x0y3_n_SLR0               : in std_logic;
        mgtrefclk0_x1y1_p_SLR0               : in std_logic;
        mgtrefclk0_x1y1_n_SLR0               : in std_logic;
        gtyrxn_DCT_in_SLR0                   : in std_logic_vector(9 downto 0);
        gtyrxp_DCT_in_SLR0                   : in std_logic_vector(9 downto 0);
        gtytxn_DCT_out_SLR0                  : out std_logic_vector(9 downto 0);
        gtytxp_DCT_out_SLR0                  : out std_logic_vector(9 downto 0);
        gtyrxn_TDAQ_in_SLR0                  : in std_logic_vector(8 downto 0);
        gtyrxp_TDAQ_in_SLR0                  : in std_logic_vector(8 downto 0);
        gtytxn_TDAQ_out_SLR0                 : out std_logic_vector(8 downto 0);
        gtytxp_TDAQ_out_SLR0                 : out std_logic_vector(8 downto 0);
         
        mgtrefclk0_x0y5_p_SLR1               : in std_logic;
        mgtrefclk0_x0y5_n_SLR1               : in std_logic;
        mgtrefclk0_x0y7_p_SLR1               : in std_logic;
        mgtrefclk0_x0y7_n_SLR1               : in std_logic;
        mgtrefclk0_x1y6_p_SLR1               : in std_logic;
        mgtrefclk0_x1y6_n_SLR1               : in std_logic;
        gtyrxn_DCT_in_SLR1                   : in std_logic_vector(19 downto 0);
        gtyrxp_DCT_in_SLR1                   : in std_logic_vector(19 downto 0);
        gtytxn_DCT_out_SLR1                  : out std_logic_vector(19 downto 0);
        gtytxp_DCT_out_SLR1                  : out std_logic_vector(19 downto 0);
        gtyrxn_TDAQ_in_SLR1                  : in std_logic_vector(5 downto 0);
        gtyrxp_TDAQ_in_SLR1                  : in std_logic_vector(5 downto 0);
        gtytxn_TDAQ_out_SLR1                 : out std_logic_vector(5 downto 0);
        gtytxp_TDAQ_out_SLR1                 : out std_logic_vector(5 downto 0);
        
        mgtrefclk0_x0y9_p_SLR2               : in std_logic;
        mgtrefclk0_x0y9_n_SLR2               : in std_logic;
        mgtrefclk0_x0y11_p_SLR2              : in std_logic;
        mgtrefclk0_x0y11_n_SLR2              : in std_logic;
        mgtrefclk0_x1y10_p_SLR2              : in std_logic;
        mgtrefclk0_x1y10_n_SLR2              : in std_logic;
        gtyrxn_DCT_in_SLR2                   : in std_logic_vector(19 downto 0);
        gtyrxp_DCT_in_SLR2                   : in std_logic_vector(19 downto 0);
        gtytxn_DCT_out_SLR2                  : out std_logic_vector(19 downto 0);
        gtytxp_DCT_out_SLR2                  : out std_logic_vector(19 downto 0);
        gtyrxn_TDAQ_in_SLR2                  : in std_logic_vector(5 downto 0);
        gtyrxp_TDAQ_in_SLR2                  : in std_logic_vector(5 downto 0);
        gtytxn_TDAQ_out_SLR2                 : out std_logic_vector(5 downto 0);
        gtytxp_TDAQ_out_SLR2                 : out std_logic_vector(5 downto 0);
        
        mgtrefclk0_x0y13_p_SLR3              : in std_logic;
        mgtrefclk0_x0y13_n_SLR3              : in std_logic;
        gtyrxn_TDAQ_in_SLR3                  : in std_logic_vector(3 downto 0);
        gtyrxp_TDAQ_in_SLR3                  : in std_logic_vector(3 downto 0);
        gtytxn_TDAQ_out_SLR3                 : out std_logic_vector(3 downto 0);
        gtytxp_TDAQ_out_SLR3                 : out std_logic_vector(3 downto 0);

        L0_acc                               : in std_logic;
        reset                                : in std_logic
  );
end SL_top;

architecture Rtl of SL_top is

    COMPONENT fifo_16x480b is
        port (
            clk   : IN STD_LOGIC;
            din   : IN STD_LOGIC_VECTOR(479 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout  : OUT STD_LOGIC_VECTOR(479 DOWNTO 0);
            full  : OUT STD_LOGIC;
            empty : OUT STD_LOGIC
        );
    end COMPONENT;
    
    COMPONENT fifo_16x128b is
        port (
            clk   : IN STD_LOGIC;
            din   : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout  : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
            full  : OUT STD_LOGIC;
            empty : OUT STD_LOGIC
        );
    end COMPONENT;
    
    
    component clk_multiplier
       Port ( clk_out_40MHz  : out std_logic;
              clk_out_320MHz : out std_logic;
              clk_out_80MHz  : out std_logic;
              clk_out_240MHz : out std_logic;
              locked         : out std_logic;
              clk_in1_p      : in std_logic;
              clk_in1_n      : in std_logic );
    end component; 

--    signal base_clock_40 : std_logic;
    signal clock_320_int : std_logic;
    signal clock_40_int  : std_logic;
    signal clock_80_int  : std_logic;
    signal clock_240_int : std_logic;
    signal reset_int     : std_logic;
    
    signal eta_hits_BI          : t_ETASTRIP_BI_WIDER;
    signal eta_hits_BI_toSLR1   : t_ETASTRIP_BI_WIDER;
    signal eta_hits_BI_fromSLR1 : t_ETASTRIP_BI_WIDER;
    signal eta_hits_BI_toSLR2   : t_ETASTRIP_BI_WIDER;
    signal phi_hits_BI          : t_PHISTRIP_BI; 
    signal phi_hits_BI_toSLR1   : t_PHISTRIP_BI;
    signal phi_hits_BI_fromSLR1 : t_PHISTRIP_BI;
    signal phi_hits_BI_toSLR2   : t_PHISTRIP_BI; 
    
    signal word_28_SLR0    : array_10x28b;
    signal word_28_SLR1    : array_20x28b;
    signal word_28_SLR2    : array_20x28b;
     
    signal word_28_SLR0_readout   : array_10x28b;
    signal word_28_SLR1_readout   : array_20x28b;
    signal word_28_SLR2_readout   : array_20x28b;
    signal word_28_SLR0_r         : array_10x28b;
    signal word_28_SLR0_r2        : array_10x28b;
    signal word_28_SLR1_tmp       : array_20x28b;

    signal word_trigger_SLR1         : std_logic_vector(255 downto 0);
    signal word_trigger_SLR1_readout : std_logic_vector(255 downto 0);
    signal word_trigger_SLR2_readout : std_logic_vector(255 downto 0);
    signal word_trigger_SLR1_readout_r : std_logic_vector(255 downto 0);
    signal word_trigger_SLR2_readout_r : std_logic_vector(255 downto 0);
    


begin

   buf_reset : IBUF
   port map (
       I => reset,
       O => reset_int
   );

--   IBUFDS_inst : IBUFDS
--   generic map (
--      DIFF_TERM => FALSE,  
--      IBUF_LOW_PWR => TRUE,
--      IOSTANDARD => "DEFAULT")
--   port map (
--      O  => base_clock_40,  
--      I  => base_clock_40_p,  
--      IB => base_clock_40_n 
--   );
   
   clk_multiplier_inst : clk_multiplier
   port map (
      clk_out_40MHz  => clock_40_int,
      clk_out_320MHz => clock_320_int,
      clk_out_80MHz  => clock_80_int,
      clk_out_240MHz => clock_240_int,
      locked         => open,
      clk_in1_p      => base_clock_40_p,
      clk_in1_n      => base_clock_40_n
   );
   


SLR0_inst : entity work.SLR0_top
    port map
    (
        -- Differential reference clock inputs SLR0
        mgtrefclk0_x0y2_p               => mgtrefclk0_x0y2_p_SLR0,
        mgtrefclk0_x0y2_n               => mgtrefclk0_x0y2_n_SLR0,
        mgtrefclk0_x1y1_p               => mgtrefclk0_x1y1_p_SLR0,
        mgtrefclk0_x1y1_n               => mgtrefclk0_x1y1_n_SLR0,
        
        -- Serial data ports for DCT transceivers        
        gtyrxn_DCT_in                   => gtyrxn_DCT_in_SLR0,  
        gtyrxp_DCT_in                   => gtyrxp_DCT_in_SLR0,  
        gtytxn_DCT_out                  => gtytxn_DCT_out_SLR0,
        gtytxp_DCT_out                  => gtytxp_DCT_out_SLR0,
               
        -- Serial data ports for other transceivers
        gtyrxn_TDAQ_in                  => gtyrxn_TDAQ_in_SLR0,
        gtyrxp_TDAQ_in                  => gtyrxp_TDAQ_in_SLR0,
        gtytxn_TDAQ_out                 => gtytxn_TDAQ_out_SLR0,
        gtytxp_TDAQ_out                 => gtytxp_TDAQ_out_SLR0,
                
        -- eta hits from BI DCTs         
        eta_hits_out => eta_hits_BI,
                                         
        -- phi hits from BI DCTs         
        phi_hits_out => phi_hits_BI,
        
        -- data to readout
        word_28_out     => word_28_SLR0,
        
        clock_320_in    => clock_320_int,
        clock_240_in    => clock_240_int,
        clock_80_in     => clock_80_int,
        clock_40_in     => clock_40_int,
        reset           => reset_int
    );


    SLR1_inst : entity work.SLR1_top
    port map
    (
        -- Differential reference clock inputs SLR1
        mgtrefclk0_x0y5_p               => mgtrefclk0_x0y5_p_SLR1,
        mgtrefclk0_x0y5_n               => mgtrefclk0_x0y5_n_SLR1,
        mgtrefclk0_x0y7_p               => mgtrefclk0_x0y7_p_SLR1,
        mgtrefclk0_x0y7_n               => mgtrefclk0_x0y7_n_SLR1,
        mgtrefclk0_x1y6_p               => mgtrefclk0_x1y6_p_SLR1,
        mgtrefclk0_x1y6_n               => mgtrefclk0_x1y6_n_SLR1,
        
        -- Serial data ports for DCT transceivers        
        gtyrxn_DCT_in                   => gtyrxn_DCT_in_SLR1,  
        gtyrxp_DCT_in                   => gtyrxp_DCT_in_SLR1,  
        gtytxn_DCT_out                  => gtytxn_DCT_out_SLR1,
        gtytxp_DCT_out                  => gtytxp_DCT_out_SLR1,
               
        -- Serial data ports for other transceivers
        gtyrxn_TDAQ_in                  => gtyrxn_TDAQ_in_SLR1,
        gtyrxp_TDAQ_in                  => gtyrxp_TDAQ_in_SLR1,
        gtytxn_TDAQ_out                 => gtytxn_TDAQ_out_SLR1,
        gtytxp_TDAQ_out                 => gtytxp_TDAQ_out_SLR1,
                
        -- eta hits from BI DCTs
        eta_hits_BI_fromSLR0 => eta_hits_BI_toSLR1,
        eta_hits_BI_toSLR2   => eta_hits_BI_fromSLR1,
         
        -- phi hits from BI DCTs
        phi_hits_BI_fromSLR0 => phi_hits_BI_toSLR1,
        phi_hits_BI_toSLR2   => phi_hits_BI_fromSLR1,
        
        -- data to readout
        word_28_out       => word_28_SLR1,
        word_28_SLR0_in   => word_28_SLR0, 
        word_28_SLR0_out  => word_28_SLR0_r,
        word_trigger_out  => word_trigger_SLR1,
        
        clock_320_in      => clock_320_int,
        clock_240_in      => clock_240_int,
        clock_80_in       => clock_80_int,
        clock_40_in       => clock_40_int,
        reset             => reset_int
    );
    
    
    SLR2_inst : entity work.SLR2_top
    port map
    (
        -- Differential reference clock inputs SLR2
        mgtrefclk0_x0y9_p               => mgtrefclk0_x0y9_p_SLR2,
        mgtrefclk0_x0y9_n               => mgtrefclk0_x0y9_n_SLR2,
        mgtrefclk0_x0y11_p              => mgtrefclk0_x0y11_p_SLR2,
        mgtrefclk0_x0y11_n              => mgtrefclk0_x0y11_n_SLR2,
        mgtrefclk0_x1y10_p              => mgtrefclk0_x1y10_p_SLR2,
        mgtrefclk0_x1y10_n              => mgtrefclk0_x1y10_n_SLR2,
        
        -- Serial data ports for DCT transceivers        
        gtyrxn_DCT_in                   => gtyrxn_DCT_in_SLR2,  
        gtyrxp_DCT_in                   => gtyrxp_DCT_in_SLR2,  
        gtytxn_DCT_out                  => gtytxn_DCT_out_SLR2,
        gtytxp_DCT_out                  => gtytxp_DCT_out_SLR2,
               
        -- Serial data ports for other transceivers
        gtyrxn_TDAQ_in                  => gtyrxn_TDAQ_in_SLR2,
        gtyrxp_TDAQ_in                  => gtyrxp_TDAQ_in_SLR2,
        gtytxn_TDAQ_out                 => gtytxn_TDAQ_out_SLR2,
        gtytxp_TDAQ_out                 => gtytxp_TDAQ_out_SLR2,
                
        -- eta hits from BI DCTs           
        eta_hits_BI_fromSLR1 => eta_hits_BI_toSLR2,  
                                         
        -- phi hits from BI DCTs         
        phi_hits_BI_fromSLR1 => phi_hits_BI_toSLR2,
        
        -- data to readout
        word_28_out           => word_28_SLR2_readout,
        word_28_SLR0_in       => word_28_SLR0_r,
        word_28_SLR0_out      => word_28_SLR0_readout,
        word_28_SLR1_in       => word_28_SLR1, 
        word_28_SLR1_out      => word_28_SLR1_readout,
        word_trigger_SLR1_in  => word_trigger_SLR1,
        word_trigger_SLR1_out => word_trigger_SLR1_readout,
        word_trigger_SLR2_out => word_trigger_SLR2_readout,
        
        clock_320_in    => clock_320_int,
        clock_40_in     => clock_40_int,
        clock_240_in    => clock_240_int,
        clock_80_in     => clock_80_int,
        reset           => reset_int
    );


        eta_BI_fifo_toSLR1 : fifo_16x480b
        port map ( 
            clk   => clock_320_int,
            din   => eta_hits_BI, 
            wr_en => '1', 
            rd_en => '1',
            dout  => eta_hits_BI_toSLR1,
            full  => open,
            empty => open
        ); 
    
    
        eta_BI_fifo_toSLR2 : fifo_16x480b
        port map ( 
            clk   => clock_320_int,
            din   => eta_hits_BI_fromSLR1, 
            wr_en => '1', 
            rd_en => '1',
            dout  => eta_hits_BI_toSLR2,
            full  => open,
            empty => open
        ); 
    
        GEN_phi_BI_fifo_layers_toSLR1 : for k in 0 to 9 generate
            phi_BI_fifo_toSLR1 : fifo_16x128b
            port map ( 
                clk   => clock_320_int,
                din   => phi_hits_BI(k), 
                wr_en => '1', 
                rd_en => '1',
                dout  => phi_hits_BI_toSLR1(k),
                full  => open,
                empty => open
            ); 
        end generate;
    
        GEN_phi_BI_fifo_layers_toSLR2 : for k in 0 to 9 generate
            phi_BI_fifo_toSLR2 : fifo_16x128b
            port map ( 
                clk   => clock_320_int,
                din   => phi_hits_BI_fromSLR1(k), 
                wr_en => '1', 
                rd_en => '1', 
                dout  => phi_hits_BI_toSLR2(k),
                full  => open,
                empty => open
            ); 
        end generate;


    SLR3_inst : entity work.SLR3_top 
    port map
    (   mgtrefclk0_x0y13_p  => mgtrefclk0_x0y13_p_SLR3,
        mgtrefclk0_x0y13_n  => mgtrefclk0_x0y13_n_SLR3,
        
        -- Serial data ports for  transceivers to FELIX 
        gtyrxn_TDAQ_in                  => gtyrxn_TDAQ_in_SLR3,
        gtyrxp_TDAQ_in                  => gtyrxp_TDAQ_in_SLR3,
        gtytxn_TDAQ_out                 => gtytxn_TDAQ_out_SLR3,
        gtytxp_TDAQ_out                 => gtytxp_TDAQ_out_SLR3,
                        
        -- DCT data
        word_28_SLR0_in => word_28_SLR0_readout,
        word_28_SLR1_in => word_28_SLR1_readout,
        word_28_SLR2_in => word_28_SLR2_readout,
        
        -- Trigger candidates
        word_trigger_SLR1_in => word_trigger_SLR1_readout_r,
        word_trigger_SLR2_in => word_trigger_SLR2_readout_r,
       
        clock_320_in    => clock_320_int,
        clock_240_in    => clock_240_int, 
        clock_80_in     => clock_80_int,
        clock_40_in     => clock_40_int, 
        L0_acc          => L0_acc,
        reset           => reset_int
    );
    
    process (clock_240_int) 
    begin
        if rising_edge(clock_240_int) then
            word_trigger_SLR1_readout_r <= word_trigger_SLR1_readout;
            word_trigger_SLR2_readout_r <= word_trigger_SLR1_readout;
        end if;                
    end process;


end Rtl;
