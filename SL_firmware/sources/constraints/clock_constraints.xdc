

##### create clocks
create_clock -period 3.125 -name clk_mgtrefclk0_x1y1_p [get_ports mgtrefclk0_x1y1_p_SLR0]
create_clock -period 4.167 -name clk_mgtrefclk0_x0y2_p [get_ports mgtrefclk0_x0y2_p_SLR0]
create_clock -period 4.167 -name clk_mgtrefclk0_x0y3_p [get_ports mgtrefclk0_x0y3_p_SLR0]
create_clock -period 3.125 -name clk_mgtrefclk0_x0y5_p [get_ports mgtrefclk0_x0y5_p_SLR1]
create_clock -period 4.167 -name clk_mgtrefclk0_x0y7_p [get_ports mgtrefclk0_x0y7_p_SLR1]
create_clock -period 3.125 -name clk_mgtrefclk0_x1y6_p [get_ports mgtrefclk0_x1y6_p_SLR1]
create_clock -period 3.125 -name clk_mgtrefclk0_x0y9_p [get_ports mgtrefclk0_x0y9_p_SLR2]
create_clock -period 4.167 -name clk_mgtrefclk0_x0y11_p [get_ports mgtrefclk0_x0y11_p_SLR2]
create_clock -period 3.125 -name clk_mgtrefclk0_x1y10_p [get_ports mgtrefclk0_x1y10_p_SLR2]
create_clock -period 4.167 -name clk_mgtrefclk0_x0y13_p [get_ports mgtrefclk0_x0y13_p_SLR3]     

create_clock -period 25.000 -name base_clock_40_p [get_ports base_clock_40_p]


### assign pins
set_property package_pin BB12 [get_ports mgtrefclk0_x1y1_n_SLR0]
set_property package_pin BB13 [get_ports mgtrefclk0_x1y1_p_SLR0]

set_property package_pin AY40 [get_ports mgtrefclk0_x0y2_n_SLR0]
set_property package_pin AY39 [get_ports mgtrefclk0_x0y2_p_SLR0]

set_property package_pin AV40 [get_ports mgtrefclk0_x0y3_n_SLR0]
set_property package_pin AV39 [get_ports mgtrefclk0_x0y3_p_SLR0]

set_property package_pin AP40 [get_ports mgtrefclk0_x0y5_n_SLR1]
set_property package_pin AP39 [get_ports mgtrefclk0_x0y5_p_SLR1]

set_property package_pin AJ42 [get_ports mgtrefclk0_x0y7_n_SLR1]
set_property package_pin AJ41 [get_ports mgtrefclk0_x0y7_p_SLR1]

set_property package_pin AM12 [get_ports mgtrefclk0_x1y6_n_SLR1]
set_property package_pin AM13 [get_ports mgtrefclk0_x1y6_p_SLR1]

set_property package_pin AA42 [get_ports mgtrefclk0_x0y9_n_SLR2]
set_property package_pin AA41 [get_ports mgtrefclk0_x0y9_p_SLR2]

set_property package_pin U42 [get_ports mgtrefclk0_x0y11_n_SLR2]
set_property package_pin U41 [get_ports mgtrefclk0_x0y11_p_SLR2]

set_property package_pin W10 [get_ports mgtrefclk0_x1y10_n_SLR2]
set_property package_pin W11 [get_ports mgtrefclk0_x1y10_p_SLR2]

set_property package_pin N42 [get_ports mgtrefclk0_x0y13_n_SLR3]
set_property package_pin N41 [get_ports mgtrefclk0_x0y13_p_SLR3]

set_property package_pin M21 [get_ports base_clock_40_n]
set_property package_pin M22 [get_ports base_clock_40_p]
