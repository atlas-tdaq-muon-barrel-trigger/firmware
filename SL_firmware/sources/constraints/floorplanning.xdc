create_pblock pblock_SLR0_inst
add_cells_to_pblock [get_pblocks pblock_SLR0_inst] [get_cells -quiet [list SLR0_inst]]
resize_pblock [get_pblocks pblock_SLR0_inst] -add {SLR0}
create_pblock pblock_SLR1_inst
add_cells_to_pblock [get_pblocks pblock_SLR1_inst] [get_cells -quiet [list SLR1_inst]]
resize_pblock [get_pblocks pblock_SLR1_inst] -add {SLR1}
create_pblock pblock_SLR2_inst
add_cells_to_pblock [get_pblocks pblock_SLR2_inst] [get_cells -quiet [list SLR2_inst]]
resize_pblock [get_pblocks pblock_SLR2_inst] -add {SLR2}
create_pblock pblock_SLR3_inst
add_cells_to_pblock [get_pblocks pblock_SLR3_inst] [get_cells -quiet [list SLR3_inst]]
resize_pblock [get_pblocks pblock_SLR3_inst] -add {SLR3}
