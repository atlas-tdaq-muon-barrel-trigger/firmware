
create_project SL_firmware SL_firmware_vivado_project -part xcvu13p-flga2577-1-e
set_property target_language VHDL [current_project]

add_files -scan_for_includes {
    sources/design_sources/my_lib.vhd
    sources/design_sources/SL_top.vhd
    sources/design_sources/top_SLR0.vhd
    sources/design_sources/top_SLR1.vhd
    sources/design_sources/top_SLR2.vhd
    sources/design_sources/top_SLR3.vhd
    sources/design_sources/trigger_logic/SLtrigger.vhd
    sources/design_sources/trigger_logic/trigalgopkg.vhd
    sources/design_sources/interface_logic/data_handler_BI.vhd
    sources/design_sources/interface_logic/data_handler_BMBO.vhd
    sources/design_sources/interface_logic/data_derandomizer_BI.vhd
    sources/design_sources/interface_logic/data_derandomizer_BMBO.vhd
    sources/design_sources/interface_logic/uplink_data_divider.vhd
    sources/design_sources/LPGBT-FPGA_logic/lpgbtfpga_package.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/fec_rsDecoderN15K13.vhd
    sources/design_sources/LPGBT-FPGA_logic/downlink/lpgbtfpga_scrambler.vhd
    sources/design_sources/LPGBT-FPGA_logic/lpgbtfpga_uplink.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/lpgbtfpga_deinterleaver.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/fec_rsDecoderN31K29.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/descrambler_51bitOrder49.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/descrambler_58bitOrder58.vhd
    sources/design_sources/LPGBT-FPGA_logic/downlink/rs_encoder_N7K5.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/lpgbtfpga_rxgearbox.vhd
    sources/design_sources/LPGBT-FPGA_logic/lpgbtfpga_downlink.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/lpgbtfpga_decoder.vhd
    sources/design_sources/LPGBT-FPGA_logic/downlink/lpgbtfpga_interleaver.vhd
    sources/design_sources/LPGBT-FPGA_logic/downlink/lpgbtfpga_encoder.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/lpgbtfpga_descrambler.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/descrambler_60bitOrder58.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/descrambler_53bitOrder49.vhd
    sources/design_sources/LPGBT-FPGA_logic/downlink/lpgbtfpga_txgearbox.vhd
    sources/design_sources/LPGBT-FPGA_logic/uplink/lpgbtfpga_framealigner.vhd
    sources/design_sources/readout_logic/RTL/BI.vhd
    sources/design_sources/readout_logic/RTL/BM.vhd
    sources/design_sources/readout_logic/RTL/memory_block.vhd
    sources/design_sources/readout_logic/RTL/multiplexer_512to1.vhd
    sources/design_sources/readout_logic/RTL/multiplexer_dOut_10DCTto1.vhd
    sources/design_sources/readout_logic/RTL/multiplexer_dOut_19DCTto1.vhd
    sources/design_sources/readout_logic/RTL/my_library.vhd
    sources/design_sources/readout_logic/RTL/top_readout.vhd
    sources/design_sources/readout_logic/RTL/cand_readout.vhd
    sources/design_sources/readout_logic/RTL/top_memory_10DCT.vhd
    sources/design_sources/readout_logic/RTL/top_memory_19DCT.vhd}

set_property file_type {VHDL 2008} [get_files  sources/design_sources/trigger_logic/SLtrigger.vhd]
set_property file_type {VHDL 2008} [get_files  sources/design_sources/trigger_logic/trigalgopkg.vhd]

add_files -fileset constrs_1 -norecurse {
    sources/constraints/TIMING_CONSTRAINTS_SLR0.xdc
    sources/constraints/TIMING_CONSTRAINTS_SLR1.xdc
    sources/constraints/TIMING_CONSTRAINTS_SLR2.xdc
    sources/constraints/clock_constraints.xdc
    sources/constraints/floorplanning.xdc}

add_files -fileset sim_1 -norecurse {
    sources/design_sources/readout_logic/sim/BI_data_generator.vhd
    sources/design_sources/readout_logic/sim/BM_data_generator.vhd
    sources/design_sources/readout_logic/sim/data_generator.vhd
    sources/design_sources/readout_logic/sim/hit_generator.vhd
    sources/design_sources/readout_logic/sim/L0A_generator.vhd
    sources/design_sources/readout_logic/sim/tb_top_readout.vhd
    sources/design_sources/readout_logic/sim/tb_top_cand_readout.vhd
    sources/simulation/SL_tb.vhd
    sources/simulation/lpgbt_emulator/lpgbt_emul_gt_exdes.vhd
    sources/simulation/lpgbt_emulator/lpgbt-emul-master/
    sources/simulation/BMBO_DCT_emulator/BMBO_DCT_wrapper.vhd
    sources/simulation/BMBO_DCT_emulator/frame_aligner.vhd
    sources/simulation/BMBO_DCT_emulator/pack.vhd
    sources/simulation/BMBO_DCT_emulator/top_BMBO_DCT.vhd
    sources/simulation/IP/fifo_16x20b/fifo_16x20b.xci
    sources/simulation/IP/lpgbt_GT/lpgbt_GT.xci
    sources/simulation/IP/lpgbt_GT/lpgbt_GT_example_gtwiz_userclk_rx.v
    sources/simulation/IP/lpgbt_GT/lpgbt_GT_example_gtwiz_userclk_tx.v
    sources/simulation/IP/lpgbt_GT/lpgbt_GT_example_wrapper.v
    sources/simulation/IP/mmcm_200_600/mmcm_200_600.xci
    sources/simulation/IP/mmcm_320/mmcm_320.xci}

add_files -norecurse -scan_for_includes {
    sources/ip/GT_DCT_SLR0/GT_DCT_SLR0.xci
    sources/ip/GT_DCT_SLR1/GT_DCT_SLR1.xci
    sources/ip/GT_DCT_SLR2/GT_DCT_SLR2.xci
    sources/ip/GT_ENDCAP_SLR0/GT_ENDCAP_SLR0.xci
    sources/ip/GT_FELIX_SLR3/GT_FELIX_SLR3.xci
    sources/ip/GT_MDTTP_SLR1/GT_MDTTP_SLR1.xci
    sources/ip/GT_MDTTP_SLR2/GT_MDTTP_SLR2.xci
    sources/ip/GT_MUCTPI_SLR1/GT_MUCTPI_SLR1.xci
    sources/ip/GT_MUCTPI_SLR2/GT_MUCTPI_SLR2.xci
    sources/ip/GT_TILECAL_SLR0/GT_TILECAL_SLR0.xci
    sources/ip/clk_multiplier/clk_multiplier.xci
    sources/ip/fifo_16x128b/fifo_16x128b.xci
    sources/ip/fifo_16x28b/fifo_16x28b.xci
    sources/ip/fifo_16x28b_sameclock/fifo_16x28b_sameclock.xci
    sources/ip/fifo_16x480b/fifo_16x480b.xci
    sources/ip/fifo_128b_cdc_320to240MHz/fifo_128b_cdc_320to240MHz.xci
    sources/design_sources/readout_logic/IP/FIFO_L0A/FIFO_L0A.xci
    sources/design_sources/readout_logic/IP/RAM_32bx8192/RAM_32bx8192.xci
    sources/design_sources/readout_logic/IP/RAM_512bx512/RAM_512bx512.xci
    sources/design_sources/readout_logic/IP/fifo_16x256b/fifo_16x256b.xci}


add_files -norecurse -scan_for_includes {
    sources/ip/GT_wrapper_functions.v
    sources/ip/GT_DCT_SLR0/GT_DCT_SLR0_example_wrapper.v
    sources/ip/GT_DCT_SLR0/GT_DCT_SLR0_example_gtwiz_userclk_rx.v
    sources/ip/GT_DCT_SLR0/GT_DCT_SLR0_example_gtwiz_userclk_tx.v
    sources/ip/GT_DCT_SLR1/GT_DCT_SLR1_example_wrapper.v
    sources/ip/GT_DCT_SLR1/GT_DCT_SLR1_example_gtwiz_userclk_rx.v
    sources/ip/GT_DCT_SLR1/GT_DCT_SLR1_example_gtwiz_userclk_tx.v
    sources/ip/GT_DCT_SLR2/GT_DCT_SLR2_example_wrapper.v
    sources/ip/GT_DCT_SLR2/GT_DCT_SLR2_example_gtwiz_userclk_rx.v
    sources/ip/GT_DCT_SLR2/GT_DCT_SLR2_example_gtwiz_userclk_tx.v
    sources/ip/GT_ENDCAP_SLR0/GT_ENDCAP_SLR0_example_wrapper.v
    sources/ip/GT_ENDCAP_SLR0/GT_ENDCAP_SLR0_example_gtwiz_userclk_rx.v
    sources/ip/GT_ENDCAP_SLR0/GT_ENDCAP_SLR0_example_gtwiz_userclk_tx.v
    sources/ip/GT_FELIX_SLR3/GT_FELIX_SLR3_example_wrapper.v
    sources/ip/GT_FELIX_SLR3/GT_FELIX_SLR3_example_gtwiz_userclk_rx.v
    sources/ip/GT_FELIX_SLR3/GT_FELIX_SLR3_example_gtwiz_userclk_tx.v
    sources/ip/GT_MDTTP_SLR1/GT_MDTTP_SLR1_example_wrapper.v
    sources/ip/GT_MDTTP_SLR1/GT_MDTTP_SLR1_example_gtwiz_userclk_rx.v
    sources/ip/GT_MDTTP_SLR1/GT_MDTTP_SLR1_example_gtwiz_userclk_tx.v
    sources/ip/GT_MDTTP_SLR2/GT_MDTTP_SLR2_example_wrapper.v
    sources/ip/GT_MDTTP_SLR2/GT_MDTTP_SLR2_example_gtwiz_userclk_rx.v
    sources/ip/GT_MDTTP_SLR2/GT_MDTTP_SLR2_example_gtwiz_userclk_tx.v
    sources/ip/GT_MUCTPI_SLR1/GT_MUCTPI_SLR1_example_wrapper.v
    sources/ip/GT_MUCTPI_SLR1/GT_MUCTPI_SLR1_example_gtwiz_userclk_rx.v
    sources/ip/GT_MUCTPI_SLR1/GT_MUCTPI_SLR1_example_gtwiz_userclk_tx.v
    sources/ip/GT_MUCTPI_SLR2/GT_MUCTPI_SLR2_example_wrapper.v
    sources/ip/GT_MUCTPI_SLR2/GT_MUCTPI_SLR2_example_gtwiz_userclk_rx.v
    sources/ip/GT_MUCTPI_SLR2/GT_MUCTPI_SLR2_example_gtwiz_userclk_tx.v
    sources/ip/GT_TILECAL_SLR0/GT_TILECAL_SLR0_example_wrapper.v
    sources/ip/GT_TILECAL_SLR0/GT_TILECAL_SLR0_example_gtwiz_userclk_rx.v
    sources/ip/GT_TILECAL_SLR0/GT_TILECAL_SLR0_example_gtwiz_userclk_tx.v}

generate_target all [get_files sources/ip/GT_DCT_SLR0/GT_DCT_SLR0.xci]
generate_target all [get_files sources/ip/GT_DCT_SLR1/GT_DCT_SLR1.xci]
generate_target all [get_files sources/ip/GT_DCT_SLR2/GT_DCT_SLR2.xci]
generate_target all [get_files sources/ip/GT_ENDCAP_SLR0/GT_ENDCAP_SLR0.xci]
generate_target all [get_files sources/ip/GT_FELIX_SLR3/GT_FELIX_SLR3.xci]
generate_target all [get_files sources/ip/GT_MDTTP_SLR1/GT_MDTTP_SLR1.xci]
generate_target all [get_files sources/ip/GT_MDTTP_SLR2/GT_MDTTP_SLR2.xci]
generate_target all [get_files sources/ip/GT_MUCTPI_SLR1/GT_MUCTPI_SLR1.xci]
generate_target all [get_files sources/ip/GT_MUCTPI_SLR2/GT_MUCTPI_SLR2.xci]
generate_target all [get_files sources/ip/GT_TILECAL_SLR0/GT_TILECAL_SLR0.xci]
generate_target all [get_files sources/ip/clk_multiplier/clk_multiplier.xci]
generate_target all [get_files sources/ip/fifo_16x480b/fifo_16x480b.xci]
generate_target all [get_files sources/ip/fifo_16x128b/fifo_16x128b.xci]
generate_target all [get_files sources/ip/fifo_128b_cdc_320to240MHz/fifo_128b_cdc_320to240MHz.xci]
generate_target all [get_files sources/design_sources/readout_logic/IP/FIFO_L0A/FIFO_L0A.xci]
generate_target all [get_files sources/design_sources/readout_logic/IP/RAM_32bx8192/RAM_32bx8192.xci]
generate_target all [get_files sources/design_sources/readout_logic/IP/RAM_512bx512/RAM_512bx512.xci]
generate_target all [get_files sources/design_sources/readout_logic/IP/fifo_16x256b/fifo_16x256b.xci]
generate_target all [get_files sources/ip/fifo_16x28b/fifo_16x28b.xci]
generate_target all [get_files sources/ip/fifo_16x28b_sameclock/fifo_16x28b_sameclock.xci]
generate_target all [get_files sources/simulation/IP/fifo_16x20b/fifo_16x20b.xci]
generate_target all [get_files sources/simulation/IP/lpgbt_GT/lpgbt_GT.xci]
generate_target all [get_files sources/simulation/IP/mmcm_200_600/mmcm_200_600.xci]
generate_target all [get_files sources/simulation/IP/mmcm_320/mmcm_320.xci]

set_property file_type {Verilog Header} [get_files  sources/ip/GT_wrapper_functions.v]

update_compile_order -fileset sources_1

set_property strategy Flow_PerfThresholdCarry [get_runs synth_1]
set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]

set_property top SL_tb [get_filesets sim_1]
set_property top_lib xil_defaultlib [get_filesets sim_1]
update_compile_order -fileset sim_1

set_property AUTO_INCREMENTAL_CHECKPOINT 0 [get_runs synth_1]
